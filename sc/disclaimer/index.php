<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = '联络';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['免责声明'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>免责声明</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
				<p>本网页所载资料乃香港知专设计学院（职业训练局 (VTC) 机构成员）根据现行情况及政策，利用现有资料製作，并已力求内容正确无误。</p>

				<p>香港知专设计学院定期检讨网页，如有需要，会更新内容。虽然香港知专设计学院已力求网页正确无误，但对于读者因使用或参考网页，或因其中资料不准确、错漏，或因浏览网页或存取其中资料而直接或间接蒙受经济或其他损失，香港知专设计学院不会负任何法律上或其他方面的责任。</p>

				<h3>版权</h3> 
				<p>本网页内容版权属香港知专设计学院所有，使用者须符合以下三个要求，方获批准使用：(1) 以下版权通告及本批准通告须同时见于所有副本；(2) 本网页内容只作为参考 资料及个人或非商业用途，并且不得全部或局部複製或刊登于任何联网电脑，或于任何媒体上广播；(3) 不得对内容作任何修改。法例上明确禁止将本网页内容作 其他用途。</p>

				<h3>个人资料</h3>
				<p>使用者只需提供某些个人资料，便可经本网页电邮至香港知专设计学院，进一步查询更多资料或问题。香港知专设计学院一向十分重视提供予本网页的所有个人资料。如使用者未能提供有关个人 资料，香港知专设计学院将无法提供使用者所需的进一步资料。</p>
				
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>