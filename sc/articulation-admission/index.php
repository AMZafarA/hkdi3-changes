<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Articulation & Education';
$section = 'continuing-education';
$subsection = 'detail';
$sub_nav = 'detail';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$id = $_REQUEST['id'] ?? '';
$dept = DM::load($DB_STATUS.'dept', $id);
$dept_name = $dept['dept_name_'.$lang];

if(!$id){
    header("location: ../index.php");
}


$first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, $id ));

$breadcrumb_arr['设计课程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr['高级文凭课程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr[$dept_name] =$host_name_with_lang.'programmes/programme.php?programmes_id='.$first_programme['programmes_id'];
$breadcrumb_arr['入学申请'] =''; 
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<div class="page-head">
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>入学资讯</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>升学阶梯</strong>
					</h2>
					<div class="txt-editor">
						<h4>
							<strong>HKDI为设计学生打造灵活升学阶梯，学生于修毕两年高级文凭课程后，可选择升读由HKDI联同六所英国着名大学共同提供的一年制的衔接学士学位，或转升其他本地大学。
							</strong>
						</h4>
						<p>如香港中学文凭考生未能达到最低入学要求，可先修读一年基础文凭(设计) 以再升读高级文凭。</p>
						<?php /*
						<a href="#" class="btn">
							<strong>Read more</strong>
						</a>
						*/ ?>
					</div>
				</div>
			</div>
			<div class="color-flow">
				<div class="content-wrapper">
					<div class="color-flow__items">
						<div id="color-flow--hkdse" class="color-flow__item">
								<div class="color-flow__item-title">香港中学文凭</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--hd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-hkdi.png" alt="" />
								<div class="color-flow__item-title">高级文凭课程</div>
								<div class="color-flow__item-label">2年</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--bd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-uk.png" alt="" />
								<div class="color-flow__item-title">英国学士学位</div>
								<div class="color-flow__item-label">1年</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--md" class="color-flow__item">
								<div class="color-flow__item-title">硕士学位</div>
								<div class="color-flow__item-label">1-2年</div>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail article-detail--txt-green" style="background:#000000;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>学费</strong>
					</h2>
					<div class="txt-editor">
						<ul>
							<li>
								<p>学费水平会每年检讨。2019/20 学年学费水平会因应通胀及有关因素作调整。</p>
							</li>
							<li>
								<p>2019/20 学年学费将于VTC入学网页公佈</p>
							</li>				
							<li>
								<p>高级文凭课程的一般修读期为两年，每年学费分两期缴付。</p>
							</li>
							<li>
								<p>基础课程文凭及职专文凭课程的一般修读期为一年，全年学费分两期缴付。</p>
							</li>
						</ul>
						<p>
							<strong>2018 / 19 学年全日制资助课程学费以供参考</strong>
						</p>

						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>课程</strong>
									</th>
									<th>
										<strong>第一年</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
									<th>
										<strong>第二年</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
								</tr>
								<tr>
									<td>高级文凭 (资助课程) </td>
									<td>$31,570</td>
									<td>$31,570</td>
								</tr>
								<tr>
									<td>高级文凭 (自资课程)</td>
									<td>$56,600</td>
									<td>$56,600</td>
								</tr>
								<tr>
									<td>基础课程文凭/ 职专文凭  (资助课程)</td>
									<td>$20,500</td>
									<td>-</td>
								</tr>
								<tr>
									<td>基础课程文凭/ 职专文凭  (自资课程)</td>
									<td>$27,000</td>
									<td>-</td>
								</tr>
							</table>
						</div>
						<div class="txt-editor__notes">
							<p>
								<strong>
									<small>备注：</small>
								</strong>
							</p>
							<ol>
								<li>除学费外，学生须缴交其他费用如保证金、学生会年费及英文单元基准评核费。高级文凭学生需缴交英文单元研习教费。</li>
								<li>基础课程文凭学生如选择修读选修单元「基础数学（三）」，需另缴学费。</li>
								<li>职专文凭学生如选择修读选修单元「数学 3E : 升学选修单元」，或需另缴学费。</li>
								<li>为增强对学生的学习支援，学院或会要求部分学生修读衔接单元 / 增润课程；或需参加额外培训 / 实习 / 公开考试，缴付所需费用。</li>
							</ol>
						</div>

					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#00f7c1;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>申请程序</strong>
					</h2>
					<div class="txt-editor">
						<p>
							<!-- 2018/19 HKDI Higher Diploma starts to receive application from 24 November 2017. -->
							<br>申请网页：
							<a href="http://www.vtc.edu.hk/admission/tc/">
								<strong>www.vtc.edu.hk/admission</strong>
							</a>
						</p>
						<p>
							<strong>高级文凭一般入学条件</strong>
						</p>
						<ul>
							<li>香港中学文凭考试五科成绩达第二级或以上，包括英国语文及中国语文；或</li>
							<li>VTC 基础文凭（级别三）/ 基础课程文凭；或</li>
							<li>VTC 中专教育文凭 / 职专文凭；或</li>
							<li>毅进文凭；或</li>
							<li>同等学历</li>
						</ul>
					</div>
					<br />
					<p>更多详请，请<a href="http://www.vtc.edu.hk/admission/tc/" class="btn"><strong>按此</strong></a></p>
				</div>
			</div>

			<div class="article-detail">
				<div class="content-wrapper">
					<div class="txt-editor__notes">
						<p>
							<strong>备注*:</strong>
						</p>
						<ol>
							<li>高级文凭课程的一般修读期为 2 年。</li>
							<li>香港中学文凭考试应用学习科目（乙类科目）取得「达标」/「达标并表现优异（I）」/「达标并表现优异（II）」的成绩，于申请入学时会被视为等同香港中学文凭考试科目成绩达「第二级」/「第三级」/「第四级」；最多计算两科应用学习科目（应用学习中文除外）。学士学位课程只考虑相关应用学习科目的成绩。请参阅学士学位课程的特定入学条件。</li>
							<li>香港中学文凭考试其他语言科目（丙类科目）取得「D 或 E 级」/「C 级或以上」的成绩，于申请入学时会被视为等同香港中学文凭考试科目成绩达「第二级」/「第三级」；只计算一科其他语言科目。</li>
							<li>持中专教育文凭 / 职专文凭并完成指定升学单元的毕业生及毅进文凭毕业生，符合报读高级文凭课程的一般入学条件（设有特定入学条件的课程除外）。</li>
							<li>部分课程设有特定入学条件，详情请参阅个别课程的课程资料及入学条件。</li>
							<li>合资格的申请能否被取录需视乎申请人的学历、面试 / 测试表现（如适用）、其他学习经验及成就以及课程学额供求情况。</li>
						</ol>
						<br>
						<p>
							<strong>注意事项</strong></p>
						<ul>
							<li>就同一公开考试，申请人可以综合不同年度考获的成绩报读 VTC 课程。若申请人于同一科目多次考获成绩，会以最佳成绩考虑。</li>
							<li>新、旧学制之公开考试成绩一般不会获合併考虑。</li>
							<li>非华语申请人如符合特定情况*，可获个别考虑以下列其他语文科成绩或资历报读课程：</li>
						</ul>
						<br>
						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>其他语文科成绩</strong>
									</th>
									<th>
										<strong>学士学位课程</strong>
									</th>
									<th>
										<strong>高级文凭课程</strong>
									</th>
								</tr>
								<tr>
									<td>香港中学文凭考试应用学习中文</td>
									<td>达标</td>
									<td>达标</td>
								</tr>
								<tr>
									<td>GCSE / IGCSE / GCE O-level 中国语文科</td>
									<td>C 级</td>
									<td>D 级</td>
								</tr>
								<tr>
									<td>GCE AS-level 中国语文科</td>
									<td>C 级</td>
									<td>E 级</td>
								</tr>
								<tr>
									<td>GCE A-level 中国语文科</td>
									<td>E 级</td>
									<td>E 级</td>
								</tr>
							</table>
							<br>
							<p>
								<small>* 特定情况为 1) 申请人在接受中小学教育期间学习中国语文少于六年时间。这项安排专为较迟开始学习中国语文 (例如来港定居时早已过了入学阶段) 或间断地在香港接受教育的学生而设；或 2) 申请人在学校学习中国语文已有六年或以上时间，但期间是按一个经调适并较浅易的课程学习，而有关的课程一般并不适用于其他大部分在本地学校就读的学生。</small>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#f6f6f6;">
				<div class="content-wrapper">
					<div class="txt-editor">
						<h3 style="color:#00f7c1;">
							<strong>香港高级程度会考学历</strong>
						</h3>
						<p>持香港高级程度会考及香港中学会考学历的申请人可报读学士学位及高级文凭课程。请参阅有关个别课程的详细资料及入学条件（旧学制中七学生适用）。</p>
						<h3 style="color:#00f7c1;">
							<strong>其他本地、内地及非本地学历</strong>
						</h3>
						<p>如申请人持有其他本地、内地或非本地学历报读课程，其入学申请须经有关学系个别评核。</p>
					</div>
				</div>
			</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>