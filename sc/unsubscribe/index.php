<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '取消订阅';
$section = '取消订阅';
$subsection = '取消订阅';
$sub_nav = '取消订阅';

$breadcrumb_arr['取消订阅'] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title page-head__title--big">
						<h2>
							<strong>取消订阅</strong>
						</h2>
					</div>
				</div>
			</div>
		</div>
		<section class="unsubscribe-section">
			<form class="unsubscribe-form__form form-container" id="unsubscribe-form">
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
							<input type="text" name="subscription_name" placeholder="姓名" class="form-check--empty">
						</div>
					</div>
				</div>
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
							<input type="text" name="subscription_email" placeholder="电子邮件" class="form-check--empty form-check--email">
						</div>
					</div>
				</div>
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
							<div class="captcha">
								<div class="captcha__img">
									<img id="booking_captcha" src="https://hkdi3.bcde.digital//include/securimage/securimage_show.php" alt="CAPTCHA Image">
								</div>
								<div class="captcha__control">
									<div class="captcha__input">
										<input type="text" name="captcha" class="form-check--empty">
									</div>
									<a href="#" class="captcha__btn" onclick="document.getElementById('booking_captcha').src = 'https://hkdi3.bcde.digital//include/securimage/securimage_show.php?' + Math.random(); return false">重新加载图像</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="btn-row">
					<div class="err-msg-holder">
						<p class="err-msg err-msg--captcha">验证码不正确。</p>
						<p class="err-msg err-msg--general">有一个未知错误。 请稍后再试。</p>
					</div>
					<div class="btn-row btn-row--al-hl">
						<a href="#" class="btn btn-send">
							<strong>发送</strong>
						</a>
					</div>
				</div>
			</form>
			<div class="contact-form__success-msg" style="display: none;">
				<p class="desc-l">You have successfully unsubscribed.</p>
			</div>
		</section>
	</main>
	<?php //include $inc_lang_path . "inc_common/inc_form_err_msg.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

</html>
