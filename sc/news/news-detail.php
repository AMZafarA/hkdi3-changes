<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '最新动态';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$news_id = @$_REQUEST['news_id'] ?? '';
switch ($news_id) {
	case 554: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;
	case 551: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;
	case 550: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;
	case 537: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;

	case 564: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;
	case 565: header("Location: http://www.hkdi.edu.hk/inspire2018/"); break;
}
$news_type = @$_REQUEST['news_type'] ?? '';
$page = @$_REQUEST['page'] ?? '';
$news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_id = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)', " news_date DESC, news_id DESC", array(STATUS_ENABLE,$news_id,$today,$today));

if ($news_id == '' || $news === NULL)
{
	header("location: index.php");
	exit();
}

if ($news_type == '')
{
	$news_list = DM::findAll($DB_STATUS.'news', ' news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)', " news_date DESC, news_id DESC", array(STATUS_ENABLE,$today,$today));
	/*
	$prev_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_id <> ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_date <= ?', " news_date DESC", array(STATUS_ENABLE,$news_id,$today,$today,$news['news_date']));
	$next_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_id <> ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_date >= ?', " news_date", array(STATUS_ENABLE,$news_id,$today,$today,$news['news_date']));
	*/
}
else
{
	$news_list = DM::findAll($DB_STATUS.'news', ' news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_type = ?', " news_date DESC, news_id DESC", array(STATUS_ENABLE,$today,$today, $news_type));
	/*
	$prev_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_id <> ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_date <= ? AND news_type = ?', " news_date DESC", array(STATUS_ENABLE,$news_id,$today,$today,$news['news_date'],$news_type));
	$next_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_id <> ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_date >= ? AND news_type = ?', " news_date", array(STATUS_ENABLE,$news_id,$today,$today,$news['news_date'],$news_type));
	*/
	// combine hkdi news and Award
	if($news_type == 'student_award' || $news_type == 'hkdi_news'){
		$news_list = DM::findAll($DB_STATUS.'news', ' news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND (news_type = ? OR news_type = ?)', " news_date DESC, news_id DESC", array(STATUS_ENABLE,$today,$today, 'student_award', 'hkdi_news'));
		$news_type = 'hkdi_news';
	}
	// end of combine hkdi news and Award
}

for ($n = 0; $n < sizeof($news_list); $n++)
{
	if ($news_id == $news_list[$n]['news_id'])
	{
		$prev_news = @$news_list[$n+1];
		$next_news = @$news_list[$n-1];
		break;
	}
}


if ($news['news_lv2']) {
	$department = DM::load('department', $news['news_lv2']);
} else {
	$department = DM::load('department', $news['news_lv1']);
}

$breadcrumb_arr['最新动态'] =$host_name_with_lang.'news/index.php?tab='.$news_type.'&page='.$page;
$breadcrumb_arr[$news['news_name_'.$lang]] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<section class="article-detail">
				<div class="content-wrapper">
					<div class="article-detail__control">
						<!--<a href="index.php?tab=<?php echo $news_type; ?>&page=<?php echo $page; ?>" class="article-detail__btn-back">&lt;&nbsp;Back</a>-->
					</div>
					<div class="article-detail__detail-row">
						<div class="article-detail__detail">
							<div class="article-detail__head">
								<h2 class="article-detail__title">
									<strong><?php echo $news['news_name_'.$lang]; ?></strong>
								</h2>
								<div class="article-detail__info-items">
									<div class="article-detail__info-item"><?php echo date('d.m.Y', strtotime($news['news_date'])); ?></div>
									<div class="article-detail__info-item"><?php echo $department['department_name_'.$lang]; ?></div>
									<div class="article-detail__info-item"><?php echo $news_types_arr[$news['news_type']][$lang]?></div>
								</div>
							</div>
							<div class="txt-editor">
								<!--
								<h4>
									<strong>
										<span class="txt-highlight">The Centre of Innovative Material and Technology (CIMT) established by Hong Kong Design Institute (HKDI) was officially
											opened on 7 April, with the participation of several hundreds of industry partners, design academics and professionals.</span>
									</strong>
								</h4>
								<p>
									On the same day, a Master Talk by renowned DJ, stand-up comedian and singer-songwriter Jan Lamb, and a Street Furniture workshop
									by street art organisation Start from Zero were organised and well received.
									<br> Housing the latest innovative materials and design technologies sourced globally, CIMT has been designed and established
									to provide an interactive platform for the materials and design industries by facilitating the exchange of latest
									materials knowledge and inspiring new applications of materials. In celebration of the Grand Opening of CIMT, and
									more importantly, the 35th Anniversary of the VTC, a series of programmes are being rolled out for students and
									the industry including thematic seminars and workshops, and a themed exhibition featuring the latest technology
									and materials applications.
								</p>
								-->
								<?php echo $news['news_detail_'.$lang]; ?>
							</div>
						</div>
						<div class="article-detail__gallery">
							<!--
							<div class="article-detail__gallery-item">
								<img src="<?php echo $img_url?>news/img-news-detail-1.jpg" alt="" />
							</div>
							<div class="article-detail__gallery-item">
								<img src="<?php echo $img_url?>news/img-news-detail-2.jpg" alt="" />
							</div>
							-->
							<?php $img_array = array('news_image1', 'news_image2', 'news_image3', 'news_image4', 'news_image5'); ?>
							<?php foreach ($img_array AS $key => $val) { ?>
							<?php 	if ($news[$val] == '' || !file_exists($news_path.$news['news_id']."/".$news[$val])) { continue; } ?>
							<div class="article-detail__gallery-item">
								<img src="<?php echo $news_url.$news['news_id']."/".$news[$val]; ?>" alt="" />
							</div>
							<?php } ?>
						</div>
					</div>
					<hr>
					<div class="article-detail__control">
						<?php if ($prev_news !== NULL) { ?>
						<a href="news-detail.php?news_id=<?php echo $prev_news['news_id']; ?>&news_type=<?php echo $prev_news['news_type']; ?>" class="article-detail__btn-prev">
							<div class="article-detail__btn-txt">
								<div class="article-detail__btn-label">上一则</div>
								<h4 class="article-detail__btn-title"><?php echo $prev_news['news_name_'.$lang]; ?></h4>
							</div>
						</a>
						<?php } ?>
						<?php if ($next_news !== NULL) { ?>
						<a href="news-detail.php?news_id=<?php echo $next_news['news_id']; ?>&news_type=<?php echo $next_news['news_type']; ?>" class="article-detail__btn-next">
							<div class="article-detail__btn-txt">
								<div class="article-detail__btn-label">下一则</div>
								<h4 class="article-detail__btn-title"><?php echo $next_news['news_name_'.$lang]; ?></h4>
							</div>
						</a>
						<?php } ?>
					</div>
				</div>
			</section>
			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
