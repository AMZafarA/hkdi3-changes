<!-- Footer -->
<footer>
    <a href="#" class="footer__backtotop" title="Back to top"></a>
    <div class="content-wrapper">
        <div class="footer__top">
            <div class="footer__panel">
                <div class="footer__panel-item">
                    <!--<a href="#" class="footer_panel__subscription">
                        <h2 class="footer__panel-title">Subscription</h2>
                        <p class="footer__panel-tag">Subscribe our newsletter and <br>event information</p>
                    </a>-->
            <h2 class="footer__panel-title">地址</h2>
                    <p>香港新界将军澳景岭路3号</p>
                </div>
                <div class="footer__panel-item">
                    <h2 class="footer__panel-title"></h2>
                    <div class="btn-share__holder">
                        <!--<a href="" class="btn-share btn-share--email"></a>-->
                        <!--<a href="" class="btn-share btn-share--wa"></a>-->
                        <a href="http://www.facebook.com/HKDI.HongKongDesignInstitute" class="btn-share btn-share--fb" target="_blank"></a>
                        <a href="https://instagram.com/hkdi_hongkongdesigninstitute" class="btn-share btn-share--ig" target="_blank"></a>
                        <a href="http://www.youtube.com/user/hkdichannel" class="btn-share btn-share--youtube" target="_blank"></a>
                        <!--<a href="http://www.twitter.com/thehkdi" class="btn-share btn-share--tw" target="_blank"></a>-->
                        <a href="https://www.linkedin.com/school/hong-kong-design-institute/?originalSubdomain=hk" class="btn-share btn-share--ln" target="_blank"></a>
                        <!--<a href="" class="btn-share btn-share--ln"></a>-->
                        <!--<a href="http://weibo.com/thehkdi" class="btn-share btn-share--wb" target="_blank"></a>-->
                    </div>
                </div>
            </div>
            <div class="footer__sitemap">
                <div class="footer__sitemap-grp footer__sitemap-grp--primary">
                    <a href="<?php echo $host_name_with_lang?>about" class="footer__sitemap-link">关于我们</a>
                    <a href="<?php echo $host_name_with_lang?>programmes" class="footer__sitemap-link">设计课程</a>
                    <a href="<?php echo $host_name_with_lang?>knowledge-centre" class="footer__sitemap-link">知识资源中心</a>
                    <!--<a href="http://www.hkdi.edu.hk/hkdi_gallery/index.php" class="footer__sitemap-link">HKDI Gallery</a>-->
            <a target="_blank" href="http://www.hkdi.edu.hk/hkdi_gallery/index.php" class="footer__sitemap-link">HKDI画廊</a>
                    <!--<a href="<?php echo $host_name_with_lang?>edt" class="footer__sitemap-link">Emerging Design Talents</a>-->
                    <a href="<?php echo $host_name_with_lang?>programmes/award.php?id=7" class="footer__sitemap-link">学生得奖作品</a>
                    <a href="<?php echo $host_name_with_lang?>global-learning" class="footer__sitemap-link">国际交流</a>
                    <!-- <a href="<?php echo $host_name_with_lang?>industrial-collaborations" class="footer__sitemap-link">业界协办项目</a> -->
                    <a href="<?php echo $host_name_with_lang?>news" class="footer__sitemap-link">最新动态</a>
                </div>
                <div class="footer__sitemap-grp">
                    <a href="<?php echo $host_name_with_lang?>contact" class="footer__sitemap-link">联络我们</a>
                    <a href="https://www.facebook.com/VTCDAA/" class="footer__sitemap-link">VTCDAA</a>
                    <a href="http://dilwl-radio708.vtc.edu.hk/" class="footer__sitemap-link">708电台</a>
                    <a href="<?php echo $host_name_with_lang?>job-openings" class="footer__sitemap-link">职位空缺</a>
                    <a href="<?php echo $host_name_with_lang?>friendly-links" class="footer__sitemap-link">友情链接</a>
                    <a href="http://www.vtc.edu.hk/admission/sc/programme/s6/higher-diploma/#design" class="footer__sitemap-link">入学申请</a>
                    <a href="<?php echo $host_name_with_lang?>disclaimer" class="footer__sitemap-link">免责声明</a>
                </div>
                <div class="footer__sitemap-grp">
                    <p><strong>请输入以下邮件地址订阅我们的新闻动态</strong></p>
                    <form id="subscribe-form" class="subscribe-form__form form-container">
                        <div class="field-row">
                            <div class="field field--1-1 field--mb-1-1">
                                <div class="field__holder">
                                    <input type="text" name="subscription_name" placeholder="姓名" class="form-check--empty">
                                </div>
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="field field--1-1 field--mb-1-1">
                                <div class="field__holder">
                                    <input type="text" name="subscription_email" placeholder="电子邮件" class="form-check--empty form-check--email">
                                </div>
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="field field--1-1 field--mb-1-1">
                                <div class="field__holder">
                                    <div class="captcha">
                                        <div class="captcha__img">
                                            <img id="booking_captcha_s" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                        </div>
                                        <div class="captcha__control">
                                            <div class="captcha__input">
                                                <input type="text" name="captcha" class="form-check--empty" />
                                            </div>
                                            <a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha_s').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">重新加载图像</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-row">
                            <div class="err-msg-holder">
                                <p class="err-msg err-msg--captcha">验证码不正确。</p>
                                <p class="err-msg err-msg--general">有一个未知错误。 请稍后再试。</p>
                            </div>
                            <div class="btn-row btn-row--al-hl">
                                <a href="#" class="btn btn-send">
                                    <strong>发送</strong>
                                </a>
                            </div>
                        </div>
                    </form>
                    <div class="subscribe-form__success-msg" style="display: none;">
                        <h3 class="desc-subtitle">Thank you!</h3>
                        <p class="desc-l">You have successfully subscribed.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__bottom-items">
                <div class="footer__bottom-item">&copy; <?php echo date("Y") ?> 香港知专设计学院。版权所有。</div>
        </div>
    </div>
</footer>
<!-- END Footer -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116403822-1');
</script>
