<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "International";
$section = 'international';
$subsection = 'international';
$sub_nav = 'international';

$breadcrumb_arr['Global Learning'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$exchange_programme_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'P'));
$incoming_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'I'));
$outgoing_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'O'));
$scholarship_list = DM::findAll($DB_STATUS.'international','international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'S'));

//$scholarship_list = DM::findAll($DB_STATUS.'international_image', 'international_image_status = ? AND international_image_pid = ?', 'international_image_seq', array(STATUS_ENABLE, '1'));

$master_lecture_list = DM::findAll($DB_STATUS.'master_lecture', 'master_lecture_status = ?', 'master_lecture_seq', array(STATUS_ENABLE));
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/international.css" type="text/css" />

        <script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
        <style>
        .programme-project .hidden-block h3:after{
            display:none;
        }
        </style>
    </head>

    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access">
            <?php echo $page_title?>
        </h1>
        <main>
            <div class="top-divide-line"></div>
            <?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            <div class="<?php echo $subsection ?>">
                <section class="international-info">
                    <h2>Global Learning</h2>
                    <h4>培育具国际视野的设计师</h4>
                    <p>HKDI 致力培育学生的国际视野及文化触觉，我们全球知名的<a class="underline-link">设计学院</a>合作，并提供<a href="javascript:scrollToElemTop($('#scholarship'), 300);" class="underline-link">学术交流奖学金</a>。让学生前往海外大学进行一个学期的学术交流,藉此扩阔视野。</u></a>.
                    </p>
                </section>
                <section id="international-student-exchange-programme" class="gallery-exhib">

                <section class="international-info" style="padding-top:0;z-index: 30;">
                                    <h2 style="margin:0 0 30px;">
                                    Sharing from
                        <span>来校交流</span>
                    </h2>
                    <p>如您就读的院校已签有<a class="underline-link">合作谅解备忘录和/或交流协议</a>，欢迎申请到HKDI 进行一个学期的学术交流。申请前，请先了解有关学分及非学分交流计划的详情。</p>
                </section>
                    <div class="video-bg">
                        <div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
                        </div>
                    </div>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">          
                                    <?php foreach ($exchange_programme_list AS $exchange_programme) { ?>
                    <div class="card is-collapsed">
                                        <div class="card__inner ani">
                                            <div class="card__img">
                            <?php if ($exchange_programme['international_image'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_image'])) { ?>
                        <img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_image']; ?>" alt="" />
                        <?php } ?>
                                            </div>
                                            <div class="card__ppl">
                                              <?php if ($exchange_programme['international_thumb'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb'])) { ?>
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb']; ?>" alt="" />
                                                </div>
                                            <?php } ?>
                        
                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $exchange_programme['international_name_'.$lang]; ?></strong>
                                                    <!--<p class="card__ppl-title"></p>-->
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $exchange_programme['international_desc_'.$lang]; ?></p>
                                                <a href="exchange-programme-details.php?international_id=<?php echo $exchange_programme['international_id']; ?>" class="btn">More</a>
                                            </div>
                                        </div>
                                    </div>
                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        <?php /*
                <section id="sharing-from-incoming-students" class="gallery-exhib">
                    <h2>Sharing from
                        <span>Incoming Students:</span>
                    </h2>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($incoming_student_list AS $incoming_student) { ?>
                                    <div class="card is-collapsed">
                                        <div class="card__inner ani">
                                            <div class="card__img">
                                                <?php if ($incoming_student['international_image'] != '' && file_exists($international_path.$incoming_student['international_id'].'/'.$incoming_student['international_image'])) { ?>
                        <img src="<?php echo $international_url.$incoming_student['international_id'].'/'.$incoming_student['international_image']; ?>" alt="" />
                        <?php } ?>
                                            </div>
                                            <div class="card__ppl">
                            <!--
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $img_url?>programme/alumni/img-programme-alumni-propic-1.jpg" alt="" />
                                                </div>
                        -->
                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $incoming_student['international_name_'.$lang]; ?></strong>
                                                    <p class="card__ppl-title"><?php echo $incoming_student['international_job_title_'.$lang]; ?></p>
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $incoming_student['international_desc_'.$lang]; ?></p>
                                                <a href="incoming-student-details.php?international_id=<?php echo $incoming_student['international_id']; ?>" class="btn">More</a>
                                            </div>
                                        </div>
                                    </div>
                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        */ ?>
                <section id="sharing-from-outgoing-students" class="gallery-exhib">

                <section class="international-info" style="padding-top:0;">
                                    <h2 style="margin:0 0 30px;">Sharing from
                        <span>外访交流</span>
                    </h2>

                    
                    <p>有意参加外访交流计划的全日制 HKDI 学生，详情请参阅申请文件及<a href="javascript:scrollToElemTop($('#scholarship'), 300);" class="underline-link">海外交流奖学金</a>。</p>
                </section>

                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($outgoing_student_list AS $outgoing_student) { ?>
                                    <div class="card is-collapsed">
                                        <div class="card__inner ani">
                                            <div class="card__img">
                                                <?php if ($outgoing_student['international_image'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_image'])) { ?>
                        <img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_image']; ?>" alt="" />
                        <?php } ?>
                                            </div>
                                            <div class="card__ppl">
                            <?php if ($outgoing_student['international_thumb'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb'])) { ?>
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb']; ?>" alt="" />
                                                </div>
                                            <?php } ?>
                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $outgoing_student['international_name_'.$lang]; ?></strong>
                                                    <p class="card__ppl-title"><?php echo $outgoing_student['international_job_title_'.$lang]; ?></p>
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $outgoing_student['international_desc_'.$lang]; ?></p>
                                                <a href="outgoing-student-details.php?international_id=<?php echo $outgoing_student['international_id']; ?>" class="btn">More</a>
                                            </div>
                                        </div>
                                    </div>
                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="scholarship" class="scholarship">
           <div class="inner">
                    <h2>奖学金 及捐赠</h2>
                    <div class="inner_content">
                    <h3>赞助学术交流</h3>
                    <p>HKDI欢迎社会各界人士赞助成绩优异及有经济需要的学生前往海外进行学术交流。</p>
                    </div>
                    
                        <section class="programme-project ani" style="width:100%; margin-bottom:30px;">
                            <div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
                                <h3 style="margin:0;color:#FFF;padding: 0;font-size:16px;">+ 详情</h3>
                                <div class="hidden">
                                    <p>您的捐赠将用作支持学生海外交流所需，包括交通、住宿及一切与学习有关之开支。捐赠港币十万元，即可帮助一位学生获取全额奖学金，赴海外大学参加一个学期的学术交流。鸣谢类别：（港币100元或以上之捐款可申请扣税）</p>
                                    <p>捐款者可选择以个人或机构名义，命名其赞助之海外交流奬学金， HKDI亦会于学院官方网页、印刷品、其它宣传媒介及每年一度之奖助学金颁奖典礼，鸣谢奖学金捐赠者及捐赠机构。</p>
                                    <p>捐款者亦可指定支持与个人或机构使命相关的学术范畴，或对受惠学生回馈捐赠机构的事宜提出建议。</p>
                                    <div><a href="mailto:nikishek@vtc.edu.hk" class="btn btn--white"><strong>查询</strong></a></div>
                                </div>
                            </div>
                        </section>
                    
                    <div class="inner_content">
                    <h3>奖学金</h3>
                    <p>亚设贝佳国际有限公司为综合房地产开发谘询和建筑工程专业服务公司，其奖学金旨在鼓励主修建筑环境设计的年轻学生为个人生活带来正向影响，并为香港社会作出积极贡献。</p>
                    </div>
            <?php /*foreach ($scholarship_list AS $scholarship) { ?>
                    <div class="scholarship-block">
                        <!--<img src="<?php echo $international_url.'1/'.$scholarship['international_image_id'].'/'.$scholarship['international_image_filename']; ?>" />-->
                        <div class="txt">
                            <p><?php echo $scholarship['international_image_donor_'.$lang]?></p>
                            <p><strong><?php echo $scholarship['international_image_desc_'.$lang]; ?></strong></p>
                        </div>
                    </div>
            <?php } */?>
            <?php foreach ($scholarship_list AS $scholarship) { ?>
                    <div class="scholarship-block">
                        <div class="txt">
                            <p><strong><?php echo $scholarship['international_name_'.$lang]?></strong></p>
                            <p><?php echo nl2br($scholarship['international_desc_'.$lang]); ?></p>
                        </div>
                    </div>
            <?php } ?>

            <!--
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo02.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo03.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo04.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo01.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo02.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
            -->
            </div>
                </section>
                <section id="master-lecture-series" class="gallery-exhib profile">
                    <h2>大师讲座
                        <span>系列</span>
                    </h2>
                <section class="international-info">
                    <p>HKDI 定期邀请来自世界各地的设计大师、学者和专业人士，与学生分享创意经验和见解。</p>
                </section>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--4-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($master_lecture_list AS $master_lecture) { ?>
                                    <div class="card is-collapsed">
                                        <div class="card__inner">
                                            <div class="card__content">
                                                <a href="#" class="btn js-expander">
                                                    <img src="<?php echo $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image']; ?>" alt="" />
                                                    <h4><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h4>
                                                    <p><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card__expander">
                                            <div class="profile-block">
                                                <div class="profile-block__detail">
                                                    <div class="img-holder">
                                                        <img src="<?php echo $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image']; ?>" alt="" />
                                                    </div>
                                                    <div class="text">
                                                        <h3 class="profile-block__name"><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h3>
                                                        <p class="profile-block__title"><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
                                                        <div class="profile-block__desc">
                                                            <p><?php echo $master_lecture['master_lecture_detail_'.$lang]; ?></p>
                                                        </div>
                                                    </div>
                                                    <!--<ul class="profile-block__list bullet-list">
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                    </ul>-->
                                                </div>
                        <?php $master_lecture_images = DM::findAll($DB_STATUS.'master_lecture_image', 'master_lecture_image_status = ? AND master_lecture_image_pid = ?', 'master_lecture_image_seq', array(STATUS_ENABLE, $master_lecture['master_lecture_id'])); ?>
                                                <div class="profile-block__gallery">
                                                    <div class="profile-block__gallery-slider swiper-container-fade swiper-container-horizontal swiper-container-free-mode swiper-container-ios">
                                                        <div class="swiper-wrapper" style="transition-duration: 0ms;">
                            <?php /*
                                                            <div class="profile-block__slide swiper-slide swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next"
                                                                data-swiper-slide-index="1" style="width: 361px; transition-duration: 0ms; opacity: 0; transform: translate3d(0px, 0px, 0px);">
                                                                <img src="../../images/programme/alumni/img-programme-alumni-gallery-demo-2.jpg" alt="">
                                                            </div>
                                                            <div class="profile-block__slide swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 361px; transition-duration: 0ms; opacity: 1; transform: translate3d(-361px, 0px, 0px);">
                                                                <img src="../../images/programme/alumni/img-programme-alumni-gallery-demo-1.jpg" alt="">
                                                            </div>                              
                            */ ?>
                                <?php foreach ($master_lecture_images AS $master_lecture_image) { ?>
                                <div class="profile-block__slide swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 361px; transition-duration: 0ms; opacity: 1; transform: translate3d(-361px, 0px, 0px);">
                                    <img src="<?php echo $master_lecture_url.$master_lecture_image['master_lecture_image_pid'].'/'.$master_lecture_image['master_lecture_image_id'].'/'.$master_lecture_image['master_lecture_image_filename']; ?>" alt="<?php echo $master_lecture_image['master_lecture_image_alt']; ?>" />
                                </div>
                                <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div id="fp-nav" class="custom-sidenav right">
                <ul>
                    <li>
                        <a href="#international-student-exchange-programme">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Sharing from Incoming Students</div>
                    </li>
                    <li>
                        <a href="#sharing-from-outgoing-students">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Sharing from Outgoing Students:</div>
                    </li>
                    <li>
                        <a href="#scholarship">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Scholarship</div>
                    </li>
                    <li>
                        <a href="#master-lecture-series">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Master Lecture Series</div>
                    </li>
                </ul>
            </div>
        </main>
        <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>

    </html>