<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "International";
$section = 'international';
$subsection = 'international-details';
$sub_nav = 'international-details';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$international_id = $_REQUEST['international_id'] ?? '';
$international_type = $_REQUEST['international_type'] ?? '';
$international = DM::findOne($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND international_id = ?', '', array(STATUS_ENABLE, $international_type, $international_id));
$international_images = DM::findAll($DB_STATUS.'international_image', 'international_image_status = ? AND international_image_pid = ?', 'international_image_seq', array(STATUS_ENABLE, $international_id));
$prev_international = DM::findOne($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND international_id <> ? AND international_seq <= ?', 'international_seq DESC', array(STATUS_ENABLE, $international_type, $international_id, $international['international_seq']));
if (!$prev_international)
{
    $prev_international = DM::findOne($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND international_id <> ?', 'international_seq DESC', array(STATUS_ENABLE, $international_type, $international_id));
}
$next_international = DM::findOne($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND international_id <> ? AND international_seq >= ?', 'international_seq', array(STATUS_ENABLE, $international_type, $international_id, $international['international_seq']));
if (!$next_international)
{
    $next_international = DM::findOne($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND international_id <> ?', 'international_seq', array(STATUS_ENABLE, $international_type, $international_id));
}

$breadcrumb_arr['Global Learning'] =$host_name_with_lang.'international/';
if ($international_type == 'O')
{
    $breadcrumb_arr['Outgoing Students'] = '';
}
else if ($international_type == 'I')
{
    $breadcrumb_arr['Incoming Students'] = '';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/international-details.css" type="text/css" />
        <script>
            $(document).ready(function () {
                var mySwiper = new Swiper ('.swiper-container', {
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                    },
                    paginationClickable: true,
                    autoHeight: true
                });
            });
        </script>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
            <?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            <!--
                <div class="top-nav">
                    <div class="top-nav-left"><a class="back" href="index.php">Back</a></div>
                    <div class="top-nav-center"><h4>Outgoing Exchange</h4></div>
                    <div class="top-nav-right">&nbsp;</div>
                </div>
                -->
                <div class="<?php echo $subsection ?>">
                <section class="international-details">
                    <h3><?php echo $international['international_name_'.$lang]; ?></h3>
            <h4><?php echo $international['international_desc_'.$lang]; ?></h4>

            <?php if ($international['international_thumb'] != '' && file_exists($international_path.$international['international_id'].'/'.$international['international_thumb'])) { ?>
                                                    <p><img src="<?php echo $international_url.$international['international_id'].'/'.$international['international_thumb']; ?>" alt="" /><p>
                                            <?php } ?>
                                            
            <?php echo $international['international_detail_'.$lang]; ?>
                </section>
                <section class="visual-holder">
            <?php if ($international['international_image1'] != '' && file_exists($international_path.$international['international_id'].'/'.$international['international_image1'])) { ?>
            <img src="<?php echo $international_url.$international['international_id'].'/'.$international['international_image1']; ?>" alt="" />
            <?php } ?>
            <?php if ($international['international_image2'] != '' && file_exists($international_path.$international['international_id'].'/'.$international['international_image2'])) { ?>
            <img src="<?php echo $international_url.$international['international_id'].'/'.$international['international_image2']; ?>" alt="" />
            <?php } ?>
        </section>
                
        <?php if (sizeof($international_images) > 0) { ?>
                <section class="photos">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                    <?php $count = 1; ?>
                    <?php $total_count = sizeof($international_images); ?>
                    <?php foreach ($international_images AS $international_image) { ?>
                <?php if ($count % 8 == 1) { ?>
                <div class="swiper-slide col">
                <?php } ?>
                    <a href="<?php echo $international_url.$international_image['international_image_pid'].'/'.$international_image['international_image_id'].'/'.$international_image['international_image_filename']; ?>"><img src="<?php echo $international_url.$international_image['international_image_pid'].'/'.$international_image['international_image_id'].'/'.'crop_'.$international_image['international_image_filename']; ?>" /></a>
                <?php if ($count % 8 == 7 || $count == ($total_count - 1)) { ?>
                </div>
                <?php } ?>
                <?php $count++; ?>
                <?php } ?>
                            </div>                            
                            <div class="swiper-pagination"></div>
                        </div>
                </section>
        <?php } ?>
                <div class="bottom-nav">
            <?php if ($prev_international) { ?>
                    <div class="bottom-nav-left"><a class="prev-page" href="?international_id=<?php echo $prev_international['international_id']; ?>"><p>Previous Article</p><h4><?php echo $prev_international['international_name_'.$lang]; ?></h4></a></div>
            <?php } ?>
            <?php if ($next_international) { ?>
                    <div class="bottom-nav-right"><a class="next-page" href="?international_id=<?php echo $next_international['international_id']; ?>"><p>Next Article</p><h4><?php echo $next_international['international_name_'.$lang]; ?></h4></a></div>
            <?php } ?>
                </div>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
