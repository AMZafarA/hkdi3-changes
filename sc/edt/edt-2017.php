<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'EMERGINF DESIGN TALENTS 2017';
$section = 'edt';
$subsection = 'edt-details';
$sub_nav = 'edt-details';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$product_id = 27;

$product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_id = ?', '', array(STATUS_ENABLE, $product_id));
$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, $product_id));

$lat = '22.3057314';
$lng = '114.2512759';
foreach ($product_sections AS $product_section)
{
    if ($product_section['product_section_type'] == 'Visit')
    {
        $lat = $product_section['product_lat'];
        $lng = $product_section['product_lng'];
    }
}

function GenTitle($title){
    $title_arr = explode(" ", $title);
    $count_arr = count($title_arr);

    $h2_title = str_replace($title_arr[$count_arr-1],"",$title);

    if($count_arr ==1){
        return "<h2>".$title."</h2>";
    }else{
        return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
    }

}

$product_background_color = $product['product_background_color'];
$product_text_color = $product['product_text_color'];
if($product_text_color==''){
$product_text_color='#000';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/edt-details.css" type="text/css" />
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN""></script>
        <script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
        <script>
            $(document).ready(function () {
                initMap();
                var mySwiper = new Swiper ('.swiper-container', {
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                    },
                    paginationClickable: true,
                    autoHeight: true
                });
				initColorHightLight();
				//initEdtVideo();
            });
			function initColorHightLight(){
			 var $colorHighlight = $('.edt-details .color-highlight');
			 if(!$colorHighlight.children('span').length){
			 	$colorHighlight.wrapInner( "<span></span>");
			 }
			}
			function initEdtVideo(){
			 	var $vidIframe = $('.edt-details .about iframe');
			 	$vidIframe.wrap( "<div class='video'><div></div></div>" );
			}
            function initMap() {
                var stylez = [{
                    featureType: "all",
                    elementType: "all",
                    stylers: [
                        { saturation: -100 }
                    ]
                }];

                var imageTag = '<?php echo $host_name?>images/common/locator.png';
                var tag = new google.maps.MarkerImage(
                    imageTag,
                    null, /* size is determined at runtime */
                    null, /* origin is 0,0 */
                    null, /* anchor is bottom center of the scaled image */
                    new google.maps.Size(50, 73)
                    );

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                    center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),

                    styles: stylez
                });
    
                var marker;
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
                    map: map,
                    icon: tag
                });
            }
        </script>
		<style>
		
		.edt-details .inner-top-banner .sub-menu{
				background-color: <?php echo $product_background_color ?>;
		}
		.edt-details .inner-top-banner .sub-menu a{
				color: <?php echo $product_text_color ?>;
		}
		.edt-details .zoning-map-info .hidden-block h3.active,
		.edt-details .access-map .access-info h2,
		.edt-details .about h2 span{
				color: <?php echo $product_background_color ?>;
		}
		.edt-details .zoning-map-info table tr td{
				border-color: <?php echo $product_background_color ?>;
				color: <?php echo $product_background_color ?>;
		}
		.edt-details .photos .col a:nth-child(3):after,
        .edt-details .photos .col a:nth-child(5):before{
                background-color:<?php echo $product_background_color?>;
        }
		</style>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            <div class="top-divide-line"></div>
            <div class="<?php echo $subsection ?>">
                <div class="top-nav">
                    <!--<div class="top-nav-left"><a class="back" href="#">Back</a></div>-->
                    <div class="top-nav-left"></div>
                    <div class="top-nav-center"><h4><?php echo $product['product_name_'.$lang]?></h4></div>
                    <div class="top-nav-right">&nbsp;</div>
                </div>
                <section class="inner-top-banner">
                    <div class="banner-visual"><img src="<?php echo $product_url.$product['product_id']."/".$product['product_banner'] ?>" /></div>
                    <div class="sub-menu">
                        <div>
                        	<?php foreach ($product_sections AS $product_section) { ?>
                <?php if ($product_section['product_section_type'] == 'CT' || $product_section['product_section_type'] == 'Acknowledgement') { continue; } ?>
                <a href="#section-<?php echo $product_section['product_section_id']; ?>"><?php echo $product_section['product_section_name_'.$lang]; ?></a>
                <?php } ?>
                        </div>
                    </div>
                </section>

                <?php for ($i = 0; $i < sizeof($product_sections); $i++) { ?>
                <?php $product_section = $product_sections[$i]; ?>
                <?php if ($product_section['product_section_type'] == 'T') { ?>
                <section class="about section-<?php echo $product_section['product_section_id']; ?>" style="background-color: <?php echo $product_section['product_section_background_color']?>;">
                    <?php echo GenTitle($product_section['product_section_title_'.$lang]);?>
                    <?php echo $product_section['product_section_content_'.$lang]; ?>
                </section>
                <?php generateBlock($product_sections, $i); ?>

                <?php } else if ($product_section['product_section_type'] == 'Photo') { ?>
                <?php $photos = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $product_section['product_section_id'])); ?>
        <section class="photos section-<?php echo $product_section['product_section_id']; ?>">
                    <h2><?php echo $product_section['product_section_title_'.$lang]; ?></h2>
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                    <?php $photo_count = 0; ?>
                    <?php foreach ($photos AS $photo) { ?>
                    <?php if ($photo_count % 8 == 0) { ?>
                    <div class="swiper-slide col">
                    <?php } ?>
                    <a href="<?php echo $product_file_url.$photo['product_section_file_pid'].'/'.$photo['product_section_file_id'].'/'.$photo['product_section_file_filename']?>"><img src="<?php echo $product_file_url.$photo['product_section_file_pid'].'/'.$photo['product_section_file_id'].'/crop_'.$photo['product_section_file_filename']; ?>" /></a>
                    <?php if ($photo_count % 8 == 7) { ?>
                    </div>
                    <?php } ?>
                    <?php $photo_count++; ?>
                    <?php } ?>
                    </div>
                            
                        </div>
                            <div class="swiper-pagination"></div>
                </section>
        <?php generateBlock($product_sections, $i); ?>
        <?php } else if ($product_section['product_section_type'] == 'Visit') { ?>
        <section class="access-map section-<?php echo $product_section['product_section_id']; ?>">
                    <div id="map" class="map"></div>
            <div class="access-info">
                <h2><?php echo $product_section['product_section_title_'.$lang]; ?></h2>
                <?php echo $product_section['product_section_content_'.$lang]; ?>
            </div>
                </section>
        <?php generateBlock($product_sections, $i); ?>
        <?php } ?>
        <?php } ?>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>


<?php
function generateBlock($product_sections, $loopI)
{
    global $lang, $product_file_url;
    for ($i = ($loopI + 1); $i < sizeof($product_sections); $i++) {
        $product_section = $product_sections[$i];
        if ($product_section['product_section_type'] == 'Acknowledgement') {
            $product_section_files = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $product_section['product_section_id']));
?>
                    <div class="block">
                            <p><?php echo $product_section['product_section_title_'.$lang]; ?></p>
                <?php foreach ($product_section_files AS $product_section_file) { ?>
                            <img src="<?php echo $product_file_url.$product_section_file['product_section_file_pid'].'/'.$product_section_file['product_section_file_id'].'/'.$product_section_file['product_section_file_filename']; ?>" />
                <?php } ?>
                    </div>
<?php } else if ($product_section['product_section_type'] == 'CT') { ?>
            <div class="hidden-block">
                <h3><?php echo $product_section['product_section_title_'.$lang]; ?></h3>
                <div class="hidden">
                    <?php echo $product_section['product_section_content_'.$lang]; ?>           
                </div>
            </div>
<?php } else { 
            break;
        }
    }
}
?>