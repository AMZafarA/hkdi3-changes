﻿<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '关于';
$section = 'about';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$breadcrumb_arr['关于我们'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
       		<link rel="stylesheet" href="<?php echo $host_name?>css/industrial-collaborations-details.css" type="text/css" />
		<script src="<?php echo $host_name?>js/about.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access">
			<?php echo $page_title?>
		</h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>关于
								<strong>HKDI</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs page-tabs--no-bottom-pad">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-about-us" class="page-tabs__btn is-active">
							<span>关于</span>
						</a>
						<a href="#tabs-about-campus" class="page-tabs__btn" id="btn-tabs-about-campus">
							<span>关于校园</span>
						</a>
						<a href="#tabs-publication" class="page-tabs__btn" id="btn-tabs-publication">
							<span>刊物</span>
						</a>
						<a href="#tabs-experience" class="page-tabs__btn" id="btn-tabs-experience">
							<span>体验设计</span>
						</a>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__bg-deco"></div>
					<div class="page-tabs__wrapper">
						<div id="tabs-about-us" class="page-tabs__content is-active">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<!--div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>关于</strong>HKDI</h2>
										</div-->
										<div class="txt-editor">
											<h4>香港知专设计学院 (HKDI) 为职业训练局辖下学院，致力提供优质教育，建构知识和发展专业，为香港创意工业培育优秀的设计人才。</h4>
											<p>HKDI 拥有多年的设计教育经验，提供共 20 个设计课程，科目涵盖四主个主要设计学系，并将其优势融合，包括建筑、室内及产品设计、传意设计及数码媒体、基础设计、以及时装及形象设计。配合灵活升学及高等教育需求，HKDI 学生于修毕两年制的高级文凭后，可直接升读由英国着名大学颁授的一年制学士学位课程。</p>
											<p>HKDI 採取「思考与实践」的教育方针，开办与时并进的课程，并积极与国内外的设计学院及业界保持紧密合作。我们为学生提供获得实践经验及参与国际交流计划的机会，以扩阔学生视野，加强学生的创新思维和社会敏感度。</p>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="about-campus">
													<strong>得奖建筑 &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="publication">
													<strong>HKDI 刊物 &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="experience">
													<strong>体验设计 &gt;</strong>
												</a>
											</div>
											<!--<div class="btn-row btn-row--al-hl">
												<a href="../edt/edt-info-day.php" class="btn">
													<strong>HKDI资料日 &gt;</strong>
												</a>
											</div>-->
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/about_image.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<div class="industrial-collaborations-details">
								<section class="membership">
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											<strong>专业学会及机构</strong></h2>
									</div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											专业学会：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ico-D_120x120.png" />
						                        <div class="txt">
						                            <p><strong>ICO-D</strong></p>
						                            <p>国际设计社团组织</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IDA_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IDA</strong></p>
						                            <p>国际设计联盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IFFTI_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFFTI </strong></p>
						                            <p>国际服装技术学院基金会</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ifi_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFI</strong></p>
						                            <p>国际室内建筑师设计师团体联盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IIID_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IIID</strong></p>
						                            <p>国际资讯设计研究所</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>TDAA</strong></p>
						                            <p>亚洲设计连</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/WDO_120x120.png" />
						                        <div class="txt">
						                            <p><strong>WDO</strong></p>
						                            <p>世界设计组织</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											学术机构：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/Cumulus_120x120.png" />
						                        <div class="txt">
						                            <p><strong>CUMULUS</strong></p>
						                            <p>国际大学协会及艺术，设计与媒体学院</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											合作机构：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/DESIS_120x120.png" />
						                        <div class="txt">
						                            <p><strong>DESIS Network</strong></p>
						                            <p>社会创新与可持续设计国际联盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>Design ED Asia</strong></p>
						                            <p>设计教育亚洲会议</p>
						                        </div>
						                    </div>
								</section>
							</div>
							<section class="sec-basic sec-basic--white">
								<div class="page-tabs__bg"></div>
								<div class="txt-editor">
									<h3 class="txt-editor__title-light">
										<strong>执行干事序言</strong></h3>
									<h4>
										<strong>我谨代表香港知专设计学院衷心欢迎您！</strong>
									</h4>
									<div class="txt-editor__grp">
										<h4>
											<strong>探索机会‧成功在望</strong>
										</h4>
										<p>香港知专设计学院透过全面和富于创意的设计课程，为学生提供一个探索设计艺术，发挥自我潜能的学习机会。</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>培养人才‧成就未来</strong>
										</h4>
										<p>学院致力培养优秀的设计人才，我们的导师充满热诚，具丰富经验，为大家提供一个全方位，又富创意的互动学习环境。我们致力开拓学生的想像力、创造力、沟通技巧和自信心，亦十分注重课程与业界的衔接。课程得到业界的参与及合作，使学生所学所识更切合行业的实际需求。课程的质素保证得到香港学术评审局认可。</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>升学就业‧成功之道</strong>
										</h4>
										<p>僱主高度肯定我们的课程。我们的毕业生在不同行业均取得一定成就。同学更可于毕业后报读本校与海外大学学位衔接课程，在一至两年内完成学业，达成继续升学的理想，开创未来的事业。</p>
									</div>
									<div class="txt-editor__grp">
										<p>我诚意邀请您加入香港知专设计学院，为您未来的成功事业而努力。</p>
									</div>
									<br>
									<div class="txt-editor__grp">
										<p>
											职业训练局执行干事</br>
											尤曾家丽女士, GBS, JP
										</p>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-about-campus" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>设计</strong>概念</h2>
										</div>
										<div class="txt-editor">
											<h4>香港知专设计学院的校舍致力营造一个有利于学习和创新探索的开放且有动感的环境。学院设有先进的工作坊、製作设施及学习资源中心，为高质素的设计教育提供优良配备。</h4>
											<p>校舍的设计由法国高地菲及伙伴建筑公司 (Coldéfy & Associés) 完成，主题是「一张白纸」，于 2006/2007 年度香港知专设计学院国际建筑设计比赛中夺得冠军。</p>
											<p>这张「白纸」是创造力的隐喻表达。它在空中连繫院校所有其他用作教学用途的大楼，展示出 HKDI 跨部门协作的特点。创新、明亮和通透的建筑设计，将多种不同的场所融合在一起，并且可让人从外部轻鬆辨别各个功能单元。</p>
											<p>整幢学院由相互联繫和贯通的基本元素组成，包括地面设计大道、平台、「空中之城」、教学大楼及观景屋顶，拥有永不过时的建筑设计，同时体现了学院促进协同合作、透明公开及交流互动的目标。此种设计亦激励和启发着学生成长为未来的设计师。</p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-design-concept-1.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>教育</strong>设施</h2>
										</div>
										<div class="txt-editor">
											<h4>HKDI 拥有逾百个工作坊和工作室，可以满足不同设计学科学生的需要。此外，我们还设有四个知识中心，倡导跨学科设计教育并鼓励学生参与创新社会研究计划，以推进设计思维。</h4>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cill.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">语文自学中心 (CILL)</h3>
													<p class="thumb-desc__desc">语文自学中心提供一个舒适且亲切的环境，让学生学习并提升语文水平。中心提供独立学习资源，包括影音光碟、书籍、杂志及桌上游戏，亦安排多元化的学习活动，如工作坊、派对、表演及比赛。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_ml.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">媒体研究所</h3>
													<p class="thumb-desc__desc">媒体研究所致力于孕育和融合创新意念及媒体科技，并推动教育、应用研究、专业培训及行业应用的相互作用。媒体研究所设计部有由平面设计、二维或三维动态视像及产品等专业设计服务，彰显媒体的物质性展现方法。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cimt.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">知专设创源 (CIMT)</h3>
													<p class="thumb-desc__desc">知专设创源是一个全面的资料库及互动学习平台，旨在促进学生、设计教育界、设计师及生产商在物料知识及相关应用方面的交流。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_lrc.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">学习资源中心 (LRC)</h3>
													<p class="thumb-desc__desc">学习资源中心存有 75,000 多件馆藏，包括书籍、期刊及影音资讯等。不过，中心并不止是图书馆，同时亦是一个充满互学互动的地方，鼓励学生一同研习、讨论课题。中心同时设有一个长时间开放的专门空间 —— Zone 24，供学生学习及讨论专题习作并在此基础上展开合作之用。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_fa.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">时装资料馆</h3>
													<p class="thumb-desc__desc">时装资料馆透过有代表性的时装珍藏，作为教学及学习工具。全新修葺的资料馆总面积为 360 平方米，用以展示约 1,500 件拥有悠久历史的时尚珍藏。参观者透过全新的互动平台，观赏这批珍藏的资料，深入了解及体验时装设计、时尚文化和时装界的发展历史。</p>
												</div>
											</div>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>运动</strong>设施</h2>
										</div>
										<div class="thumb-desc">
										<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_swimming_pool.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">游泳池</h3>

													<p class="thumb-desc__desc"><strong>由於院校进行维修工程，本院游泳池将於2019年10月23日开始暂停开放。</strong><br><br>
													&nbsp;<br>4月至10月期间逢星期二至日开放 。<br>
													<strong>公众人士使用时段 (收费):</strong><br>
													星期二至五 | 1800 – 2100 <br>
													星期六、日及公众假期 | 0800 – 1100; 1300 – 1600; 1800 – 2100<br>
													游泳池入场费: 成人港币20元 | 12岁以下小童及60岁以上长者港币10元<br>
													查询: 3928 2222 / 3928 2000 (办公时间) 3928 2999 (非办公时间及恶劣天气)
													</p>

												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_basketball_court.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">篮球场</h3>

													<p class="thumb-desc__desc"><strong>由於院校进行维修工程，本院篮球场将於2019年10月23日开始暂停开放。</strong><br><br>
													<strong>公众人士使用时段 (收费):</strong><br>
													星期一至五 | 1900 – 2100 <br>
													星期六 | 1300 – 2100 <br>
													星期日及公众假期 | 0900 – 2100<br>
													篮球场租用收费: 每小时港币80元<br>
													查询: 3928 2222 / 3928 2000 (办公时间) 3928 2999 (非办公时间及恶劣天气)
													</p>

												</div>
											</div>
                                                                                        <div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_badminton.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">羽毛球场(多用途会堂)</h3>

													<p class="thumb-desc__desc"><strong>由於院校进行维修工程，本院羽毛球场将於2019年10月23日开始暂停开放。</strong><br><br>
													<strong>公众人士使用时段 (收费):</strong><br>
													星期六 | 1400 – 1800 <br>
													星期日 | 0900 – 1800<br>
													羽毛球场租用收费: 每小时港币90元<br>
													查询: 3928 2222 / 3928 2000 (办公时间) 3928 2999 (非办公时间及恶劣天气)
													</p>

												</div>
											</div>
                                                                                        <div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_tennis_court.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">网球场</h3>

													<p class="thumb-desc__desc"><strong>由於院校进行维修工程，本院网球场将於2019年10月23日开始暂停开放。</strong><br><br>
													<strong>外间团体使用时段 (收费):</strong><br>
													星期六 | 1200 – 1800 <br>
													星期日 | 0900 – 1800<br>
													网球场租用收费: 每小时港币50元<br>
													查询: 3928 2222 / 3928 2000 (办公时间) 3928 2999 (非办公时间及恶劣天气)
													</p>

												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width">
								<div class="content-wrapper">
									<div class="article-detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>其他</strong>设施</h2>
												<p class="detail__head-desc">为方便学生于一个设备齐全的环境中灵活学习，HKDI备有一系列的专业场地，讲师可以即席示范，并让学生实践知识及技巧。场地亦会用作其他教学相关用途，如举办大型授课及讲座，以及设计展览等。于非教学时间，场地亦供举办其他教育相关活动。</p>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-1.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">VTC 综艺馆</h3>
													<p class="thumb-desc__desc">VTC 综艺馆佔地 963 平方米，可容纳 700 多人。场地备有特别设计的音响设施，藉以提升各种不同表演、会议以及其他活动的音响效果。场馆亦设有残疾人士通道，是学院与社区互动共融的中心。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-2.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">体验中心</h3>
													<p class="thumb-desc__desc">体验中心为多用途空间，佔地 170 平方米，适合举办小型活动及展览。空间有两面均为落地玻璃窗，连接设计大道。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-3.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">HKDI Gallery</h3>
													<p class="thumb-desc__desc">HKDI Gallery 佔地 600 平方米，提供宽广的展览空间以供学院举行设计展览。场馆亦对外开放，可用于举办展览、贸易和行业相关活动，以及展出学生作品。</p>
													<!--<a href="#" class="thumb-desc__link">HKDI Gallery Website ></a>-->
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-4.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">设计大道</h3>
													<p class="thumb-desc__desc">全长达 125米 的设计大道贯通学院地面，连接两旁的综艺馆、展览场地及公共设施。设计大道被视为校园的「活动空间」开放而阔偌的通道不但为校园营造空间感，更利用两旁的展览及演艺场地汇聚设计气息，为学生提供活动和诱发灵感的互动空间。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-5.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">d-mart</h3>
													<p class="thumb-desc__desc">d-mart 佔地 1,040 平方米，提供宽广的展览空间以供学院举行设计展览，场馆亦对外开放，可用于举办展览、贸易和行业相关活动，以及展出学生作品。</p>
												</div>
											</div>


											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-multi-purpose-hall.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">多用途会堂</h3>
													<p class="thumb-desc__desc">多用途会堂是位于香港知专设计学院地下的多用途场地，占地338.6 平方米。场地主要供学院之学生作教学用途，亦会开放举办各类型的讲座及工作坊。</p>
												</div>
											</div>


										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="booking_enquiry">
								<div class="article-detail article-detail--green" name="booking">
									<div class="content-wrapper">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>租借</strong>场地</h2>
										</div>
										<div class="txt-editor">
											<h4>由於院校进行维修工程，本院租借场地将於2019年10月23日开始暂停服务。</h4>
										</div>
										<!--
										<div class="txt-editor">
											<h4>欢迎使用网上租用场地查询服务, 于递交申请前，请细阅申请人须知及有关使用条款。</h4>
										</div>
										<div class="contact-form">
											<form class="contact-form__form form-container" id="form-booking" action="<?php echo $host_name; ?>send-form.php" method="POST">

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>租借日期</strong>
															</div>
															<div class="field__date-fields">
																<div class="field__date-field">
																	<div class="field__date-label">由:</div>
																	<input type="text" name="booking_from_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
																<div class="field__date-field">
																	<div class="field__date-label">至:</div>
																	<input type="text" name="booking_to_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>场地租借</strong>
															</div>
															<div class="field__checkbox-holder">
																<div id="booking-checks" class="field__checkbox-fields form-check--empty">
																	<div class="custom-checkbox">
																		<input id="booking_checkbox1" data-limit="289" type="checkbox" name="booking_lecture_theatre_289" value="y" />
																		<label for="booking_checkbox1">演讲厅 (289 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox2" data-limit="120" type="checkbox" name="booking_lecture_theatre_120" value="y" />
																		<label for="booking_checkbox2">演讲厅 (120 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox3" data-limit="80" type="checkbox" name="booking_function_room" value="y" />
																		<label for="booking_checkbox3">	多用途活动室 (A002-A003) (80 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox4" data-limit="740" type="checkbox" name="booking_vtc_auditorium" value="y" />
																		<label for="booking_checkbox4">VTC 综艺馆 (740 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox5" type="checkbox" name="booking_hkdi_gallery" value="y" />
																		<label for="booking_checkbox5">HKDI Gallery</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox6" type="checkbox" name="booking_dmart" value="y" />
																		<label for="booking_checkbox6">d-mart</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox7" data-limit="50" type="checkbox" name="booking_experience_center" value="y" />
																		<label for="booking_checkbox7">体验中心 (50 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox8" type="checkbox" name="booking_design_beulevard" value="y" />
																		<label for="booking_checkbox8">设计大道</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox9" data-limit="80" type="checkbox" name="booking_multi_purpose_hall" value="y" />
																		<label for="booking_checkbox9">多用途会堂 (80 座位)</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>


													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>场地租借用途</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="booking_purpose" class="is-black form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="number" name="booking_participants" placeHolder="预计参加人数" class="form-check--empty form-check--max" />
															<span class="field__inline-txt booking-max-num-holder is-hidden">(最多：<span id="booking-max-num"></span>人)</span>
														</div>
													</div>
												</div>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">申请人详情</h3>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_organisation" placeHolder="机构" class="form-check--empty" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_contact_person" placeHolder="联络人" class="form-check--empty" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_tel" placeHolder="电话" maxlength="25" class="form-check--empty form-check--tel" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_fax" placeHolder="传真" maxlength="25" class="form-check--option form-check--tel" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_email" placeHolder="电邮" class="form-check--empty form-check--email" />
														</div>
													</div>
												</div>
												<br>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">验证码</h3>
												</div>
												<p class="contact-form__desc">请输入图中的数字</p>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="captcha">
																<div class="captcha__img">
																	<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																</div>
																<div class="captcha__control">
																	<div class="captcha__input">
																		<input type="text" name="captcha" class="form-check--empty" />
																	</div>
																	<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">更新验证码</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="contact-form__notes">
													<div class="contact-form__notes-title">申请人须知:</div>
													<ol class="contact-form__notes-list">
														<li>租借申请一般须于活动日期最迟一个月前或最早四个月前提交。</li>
														<li>申请人必须为香港注册「公司」或「团体」，以个人名义申请者恕不受理。</li>
														<li>如对本表格有任何疑问，请致电3928 2761。</li>
														<li>如有特别原因须预早筹备活动，可连同有关活动内容及计划书，透过<a href="mailto:hkdi-booking@vtc.edu.hk" class="underline-link">电邮</a>申请。</li>
													</ol>
												</div>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<br>
															<div class="custom-checkbox form-check--tnc">
																<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty" />
																<label for="contact_book">本人已细阅及同意「VTC Hire of Accommodation in Council Premises Term and Condition of Hire」
																	<a href="<?php echo $img_url?>about/VTC_Hire_of_Accommodation_in_Council_Premises_TC.PDF" class="underline-link" target="_blank">(PDF)</a> 及
																	「Guidelines for Event Organisation in HKDI & IVE(LWL)」<a href="<?php echo $img_url?>about/Guidelines_for_Event_Organisation_in_HKDI_ IVE(LWL).pdf" class="underline-link" target="_blank">(PDF)</a> 各项条款。
																</label>
															</div>
														</div>
													</div>
												</div>

												<div class="btn-row">
													<div class="err-msg-holder">
														<p class="err-msg err-msg--empty">必须填写</p>
														<p class="err-msg err-msg--tel">必须填写电话号码</p>
														<p class="err-msg err-msg--email">必须填写</p>
														<p class="err-msg err-msg--email-format">电邮格式无效</p>
														<p class="err-msg err-msg--phone">必须填写电话号码</p>
														<p class="err-msg err-msg--phone-format">电话号码格式无效</p>
														<p class="err-msg err-msg--tnc">必须接受条文及细则</p>
														<p class="err-msg err-msg--captcha">必须填写验证码</p>
														<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														<p class="err-msg err-msg--max">Exceeded the max. no. of seats.</p>
														<p class="err-msg err-msg-datepicker">Invalid date range </p>
													</div>
													<div class="btn-row btn-row--al-hl">
														<a href="#" class="btn btn-submit">
															<strong>送出</strong>
														</a>
														<a href="#" class="btn btn-reset">
															<strong>重设</strong>
														</a>
													</div>
												</div>
												<input type="hidden" name="lang" value="<?php echo $lang?>">
											</form>
											<div class="contact-form__success-msg">
												<h3 class="desc-subtitle">Thank you!</h3>
												<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
											</div>
										</div>
									-->
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-publication" class="page-tabs__content">
							<section class="article-detail">
								<!--div class="article-detail__head">
									<h2 class="article-detail__title">
										<strong>关于</strong>HKDI</h2>
								</div-->
								<div class="txt-editor">
									<h4>
										<strong>香港知专设计学院 (HKDI) 为职业训练局辖下学院，致力提供优质教育，建构知识和发展专业，为香港创意工业培育优秀的设计人才。</strong>
									</h4>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>课程概览</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/HKDI_prospectus_201920.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/HKDI_prospectus_201920_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>香港知专设计学院2019/20课程概览</strong>
											</h3>
											<!--<p class="publication-list__item-desc">Merry So</p>-->
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>SIGNED 杂志</strong>
									</h3>
									<!--<div class="publication-list__items">
										<?php for ($i = 17; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>ISSUE No. <?php echo $i; ?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<?php } ?>
									</div>-->
									<div class="publication-list__items">
									<?php
										$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));

										foreach ($issue_list AS $issue) {
											$issue_id = $issue['issue_id'];
											$issue_cover_image = $issue['issue_cover_image'];
											$issue_name = $issue['issue_name_'.$lang];

											if ($issue['issue_cover_image'] != '' && file_exists($issue_path.$issue_id."/".$issue_cover_image)) {
												$issue_cover_image = $issue_url.$issue_id."/".$issue_cover_image;
											}
									?>

										<a href="<?php echo $host_name_with_lang."news/publication.php?issue_id=".$issue_id?>" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $issue_cover_image?>" alt="" />
											<h3 class="publication-list__item-title">
												<strong><?php echo $issue_name?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									<?php for ($i = 14; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第<?php echo $i; ?>期</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>学术及研究</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/academic/5. Open Design in Action eBook.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Deisgn for Action by DESIS Lab_compressed.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Deisgn for Action by DESIS Lab</strong>
											</h3>
										</a>

										<a href="<?php echo $img_url?>about/publication/academic/Cumulus Hong Kong Proceeding.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Cumulus Hong Kong 2016 Working Papers by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Cumulus Hong Kong 2016 Working Papers by HKDI</strong>
											</h3>
										</a>
										<a href="<?php echo $img_url?>about/publication/academic/Open Design for Everything.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Design for E-very-thing by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Design for E-very-thing by HKDI</strong>
											</h3>
										</a>
										<a href="<?php echo $img_url?>about/publication/academic/4. FineDying_20150817.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Fine Dying  by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Fine Dying  by DESIS Lab</strong>
											</h3>
										</a>
										<a href="<?php echo $img_url?>about/publication/academic/3. ODF-book-v12.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Conversation from Open Design Forum 2014 by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Conversation from Open Design Forum 2014 by DESIS Lab</strong>
											</h3>
										</a>
										<a href="javascript:" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/footwear-H.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>鞋子箚记 – 香港老鞋匠口述历史</strong>
											</h3>
										</a>
										<a href="javascript:" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Fashion-cover-high-res-CMYK-300dpi.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>衣悭饰俭超实用置装术</strong>
											</h3>
										</a>
										<a href="javascript:" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/pattern-BK-high-res-CMYK-300dpi.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>布喜简易布艺DIY</strong>
											</h3>
										</a>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>得奖作品集 DAZZLE</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/DZ_design_content_web.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle3_cover.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第三期, 2016</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/dazzle2013.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle2_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第二期, 2013</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/Dazzle.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第一期, 2011</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-experience" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="txt-editor">
											<h4>
												<strong>我们为中学生提供量身定製的活动时间表，以便让他们更好地了解 HKDI 的设计教育，以及在设计领域可能获得的职业前景。</strong>
											</h4>
										</div>
									</div>
								</div>
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>学院</strong>导赏团</h2>
										</div>
										<div class="txt-editor">
											<p>
												学院导赏团可以让同学感受到学习环境的气氛，并详细了解 HKDI。参观的学生将会游览学院的设施，包括知识资源中心、不同设计学系的工作坊和工作室、学生作品，及 HKDI 与国际设计艺术伙伴合办的季节性展览等。
											</p>
											<p>每个导赏团大约 45 分钟，每团参观人数为 20 至25 位学生，并由最少一名老师或负责人陪同参加。我们可以安排特定设计工作坊或设计课程的参观路线，但需学校至少提前三个星期提交预约申请。</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>导引</strong>课程</h2>
										</div>
										<div class="txt-editor">
											<p>
												每个设计学系都可提供独一无二的导引课程，让同学尝试体验于 HKDI 学习，从而令他们更专注于不同的设计过程。传意设计及数码媒体学系将介绍三维打印或 360 度拍摄手法；基础设计学系将示范如何使用日常电子装置来创作艺术；时装及形象设计学系可让学生体验时装、形象和品牌的设计过程；而产品及室内设计学系将教授如何利用三维空间设计理论来打造室内及产品设计。
											</p>
											<p>每个导引课程大约 1.5 小时，每个课程的参加人数为 18 至 25 名学生，并由最少一名老师或负责人陪同参加。学校应至少提前一个月申请，另外导引课程内容以 HKDI 学院的工作室情况而定。
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>升学</strong>讲座</h2>
										</div>
										<div class="txt-editor">
											<p>在升学讲座中，HKDI 讲师将会分享设计课程资讯、入学要求及设计行业的就业出路。讲师亦会透过学生作品和业界合作，分享其有趣的教学方法及知识交流课程。
											</p>
											<p>有兴趣的学校可以选择心仪的学系课程，每个讲座为大约 45 至 60 分钟，此时间可进一步协商。我们亦可安排展出 HKDI 学生作品，但须按实际情况而定。
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>校友</strong>分享</h2>
										</div>
										<div class="txt-editor">
											<p>得到学界和业界认可的 HKDI 杰出校友，活动当日会参与分享及问答环节。通过互动和聊天，让学生透过年龄相彷而有经验的毕业生去认识 HKDI 的学习环境、就业出路以及制定生涯规划方桉。
											</p>
											<p>有兴趣的学校可以选择心仪的学系课程，分享会为大约 40 分钟，接着会有问答环节。一个分享会最多可安排 2 名校友。建议由学校安排一位老师作分享会的主持人，带领学生提出相关和实用的问题。
											</p>
											<p>有兴趣的教师应于拟议参观日前三週填写并提交下列表格。提交申请表格前，请仔细阅读《申请人须知》(Notes to Applicants)。</p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-taster.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="guided_tour_application_form" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>学院参观申请表格</strong></h2>
										</div>
										<div class="txt-editor">
											<h4>由於院校进行维修工程，学院参观申请将於2019年10月23日开始暂停服务。</h4>
											</div>
										<!--
										<div class="txt-editor">
											<h4>欢迎参观香港知专设计学院，了解本院提供之课程及服务，请于提交学院参观申请预约前，先细读申请须知各项细则。 请于计划参观日期起计，最少三个星期前提交预约申请表格。(电邮：eao-dilwl@vtc.edu.hk) 本院将收到申请团体/申请者的预约申请后之七个工作天内发电邮确认。</h4>
											<div class="contact-form contact-form--txt-white">
												<form class="contact-form__form form-container" id="form-application">
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>参观日期</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date1" placeholder="首选" class="form-check--empty datepicker datepicker--application" />
																	</div>
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date2" placeholder="次选" class="form-check--empty datepicker datepicker--application" />
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>参观时间</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_visit_time">
																				<option value="10:00 - 11:00">10:00 - 11:00</option>
																				<option value="11:00 - 12:00">11:00 - 12:00</option>
																				<option value="14:30 - 15:30">14:30 - 15:30</option>
																				<option value="15:30 - 16:30">15:30 - 16:30</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>参观人数</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="number" name="application_form_visitors_no" placeholder="参观人数" class="form-check--empty form-check--num" />
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>参观者名单，访客背景资料</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_list" class="form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>语言</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_list_lang">
																				<option value="Cantonese">粤语</option>
																				<option value="English">英语</option>
																				<option value="Chinese">普通话</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>到访目的</strong>
																</div>
																<div class="field__checkbox-holder">
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj1" type="radio" name="application_form_obj_academic_collaboration" value="y" checked/>
																			<label for="apply_obj1">学术交流</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj2" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj2">业界/机构参观</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj3" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj3">学校导赏团，注明学生就续级别</label>
																			<div data-ref="apply_obj3" class="radio-subs radio-child-container">
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj4" type="checkbox" name="application_form_obj_form4" value="y" />
																					<label for="apply_obj4">中四</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj5" type="checkbox" name="application_form_obj_form5" value="y" />
																					<label for="apply_obj5">中五</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj6" type="checkbox" name="application_form_obj_form6" value="y" />
																					<label for="apply_obj6">中六</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<label for="apply_others">其他</label>
																					<input type="text" name="application_form_obj_others" class="custom-checkbox__txt-input" />
																				</div>
																			</div>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj7" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj7">其他 (请注明):</label>
																			<span  data-ref="apply_obj7" class="radio-subs">
																			<input type="text" name="application_form_obj_others_desc" class="custom-checkbox__txt-input form-check--empty" />
																			</span>
																		</div>
																</div>
															</div>
														</div>
													</div>

													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>申请团体/申请者资料</strong>
														</h3>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_organisation" placeHolder="申请团体名称" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<div class="custom-select">
																	<select name="application_form_title">
																		<option>称谓</option>
																		<option value="Mr">先生</option>
																		<option value="Mrs">女士</option>
																		<option value="Ms">太太</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact_person" placeHolder="联络人姓名" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_position" placeHolder="职位名称" class="form-check--empty" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact" placeHolder="联络电话" class="form-check--empty form-check--tel" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_mobile" placeHolder="流动电话" class="form-check--option form-check--tel" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_email" placeHolder="电邮" class="form-check--empty form-check--email" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>备注</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_remarks" class="form-check--option" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<br>
													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>验证码</strong>
														</h3>
													</div>
													<p class="contact-form__desc">请输入图中的数字</p>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="captcha">
																	<div class="captcha__img">
																		<img id="application_form_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																	</div>
																	<div class="captcha__control">
																		<div class="captcha__input">
																			<input type="text" name="captcha" class="form-check--empty" />
																		</div>
																		<a href="#" class="captcha__btn" onClick="document.getElementById('application_form_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">更新验证码</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="contact-form__notes">
														<div class="contact-form__notes-title">申请须知:</div>
														<p class="contact-form__notes-desc">(提交学院参观申请预约前，请先细读以下各项)</p>
														<ol class="contact-form__notes-list">
															<li>申请团体/申请者须于计划参观日期起计，最少三个星期前提交预约申请表格。(电邮 : <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>)</li>
															<li>香港知专设计学院及香港专业教育学院(李惠利)欢迎公众人士参观本校，惟校园导赏一般仅提供予学校、学院、团体及业界组织，让他们认识本校校园设施、了解本校提供之课程、学生生活、业界和院校的合作及最新发展。</li>
															<li>如申请学校导赏团，请注明学生就续级别，例如：中五、中六。</li>
															<li>申请团体/申请者必须连同申请表格，提交参观者名单/访客背景资料。</li>
															<li>如申请团体/申请者已曾就同一次参观透过此系统又或电邮进行预约，请勿重覆递交申请表。</li>
															<li>学院参观或导赏团于星期六、日及公众假期停止服务。</li>
															<li>若悬挂黄色暴雨警告或三号颱风讯号，所有学院参观或导赏团将会取消。</li>
															<li>申请团体/申请者提供的个人资料将由本院用于处理参观申请及内部报告。</li>
															<li>本院收到申请团体/申请者的预约申请后，将发电邮确认。如提交申请及所有所需文件后七个工作天仍未收到回覆确认，请即联络本院外事处，电话(852) 3928 2561 或 电邮: <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>。</li>
															<li>香港知专设计学院及香港专业教育学院(李惠利)对申请事宜保留一切权利。如发现申请参观之团体/申请者呈报失实资料，学院有权拒绝或取消其申请。</li>
															<li>如欲取消已申请之学院参观，申请团体/申请者须于拟定参观日期两个工作天前以书面或电邮方式通知本院外事处职员，以便安排取消有关申请。</li>
														</ol>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<br>
																<div class="custom-checkbox form-check--tnc">
																	<input id="apply_tnc" type="checkbox" name="apply_tnc" class="form-check--empty " />
																	<label for="apply_tnc">本人同意及接受「学院参观 – 申请须知」内的条文及细则。</label>
																</div>
															</div>
														</div>
													</div>

													<div class="btn-row">
														<div class="err-msg-holder">
															<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
															<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														</div>
														<div class="btn-row btn-row--al-hl">
															<a href="#" class="btn btn-submit">
																<strong>送出</strong>
															</a>
															<a href="#" class="btn btn-reset">
																<strong>重设</strong>
															</a>
														</div>
													</div>
													<input type="hidden" name="lang" value="<?php echo $lang?>">
												</form>
												<div class="contact-form__success-msg">
													<h3 class="desc-subtitle">Thank you!</h3>
													<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
												</div>
											</div>
										</div>
									-->
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-professional-membership" class="page-tabs__content">

						</div>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>
				<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
