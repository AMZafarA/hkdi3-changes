<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "学士学位课程";
$section = 'programmes';
$subsection = 'bachelor-degree';
$sub_nav = 'bachelor-degree';


$breadcrumb_arr['设计课程'] =$host_name_with_lang.'programmes/#tabs-dg';
$breadcrumb_arr['学位课程'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$sql = "select DISTINCT universities_id,universities_image from ".$DB_STATUS."top_up_degree,".$DB_STATUS."universities where universities_status = '1' and top_up_degree_status = '1' and top_up_degree_univeristy_id=universities_id order by universities_seq desc";

$result = DM::execute($sql);

/*foreach($result as $row){
    echo "aaaaa".$row['universities_name_lang1'];
}*/

$universities_list = DM::findAll($DB_STATUS.'universities', ' universities_status = ?', " universities_seq desc", array(STATUS_ENABLE));

foreach($universities_list as $key => $universities) {
    $degree_list = DM::findAll($DB_STATUS.'top_up_degree', ' top_up_degree_status = ? and top_up_degree_univeristy_id = ?', " top_up_degree_seq desc", array(STATUS_ENABLE,$universities['universities_id']));

    foreach($degree_list as $key1 => $degree) {
        $degree_arr[$universities['universities_id']][] =$degree['top_up_degree_id'];
    }
}

$degree_list = DM::findAll($DB_STATUS.'top_up_degree', ' top_up_degree_status = ? ', " top_up_degree_seq desc", array(STATUS_ENABLE));



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/bachelor-degree.css" type="text/css" />
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
            <?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
                <div class="<?php echo $subsection ?> bd-info">
                    <section class="bachelor-degree-info bd-info__college">
                        <h2>学士学位课程</h2>
                        <p>HKDI为高级文凭毕业生提供<?php echo count($degree_list) ?>个衔接学士学位课程，仅须额外一年时间便可修毕学士学位课程。课程由HKDI联同六所英国着名大学共同提供，请点选下列课程或学校以了解更多详情。</p>
                        <p>同时，VTC豁下之香港高等教育科技学院亦提供四年制学士学位课程。</p>
                        <?php
                            foreach($result as $key =>$row){
                                $universities_id = $row['universities_id'];
                                $target = implode(",", $degree_arr[$universities_id]);
                                $universities_image = $row['universities_image'];
                                if($universities_image){
                                    $universities_image = $universities_url . $universities_id . "/" . $universities_image;
                                }
                        ?>
                        <a data-target="<?php echo $target?>" href="#"><img src="<?php echo $universities_image?>" /></a>
                        <?php } ?>

                    </section>


                    <section class="bachelor-degree-list">
                    <p class="title">课程：</p>
                    <ol class="bd-info__list">
                    <?php
                        foreach($degree_list as $key1 => $degree) {
                           $top_up_degree_id = $degree['top_up_degree_id'];
                           $top_up_degree_name = $degree['top_up_degree_name_'.$lang];
                    ?>
                        <li class="bd-info__item"  data-programme-id="<?php echo $top_up_degree_id?>"><a href="bachelor-degree-detail.php?degree_id=<?php echo $top_up_degree_id?>"><?php echo $top_up_degree_name?></a></li>
                    <?php } ?>
                    </ol>
                    </section>
                </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
