<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '课程';
$section = 'programmes';
$subsection = 'alumni';
$sub_nav = 'alumni';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$alumni_lv1 = 1;
$alumni_lv2 = $_REQUEST['alumni_lv2'] ?? '';
$alumni_list = DM::findAll($DB_STATUS.'alumni', ' alumni_status = ? AND alumni_lv1 = ? AND alumni_lv2 = ?', "alumni_seq DESC", array(STATUS_ENABLE,$alumni_lv1, $alumni_lv2));
for ($i = 0; $i < sizeof($alumni_list); $i++)
{
	$images = DM::findAll($DB_STATUS.'alumni_image', 'alumni_image_status = ? AND alumni_image_pid = ?', "alumni_image_seq", array(STATUS_ENABLE,$alumni_list[$i]['alumni_id']));
	$alumni_list[$i]['images'] = $images;
}

if ($alumni_lv2 == '') {
    header("location: ../index.php");
}


$dept = DM::load($DB_STATUS.'dept', $alumni_lv2);
$dept_name = $dept['dept_name_'.$lang];

$first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, $alumni_lv2 ));

$breadcrumb_arr['设计课程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr['高级文凭课程'] =$host_name_with_lang.'programmes/#tabs-hd';
$breadcrumb_arr[$dept_name] =$host_name_with_lang.'programmes/programme.php?programmes_id='.$first_programme['programmes_id'];
$breadcrumb_arr['校友专访'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<div class="page-head">
				<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>校友</strong>
								<br>专访</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">HKDI多年培育出多名在不同行业推动设计的校友。了解更多关于他们的故事。</p>
					</div>
				</div>
			</div>
			<div class="cards">
				<div class="cards__outer">
					<div class="cards__inner ani">
						<?php foreach ($alumni_list AS $alumni) { ?>
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="card__img">
									<img src="<?php echo $alumni_url.$alumni['alumni_id'].'/'.$alumni['alumni_thumb']; ?>" alt="" />
								</div>
								<div class="card__ppl">
									<div class="card__ppl-pic">
										<img src="<?php echo $alumni_url.$alumni['alumni_id'].'/'.$alumni['alumni_image']; ?>" alt="" />
									</div>
									<div class="card__ppl-txt">
										<strong class="card__ppl-name"><?php echo $alumni['alumni_name_'.$lang]; ?></strong>
										<p class="card__ppl-title"><?php echo $alumni['alumni_job_title_'.$lang]; ?></p>
									</div>
								</div>
								<div class="card__content">
									<p class="desc"><?php echo $alumni['alumni_desc_'.$lang]; ?></p>
									<a href="#" class="btn js-expander">更多</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="profile-block">
									<div class="profile-block__detail">
										<h3 class="profile-block__name"><?php echo $alumni['alumni_name_'.$lang]; ?></h3>
										<?php echo $alumni['alumni_detail_'.$lang]; ?>
										<?php /*<div class="profile-block__control">
											
											<div class="profile-block__control-item">
												<a href="#" class="btn btn--solid-white profile-block__btn-switch">Contact <?php echo $alumni['alumni_name_'.$lang]; ?></a>
											</div>
											
											<div class="profile-block__control-item">
												<h4 class="profile-block__control-title">Share</h4>
												<a href="#" class="btn-share btn-share--white btn-share--fb"></a>
												<a href="#" class="btn-share btn-share--white btn-share--email"></a>
												<a href="#" class="btn-share btn-share--white btn-share--tw"></a>
											</div>
										</div>*/ ?>
									</div>
									<div class="profile-block__gallery">
										<div class="profile-block__gallery-slider">
											<div class="swiper-wrapper">
												<?php for ($i = 0; $i < sizeof($alumni['images']); $i++) { ?>
												<?php 	$image = $alumni['images'][$i]; ?>
												<div class="profile-block__slide swiper-slide">
													<img src="<?php echo $alumni_url.$alumni['alumni_id'].'/'.$image['alumni_image_id'].'/'.$image['alumni_image_filename']; ?>" alt="" />
												</div>
												<?php } ?>
											</div>
											<div class="profile-block__gallery-pagination"></div>
										</div>
									</div>
								</div>
								<?php /*
								<div class="profile-block__contact">
									<div class="sec-intro sec-intro--txt-white">
										<h2 class="sec-intro__sub-title">
											<strong>Contact</strong> Form</h2>
									</div>
									<div class="contact-form">
										<form class="contact-form__form form-container">
											<div class="field-row">
												<div class="field field--1-2 field--mb-1-1">
													<div class="field__holder">
														<input type="text" name="contact_name" placeHolder="Name" class="form-check--empty" />
													</div>
												</div>
												<div class="field field--1-2 field--mb-1-1">
													<div class="field__holder">
														<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
													</div>
												</div>
											</div>
											<div class="field-row">
												<div class="field field--1-2 field--mb-1-1">
													<div class="field__holder">
														<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel"
														/>
													</div>
												</div>
												<div class="field field--1-2 field--mb-1-1">
													<div class="field__holder">
														<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email"
														/>
													</div>
												</div>
											</div>
											<div class="field-row">
												<div class="field field--1-1 field--mb-1-1">
													<div class="field__holder">
														<input type="text" name="contact_inquiry" placeHolder="Inquiry" class="form-check--empty " />
													</div>
												</div>
											</div>
											<div class="btn-row">
												<div class="err-msg-holder">
													<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
													<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
												</div>
												<div class="btn-row btn-row--al-hl">
													<a href="<?php echo $host_name_with_lang ?>" class="btn btn--solid-white">Send</a>
													<a href="<?php echo $host_name_with_lang ?>" class="btn btn--white profile-block__btn-switch">Cancel</a>
												</div>
											</div>
										</form>
										<div class="contact-form__success-msg">
											<h3 class="desc-subtitle">Thank you!</h3>
											<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
										</div>
									</div>
								</div>
								*/ ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>

			</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>