<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "Master Degree";
$section = 'programmes';
$subsection = 'master-degree';
$sub_nav = 'master-degree';

$breadcrumb_arr['Design Programmes'] =$host_name_with_lang.'programmes/#tabs-md'; 
$breadcrumb_arr['Master Degree'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, '2'));
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/master-details.css" type="text/css" />
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
            <?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
                <div class="<?php echo $subsection ?>">
        <?php foreach ($product_sections AS $product_section) { ?>
        <?php echo $product_section['product_section_content_'.$lang]; ?>
        <?php } ?>
        
                <!--<section class="degree-details">
                    <h2>Master Degree <br><span>Details of Programme</span></h2>
                    <div class="video-list">
                        <div><iframe src="https://www.youtube.com/embed/Pme1lqiGmT0?rel=0" frameborder="0" allowfullscreen></iframe><h4>Teaching Team and Guest Speakers</h4></div>
                        <div><iframe src="https://www.youtube.com/embed/Pme1lqiGmT0?rel=0" frameborder="0" allowfullscreen></iframe><h4>Teaching team</h4></div>
                        <div><iframe src="https://www.youtube.com/embed/Pme1lqiGmT0?rel=0" frameborder="0" allowfullscreen></iframe><h4>Guest speaker</h4></div>
                    </div>
                    <div class="txt">
                        <h4>Duration: Part-time in 2 years (Actual scheduled lessons within 8 to 9 months)</h4>
                        <ul>
                            <li>Total contact hours of this Master programme is about 180 hours (including lecture, tutorial and presentation). Each lesson is mostly 3 hours long, therefore the total number of lessons is around 60.</li>
                            <li>Normally two lessons are scheduled per week; one on weekday evening and one on Saturday afternoon.</li>
                            <li>There will be no lessons during major festivals, including but not limited to Chinese New Year, Easter, summer months and Christmas.</li>
                            <li>Only 8 to 9 months are scheduled with weekly lessons over the 2-year part-time programme, therefore the intermediate breaks can provide sufficient free weeks for self-learning and preparation. However, intensive lesson schedule will be arranged in those weekends during UK teachers' visit (3 visits per year).</li>
                            <li>Full-time study mode will be available in 2018.</li>
                        </ul>
                        <h4>Study Location</h4>
                        <p>The lessons will be mainly delivered in Wan Chai VTC Tower during weekdays while HKDI campus is mainly for online lessons with UK class and presentation sessions during weekend.</p>
                    </div>
                    <div class="visual-holder"><img src="<?php echo $inc_root_path ?>images/programme/degree/detail02.jpg" /></div>
                </section>
                
                <section class="organisations-logo">
                    <h4>Supporting Organisations</h4>
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/01.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/02.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/03.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/04.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/05.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/organiser/06.jpg" />
                </section>
                
                <section class="userful-link">
                    <div>
                        <h2>Useful <span>Links</span></h2>
                        <h3>Media Link</h3>
                        <p><small>14 Dec 2016</small></p>
                        <p><a href="#">Metro Daily (Online Version)  &gt;</a></p>
                        <p><a href="#">Apple Daily (Online Version)  &gt;</a></p>
                        <p><a href="#">Hong Kong Economic Times (Online Version)  &gt;</a></p>
                        
                        <p><small>17 Oct 2016</small></p>
                        <p><a href="#">Oriental Daily (Online Version)  &gt;</a></p>
                    </div>
                    <div>
                        <h3>Online Application</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,</p>
                        <a class="border-btn" href="#">Apply Now</a>
                        <h3>Application Form</h3>
                        <a class="border-btn" href="#">Download</a>
                    </div>
                </section>-->
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
