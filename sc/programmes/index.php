<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '课程';
$section = 'programmes';
$subsection = 'index';
$sub_nav = 'index';

$breadcrumb_arr['设计课程'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$AIP_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 6));
$CDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 7));
$DFS_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 5));
$FID_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 8));
$DDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 22));

$DFS_dept = DM::load($DB_STATUS.'dept', '5');
$AIP_dept = DM::load($DB_STATUS.'dept', '6');
$CDM_dept = DM::load($DB_STATUS.'dept', '7');
$FID_dept = DM::load($DB_STATUS.'dept', '8');
$DDM_dept = DM::load($DB_STATUS.'dept', '22');

if ($DFS_dept['dept_image'] != '' && file_exists($dept_path.$DFS_dept['dept_id']."/".$DFS_dept['dept_image'])) {
	$DFS_image = $dept_url.$DFS_dept['dept_id']."/".$DFS_dept['dept_image'];
}

if ($AIP_dept['dept_image'] != '' && file_exists($dept_path.$AIP_dept['dept_id']."/".$AIP_dept['dept_image'])) {
	$AIP_image = $dept_url.$AIP_dept['dept_id']."/".$AIP_dept['dept_image'];
}

if ($CDM_dept['dept_image'] != '' && file_exists($dept_path.$CDM_dept['dept_id']."/".$CDM_dept['dept_image'])) {
	$CDM_image = $dept_url.$CDM_dept['dept_id']."/".$CDM_dept['dept_image'];
}

if ($FID_dept['dept_image'] != '' && file_exists($dept_path.$FID_dept['dept_id']."/".$FID_dept['dept_image'])) {
	$FID_image = $dept_url.$FID_dept['dept_id']."/".$FID_dept['dept_image'];
}

if ($DDM_dept['dept_image'] != '' && file_exists($dept_path.$DDM_dept['dept_id']."/".$DDM_dept['dept_image'])) {
	$DDM_image = $dept_url.$DDM_dept['dept_id']."/".$DDM_dept['dept_image'];
}
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>设计</strong>
								<br>课程</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">融会创意的设计教学</p>
					</div>
				</div>
			</div>
			<div class="page-tabs">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-hd" class="page-tabs__btn is-active">
							<span>高级文凭课程</span>
						</a>
						<a href="#tabs-dg" class="page-tabs__btn">
							<span>学位课程</span>
						</a>
						<a href="#tabs-md" class="page-tabs__btn">
							<span>硕士学位课程</span>
						</a>
						<a target="_blank" href="http://www.hkdi.edu.hk/peec/" class="page-tabs__btn is-link">
<?php
/*
						<a href="<?php echo $host_name_with_lang?>continuing-education/" class="page-tabs__btn is-link">
*/
?>
							<span>持续进修</span>
						</a>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__wrapper">
						<div id="tabs-hd" class="page-tabs__content is-active">
							<div class="programme-portal">
								<div class="programme-intro">
									<h2 class="programme-intro__title">
										<strong>高级</strong>文凭课程</h2>
									<div class="programme-intro__desc">
										<p>HKDI提供超过20个设计相关的课程</p>
										<p>
											<b><i>
											「二零一八年度全日制高级文凭课程毕业生的就业率超越百分之九十，高于目标比率；而升学率则为百分之三十七。」
										</b></i>
										</p>
									</div>
								</div>
								<div id="pp-cdd" class="programme-portal__item ani">
									<a href="programme.php?programmes_id=<?php echo $AIP_first_programme['programmes_id']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $AIP_image?>" alt="" />
											<h2 class="programme-portal__item-txt">建筑、室内及产品设计</h2>
										</div>
									</a>
								</div>
								<div id="pp-dfs" class="programme-portal__item ani">

									<a href="programme.php?programmes_id=<?php echo $CDM_first_programme['programmes_id']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $CDM_image?>" alt="" />
											<h2 class="programme-portal__item-txt">传意设计</h2>
										</div>
									</a>
								</div>
								<!--div id="pp-aip" class="programme-portal__item ani">
									<a href="programme.php?programmes_id=<?php echo $DFS_first_programme['programmes_id']; ?>">
									<div class="programme-portal__item-body">
										<img class="programme-portal__item-img" src="<?php echo $DFS_image?>" alt="" />
										<h2 class="programme-portal__item-txt">基础设计</h2>
									</div>
								</a>
							</div-->
								<div id="pp-fid" class="programme-portal__item ani">

									<a href="programme.php?programmes_id=<?php echo $DDM_first_programme['programmes_id']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $DDM_image?>" alt="" />
											<h2 class="programme-portal__item-txt">数码媒体</h2>
										</div>
									</a>
								</div>
								<div id="pp-ddm" class="programme-portal__item ani">
									<a href="programme.php?programmes_id=<?php echo $FID_first_programme['programmes_id']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $FID_image?>" alt="" />
											<h2 class="programme-portal__item-txt">时装及形象设计</h2>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div id="tabs-dg" class="page-tabs__content">
							<div class="programme-intro">
								<h2 class="programme-intro__title">
									<strong>学位课程</strong>
								</h2>
								<div class="programme-intro__desc">
									<p>学生可以合共三年时间，完成为期两年的高级文凭课程，及一年衔接学士学位课程
									</p>
								</div>
							</div>
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
										     	香港高级<br />程度会考
										     </div>
										</div>
									</div>
									<a href="javascript:toTabsOne();" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												首两年<br />高级文凭
										     </div>
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												第三年<br />学士学位
										     </div>
										</div>
									</a>
<!-- 									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												第四年<br />硕士学位
										     </div>
										</div>
									</a> -->
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">学士学位课程</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">学士学位课程由HKDI联同六所英国着名大学共同提供。修读学生只需先完成两年高级文凭课程，便可选择升读为期一年的学士学位课程</p>
									</div>
								</a>
							</div>
						</div>
						<div id="tabs-md" class="page-tabs__content">
<!-- 							<div class="programme-intro">
								<h2 class="programme-intro__title">
									<strong>硕士</strong>学位课程
								</h2>
								<div class="programme-intro__desc">
									<p>学生于修毕两年的高级文凭课程及一年衔接学士学位后，可于第四年修读硕士学位课程
									</p>
								</div>
							</div> -->
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
										     	香港高级<br />程度会考
										     </div>
										</div>
									</div>
									<a href="javascript:$('.page-tabs__btn').eq(0).click();" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												首两年<br />高级文凭
										     </div>
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												第三年<br />学士学位
										     </div>
										</div>
									</a>
<!-- 									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
												第四年<br />硕士学位
										     </div>
										</div>
									</a> -->
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">硕士学位课程</h3>
									</div>
<!-- 									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI 伙拍英国伯明翰城市大学提供设计管理学文学硕士，予修毕高级文凭课程及衔接学士学位的学生，用额外一或两年完成硕士学位课程</p>
									</div> -->
</a>
							</div>
						</div>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>

			<?php /* ?>
            <div class="video-bg">
                <div class="video-bg__holder">
                    <video autoplay loop muted playsinline webkit-playsinline data-keeplaying="true" poster="<?php echo $img_url ?>video/video-smoke.jpg">
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/mp4" />
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/webm" />
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/ogg" />
                    </video>
                </div>
            </div>
            <?php */ ?>
				<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>
	<script>
		function toTabsOne(){
			$('.page-tabs__btn').eq(0).click();
		}
	</script>
	</html>
