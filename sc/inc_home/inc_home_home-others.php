<section class="section home-others" data-tooltip-name="更多活动">
    <div class="fullpage__content">
        <div class="fullpage__content-holder">
            <div class="home-others__holder">
                <div class="home-others__item">
                    <a href="http://www.vtc.edu.hk/admission/sc/programme/s6/higher-diploma/#design" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-CA2021_sc.jpg" alt="" />
                        <span class="home-others__item-btn">高级文凭课程<br>入学申请</span>
                    </a>
                </div>
                <div class="home-others__item">
                    <a href="https://www.hkdi.edu.hk/sc/edt/edt.php" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-edt2021.jpg" alt="" />
                        <span class="home-others__item-btn">Emerging Design Talents 2021: Design for Humanity</span>
                    </a>
                </div>
                <div class="home-others__item">
                    <a href="global-learning/" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-global_learning.jpg" alt="" />
                        <span class="home-others__item-btn">国际交流</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>