<section class="section home-top" data-tooltip-name="Top" data-home-top-style="<?php echo mt_rand(2, 6); ?>" style="height:100vh;">
    <div class="home-top__holder">
        <svg id="hm-slg" class="home-top__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1235 450" width="1235px" height="450px" preserveAspectRatio="xMidYMid slice">
            <defs>
                <mask id="mask" x="0" y="0" width="1235" height="450" >
                  <rect x="-1" y="-1" width="1237" height="452"></rect>
                    <g transform="translate(0, 0) scale(2)">
              				<path d="M95.8,201.9v-19.4H130c20.3,0,36.7-16.5,36.7-36.7v-21.5H2.6v21.5
              					c0,20.3,17.8,36.7,38,36.7h32.9v19.4H0V221h169.3v-19.1H95.8z M40.6,164c-8.7-0.2-17-8.9-17-18.3v-3.5h122.2v3.5
              					c0,9.4-7.3,17.9-17,18.3C104.9,164.9,65.1,164.6,40.6,164z"/>
              				<path d="M73.5,48.1H54c-12.7,0-50.1-1.5-50.1-1.5v19.9h69.6v3.4
              					C73.5,82,65,88,55.2,88.5c-21.7,1-52.6-0.7-52.6-0.7v19.7c0,0,29.4,0.6,55.2,0c10.6-0.3,21.2-6,26.8-14.3c5.3,8,16.5,14,26.9,14.3
              					c26.2,1,55.2,0,55.2,0V87.8c0,0-27.1,2-52.6,0.7c-8-0.4-18.3-6.5-18.3-18.7v-3.4h69.6V46.9c0,0-35.8,1.2-50.1,1.2H95.8l0,0V29.9
              					c24.7-0.8,47.7-3.4,71.1-10.1V0C143,8.1,108.7,12.8,85.2,12.8C61.3,12.8,26.8,8.6,2.4,0v19.8c23.5,6.7,46.4,9.3,71.1,10.1V48.1z"/>
              			</g>
                    <rect id="hm-slg--line" x="380" y="285" width="850" height="8"/>
                    <text id="hm-slg--hkid"  class="hm-slg--hkid" x="365" y="120">Hong Kong</text>
                    <text class="hm-slg--hkid"  x="365" y="240">Design Institute</text>
<!--
                    <g transform="translate(400, 0) scale(1)">
                        <path class="st0" d="M362.1,122.8c5.4-5.2,10.2-11.1,14.1-17.8C384,92.3,388,78.4,388,63.7V2h-52v61.7c0,8.8-3.8,17.3-10.7,25.1   c-6.8,7.6-14,11.2-22.1,11.2H276V2h-52v253h52V149h29.6c8.9,0,16.5,3.8,24.1,11.7c6.9,7.4,10.3,15.5,10.3,23.8V255h53v-72.5   c0-16.1-4.9-30.7-14.5-43.5C373.8,132.8,368.2,127.4,362.1,122.8z"/>
                        <path class="st0" d="M523.5,2H427v253h96.8c21.1,0,39.6-7.1,54.4-21.1c15-14.2,22.8-32,22.8-53.1V79.1c0-21-7.8-39.3-22.7-54.3   C563.3,9.7,544.7,2,523.5,2z M548,79.1v101.7c0,3.3-1,8.7-7.9,15.4c-5.4,5.2-10.8,7.8-16.7,7.8H480V54h45.7c6.1,0,11,2.1,15.4,6.7   C545.7,65.5,548,71.3,548,79.1z"/>
                        <rect id="hm-slg--hkid__i" x="637" y="2" class="st0" width="55" height="253"/>
                        <path class="st0" d="M127,99H75.5c-1.8-34-6.2-62.6-14.1-90.1L59.5,2H1.1l4.1,12.4c12,36.2,17.5,84.2,17.5,113.5   c0,31.9-5.4,79.7-17.5,114.7L1,255h58.5l2-6.5c8.4-29.4,12.9-59.5,14.5-97.5H127v104h55V2h-55V99z"/>
                        <g>
                            <path class="st0" d="M765,144c17.1,0,31-13.9,31-31c0-17.1-13.9-31-31-31c-17.1,0-31,13.9-31,31C734,130.1,747.9,144,765,144z"/>
                            <path class="st0" d="M765,190c-17.1,0-31,13.9-31,31s13.9,31,31,31c17.1,0,31-13.9,31-31S782.1,190,765,190z"/>
                        </g>
                    </g>
-->
                    <g transform="translate(0, 0) scale(2)">
                        <path class="st0" d="M95.8,201.9v-19.4H130c20.3,0,36.7-16.5,36.7-36.7v-21.5H2.6v21.5c0,20.3,17.8,36.7,38,36.7h32.9v19.4H0v19.1
                              h169.3v-19.1H95.8z M40.6,164c-8.7-0.2-17-8.9-17-18.3v-3.5h122.2v3.5c0,9.4-7.3,17.9-17,18.3C104.9,164.9,65.1,164.6,40.6,164z"/>
                        <path class="st0" d="M73.5,48.1H54c-12.7,0-50.1-1.5-50.1-1.5v19.9h69.6v3.4C73.5,82,65,88,55.2,88.5c-21.7,1-52.6-0.7-52.6-0.7
                              v19.7c0,0,29.4,0.6,55.2,0c10.6-0.3,21.2-6,26.8-14.3c5.3,8,16.5,14,26.9,14.3c26.2,1,55.2,0,55.2,0V87.8c0,0-27.1,2-52.6,0.7
                              c-8-0.4-18.3-6.5-18.3-18.7v-3.4h69.6V46.9c0,0-35.8,1.2-50.1,1.2H95.8h0V29.9c24.7-0.8,47.7-3.4,71.1-10.1V0
                              C143,8.1,108.7,12.8,85.2,12.8C61.3,12.8,26.8,8.6,2.4,0v19.8c23.5,6.7,46.4,9.3,71.1,10.1V48.1z"/>
                            </g>
                            <text class="hm-slg--hash" x="380" y="375">#设计课程</text>
                            <text class="hm-slg--hash" x="590" y="375">#HKDI知识资源中心</text>
                            <text class="hm-slg--hash" x="380" y="425">#设计展览</text>
                            <text class="hm-slg--hash" x="590" y="425">#入学申请</text>
                            <text class="hm-slg--hash" x="800" y="425">#设计思维</text>
                </mask>
            </defs>
            <rect x="0" y="0" width="1920" height="1080"/>
            <g style="opacity: 0;">
              <a href="javascript:scrollToElemTop($('.academic-programmes'), 1000)"><text class="hm-slg--hash" x="380" y="375">#设计课程</text></a>
              <a href="javascript:scrollToElemTop($('.knowledge-centres'), 1000)"><text class="hm-slg--hash" x="590" y="375">#HKDI知识资源中心</text></a>
              <a href="<?php echo $inc_lang_path?>hkdi_gallery"><text class="hm-slg--hash" x="380" y="425">#设计展览</text></a>
              <a href="http://www.vtc.edu.hk/admission/sc/programme/s6/higher-diploma/#design" target="_blank"><text class="hm-slg--hash" x="590" y="425">#入学申请</text></a>
              <a href="<?php echo $inc_lang_path?>news/news-detail.php?news_id=602&tab=all&page=1"><text class="hm-slg--hash" x="800" y="425">#设计思维</text></a>
            </g>
        </svg>
        <div class="home-top__vid">
            <div class="home-top__vid-holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
            </div>
        </div>
    </div>
    <div class="sec-footbar">
        <div class="content-wrapper">
            <div class="sec-footbar__grp">
                <a href="#" class="sec-footbar__btn-down"></a>
            </div>
            <div class="sec-footbar__grp">
                <h2 class="sec-footbar__title">你在找寻:</h2>
                <a href="index.php?cat=1" class="sec-footbar__item <?php echo $cat == 1 ? 'is-active' : ''; ?>">设计课程</a>
                <a href="index.php?cat=2" class="sec-footbar__item <?php echo $cat == 2 ? 'is-active' : ''; ?>">合作伙伴</a>
                <!--<a href="index.php?cat=3" class="sec-footbar__item <?php echo $cat == 3 ? 'is-active' : ''; ?>">探索资讯</a>-->
                <a href="<?php echo $host_name_with_lang?>about/#tabs-experience" class="sec-footbar__item">体验设计</a>
                
				        <a href="https://www.vtc.edu.hk/admission/sc/programme/s6/higher-diploma/#design" class="sec-footbar__item " target="_blank">入学申请</a>
            </div>
            <?php /* ?>
            <div class="sec-footbar__grp">
                <a id="btnTest" href="#" class="sec-footbar__item">TEST</a>
                <a id="btnTest2" href="#" class="sec-footbar__item">TEST2</a>
            </div>
              <?php */ ?>
        </div>
    </div>
</section>
