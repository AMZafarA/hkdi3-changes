﻿<section class="section home-others" data-tooltip-name="更多活动">
    <div class="fullpage__content">
        <div class="fullpage__content-holder">
            <div class="home-others__holder">
                <div class="home-others__item">
                    <a href="http://www.vtc.edu.hk/admission/sc/s6/application-information/degree-higher-diploma-foundation-diploma/central-admission-scheme/?utm_source=Google&utm_medium=SEM&utm_term=vtc_admission&utm_campaign=IVE/HKDI/ICI_SummerRound_AsiaPac" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-CA_sc.jpg" alt="" />
                        <span class="home-others__item-btn">統一收生計劃</span>
                    </a>
                </div>
                <div class="home-others__item">
                    <a href="http://www.hkdi.edu.hk/sc/news/news-detail.php?news_id=602&tab=all&page=1" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-DTteam.jpg" alt="" />
                        <span class="home-others__item-btn">HKDI设计思维团队</span>
                    </a>
                </div>
                <div class="home-others__item">
                    <a href="global-learning/" class="home-others__item-inner">
                        <img class="home-others__item-img" src="<?php echo $img_url ?>home/img-home-global_learning.jpg" alt="" />
                        <span class="home-others__item-btn">国际交流</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>