<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Continuing Education';
$section = 'continuing-education';
$subsection = 'detail';
$sub_nav = 'detail';

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$continuing_edcation = DM::findOne($DB_STATUS.'continuing_edcation', 'continuing_edcation_status = ? AND continuing_edcation_id = ?', '', array(STATUS_ENABLE, '1'));
$banners = DM::findAll($DB_STATUS.'continuing_edcation_image', 'continuing_edcation_image_status = ? AND continuing_edcation_image_pid = ?', '', array(STATUS_ENABLE, '1'));

$continuing_education_groups = DM::findAll($DB_STATUS.'continuing_education_group', 'continuing_education_group_status = ?', 'continuing_education_group_seq', array(STATUS_ENABLE));
for ($i = 0; $i < sizeof($continuing_education_groups); $i++)
{
	$courses = DM::findAll($DB_STATUS.'product', 'product_status = ? AND product_type = ? AND product_signed_id = ? ', 'product_seq', array(STATUS_ENABLE, '3', $continuing_education_groups[$i]['continuing_education_group_id']));
	$continuing_education_groups[$i]['courses'] = $courses;
}

$conditions = "news_status = ? AND news_lv1 = ? AND news_from_date <= ? AND news_to_date >= ?";
$parameters = array(STATUS_ENABLE, '4', $today, $today);
$news_limit = 4;
$news_list = DM::findAllWithLimit($DB_STATUS.'news', $news_limit, $conditions, "news_date DESC", $parameters);
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>持续进修</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="banner-slider">
				<div class="content-wrapper">
					<div class="banner-slider__inner">
						<div class="banner-slider__slider">
							<div class="swiper-wrapper">
								<?php foreach ($banners AS $banner) { ?>
								<?php 	if ($banner['continuing_edcation_image_filename'] != '' && file_exists($continuing_edcation_path.$banner['continuing_edcation_image_pid'].'/'.$banner['continuing_edcation_image_id'].'/'.$banner['continuing_edcation_image_filename'])) { ?>
								<div class="banner-slider__slide swiper-slide">
									<img src="<?php echo $continuing_edcation_url.$banner['continuing_edcation_image_pid'].'/'.$banner['continuing_edcation_image_id'].'/'.$banner['continuing_edcation_image_filename']; ?>" alt="" />
								</div>
								<?php 	} ?>
								<?php } ?>
							</div>
						</div>
						<div class="banner-slider__pagination"></div>
					</div>
				</div>
			</div>
			<div class="txt-editor">
				<div class="content-wrapper">
					<p><?php echo $continuing_edcation['continuing_edcation_desc_'.$lang]; ?></p>
				</div>
			</div>
			<div class="info-table">
				<div class="content-wrapper">
					<div class="info-table__row">
						<div class="info-table__col">
							<?php for ($i = 0; $i < ceil(sizeof($continuing_education_groups) / 2); $i++) { ?>
							<div class="info-table__table">
								<table>
									<tr>
										<th>
											<span><?php echo $continuing_education_groups[$i]['continuing_education_group_name_'.$lang]; ?></span>
										</th>
										<th>
											<span>申请截止日期</span>
										</th>
									</tr>
									<?php for ($c = 0; $c < sizeof($continuing_education_groups[$i]['courses']); $c++) { ?>
									<?php 	$course = $continuing_education_groups[$i]['courses'][$c]; ?>
									<tr class="info-table__link" data-href="course.php?product_id=<?php echo $course['product_id']; ?>">
										<td><?php echo str_pad(($c+1), 2, "0", STR_PAD_LEFT); ?>.
											<strong><?php echo $course['product_name_'.$lang]; ?></strong>
										</td>
										<td>
											<small><?php echo date('d.m.Y', strtotime($course['product_date'])); ?></small>
										</td>
									</tr>
									<?php } ?>
								</table>
							</div>
							<?php } ?>	
							<?php for ($i; $i < sizeof($continuing_education_groups); $i++) { ?>
							<div class="info-table__table">
								<table>
									<tr>
										<th>
											<span><?php echo $continuing_education_groups[$i]['continuing_education_group_name_'.$lang]; ?></span>
										</th>
										<th>
											<span>申请截止日期</span>
										</th>
									</tr>
									<?php for ($c = 0; $c < sizeof($continuing_education_groups[$i]['courses']); $c++) { ?>
									<?php 	$course = $continuing_education_groups[$i]['courses'][$c]; ?>
									<tr class="info-table__link" data-href="course.php?product_id=<?php echo $course['product_id']; ?>">
										<td><?php echo str_pad(($c+1), 2, "0", STR_PAD_LEFT); ?>.
											<strong><?php echo $course['product_name_'.$lang]; ?></strong>
										</td>
										<td>
											<small><?php echo date('d.m.Y', strtotime($course['product_date'])); ?></small>
										</td>
									</tr>
									<?php } ?>
								</table>
							</div>
							<?php } ?>						
						</div>
						<div class="info-table__col">
						<a href="<?php echo $host_name_with_lang?>application" class="btn"><strong>入学申请</strong></a>
						<a href="<?php echo $host_name_with_lang?>financial-aids" class="btn"><strong>学费资助</strong></a>
						<a href="<?php echo $host_name_with_lang?>corporate-training" class="btn"><strong>企业培训</strong></a>
						<br />
						<br />
						<br />
						<h3>联络我们</h3>
						<p>
						电话: +852 3928 2777
						<br />
						电邮: hkdi@vtc.edu.hk
						
						</p>
						
						<h3>开放时间:</h3>
						<p>
							星期一及星期三 上午9时至下午6时
						<br />
							星期二, 星期四及星期五 上午9时至下午8时
						<br />
							(午休时间: 每日下午1时至2时)
						</p>
						
						<h3>关注我们</h3>
						<p>
                    <div class="btn-share__holder">
                        <a href="http://www.facebook.com/HongKongDesignInstitute" class="btn-share btn-share--fb" target="_blank"></a>
                        <a href="https://instagram.com/hongkongdesigninstitute" class="btn-share btn-share--ig" target="_blank"></a>
                        <a href="http://www.youtube.com/user/hkdichannel" class="btn-share btn-share--youtube" target="_blank"></a>
                        <a href="http://www.twitter.com/thehkdi" class="btn-share btn-share--tw" target="_blank"></a>
                        <a href="http://weibo.com/thehkdi" class="btn-share btn-share--wb" target="_blank"></a>
                    </div>
						</p>
						</div>
					</div>
				</div>
			</div>

			<div class="article-detail">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>Job Openings</strong> @ PEEC
					</h2>
					<div class="txt-editor">
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque.</p>
						<a href="#" class="btn"><strong>Read more</strong></a>
					</div>
				</div>
			</div>
			<div id="programme" class="programme--ce">

				<?php /*<section class="programme-related ani">
					<div class="blue block">
						<div class="table">
							<div class="cell">
								<h3>Staff Directory</h3>
								<p>Lorem ipsum dolor sit amet, con sectetuer adipi scing elit. Aenean com modo ligula eget dolor. Ae orem ipsum dolor
									sita.
								</p>
								<a class="btn-arrow" href="#"></a>
							</div>
						</div>
					</div>
					<div class="purple block">
						<div class="table">
							<div class="cell">
								<h3>Alumni Interview</h3>
								<p>Lorem ipsum dolor sit amet, con sectetuer adipi scing elit. Aenean com modo ligula eget do massa.Lorem ips.</p>
								<p class="cell-picture">
									<img src="<?php echo $inc_root_path ?>images/programme/programme/08.jpg" />Merry So</p>
									<a class="btn-arrow btn-arrow-right" href="#"></a>
							</div>
						</div>
					</div>
					<div class="dark-blue block">
						<div class="table">
							<div class="cell">
								<h3>Student Awards</h3>
								<p class="date">1 April 2017</p>
								<p>Lorem ipsum dolor sit amet, con sectetuer adipi scing elit. Aenean com modo ligula eget dolor. Ae nean massa.Lorem
									ips.
								</p>
								<a class="btn-arrow btn-arrow-right" href="#"></a>
							</div>
						</div>
					</div>
					<div class="black block">
						<div class="table">
							<div class="cell">
								<h3>HKDI Design Programmes</h3>
								<a href="#">Communication Design &amp; Digital Media &gt;</a>
								<br />
								<a href="#">Design Foundation Studies &gt;</a>
								<br />
								<a href="#">Fashion &amp; Image Design &gt;</a>
							</div>
						</div>
					</div>
				</section> */?>

				<?php /*<section class="bottom-panel">
					<div class="content-wrapper">
						<div class="bottom-panel__row">
							<div class="bottom-panel__col">
								<h3 class="bottom-panel__title">
									<strong>Contact</strong> Us</h3>
								<div class="bottom-panel__grp">
									<h3 class="bottom-panel__grp-title">Enquiry</h3>
									<p class="bottom-panel__desc">
										Tel:      +852 3928-2777
										<br> Email:  peec.hkdi@vtc.edu.hk
									</p>
								</div>
								<div class="bottom-panel__grp">
									<h3 class="bottom-panel__grp-title">Office Hours</h3>
									<p class="bottom-panel__desc">
										Special Arrangement in Jul & Aug 2017:
										<br> Mon, Tue, Thu, 9:00am – 1:00pm & 2:00pm – 6:00pm
										<br> Wed & Fri, 9:00am – 1:00pm & 2:00pm – 8:00pm
										<br> (Lunch Break - 1.00pm - 2.00pm)
									</p>
								</div>
							</div>
							<div class="bottom-panel__col">
								<h3 class="bottom-panel__title">
									<strong>Find</strong> Us On</h3>
								<div class="bottom-panel__grp">
									<h3 class="bottom-panel__grp-title">Facebook</h3>
									<a class="bottom-panel__link" href="#">HKDI-PEEC &gt;</a>
									<a class="bottom-panel__link" href="#">HKDI x Models International &gt;</a>
									<a class="bottom-panel__link" href="#">Creative Multimedia Illustration Workshop &gt;</a>
									<a class="bottom-panel__link" href="#">Workshop by COMINGdepartment &gt;</a>
								</div>
								<div class="bottom-panel__grp">
									<div class="bottom-panel__item">
										<h3 class="bottom-panel__grp-title">
											<strong>Instagram</strong>
										</h3>
										<a class="bottom-panel__link" href="#">HKDI-PEEC &gt;</a>
									</div>
									<div class="bottom-panel__item">
										<h3 class="bottom-panel__grp-title">
											<strong>Youtube</strong>
										</h3>
										<a class="bottom-panel__link" href="#">HKDI-PEEC &gt;</a>
									</div>
									<div class="bottom-panel__item">
										<h3 class="bottom-panel__grp-title">
											<strong>Weibo</strong>
										</h3>
										<a class="bottom-panel__link" href="#">HKDI-PEEC &gt;</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>*/ ?>
				
				<section class="programme-news ani">					
					<?php foreach ($news_list AS $news) { ?>
					<?php 	if ($news['news_thumb'] != '' && file_exists($news_path.$news['news_id']."/".$news['news_thumb'])) { ?>
					<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>">
						<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
						<div class="txt">
							<p class="date">Latest News | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
							<p><?php echo $news['news_name_'.$lang]; ?></p>
							<p class="btn-arrow"></p>
						</div>
					</a>
					<?php 	} else { ?>
					<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>">
						<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
						<div class="table">
							<div class="cell">
								<p>Latest News</p>
								<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
							</div>
							<p class="btn-arrow"></p>
						</div>
					</a>
					<?php 	} ?>
					<?php } ?>
				</section>
			</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
