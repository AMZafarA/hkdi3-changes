<?php
$inc_root_path = '../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Home';
$section = 'home';
$subsection = 'index';
$sub_nav = 'home';
$cat = 2;
if (isset($_GET['cat'])) {
    $cat = $_GET['cat'];
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
  <script>
  $( function() {
    $( "#aaa" ).draggable();
  } );
  </script>
    </head>
    <body class="page-<?php echo $section ?> at-home-top" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>" data-fullpage-index="1">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="video-bg" style="opacity:1;">
                <div class="video-bg__holder">
                    <video autoplay loop muted playsinline webkit-playsinline data-keeplaying="true" poster="<?php echo $img_url ?>video/video-smoke.jpg">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
                    </video>
                </div>
            </div>
            <div id="aaa" style="clip: rect(0, auto, auto, 0);opacity:1;top:50%;left:50%;margin-left:-150px;margin-top:-150px;width:300px;height:300px; overflow:hidden;display: block;position: absolute;">
                <div class="video-bg" style="opacity:1;left: 0;">
                    <div class="video-bg__holder">
                        <video autoplay loop muted playsinline webkit-playsinline data-keeplaying="true" poster="<?php echo $img_url ?>video/video-smoke.jpg">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
                        </video>
                    </div>
                </div>
            </div>
        </main>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
