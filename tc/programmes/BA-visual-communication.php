<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = "BA (Hons) Fine Art";
$section = 'programmes';
$subsection = 'bachelor-degree';
$sub_nav = 'bachelor-degree';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/ba-details.css" type="text/css" />
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
                <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
                <div class="<?php echo $subsection ?>">
            	<section class="degree-details">
                	<h2>BA (Hons) <span>Visual Communication</span><small>(Graphic Communication)</small></h2>
                    <p><small>Registration Number (Non-Local Higher and Professional Education (Regulation) Ordinance): 252577</small></p>
                    <h4>This programme is centered on the development of an art practice to a professional standard. This involves, through the study of three interlinked modules, the initiation, development and a professional resolution of a body of practical art work in the form of an exhibition; a 5,000-6,000 words dissertation linked to the students' practice; and a professional visual and verbal presentation in the form of a website and a 10-15min live 'pitch' in front of an audience.</h4>
                    <p>In the studio, outside of the set tutorial times, the students will be independent in organising and managing their study and the effective use of resources. The course requires self-motivation, initiative and imagination. Students need to think like an artist!
Students progress to professional practice, to further study and to a wide range of career sequels including teaching, design, theatre, television and media, marketing, curation and arts administration.In the studio, outside of the set tutorial times, the students will be independent in organising and managing their study and the effective use of resources. The course requires self-motivation, initiative and imagination. Students need to think like an artist!
Students progress to professional practice, to further study and to a wide range of career sequels including teaching, design, theatre, television and media, marketing, curation and arts administration.</p>
				    <a class="border-btn" href="#">Enroll</a>
                </section>
                
            	<section class="visual-holder">
                    <img src="<?php echo $inc_root_path ?>images/programme/degree/detail01.jpg" />
                    <img src="<?php echo $inc_root_path ?>images/programme/degree/detail02.jpg" />
                </section>
                
                <section class="school-info">
                    <div>
                        <p><small>Programme organized by</small></p>
                        <img src="<?php echo $inc_root_path ?>images/programme/degree/logo.png" />
                        <h3>Birmingham City University, UK</h3>
                        <p class="highlight">BCU is one of the foremost centres for art, architecture and design education in the UK. The university has a proud tradition dating back to the foundation of the Birmingham School of Art in 1843, and has an international reputation for the quality of its programmes. </p>
				        <p>Currently, there are almost 4,000 full-time and part-time students studying a substantial portfolio of courses. These include further education certificates and diplomas, and undergraduate and postgraduate programmes – all of which provide students with the knowledge, skills and confidence that are relevant to contemporary creative practice.</p>
                        <p>BCU is ranked in the top 10 in the UK for Art and Design research, delivering focused 'world class' solutions targeted at priority and emerging themes</p>
                    </div>
                </section>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
