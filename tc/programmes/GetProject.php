<?php
$inc_root_path = '../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";


$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);
$page_size = 6;

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$programmes_id = $_REQUEST['programmes_id'] ?? '';
$lang = $_REQUEST['lang'] ?? '';

$programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_id = ?', " programmes_seq desc", array(STATUS_ENABLE,$programmes_id ));

if(!$programme){
    //header("location: ../index.php");
}

$programmes_id = $programme['programmes_id'];
$programmes_lv2 = $programme['programmes_lv2'];



$project_list = DM::findAllWithPaging($DB_STATUS.'project', $page, $page_size, ' project_status = ? and project_lv2 = ?', " project_seq desc", array(STATUS_ENABLE,$programmes_lv2));

foreach($project_list as $key => $project) {  
									$project_name = $project['project_name_'.$lang];
									$project_id = $project['project_id'];
									$project_image1 = $project['project_image1'];
									$project_image2 = $project['project_image2'];
									$project_detail = $project['project_detail_'.$lang];

									if ($project_image1 != '' && file_exists($project_path.$project_id."/".$project_image1)) {
										$project_image1 =$project_url.$project_id."/".$project_image1;
									}else{
										$project_image1 ='';
									}

									if ($project_image2 != '' && file_exists($project_path.$project_id."/".$project_image2)) {
										$project_image2 =$project_url.$project_id."/".$project_image2;
									}else{
										$project_image2 ='';
									}
							?>
							<div class="hidden-block">
								<h3><?php echo $project_name?></h3>
								<div class="hidden">
									<p class="title"><?php echo $project_name?></p>
									<?php if ($project_image1 != '' || $project_image2 != ''){ ?>
									<div class="img-holder">
										<?php if ($project_image1 != ''){ ?>
										<img src="<?php echo $project_image1 ?>" />
										<?php } ?>
										<?php if ($project_image2 != ''){ ?>
										<img src="<?php echo $project_image2 ?>" />
										<?php } ?>
									</div>
									<?php } ?>
									<?php echo $project_detail?>
								</div>
							</div>
							<?php } ?>

<?php } ?>