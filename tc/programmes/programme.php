﻿<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '課程';
$section = 'programmes';
$subsection = 'detail';
$sub_nav = 'detail';


DM::setup($db_host, $db_name, $db_username, $db_pwd);
$programmes_id = $_REQUEST['programmes_id'] ?? '';

$programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_id = ?', " programmes_seq desc", array(STATUS_ENABLE,$programmes_id ));

if(!$programme){
    header("location: ../index.php");
}

$programmes_id = $programme['programmes_id'];
$programmes_lv2 = $programme['programmes_lv2'];

$dept = DM::load($DB_STATUS.'dept', $programmes_lv2);
$dept_desc = $dept['dept_detail_'.$lang];
$dept_name = $dept['dept_name_'.$lang];

$department = DM::findOne($DB_STATUS.'department', ' department_id = ?', " ", array($programmes_lv2));
$department_name = $department['department_name_'.$lang];

$AIP_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,$programmes_lv2));

$news_list = DM::findAllWithLimit($DB_STATUS.'news', '4',' news_status = ? and news_lv2 = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)', " news_date desc", array(STATUS_ENABLE,$programmes_lv2,$today,$today));

$project_list = DM::findAllWithLimit($DB_STATUS.'project', '6', ' project_status = ? and project_lv2 = ?', " project_seq desc", array(STATUS_ENABLE,$programmes_lv2));
$total_project = DM::count($DB_STATUS.'project', ' project_status = ? and project_lv2 = ?', array(STATUS_ENABLE,$programmes_lv2));
$page_size = 6;
$total_project_page = ceil($total_project / $page_size);

$alumni = DM::findOne($DB_STATUS.'alumni', ' alumni_status = ? AND alumni_lv1 = ? AND alumni_lv2 = ?', "alumni_seq DESC", array(STATUS_ENABLE,'1', $programmes_lv2));
$student_award = DM::findOne($DB_STATUS.'student_award', ' student_award_status = ? AND student_award_lv1 = ? AND student_award_lv2 = ?', " student_award_seq DESC", array(STATUS_ENABLE,'1', $programmes_lv2));

$AIP_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 6));
$CDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 7));
$DFS_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 5));
$FID_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 8));
$DDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 22));

$breadcrumb_arr['設計課程'] =$host_name_with_lang.'programmes/#tabs-hd';
$breadcrumb_arr['高級文憑課程'] =$host_name_with_lang.'programmes/#tabs-hd';
$breadcrumb_arr[$dept_name] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script>
		var php_lang = "<?php echo $lang?>";
		var programmes_id = "<?php echo $programmes_id?>";

		$(function(){
			$('.programme-info__detail .txt-editor a[href^="https://www.vtc.edu.hk/admission/tc/programme/"]').addClass('btn').html('入學申請');
		});
		</script>
		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
		<style>
			.swiper-button-prev {
				background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%2300a6e9'%2F%3E%3C%2Fsvg%3E");
			}

			.swiper-button-next {
				background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%2300a6e9'%2F%3E%3C%2Fsvg%3E");
				right: 30px;
			}
		</style>
		<?php
			if ($programmes_lv2 == '5') {
				$deptColor = "green";
				$deptColorCode = "#00ff88";
			} else if ($programmes_lv2 == '6') {
				$deptColor = "blue";
				$deptColorCode = "#0099ff";
			} else if ($programmes_lv2 == '7') {
				$deptColor = "yellow";
				$deptColorCode = "#ff6600";
			} else if ($programmes_lv2 == '8') {
				$deptColor = "red";
				$deptColorCode = "#ff2a69";
			}
      else if ($programmes_lv2 == '22') {
        $deptColor = "orange";
        $deptColorCode = "#fedd00";
      }
		?>
		<style>
		.programme-info__title,
		.programme-info__title-num,
		.programme-info__list-item.is-active, .programme-info__list-item:hover{
		    color: <?php echo $deptColorCode?>;
		}
		.programme-content__img-slider:before,
		.page-head.page-head--has-list .page-head__blue-bar{
		    background-color: <?php echo $deptColorCode?>;
		}

		.programme-info__title strong{
		    color: #000;
		}
		.programme-info__btn-prev:before, .programme-info__btn-next:before{
		    background-image:url(<?php echo $host_name?>images/common/icons/svg/<?php echo $deptColor?>/icon-arrow-down.svg);
		}
		</style>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access">
			<?php echo $page_title?>
		</h1>
		<main>
			<!--Refactoring-->
			<section class="programme-info">
				<div class="page-head page-head--has-list">
					<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
					<div class="page-head__wrapper">
						<div class="page-head__title-holder">
							<div class="page-head__title">
								<h2>
									<strong><?php echo $department_name?></strong>
								</h2>
							</div>
							<p class="page-head__desc"><?php echo $dept_desc?></p>

								<a href="http://www.vtc.edu.hk/admission/tc/programme/s6/higher-diploma/#design" class="btn">入學申請</a>
						</div>
						<div class="page-head__list-holder">
							<!--
              <?php
              if ($programmes_lv2 == '22' || $programmes_lv2 == '7') {
               ?>
                <div class="page-head__remark" style="background-color: <?php echo $deptColorCode?>">
                  <p>由2019年9月1日起，傳意設計及數碼媒體學系將重組為傳意設計學系及數碼媒體學系。</p>
                </div>
              <?php } ?>
          -->
							<div class="programme-info__list">
								<h3 class="programme-info__list-title">高級文憑課程：</h3>
								<?php
									$i = 1;
									foreach($AIP_list as $key => $AIP) {
										$num_padded = sprintf("%02d", $i);
										$programmes_name = $AIP['programmes_name_'.$lang];
										$pid = $AIP['programmes_id'];
										$programmes_name = $AIP['programmes_name_'.$lang];
										$programmes_detail_title_bold = $AIP['programmes_detail_title_bold_'.$lang];
										$programmes_detail_title = $AIP['programmes_detail_title_'.$lang];
										$programmes_detail = $AIP['programmes_detail_'.$lang];

								?>
								<a class="programme-info__list-item <?php echo $programmes_id == $pid?'is-active':''?>" href="#programme-<?php echo $pid?>">
									<span class="programme-info__list-id"><?php echo $num_padded?>.</span> <?php echo $programmes_name?></a>
								<?php $i++;} ?>
							</div>
						</div>
					</div>
					<div class="page-head__blue-bar" style="overflow: hidden;">
						<video autoplay loop muted playsinline webkit-playsinline data-keeplaying="true" poster="<?php echo $img_url?>video/video-smoke.jpg" style="opacity: 0.4;">
		                    <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/mp4" />
		                    <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/webm" />
		                    <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/ogg" />
		                </video>
					</div>
				</div>
				<div class="programme-info__content">
				<?php
					$i = 1;
					foreach($AIP_list as $key => $AIP) {
						$num_padded = sprintf("%02d", $i);
						$programmes_name = $AIP['programmes_name_'.$lang];
						$pid = $AIP['programmes_id'];
						$programmes_name = $AIP['programmes_name_'.$lang];
						$programmes_detail_title_bold = $AIP['programmes_detail_title_bold_'.$lang];
						$programmes_detail_title = $AIP['programmes_detail_title_'.$lang];
						$programmes_detail = $AIP['programmes_detail_'.$lang];
						$programmes_banner = $AIP['programmes_banner'];
						if($programmes_banner && ( file_exists($programmes_path.$pid."/".$programmes_banner) || file_exists(str_replace( "uploaded_files" , "CMS/uploaded_files" , $programmes_path ).$pid."/".$programmes_banner) )){
							if( file_exists($programmes_path.$pid."/".$programmes_banner) ) {
								$programmes_banner = $programmes_url . $pid . "/" . $programmes_banner;
							}
							elseif(file_exists(str_replace( "uploaded_files" , "CMS/uploaded_files" , $programmes_path ).$pid."/".$programmes_banner)) {
								$programmes_banner = str_replace( "uploaded_files" , "CMS/uploaded_files" , $programmes_url ) . $pid . "/" . $programmes_banner;
							}
						}else{
							$programmes_banner ='';
						}

				?>
					<!-- programme item -->
					<div id="programme-<?php echo $pid?>" class="programme-info__item <?php echo $programmes_id == $pid?'is-active':''?>">
						<div class="programme-info__banner">
							<img src="<?php echo $programmes_banner ?>" />
						</div>
						<div class="programme-info__detail">
							<div class="programme-info__head">
								<h2 class="programme-info__title">
									<span class="programme-info__title-num"><?php echo $num_padded?></span>
									<span>
										<strong><?php echo $programmes_detail_title_bold?></strong><?php echo $programmes_detail_title?></span>
								</h2>
								<div class="programme-info__control">
									<div class="programme-info__control-row">
										<a href="#" class="programme-info__btn-prev"></a>
										<a href="#" class="programme-info__btn-next"></a>
									</div>
								</div>
							</div>
							<div class="txt-editor">
								<?php echo $programmes_detail?>


								<?php
									$image_list = DM::findAll($DB_STATUS.'programmes_image', ' programmes_image_status = ? and programmes_image_pid = ?', " programmes_image_seq", array(STATUS_ENABLE,$pid));

									if($image_list){
								?>
								<div class="programme-content__img-slider ani swiper-container">
									<div class="programme-content__img-slides swiper-wrapper">
									<?php
										foreach($image_list as $key => $image) {
											$programmes_image_id = $image['programmes_image_id'];
											$programmes_image_filename = $image['programmes_image_filename'];
											if($programmes_image_filename){
											    $programmes_image_filename = $programmes_url . $pid . "/" . $programmes_image_id . "/" . $programmes_image_filename;
											}
									?>
										<div class="programme-content__img-slide swiper-slide">
											<div class="programme-content__img-slide-inner">
												<img src="<?php echo $programmes_image_filename ?>" />
											</div>
										</div>
									<?php } ?>

									</div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-button-next"></div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<!-- END programme item -->
					<?php $i++;} ?>
				</div>
			</section>
			<!--END Refactoring-->
					<div id="programme">
			<?php/*?>
						<section class="programme-intro">
							<div class="left">
								<h2>Architecture,
									<br>Interior &amp;
									<br>Product Design</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
									natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. lentesque eu, pretium quis, sem. Nulla
									consequat massa quis enim. Donec pedengilltate eget, arcu. </p>
								<div class="blueblock--100"></div>
							</div>
							<div class="right">
								<p class="title">High Diploma :</p>
								<a href="#">
									<span>01.</span> Architectural Design</a>
								<a href="#">
									<span>02.</span> Furniture and Lifestyle Product Design</a>
								<a href="#">
									<span>03.</span> Jewellery and Image Product Design</a>
								<a href="#">
									<span>04.</span> Jewellery Design and Technology</a>
								<a href="#">
									<span>05.</span> Landscape Architecture</a>
								<a href="#">
									<span>06.</span> Product, Interior and Exhibition Design (Subject Group)</a>
								<a href="#">
									<span>07.</span> Stage and Set Design</a>
							</div>
							<div class="blueblock--85--right ani"></div>
							<div class="programme-content-holder ani">
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">01</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>

											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">02</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">03</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">04</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">05</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">06</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<!-- programme-content -->
								<div class="programme-content">
									<div class="banner">
										<img src="<?php echo $inc_root_path ?>images/programme/programme/key-visual.jpg" />
									</div>
									<div class="content">
										<h2 class="programme-title">
											<span class="num">07</span>
											<span>
												<strong>Architectural</strong>Design:</h2>
										<div class="txt">
											<p>
												<strong>This programme lays a foundation of both design philosophy and technical knowledge for students who intend to
													pursue a profession in the architecture, providing the background training in visual communication, basic two
													and three dimensional designs, architectural theory and history, and fundamental building methodology and construction
													technology for the students to practice or to continue with further study in architecture. </strong>
											</p>
											<p>It equips students with the technical aptitude, professional knowledge and skills together with the necessary
												knowledge in building regulations, professional language, leadership qualities, interpersonal skills, and the
												blend of theoretical knowledge and practical application, to enable them to pursue careers in the profession
												of architecture. It also aims to cultivate a holistic approach to design education that encompasses both conceptual
												and professional studies to strengthen graduates' capability of independent decision-making.</p>
											<p>Graduates can apply for admissions to the University of Lincoln (UK) architecture degree Hong Kong programme,
												then further pursue their studies in other relevant Master's degree programmes. Students can be engaged in the
												professional or other related fields of architectural design, research and management. And eventually obtain
												professional qualification as a registered architect.</p>
											<p>Students can participate in international exchange programmes and field trips, to work in close collaborations
												with the industry's top design schools, organizations and professional practices. Destinations include Spain,
												Norway, Sweden, South Korea, China, etc. and through design workshops and collaborative exercises, students are
												exposed to various cultures and different design methods, broadening their horizons.</p>
											<p class="programme-code">Programme code DE114501 – subvented programme Full Time - Semester-based (Normally to be completed in 2 years)</p>
											<div class="programme-content__img-slider ani swiper-container">
												<div class="programme-content__img-slides swiper-wrapper">
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/02.jpg" />
														</div>
													</div>
													<div class="programme-content__img-slide swiper-slide">
														<div class="programme-content__img-slide-inner">
															<img src="<?php echo $inc_root_path ?>images/programme/programme/01.jpg" />
														</div>
													</div>
												</div>
												<div class="swiper-button-prev"></div>
												<div class="swiper-button-next"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- programme-content -->
								<a class="btn-left-arrow--blue" href="#"></a>
								<a class="btn-right-arrow--blue" href="#"></a>
							</div>
						</section>
                <?php*/?>

						<section class="programme-news ani">
              <h2>學系動態</h2>
						<?php
							foreach($news_list as $key => $news) {
								$news_name = $news['news_name_'.$lang];
								$news_id = $news['news_id'];
								$news_type = $news['news_type'];
								$news_date = $news['news_date'];
								$news_thumb = $news['news_thumb'];

								if($lang =="lang1"){
									$news_date_create= date_create($news_date);
									$news_date = date_format($news_date_create, "j F Y");
									$news_short_date = date_format($news_date_create, "M j");
								}else{
									$news_date_create= date_create($news_date);
									$news_date = date_format($news_date_create, "Y年m月d日");
									$news_short_date = date_format($news_date_create, "m月d");
								}

							if ($news_thumb != '' && file_exists($news_path.$news_id."/".$news_thumb)) {
								$news_thumb =$news_url.$news_id."/".$news_thumb;
						?>
							<a class="block" href="news-detail.php?news_id=<?php echo $news_id?>&programmes_id=<?php echo $programmes_id?>">
								<img src="<?php echo $news_thumb?>" />
								<div class="txt">
									<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo $news_date?></p>
									<p><?php echo $news_name?></p>
									<p class="btn-arrow"></p>
								</div>
							</a>

						<?php
							}else{
						?>
							<a class="block" href="news-detail.php?news_id=<?php echo $news_id?>&programmes_id=<?php echo $programmes_id?>">
								<p class="date"><?php echo $news_short_date?></p>
								<div class="table">
									<div class="cell">
										<p>Latest News</p>
										<p><?php echo $news_date?></p>
										<p class="title"><?php echo $news_name?></p>
									</div>
									<p class="btn-arrow"></p>
								</div>
							</a>
						<?php } }?>

						</section>

						<?php if($project_list){ ?>
						<section class="programme-project ani">
							<h2>Projects</h2>
							<div class="programme-project__blocks">
							<?php	$i=1;
								foreach($project_list as $key => $project) {
									$project_name = $project['project_name_'.$lang];
									$project_id = $project['project_id'];
									$project_image1 = $project['project_image1'];
									$project_image2 = $project['project_image2'];
									$project_detail = $project['project_detail_'.$lang];

									if ($project_image1 != '' && file_exists($project_path.$project_id."/".$project_image1)) {
										$project_image1 =$project_url.$project_id."/".$project_image1;
									}else{
										$project_image1 ='';
									}

									if ($project_image2 != '' && file_exists($project_path.$project_id."/".$project_image2)) {
										$project_image2 =$project_url.$project_id."/".$project_image2;
									}else{
										$project_image2 ='';
									}
							?>
							<div class="hidden-block">
								<h3><?php echo $project_name?></h3>
								<div class="hidden">
									<p class="title"><?php echo $project_name?></p>
									<?php if ($project_image1 != '' || $project_image2 != ''){ ?>
									<div class="img-holder">
										<?php if ($project_image1 != ''){ ?>
										<img src="<?php echo $project_image1 ?>" />
										<?php } ?>
										<?php if ($project_image2 != ''){ ?>
										<img src="<?php echo $project_image2 ?>" />
										<?php } ?>
									</div>
									<?php } ?>
									<?php echo $project_detail?>
								</div>
							</div>
							<?php } ?>
							</div>

							<div class="pagination pagination--programme-projects" data-current-page="1" data-total-pages="<?php echo $total_project_page?>">
								<a href="#" class="pagination__btn-prev">上一頁</a>
								<div class="pagination__pages">
									<span>第</span>
									<div class="pagination__current">
										<div class="pagination__current-pg"></div>
									</div>
									<span>頁/共</span>
									<div class="pagination__total"></div>
									<span>頁</span>
								</div>
								<a href="#" class="pagination__btn-next">下一頁</a>
							</div>

						</section>
						<?php } ?>

						<section class="programme-related ani">
							<!--<div class="blue block">
								<div class="table">
									<div class="cell">
										<h3>Staff Directory</h3>
										<p>Lorem ipsum dolor sit amet, con sectetuer adipi scing elit. Aenean com modo ligula eget dolor. Ae orem ipsum dolor
											sita.
										</p>
										<a class="btn-arrow" href="#"></a>
									</div>
								</div>
							</div>-->
							<div class="purple block">
								<div class="table">
									<div class="cell">
										<h3>校友專訪</h3>
										<p><?php echo $alumni['alumni_desc_'.$lang]; ?></p>
										<p class="cell-picture">
											<?php if ($alumni['alumni_image'] != '' && file_exists($alumni_path.$alumni['alumni_id'].'/'.$alumni['alumni_image'])) { ?>
											<img src="<?php echo $alumni_url.$alumni['alumni_id'].'/'.$alumni['alumni_image']; ?>" alt="" />
											<?php } ?>
											<span class="cell-span">
											<?php echo $alumni['alumni_name_'.$lang]; ?></p>
											</span>
										<?php
											$href = '#';

											if ($programmes_lv2 == 5) {
												$href = 'alumni_dfs.php';
											} else if ($programmes_lv2 == 6) {
												$href = 'alumni_aipd.php';
											} else if ($programmes_lv2 == 7) {
												$href = 'alumni_cddm.php';
											} else if ($programmes_lv2 == 8) {
												$href = 'alumni_fid.php';
                      } else if ($programmes_lv2 == 22) {
                        $href = 'alumni_ddm.php';
                      }
										?>
										<a class="btn-arrow btn-arrow-right" href="<?php echo $href; ?>"></a>
									</div>
								</div>
							</div>
							<div class="dark-blue block">
								<div class="table">
									<div class="cell">
										<h3>學生得獎作品</h3>
										<?php /*<p class="date">1 April 2017</p>*/ ?>
										<p><?php echo $student_award['student_award_student_name1_'.$lang]; ?></p>
										<p><?php echo $student_award['student_award_program1_'.$lang]; ?></p>
										<?php
											$href = '#';
											$href ='award.php?id='.$programmes_lv2;

											/*if ($programmes_lv2 == 5) {
												$href = 'award_dfs.php';
											} else if ($programmes_lv2 == 6) {
												$href = 'award_aipd.php';
											} else if ($programmes_lv2 == 7) {
												$href = 'award_cddm.php';
											} else if ($programmes_lv2 == 8) {
												$href = 'award_fid.php';
											} */
										?>
										<a class="btn-arrow btn-arrow-right" href="<?php echo $href; ?>"></a>
									</div>
								</div>
							</div>
							<div class="black block">
								<div class="table">
									<div class="cell">
										<h3>HKDI 設計課程</h3>
										<?php if ($programmes_lv2 != '6' && $AIP_first_programme) { ?>
										<a href="programme.php?programmes_id=<?php echo $AIP_first_programme['programmes_id']; ?>">建築、室內及產品設計</a><br />
										<?php } ?>
										<?php if ($programmes_lv2 != '7' && $CDM_first_programme) { ?>
										<a href="programme.php?programmes_id=<?php echo $CDM_first_programme['programmes_id']; ?>">傳意設計</a><br />
										<?php } ?>
										<?php if ($programmes_lv2 != '5' && $DFS_first_programme) { ?>
										<a href="programme.php?programmes_id=<?php echo $DFS_first_programme['programmes_id']; ?>">基礎設計</a><br />
										<?php } ?>
                    <?php if ($programmes_lv2 != '22' && $DDM_first_programme) { ?>
                    <a href="programme.php?programmes_id=<?php echo $DDM_first_programme['programmes_id']; ?>">數碼媒體</a><br />
                    <?php } ?>
										<?php if ($programmes_lv2 != '8' && $FID_first_programme) { ?>
										<a href="programme.php?programmes_id=<?php echo $FID_first_programme['programmes_id']; ?>">時裝及形象設計</a>
										<?php } ?>
									</div>
								</div>
							</div>
						</section>

					</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
