<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '課程';
$section = 'programmes';
$subsection = 'course';
$sub_nav = 'course';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$id = $_REQUEST['id'] ?? '';
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);
$page_size = 8;

$student_award_lv1 = 1;
$student_award_lv2 = $_REQUEST['student_award_lv2'] ?? '';
//$student_award_list = DM::findAll($DB_STATUS.'student_award', ' student_award_status = ? AND student_award_lv1 = ? AND student_award_lv2 = ?', " student_award_seq DESC", array(STATUS_ENABLE,$student_award_lv1, $student_award_lv2));
$student_award_list = DM::findAllWithPaging($DB_STATUS.'student_award', $page, $page_size,' student_award_status = ? and student_award_award_image1 is not null', " student_award_seq DESC", array(STATUS_ENABLE));

$total_award = DM::count($DB_STATUS.'student_award', ' student_award_status = ? and student_award_award_image1 is not null', array(STATUS_ENABLE));

for ($i = 0; $i < sizeof($student_award_list); $i++)
{
	for ($s = 1; $s <= 4; $s++)
	{
		$student_award_list[$i]['student_award_student'.$s.'_is_show'] = ($student_award_list[$i]['student_award_student_image'.$s] != '' && file_exists($student_award_path.$student_award_list[$i]['student_award_id'].'/'.$student_award_list[$i]['student_award_student_image'.$s]) ? TRUE : FALSE);
	}
}

if ($student_award_lv2 == '' || !$student_award_list) {
   // header("location: ../index.php");
}


$dept = DM::load($DB_STATUS.'dept', $id);
$dept_name = $dept['dept_name_'.$lang];

$first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, $id ));

$breadcrumb_arr['設計課程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr['高級文憑課程'] =$host_name_with_lang.'programmes/#tabs-hd';
$breadcrumb_arr[$dept_name] =$host_name_with_lang.'programmes/programme.php?programmes_id='.$first_programme['programmes_id'];
$breadcrumb_arr['學生得獎作品'] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <script type="text/javascript" src="<?php echo $host_name?>js/lib/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo $host_name?>js/award.js"></script>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div id="award">
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            	<section class="award-title">
                	<div class="title"><h2>學生<br><span class="light">得獎作品</span></h2></div>
                    <div class="desc"><p>HKDI學生的傑出設計意念使得他們贏得無數設計獎項。瀏覽以下更多精彩作品。</p></div>
                </section>
                <section class="award-details">
                	<div class="profile-pic">
				
				<?php foreach ($student_award_list AS $student_award) { ?>
				<?php 	if ($student_award['student_award_type'] == '1') { ?>
				<?php 		if ($student_award['student_award_student1_is_show']) { ?>
				<a class="item-holder" href="#"><img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_student_image1']; ?>" /></a>
				<?php 		} ?>
				<?php 	} else if ($student_award['student_award_type'] == '4') { ?>
				<div class="item-holder">
					<?php for ($i = 1; $i <= 4; $i++) { ?>
					<?php 	if ($student_award['student_award_student'.$i.'_is_show']) { ?>
					<a href="#"><img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_student_image'.$i]; ?>" /></a>
					<?php 	} ?>
					<?php } ?>
				</div>
				<?php 	} ?>
				<?php } ?>
			</div>


            <?php if ($total_award > $page_size) { ?>
            <div class="pagination mobile">
                <?php if($page > 1) { ?>
                    <a href="award.php?page=<?php echo $page-1 ?>" class="pagination__btn-prev">上一頁</a>
                <?php } ?>
                <div class="pagination__pages">
                    <div class="pagination__current">
                        <div class="pagination__current-pg"><?php echo $page?></div>
                    </div>
                    <span>of</span>
                    <div class="pagination__total"><?php echo ceil($total_award / $page_size)?></div>
                </div>
                <?php if ($total_award > $page_size * $page) { ?>
                <a href="award.php?page=<?php echo $page+1 ?>" class="pagination__btn-next">下一頁</a>
                <?php } ?>
            </div>
            <?php } ?>

			<div class="award-details-holder">
				<?php foreach ($student_award_list AS $student_award) { ?>
				<?php 	if ($student_award['student_award_type'] == '1') { ?>
				<?php 		if ($student_award['student_award_student1_is_show']) { ?>
				<div class="details">
					<div class="profile">
						<img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_student_image1']; ?>" />
						<p><strong><?php echo $student_award['student_award_student_name1_'.$lang]; ?></strong><?php echo $student_award['student_award_program1_'.$lang]; ?></p>
					</div>
					<p><?php echo nl2br($student_award['student_award_detail1_'.$lang]); ?></p>
					<hr />
					<h3><strong><?php echo $student_award['student_award_award1_'.$lang]; ?></strong><?php echo $student_award['student_award_award_detail1_'.$lang]; ?></h3>
					<?php if ($student_award['student_award_award_image1'] != '' && file_exists($student_award_path.$student_award['student_award_id'].'/'.$student_award['student_award_award_image1'])) { ?>
					<img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_award_image1']; ?>" />
                    <p><?php echo $student_award['student_award_title1_'.$lang]; ?></p>
					<?php } ?>
				</div>
				<?php 		} ?>
				<?php 	} else if ($student_award['student_award_type'] == '4') { ?>
				<?php 		for ($i = 1; $i <= 4; $i++) { ?>
				<?php 			if ($student_award['student_award_student'.$i.'_is_show']) { ?>
				<div class="details">
					<div class="profile">
						<img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_student_image'.$i]; ?>" />
						<p><strong><?php echo $student_award['student_award_student_name'.$i.'_'.$lang]; ?></strong><?php echo $student_award['student_award_program'.$i.'_'.$lang]; ?></p>
					</div>
					<p><?php echo nl2br($student_award['student_award_detail1_'.$lang]); ?></p>
					<hr />
					<h3><strong><?php echo $student_award['student_award_award'.$i.'_'.$lang]; ?></strong><?php echo $student_award['student_award_award_detail'.$i.'_'.$lang]; ?></h3>
					<?php if ($student_award['student_award_award_image'.$i] != '' && file_exists($student_award_path.$student_award['student_award_id'].'/'.$student_award['student_award_award_image'.$i])) { ?>
					<img src="<?php echo $student_award_url.$student_award['student_award_id'].'/'.$student_award['student_award_award_image'.$i]; ?>" />
                    <p><?php echo $student_award['student_award_title'.$i.'_'.$lang]; ?></p>
					<?php } ?>
				</div>
				<?php 			} ?>
				<?php 		} ?>
				<?php 	} ?>
				<?php } ?>
		    	<?php /*
                        <!-- award-details -->
                        <div class="details">
                        	<div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 1</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 2</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 3</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 4</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 5</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 6</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 7</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 8</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 9</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 10</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 11</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 12</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 13</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 14</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
                        <!-- award-details -->
                        <div class="details">
                            <div class="profile"><img src="<?php echo $inc_root_path ?>images/programme/award/thu01.jpg" /><p><strong>Kenneth Lee</strong>Graduate of Higher Diploma in Product Design</p></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
                            <hr />
                            <h3><strong>HKDA Golden Award 15</strong>Lorem ipasun dolor stisfjd aenean cimmidi ligula</h3>
                            <img src="<?php echo $inc_root_path ?>images/programme/award/work01.jpg" />
                        </div>
                        <!-- award-details -->
			*/ ?>
                    </div>

                    <?php if ($total_award > $page_size) { ?>
                        <div class="pagination desktop">
                            <?php if($page > 1) { ?>
                                <a href="award.php?id=<?php echo $id?>&page=<?php echo $page-1 ?>" class="pagination__btn-prev">上一頁</a>
                            <?php } ?>
                            <div class="pagination__pages">
                                <div class="pagination__current">
                                    <div class="pagination__current-pg"><?php echo $page?></div>
                                </div>
                                <span>of</span>
                                <div class="pagination__total"><?php echo ceil($total_award / $page_size)?></div>
                            </div>
                            <?php if ($total_award > $page_size * $page) { ?>
                            <a href="award.php?id=<?php echo $id?>&page=<?php echo $page+1 ?>" class="pagination__btn-next">下一頁</a>
                            <?php } ?>
                        </div>
                    <?php } ?>


                    <!--<div class="pagination desktop">
                        <div class="pagination__pages">
                            <div class="pagination__current">
                                <div class="pagination__current-pg">1</div>
                            </div>
                            <span>of</span>
                            <div class="pagination__total">19</div>
                        </div>
                        <a href="index.php?tab=all&amp;page=2" class="pagination__btn-next">Next</a>
                    </div>-->
                </section>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
