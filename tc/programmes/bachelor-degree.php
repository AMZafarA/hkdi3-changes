<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "學士學位課程";
$section = 'programmes';
$subsection = 'bachelor-degree';
$sub_nav = 'bachelor-degree';


$breadcrumb_arr['設計課程'] =$host_name_with_lang.'programmes/#tabs-dg';
$breadcrumb_arr['學位課程'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$sql = "select DISTINCT universities_id,universities_image from ".$DB_STATUS."top_up_degree,".$DB_STATUS."universities where universities_status = '1' and top_up_degree_status = '1' and top_up_degree_univeristy_id=universities_id order by universities_seq desc";

$result = DM::execute($sql);

/*foreach($result as $row){
    echo "aaaaa".$row['universities_name_lang1'];
}*/

$universities_list = DM::findAll($DB_STATUS.'universities', ' universities_status = ?', " universities_seq desc", array(STATUS_ENABLE));

foreach($universities_list as $key => $universities) {
    $degree_list = DM::findAll($DB_STATUS.'top_up_degree', ' top_up_degree_status = ? and top_up_degree_univeristy_id = ?', " top_up_degree_seq desc", array(STATUS_ENABLE,$universities['universities_id']));

    foreach($degree_list as $key1 => $degree) {
        $degree_arr[$universities['universities_id']][] =$degree['top_up_degree_id'];
    }
}

$degree_list = DM::findAll($DB_STATUS.'top_up_degree', ' top_up_degree_status = ? ', " top_up_degree_seq desc", array(STATUS_ENABLE));



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/bachelor-degree.css" type="text/css" />
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
                <div class="<?php echo $subsection ?> bd-info">
                	<section class="bachelor-degree-info bd-info__college">
                    	<h2>學士學位課程</h2>
                        <p>HKDI為高級文憑畢業生提供<?php echo count($degree_list) ?>個銜接學士學位課程，僅須額外一年時間便可修畢學士學位課程。課程由HKDI聯同六所英國著名大學共同提供，請點選下列課程或學校以了解更多詳情。</p>
                        <p>同時，VTC豁下之香港高等教育科技學院亦提供四年制學士學位課程。</p>
                        <?php
                            foreach($result as $key =>$row){
                                $universities_id = $row['universities_id'];
                                $target = implode(",", $degree_arr[$universities_id]);
                                $universities_image = $row['universities_image'];
                                if($universities_image){
                                    $universities_image = $universities_url . $universities_id . "/" . $universities_image;
                                }
                        ?>
                        <a data-target="<?php echo $target?>" href="#"><img src="<?php echo $universities_image?>" /></a>
                        <?php } ?>

                    </section>


                	<section class="bachelor-degree-list">
                    <p class="title">課程：</p>
                    <ol class="bd-info__list">
                    <?php
                        foreach($degree_list as $key1 => $degree) {
                           $top_up_degree_id = $degree['top_up_degree_id'];
                           $top_up_degree_name = $degree['top_up_degree_name_'.$lang];
                    ?>
                        <li class="bd-info__item"  data-programme-id="<?php echo $top_up_degree_id?>"><a href="bachelor-degree-detail.php?degree_id=<?php echo $top_up_degree_id?>"><?php echo $top_up_degree_name?></a></li>
                    <?php } ?>
                    </ol>
                    </section>
                </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
