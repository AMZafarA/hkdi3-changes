<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '關於';
$section = 'about';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$breadcrumb_arr['關於我們'] =''; 
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
       		<link rel="stylesheet" href="<?php echo $host_name?>css/industrial-collaborations-details.css" type="text/css" />
		<script src="<?php echo $host_name?>js/about.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access">
			<?php echo $page_title?>
		</h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>關於
								<strong>HKDI</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs page-tabs--no-bottom-pad">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-about-us" class="page-tabs__btn is-active">
							<span>關於</span>
						</a>
						<a href="#tabs-about-campus" class="page-tabs__btn" id="btn-tabs-about-campus">
							<span>關於校園</span>
						</a>
						<a href="#tabs-publication" class="page-tabs__btn" id="btn-tabs-publication">
							<span>刊物</span>
						</a>
						<a href="#tabs-experience" class="page-tabs__btn" id="btn-tabs-experience">
							<span>體驗設計</span>
						</a>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__bg-deco"></div>
					<div class="page-tabs__wrapper">
						<div id="tabs-about-us" class="page-tabs__content is-active">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>關於</strong>HKDI</h2>
										</div>
										<div class="txt-editor">
											<h4>香港知專設計學院 (HKDI) 為職業訓練局轄下學院，致力提供優質教育，建構知識和發展專業，為香港創意工業培育優秀的設計人才。</h4>
											<p>HKDI 擁有多年的設計教育經驗，提供共 20 個設計課程，科目涵蓋四主個主要設計學系，並將其優勢融合，包括建築、室內及產品設計、傳意設計及數碼媒體、基礎設計、以及時裝及形象設計。配合靈活升學及高等教育需求，HKDI 學生於修畢兩年制的高級文憑後，可直接升讀由英國著名大學頒授的一年制學士學位課程。</p>
											<p>HKDI 採取「思考與實踐」的教育方針，開辦與時並進的課程，並積極與國內外的設計學院及業界保持緊密合作。我們為學生提供獲得實踐經驗及參與國際交流計劃的機會，以擴闊學生視野，加強學生的創新思維和社會敏感度。</p>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="about-campus">
													<strong>得獎建築 &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="publication">
													<strong>HKDI 刊物 &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="experience">
													<strong>體驗設計 &gt;</strong>
												</a>
											</div>
											<!--<div class="btn-row btn-row--al-hl">
												<a href="../edt/edt-info-day.php" class="btn">
													<strong>HKDI資料日 &gt;</strong>
												</a>
											</div>-->
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/about_image.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<div class="industrial-collaborations-details">
								<section class="membership">
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											<strong>專業學會及機構</strong></h2>
									</div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											專業學會：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ico-D_120x120.png" />
						                        <div class="txt">
						                            <p><strong>ICO-D</strong></p>
						                            <p>國際設計社團組織</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IDA_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IDA</strong></p>
						                            <p>國際設計聯盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IFFTI_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFFTI </strong></p>
						                            <p>國際服裝技術學院基金會</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ifi_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFI</strong></p>
						                            <p>國際室內建築師設計師團體聯盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IIID_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IIID</strong></p>
						                            <p>國際資訊設計研究所</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>TDAA</strong></p>
						                            <p>亞洲設計連</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/WDO_120x120.png" />
						                        <div class="txt">
						                            <p><strong>WDO</strong></p>
						                            <p>世界設計組織</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											學術機構：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/Cumulus_120x120.png" />
						                        <div class="txt">
						                            <p><strong>CUMULUS</strong></p>
						                            <p>國際大學協會及藝術，設計與媒體學院</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											合作機構：</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/DESIS_120x120.png" />
						                        <div class="txt">
						                            <p><strong>DESIS Network</strong></p>
						                            <p>社會創新與可持續設計國際聯盟</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>Design ED Asia</strong></p>
						                            <p>設計教育亞洲會議</p>
						                        </div>
						                    </div>
								</section>
							</div>
							<section class="sec-basic sec-basic--white">
								<div class="page-tabs__bg"></div>
								<div class="txt-editor">
									<h3 class="txt-editor__title-light">
										<strong>執行幹事序言</strong></h3>
									<h4>
										<strong>我謹代表香港知專設計學院衷心歡迎您！</strong>
									</h4>
									<div class="txt-editor__grp">
										<h4>
											<strong>探索機會‧成功在望</strong>
										</h4>
										<p>香港知專設計學院透過全面和富於創意的設計課程，為學生提供一個探索設計藝術，發揮自我潛能的學習機會。</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>培養人才‧成就未來</strong>
										</h4>
										<p>學院致力培養優秀的設計人才，我們的導師充滿熱誠，具豐富經驗，為大家提供一個全方位，又富創意的互動學習環境。我們致力開拓學生的想像力、創造力、溝通技巧和自信心，亦十分注重課程與業界的銜接。課程得到業界的參與及合作，使學生所學所識更切合行業的實際需求。課程的質素保證得到香港學術評審局認可。</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>升學就業‧成功之道</strong>
										</h4>
										<p>僱主高度肯定我們的課程。我們的畢業生在不同行業均取得一定成就。同學更可於畢業後報讀本校與海外大學學位銜接課程，在一至兩年內完成學業，達成繼續升學的理想，開創未來的事業。</p>
									</div>
									<div class="txt-editor__grp">
										<p>我誠意邀請您加入香港知專設計學院，為您未來的成功事業而努力。</p>
									</div>
									<br>
									<div class="txt-editor__grp">
										<p>
											職業訓練局執行幹事</br>
											尤曾家麗女士, GBS, JP
										</p>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-about-campus" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>設計</strong>概念</h2>
										</div>
										<div class="txt-editor">
											<h4>香港知專設計學院的校舍致力營造一個有利於學習和創新探索的開放且有動感的環境。學院設有先進的工作坊、製作設施及學習資源中心，為高質素的設計教育提供優良配備。</h4>
											<p>校舍的設計由法國高地菲及夥伴建築公司 (Coldéfy & Associés) 完成，主題是「一張白紙」，於 2006/2007 年度香港知專設計學院國際建築設計比賽中奪得冠軍。</p>
											<p>這張「白紙」是創造力的隱喻表達。它在空中連繫院校所有其他用作教學用途的大樓，展示出 HKDI 跨部門協作的特點。創新、明亮和通透的建築設計，將多種不同的場所融合在一起，並且可讓人從外部輕鬆辨別各個功能單元。</p>
											<p>整幢學院由相互聯繫和貫通的基本元素組成，包括地面設計大道、平台、「空中之城」、教學大樓及觀景屋頂，擁有永不過時的建築設計，同時體現了學院促進協同合作、透明公開及交流互動的目標。此種設計亦激勵和啟發著學生成長為未來的設計師。</p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-design-concept-1.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>教育</strong>設施</h2>
										</div>
										<div class="txt-editor">
											<h4>HKDI 擁有逾百個工作坊和工作室，可以滿足不同設計學科學生的需要。此外，我們還設有四個知識中心，倡導跨學科設計教育並鼓勵學生參與創新社會研究計劃，以推進設計思維。</h4>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cill.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">語文自學中心 (CILL)</h3>
													<p class="thumb-desc__desc">語文自學中心提供一個舒適且親切的環境，讓學生學習並提升語文水平。中心提供獨立學習資源，包括影音光碟、書籍、雜誌及桌上遊戲，亦安排多元化的學習活動，如工作坊、派對、表演及比賽。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_ml.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">媒體研究所</h3>
													<p class="thumb-desc__desc">媒體研究所致力於孕育和融合創新意念及媒體科技，並推動教育、應用研究、專業培訓及行業應用的相互作用。媒體研究所設計部有由平面設計、二維或三維動態視像及產品等專業設計服務，彰顯媒體的物質性展現方法。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cimt.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">知專設創源 (CIMT)</h3>
													<p class="thumb-desc__desc">知專設創源是一個全面的資料庫及互動學習平台，旨在促進學生、設計教育界、設計師及生產商在物料知識及相關應用方面的交流。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_lrc.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">學習資源中心 (LRC)</h3>
													<p class="thumb-desc__desc">學習資源中心存有 75,000 多件館藏，包括書籍、期刊及影音資訊等。不過，中心並不止是圖書館，同時亦是一個充滿互學互動的地方，鼓勵學生一同研習、討論課題。中心同時設有一個長時間開放的專門空間 —— Zone 24，供學生學習及討論專題習作並在此基礎上展開合作之用。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_fa.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">時裝資料館</h3>
													<p class="thumb-desc__desc">時裝資料館透過有代表性的時裝珍藏，作為教學及學習工具。全新修葺的資料館總面積為 360 平方米，用以展示約 1,500 件擁有悠久歷史的時尚珍藏。參觀者透過全新的互動平台，觀賞這批珍藏的資料，深入了解及體驗時裝設計、時尚文化和時裝界的發展歷史。</p>
												</div>
											</div>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>運動</strong>設施</h2>
										</div>
										<div class="thumb-desc">
										<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_swimming_pool.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">游泳池</h3>
													
													<p class="thumb-desc__desc">
													<span style="color:#ffffff;font-weight:bold;">
													<u>游泳池冬季休池</u><br>
													冬季本院游泳池將於 2018年 11月 1日至 2019年 4月初暫停開放。
													</span>
													</p>													
													
													<p class="thumb-desc__desc">4月至10月期間逢星期二至日開放 。<br>
													<strong>學院使用時段:</strong><br>
													星期二至五 | 0930 – 2100 (1330 – 1430 除外)<br>
													星期六、日及公眾假期 | 0800 – 1100; 1300 – 1600; 1800 – 2100<br><br>
													
													<strong>公眾人士使用時段 (收費):</strong><br>
													星期二至五 | 1800 – 2100 <br>
													星期六、日及公眾假期 | 0800 – 1100; 1300 – 1600; 1800 – 2100<br>
													游泳池入場費: 成人港幣20元 | 12歲以下小童及60歲以上長者港幣10元<br>
													查詢: 3928 2222 / 3928 2000 (辦公時間) 3928 2999 (非辦公時間及惡劣天氣)
													</p>

												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_basketball_court.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">籃球場</h3>
													
													<p class="thumb-desc__desc">
													<span style="color:#ffffff;font-weight:bold;">
													<u>學院籃球場恢復開放</u><br>
													本院籃球場緊急維修工程經已完成，並由2018年11月26日 (星期一) 起恢復正常開放時間。
													</span>
													</p>
													
													<p class="thumb-desc__desc">
													<strong>學院使用時段:</strong><br>
													星期一至五 | 0830 – 1900 <br>
													星期六 | 0930 – 1200 <br><br>
													
													<strong>公眾人士使用時段 (收費):</strong><br>
													星期一至五 | 1900 – 2100 <br>
													星期六 | 1300 – 2100 <br>
													星期日及公眾假期 | 0900 – 2100<br>
													籃球場租用收費: 每小時港幣80元<br>
													查詢: 3928 2222 / 3928 2000 (辦公時間) 3928 2999 (非辦公時間及惡劣天氣)
													</p>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width">
								<div class="content-wrapper">
									<div class="article-detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>其他</strong>設施</h2>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-1.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">VTC 綜藝館</h3>
													<p class="thumb-desc__desc">VTC 綜藝館佔地 963 平方米，可容納 700 多人。場地備有特別設計的音響設施，藉以提升各種不同表演、會議以及其他活動的音響效果。場館亦設有殘疾人士通道，是學院與社區互動共融的中心。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-2.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">體驗中心</h3>
													<p class="thumb-desc__desc">體驗中心為多用途空間，佔地 170 平方米，適合舉辦小型活動及展覽。空間有兩面均為落地玻璃窗，連接設計大道。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-3.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">HKDI Gallery</h3>
													<p class="thumb-desc__desc">HKDI Gallery 佔地 600 平方米，提供寬廣的展覽空間以供學院舉行設計展覽。場館亦對外開放，可用於舉辦展覽、貿易和行業相關活動，以及展出學生作品。</p>
													<!--<a href="#" class="thumb-desc__link">HKDI Gallery Website ></a>-->
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-4.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">設計大道</h3>
													<p class="thumb-desc__desc">全長達 125米 的設計大道貫通學院地面，連接兩旁的綜藝館、展覽場地及公共設施。設計大道被視為校園的「活動空間」開放而闊偌的通道不但為校園營造空間感，更利用兩旁的展覽及演藝場地匯聚設計氣息，為學生提供活動和誘發靈感的互動空間。</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-5.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">d-mart</h3>
													<p class="thumb-desc__desc">d-mart 佔地 1,040 平方米，提供寬廣的展覽空間以供學院舉行設計展覽，場館亦對外開放，可用於舉辦展覽、貿易和行業相關活動，以及展出學生作品。</p>
												</div>
											</div>
											
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-multi-purpose-hall.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">多用途會堂</h3>
													<p class="thumb-desc__desc">多用途會堂是位於香港知專設計學院地下的多用途場地，佔地338.6 平方米。場地主要供學院之學生作教學用途，亦會開放舉辦各類型的講座及工作坊。如有任何查詢，請電郵: <a href="mailto:hkdi-booking@vtc.edu.hk">hkdi-booking@vtc.edu.hk</a>。</p>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="booking_enquiry">
								<div class="article-detail article-detail--green" name="booking">
									<div class="content-wrapper">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>租借</strong>場地</h2>
										</div>
										<div class="txt-editor">
											<h4>歡迎使用網上租用場地查詢服務, 於遞交申請前，請細閱申請人須知及有關使用條款。</h4>
										</div>
										<div class="contact-form">
											<form class="contact-form__form form-container" id="form-booking" action="<?php echo $host_name; ?>send-form.php" method="POST">

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>租借日期</strong>
															</div>
															<div class="field__date-fields">
																<div class="field__date-field">
																	<div class="field__date-label">由:</div>
																	<input type="text" name="booking_from_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
																<div class="field__date-field">
																	<div class="field__date-label">至:</div>
																	<input type="text" name="booking_to_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>場地租借</strong>
															</div>
															<div class="field__checkbox-holder">
																<div id="booking-checks" class="field__checkbox-fields form-check--empty">
																	<div class="custom-checkbox">
																		<input id="booking_checkbox1" data-limit="289" type="checkbox" name="booking_lecture_theatre_289" value="y" />
																		<label for="booking_checkbox1">演講廳 (289 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox2" data-limit="120" type="checkbox" name="booking_lecture_theatre_120" value="y" />
																		<label for="booking_checkbox2">演講廳 (120 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox3" data-limit="80" type="checkbox" name="booking_function_room" value="y" />
																		<label for="booking_checkbox3">	多用途活動室 (A002-A003) (80 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox4" data-limit="740" type="checkbox" name="booking_vtc_auditorium" value="y" />
																		<label for="booking_checkbox4">VTC 綜藝館 (740 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox5" type="checkbox" name="booking_hkdi_gallery" value="y" />
																		<label for="booking_checkbox5">HKDI Gallery</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox6" type="checkbox" name="booking_dmart" value="y" />
																		<label for="booking_checkbox6">d-mart</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox7" data-limit="50" type="checkbox" name="booking_experience_center" value="y" />
																		<label for="booking_checkbox7">體驗中心 (50 座位)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox8" type="checkbox" name="booking_design_beulevard" value="y" />
																		<label for="booking_checkbox8">設計大道</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>


													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>場地租借用途</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="booking_purpose" class="is-black form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="number" name="booking_participants" placeHolder="預計參加人數" class="form-check--empty form-check--max" />
															<span class="field__inline-txt booking-max-num-holder is-hidden">(最多：<span id="booking-max-num"></span>人)</span>
														</div>
													</div>
												</div>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">申請人詳情</h3>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_organisation" placeHolder="機構" class="form-check--empty" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_contact_person" placeHolder="聯絡人" class="form-check--empty" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_tel" placeHolder="電話" maxlength="25" class="form-check--empty form-check--tel" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_fax" placeHolder="傳真" maxlength="25" class="form-check--option form-check--tel" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_email" placeHolder="電郵" class="form-check--empty form-check--email" />
														</div>
													</div>
												</div>
												<br>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">驗證碼</h3>
												</div>
												<p class="contact-form__desc">請輸入圖中的數字</p>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="captcha">
																<div class="captcha__img">
																	<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																</div>
																<div class="captcha__control">
																	<div class="captcha__input">
																		<input type="text" name="captcha" class="form-check--empty" />
																	</div>
																	<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">更新驗證碼</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="contact-form__notes">
													<div class="contact-form__notes-title">申請人須知:</div>
													<ol class="contact-form__notes-list">
														<li>租借申請一般須於活動日期最遲一個月前或最早四個月前提交。</li>
														<li>申請人必須為香港註冊「公司」或「團體」，以個人名義申請者恕不受理。</li>
														<li>如對本表格有任何疑問，請致電3928 2761。</li>
														<li>如有特別原因須預早籌備活動，可連同有關活動內容及計劃書，透過<a href="mailto:hkdi-booking@vtc.edu.hk" class="underline-link">電郵</a>申請。</li>
													</ol>
												</div>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<br>
															<div class="custom-checkbox form-check--tnc">
																<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty" />
																<label for="contact_book">本人已細閱及同意「VTC Hire of Accommodation in Council Premises Term and Condition of Hire」
																	<a href="<?php echo $img_url?>about/VTC_Hire_of_Accommodation_in_Council_Premises_TC.PDF" class="underline-link" target="_blank">(PDF)</a> 及
																	 「Guidelines for Event Organisation in HKDI & IVE(LWL)」<a href="<?php echo $img_url?>about/Guidelines_for_Event_Organisation_in_HKDI_ IVE(LWL).pdf" class="underline-link" target="_blank">(PDF)</a> 各項條款。
																</label>
															</div>
														</div>
													</div>
												</div>

												<div class="btn-row">
													<div class="err-msg-holder">
														<p class="err-msg err-msg--empty">必須填寫</p>
														<p class="err-msg err-msg--tel">必須填寫電話號碼</p>
														<p class="err-msg err-msg--email">必須填寫</p>
														<p class="err-msg err-msg--email-format">電郵格式無效</p>
														<p class="err-msg err-msg--phone">必須填寫電話號碼</p>
														<p class="err-msg err-msg--phone-format">電話號碼格式無效</p>
														<p class="err-msg err-msg--tnc">必須接受條文及細則</p>
														<p class="err-msg err-msg--captcha">必須填寫驗證碼</p>
														<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														<p class="err-msg err-msg--max">Exceeded the max. no. of seats.</p>
														<p class="err-msg err-msg-datepicker">Invalid date range </p>
													</div>
													<div class="btn-row btn-row--al-hl">
														<a href="#" class="btn btn-submit">
															<strong>送出</strong>
														</a>
														<a href="#" class="btn btn-reset">
															<strong>重設</strong>
														</a>
													</div>
												</div>
												<input type="hidden" name="lang" value="<?php echo $lang?>">
											</form>
											<div class="contact-form__success-msg">
												<h3 class="desc-subtitle">Thank you!</h3>
												<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-publication" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__head">
									<h2 class="article-detail__title">
										<strong>關於</strong>HKDI</h2>
								</div>
								<div class="txt-editor">
									<h4>
										<strong>香港知專設計學院 (HKDI) 為職業訓練局轄下學院，致力提供優質教育，建構知識和發展專業，為香港創意工業培育優秀的設計人才。</strong>
									</h4>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>課程概覽</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/HKDI_prospectus_201819.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/HKDI_prospectus_201819_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>香港知專設計學院2018/19課程概覽</strong>
											</h3>
											<!--<p class="publication-list__item-desc">Merry So</p>-->
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>SIGNED 雜誌</strong>
									</h3>
									<!--<div class="publication-list__items">
										<?php for ($i = 17; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>ISSUE No. <?php echo $i; ?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<?php } ?>
									</div>-->
									<div class="publication-list__items">
									<?php
										$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));

										foreach ($issue_list AS $issue) {
											$issue_id = $issue['issue_id'];
											$issue_cover_image = $issue['issue_cover_image'];
											$issue_name = $issue['issue_name_'.$lang];

											if ($issue['issue_cover_image'] != '' && file_exists($issue_path.$issue_id."/".$issue_cover_image)) { 
												$issue_cover_image = $issue_url.$issue_id."/".$issue_cover_image;
											}
									?>

										<a href="<?php echo $host_name_with_lang."news/publication.php?issue_id=".$issue_id?>" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $issue_cover_image?>" alt="" />
											<h3 class="publication-list__item-title">
												<strong><?php echo $issue_name?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									<?php for ($i = 14; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第<?php echo $i; ?>期</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>學術及研究</strong>
									</h3>
									<div class="publication-list__items">
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Conversation from Open Design Forum 2014 by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Conversation from Open Design Forum 2014 by DESIS Lab</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Cumulus Hong Kong 2016 Working Papers by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Cumulus Hong Kong 2016 Working Papers by HKDI</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Fine Dying  by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Fine Dying  by DESIS Lab</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Deisgn for Action by DESIS Lab_compressed.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Deisgn for Action by DESIS Lab_compressed</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Design for E-very-thing by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Design for E-very-thing by HKDI</strong>
											</h3>
										</div>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>得獎作品集 DAZZLE</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/DZ_design_content_web.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle3_cover.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第三期, 2016</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/dazzle2013.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle2_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第二期, 2013</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/Dazzle.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>第一期, 2011</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-experience" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="txt-editor">
											<h4>
												<strong>我們為中學生提供量身定製的活動時間表，以便讓他們更好地了解 HKDI 的設計教育，以及在設計領域可能獲得的職業前景。</strong>
											</h4>
										</div>
									</div>
								</div>
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>學院</strong>導賞團</h2>
										</div>
										<div class="txt-editor">
											<p>
												學院導賞團可以讓同學感受到學習環境的氣氛，並詳細了解 HKDI。參觀的學生將會遊覽學院的設施，包括知識資源中心、不同設計學系的工作坊和工作室、學生作品，及 HKDI 與國際設計藝術夥伴合辦的季節性展覽等。
											</p>
											<p>每個導賞團大約 45 分鐘，每團參觀人數為 20 至25 位學生，並由最少一名老師或負責人陪同參加。我們可以安排特定設計工作坊或設計課程的參觀路線，但需學校至少提前三個星期提交預約申請。</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>導引</strong>課程</h2>
										</div>
										<div class="txt-editor">
											<p>
												每個設計學系都可提供獨一無二的導引課程，讓同學嘗試體驗於 HKDI 學習，從而令他們更專注於不同的設計過程。傳意設計及數碼媒體學系將介紹三維打印或 360 度拍攝手法；基礎設計學系將示範如何使用日常電子裝置來創作藝術；時裝及形象設計學系可讓學生體驗時裝、形象和品牌的設計過程；而產品及室內設計學系將教授如何利用三維空間設計理論來打造室內及產品設計。
											</p>
											<p>每個導引課程大約 1.5 小時，每個課程的參加人數為 18 至 25 名學生，並由最少一名老師或負責人陪同參加。學校應至少提前一個月申請，另外導引課程內容以 HKDI 學院的工作室情況而定。 
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>升學</strong>講座</h2>
										</div>
										<div class="txt-editor">
											<p>在升學講座中，HKDI 講師將會分享設計課程資訊、入學要求及設計行業的就業出路。講師亦會透過學生作品和業界合作，分享其有趣的教學方法及知識交流課程。
											</p>
											<p>有興趣的學校可以選擇心儀的學系課程，每個講座為大約 45 至 60 分鐘，此時間可進一步協商。我們亦可安排展出 HKDI 學生作品，但須按實際情況而定。
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>校友</strong>分享</h2>
										</div>
										<div class="txt-editor">
											<p>得到學界和業界認可的 HKDI 傑出校友，活動當日會參與分享及問答環節。通過互動和聊天，讓學生透過年齡相仿而有經驗的畢業生去認識 HKDI 的學習環境、就業出路以及制定生涯規劃方案。
											</p>
											<p>有興趣的學校可以選擇心儀的學系課程，分享會為大約 40 分鐘，接著會有問答環節。一個分享會最多可安排 2 名校友。建議由學校安排一位老師作分享會的主持人，帶領學生提出相關和實用的問題。
											</p>
											<p>有興趣的教師應於擬議參觀日前三週填寫並提交下列表格。提交申請表格前，請仔細閱讀《申請人須知》(Notes to Applicants)。</p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-taster.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="guided_tour_application_form" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>學院參觀申請表格</strong></h2>
										</div>
										<div class="txt-editor">
											<h4>歡迎參觀香港知專設計學院，了解本院提供之課程及服務，請於提交學院參觀申請預約前，先細讀申請須知各項細則。 請於計劃參觀日期起計，最少三個星期前提交預約申請表格。(電郵：eao-dilwl@vtc.edu.hk) 本院將收到申請團體/申請者的預約申請後之七個工作天內發電郵確認。</h4>
											<div class="contact-form contact-form--txt-white">
												<form class="contact-form__form form-container" id="form-application">
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>參觀日期</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date1" placeholder="首選" class="form-check--empty datepicker datepicker--application" />
																	</div>
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date2" placeholder="次選" class="form-check--empty datepicker datepicker--application" />
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>參觀時間</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_visit_time">
																				<option value="10:00 - 11:00">10:00 - 11:00</option>
																				<option value="11:00 - 12:00">11:00 - 12:00</option>
																				<option value="14:30 - 15:30">14:30 - 15:30</option>
																				<option value="15:30 - 16:30">15:30 - 16:30</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>參觀人數</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="number" name="application_form_visitors_no" placeholder="參觀人數" class="form-check--empty form-check--num" />
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>參觀者名單，訪客背景資料</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_list" class="form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>語言</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_list_lang">
																				<option value="Cantonese">粵語</option>
																				<option value="English">英語</option>
																				<option value="Chinese">普通話</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>到訪目的</strong>
																</div>
																<div class="field__checkbox-holder">
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj1" type="radio" name="application_form_obj_academic_collaboration" value="y" checked/>
																			<label for="apply_obj1">學術交流</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj2" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj2">業界/機構參觀</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj3" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj3">學校導賞團，註明學生就續級別</label>
																			<div data-ref="apply_obj3" class="radio-subs radio-child-container">
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj4" type="checkbox" name="application_form_obj_form4" value="y" />
																					<label for="apply_obj4">中四</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj5" type="checkbox" name="application_form_obj_form5" value="y" />
																					<label for="apply_obj5">中五</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj6" type="checkbox" name="application_form_obj_form6" value="y" />
																					<label for="apply_obj6">中六</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<label for="apply_others">其他</label>	
																					<input type="text" name="application_form_obj_others" class="custom-checkbox__txt-input" />
																				</div>
																			</div>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj7" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj7">其他 (請註明):</label>
																			<span  data-ref="apply_obj7" class="radio-subs">
																			<input type="text" name="application_form_obj_others_desc" class="custom-checkbox__txt-input form-check--empty" />
																			</span>
																		</div>
																</div>
															</div>
														</div>
													</div>

													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>申請團體/申請者資料</strong>
														</h3>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_organisation" placeHolder="申請團體名稱" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<div class="custom-select">
																	<select name="application_form_title">
																		<option>稱謂</option>
																		<option value="Mr">先生</option>
																		<option value="Mrs">女士</option>
																		<option value="Ms">太太</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact_person" placeHolder="聯絡人姓名" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_position" placeHolder="職位名稱" class="form-check--empty" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact" placeHolder="聯絡電話" class="form-check--empty form-check--tel" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_mobile" placeHolder="流動電話" class="form-check--option form-check--tel" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_email" placeHolder="電郵" class="form-check--empty form-check--email" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>備註</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_remarks" class="form-check--option" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<br>
													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>驗證碼</strong>
														</h3>
													</div>
													<p class="contact-form__desc">請輸入圖中的數字</p>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="captcha">
																	<div class="captcha__img">
																		<img id="application_form_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																	</div>
																	<div class="captcha__control">
																		<div class="captcha__input">
																			<input type="text" name="captcha" class="form-check--empty" />
																		</div>
																		<a href="#" class="captcha__btn" onClick="document.getElementById('application_form_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">更新驗證碼</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="contact-form__notes">
														<div class="contact-form__notes-title">申請須知:</div>
														<p class="contact-form__notes-desc">(提交學院參觀申請預約前，請先細讀以下各項)</p>
														<ol class="contact-form__notes-list">
															<li>申請團體/申請者須於計劃參觀日期起計，最少三個星期前提交預約申請表格。(電郵 : <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>)</li>
															<li>香港知專設計學院及香港專業教育學院(李惠利)歡迎公眾人士參觀本校，惟校園導賞一般僅提供予學校、學院、團體及業界組織，讓他們認識本校校園設施、了解本校提供之課程、學生生活、業界和院校的合作及最新發展。</li>
															<li>如申請學校導賞團，請註明學生就續級別，例如：中五、中六。</li>
															<li>申請團體/申請者必須連同申請表格，提交參觀者名單/訪客背景資料。</li>
															<li>如申請團體/申請者已曾就同一次參觀透過此系統又或電郵進行預約，請勿重覆遞交申請表。</li>
															<li>學院參觀或導賞團於星期六、日及公眾假期停止服務。</li>
															<li>若懸掛黃色暴雨警告或三號颱風訊號，所有學院參觀或導賞團將會取消。</li>
															<li>申請團體/申請者提供的個人資料將由本院用於處理參觀申請及內部報告。</li>
															<li>本院收到申請團體/申請者的預約申請後，將發電郵確認。如提交申請及所有所需文件後七個工作天仍未收到回覆確認，請即聯絡本院外事處，電話(852) 3928 2561 或 電郵: <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>。</li>
															<li>香港知專設計學院及香港專業教育學院(李惠利)對申請事宜保留一切權利。如發現申請參觀之團體/申請者呈報失實資料，學院有權拒絕或取消其申請。</li>
															<li>如欲取消已申請之學院參觀，申請團體/申請者須於擬定參觀日期兩個工作天前以書面或電郵方式通知本院外事處職員，以便安排取消有關申請。</li>
														</ol>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<br>
																<div class="custom-checkbox form-check--tnc">
																	<input id="apply_tnc" type="checkbox" name="apply_tnc" class="form-check--empty " />
																	<label for="apply_tnc">本人同意及接受「學院參觀 – 申請須知」內的條文及細則。</label>
																</div>
															</div>
														</div>
													</div>

													<div class="btn-row">
														<div class="err-msg-holder">
															<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
															<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														</div>
														<div class="btn-row btn-row--al-hl">
															<a href="#" class="btn btn-submit">
																<strong>送出</strong>
															</a>
															<a href="#" class="btn btn-reset">
																<strong>重設</strong>
															</a>
														</div>
													</div>
													<input type="hidden" name="lang" value="<?php echo $lang?>">
												</form>
												<div class="contact-form__success-msg">
													<h3 class="desc-subtitle">Thank you!</h3>
													<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-professional-membership" class="page-tabs__content">
							
						</div>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>
				<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
