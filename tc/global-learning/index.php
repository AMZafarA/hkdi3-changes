﻿<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "國際交流";
$section = 'international';
$subsection = 'international';
$sub_nav = 'international';

$breadcrumb_arr['國際交流'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$exchange_programme_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'P'));
$incoming_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'I'));
$outgoing_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'O'));
$scholarship_list = DM::findAll($DB_STATUS.'international','international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'S'));

//$scholarship_list = DM::findAll($DB_STATUS.'international_image', 'international_image_status = ? AND international_image_pid = ?', 'international_image_seq', array(STATUS_ENABLE, '1'));

$master_lecture_list = DM::findAll($DB_STATUS.'master_lecture', 'master_lecture_status = ?', 'master_lecture_seq', array(STATUS_ENABLE));
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

    <head>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/international.css" type="text/css" />

		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
		<style>
		.programme-project .hidden-block h3:after{
			display:none;
		}
    .underline-link{
      text-decoration: underline !important;
    }
		</style>
    </head>

    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access">
            <?php echo $page_title?>
        </h1>
        <main>
            <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            <div class="<?php echo $subsection ?>">
                <section class="international-info">
                    <h2>國際交流</h2>
                    <h4>培育具國際視野的設計師</h4>
                    <p>HKDI 致力培育學生的國際視野及文化觸覺，我們<a href="javascript:scrollToElemTop($('#international-academic-partners'), 300);"><u>全球知名的設計學院</u></a>合作，並提供<a href="javascript:scrollToElemTop($('#scholarship'), 300);" class="underline-link">學術交流獎學金</a>。讓學生前往海外大學進行一個學期的學術交流,藉此擴闊視野。</u></a>.
                    </p>
                </section>
                <section id="international-student-exchange-programme" class="gallery-exhib">

                <section class="international-info" style="padding-top:0;z-index: 30;">
				    <h2 style="margin:0 0 30px;">來校交流</h2>
					<p>如您就讀的院校已簽有<a href="javascript:scrollToElemTop($('#international-academic-partners'), 300)" class="underline-link">合作諒解備忘錄和/或交流協議</a>，歡迎申請到HKDI 進行一個學期的學術交流。申請前，請先了解有關<a href="http://www.hkdi.edu.hk/images/global-learning/Credit-bearingInboundExchangeFactsheet_AY1920.pdf" class="underline-link" target="_blank">學分</a>及<a href="http://www.hkdi.edu.hk/images/global-learning/Non-credit-bearingInboundExchangeFactsheets_AY1920.pdf" class="underline-link" target="_blank">非學分</a>交流計劃的詳情。</p>
				</section>
                    <div class="video-bg">
                        <div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
                        </div>
                    </div>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($exchange_programme_list AS $exchange_programme) { ?>
				    <div class="card is-collapsed" style="margin-top: 0 !important;">
                                        <div class="card__inner ani">
                                            <div class="card__img">
					    	<?php if ($exchange_programme['international_image'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_image'])) { ?>
						<img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_image']; ?>" alt="" />
						<?php } ?>
                                            </div>
                                            <div class="card__ppl">
					    	                  <?php if ($exchange_programme['international_thumb'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb'])) { ?>
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb']; ?>" alt="" />
                                                </div>
                                            <?php } ?>

                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $exchange_programme['international_name_'.$lang]; ?></strong>
                                                    <!--<p class="card__ppl-title"></p>-->
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $exchange_programme['international_desc_'.$lang]; ?></p>
                                                <a href="exchange-programme-details.php?international_id=<?php echo $exchange_programme['international_id']; ?>" class="btn">更多</a>
                                            </div>
                                        </div>
                                    </div>
				    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
		<?php /*
                <section id="sharing-from-incoming-students" class="gallery-exhib">
                    <h2>Sharing from
                        <span>Incoming Students:</span>
                    </h2>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($incoming_student_list AS $incoming_student) { ?>
                                    <div class="card is-collapsed">
                                        <div class="card__inner ani">
                                            <div class="card__img">
                                                <?php if ($incoming_student['international_image'] != '' && file_exists($international_path.$incoming_student['international_id'].'/'.$incoming_student['international_image'])) { ?>
						<img src="<?php echo $international_url.$incoming_student['international_id'].'/'.$incoming_student['international_image']; ?>" alt="" />
						<?php } ?>
                                            </div>
                                            <div class="card__ppl">
					    	<!--
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $img_url?>programme/alumni/img-programme-alumni-propic-1.jpg" alt="" />
                                                </div>
						-->
                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $incoming_student['international_name_'.$lang]; ?></strong>
                                                    <p class="card__ppl-title"><?php echo $incoming_student['international_job_title_'.$lang]; ?></p>
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $incoming_student['international_desc_'.$lang]; ?></p>
                                                <a href="incoming-student-details.php?international_id=<?php echo $incoming_student['international_id']; ?>" class="btn">More</a>
                                            </div>
                                        </div>
                                    </div>
				    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
		*/ ?>
                <section id="sharing-from-outgoing-students" class="gallery-exhib">

                <section class="international-info" style="padding-top:0;">
				    <h2 style="margin:0 0 30px;">外訪交流</h2>


					<p>有意參加外訪交流計劃的全日制 HKDI 學生，詳情請參閱申請文件及<a href="javascript:scrollToElemTop($('#scholarship'), 300);" class="underline-link">海外交流獎學金</a>。</p>
				</section>

                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--3-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($outgoing_student_list AS $outgoing_student) { ?>
                                    <div class="card is-collapsed" style="margin-top:0 !important;">
                                        <div class="card__inner ani">
                                            <div class="card__img">
                                                <?php if ($outgoing_student['international_image'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_image'])) { ?>
						<img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_image']; ?>" alt="" />
						<?php } ?>
                                            </div>
                                            <div class="card__ppl">
					    	<?php if ($outgoing_student['international_thumb'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb'])) { ?>
                                                <div class="card__ppl-pic">
                                                    <img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb']; ?>" alt="" />
                                                </div>
                                            <?php } ?>
                                                <div class="card__ppl-txt">
                                                    <strong class="card__ppl-name"><?php echo $outgoing_student['international_name_'.$lang]; ?></strong>
                                                    <p class="card__ppl-title"><?php echo $outgoing_student['international_job_title_'.$lang]; ?></p>
                                                </div>
                                            </div>
                                            <div class="card__content">
                                                <p class="desc"><?php echo $outgoing_student['international_desc_'.$lang]; ?></p>
                                                <a href="outgoing-student-details.php?international_id=<?php echo $outgoing_student['international_id']; ?>" class="btn">更多</a>
                                            </div>
                                        </div>
                                    </div>
				    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>





<section id="international-academic-partners" class="international-academic-partners">
                    <div class="inner">

<h2>學術合作伙伴</h2>
                        <section class="programme-project ani" style="width:100%; margin-bottom:30px;">
                            <div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
                                <h3 class="detail init-detail">美洲</h3>
                                <div class="hidden">








<h4 class="programme-country">美國</h4>
                                    <ul class="init-ul">

<li>Southern California Institute of Architecture</li>
<li>The New School</li>
                                    </ul>
                                </div>
                            </div>
                        </section>




<section class="programme-project ani" style="width:100%; margin-bottom:30px;">
                            <div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">



<h3 class="detail init-detail">澳洲</h3>
                                <div class="hidden">
                    <h4 class="programme-country">澳洲</h4>
                                    <ul class="init-ul">
<li>RMIT University</li>
<li>University of New South Wales</li>
<li>University Technology Sydney</li>

                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section class="programme-project ani" style="width:100%; margin-bottom:30px;">
                            <div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
                                <h3 class="detail init-detail">亞洲</h3>
                                <div class="hidden">


<h4 class="programme-country">中國</h4>
                                    <ul class="init-ul">

<li>Beijing Film Academy</li>
<li>Beijing Institute of Fashion Technology</li>
<li>Central Academy of Fine Arts</li>
<li>Guangzhou Academy of Fine Arts</li>
<li>Shenzhen Polytechnic</li>
<li>Shenzhen University</li>
<li>Tsinghua University</li>
<li>Wuxi Vocational Institute of Arts and Technology</li>

</ul>





<h4 class="programme-country">以色列</h4>
                                    <ul class="init-ul">
                                        <li>Bezalel Academy of Arts and Design</li>
                    </ul>


<h4 class="programme-country">日本</h4>

<ul class="init-ul">

<li>Bunka Gakuen Educational Foundation (Bunka Gakuen University, Bunka Fashion Graduate University, Bunka Fashion College)</li>

</ul>

<h4 class="programme-country">南韓</h4>
                                    <ul class="init-ul">

<li>Hongik University</li>


</ul>


</div>
                            </div>
                        </section>




<section class="programme-project ani" style="width:100%; margin-bottom:30px;">

<div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">

<h3 class="detail init-detail">歐洲</h3>

<div class="hidden">




                    <h4 class="programme-country">比利時</h4>
                                    <ul class="init-ul">
                                        <li>École nationale supérieure des arts visuels de La Cambre</li>
                                        <li>Hasselt University</li>

                                    </ul>



<h4 class="programme-country">丹麥</h4>

<ul class="init-ul">



<li>Royal Danish Academy – Architecture, Design, Conservation</li>

</ul>





                    <h4 class="programme-country">法國</h4>
                                    <ul class="init-ul">
                                        <li>École Camondo – Les Arts Decoratifs</li>
                                        <li>Haute École de Joaillerie</li>
                                    </ul>


<h4 class="programme-country">意大利</h4>

<ul class="init-ul">

<li>Domus Academy</li>
<li>Istituto Europeo di Design</li>
<li>Istituto Marangoni Milano</li>
<li>Nuova Accademia di Bella</li>
<li>Politecnico di Milano</li>

</ul>




<h4 class="programme-country">荷蘭</h4>

<ul class="init-ul">

<li>Amsterdam Fashion Institute</li>

<li>ROC van Amsterdam – Jean School</li>


</ul>





<h4 class="programme-country">挪威</h4>

<ul class="init-ul">

<li>Bergen School of Architecture</li>



</ul>





<h4 class="programme-country">波蘭</h4>
                                    <ul class="init-ul">
                                        <li>Academy of Fine Arts in Gdańsk</li>
                                        <li>University of Arts in Poznań</li>
                                    </ul>




<h4 class="programme-country">瑞典</h4>
                                    <ul class="init-ul">

<li>Linnaeus University</li>

<li>Malmö University</li>

<li>University of Borås</li>

</ul>







<h4 class="programme-country">瑞士</h4>
                                    <ul class="init-ul">
                                        <li>Ecole cantonale d’art de Lausanne (ECAL)</li>
                                        <li>Haute école d’art et de design Genève</li>
                                        <li>Zurich University of the Arts (ZHdk)</li>
                                    </ul>




<h4 class="programme-country">英國</h4>

<ul class="init-ul">

<li>Glasgow School of Art Kingston University</li>
<li>Manchester Metropolitan University</li>
<li>Ravensbourne University London</li>
<li>University of the Arts London</li>
<li>University of Gloucestershire</li>
<li>University of Lincoln</li>
<li>University of Reading</li>
<li>University of Westminster</li>

</ul>



</div>
                            </div>
                        </section>
                    </div>
                </section>




<section id="scholarship" class="scholarship">
		   <div class="inner">
                    <h2>獎學金及捐贈</h2>
                    <div class="inner_content">
                    <h3>贊助學術交流</h3>
					<p>HKDI歡迎社會各界人士贊助成績優異及有經濟需要的學生前往海外進行學術交流。</p>
					</div>

						<section class="programme-project ani" style="width:100%; margin-bottom:30px;">
							<div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
								<h3 style="margin:0;color:#FFF;padding: 0;font-size:16px;" class="detail">+ 詳情</h3>
								<div class="hidden">
									<p>您的捐贈將用作支持學生海外交流所需，包括交通、住宿及一切與學習有關之開支。捐贈港幣十萬元，即可幫助一位學生獲取全額獎學金，赴海外大學參加一個學期的學術交流。鳴謝類別：（港幣100元或以上之捐款可申請扣稅）</p>
									<p>捐款者可選擇以個人或機構名義，命名其贊助之海外交流奬學金， HKDI亦會於學院官方網頁、印刷品、其它宣傳媒介及每年一度之獎助學金頒獎典禮，鳴謝獎學金捐贈者及捐贈機構。</p>
									<p>捐款者亦可指定支持與個人或機構使命相關的學術範疇，或對受惠學生回饋捐贈機構的事宜提出建議。</p>
									<div><a href="mailto:nikishek@vtc.edu.hk" class="btn btn--white"><strong>查詢</strong></a></div>
								</div>
							</div>
						</section>

                    <div class="inner_content">
                    <h3>獎學金</h3>
					<p>亞設貝佳國際有限公司為綜合房地產開發諮詢和建築工程專業服務公司，其獎學金旨在鼓勵主修建築環境設計的年輕學生為個人生活帶來正向影響，並為香港社會作出積極貢獻。</p>
					</div>
		    <?php /*foreach ($scholarship_list AS $scholarship) { ?>
                    <div class="scholarship-block">
                        <!--<img src="<?php echo $international_url.'1/'.$scholarship['international_image_id'].'/'.$scholarship['international_image_filename']; ?>" />-->
                        <div class="txt">
                            <p><?php echo $scholarship['international_image_donor_'.$lang]?></p>
                            <p><strong><?php echo $scholarship['international_image_desc_'.$lang]; ?></strong></p>
                        </div>
                    </div>
		    <?php } */?>
        <!--
            <?php foreach ($scholarship_list AS $scholarship) { ?>
                    <div class="scholarship-block">
                        <div class="txt">
                            <p><strong><?php echo $scholarship['international_name_'.$lang]?></strong></p>
                            <p><?php echo nl2br($scholarship['international_desc_'.$lang]); ?></p>
                        </div>
                    </div>
            <?php } ?>
          -->
          <?php foreach ($scholarship_list AS $scholarship) { ?>
                          <?php
                              if($scholarship['international_name_'.$lang] && !$scholarship['international_desc_'.$lang]){
                                  //$scholarship_class = 'class="scholarship"';
                ?>
                <div style="margin-top: 15px;">
                <h3 style="color: #FFF;"><?php echo $scholarship['international_name_'.$lang]?></h3>
                </div>
                <?php
                              }else{
                                  //$scholarship_class ='';
              ?>
                  <div class="scholarship-block">
                      <div class="txt">

                          <p ><strong><?php echo $scholarship['international_name_'.$lang]?></strong></p>
                          <p><?php echo nl2br($scholarship['international_desc_'.$lang]); ?></p>
                      </div>
                              </div>
              <?php
                              }
                          ?>
          <?php } ?>
		    <!--
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo02.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo03.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo04.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo01.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
                    <div class="scholarship-block">
                        <img src="<?php echo $inc_root_path ?>images/industrial/logo02.png" />
                        <div class="txt">
                            <p>
                                <strong>Lorem ipsum dolor sit amet, consectetuer adipi</strong>
                            </p>
                            <p>IDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                                enim.</p>
                        </div>
                    </div>
		    -->
		    </div>
                </section>
                <section id="master-lecture-series" class="gallery-exhib profile">
                <section class="international-info">
                  <h2 style="margin-left:0; margin-right:0; margin-bottom:30px;">大師講座系列</h2>
                  <p>HKDI 定期邀請來自世界各地的設計大師、學者和專業人士，與學生分享創意經驗和見解。</p>
                </section>
                    <div class="gallery-exhib__wrapper">
                        <div class="cards cards--4-for-dt">
                            <div class="cards__outer">
                                <div class="cards__inner">
                                    <?php foreach ($master_lecture_list AS $master_lecture) { ?>
                                      <div class="card is-collapsed"  style="margin-top:0; margin-bottom: 60px;">
                                        <div class="card__inner">
                                          <div class="card__content" style="margin-top:0;">
                                                <a href="#" class="btn js-expander">
                                                    <img src="<?php echo $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image']; ?>" alt="" />
                                                    <h4><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h4>
                                                    <p><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card__expander">
                                            <div class="profile-block">
                                                <div class="profile-block__detail">
                                                    <div class="img-holder">
                                                        <img src="<?php echo $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image']; ?>" alt="" />
                                                    </div>
                                                    <div class="text">
                                                        <h3 class="profile-block__name"><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h3>
                                                        <p class="profile-block__title"><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
                                                        <div class="profile-block__desc">
                                                            <p><?php echo $master_lecture['master_lecture_detail_'.$lang]; ?></p>
                                                        </div>
                                                    </div>
                                                    <!--<ul class="profile-block__list bullet-list">
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                    </ul>-->
                                                </div>
						<?php $master_lecture_images = DM::findAll($DB_STATUS.'master_lecture_image', 'master_lecture_image_status = ? AND master_lecture_image_pid = ?', 'master_lecture_image_seq', array(STATUS_ENABLE, $master_lecture['master_lecture_id'])); ?>
                                                <div class="profile-block__gallery">
                                                    <div class="profile-block__gallery-slider swiper-container-fade swiper-container-horizontal swiper-container-free-mode swiper-container-ios">
                                                        <div class="swiper-wrapper" style="transition-duration: 0ms;">
							<?php /*
                                                            <div class="profile-block__slide swiper-slide swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next"
                                                                data-swiper-slide-index="1" style="width: 361px; transition-duration: 0ms; opacity: 0; transform: translate3d(0px, 0px, 0px);">
                                                                <img src="../../images/programme/alumni/img-programme-alumni-gallery-demo-2.jpg" alt="">
                                                            </div>
                                                            <div class="profile-block__slide swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 361px; transition-duration: 0ms; opacity: 1; transform: translate3d(-361px, 0px, 0px);">
                                                                <img src="../../images/programme/alumni/img-programme-alumni-gallery-demo-1.jpg" alt="">
                                                            </div>
							*/ ?>
								<?php foreach ($master_lecture_images AS $master_lecture_image) { ?>
								<div class="profile-block__slide swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 361px; transition-duration: 0ms; opacity: 1; transform: translate3d(-361px, 0px, 0px);">
									<img src="<?php echo $master_lecture_url.$master_lecture_image['master_lecture_image_pid'].'/'.$master_lecture_image['master_lecture_image_id'].'/'.$master_lecture_image['master_lecture_image_filename']; ?>" alt="<?php echo $master_lecture_image['master_lecture_image_alt']; ?>" />
								</div>
								<?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
				    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div id="fp-nav" class="custom-sidenav right">
                <ul>
                    <li>
                        <a href="#international-student-exchange-programme">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">來校交流</div>
                    </li>
                    <li>
                        <a href="#sharing-from-outgoing-students">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">外訪交流</div>
                    </li>
                    <li>
                        <a href="#international-academic-partners">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">學術合作夥伴</div>
                    </li>
                    <li>
                        <a href="#scholarship">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">獎學金及捐贈</div>
                    </li>
                    <li>
                        <a href="#master-lecture-series">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">大師講座系列</div>
                    </li>
                </ul>
            </div>
        </main>
        <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>

    </html>
