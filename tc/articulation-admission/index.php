<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Articulation & Education';
$section = 'continuing-education';
$subsection = 'detail';
$sub_nav = 'detail';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$id = $_REQUEST['id'] ?? '';
$dept = DM::load($DB_STATUS.'dept', $id);
$dept_name = $dept['dept_name_'.$lang];

if(!$id){
    header("location: ../index.php");
}


$first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, $id ));

$breadcrumb_arr['設計課程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr['高級文憑課程'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr[$dept_name] =$host_name_with_lang.'programmes/programme.php?programmes_id='.$first_programme['programmes_id'];
$breadcrumb_arr['入學申請'] =''; 
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<div class="page-head">
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>入學資訊</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>升學階梯</strong>
					</h2>
					<div class="txt-editor">
						<h4>
							<strong>HKDI為設計學生打造靈活升學階梯，學生於修畢兩年高級文憑課程後，可選擇升讀由HKDI聯同六所英國著名大學共同提供的一年制的銜接學士學位，或轉升其他本地大學。
							</strong>
						</h4>
						<p>如香港中學文憑考生未能達到最低入學要求，可先修讀一年基礎文憑(設計) 以再升讀高級文憑。</p>
						<?php /*
						<a href="#" class="btn">
							<strong>Read more</strong>
						</a>
						*/ ?>
					</div>
				</div>
			</div>
			<div class="color-flow">
				<div class="content-wrapper">
					<div class="color-flow__items">
						<div id="color-flow--hkdse" class="color-flow__item">
								<div class="color-flow__item-title">香港中學文憑</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--hd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-hkdi.png" alt="" />
								<div class="color-flow__item-title">高級文憑課程</div>
								<div class="color-flow__item-label">2年</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--bd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-uk.png" alt="" />
								<div class="color-flow__item-title">英國學士學位</div>
								<div class="color-flow__item-label">1年</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--md" class="color-flow__item">
								<div class="color-flow__item-title">碩士學位</div>
								<div class="color-flow__item-label">1-2年</div>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail article-detail--txt-green" style="background:#000000;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>學費</strong>
					</h2>
					<div class="txt-editor">
						<ul>
							<li>
								<p>學費水平會每年檢討。2019/20 學年學費水平會因應通脹及有關因素作調整。</p>
							</li>
							<li>
								<p>2019/20 學年學費將於VTC入學網頁公佈</p>
							</li>				
							<li>
								<p>高級文憑課程的一般修讀期為兩年，每年學費分兩期繳付。</p>
							</li>
							<li>
								<p>基礎課程文憑及職專文憑課程的一般修讀期為一年，全年學費分兩期繳付。</p>
							</li>
						</ul>
						<p>
							<strong>2018 / 19 學年全日制資助課程學費以供參考</strong>
						</p>

						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>課程</strong>
									</th>
									<th>
										<strong>第一年</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
									<th>
										<strong>第二年</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
								</tr>
								<tr>
									<td>高級文憑 (資助課程) </td>
									<td>$31,570</td>
									<td>$31,570</td>
								</tr>
								<tr>
									<td>高級文憑 (自資課程)</td>
									<td>$56,600</td>
									<td>$56,600</td>
								</tr>
								<tr>
									<td>基礎課程文憑/ 職專文憑  (資助課程)</td>
									<td>$20,500</td>
									<td>-</td>
								</tr>
								<tr>
									<td>基礎課程文憑/ 職專文憑  (自資課程)</td>
									<td>$27,000</td>
									<td>-</td>
								</tr>
							</table>
						</div>
						<div class="txt-editor__notes">
							<p>
								<strong>
									<small>備註：</small>
								</strong>
							</p>
							<ol>
								<li>除學費外，學生須繳交其他費用如保證金、學生會年費及英文單元基準評核費。高級文憑學生需繳交英文單元研習教費。</li>
								<li>基礎課程文憑學生如選擇修讀選修單元「基礎數學（三）」，需另繳學費。</li>
								<li>職專文憑學生如選擇修讀選修單元「數學 3E : 升學選修單元」，或需另繳學費。</li>
								<li>為增強對學生的學習支援，學院或會要求部分學生修讀銜接單元 / 增潤課程；或需參加額外培訓 / 實習 / 公開考試，繳付所需費用。</li>
							</ol>
						</div>

					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#00f7c1;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>申請程序</strong>
					</h2>
					<div class="txt-editor">
						<p>
							<!-- 2018/19 HKDI Higher Diploma starts to receive application from 24 November 2017. -->
							<br>申請網頁：
							<a href="http://www.vtc.edu.hk/admission/tc/">
								<strong>www.vtc.edu.hk/admission</strong>
							</a>
						</p>
						<p>
							<strong>高級文憑一般入學條件</strong>
						</p>
						<ul>
							<li>香港中學文憑考試五科成績達第二級或以上，包括英國語文及中國語文；或</li>
							<li>VTC 基礎文憑（級別三）/ 基礎課程文憑；或</li>
							<li>VTC 中專教育文憑 / 職專文憑；或</li>
							<li>毅進文憑；或</li>
							<li>同等學歷</li>
						</ul>
					</div>
					<br />
					<p>更多詳請，請<a href="http://www.vtc.edu.hk/admission/tc/" class="btn"><strong>按此</strong></a></p>
				</div>
			</div>

			<div class="article-detail">
				<div class="content-wrapper">
					<div class="txt-editor__notes">
						<p>
							<strong>備註*:</strong>
						</p>
						<ol>
							<li>高級文憑課程的一般修讀期為 2 年。</li>
							<li>香港中學文憑考試應用學習科目（乙類科目）取得「達標」/「達標並表現優異（I）」/「達標並表現優異（II）」的成績，於申請入學時會被視為等同香港中學文憑考試科目成績達「第二級」/「第三級」/「第四級」；最多計算兩科應用學習科目（應用學習中文除外）。學士學位課程只考慮相關應用學習科目的成績。請參閱學士學位課程的特定入學條件。</li>
							<li>香港中學文憑考試其他語言科目（丙類科目）取得「D 或 E 級」/「C 級或以上」的成績，於申請入學時會被視為等同香港中學文憑考試科目成績達「第二級」/「第三級」；只計算一科其他語言科目。</li>
							<li>持中專教育文憑 / 職專文憑並完成指定升學單元的畢業生及毅進文憑畢業生，符合報讀高級文憑課程的一般入學條件（設有特定入學條件的課程除外）。</li>
							<li>部分課程設有特定入學條件，詳情請參閱個別課程的課程資料及入學條件。</li>
							<li>合資格的申請能否被取錄需視乎申請人的學歷、面試 / 測試表現（如適用）、其他學習經驗及成就以及課程學額供求情況。</li>
						</ol>
						<br>
						<p>
							<strong>注意事項</strong></p>
						<ul>
							<li>就同一公開考試，申請人可以綜合不同年度考獲的成績報讀 VTC 課程。若申請人於同一科目多次考獲成績，會以最佳成績考慮。</li>
							<li>新、舊學制之公開考試成績一般不會獲合併考慮。</li>
							<li>非華語申請人如符合特定情況*，可獲個別考慮以下列其他語文科成績或資歷報讀課程：</li>
						</ul>
						<br>
						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>其他語文科成績</strong>
									</th>
									<th>
										<strong>學士學位課程</strong>
									</th>
									<th>
										<strong>高級文憑課程</strong>
									</th>
								</tr>
								<tr>
									<td>香港中學文憑考試應用學習中文</td>
									<td>達標</td>
									<td>達標</td>
								</tr>
								<tr>
									<td>GCSE / IGCSE / GCE O-level 中國語文科</td>
									<td>C 級</td>
									<td>D 級</td>
								</tr>
								<tr>
									<td>GCE AS-level 中國語文科</td>
									<td>C 級</td>
									<td>E 級</td>
								</tr>
								<tr>
									<td>GCE A-level 中國語文科</td>
									<td>E 級</td>
									<td>E 級</td>
								</tr>
							</table>
							<br>
							<p>
								<small>* 特定情況為 1) 申請人在接受中小學教育期間學習中國語文少於六年時間。這項安排專為較遲開始學習中國語文 (例如來港定居時早已過了入學階段) 或間斷地在香港接受教育的學生而設；或 2) 申請人在學校學習中國語文已有六年或以上時間，但期間是按一個經調適並較淺易的課程學習，而有關的課程一般並不適用於其他大部分在本地學校就讀的學生。</small>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#f6f6f6;">
				<div class="content-wrapper">
					<div class="txt-editor">
						<h3 style="color:#00f7c1;">
							<strong>香港高級程度會考學歷</strong>
						</h3>
						<p>持香港高級程度會考及香港中學會考學歷的申請人可報讀學士學位及高級文憑課程。請參閱有關個別課程的詳細資料及入學條件（舊學制中七學生適用）。</p>
						<h3 style="color:#00f7c1;">
							<strong>其他本地、內地及非本地學歷</strong>
						</h3>
						<p>如申請人持有其他本地、內地或非本地學歷報讀課程，其入學申請須經有關學系個別評核。</p>
					</div>
				</div>
			</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>