<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = '最新動態';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/event.js "></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>EVENT</strong> RSVP</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="event-banner">
				<img src="<?php echo $inc_root_path ?>images/news/img-publication-banner.jpg" />
			</section>
			<section class="exhib-contact">
				<div class="exhib-contact__wrapper">
					<div class="sec-intro">
						<h2 class="sec-intro__title">
							<strong>RSVP/ Enquiry</strong>
					</div>
					<div class="contact-form">
						<div class="contact-form__info">
							<h3 class="contact-form__info-title">Contact</h3>
							<div class="contact-form__info-row">
								<div class="contact-form__info-txt">
									<p>Hong Kong Design Institute, Room C819A, 3 King Ling Road,
										<br>Tseung Kwan O, N.T., H.K. </p>
								</div>
								<div class="contact-form__info-contact">
									<a href="mailto:hkdi-desislab@vtc.edu.hk" class="contact-form__contact-item">hkdi-desislab@vtc.edu.hk</a>
									<a href="tel:39282894" class="contact-form__contact-item">Tel 852-3928 2894</a>
								</div>
							</div>
						</div>
						<form class="contact-form__form form-container">
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="field__inline-txt">
											<strong>Title</strong>
										</div>
										<div class="field__inline-field">
											<div class="custom-select">
												<select name="contact_title">
													<option value="mr">Mr.</option>
													<option value="mrs">Mrs.</option>
													<option value="ms">Ms.</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_name" placeHolder="Name" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_msg" placeHolder="Message" />
									</div>
								</div>
							</div>
							<br>
							<p class="contact-form__desc">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel2" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="field__inline-txt">
											<strong>Title</strong>
										</div>
										<div class="field__inline-field">
											<div class="custom-select">
												<select name="contact_title2">
													<option value="mr">Mr.</option>
													<option value="mrs">Mrs.</option>
													<option value="ms">Ms.</option>
												</select>
											</div>
										</div>
										<div class="field__inline-txt">(1 ticket is offered: additional ticket will be subject to availability.)</div>
									</div>
								</div>
							</div>
							<p class="contact-form__tnc">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor
								sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie
								malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Proin eget tortor risus. Quisque velit nisi,
								pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan
								id imperdiet et, porttitor at sem. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet
								et, porttitor at sem.</p>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="captcha">
											<div class="captcha__img">
												<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
											</div>
											<div class="captcha__control">
												<div class="captcha__input">
													<input type="text" name="captcha" class="form-check--empty" />
												</div>
												<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn-row">
								<div class="err-msg-holder">
									<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
									<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
								</div>
								<div class="btn-row btn-row--al-hl">
									<a href="<?php echo $host_name_with_lang ?>" class="btn btn-send">
										<strong>Send</strong>
									</a>
								</div>
							</div>
						</form>
						<div class="contact-form__success-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
						</div>
					</div>
				</div>
			</section>


			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>