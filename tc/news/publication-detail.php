<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '最新動態';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$product_id = @$_REQUEST['product_id'] ?? '';
$product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_id = ?', '', array(STATUS_ENABLE, $product_id));

if($product['product_type'] !='2' || !$product){
    header("location: index.php");
    exit();
}

$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, $product_id));
$issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ? AND issue_id = ?', '', array(STATUS_ENABLE, $product['product_signed_id']));
$product_list = DM::findAll($DB_STATUS.'product', 'product_status = ? AND product_signed_id = ? AND product_type = ? AND product_id <> ?', 'product_seq desc', array(STATUS_ENABLE, $product['product_signed_id'], '2', $product['product_id']));

$breadcrumb_arr['最新動態'] =$host_name_with_lang.'news/';
$breadcrumb_arr[$issue['issue_name_'.$lang]] =$host_name_with_lang.'news/publication.php?issue_id='.$issue['issue_id'];
$breadcrumb_arr[$product['product_name_'.$lang]] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<section class="article-detail">
				<div class="content-wrapper">
					<div class="article-detail__control">
						<!--<a href="publication.php?issue_id=<?php echo $issue['issue_id']; ?>" class="article-detail__btn-back">&lt;&nbsp;Back</a>-->
					</div>
					<div class="article-detail__head">
						<h2 class="article-detail__title"><strong><?php echo $product['product_name_'.$lang]; ?></strong></h2>
						<div class="article-detail__info-items">
							<div class="article-detail__info-item"><?php echo date('d.m.Y', strtotime($issue['issue_date'])); ?></div>
							<div class="article-detail__info-item">HKDI</div>
							<div class="article-detail__info-item">推介</div>
						</div>
					</div>
					<div class="article-detail__cover">
						<?php if ($product['product_thumb_banner'] != '' && file_exists($product_path.$product['product_id']."/".$product['product_thumb_banner'])) { ?>
						<img src="<?php echo $product_url.$product['product_id']."/".$product['product_thumb_banner']; ?>" alt="" />
						<?php } ?>
					</div>
					<div class="txt-editor">
					<?php
						foreach ($product_sections AS $section) {
							if ($section['product_section_type'] == 'T') {
								echo $section['product_section_content_'.$lang];
							} else if ($section['product_section_type'] == 'TI') {
					?>
						<table class="txt-editor__rwd-table">
							<tr>
								<?php if ($section['product_section_image_position'] == '1') { ?>
								<td>
									<?php if ($section['product_section_image'] != '' && file_exists($product_path.$section['product_section_pid']."/".$section['product_section_id']."/".$section['product_section_image'])) { ?>
									<img src="<?php echo $product_url.$section['product_section_pid']."/".$section['product_section_id']."/".$section['product_section_image']; ?>" alt="" />
									<?php } ?>
								</td>
								<?php } ?>
								
								<td><?php echo $section['product_section_content_'.$lang]; ?></td>
								
								<?php if ($section['product_section_image_position'] == '2') { ?>
								<td>
									<?php if ($section['product_section_image'] != '' && file_exists($product_path.$section['product_section_pid']."/".$section['product_section_id']."/".$section['product_section_image'])) { ?>
									<img src="<?php echo $product_url.$section['product_section_pid']."/".$section['product_section_id']."/".$section['product_section_image']; ?>" alt="" />
									<?php } ?>
								</td>
								<?php } ?>
							</tr>
						</table>	
					<?php		
							}
						}
					?>
					</div>
					<hr>
					<div class="article-detail__others">
						<h2 class="article-detail__title"><strong>Others</strong></h2>
						<div class="programme-news ani">
							<?php foreach ($product_list AS $temp_product) { ?>
							<a class="block" href="publication-detail.php?product_id=<?php echo $temp_product['product_id']; ?>">
								<?php if ($temp_product['product_banner'] != '' && file_exists($product_path.$temp_product['product_id']."/".$temp_product['product_banner'])) {  ?>
								<img src="<?php echo $product_url.$temp_product['product_id']."/".$temp_product['product_banner']; ?>" alt="" />
								<?php } ?>
								<div class="txt">
									<p class="date">最新動態 | <?php echo date('j F Y', strtotime($issue['issue_date'])); ?></p>
									<p><?php echo $temp_product['product_name_'.$lang]; ?></p>
									<p class="btn-arrow"></p>
								</div>
							</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>