<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = '聯絡';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['免責聲明'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>免責聲明</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
				<p>本網頁所載資料乃香港知專設計學院（職業訓練局 (VTC) 機構成員）根據現行情況及政策，利用現有資料製作，並已力求內容正確無誤。</p>

				<p>香港知專設計學院定期檢討網頁，如有需要，會更新內容。雖然香港知專設計學院已力求網頁正確無誤，但對於讀者因使用或參考網頁，或因其中資料不準確、錯漏，或因瀏覽網頁或存取其中資料而直接或間接蒙受經濟或其他損失，香港知專設計學院不會負任何法律上或其他方面的責任。</p>

				<h3>版權</h3> 
				<p>本網頁內容版權屬香港知專設計學院所有，使用者須符合以下三個要求，方獲批准使用：(1) 以下版權通告及本批准通告須同時見於所有副本；(2) 本網頁內容只作為參考 資料及個人或非商業用途，並且不得全部或局部複製或刊登於任何聯網電腦，或於任何媒體上廣播；(3) 不得對內容作任何修改。法例上明確禁止將本網頁內容作 其他用途。</p>

				<h3>個人資料</h3>
				<p>使用者只需提供某些個人資料，便可經本網頁電郵至香港知專設計學院，進一步查詢更多資料或問題。香港知專設計學院一向十分重視提供予本網頁的所有個人資料。如使用者未能提供有關個人 資料，香港知專設計學院將無法提供使用者所需的進一步資料。</p>
				
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>