<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = '聯絡';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['友情連結'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>友情連結</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
				<h3>職業訓練局及其他轄下學院</span></h3>
				<ul>
					<li>
						<a href="http://www.vtc.edu.hk" target="_blank">職業訓練局 (VTC)</a></li>
					<li>
						<a href="http://www.ive.edu.hk/ivesite/html/en/aboutive.html" target="_blank">香港專業教育學院 (IVE)</a></li>
					<li>
						<a href="http://www.shape.edu.hk/contentpage.php?id=20071023102140&amp;lang=e" target="_blank">才晉高等教育學院 (SHAPE)</a></li>
					<li>
						<a href="http://www.thei.edu.hk" target="_blank">香港高等科技教育學院(THEI)</a></li>
				</ul>
				<h3>香港設計組織</h3>
				<ul>
					<li>
						<a href="http://www.hkdesigncentre.org" target="_blank">香港設計中心 (HKDC)</a></li>
					<li>
						<a href="http://www.hkfda.org" target="_blank">香港時裝設計師協會 (FDA)</a></li>
					<li>
						<a href="http://www.hongkongda.com/" target="_blank">香港設計師協會 (HKDA)</a></li>
					<li>
						<a href="http://www.hkida.org/" target="_blank">香港室內設計協會 (HKIDA)</a></li>
					<li>
						<a href="http://www.idshk.org" target="_blank">香港工業設計師協會 (IDSHK)</a></li>
				</ul>
				<h3>國際設計組織</h3>
				<ul>
					<li>
						<a href="http://www.icograda.org" target="_blank">International Council of Graphic Design Associations (ICOGRADA)</a></li>
					<li>
						<a href="http://www.icsid.org" target="_blank">International Council of Societies of Industrial Design (ICSID)</a></li>
					<li>
						<a href="http://www.ifiworld.org" target="_blank">International Federation of Interior Architects/Designers (IFI)</a></li>
					<li>
						<a href="http://www.iffti.com" target="_blank">International Foundation of Fashion Technology Institutes (IFFTI)</a></li>
				</ul>
				
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>