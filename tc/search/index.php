<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '聯絡';
$section = '聯絡';
$subsection = '';
$sub_nav = '';
$char_limit = 100;

$breadcrumb_arr['搜索'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$search_text = @$_POST['search_text'];

if ($search_text != '')
{
	$news_list = DM::findAll($DB_STATUS.'news', 'news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND (news_name_' . $lang . ' LIKE ? OR news_detail_' . $lang . ' LIKE ?)', "news_date DESC", array(STATUS_ENABLE,$today,$today,'%'.$search_text.'%','%'.$search_text.'%'));
	$products_list = DM::findAll($DB_STATUS.'product', 'product_status = ? AND product_type = ? AND (product_from_date <= ? OR product_from_date IS NULL) AND (product_to_date >= ? OR product_to_date IS NULL) AND (product_name_' . $lang . ' LIKE ? OR product_detail_' . $lang . ' LIKE ?)', "product_date DESC", array(STATUS_ENABLE,2,$today,$today,'%'.$search_text.'%','%'.$search_text.'%'));
	$programmes_list = DM::findAll($DB_STATUS.'programmes', 'programmes_status = ? AND (programmes_name_' . $lang . ' LIKE ? OR programmes_detail_' . $lang . ' LIKE ?)', "", array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));
	$top_up_degree_list = DM::findAll($DB_STATUS.'top_up_degree', 'top_up_degree_status = ? AND (top_up_degree_name_' . $lang . ' LIKE ? OR top_up_degree_detail_' . $lang . ' LIKE ?)', "", array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));
	$outgoing_student_list = DM::findAll($DB_STATUS.'international', 'international_status = ? AND international_type = ? AND (international_name_' . $lang . ' LIKE ? OR international_detail_' . $lang . ' LIKE ?)', "", array(STATUS_ENABLE, "O",'%'.$search_text.'%','%'.$search_text.'%'));
	$gallery_list = DM::findAll($DB_STATUS.'product','product_status = ? AND product_type = ? AND (product_name_' . $lang . ' LIKE ? OR product_detail_' . $lang . ' LIKE ?)', " product_seq DESC, product_date DESC", array(STATUS_ENABLE,'4','%'.$search_text.'%','%'.$search_text.'%'));




	$project_list = DM::findAll($DB_STATUS.'project','project_publish_status = ? AND project_status = ? AND (project_name_' . $lang . ' LIKE ? OR project_detail_' . $lang . ' LIKE ?)', "project_seq DESC", array('AL',STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$international_p_list = DM::findAll($DB_STATUS.'international','international_type = ? AND international_status = ? AND (international_name_' . $lang . ' LIKE ? OR international_detail_' . $lang . ' LIKE ?)', "international_seq DESC", array('P', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$international_o_list = DM::findAll($DB_STATUS.'international','international_type = ? AND international_status = ? AND (international_name_' . $lang . ' LIKE ? OR international_detail_' . $lang . ' LIKE ?)', "international_seq DESC", array('O', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$international_s_list = DM::findAll($DB_STATUS.'international','international_type = ? AND international_status = ? AND (international_name_' . $lang . ' LIKE ? OR international_detail_' . $lang . ' LIKE ?)', "international_seq DESC", array('S', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$international_i_list = DM::findAll($DB_STATUS.'international','international_type = ? AND international_status = ? AND (international_name_' . $lang . ' LIKE ? OR international_detail_' . $lang . ' LIKE ?)', "international_seq DESC", array('I', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$rsvp_list = DM::findAll($DB_STATUS.'rsvp','rsvp_publish_status = ? AND rsvp_status = ? AND (rsvp_name_' . $lang . ' LIKE ? OR rsvp_detail_' . $lang . ' LIKE ?)', "rsvp_created_by DESC", array('AL',STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$collaboration_list = DM::findAll('new_collaboration','new_collaboration_status = ? AND (new_collaboration_name_' . $lang . ' LIKE ? OR new_collaboration_detail_' . $lang . ' LIKE ?)', "", array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%'));

	$pages = array(
		array(
			"file" => '/about/index.php',
			"url" => 'about/',
			"title" => 'About'
		),
		array(
			"file" => '/news/index.php',
			"url" => 'news/',
			"title" => 'News'
		)
	);

	$count_pages = 0;
	$result_pages = array();
	foreach ($pages as $page) {
		$content = file_get_contents($root_path . $lang_name[$lang] . $page["file"]);
		$pos = strpos ( strtolower( $content ) , strtolower( $search_text ) );
		if ( $pos !== false ) {
			$p = array();
			$count_pages++;
			$_c = remove_all( $content );
			$og_pos = strpos( strip_tags( strtolower( $_c ) ) , strtolower( $search_text ) );
			$pos_1 = ( $og_pos - $char_limit <= 0 ) ? 0 : $og_pos - $char_limit;
			$pos_2 = ( $og_pos + $char_limit > strlen( $_c ) ) ? strlen( $_c ) : $og_pos + $char_limit;
			$p["file"] = $page["file"];
			$p["url"] = $page["url"];
			$p["title"] = $page["title"];
			$p["text"] = substr( $_c , $pos_1 , $pos_2 );
			$result_pages[] = $p;
		} 
	}


	// dynamic pages (without id) from database
	$c_pages = array(
		array(
			"url" => 'knowledge-centre/ccd.php',
			"title" => 'CCD',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 8 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 23 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/cdss.php',
			"title" => 'CDSS',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 8 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 23 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/cimt.php',
			"title" => 'CIMT',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 4 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 17 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/desis_lab.php',
			"title" => 'Desis Lab',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 5 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 18 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/fashion_archive.php',
			"title" => 'Fashion Archive',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 7 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 20 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/media_lab.php',
			"title" => 'Media Lab',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 6 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 19 , '%'.$search_text.'%' )
				)
			)
		)
	);

$c_count_pages = 0;
$c_result_pages = array();
foreach ( $c_pages as $page ) {
	$f = false;
	foreach ($page["search"] as $p) {
		if( !$f ) {
			$s = DM::findOne($DB_STATUS.$p["table"], $p["conditions"], "", $p["parameters"]);
			if( !empty( $s ) )  {
				$f = true;
				$cp = array();
				$cp["title"] = $page["title"];
				$cp["url"] = $page["url"];
				$content = remove_all($s[$p["text"].$lang]);
				$pos = strpos ( strtolower( $content ) , strtolower( $search_text ) );
				if ( $pos !== false ) {
					$p = array();
					$count_pages++;
					$_c = $content;
					$og_pos = strpos( strip_tags( strtolower( $_c ) ) , strtolower( $search_text ) );
					$pos_1 = ( $og_pos - $char_limit <= 0 ) ? 0 : $og_pos - $char_limit;
					$pos_2 = ( $og_pos + $char_limit > strlen( $_c ) ) ? strlen( $_c ) : $og_pos + $char_limit;
					$cp["text"] = substr( $_c , $pos_1 , $pos_2 );
				} 
				$c_result_pages[] = $cp;
				$c_count_pages++;
			}
		}
	}
}
}
$total_count = sizeof($news_list) + sizeof($products_list) + sizeof($programmes_list) + sizeof($top_up_degree_list) + sizeof($outgoing_student_list) + sizeof($gallery_list) + sizeof($project_list) + sizeof($international_p_list) + sizeof($international_o_list) + sizeof($international_s_list) + sizeof($international_i_list) + sizeof($rsvp_list) + sizeof($collaboration_list) + $count_pages + $c_count_pages;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/search.css" type="text/css" />
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title page-head__title--big">
						<h2><strong>搜索</strong></h2>
					</div>
				</div>
			</div>
		</div>
		<form class="search-field" action="index.php" method="POST"><input class="field" type="search" name="search_text" placeholder="輸入關鍵字" value="<?php echo $search_text; ?>" /><input class="submit-btn" type="button" /></form>
		<section class="search-result">
			<p class="search-result__keywords"><strong>Keywords:</strong> <span class="keywords"><?php echo $search_text; ?></span> (<?php echo $total_count; ?> Results)</p>
			<?php foreach ($news_list AS $news) { ?>
				<?php 
				$t = '<span class="link_title">' . $news['news_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($news['news_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($news["news_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($news["news_detail_".$lang])) ? strlen($news["news_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($news["news_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>news/news-detail.php?news_id=<?php echo $news['news_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>
			<?php foreach ($products_list AS $products) { ?>
				<?php
				$t = '<span class="link_title">' . $products['product_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($products['product_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($products["product_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($products["product_detail_".$lang])) ? strlen($products["product_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($products["product_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>news/publication-detail.php?product_id=<?php echo $products['product_id']; ?>"><?php echo $t ?></a>
			<?php } ?>
			<?php foreach ($programmes_list AS $programmes) { ?>
				<?php 
				$t = '<span class="link_title">' . $programmes['programmes_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($programmes['programmes_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($programmes["programmes_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($programmes["programmes_detail_".$lang])) ? strlen($programmes["programmes_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($programmes["programmes_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>programmes/programme.php?programmes_id=<?php echo $programmes['programmes_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>
			<?php foreach ($top_up_degree_list AS $top_up_degree) { ?>
				<?php 
				$t = '<span class="link_title">' . $top_up_degree['top_up_degree_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($top_up_degree['top_up_degree_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($top_up_degree["top_up_degree_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($top_up_degree["top_up_degree_detail_".$lang])) ? strlen($top_up_degree["top_up_degree_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<hr><small class="link_text">' . substr(strip_tags($top_up_degree["top_up_degree_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>programmes/bachelor-degree-detail.php?degree_id=<?php echo $top_up_degree['top_up_degree_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>
			<?php foreach ($outgoing_student_list AS $outgoing_student) { ?>
				<?php 
				$t = '<span class="link_title">' . $outgoing_student['international_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($outgoing_student['international_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($outgoing_student["international_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($outgoing_student["international_detail_".$lang])) ? strlen($outgoing_student["international_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($outgoing_student["international_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>global-learning/outgoing-student-details.php?international_id=<?php echo $outgoing_student['international_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>
			<?php foreach ($gallery_list AS $gallery) { ?>
				<?php if ($gallery['product_id'] != 18 && $gallery['product_id'] != 19 && $gallery['product_id'] != 17) continue; ?>
				<?php
				switch ($gallery['product_id']) {
					case 17:
					$gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/head/';
					break;
					case 18:
					$gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/cnsm/index.html';
					break;
					case 19:
					$gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/reddot/';
					break;
				}
				?>
				<?php 
				$t = '<span class="link_title">' . $gallery['product_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($gallery['product_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($gallery["product_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($gallery["product_detail_".$lang])) ? strlen($gallery["product_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<hr><small class="link_text">' . substr(strip_tags($gallery["product_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $gallery_redirect_link?>"><?php echo $t; ?></a>
			<?php } ?>



			<?php foreach ($project_list AS $project) { ?>
				<?php 
				$t = '<span class="link_title">' . $project['project_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($project['project_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($project["project_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($project["project_detail_".$lang])) ? strlen($project["project_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($project["project_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>knowledge-centre/detail.php?product_id=<?php echo $project['project_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>



			<?php foreach ($international_p_list AS $international_p) { ?>
				<?php 
				$t = '<span class="link_title">' . $international_p['international_name'.$lang] . "</span>"; 
				if (strpos(strtolower($international_p['international_name'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($international_p["international_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($international_p["international_detail_".$lang])) ? strlen($international_p["international_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($international_p["international_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>global-learning/exchange-programme-details.php?international_id=<?php echo $international_p['international_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>


			<?php foreach ($international_o_list AS $international_o) { ?>
				<?php 
				$t = '<span class="link_title">' . $international_o['international_name'.$lang] . "</span>"; 
				if (strpos(strtolower($international_o['international_name'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($international_o["international_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($international_o["international_detail_".$lang])) ? strlen($international_o["international_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($international_o["international_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>global-learning/outgoing-student-details.php?international_id=<?php echo $international_o['international_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>


			<?php foreach ($international_s_list AS $international_s) { ?>
				<?php 
				$t = '<span class="link_title">' . $international_s['international_name'.$lang] . "</span>"; 
				if (strpos(strtolower($international_s['international_name'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($international_s["international_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($international_s["international_detail_".$lang])) ? strlen($international_s["international_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($international_s["international_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>knowledge-centre/detail.php?product_id=<?php echo $international_s['international_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>


			<?php foreach ($international_i_list AS $international_i) { ?>
				<?php 
				$t = '<span class="link_title">' . $international_i['international_name'.$lang] . "</span>"; 
				if (strpos(strtolower($international_i['international_name'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($international_i["international_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($international_i["international_detail_".$lang])) ? strlen($international_i["international_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($international_i["international_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>global-learning/incoming-student-details.php?international_id=<?php echo $international_i['international_id']; ?>"><?php echo $t; ?></a>
			<?php } ?>

			<?php foreach ($rsvp_list AS $rsvp) { ?>
				<?php 
				$t = '<span class="link_title">' . $rsvp['rsvp_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($rsvp['rsvp_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($rsvp["rsvp_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($rsvp["rsvp_detail_".$lang])) ? strlen($rsvp["rsvp_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($rsvp["rsvp_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>news/event.php?name=<?php echo $rsvp['rsvp_url_alias']; ?>"><?php echo $t; ?></a>
			<?php } ?>

			<?php foreach ($collaboration_list AS $collaboration) { ?>
				<?php 
				$t = '<span class="link_title">' . $collaboration['collaboration_name_'.$lang] . "</span>"; 
				if (strpos(strtolower($collaboration['collaboration_name_'.$lang]), strtolower($search_text)) === false) {
					$og_pos = strpos(strip_tags(strtolower($collaboration["new_collaboration_detail_".$lang])), strtolower($search_text));
					$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
					$pos_2 = ($og_pos+$char_limit > strlen($collaboration["new_collaboration_detail_".$lang])) ? strlen($collaboration["new_collaboration_detail_".$lang]) : $og_pos+$char_limit;
					
					$t .= '<br><small class="link_text">' . substr(strip_tags($collaboration["new_collaboration_detail_".$lang]), $pos_1, $pos_2) . '</small>';
				}
				?>
				<a class="result" href="<?php echo $host_name_with_lang?>collaboration/; ?>"><?php echo $t; ?></a>
			<?php } ?>

			<?php foreach ($result_pages as $page) {
				$url = $host_name_with_lang . $page["url"];
				$t = $page["title"];
				$t .= '<br><small class="link_text">' . $page["text"] . '</small>';
				?>
				<a class="result" href="<?php echo $url;?>"><?php echo $t; ?></a>
			<?php } ?>

			<?php foreach ($c_result_pages as $page) {
				$url = $host_name_with_lang . $page["url"];
				$t = $page["title"];
				if( !empty( $page["text"] ) )
					$t .= '<br><small class="link_text">' . $page["text"] . '</small>';
				?>
				<a class="result" href="<?php echo $url;?>"><?php echo $t; ?></a>
			<?php } ?>


			
		</section>


		<?php
		function remove_all($content) {
			$content = preg_replace('/<\?php[\s\S]+?\?>/', '', $content);
			$content = preg_replace('/<script[\s\S]+?<\/script>/', '', $content);
			$content = preg_replace('/<head[\s\S]+?<\/head>/', '', $content);
			$content = preg_replace('/<header[\s\S]+?<\/header>/', '', $content);
			$content = preg_replace('/<nav[\s\S]+?<\/nav>/', '', $content);
			$content = preg_replace('/<footer[\s\S]+?<\/footer>/', '', $content);
			$content = preg_replace('/<link[\s\S]+?>/', '', $content);
			$content = strip_tags($content);
			return $content;
		}
		?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>


	<style>
		span.highlighted {background: yellow;}
		small.link_text {font-weight: normal !important; margin-top: 10px; display: block;}
	</style>
	<script>
		$(document).ready(function() {
			var search = '<?php echo $search_text; ?>';
			var regex = new RegExp(search, 'gi')

			var elems = [ "link_title" , "link_text" ];
			for (var i = 0; i < elems.length; i++) {
				var elem = document.getElementsByClassName(elems[i]);
				for (var j = 0; j < elem.length; j++) {
					var result = elem[j].innerText.replace(regex, function(response) {
						return "<span style='background-color: yellow;'>" + response + "</span>"
					});
					if( elems[i] == "link_text" )
						result = "... " + result + " ..."
					elem[j].innerHTML = result
				}
			}

		});
	</script>
</body>

</html>