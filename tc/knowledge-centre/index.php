<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '知識資源中心';
$section = 'knowledge-centre';
$subsection = 'knowledge-centre';
$sub_nav = 'knowledge-centre';

$breadcrumb_arr['知識資源中心'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$product_cimt = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('4'));
$product_desis_lab = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('5'));
$product_media_lab = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('6'));
$product_fashion_archive = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('7'));
$product_ccd = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('8'));
$product_cdss = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('153'));

?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>知識</strong>
								<br>資源中心</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">香港知專設計學院的倡導跨學科設計教育並鼓勵學生參與創新社會研究計劃，以推進設計思維。</p>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='ccd.php'" style="background-image:url(<?php echo $product_url.$product_ccd['product_id'].'/'.$product_ccd['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>傳意設計研究中心</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_ccd['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="ccd.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='cdss.php'" style="background-image:url(<?php echo $product_url.$product_cdss['product_id'].'/'.$product_cdss['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>設計企劃研究中心</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_cdss['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="cdss.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='cimt.php'" style="background-image:url(<?php echo $product_url.$product_cimt['product_id'].'/'.$product_cimt['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>知專設創源</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_cimt['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="cimt.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='desis_lab.php'" style="background-image:url(<?php echo $product_url.$product_desis_lab['product_id'].'/'.$product_desis_lab['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>社會設計工作室</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_desis_lab['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="desis_lab.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='fashion_archive.php'" style="background-image:url(<?php echo $product_url.$product_fashion_archive['product_id'].'/'.$product_fashion_archive['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>時裝資料館</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_fashion_archive['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="fashion_archive.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='media_lab.php'" style="background-image:url(<?php echo $product_url.$product_media_lab['product_id'].'/'.$product_media_lab['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>媒體研究所</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_media_lab['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="media_lab.php" class="btn btn--white">詳情</a>
					</div>
				</div>
			</div>
			<!--
			<div class="img-portal" style="background-image:url(<?php echo $img_url?>knowledge-centre/img-knowledge-centre-5.jpg);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>DFS</span>
						</h3>
						<p class="img-portal__desc">HKDI DESIS Lab for Social Design Research is a new cross-disciplinary action research group at the Hong Kong Design Institute (HKDI), founded in summer 2013, with the aims to set up a research platform of social design, an emerging field that advocates a new approach to design: ‘designers as enablers of social change’. It is part of the DESIS International Network (Design for Social Innovation and Sustainability) collaborating with over 40 DESIS Labs around the world. The network aims to advance international knowledge in design for the social good and develop social practice to benefit Hong Kong society.</p>
					</div>
					<div class="img-portal__btn">
						<a href="#" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			-->
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
