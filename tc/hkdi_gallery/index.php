<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'HKDI Gallery';
$section = 'gallery';
$subsection = 'alumni';
$sub_nav = 'alumni';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$exhib_space_arr['20'] = 'hkdi-gallery';
$exhib_space_arr['21'] = 'd-mart';
$exhib_space_arr['22'] = 'exp-center';

$highlight_show = 5;

$gallery_highlight_list = DM::findAllWithLimit($DB_STATUS.'product', $highlight_show,'product_status = ? AND product_type = ? and product_index_show = ?', 'product_from_date desc', array(STATUS_ENABLE, '4',STATUS_ENABLE));


$breadcrumb_arr['HKDI Gallery'] ='';

?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
		<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN"></script>-->
        	<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js "></script>
		<script>
			$(document).ready(function() {
				//initMap();
			});

			function initMap() {

				var mapStyles = [{
						"stylers": [{
							"saturation": -100
						}]
					},
					{
						"featureType": "water",
						"stylers": [{
								"lightness": -30
							},
							{
								"gamma": 0.91
							}
						]
					}
				];



				<?php
					foreach ($exhib_space_arr as $key => $value) {
						$result_v = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'Visit'));
						$product_lng = $result_v['product_lng'];
						$product_lat = $result_v['product_lat'];

						if($product_lng && $product_lat){

				?>
				var map2 = new google.maps.Map(document.getElementById('map'), {
					zoom: 18,
					center: new google.maps.LatLng(<?php echo $product_lat?>, <?php echo $product_lng?>),
					styles: mapStyles
				});
				var marker2;
				marker2 = new google.maps.Marker({
					position: new google.maps.LatLng(<?php echo $product_lat?>, <?php echo $product_lng?>),
					map: map2
				});
				<?php } } ?>

			}
		</script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<!--<strong>HKDI Gallery</strong>-->
								<img src="/images/common/hkdigallery_logo.png" style="max-width:  318px"/>
								<br></h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">隸屬香港知專設計學院（HKDI），HKDI Gallery為一充滿活力及視野的展覽場地。每年我們均會與海內外不同單位，如國際知名博物館、設計師、策展人等合作，以當代設計為議題，舉辦涵蓋平面設計、建築、時裝、產品設計及多媒體等類別的一系列頂尖展覽。</p>
						<hr>
						<a href="http://www.facebook.com/hkdi.gallery" class="btn">HKDI Gallery Facebook Page ></a>
					</div>
				</div>
			</div>
			<section class="gallery-exhib">
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
				<div class="gallery-exhib__wrapper">
					<h3 class="gallery-exhib__title ani">
						<strong>21/22</strong>
<?php
/*
<strong><?php echo date('y')-1?>/<?php echo date('y')?></strong>
*/
?>
						<br>展覽
					</h3>
					<time class="exhib-list__item-date" style="font-size: 20px;">
設計精髓為何？HKDI Gallery以 #EssenceofDesign 為主題，於2021-2022年度呈獻三個風格廻異的設計展覽，從多角度探索此終極設計命題。包括十一月底開幕的「紅點設計展：設計．價值」、明年一月開幕的「Zaha Hadid Architects: Vertical Urbanism」及二月開幕的Lab4Living「Beyond 100」，請留意我們的最新動向。

<br>&nbsp<br>





</time>
					<div class="gallery-exhib__items ani">
						<?php
							foreach ($gallery_highlight_list AS $gallery) {
						?>
						<a href="gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="gallery-exhib__item">
							<img src="<?php echo $product_url.$gallery['product_id'].'/'.$gallery['product_thumb_banner']; ?>" alt="" />
							<div class="gallery-exhib__item-txt">
								<h4 class="gallery-exhib__item-title"><?php echo $gallery['product_name_'.$lang]; ?></h4>
								<time class="gallery-exhib__item-date"><?php echo date('j.n.y', strtotime($gallery['product_from_date'])); ?> - <?php echo date('j.n.y', strtotime($gallery['product_to_date'])); ?></time>
							</div>
						</a>
						<?php } ?>
						<!--
						<div class="gallery-exhib__item">
							<img src="<?php echo $img_url?>gallery/img-gallery-exhibition-4.jpg" alt="" />
							<div class="gallery-exhib__item-txt">
								<h4 class="gallery-exhib__item-title">Japanese Poster Artists – Cherry Blossom and Asceticism</h4>
								<time class="gallery-exhib__item-date">26.8.16 - 2.2.17</time>
							</div>
						</div>
						-->
					</div>
				</div>
			</section>
			<section class="exhib-list">
				<div class="exhib-list__wrapper">
					<div class="tabs">
						<div class="tabs__btns">
							<!--
							<a href="#tabs--exhib-all" class="tabs__btn is-active">All</a>
							<a href="#tabs--exhib-architectural" class="tabs__btn">Architectural</a>
							<a href="#tabs--exhib-typography" class="tabs__btn">Typography</a>
							<a href="#tabs--exhib-graphic-design" class="tabs__btn">Graphic Design</a>
							<a href="#tabs--exhib-photography" class="tabs__btn">Photography</a>
							<a href="#tabs--exhib-fashion" class="tabs__btn">Fashion</a>
							<a href="#tabs--exhib-product" class="tabs__btn">Product</a>
							<a href="#tabs--exhib-collaboration-with-international-institute" class="tabs__btn">Collaboration with International Institute</a>
							-->
							<a href="#tabs--exhib-all" class="tabs__btn is-active">全部</a>
							<?php foreach ($exhibiton_types AS $exhibiton_type) { ?>
							<a href="#tabs--exhib-<?php echo $exhibiton_type['key']; ?>" class="tabs__btn"><?php echo $exhibiton_type[$lang]; ?></a>
							<?php } ?>
						</div>
						<div class="tabs__contents">
							<?php $page_size = 8; ?>
							<?php	$temp_gallery_list = DM::findAllWithPaging($DB_STATUS.'product', 1, $page_size, 'product_status = ? AND product_type = ? ', 'product_from_date desc', array(STATUS_ENABLE, '4')); ?>
							<?php	$temp_gallery_next_list = DM::findAllWithPaging($DB_STATUS.'product', 2, $page_size, 'product_status = ? AND product_type = ? ', 'product_from_date desc', array(STATUS_ENABLE, '4')); ?>
							<div id="tabs--exhib-all" class="tabs__content more-content is-active">
								<div class="exhib-list__items more-content__content">
									<?php foreach ($temp_gallery_list AS $gallery) { ?>
									<a href="gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="exhib-list__item">
										<img src="<?php echo $product_url.$gallery['product_id'].'/'.$gallery['product_thumb_banner']; ?>" alt="" />
										<div class="exhib-list__item-txt">
											<h4 class="exhib-list__item-title"><?php echo $gallery['product_name_'.$lang]; ?></h4>
											<time class="exhib-list__item-date"><?php echo date('j.n.y', strtotime($gallery['product_from_date'])); ?> - <?php echo date('j.n.y', strtotime($gallery['product_to_date'])); ?></time>
										</div>
									</a>
									<?php } ?>
								</div>
								<?php if ($temp_gallery_next_list) { ?>
								<div class="btn-row">
									<a href="#" class="btn more-content__btn" data-page-no="2" data-exhib-type="all">
										<strong>更多</strong>
									</a>
								</div>
								<?php } ?>
							</div>

							<?php foreach ($exhibiton_types AS $exhibiton_type) { ?>
								<?php	$temp_gallery_list = DM::findAllWithPaging($DB_STATUS.'product', 1, $page_size, 'product_status = ? AND product_type = ?  AND product_exhibition_type = ?', 'product_from_date desc', array(STATUS_ENABLE, '4', $exhibiton_type['value']));; ?>
								<?php	$temp_gallery_next_list = DM::findAllWithPaging($DB_STATUS.'product', 2, $page_size, 'product_status = ? AND product_type = ?  AND product_exhibition_type = ?', 'product_from_date desc', array(STATUS_ENABLE, '4', $exhibiton_type['value']));; ?>
							<div id="tabs--exhib-<?php echo $exhibiton_type['key']; ?>" class="tabs__content more-content">
								<div class="exhib-list__items more-content__content">
									<?php foreach ($temp_gallery_list AS $gallery) { ?>
									<a href="gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="exhib-list__item">
										<img src="<?php echo $product_url.$gallery['product_id'].'/'.$gallery['product_thumb_banner']; ?>" alt="" />
										<div class="exhib-list__item-txt">
											<h4 class="exhib-list__item-title"><?php echo $gallery['product_name_'.$lang]; ?></h4>
											<time class="exhib-list__item-date"><?php echo date('j.n.y', strtotime($gallery['product_from_date'])); ?> - <?php echo date('j.n.y', strtotime($gallery['product_to_date'])); ?></time>
										</div>
									</a>
									<?php } ?>
								</div>
								<?php if ($temp_gallery_next_list) { ?>
								<div class="btn-row">
									<a href="#" class="btn more-content__btn" data-page-no="2" data-exhib-type="<?php echo $exhibiton_type['value']; ?>">
										<strong>更多</strong>
									</a>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>

			<section class="exhib-space" style="">
				<div class="exhib-space__intro is-active" style="background: url(../../../images/gallery/becomefriends/bg_becomefriends.jpg) no-repeat center center">
					<center><a href="https://forms.gle/HxPGdEy7UTEyBFJX8" target="_blank"><img src="../../../images/gallery/becomefriends/caption/TC01_W.svg" style="width: 80%; height: auto;"></a></center>
				</div>
			</section>
			
			<section class="exhib-space">
				<div class="exhib-space__intro is-active">
					<div class="exhib-space__wrapper">
						<div class="page-head__title">
							<h2>
								<strong>展覽空間</strong>
							</h2>
						</div>
						<div class="exhib-space__portal">
							<a href="#exhib-space--hkdi-gallery" class="exhib-space__portal-btn">HKDI Gallery ></a>
							<a href="#exhib-space--d-mart" class="exhib-space__portal-btn">d-Mart ></a>
							<a href="#exhib-space--exp-center" class="exhib-space__portal-btn">Experience Center ></a>
						</div>
					</div>
				</div>

				<?php
					foreach ($exhib_space_arr as $key => $value) {
						$result = DM::findOne($DB_STATUS.'product', ' product_id = ?', " ", array($key));
						$result_t = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'T'));
						$result_v = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'Visit'));
						$product_lng = $result_v['product_lng'];
						$product_lat = $result_v['product_lat'];

						$product_id = $result['product_id'];
						$image1 = $result['product_banner'];
						$image2 = $result['product_thumb_banner'];

						if ($image1 != '' && file_exists($product_path.$product_id.'/'.$image1)){
							$image1 = $product_url.$product_id.'/'.$image1;
						}

						if ($image2 != '' && file_exists($product_path.$product_id.'/'.$image2)){
							$image2 = $product_url.$product_id.'/'.$image2;
						}

				?>
				<div id="exhib-space--<?php echo $value?>" class="exhib-space__content-holder">
					<div class="exhib-space__wrapper">
						<div class="exhib-space__control">
							<a href="#" class="exhib-space__back-btn">&lt;&nbsp;Back</a>
						</div>
						<div class="exhib-space__content">
							<div class="exhib-space__loc-item exhib-space__detail">
								<?php echo $result_t['product_section_content_'.$lang]?>
							</div>
							<?php if ($image1){?>
							<div class="exhib-space__loc-item">
								<img src="<?php echo $image1?>" alt="" />
							</div>
							<?php } ?>

							<?php if($product_lng && $product_lat){ ?>
							<div class="exhib-space__loc-item">
								<div class="exhib-space__map-holder">
									<!--img class="exhib-space__map-placeholder" src="<?php echo $img_url?>gallery/img-gallery-map-1.jpg" alt="" /-->
									<div id="exhib-space-map-<?php echo $value?>" class="exhib-space__map"></div>
								</div>
							</div>
							<?php } ?>

							<?php if ($image2){?>
							<div class="exhib-space__loc-item">
								<img src="<?php echo $image2?>" alt="" />
							</div>
							<?php } ?>
						</div>
						<!--<div class="exhib-space__control">
							<a href="#" class="btn btn--white">Enquiry</a>
						</div>-->
					</div>
				</div>
				<?php } ?>

			</section>
			<section class="exhib-contact">
				<div class="exhib-contact__wrapper">
					<div class="sec-intro">
						<h2 class="sec-intro__title">
							<strong>聯絡我們</strong></h2>
					</div>
					<div class="contact-form">
						<div class="contact-form__info">
							<h3 class="contact-form__info-title">聯絡</h3>
							<div class="contact-form__info-row">
								<div class="contact-form__info-txt">
									<p>香港新界將軍澳景嶺路3號</p>
								</div>
								<div class="contact-form__info-contact">
									<a href="mailto:hkdi-gallery@vtc.edu.hk" class="contact-form__contact-item">hkdi-gallery@vtc.edu.hk</a>
									<a href="tel:39282894" class="contact-form__contact-item">Tel 852-3928 2566</a>
								</div>
							</div>
						</div>
						<form class="contact-form__form form-container">

							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_name" placeHolder="名字" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_company" placeHolder="公司" class="form-check--empty" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel" placeHolder="電話" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_email" placeHolder="電郵" class="form-check--empty form-check--email" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_msg" placeHolder="詳細訊息" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<br>
										<div class="custom-checkbox">
											<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty " />
											<label for="contact_book"><strong>預約參觀</strong></label>
										</div>
									</div>
								</div>
							</div>
							<div class="tobe-shown" data-trigger="#contact_book">
								<div class="field-row">
									<div class="field field--1-1 field--mb-1-1">
										<div class="field__holder">
											<div class="field__inline-txt">
												<strong>預約日期 </strong>
											</div>
											<div class="field__date-fields">
												<div class="field__date-field">
													<div class="field__date-label">由</div>
													<input type="text" name="booking_date_from" class="form-check--empty datepicker datepicker--booking" />
												</div>
												<div class="field__date-field">
													<div class="field__date-label">至</div>
													<input type="text" name="booking_date_to" class="form-check--empty datepicker datepicker--booking" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="field-row">
									<div class="field field--1-2 field--mb-1-1">
										<div class="field__holder">
											<input type="text" name="booking_participants" placeHolder="預計人數" class="form-check--empty" />
										</div>
									</div>
								</div>
							</div>

							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="captcha">
											<div class="captcha__img">
												<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
											</div>
											<div class="captcha__control">
												<div class="captcha__input">
													<input type="text" name="captcha" class="form-check--empty" />
												</div>
												<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">重新載入</a>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="btn-row">
								<div class="err-msg-holder">
									<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
									<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
								</div>
								<div class="btn-row btn-row--al-hl">
									<a href="#" class="btn btn-send">
										<strong>送出</strong>
									</a>
								</div>
							</div>
						</form>
						<div class="contact-form__success-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
						</div>
					</div>
				</div>
			</section>

			<section class="loc-map">
				<div class="loc-map__detail-holder">
					<div class="loc-map__detail">
						<h2 class="loc-map__title">地址</h2>
						<div class="loc-map__detail-items">
							<div class="loc-map__detail-item">
								<p class="loc-map__detail-decs">
									香港新界將軍澳景嶺路3號
								</p>
							</div>
						</div>
					</div>
				</div>
				<div id="map" class="loc-map__map-holder">
					<div id="map" class="loc-map__map"></div>
				</div>
			</section>
		</main>
		<?php //include $inc_lang_path . "inc_common/inc_form_err_msg.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
