<section class="section latest-news" data-tooltip-name="最新動態">
    <div class="fullpage__content">
        <div class="fullpage__content-holder">
            <div class="latest-news__holder">
              <div class="latest-news__content">
                <?php $x = 0 ;?>
                <?php foreach ($news_list AS $news) {
                  $size = 'large';
                  $activeItem = '';
                  $orderClass = ' latest-news__box-others';
                  if($x == 0) {$size = 'large'; $activeItem = ' latest-news__box-active'; $orderClass = ' latest-news__box-one';};
                  if($x == 1) {$orderClass = ' latest-news__box-two';};
                  if($x == 2) {$orderClass = ' latest-news__box-three';};
                  if($x > 0 && $x < 4) {$size = 'middle';};
                  if($x >= 4) {$size = 'small';};
                   $x = $x + 1;
                  ?>
                  <a href="news/news-detail.php?news_id=<?php echo $news['news_id']; ?>" class="latest-news__box latest-news__box-<?php echo $size.$activeItem.$orderClass?> ">
                    <img class="latest-news__box-img" src="<?php echo $img_url ?>home/img-latest-news-bg.png" alt="" style="background-image:url(<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>);">
                    <div class="latest-news__box-details">
                      <div class="latest-news__box-details-date"> <?php echo $news_types_arr[$news['news_type']][$lang]?>&nbsp;&nbsp;|&nbsp;&nbsp;<time datetime="<?php echo $news['news_date']; ?>"><?php echo date('Y年m月j日', strtotime($news['news_date'])); ?> </time></div>
                      <div class="latest-news__box-details-title"><?php echo $news['news_name_'.$lang]; ?></div>
                    </div>
                  </a>
                <?php } ?>
              </div>
              <div class="latest-news__btn-wrapper">
                <a href="<?php echo $host_name_with_lang;?>news" class="latest-news__btn-more tc">更多動態</a>
              </div>
            </div>
        </div>
    </div>
</section>
