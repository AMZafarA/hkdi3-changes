<section class="section knowledge-centres" data-tooltip-name="知識資源中心">
	<div class="fullpage__content">
		<div class="fullpage__content-holder">
			<div class="knowledge-centres__holder">
				<div class="knowledge-centres__info">
					<div class="knowledge-centres__info-holder">
						<div class="knowledge-centres__txt">
							<h2 class="knowledge-centres__title">知識資源中心</h2>
							<p class="knowledge-centres__desc">
								香港知專設計學院的倡導跨學科設計教育並鼓勵學生參與創新社會研究計劃，以推進設計思維。
							</p>
						</div>
						<div class="knowledge-centres__links">
							<a data-kc-id="ccd" class="knowledge-centres__link" href="knowledge-centre/ccd.php">傳意設計研究中心</a>
							<a data-kc-id="cdss" class="knowledge-centres__link" href="knowledge-centre/cdss.php">設計企劃研究中心</a>

							<a data-kc-id="cimt" class="knowledge-centres__link" href="knowledge-centre/cimt.php">知專設創源</a>
							<a data-kc-id="dl" class="knowledge-centres__link" href="knowledge-centre/desis_lab.php">社會設計工作室</a>
							<a data-kc-id="fa" class="knowledge-centres__link" href="knowledge-centre/fashion_archive.php">時裝資料館</a>
							<a data-kc-id="ml" class="knowledge-centres__link" href="knowledge-centre/media_lab.php">媒體研究所</a>
						</div>
					</div>
					<a class="knowledge-centres__btn" href="knowledge-centre/index.php"></a>
				</div>
				<div class="knowledge-centres__thumbs">
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="ccd" class="knowledge-centres__thumb" href="knowledge-centre/ccd.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-ccd.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">傳意設計研究中心</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="cdss" class="knowledge-centres__thumb" href="knowledge-centre/cdss.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-cdss.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">設計企劃研究中心</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="cimt" class="knowledge-centres__thumb" href="knowledge-centre/cimt.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-cimt.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">知專設創源</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="dl" class="knowledge-centres__thumb" href="knowledge-centre/desis_lab.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-desis-lab.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">社會設計工作室</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="fa" class="knowledge-centres__thumb" href="knowledge-centre/fashion_archive.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-fashion-archive.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">時裝資料館</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="ml" class="knowledge-centres__thumb" href="knowledge-centre/media_lab.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-media-lab.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">媒體研究所</span>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
