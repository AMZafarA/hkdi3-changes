<section class="section academic-programmes" data-tooltip-name="設計課程">
	<div class="fullpage__content">
		<div class="fullpage__content-holder">
			<div class="academic-programmes__holder">
				<h2 class="academic-programmes__title title-lv1">設計課程</h2>
				<div class="tabs tabs--sync-height">
					<div class="tabs__btns">
						<a href="#tabs--higher-diploma" class="tabs__btn is-active">高級文憑課程</a>
						<a href="#tabs--degree" class="tabs__btn">學位課程</a>
						<a href="#tabs--master-degree" class="tabs__btn">碩士學位課程</a>
<?php
/*
						<!--<a href="<?php echo $host_name_with_lang?>continuing-education/" class="tabs__btn is-link">Continuing Education</a>-->
*/
?>
						<a target="_blank" href="http://www.hkdi.edu.hk/peec/" class="tabs__btn is-link">持續進修</a>
					</div>
					<div class="tabs__contents">
						<div id="tabs--higher-diploma" class="tabs__content is-active">
							<div class="tetris-tabs">
								<div class="tetris-tabs__btns swiper-container">
									<div class="tetris-tabs__btns-wrapper swiper-wrapper">
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-aip" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=1" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">建築、<br>室內及<br>產品設計</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div>
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-cdm" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=5" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">傳意設計</span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
											</a>
										</div>
										<!--div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-dfs" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=2" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">基礎設計</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div-->
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-ddm" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=28" class="tetris-tabs__btn swiper-slide">
												<span class="tetris-tabs__programmes-name">數碼媒體</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div>
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-fid" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=18" class="tetris-tabs__btn swiper-slide">
												<span class="tetris-tabs__programmes-name">時裝及<br>形象設計</span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
											</a>
										</div>
									</div>
								</div>
								<div class="tetris-tabs__contents">
									<div id="sec-aip" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">建築、室內及產品設計</h3>
											</div-->
											<?php /*
											<div class="ap-content__list-holder">
											<?php
												$numItems = count($AIP_list);
												$i =0; foreach($AIP_list as $key => $AIP) {

													if ($i%4==0) {
											?>
													<ul class="ap-content__list-col">
												<?php } ?>

														<li>
															<a href="programmes/programme.php?programmes_id=<?php echo $AIP['programmes_id']?>"><?php echo $AIP['programmes_name_'.$lang]?></a>
														</li>
												<?php $i++; if($i === $numItems || $i%4==0){ ?>
													</ul>

											<?php } } ?>
											</div>
											*/ ?>
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($AIP_list) / 2)); $i++) { ?>
												<?php 	$programme = $AIP_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($AIP_list)); $i++) { ?>
												<?php 	$programme = $AIP_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
									<div id="sec-cdm" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">傳意設計及數碼媒體
													< <span class="displayinline-block">Digital Media</span> >
												</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($CDM_list) / 2)); $i++) { ?>
												<?php 	$programme = $CDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($CDM_list)); $i++) { ?>
												<?php 	$programme = $CDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
									<?php /*
									<div id="sec-dfs" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">基礎設計</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($DFS_list) / 2)); $i++) { ?>
												<?php 	$programme = $DFS_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($DFS_list)); $i++) { ?>
												<?php 	$programme = $DFS_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div-->*/
									 ?>
									<div id="sec-ddm" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">時裝及形象設計</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($DDM_list) / 2)); $i++) { ?>
												<?php 	$programme = $DDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($DDM_list)); $i++) { ?>
												<?php 	$programme = $DDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
									<div id="sec-fid" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">時裝及形象設計</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($FID_list) / 2)); $i++) { ?>
												<?php 	$programme = $FID_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($FID_list)); $i++) { ?>
												<?php 	$programme = $FID_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tabs--degree" class="tabs__content">
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">香港高級<br />程度會考</div>
									</div>
									<a href="programmes/#tabs-hd" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
												首兩年<br />高級文憑
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
												第三年<br />學士學位
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
												第四年<br />碩士學位
										</div>
									</a>
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">學士學位課程</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">學士學位課程由HKDI聯同六所英國著名大學共同提供。修讀學生只需先完成兩年高級文憑課程，便可選擇升讀為期一年的學士學位課程</p>
									</div>
								</a>
							</div>
						</div>
						<div id="tabs--master-degree" class="tabs__content">
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">香港高級<br />程度會考</div>
									</div>
									<a href="programmes/#tabs-hd" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
												首兩年<br />高級文憑
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
												第三年<br />學士學位
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
												第四年<br />碩士學位
										</div>
									</a>
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">碩士學位課程</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI 夥拍英國伯明翰城市大學提供設計管理學文學碩士，予修畢高級文憑課程及銜接學士學位的學生，用額外一或兩年完成碩士學位課程</p>
									</div>
								</a>
							</div>
						</div>
						<div id="tabs--continuing-education" class="tabs__content">
							<h3 class="ap-content__title">持續進修</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
