<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = '聯絡';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['聯絡我們'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $host_name?>css/contact.css" type="text/css" />
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN"></script>
		<script>
			$(document).ready(function() {
				initMap();
			});

			function initMap() {

				var mapStyles = [{
						"stylers": [{
							"saturation": -100
						}]
					},
					{
						"featureType": "water",
						"stylers": [{
								"lightness": -30
							},
							{
								"gamma": 0.91
							}
						]
					}
				];
				var map2 = new google.maps.Map(document.getElementById('contact-map'), {
					zoom: 14,
					center: new google.maps.LatLng(22.3057197,114.253429),
					styles: mapStyles
				});
				var marker2;
				marker2 = new google.maps.Marker({
					position: new google.maps.LatLng(22.3057197,114.253429),
					map: map2
				});

			}
		</script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>聯絡我們</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="contact">
				<div id="contact-map"></div>
				<div class="contact-info">
					<p class="facilities-name">香港知專設計學院</p>
					<p>香港新界將軍澳景嶺路3號.<br>
					<a class="mail" href="mailto:hkdi@vtc.edu.hk">hkdi@vtc.edu.hk</a></p>

					<p class="facilities-name">學院秘書處</p>
					<p><span class="tel">(852) 3928 2000 / (852) 3928 2222</span><br>
					<span class="fax">(852) 3928 2024</span></p>

					<p class="facilities-name">建築、室內及產品設計學系 (AIP)</p>
						<span class="tel">(852) 3928 2800</span><br>
					<span class="fax">(852) 3928 2801</span></p>

					<p class="facilities-name">傳意設計學系 (DCD)</p>
					<p><span class="tel">(852) 3928 2929</span><br>
						<span class="fax">(852) 3928 2945</span></p>



					<p class="facilities-name">數碼媒體學系 (DDM)</p>
					<p><span class="tel">(852) 3928 2700</span><br>
						<span class="fax">(852) 3928 2701</span></p>

					<p class="facilities-name">時裝及形象設計學系 (FID)</p>
					<p><span class="tel">(852) 3928 2900</span><br>
						<span class="fax">(852) 3928 2988</span></p>

					<p class="facilities-name">專業促進中心 (PEEC)</p>
					<p><span class="tel">(852) 3928 2777</span><br>
						<span class="fax">(852) 3928 2054</span><br>
						<a class="mail" href="mailto:peec.hkdi@vtc.edu.hk">peec.hkdi@vtc.edu.hk</a></p>

					<p class="facilities-name">學院外事處</p>
					<p><span class="tel">(852) 3928 2561 (學院參觀) / 3928 2761 (場地設施租用) / 3928 2178 (學術交流)</span><br>
					<span class="fax">(852) 3928 2194</span></p>
				</div>
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
