<?php

$inc_root_path = '';error_reporting(0);
require_once "include/config.php";
require_once "include/PHPMailer/PHPMailerAutoload.php";
include_once 'include/securimage/securimage.php';
include_once "include/classes/DataMapper.php";

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$_POST['type'] = (@$_POST['type'] == '' ? $_GET['type'] : $_POST['type']);
foreach ($_POST as $key => $value){
	$$key = $value;
	// echo $$key . ' => ' . $value . "<br><br>";
}
$securimage = new Securimage();
if ($securimage->check($captcha) == false) {
	$return_json['status'] = 2;
	echo json_encode($return_json);
	exit();
}

if ($type == 'booking_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username = $smtp_username;
	// $mail->Password = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-ereply@vtc.edu.hk", "HKDI");
	$mail->CharSet = "UTF-8";

	// For old Venue Booking content
	$now_unix = time(); // For old email content
	$now = date('Y-m-d H:i:s', $now_unix); // For old email content
	$subject = "HKDI Venue Booking Request:" . $now_unix;

	$venue = ""; // For old email content
	if ($booking_lecture_theatre_289 != '') { $venue .= "; "."Lecture Theatre (289 seats)";}
	if ($booking_lecture_theatre_120 != '') { $venue .= "; "."Lecture Theatre (120 seats)";}
	if ($booking_function_room != '') { $venue .= "; "."Function Room (A002-A003) (80 seats)";}
	if ($booking_vtc_auditorium != '') { $venue .= "; "."VTC Auditorium (740 seats)";}
	if ($booking_hkdi_gallery != '') { $venue .= "; "."HKDI Gallery";}
	if ($booking_dmart != '') { $venue .= "; "."d-mart";}
	if ($booking_experience_center != '') { $venue .= "; "."Experience Centre (50 seats)";}
	if ($booking_design_beulevard != '') { $venue .= "; "."Design Boulevard";}
	if ($booking_multi_purpose_hall != '') { $venue .= "; "."Multi-purpose Hall (80 seat)";}

	if (strlen($venue) >= 2) {$venue = substr($venue,2);}

	if($lang =='lang1'){
		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Venue Booking Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will contact you in the soonest. For any enquiry, please contact us at 3928 2761.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Applicant:</td><td width="450">'.$booking_contact_person.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$booking_organisation.'</td></tr>
		<tr><td width="200">Proposed booking date (from):</td><td width="450">'.$booking_from_date.'</td></tr>
		<tr><td width="200">Proposed booking date (to):</td><td width="450">'.$booking_to_date.'</td></tr>
		<tr><td width="200">Contact:</td><td width="450">'.$booking_tel.'</td></tr>
		<tr><td width="200">Fax number:</td><td width="450">'.$booking_fax.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$booking_email.'</td></tr>
		<tr><td width="200">Purpose of the event:</td><td width="450">'.$booking_purpose.'</td></tr>
		<tr><td width="200">Estimated number of participants:</td><td width="450">'.$booking_participants.'</td></tr>
		<tr><td width="200">Proposed venue(s) of booking:</td><td width="450">'.$venue.'</td></tr>
	</table>';
	}else if($lang =='lang2'){
		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Venue Booking Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will contact you in the soonest. For any enquiry, please contact us at 3928 2761.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Applicant:</td><td width="450">'.$booking_contact_person.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$booking_organisation.'</td></tr>
		<tr><td width="200">Proposed booking date (from):</td><td width="450">'.$booking_from_date.'</td></tr>
		<tr><td width="200">Proposed booking date (to):</td><td width="450">'.$booking_to_date.'</td></tr>
		<tr><td width="200">Contact:</td><td width="450">'.$booking_tel.'</td></tr>
		<tr><td width="200">Fax number:</td><td width="450">'.$booking_fax.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$booking_email.'</td></tr>
		<tr><td width="200">Purpose of the event:</td><td width="450">'.$booking_purpose.'</td></tr>
		<tr><td width="200">Estimated number of participants:</td><td width="450">'.$booking_participants.'</td></tr>
		<tr><td width="200">Proposed venue(s) of booking:</td><td width="450">'.$venue.'</td></tr>
	</table>';
	}else if($lang =='lang3'){
		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Venue Booking Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will contact you in the soonest. For any enquiry, please contact us at 3928 2761.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Applicant:</td><td width="450">'.$booking_contact_person.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$booking_organisation.'</td></tr>
		<tr><td width="200">Proposed booking date (from):</td><td width="450">'.$booking_from_date.'</td></tr>
		<tr><td width="200">Proposed booking date (to):</td><td width="450">'.$booking_to_date.'</td></tr>
		<tr><td width="200">Contact:</td><td width="450">'.$booking_tel.'</td></tr>
		<tr><td width="200">Fax number:</td><td width="450">'.$booking_fax.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$booking_email.'</td></tr>
		<tr><td width="200">Purpose of the event:</td><td width="450">'.$booking_purpose.'</td></tr>
		<tr><td width="200">Estimated number of participants:</td><td width="450">'.$booking_participants.'</td></tr>
		<tr><td width="200">Proposed venue(s) of booking:</td><td width="450">'.$venue.'</td></tr>
	</table>';
	}

	/*
		// FilmStudio - First version
		$content .= 'Application ID: ' . $now_unix . '<br />';
		$content .= 'Submit Date Time: ' . $now . '<br />';
		$content .= 'Name of Applicant: ' . $booking_contact_person . '<br />';
		$content .= 'Name of Organisation: ' . $booking_organisation . '<br />';
		$content .= 'Proposed booking date (from): ' . $booking_from_date . '<br />';
		$content .= 'Proposed booking date (to): ' . $booking_to_date . '<br />';
		$content .= 'Contact: ' . $booking_tel . '<br />';
		$content .= 'Fax number: ' . $booking_fax . '<br />';
		$content .= 'Email: ' . $booking_email . '<br />';
		$content .= 'Purpose of the event: ' . $booking_purpose . '<br />';
		$content .= 'Estimated number of participants: ' . $booking_participants . '<br />';
		$content .= 'Proposed venue(s) of booking: ' . $venue . '<br />';
	*/
	/*
		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= 'Proposed booking date(s): From'.$booking_from_date. ' to '.$booking_to_date.'<br /><br />';
		$content .= 'Proposed venue(s) of booking: '.'<br />';
		$content .= ($booking_lecture_theatre_289 != '' ? 'Lecture Theatre (289 seats)'.'<br />' : '');
		$content .= ($booking_lecture_theatre_120 != '' ? 'Lecture Theatre (120 seats)'.'<br />' : '');
		$content .= ($booking_function_room != '' ? 'Function Room (A002-A003) (80 seats)'.'<br />' : '');
		$content .= ($booking_vtc_auditorium != '' ? 'VTC Auditorium (740 seats)'.'<br />' : '');
		$content .= ($booking_hkdi_gallery != '' ? 'HKDI Gallery'.'<br />' : '');
		$content .= ($booking_dmart != '' ? 'd-mart'.'<br />' : '');
		$content .= ($booking_experience_center != '' ? 'Experience Centre (50 seats)'.'<br />' : '');
		$content .= ($booking_design_beulevard != '' ? 'Design Boulevard'.'<br />' : '');
		$content .= '<br />Purpose of the event: '.$booking_purpose.'<br />';
		$content .= 'Estimated number of participants: '.$booking_participants.'<br />';
		$content .= 'Organisation: '.$booking_organisation.'<br />';
		$content .= 'Contact Person: '.$booking_contact_person.'<br />';
		$content .= 'Telephone: '.$booking_tel.'<br />';
		$content .= 'Fax: '.$booking_fax.'<br />';
		$content .= 'Email: '.$booking_email.'<br />';
		$content .= "</div>";
	*/

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress($booking_email,'');

	$mail->AddBCC("hkdi-booking@vtc.edu.hk",'');
	$mail->AddBCC("may_leung@vtc.edu.hk",'');
	$mail->AddBCC("evalcm@vtc.edu.hk",'');
	$mail->AddBCC("josie.l@vtc.edu.hk",'');
	$mail->AddBCC("ritatsc@vtc.edu.hk",'');
	$mail->AddBCC("waltwong@vtc.edu.hk",'');

	$mail->Send(); // Send first email to applicant

	//	$mail->ClearAddresses();
	$mail->ClearAllRecipients();

	// store into db
	$now = date('Y-m-d H:i:s');
	$values = $_REQUEST;
	unset($values['type']);
	$values['booking_status'] = STATUS_ENABLE;
	$values['booking_created_date'] = $now;
	$values['booking_updated_date'] = $now;
	DM::insert('booking', $values);

	$return_json['status'] = '1';
	echo json_encode($return_json);
} else if ($type == 'application_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	//	$mail->SetFrom("dilwl-emkt@vtc.edu.hk", "HKDI");
	$mail->SetFrom("hkdi-ereply@vtc.edu.hk", "HKDI");
	$mail->CharSet = "UTF-8";


	// For old Campus Visit form content
	$now_unix = time(); // For old email content
	$now = date('Y-m-d H:i:s', $now_unix); // For old email content
	$subject = "HKDI Campus Visit Request:" . $now_unix;

	if($lang =='lang1'){

		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Campus Visit Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will get in touch with you shortly. For any enquiry, please contact us at 3928 2561.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$application_form_organisation.'</td></tr>
		<tr><td width="200">Language:</td><td width="450">'.$application_form_list_lang.'</td></tr>
		<tr><td width="200">Dates of visit 1st priority:</td><td width="450">'.$application_form_visit_date1.'</td></tr>
		<tr><td width="200">Dates of visit 2nd priority:</td><td width="450">'.$application_form_visit_date2.'</td></tr>
		<tr><td width="200">Time of visit:</td><td width="450">'.$application_form_visit_time.'</td></tr>
		<tr><td width="200">No. of visitors:</td><td width="450">'.$application_form_visitors_no.'</td></tr>
		<tr><td width="200">Objective</td><td width="450">'.$obj.'</td></tr>
		<tr><td width="200">Profile:</td><td width="450">'.$application_form_list.'</td></tr>
		<tr><td width="200">Contact person:</td><td width="450">'.$application_form_title." ".$application_form_contact_person.'</td></tr>
		<tr><td width="200">Position:</td><td width="450">'.$application_form_position.'</td></tr>
		<tr><td width="200">Contact number:</td><td width="450">'.$application_form_contact.'</td></tr>
		<tr><td width="200">Mobile number:</td><td width="450">'.$application_form_mobile.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$application_form_email.'</td></tr>
		<tr><td width="200">Other request:</td><td width="450">'.$application_form_remarks.'</td></tr>
	</table>';
	}else if($lang =='lang2'){

		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Campus Visit Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will get in touch with you shortly. For any enquiry, please contact us at 3928 2561.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$application_form_organisation.'</td></tr>
		<tr><td width="200">Language:</td><td width="450">'.$application_form_list_lang.'</td></tr>
		<tr><td width="200">Dates of visit 1st priority:</td><td width="450">'.$application_form_visit_date1.'</td></tr>
		<tr><td width="200">Dates of visit 2nd priority:</td><td width="450">'.$application_form_visit_date2.'</td></tr>
		<tr><td width="200">Time of visit:</td><td width="450">'.$application_form_visit_time.'</td></tr>
		<tr><td width="200">No. of visitors:</td><td width="450">'.$application_form_visitors_no.'</td></tr>
		<tr><td width="200">Objective</td><td width="450">'.$obj.'</td></tr>
		<tr><td width="200">Profile:</td><td width="450">'.$application_form_list.'</td></tr>
		<tr><td width="200">Contact person:</td><td width="450">'.$application_form_title." ".$application_form_contact_person.'</td></tr>
		<tr><td width="200">Position:</td><td width="450">'.$application_form_position.'</td></tr>
		<tr><td width="200">Contact number:</td><td width="450">'.$application_form_contact.'</td></tr>
		<tr><td width="200">Mobile number:</td><td width="450">'.$application_form_mobile.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$application_form_email.'</td></tr>
		<tr><td width="200">Other request:</td><td width="450">'.$application_form_remarks.'</td></tr>
	</table>';
	}else if($lang =='lang3'){

		$content = '<div style="font-size:17px;line-height:24px;">';
		$content .= '<p>HKDI Campus Visit Request</p><p>Thank you for your application. Details of your application are listed below for your reference. We will get in touch with you shortly. For any enquiry, please contact us at 3928 2561.</p>';
		$content .= '
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="200">Application ID:</td><td width="450">'.$now_unix.'</td></tr>
		<tr><td width="200">Submit Date Time:</td><td width="450">'.$now.'</td></tr>
		<tr><td width="200">Name of Organisation:</td><td width="450">'.$application_form_organisation.'</td></tr>
		<tr><td width="200">Language:</td><td width="450">'.$application_form_list_lang.'</td></tr>
		<tr><td width="200">Dates of visit 1st priority:</td><td width="450">'.$application_form_visit_date1.'</td></tr>
		<tr><td width="200">Dates of visit 2nd priority:</td><td width="450">'.$application_form_visit_date2.'</td></tr>
		<tr><td width="200">Time of visit:</td><td width="450">'.$application_form_visit_time.'</td></tr>
		<tr><td width="200">No. of visitors:</td><td width="450">'.$application_form_visitors_no.'</td></tr>
		<tr><td width="200">Objective</td><td width="450">'.$obj.'</td></tr>
		<tr><td width="200">Profile:</td><td width="450">'.$application_form_list.'</td></tr>
		<tr><td width="200">Contact person:</td><td width="450">'.$application_form_title." ".$application_form_contact_person.'</td></tr>
		<tr><td width="200">Position:</td><td width="450">'.$application_form_position.'</td></tr>
		<tr><td width="200">Contact number:</td><td width="450">'.$application_form_contact.'</td></tr>
		<tr><td width="200">Mobile number:</td><td width="450">'.$application_form_mobile.'</td></tr>
		<tr><td width="200">Email:</td><td width="450">'.$application_form_email.'</td></tr>
		<tr><td width="200">Other request:</td><td width="450">'.$application_form_remarks.'</td></tr>
	</table>';
	}
	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress($application_form_email,'');

	$mail->AddBCC("jasonlam@vtc.edu.hk",'');
	$mail->AddBCC("cherry-lam@vtc.edu.hk",'');
	$mail->AddBCC("lunalau@vtc.edu.hk",'');
	$mail->AddBCC("waltwong@vtc.edu.hk",'');

	$mail->Send(); // Send first email to applicant

	//	$mail->ClearAddresses();
	$mail->ClearAllRecipients();

	// store into db
	$now = date('Y-m-d H:i:s');
	$values = $_REQUEST;
	unset($values['type']);
	$values['application_form_status'] = STATUS_ENABLE;
	$values['application_form_created_date'] = $now;
	$values['application_form_updated_date'] = $now;
	DM::insert('application_form', $values);

	$return_json['status'] = '1';
	echo json_encode($return_json);
} else if ($type == 'dept_contact_form')
{
	$product_section = DM::load('product_section', $product_section_id);
	$receiver = $product_section['product_section_email'];

	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	//	$mail->SetFrom("dilwl-emkt@vtc.edu.hk", "HKDI");
	$mail->SetFrom("hkdi-ereply@vtc.edu.hk", "HKDI");
	$mail->CharSet = "UTF-8";

	// For old Venue Booking content
	$now_unix = time(); // For old email content
	$now = date('Y-m-d H:i:s', $now_unix); // For old email content
	//	$subject = "Knowledge Centre Contact Enquiry: " . $now_unix . " (Section ID: ".$product_section_id. ")";
	//	$subject = "Knowledge Centre Contact Enquiry";
	$subject = "Online Enquiry";

	$subject = $product_section_subject . $subject;
	/*switch ($product_section_id) {
		case 11: $subject = "CIMT " . $subject; break;
		case 37: $subject = "Fashion Archive " . $subject; break;
		case 58: $subject = "DESIS Lab " . $subject; break;
		case 60: $subject = "Media Lab " . $subject; break;
		default: $subject = "Knowledge Centre " . $subject;
	}*/

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Date: ".$now."<br />";
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company/Org.: ".$contact_company."<br />";
	$content .= "Phone No.: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$content .= "Message: ".$contact_msg."<br />";
	if ($contact_inquiry != '')
	{
		$content .= "<strong>Site Visit</strong><br />";
		$content .= 'Proposed booking date(s) From '.$booking_date_from." to ".$booking_date_to."<br />";
		$content .= 'Estimated number of participants: '.$booking_participants;
	}
	$utms = [ "utm_source" , "utm_medium" , "utm_campaign" , "utm_id" , "utm_term" , "utm_content" ];
	foreach ( $utms as $utm ) {
		if( isset( $$utm ) )
			$content .= "$utm: ".$$utm."<br />";
	}
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress($receiver,'');
	//	$mail->AddAddress("waltwong@vtc.edu.hk",'');
	$mail->Send();
	//	$mail->ClearAddresses();
	$mail->ClearAllRecipients();

	$return_json['status'] = '1';
	echo json_encode($return_json);
} else if ($type == 'gallery_contact_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	//	$mail->SetFrom("dilwl-emkt@vtc.edu.hk", "HKDI");
	$mail->SetFrom("hkdi-ereply@vtc.edu.hk", "HKDI");
	$mail->CharSet = "UTF-8";
	$subject = "Gallery Contact Form:" . $now_unix;

	$content = '<div style="font-size:17px;line-height:24px;">';
	//	$content .= "Form (Section ID): ".$product_section_id."<br />";
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company: ".$contact_company."<br />";
	$content .= "Phone: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$utms = [ "utm_source" , "utm_medium" , "utm_campaign" , "utm_id" , "utm_term" , "utm_content" ];
	foreach ( $utms as $utm ) {
		if( isset( $$utm ) )
			$content .= "$utm: ".$$utm."<br />";
	}
	$content .= "Message: ".$contact_msg."<br /><br />";
	if ($contact_inquiry != '')
	{
		$content .= 'Proposed booking date(s) From '.$booking_date_from." to ".$booking_date_to."<br />";
		$content .= 'Estimated number of participants: '.$booking_participants;
	}
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress($contact_email,'');
	//	$mail->AddAddress("waltwong@vtc.edu.hk",'');

	$mail->Send();
	$mail->ClearAddresses();

	$return_json['status'] = '1';
	echo json_encode($return_json);
} else if ($type == 'edt_contact_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
	$subject = "Contact Us Form";
	$mail->CharSet = "UTF-8";

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company: ".$contact_company."<br />";
	$content .= "Phone: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$content .= "Inquiry: ".$contact_inquiry."<br /><br />";
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress("lawrence.lam@firmstudio.com",'');
	$mail->Send();
	$mail->ClearAddresses();

	$return_json['status'] = '1';
	echo json_encode($return_json);
}else if ($type == 'event_contact_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
	$subject = "RSVP/ Enquiry";
	$mail->CharSet = "UTF-8";

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Title: ".$contact_title."<br />";
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company: ".$contact_company."<br />";
	$content .= "Phone: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$content .= "Message: ".$contact_msg."<br /><br />";
	$content .= "Additional Ticket Title: ".$contact_title2."<br />";
	$content .= "Additional Ticket Phone: ".$contact_tel2."<br />";
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress("lawrence.lam@firmstudio.com",'');
	$mail->Send();
	$mail->ClearAddresses();

	$return_json['status'] = '1';
	echo json_encode($return_json);
}else if ($type == 'industrial_collaborations_contact_form')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
	$subject = "Recruit Graduate & Industrial Attachment";
	$mail->CharSet = "UTF-8";

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company: ".$contact_company."<br />";
	$content .= "Phone: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$content .= "Types of business: ".$contact_business."<br />";
	$content .= "Message: ".$contact_msg."<br /><br />";
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress("lawrence.lam@firmstudio.com",'');
	$mail->Send();
	$mail->ClearAddresses();

	$return_json['status'] = '1';
	echo json_encode($return_json);
}else if ($type == 'subscribe')
{
	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
	$subject = "Subscription";
	$mail->CharSet = "UTF-8";

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Name: ".$subscription_name."<br />";
	$content .= "Email: ".$subscription_email."<br />";
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress("cellis.testing123@gmail.com",'');
	$mail->Send();
	$mail->ClearAddresses();

	// store into db
	$now = date('Y-m-d H:i:s');
	$values = $_REQUEST;
	unset($values['type']);
	$values['subscription_status'] = STATUS_ENABLE;
	$values['subscription_created_by'] = 0;
	$values['subscription_created_date'] = $now;
	$values['subscription_updated_by'] = 0;
	$values['subscription_updated_date'] = $now;
	DM::insert('subscription', $values);

	$return_json['status'] = '1';
	echo json_encode($return_json);
}else if ($type == 'unsubscribe')
{
	if( isset( $unsubscribe_checkbox ) && $unsubscribe_checkbox ) {

		$user_found = DM::findOne('subscription', 'subscription_email = ?', '', array($subscription_email));
		if( empty( $user_found ) ) {
			$return_json['status'] = '3';
			echo json_encode($return_json);
		}
		else {
			// send email
			$mail = new PHPMailer;
			mb_internal_encoding('UTF-8');

			// $mail->IsSMTP();

			// $mail->Host = $SYSTEM_SMTP_SERVER;
			// $mail->SMTPAuth = $smtp_require_auth;
			// $mail->Username  = $smtp_username;
			// $mail->Password   = $smtp_password;
			// $mail->SMTPSecure = $smtp_security;
			// $mail->Port = $smtp_port;
			$mail->IsHTML(true);

			$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
			$subject = "Unsubscribed";
			$mail->CharSet = "UTF-8";

			$content = '<div style="font-size:17px;line-height:24px;">';
			$content .= "Email: ".$subscription_email."<br />";
			$content .= "</div>";

			$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
			$mail->MsgHTML($content);

			$mail->AddAddress("cellis.testing123@gmail.com",'');
			$mail->Send();
			$mail->ClearAddresses();


			$subscription = DM::findOne('subscription', 'subscription_email = ? AND subscription_status = ?', '', array($subscription_email, STATUS_ENABLE));
			$subscription_id = $subscription["subscription_id"] ?? 0;


			if( $subscription_id ) {
				$values = array();
				$values['subscription_id'] = $subscription_id;
				$values['subscription_status'] = STATUS_DISABLE;
				DM::update('subscription', $values);
			}


			$return_json['status'] = '1';
			echo json_encode($return_json);
		}
	} 
	else {
		$return_json['status'] = '4';
		echo json_encode($return_json);
	}
}
?>
