<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="scholarship" ) {

		$id = $_REQUEST['scholarship_id'] ?? '';
		$scholarship_lv1 = $_REQUEST['scholarship_lv1'] ?? '';
		$scholarship_lv2 = $_REQUEST['scholarship_lv2'] ?? '';

		$permission = DM::load("scholarship", $id);
		//$permission_name = $permission['scholarship_name_lang1'];
		$permission_name = 'scholarship';
		$type_name = DM::load("department", $id);
		$type_namelv1 = $type_name['department_name_lang1'];
		$type_name = "Department";
		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], $permission_name)){
				header("location: ../dashboard.php?msg=F-1000");
				return;
			}
		}

		if($processType == "" || $processType == "submit_for_approval") {
			$values = $_REQUEST;
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();
			if($isNew){
				$values['scholarship_publish_status'] = "D";
				$values['scholarship_status'] = STATUS_ENABLE;
				$values['scholarship_created_by'] = $_SESSION['sess_id'];
				$values['scholarship_created_date'] = $timestamp;
				$values['scholarship_updated_by'] = $_SESSION['sess_id'];
				$values['scholarship_updated_date'] = $timestamp;
				$values['scholarship_type'] = "1";

				$id = DM::insert('scholarship', $values);

				if(!file_exists($scholarship_path . $id. "/")){
					mkdir($scholarship_path . $id . "/", 0775, true);
				}

			} else {
				$ObjRec = DM::load('scholarship', $id);

				if($ObjRec['scholarship_status'] == STATUS_DISABLE)
				{
					header("location: list.php?scholarship_lv1=".$scholarship_lv1."&scholarship_lv2=".$scholarship_lv2."&msg=F-1002");
					return;
				}

				$values['scholarship_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';
				$values['scholarship_updated_by'] = $_SESSION['sess_id'];
				$values['scholarship_updated_date'] = $timestamp;

				DM::update('scholarship', $values);
			}

			$ObjRec = DM::load('scholarship', $id);
			$scholarship_img_arr = array();
			foreach($scholarship_img_arr as $key=>$val){
				if($_FILES[$val]['tmp_name'] != ""){
					if($ObjRec[$val] != "" && file_exists($scholarship_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($scholarship_path  . $id . "/" . $ObjRec[$val]);
					}
					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);
					$newname = moveFile($scholarship_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($scholarship_path . $id . "/".$newname);

					/*$newWidth = '1500';
					$newHeight = '500';

					if($width > $newWidth){
						resizeImageByHeight($scholarship_path. $id."/". $newname, $newWidth , $scholarship_path. $id."/". $newname);
					}

					if($height > $newHeight){
						resizeImageByWidth($programmes_path. $id."/". $newname, $newHeight , $programmes_path. $id."/". $newname);
					}

					resizeAndBg($programmes_path. $id."/". $newname,$programmes_path. $id."/". $newname,$newWidth,$newHeight);*/

					$values = array();
					$values[$val] = $newname;
					$values['scholarship_id'] = $id;

					DM::update('scholarship', $values);
					unset($values);

					$ObjRec = DM::load('scholarship', $id);
				}
			}

			if($processType == ""){
				// Logger::log($_SESSION['sess_id'], $timestamp, "scholarship '" . $ObjRec['scholarship_name_lang1'] . "' (ID:" . $ObjRec['scholarship_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
				// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, $isNew ? "CS" : "ES");
			} else if($processType == "submit_for_approval"){
				// ApprovalService::withdrawApproval( $permission_name,$type, $id, $timestamp);
				// Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['scholarship_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
				// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $permission_name, $isNew ? "CS" : "ES");
				// $files = array($host_name."en/scholarship/scholarship.php", $host_name."tc/scholarship/scholarship.php", $host_name."sc/scholarship/programmes.php");
				// $approval_id = ApprovalService::createApprovalRequest( $permission_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				// NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);
			}

			header("location:scholarship.php?scholarship_id=" . $id . "&msg=S-1001");
			return;
		}

		else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('programmes', $id);

			if($ObjRec == NULL || $ObjRec == "") {
				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1002");
				return;
			}

			if($ObjRec['programmes_status'] == STATUS_DISABLE) {
				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1004");
				return;
			}

			$isPublished = SystemUtility::isPublishedRecord("programmes", $id);

			$values['programmes_id'] = $id;
			$values['programmes_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['programmes_status'] = STATUS_DISABLE;
			$values['programmes_updated_by'] = $_SESSION['sess_id'];
			$values['programmes_updated_date'] = getCurrentTimestamp();
			DM::update('programmes', $values);
			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("programmes_" . $programmes_lv1."_".$programmes_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Programmes '" . $ObjRec['programmes_name_lang1'] . "' (ID:" . $ObjRec['programmes_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "DS");

				$files = array($host_name."en/programmes/programmes.php", $host_name."tc/programmes/programmes.php", $host_name."sc/programmes/programmes.php");

				$approval_id = ApprovalService::createApprovalRequest("programmes_" . $programmes_lv1."_".$programmes_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("Programmes - Higher Diploma - ".$type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "D");
			}

			header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=S-1002");
			return;
		} else if($processType == "undelete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('programmes', $id);

			if($ObjRec == NULL || $ObjRec == "") {
				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1002");
				return;
			}

			if($ObjRec['programmes_status'] == STATUS_ENABLE || $ObjRec['programmes_publish_status'] == 'AL') {
				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1006");
				return;
			}

			$values['programmes_id'] = $id;
			$values['programmes_publish_status'] = 'D';
			$values['programmes_status'] = STATUS_ENABLE;
			$values['programmes_updated_by'] = $_SESSION['sess_id'];
			$values['programmes_updated_date'] = getCurrentTimestamp();
			DM::update('programmes', $values);
			unset($values);

			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			$parameters = array(STATUS_ENABLE, 'programmes', $id, 'RCA', 'RAA', 'APL');

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;

				DM::update('approval', $values);
				unset($values);

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));

				$recipient_list = array("E", "C");

				if($approval_request['approval_approval_status'] != "RCA"){
					$recipient_list[] = "A";
				}

				$message = "";

				switch($approval_request['approval_approval_status']){
					case "RCA":
						$message = "Request for checking";
					break;
					case "RAA":
						$message = "Request for approval";
					break;
					case "APL":
						$message = "Pending to launch";
					break;
				}

				$message .= " of programmes '" . $ObjRec['programmes_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'programmes_' . $programmes_lv1."_".$programmes_lv2, "WA");
			}

			Logger::log($_SESSION['sess_id'], $timestamp, "Programmes '" . $ObjRec['programmes_name_lang1'] . "' (ID:" . $ObjRec['programmes_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'programmes_' . $programmes_lv1."_".$programmes_lv2, "UD");
			header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=S-1003");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>