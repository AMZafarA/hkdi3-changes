<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if($type=="user" )
	{	
		if($processType == "update_password") {
			$user_rec = DM::load('user', $_SESSION['sess_id']);

			if($user_rec['user_password_hash'] != hash('sha1', $user_rec['user_password_salt'] . $_REQUEST['old_password'])){
				header("location:info.php?msg=F-1007");
				return;
			}
			
			if($_REQUEST['new_password'] != $_REQUEST['confirm_new_password']){
				header("location:info.php?msg=F-1008");
				return;
			}

			$timestamp = getCurrentTimestamp();
			$new_salt = strRand(20);
			
			$values['user_id'] = $_SESSION['sess_id'];
			$values['user_password_hash'] = hash('sha1', $new_salt . $_REQUEST['new_password']);
			$values['user_password_salt'] = $new_salt;
			$values['user_updated_by'] = $_SESSION['sess_id'];
			$values['user_updated_date'] = $timestamp;

			DM::update('user', $values);
			
			Logger::log($_SESSION['sess_id'], $timestamp, "User ID : " . $user_rec['user_id'] . " updated his password");

			header("location:info.php?msg=S-1004");
			return;
		}
	}

	Logger::log($_SESSION['sess_id'], $timestamp, "User ID : " . $_SESSION['sess_id'] . " unknown action");
	header("location:../dashboard.php?msg=F-1001");
	return;
?>