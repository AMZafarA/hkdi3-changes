<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "include/system_version.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script>
			function submitForm(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#old_password").val() == ""){
					flag = false;
					errMessage += "Old Password\n";
				}
				
				if($("#new_password").val() == ""){
					flag = false;
					errMessage += "New Password\n";
				}
				
				if($("#confirm_new_password").val() == ""){
					flag = false;
					errMessage += "Confirm New Password\n";
				}
				
				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				if(flag && $("#new_password").val() != $("#confirm_new_password").val()){
					flag = false;
					alert("New Password does not match with confirm new password");
					return;
				}
				
				document.form1.submit();
			}
			
			function resetForm(){
				document.form1.reset();
			}
			
			$( document ).ready(function() {
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Update Password</li>
				</ol>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post">
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Old Password</label>
					<div class="col-xs-6">
						<input type="password" class="form-control" placeholder="Old Password" name="old_password" id="old_password"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">New Password</label>
					<div class="col-xs-6">
						<input type="password" class="form-control" placeholder="New Password" name="new_password" id="new_password"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Confirm Password</label>
					<div class="col-xs-6">
						<input type="password" class="form-control" placeholder="Confirm Password" name="confirm_new_password" id="confirm_new_password"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12" style="text-align:right">
						<button type="button" class="btn btn-primary" onclick="submitForm(); return false;">
							<i class="fa fa-floppy-o fa-lg"></i> Save
						</button>
						<button type="button" class="btn btn-danger" onclick="resetForm(); return false;">
							<i class="fa fa-undo fa-lg"></i> Reset
						</button>
						<input type="hidden" id="type" name="type" value="user"/>
						<input type="hidden" id="processType" name="processType" value="update_password"/>
					</div>
				</div>
			</form>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>