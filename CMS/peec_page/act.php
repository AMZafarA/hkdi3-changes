<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";
	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";

	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="peec_page" ) {

		$id = $_REQUEST['peec_page_id'] ?? '';

		$permission = DM::load("peec_page", $id);
		//$permission_name = $permission['peec_name_lang1'];
		$permission_name = 'peec_page';
		$type_name = DM::load("department", 28);
		$type_namelv1 = $type_name['department_name_lang1'];
		$type_name = "Department";
		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], $permission_name)){
				header("location: ../dashboard.php?msg=F-1000");
				return;
			}
		}

		if($processType == "" || $processType == "submit_for_approval") {
			$values = $_REQUEST;
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();
			if($isNew){
				$values['peec_page_publish_status'] = "D";
				$values['peec_status'] = STATUS_ENABLE;
				$values['peec_created_by'] = $_SESSION['sess_id'];
				$values['peec_created_date'] = $timestamp;
				$values['peec_updated_by'] = $_SESSION['sess_id'];
				$values['peec_updated_date'] = $timestamp;
				$values['peec_type'] = "1";

				$id = DM::insert('peec_page', $values);

				if(!file_exists($peec_path . $id. "/")){
					mkdir($peec_path . $id . "/", 0775, true);
				}

			} else {
				$ObjRec = DM::load('peec_page', $id);

				if($ObjRec['peec_status'] == STATUS_DISABLE)
				{
					header("location: list.php&msg=F-1002");
					return;
				}

				$values['peec_page_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';
				$values['peec_updated_by'] = $_SESSION['sess_id'];
				$values['peec_updated_date'] = $timestamp;

				DM::update('peec_page', $values);
			}

			$ObjRec = DM::load('peec_page', $id);
			$peec_img_arr = array('peec_image');
			foreach($peec_img_arr as $key=>$val){
				if($_FILES[$val]['tmp_name'] != ""){
					if($ObjRec[$val] != "" && file_exists($peec_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($peec_path  . $id . "/" . $ObjRec[$val]);
					}
					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);
					$newname = moveFile($peec_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($peec_path . $id . "/".$newname);

					$values = array();
					$values[$val] = $newname;
					$values['peec_page_id'] = $id;

					DM::update('peec_page', $values);
					unset($values);

					$ObjRec = DM::load('peec_page', $id);
				}
			}

			if($processType == ""){
				Logger::log($_SESSION['sess_id'], $timestamp, "peec '" . $ObjRec['peec_name_lang1'] . "' (ID:" . $ObjRec['peec_page_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, $isNew ? "CS" : "ES");
			} else if($processType == "submit_for_approval"){
				ApprovalService::withdrawApproval( $permission_name,$type, $id, $timestamp);
				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['peec_page_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $permission_name, $isNew ? "CS" : "ES");
				$files = array($host_name."peec_page/peec_page.php?peec_page_id=".$id);
				$approval_id = ApprovalService::createApprovalRequest( $permission_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);
			}

			header("location:peec_page.php?peec_page_id=" . $id . "&msg=S-1001");
			return;
		}

		else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('peec_page', $id);

			if($ObjRec == NULL || $ObjRec == "") {
				header("location: list.php&msg=F-1002");
				return;
			}

			if($ObjRec['peec_status'] == STATUS_DISABLE) {
				header("location: list.php&msg=F-1004");
				return;
			}

			$isPublished = SystemUtility::isPublishedRecord("peec", $id);

			$values['peec_page_id'] = $id;
			$values['peec_page_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['peec_status'] = STATUS_DISABLE;
			$values['peec_updated_by'] = $_SESSION['sess_id'];
			$values['peec_updated_date'] = getCurrentTimestamp();
			DM::update('peec_page', $values);
			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("peec", $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "peec '" . $ObjRec['peec_name_lang1'] . "' (ID:" . $ObjRec['peec_page_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec", "DS");

				$files = array($host_name."peec_page/peec_page.php?peec_page_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("peec", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("peec - Higher Diploma - ".$type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "peec", "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec", "D");
			}

			header("location: list.php&msg=S-1002");
			return;
		} else if($processType == "undelete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('peec_page', $id);

			if($ObjRec == NULL || $ObjRec == "") {
				header("location: list.php&msg=F-1002");
				return;
			}

			if($ObjRec['peec_status'] == STATUS_ENABLE || $ObjRec['peec_page_publish_status'] == 'AL') {
				header("location: list.php&msg=F-1006");
				return;
			}

			$values['peec_page_id'] = $id;
			$values['peec_page_publish_status'] = 'D';
			$values['peec_status'] = STATUS_ENABLE;
			$values['peec_updated_by'] = $_SESSION['sess_id'];
			$values['peec_updated_date'] = getCurrentTimestamp();
			DM::update('peec_page', $values);
			unset($values);

			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			$parameters = array(STATUS_ENABLE, 'peec', $id, 'RCA', 'RAA', 'APL');

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;

				DM::update('approval', $values);
				unset($values);

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));

				$recipient_list = array("E", "C");

				if($approval_request['approval_approval_status'] != "RCA"){
					$recipient_list[] = "A";
				}

				$message = "";

				switch($approval_request['approval_approval_status']){
					case "RCA":
						$message = "Request for checking";
					break;
					case "RAA":
						$message = "Request for approval";
					break;
					case "APL":
						$message = "Pending to launch";
					break;
				}

				$message .= " of peec '" . $ObjRec['peec_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'peec', "WA");
			}

			Logger::log($_SESSION['sess_id'], $timestamp, "peec '" . $ObjRec['peec_name_lang1'] . "' (ID:" . $ObjRec['peec_page_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'peec', "UD");
			header("location: list.php&msg=S-1003");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>