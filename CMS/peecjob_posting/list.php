<?php
include "../include/config.php";
include $SYSTEM_BASE . "include/function.php";
include $SYSTEM_BASE . "session.php";
include $SYSTEM_BASE . "include/system_message.php";
include $SYSTEM_BASE . "include/classes/DataMapper.php";
include $SYSTEM_BASE . "include/classes/SearchUtil.php";
include $SYSTEM_BASE . "include/classes/SystemUtility.php";
include $SYSTEM_BASE . "include/classes/PermissionUtility.php";
$msg = $_REQUEST['msg'] ?? '';
DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "peecjob_entries") ){
	header("location: ../dashboard.php?msg=F-1000");
	return;
}
$page_no = $_REQUEST['page_no'] ?? '';
$page_size = 10;
if($page_no == "" || !isInteger($page_no)){
	$page_no = 1;
}
$sql = "";
$parameters = array();

$total_record = DM::count("peecjob_entry", $sql, $parameters);
$total_page = ceil($total_record / $page_size);
if($total_page == 0)
	$total_page = 1;
if($page_no > $total_page){
	$page_no = $total_page;
}
$results = DM::findAllWithPaging("peecjob_entry", $page_no, $page_size, $sql, " date_created DESC ", $parameters);
$headers = DM::findColumnHeaders("peecjob_entry_details", "entry_key");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "../header.php" ?>
	<script language="javascript" src="../js/SearchUtil.js"></script>
	<script>
		function deleteRecord(itemId){
			if(confirm("Are you sure you want to delete this record ?")){
				$("#processType").val('delete');
				$("#subscription_id").val(itemId);
				$('#approvalModalDialog').modal('show');
			}
		}
		function undeleteRecord(itemId){
			if(confirm("Are you sure you want to undelete this record ?")){
				$("#processType").val('undelete');
				$("#subscription_id").val(itemId);
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form2;
					frm.submit();
				})
				$('#loadingModalDialog').modal('show');
			}
		}
		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form2;
					frm.submit();
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		function exportCSV() {
			console.log("~ exportCSV ")
			$.ajax({
				url: './subscribe-export.php',
				type: 'POST',
			}).done(function (data) {
				data = JSON.parse(data)
				if (data.path != '') {
					window.open(data.path, '_blank').focus();
				} 
			});
		}

		$( document ).ready(function() {
			$("#page_first").bind('click', function(){
				$("#page_no").val(1);
				$("#form1").submit();
			});
			$("#page_last").bind('click', function(){
				$("#page_no").val(<?php echo $total_page ?>);
				$("#form1").submit();
			});
			$("#page_prev").bind('click', function(){
				$("#page_no").val(<?php echo $page_no == 1 ? 1 : $page_no - 1 ?>);
				$("#form1").submit();
			});
			$("#page_next").bind('click', function(){
				$("#page_no").val(<?php echo $page_no < $total_page ? $page_no + 1 : $total_page ?>);
				$("#form1").submit();
			});

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>
		});
	</script>
	<style>
		.table > thead > tr > th, .table > tbody > tr > td { vertical-align:middle !important; }
		.table-row { overflow: auto; }
	</style>
</head>
<body>
	<?php include "../menu.php" ?>
	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">

					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li class="active">PEEC Job Postings</li>

			</ol>
			<span class="breadcrumb-right col-xs-2">

			</span>
		</div>
		<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
			<div class="row table-row">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="col-xs-1">#</th>
							<th class="col-xs-3">Submission Date</th>
							<th class="col-xs-3">Job Title</th>
							<th class="col-xs-3">Company</th>
							<th class="col-xs-2">Type</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$count = 1;
						foreach ($results as $result) {
							$r = array();
							$rows = DM::findAll("peecjob_entry_details", "peecjob_entry_id = ?", "", array($result["peecjob_entry_id"]));
							foreach($rows as $row) {
								$r[$row["entry_key"]] = $row["entry_value"];
							}
							?>
							<tr>
								<td><?php echo $count; $count++;?></td>
								<td><?php echo $r["Date"]; ?></td>
								<td><?php echo $r["Job Title (English / Chinese)"]; ?></td>
								<td><?php echo $r["Company Name (English / Chinese)"]; ?></td>
								<td><?php echo ucwords(str_replace("-", " ", $r["Type of Job"])); ?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
					<!-- <thead>
						<tr>
							<th class="col-xs-1">#</th>
							<?php
							foreach ($headers as $h) {
								?>
								<th><?php echo $h["entry_key"]; ?></th>
								<?php
							}
							?>
						</tr>
					</thead> -->
					<!-- <tbody>
						<?php
						$arr = array();
						foreach ($results as $result) {
							$r = array();
							$rows = DM::findAll("peecjob_entry_details", "peecjob_entry_id = ?", "", array($result["peecjob_entry_id"]));
							foreach($rows as $row) {
								$r[$row["entry_key"]] = $row["entry_value"];
							}
							$arr[] = $r;
						}
						$count = 1;
						foreach ($arr as $r) {
							?>
							<tr>
								<td><?php echo $count; ?></td>
								<?php
								foreach($headers as $h) {
									if(isset($r[$h["entry_key"]])) {
										?>
										<td><?php echo $r[$h["entry_key"]]; ?></td>
										<?php
									}
									else {
										?>
										<td><?php echo "N/A"; ?></td>
										<?php
									}
								}
								?>
							</tr>
							<?php
							$count++;
						}
						?>
					</tbody> -->
				</table>
			</div>
			<div class="row" style="float:right">
				<ul class="pager">
					<li>
						<button id="page_first" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-backward"></span> First
						</button>
					</li>
					<li>
						<button id="page_prev" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-chevron-left"></span> Previous
						</button>
					</li>
					<li>
						<button id="page_next" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-chevron-right"></span> Next
						</button>
					</li>
					<li>
						<button id="page_last" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-forward"></span> Last
						</button>
					</li>
				</ul>
				<input type="hidden" id="page_no" name="page_no" value=""/>
			</div>
		</form>
		<div style="clear: both;"></div>
		<div class="row text-right">
			<button class="btn btn-primary" onclick="exportCSV()">Export</button>
		</div>
		<form class="form-horizontal" role="form" id="form2" name="form2" action="act.php" method="post">
			<input type="hidden" id="type" name="type" value="subscription"/>
			<input type="hidden" id="processType" name="processType" value=""/>
			<input type="hidden" id="approval_remarks" name="approval_remarks" value=""/>
			<input type="hidden" id="approval_launch_date" name="approval_launch_date" value=""/>
			<input type="hidden" id="subscription_id" name="subscription_id" value=""/>
		</form>
	</div>
	<!-- End page content -->
	<?php include "../footer.php" ?>
</body>
</html>
