<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="alumni" ) {

		$id = $_REQUEST['alumni_id'] ?? '';
		$alumni_lv1 = $_REQUEST['alumni_lv1'] ?? '';
		$alumni_lv2 = $_REQUEST['alumni_lv2'] ?? '';

		$type_name = DM::load("department", $alumni_lv1);

		$type_namelv1 = $type_name['department_name_lang1'];

		$type_name = DM::load("department", $alumni_lv2);

		$type_namelv2 = $type_name['department_name_lang1'];

		$type_name = "Alumni";

		$image_file = $alumni_path . $id . "/";

		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if($type_namelv2){ $type_name .= " - ".$type_namelv2;}

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "alumni_" . $alumni_lv1."_".$alumni_lv2)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			if($isNew){

				$values['alumni_publish_status'] = "D";
				$values['alumni_status'] = STATUS_ENABLE;
				$values['alumni_created_by'] = $_SESSION['sess_id'];
				$values['alumni_created_date'] = $timestamp;
				$values['alumni_updated_by'] = $_SESSION['sess_id'];
				$values['alumni_updated_date'] = $timestamp;
				$values['alumni_type'] = "1";

				$id = DM::insert('alumni', $values);

				if(!file_exists($alumni_path . $id. "/")){
					mkdir($image_file, 0775, true);
				}

			} else {

				$ObjRec = DM::load('alumni', $id);



				if($ObjRec['alumni_status'] == STATUS_DISABLE)

				{

					header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=F-1002");

					return;

				}


				$values['alumni_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['alumni_updated_by'] = $_SESSION['sess_id'];

				$values['alumni_updated_date'] = $timestamp;



				DM::update('alumni', $values);

			}


			$ObjRec = DM::load('alumni', $id);

			$alumni_img_arr = array('alumni_image','alumni_thumb');

			foreach($alumni_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($image_file . $ObjRec[$val])){
					//	@unlink($image_file . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($image_file, $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


						list($width, $height) = getimagesize($image_file.$newname);

						if($val =='alumni_image'){
							$newWidth ="50";
							$newHeight="80";
						}else{
							$newWidth ="410";
							$newHeight="290";
						}

						cropImage($image_file. $newname, $newWidth,$newHeight , $image_file. $newname);

						/*if($width > '1500'){
							resizeImageByWidth($alumni_path. $id."/". $newname, "1500" , $alumni_path. $id."/". $newname);
						}else if($height > '1000'){
							resizeImageByHeight($alumni_path. $id."/". $newname, "1000" , $alumni_path. $id."/". $newname);
						}*/



					$values = array();
					$values[$val] = $newname;
					$values['alumni_id'] = $id;

					DM::update('alumni', $values);
					unset($values);

					$ObjRec = DM::load('alumni', $id);
				}
			}


		/*	if($processType == "submit_for_approval"){
				$filelist = scandir($image_file);
				$approval_filelist = scandir($image_approval_file);
				var_dump($ObjRec['alumni_image']);
				var_dump($ObjRec['alumni_thumb']);
				var_dump($approval_filelist);
				for($i = 0; $i < count($approval_filelist); $i++ ){
					if(!is_dir($image_approval_file . $approval_filelist[$i])){
						if($approval_filelist[$i] != $ObjRec['alumni_image'] || $approval_filelist[$i] != $ObjRec['alumni_thumb']){
								unlink($image_approval_file . $approval_filelist[$i]);
						}
					}
				}
				for($i = 0; $i < count($filelist); $i++ ){
					var_dump($filelist[$key]);
					if(!is_dir($image_file . $filelist[$i])){
							copy($image_file . $filelist[$i], $image_approval_file . $filelist[$i]);
					}
				}
			}*/

			//*************add image**************//
			$files = $_REQUEST['file_section'] ?? '';
			if($files != "" && is_array($files)){
				foreach($files as $file){
					$file = json_decode($file, true);

					$lang1_files = $file['lang1_files'];
					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){
							if($lang1_file['alumni_image_id'] == "") {

								$lang1_file['alumni_image_pid'] = $id;
								$lang1_file['alumni_image_seq'] = $count++;
								$lang1_file['alumni_image_filename'] = $lang1_file['filename'];
								$lang1_file['alumni_image_alt'] = $lang1_file['alumni_image_alt'];
								$lang1_file['alumni_image_status'] = STATUS_ENABLE;
								$lang1_file['alumni_image_created_by'] = $_SESSION['sess_id'];
								$lang1_file['alumni_image_created_date'] = $timestamp;
								$lang1_file['alumni_image_updated_by'] = $_SESSION['sess_id'];
								$lang1_file['alumni_image_updated_date'] = $timestamp;

								$file_section_id = DM::insert('alumni_image', $lang1_file);

								$file_section_list[] = $file_section_id;
								if(!file_exists($image_file.$file_section_id."/")){
									mkdir($image_file.$file_section_id."/", 0775, true);
								}

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $image_file.$file_section_id."/" . $lang1_file['filename']);


									list($width, $height) = getimagesize($image_file.$file_section_id."/" . $lang1_file['filename']);

									if($width > '1000'){
										resizeImageByWidth($image_file.$file_section_id."/" . $lang1_file['filename'], "1000" , $image_file.$file_section_id."/" . $lang1_file['filename']);
									}else if($height > '1000'){
										resizeImageByHeight($image_file.$file_section_id."/" . $lang1_file['filename'], "1000" , $image_file.$file_section_id."/" . $lang1_file['filename']);
									}


									$crop = $image_file.$file_section_id."/". "crop_".$lang1_file['filename'];
									cropImage($image_file.$file_section_id."/" . $lang1_file['filename'], "125","100" , $crop);

							}else{
								$file_section_list[] = $lang1_file['alumni_image_id'] ?? '';
								$file_section_id = $lang1_file['file_section_id'] ?? '';
								$lang1_file['alumni_image_seq'] = $count++;
								$lang1_file['alumni_image_update_by'] = $_SESSION['sess_id'];
								$lang1_file['alumni_image_update_date'] = $timestamp;

								DM::update('alumni_image', $lang1_file);
							}
						}
					}
				}
			}

			$sql = " alumni_image_status = ? AND alumni_image_pid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($file_section_list) > 0){ print_r($file_section_list);
				$sql .= " AND alumni_image_id NOT IN (" . implode(',', array_fill(0, count($file_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $file_section_list);

			}

			$alumni_section_files = DM::findAll("alumni_image", $sql, " alumni_image_seq ", $parameters);

			foreach($alumni_section_files as $alumni_section_file){

				$values = array();
				$values['alumni_image_id'] = $alumni_section_file['alumni_image_id'];
				$values['alumni_image_status'] = STATUS_DISABLE;
				$values['alumni_image_update_by'] = $_SESSION['sess_id'];
				$values['alumni_image_update_date'] = $timestamp;

				DM::update('alumni_image', $values);

				unset($values);
			}

			//*************add image**************//



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "alumni '" . $ObjRec['alumni_name_lang1'] . "' (ID:" . $ObjRec['alumni_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "alumni_" . $alumni_lv1."_".$alumni_lv2, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "alumni_" . $alumni_lv1."_".$alumni_lv2,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['alumni_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "alumni_" . $alumni_lv1."_".$alumni_lv2, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."alumni/alumni.php?alumni_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "alumni_" . $alumni_lv1."_".$alumni_lv2, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "alumni_" . $alumni_lv1."_".$alumni_lv2, "PA", $approval_id);

			}

			header("location:alumni.php?alumni_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('alumni', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['alumni_status'] == STATUS_DISABLE)

			{

				header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("alumni", $id);

			$values['alumni_id'] = $id;
			$values['alumni_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['alumni_status'] = STATUS_DISABLE;
			$values['alumni_updated_by'] = $_SESSION['sess_id'];
			$values['alumni_updated_date'] = getCurrentTimestamp();


			DM::update('alumni', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("alumni_" . $alumni_lv1."_".$alumni_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "alumni '" . $ObjRec['alumni_name_lang1'] . "' (ID:" . $ObjRec['alumni_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("alumni_" . $alumni_lv1."_".$alumni_lv2, $id, $timestamp, $_SESSION['sess_id'], array("E"), "alumni_" . $alumni_lv1."_".$alumni_lv2, "DS");

				$files = array($SYSTEM_HOST."alumni/alumni.php?alumni_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("alumni_" . $alumni_lv1."_".$alumni_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("alumni_" . $alumni_lv1."_".$alumni_lv2, $id, $timestamp, $_SESSION['sess_id'], array("A"), "alumni_" . $alumni_lv1."_".$alumni_lv2, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "alumni_" . $alumni_lv1."_".$alumni_lv2, "D");
			}





			header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('alumni', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['alumni_status'] == STATUS_ENABLE || $ObjRec['alumni_publish_status'] == 'AL')

			{

				header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=F-1006");

				return;

			}



			$values['alumni_id'] = $id;

			$values['alumni_publish_status'] = 'D';

			$values['alumni_status'] = STATUS_ENABLE;

			$values['alumni_updated_by'] = $_SESSION['sess_id'];

			$values['alumni_updated_date'] = getCurrentTimestamp();



			DM::update('alumni', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'alumni', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of alumni '" . $ObjRec['alumni_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "alumni_" . $alumni_lv1."_".$alumni_lv2, "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "alumni '" . $ObjRec['alumni_name_lang1'] . "' (ID:" . $ObjRec['alumni_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "alumni_" . $alumni_lv1."_".$alumni_lv2, "UD");



			header("location: list.php?alumni_lv1=".$alumni_lv1."&alumni_lv2=".$alumni_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
