<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$section_header = 'cimt';

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], $section_header)){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$product_id = "4";

	include_once "content.php";
?>