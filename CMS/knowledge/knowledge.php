<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "cimt")){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$product_id = "4";	
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($product_id != ""){
		if(!isInteger($product_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		$obj_row = DM::load('product', $product_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		if(!file_exists($product_path . $product_id)){
			mkdir($product_path . $product_id, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "product/" . $product_id . "/";
		
		/*$menu = DM::findOne("menu", " menu_status = ? AND menu_link_type = ? AND menu_link_page_id = ? ", " menu_id ", array(STATUS_ENABLE, 'U', $page_id));
	
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "page_" . $menu['menu_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}*/
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
		
/*		if(!file_exists($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id)){
			mkdir($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id, 0775, true);
		}*/
		
	} else {
		/*header("location: list.php?msg=F-1002");
		return;*/
		$product_publish_status = 'D';
		
		$length = 10;

		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		
		if(!file_exists($product_path . $randomString)){
			mkdir($product_path . $randomString, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "product/" . $randomString . "/";
	}
	
	
	$product_section = DM::findAll('product_section', ' product_section_status = ? And product_section_pid = ? ', " product_section_sequence ", array(STATUS_ENABLE,$product_id));
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
        <script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
        <script type="text/x-tmpl" id="text_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							Text Section
						</label>
						<label class="col-xs-2 control-label">
							Background Color
						</label>
						<span class="col-xs-2">
							<div class="input-group color_code">
								<select class="form-control" name="product_section_background_color">
									<option value="0">White</option>
									<option value="1">Red</option>
								</select>
							</div>
						</span>
						<span class="col-xs-6" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="T"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo $product_section_name_lang1?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang1" name="product_section_title_lang1" placeholder="Title (English)" value="<?php echo $product_section_title_lang1?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $product_section_name_lang2?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang2" name="product_section_title_lang2" placeholder="Title (繁中)" value="<?php echo $product_section_title_lang2?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $product_section_name_lang3?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang3" name="product_section_title_lang3" placeholder="Title (簡中)" value="<?php echo $product_section_title_lang3?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="product_section_content_lang3"></textarea>
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</script>

		<script type="text/x-tmpl" id="news_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-6 control-label">
							News Section
						</label>
						<span class="col-xs-6" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="N"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo $product_section_name_lang1?>">
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $product_section_name_lang2?>">
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $product_section_name_lang3?>">
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</script>


		<script type="text/x-tmpl" id="project_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-6 control-label">
							Project Section
						</label>
						<span class="col-xs-6" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="P"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo $product_section_name_lang1?>">
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $product_section_name_lang2?>">
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $product_section_name_lang3?>">
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</script>


		<script type="text/x-tmpl" id="team_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							Team Section
						</label>
						<span class="col-xs-10" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="Team"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-2">繁中</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-3">簡中</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-4">Partner</a></li>
						</ul>
						<div id="tabs-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (繁中)"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (簡中)"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-4">
							<div class="form-group">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-4">
											<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</script>

		<script type="text/x-tmpl" id="Partner_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Title
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_title" placeholder="Title" value="{%=o.filename%}"/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="product_section_file_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
					<input type="hidden" name="product_section_file_type" value="Partner"/>
					<input type="hidden" name="product_section_file_url" value=""/>
				</div>
				
			</div>
		</script>

		<script type="text/x-tmpl" id="contact_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							Contact Section
						</label>
						<span class="col-xs-10" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="C"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-{%=o.tmp_id%}-1">Info</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-2">English</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-3">繁中</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-4">簡中</a></li>
						</ul>
						<div id="tabs-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_email" placeholder="E-mail"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_tel" placeholder="Tel"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_content_lang1" placeholder="Address (English)"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_content_lang2" placeholder="Address (繁中)"/>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-4">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_content_lang3" placeholder="Address (簡中)"/>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</script>
        
		<script>

			var file_list = {};
			var counter = 0;
			
			function checkForm(){				
				var flag=true;
				var errMessage="";
				

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');

			}
			
			function checkForm2(){
				var flag=true;				
				
					$('#approvalModalDialog').modal('show');

			}
			
			
			function cloneTextBlock(idx){
				if($("input[name='product_section_type'][value='T']").length <= idx){
					cloneNewsBlock(0)
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='T']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneTextBlock(++idx);
			}

			function cloneNewsBlock(idx){
				if($("input[name='product_section_type'][value='N']").length <= idx){
					cloneProjectBlock(0);
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='N']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','news_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneNewsBlock(++idx);
			}

			function cloneProjectBlock(idx){
				if($("input[name='product_section_type'][value='P']").length <= idx){
					cloneTeamBlock(0);
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='P']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','project_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneProjectBlock(++idx);
			}

			function cloneTeamBlock(idx){
				if($("input[name='product_section_type'][value='Team']").length <= idx){
					cloneContactBlock(0);
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='Team']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));


				var obj = {"product_section_type":"Team"};				
				obj['product_section_id'] = $(frm).find("input[name='product_section_id']").val();
				obj['product_section_name_lang1'] = $(frm).find("input[name='product_section_name_lang1']").val();
				obj['product_section_name_lang2'] = $(frm).find("input[name='product_section_name_lang2']").val();
				obj['product_section_name_lang3'] = $(frm).find("input[name='product_section_name_lang3']").val();
				
				obj['product_section_tid'] = $(frm).find("input[name='product_section_tid']").val();
				obj['product_section_sequence'] = $(frm).find("input[name='product_section_sequence']").val();
				obj['lang1_files'] = [];

				//var obj = JSON.stringify(cloneFormToObject(frm));
				//obj['lang1_files'] = [];

				
				cloneFileElement(obj, idx, 0);
			}



			function cloneFileElement(dataObj, parentIdx, elementIdx){
				var frm = $("input[name='product_section_type'][value='Team']:eq(" + parentIdx + ")").closest("form");
				
				if($(frm).find("div.file_row").length <= elementIdx){
					var data = JSON.stringify(dataObj);
					$('<input>').attr('name','team_section[]').attr('type','hidden').val(data).appendTo('#form1');					
					cloneTeamBlock(++parentIdx);
					return;
				}
				
				var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");
				
					var fileObj = {};
					fileObj['product_section_file_title'] = $(row).find("input[name='product_section_file_title']").val();
					fileObj['product_section_file_detail'] = $(row).find("input[name='product_section_file_detail']").val();
					fileObj['product_section_file_link'] = $(row).find("input[name='product_section_file_link']").val();
					fileObj['product_section_file_id'] = $(row).find("input[name='product_section_file_id']").val();
					fileObj['product_section_file_lang'] = $(row).find("input[name='product_section_file_lang']").val();
					fileObj['product_section_file_type'] = $(row).find("input[name='product_section_file_type']").val();
					fileObj['product_section_file_url'] = $(row).find("input[name='product_section_file_url']").val();
					fileObj['tmp_filename'] = "";
					fileObj['filename'] = "";

					var fd = new FormData();
					fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);
					
					$.ajax({
						url: "../pre_upload/act.php",
						data: fd,
						processData: false,
						contentType: false,
						type: 'POST',
						dataType : "json",
						success: function(data) {
							fileObj['tmp_filename'] = data.tmpname;
							fileObj['filename'] = data.filename;
							
								dataObj['lang1_files'].push(fileObj);
							
							cloneFileElement(dataObj, parentIdx, ++elementIdx);
						}
					});
			}


			function cloneContactBlock(idx){
				if($("input[name='product_section_type'][value='C']").length <= idx){
					document.form1.submit();
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='C']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','contact_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneContactBlock(++idx);
			}
			
			
			
			function moveUp(button){
				var $current = $(button).closest(".section_row");
				var $previous = $current.prev('.section_row');
				if($previous.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertBefore($previous);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveDown(button){
				var $current = $(button).closest(".section_row");
				var $next = $current.next('.section_row');
				if($next.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertAfter($next);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function removeRow(button){
				$(button).closest(".section_row").detach();
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}

			function moveFileUp(button){
				var $current = $(button).closest(".file_row");
				var $previous = $current.prev('.file_row');
				if($previous.length !== 0){
					$current.insertBefore($previous);
				}
			}
			
			function moveFileDown(button){
				var $current = $(button).closest(".file_row");
				var $next = $current.next('.file_row');
				if($next.length !== 0){
					$current.insertAfter($next);
				}
			}
			
			function removeFileItem(button){
				$(button).closest(".file_row").detach();				
			}
			
			/*function addTextBlock(tab_id){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}*/

			function addFile(fileBrowser, language){
				if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
						return;
				
				var files = $(fileBrowser)[0].files;
				
				for(var i = 0; i < files.length; i++){
					var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
					file_list[obj.tmp_id] = files[i];
					var content = tmpl("Partner_template", obj);
					var header_row = $(fileBrowser).closest("div.form-group");
					
					if($(header_row).is(":last-child")){
						$(content).insertAfter(header_row);
					} else {
						$(content).insertAfter($(header_row).siblings(":last"));
					}
				}
			}
			
			function addTextBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				//$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}

			function addNewsBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("news_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}

			function addProjectBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("project_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}

			function addTeamBlock(tab_id){
				var tmp_id = "tmp_" + (counter++)  ;
				var obj = {tmp_id:tmp_id};
				var content = tmpl("team_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				$("#tabs-" + tmp_id).tabs({});
				$("#tabs-" + tmp_id + " input[name='upload_eng_pdf_file']").filestyle({input:false, buttonText: "&nbsp;Choose file", badge:false,iconName: "glyphicon-file"});
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addContactBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("contact_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});

			
			}
			
			function submitForm(){
				document.form1.submit();
			}
			
			function removeIcon(button){
				$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				$("#tabs").tabs({});

				$("#upload_icon").filestyle({buttonText: ""});				
				$("#product_banner").filestyle({buttonText: ""});
				
				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});
				
				$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
					$(element).find("div.col-xs-12[id^='tabs']").tabs({});
					$(element).find('textarea').tinymce(tinymceConfig);
				});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
			.section_row {
				margin-bottom:10px; 
				padding: 7px 15px 4px;
				border: 1px solid #ccc;
				border-radius: 4px;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><?php echo $product_name_lang1?></li>
					<li class="active">Update</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
            
            <div id="top_submit_button_row" class="row" style="padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
            
            
			<div class="row">
				<table class="table" style="margin-bottom: 5px;">
					<tbody>
						<tr class="<?php echo ($product_publish_status == "RCA" || $product_publish_status == "RAA") ? "danger" : "" ?>">
							<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
							<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
								<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $product_publish_status] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
            
            
            <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td class="col-xs-12" style="vertical-align:middle" colspan="2">
									<div class="row">
										<div class="col-xs-12">
											<a role="button" class="btn btn-default" onclick="addTextBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Text Section
											</a>
											<a role="button" class="btn btn-default" onclick="addNewsBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> News Section
											</a>
											<a role="button" class="btn btn-default" onclick="addProjectBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Project Section
											</a>
											<a role="button" class="btn btn-default" onclick="addTeamBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Team Section
											</a>
											<a role="button" class="btn btn-default" onclick="addContactBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Contact Section
											</a>
										</div>
									
									</div>
								</td>
							</tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Banner</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="product_banner" name="product_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										<!--(Width: <?php echo $PRODUCT_BANNER_WIDTH ?>px, Height: <?php echo $PRODUCT_BANNER_HEIGHT ?>px)-->
									</p>
								</div>
								<?php if($product_banner != "" && file_exists($product_path . $product_id . "/" . $product_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
								</td>
                            </tr>
                            
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Title (English)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <input type="text" class="form-control" name="product_name_lang1" placeholder="Title (English)" value="<?php echo $product_name_lang1?>">
								</td>
							</tr>

							<tr>
								<td class="col-xs-2" style="vertical-align:middle">Title (繁中)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <input type="text" class="form-control" name="product_name_lang2" placeholder="Title (繁中)" value="<?php echo $product_name_lang2?>">
								</td>
							</tr>

							<tr>
								<td class="col-xs-2" style="vertical-align:middle">Title (簡中)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <input type="text" class="form-control" name="product_name_lang3" placeholder="Title (簡中)" value="<?php echo $product_name_lang3?>">
								</td>
							</tr>
                           

						</tbody>
					</table>
                    </div>
				<input type="hidden" name="type" value="product" />
				<input type="hidden" name="processType" id="processType" value="">
				<input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
                <input type="hidden" name="product_type" value="1" />
				<input type="hidden" name="product_status" value="1" />
				<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
				<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
                <input type="hidden" name="randomString" value="<?php echo $randomString ?>" />
                <input type="hidden" name="master_lv1" id="master_lv1" value="<?php echo $master_lv1 ?>" />
                <input type="hidden" name="master_lv2" id="master_lv2" value="<?php echo $master_lv2 ?>" />
                
			</form>
            
            <?php foreach($product_section as $value) {
            	if($value['product_section_type'] == "T") {
					printTextBlock($value);
				} else if($value['product_section_type'] == "N"){
					printNewsBlock($value);
				}else if($value['product_section_type'] == "P"){
					printProjectBlock($value);
				}else if($value['product_section_type'] == "Team"){
					printTeamBlock($value);
				}else if($value['product_section_type'] == "C"){
					printContactBlock($value);
				}
			} ?>
            
			
			<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>

<?php
	function printTextBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-2 control-label">
                    Text Block
                </label>
            	<label class="col-xs-2 control-label">
                    Background Color
                </label>
                <span class="col-xs-2">
                    <div class="input-group color_code">
                        <select class="form-control" name="product_section_background_color">
                            <option value="0" <?php echo "0" == $record['product_section_background_color'] ? "selected" : "" ?> >White</option>
                            <option value="1" <?php echo "1" == $record['product_section_background_color'] ? "selected" : "" ?>>Red</option>
                        </select>
                    </div>
                </span>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="T"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['product_section_name_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang1" name="product_section_title_lang1" placeholder="Title (English)" value="<?php echo htmlspecialchars($record['product_section_title_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['product_section_name_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang2" name="product_section_title_lang2" placeholder="Title (繁中)" value="<?php echo htmlspecialchars($record['product_section_title_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['product_section_name_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang3" name="product_section_title_lang3" placeholder="Title (簡中)" value="<?php echo htmlspecialchars($record['product_section_title_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="product_section_content_lang3"><?php echo htmlspecialchars($record['product_section_content_lang3']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
                
			</div>
		</form>
	</div>
<?php
	}   
?>


<?php
	function printNewsBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-6 control-label">
                    News Block
                </label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="N"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['product_section_name_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['product_section_name_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['product_section_name_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
	}   
?>


<?php
	function printProjectBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-6 control-label">
                    Project Block
                </label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="P"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['product_section_name_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['product_section_name_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['product_section_name_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
	}   
?>


<?php
	function printContactBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-6 control-label">
                    Contact Block
                </label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="C"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">Info</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_email" placeholder="E-mail" value="<?php echo htmlspecialchars($record['product_section_email']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_tel" placeholder="Tel" value="<?php echo htmlspecialchars($record['product_section_tel']) ?>"/>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1" placeholder="Address (English)" value="<?php echo htmlspecialchars($record['product_section_content_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2" placeholder="Address (繁中)" value="<?php echo htmlspecialchars($record['product_section_content_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="product_section_content_lang3" placeholder="Address (簡中)" value="<?php echo htmlspecialchars($record['product_section_content_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
	}   
?>


<?php
	function printTeamBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
		$lang1_files = DM::findAll("product_section_file", " product_section_file_status = ? AND product_section_file_psid = ? AND product_section_file_lang = ? ", " product_section_file_sequence ", array(STATUS_ENABLE, $record['product_section_id'], 'lang1'));

?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-6 control-label">
                    Team Block
                </label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="Team"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">Partner</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="product_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['product_section_name_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="product_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['product_section_name_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="product_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['product_section_name_lang3']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-4">
									<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
								</div>
							</div>
						</div>
					</div>
					<?php
						foreach($lang1_files as $lang1_file) { 
							printFileItem($lang1_file);
						}
					?>

				</div>
			</div>
		</form>
	</div>
<?php
	}   


	function printFileItem($fileItem){
?>
	<?php if($fileItem['product_section_file_type'] == "Partner") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
            <div class="col-xs-1">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Title
                    </div>
                </div>
            </div>
			<div class="col-xs-8">
            	<div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_title" placeholder="Title (English)" value="<?php echo htmlspecialchars($fileItem['product_section_file_title']) ?>"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="../../uploaded_files/product_file/<?php echo $fileItem['product_section_file_pid'] . "/". $fileItem['product_section_file_id'] ?>/<?php echo $fileItem['product_section_file_filename'] ?> " target="_blank">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="product_section_file_id" value="<?php echo htmlspecialchars($fileItem['product_section_file_id']) ?>"/>
				<input type="hidden" name="product_section_file_lang" value="<?php echo htmlspecialchars($fileItem['product_section_file_lang']) ?>"/>
				<input type="hidden" name="product_section_file_type" value="<?php echo htmlspecialchars($fileItem['product_section_file_type']) ?>"/>
				<input type="hidden" name="product_section_file_url" value=""/>
			</div>
		</div>
	<?php } ?>
<?php
	}
?>