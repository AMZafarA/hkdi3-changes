<?php
	include "../include/config.php";
	include $SYSTEM_BASE . "include/function.php";
	include $SYSTEM_BASE . "session.php";
	include $SYSTEM_BASE . "include/system_message.php";
	include $SYSTEM_BASE . "include/classes/DataMapper.php";
	include $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$student_award_id = $_REQUEST['student_award_id'] ?? '';
	$student_award_type = $_REQUEST['student_award_type'] ?? '';
	$student_award_lv1 = $_REQUEST['student_award_lv1'] ?? '';
	$student_award_lv2 = $_REQUEST['student_award_lv2'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';	
	
	if(!isInteger($student_award_id))
	{
		$student_award_id = "";
	}
	
	if($student_award_id != ""){
		$obj_row = DM::load('student_award', $student_award_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}

		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "student_award_" . $student_award_lv1."_".$student_award_lv2)){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}

		if(!file_exists($student_award_path . $student_award_id)){
			mkdir($student_award_path . $student_award_id, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "student_award/" . $student_award_id . "/";
		

		$last_updated_user = DM::load('user', $student_award_updated_by);

	} else if(!PermissionUtility::canCreateByKey($_SESSION['sess_id'], "student_award_" . $student_award_lv1."_".$student_award_lv2)){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	} else {
		$student_award_publish_status = "D";
	} 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
        <script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.cookie.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/bootstrap-colorpicker.min.js"></script>
		<link href="<?php echo $SYSTEM_HOST ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">


		<script type="text/x-tmpl" id="PDF_file_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Alt
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="student_award_image_alt" placeholder="Alt" value="{%=o.filename%}"/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="student_award_image_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
				</div>
				
			</div>
		</script>
		<script>
			var file_list = {};
			var counter = 0;

			function checkForm(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#press_release_student_award_date").val() == ""){
					flag = false;
					errMessage += "student_award Date\n";
				}
				
				if($("#student_award_start_show_date").val() == ""){
					flag = false;
					errMessage += "Start Display Date\n";
				}
				
				if($("#student_award_eng_title").val() == ""){
					flag = false;
					errMessage += "Title (English)\n";
				}
				
				if($("#student_award_chi_title").val() == ""){
					flag = false;
					errMessage += "Title (中文)\n";
				}
				
				<?php if($student_award_id == "") { ?>
					if($("#upload_eng_pdf").val() == ""){
						flag = false;
						errMessage += "PDF (English)\n";
					}
					
					if($("#upload_chi_pdf").val() == ""){
						flag = false;
						errMessage += "PDF (中文)\n";
					}
				<?php } ?>

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					submitForm();
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			function checkForm2(){
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#student_award_student_award_date").val() == ""){
					flag = false;
					errMessage += "student_award Date\n";
				}
				
				if($("#student_award_start_show_date").val() == ""){
					flag = false;
					errMessage += "Start Display Date\n";
				}
				
				if($("#student_award_eng_title").val() == ""){
					flag = false;
					errMessage += "Title (English)\n";
				}
				
				if($("#student_award_chi_title").val() == ""){
					flag = false;
					errMessage += "Title (中文)\n";
				}
				
				<?php if($student_award_id == "") { ?>
					if($("#upload_eng_pdf").val() == ""){
						flag = false;
						errMessage += "PDF (English)\n";
					}
					
					if($("#upload_chi_pdf").val() == ""){
						flag = false;
						errMessage += "PDF (中文)\n";
					}
				<?php } ?>

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$('#approvalModalDialog').modal('show');
			}
			
			function checkForm3(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					$('#processType').val('submit_for_production');
					submitForm();
				})
				
				$('#loadingModalDialog').modal('show');
				$('#processType').val('');
			}

			function corp(){
				if (confirm('Are you sure you want to corp the image?')) {
				    $('#processType').val('corp');
				document.form1.submit();
				} else {
				    // Do nothing!
				}
				
			}
			
			function submitForm(){
				var obj = {"product_section_type":"F"};		
				obj['lang1_files'] = [];
				cloneFile(obj,0);	
				//document.form1.submit();
			}

			function removeAimg(button){
				//$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}

			function removeSimg(button){
				//$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}

			function convert_tchi_to_schi() {
				$("#properties_name_lang3").val(toSimplifiedString($("#properties_name_lang2").val()));
				$("#properties_address_lang3").val(toSimplifiedString($("#properties_address_lang2").val()));
				$("#properties_parking_lang3").val(toSimplifiedString($("#properties_parking_lang2").val()));
				$("#properties_lift_lang3").val(toSimplifiedString($("#properties_lift_lang2").val()));
				$("#properties_floor_height_lang3").val(toSimplifiedString($("#properties_floor_height_lang2").val()));
				$("#properties_floor_loading_lang3").val(toSimplifiedString($("#properties_floor_loading_lang2").val()));
				$("#properties_park_entrance_lang3").val(toSimplifiedString($("#properties_park_entrance_lang2").val()));
				$("#properties_cargo_lift_lang3").val(toSimplifiedString($("#properties_cargo_lift_lang2").val()));
				$("#properties_cargo_lift_max_load_lang3").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang2").val()));
				$("#properties_air_conditioning_lang3").val(toSimplifiedString($("#properties_air_conditioning_lang2").val()));
				$("#properties_fire_services_lang3").val(toSimplifiedString($("#properties_fire_services_lang2").val()));
				$("#properties_security_lang3").val(toSimplifiedString($("#properties_security_lang2").val()));
				$("#properties_transportaion_lang3").val(toSimplifiedString($("#properties_transportaion_lang2").val()));
				$("#properties_enquiries_lang3").val(toSimplifiedString($("#properties_enquiries_lang2").val()));
				$("#properties_enquiries2_lang3").val(toSimplifiedString($("#properties_enquiries2_lang2").val()));
				$("#properties_keywords_lang3").val(toSimplifiedString($("#properties_keywords_lang2").val()));
				$("#properties_description_lang3").val(toSimplifiedString($("#properties_description_lang2").val()));
			};
			
			function convert_schi_to_tchi() {
				$("#properties_name_lang2").val(toTraditionalString($("#properties_name_lang3").val()));
				$("#properties_address_lang2").val(toTraditionalString($("#properties_address_lang3").val()));
				$("#properties_parking_lang2").val(toTraditionalString($("#properties_parking_lang3").val()));
				$("#properties_lift_lang2").val(toTraditionalString($("#properties_lift_lang3").val()));
				$("#properties_floor_height_lang2").val(toTraditionalString($("#properties_floor_height_lang3").val()));
				$("#properties_floor_loading_lang2").val(toTraditionalString($("#properties_floor_loading_lang3").val()));
				$("#properties_park_entrance_lang2").val(toTraditionalString($("#properties_park_entrance_lang3").val()));
				$("#properties_cargo_lift_lang2").val(toTraditionalString($("#properties_cargo_lift_lang3").val()));
				$("#properties_cargo_lift_max_load_lang2").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang3").val()));
				$("#properties_air_conditioning_lang2").val(toSimplifiedString($("#properties_air_conditioning_lang3").val()));
				$("#properties_fire_services_lang2").val(toSimplifiedString($("#properties_fire_services_lang3").val()));
				$("#properties_security_lang2").val(toSimplifiedString($("#properties_security_lang3").val()));
				$("#properties_transportaion_lang2").val(toTraditionalString($("#properties_transportaion_lang3").val()));
				$("#properties_enquiries_lang2").val(toTraditionalString($("#properties_enquiries_lang3").val()));
				$("#properties_enquiries2_lang2").val(toTraditionalString($("#properties_enquiries2_lang3").val()));
				$("#properties_keywords_lang2").val(toSimplifiedString($("#properties_keywords_lang3").val()));
				$("#properties_description_lang2").val(toSimplifiedString($("#properties_description_lang3").val()));
				
			};


			function addFile(fileBrowser, language){
				if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
						return;
				
				var files = $(fileBrowser)[0].files;
				
				for(var i = 0; i < files.length; i++){
					var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
					file_list[obj.tmp_id] = files[i];
					var content = tmpl("PDF_file_template", obj);
					var header_row = $(fileBrowser).closest("div.form-group");
					
					if($(header_row).is(":last-child")){
						$(content).insertAfter(header_row);
					} else {
						$(content).insertAfter($(header_row).siblings(":last"));
					}
				}
			}
			
			function cloneFile(dataObj, elementIdx){ 
				var frm = $("#form1");
				
				if($(frm).find("div.file_row").length <= elementIdx){
					var data = JSON.stringify(dataObj);
					$('<input>').attr('name','file_section[]').attr('type','hidden').val(data).appendTo('#form1');	
					document.form1.submit();					
					return;
				}
				
				var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");
				
				//var files =$("input[name='project_image_filename']")[0].files;
				
				/*if(files.length == 0){

					return;
				}*/
				

				
				var fileObj = {};
				fileObj['student_award_image_alt'] = $(row).find("input[name='student_award_image_alt']").val();
				fileObj['student_award_image_id'] = $(row).find("input[name='student_award_image_id']").val();
				fileObj['tmp_filename'] = "";
				fileObj['filename'] = "";
				
				
				
	


				var fd = new FormData();
				//fd.append('upload_file', files[0]);
				fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);
				
				$.ajax({
					url: "../pre_upload/act.php",
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					dataType : "json",
					success: function(data) {
						fileObj['tmp_filename'] = data.tmpname;
						fileObj['filename'] = data.filename;
				
						dataObj['lang1_files'].push(fileObj);
						cloneFile(dataObj, ++elementIdx);
						//$('<input>').attr('name','file_section[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');
						
						//document.form1.submit();
					}
				});
				

			}
			
			function moveFileUp(button){
				var $current = $(button).closest(".file_row");
				var $previous = $current.prev('.file_row');
				if($previous.length !== 0){
					$current.insertBefore($previous);
				}
			}
			
			function moveFileDown(button){
				var $current = $(button).closest(".file_row");
				var $next = $current.next('.file_row');
				if($next.length !== 0){
					$current.insertAfter($next);
				}
			}
			
			function removeFileItem(button){
				$(button).closest(".file_row").detach();				
			}


			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						submitForm();
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				$("#tabs").tabs({});
			
				$(".student_award_student_image,.student_award_award_image").filestyle({buttonText: ""});

				$('.tinymcetextarea').tinymce(tinymceConfig);

				$("#news_to_date,#news_from_date,#news_date").datepicker({"dateFormat" : "yy-mm-dd"});



				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
				
			});
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
		</style>


	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Student Award</li>
					<?php
						$department = DM::load("department", $student_award_lv1);
					?>
					<li class="active"><?php echo $department['department_name_lang1']?></li>

					<?php
						$department = DM::load("department", $student_award_lv2);
					?>
					<li class="active"><a href="list.php?student_award_lv1=<?php echo $student_award_lv1?>&student_award_lv2=<?php echo $student_award_lv2?>"><?php echo $department['department_name_lang1']?></a></li>
					<li class="active"><?php echo $student_award_id == "" ? "Create" : "Update" ?></li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>

			<div id="top_submit_button_row" class="row" style="padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="previewPost(); return false;">
						<i class="fa fa-eye fa-lg"></i> Preview
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>

			<div class="row">
				<table class="table" style="margin-bottom: 5px;">
					<tbody>
						<tr class="<?php echo ($student_award_publish_status == "RCA" || $student_award_publish_status == "RAA") ? "danger" : "" ?>">
							<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
							<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
								<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $student_award_publish_status] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
						<?php for($i=1;$i <=$student_award_type;$i++){ ?>
						<li><a href="#tabs-<?php echo $i?>">Block <?php echo $i?></a></li>
						<?php } ?>
					</ul>
                    
                    	<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
                    <?php for($i=1;$i <=$student_award_type;$i++){ ?>
					<div id="tabs-<?php echo $i?>">

							<?php if($i==1){ ?>

							<div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Priority</label>
	                            <div class="col-xs-6">
	                                <input type="text" class="form-control" placeholder="Priority" name="student_award_seq" id="student_award_seq" value="<?php echo $student_award_seq ?>"/>
	                            </div>
	                        </div>


							<?php } ?>
							<div class="form-group">
                                <label class="col-xs-2 control-label">Student Image</label>
                                <div class="col-xs-5">
                                    <input type="file" class="student_award_student_image" id="student_award_student_image<?php echo $i?>" name="student_award_student_image<?php echo $i?>" accept="image/*"/>
                                    <p style="padding-top:5px;padding-left:5px;">
                                        (Width: 620px, Height: 620px)
                                    </p>
                                </div>
                                <?php if(${"student_award_student_image".$i} != "" && file_exists($student_award_path . $student_award_id . "/" . ${"student_award_student_image".$i})) { ?>
                                    <div class='col-xs-5'>
                                        <a role="button" class="btn btn-default" href="<?php echo $student_award_url . $student_award_id . "/" . ${"student_award_student_image".$i} ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
                                        <a role="button" class="btn btn-default" href="#" onclick="removeSimg(this); return false;"><i class="glyphicon glyphicon-remove"></i> Remove</a>
                                        <input type="hidden" name="removeSimg<?php echo $i?>" value='1'>
                                    </div>
    							<?php } ?>
                            </div>

                             <div class="form-group">
                                <label class="col-xs-2 control-label">Award Image</label>
                                <div class="col-xs-5">
                                    <input type="file" class="student_award_award_image" id="student_award_award_image<?php echo $i?>" name="student_award_award_image<?php echo $i?>" accept="image/*"/>
                                    <p style="padding-top:5px;padding-left:5px;">
                                        (Width: 620px, Height: 620px)
                                    </p>
                                </div>
                                <?php if(${"student_award_award_image".$i} != "" && file_exists($student_award_path . $student_award_id . "/" . ${"student_award_award_image".$i})) { ?>
                                    <div class='col-xs-5'>
                                        <a role="button" class="btn btn-default" href="<?php echo $student_award_url . $student_award_id . "/" . ${"student_award_award_image".$i} ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
                                        <a role="button" class="btn btn-default" href="#" onclick="removeAimg(this); return false;"><i class="glyphicon glyphicon-remove"></i> Remove</a>
                                        <input type="hidden" name="removeAimg<?php echo $i?>" value='1'>
                                    </div>
    							<?php } ?>
                            </div>

                            

                            <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Student Name(Eng)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Student Name(Eng)" name="student_award_student_name<?php echo $i?>_lang1" id="student_award_student_name<?php echo $i?>_lang1" value="<?php echo ${"student_award_student_name".$i."_lang1"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Student Name(繁中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Student Name(繁中)" name="student_award_student_name<?php echo $i?>_lang2" id="student_award_student_name<?php echo $i?>_lang2" value="<?php echo ${"student_award_student_name".$i."_lang2"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Student Name(簡中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Student Name(簡中)" name="student_award_student_name<?php echo $i?>_lang3" id="student_award_student_name<?php echo $i?>_lang3" value="<?php echo ${"student_award_student_name".$i."_lang3"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Programmes(Eng)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Programmes(Eng)" name="student_award_program<?php echo $i?>_lang1" id="student_award_program<?php echo $i?>_lang1" value="<?php echo ${"student_award_program".$i."_lang1"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Programmes(繁中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Programmes(繁中)" name="student_award_program<?php echo $i?>_lang2" id="student_award_program<?php echo $i?>_lang2" value="<?php echo ${"student_award_program".$i."_lang2"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Programmes(簡中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Programmes(簡中)" name="student_award_program<?php echo $i?>_lang3" id="student_award_program<?php echo $i?>_lang3" value="<?php echo ${"student_award_program".$i."_lang3"} ?>"/>
	                            </div>
	                        </div> 

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Detail(Eng)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Detail(Eng)" name="student_award_detail<?php echo $i?>_lang1" id="student_award_detail<?php echo $i?>_lang1" ><?php echo ${"student_award_detail".$i."_lang1"}?></textarea>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Detail(繁中)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Detail(繁中)" name="student_award_detail<?php echo $i?>_lang2" id="student_award_detail<?php echo $i?>_lang2" ><?php echo ${"student_award_detail".$i."_lang2"}?></textarea>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Detail(簡中)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Detail(簡中)" name="student_award_detail<?php echo $i?>_lang3" id="student_award_detail<?php echo $i?>_lang3" ><?php echo ${"student_award_detail".$i."_lang3"}?></textarea>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Name(Eng)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Award Name(Eng)" name="student_award_award<?php echo $i?>_lang1" id="student_award_award<?php echo $i?>_lang1" value="<?php echo ${"student_award_award".$i."_lang1"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Name(繁中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Award Name(繁中)" name="student_award_award<?php echo $i?>_lang2" id="student_award_award<?php echo $i?>_lang2" value="<?php echo ${"student_award_award".$i."_lang2"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Name(簡中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Award Name(簡中)" name="student_award_award<?php echo $i?>_lang3" id="student_award_award<?php echo $i?>_lang3" value="<?php echo ${"student_award_award".$i."_lang3"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Title(Eng)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Title(Eng)" name="student_award_title<?php echo $i?>_lang1" id="student_award_title<?php echo $i?>_lang1" value="<?php echo ${"student_award_title".$i."_lang1"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Title(繁中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Title(繁中)" name="student_award_title<?php echo $i?>_lang2" id="student_award_title<?php echo $i?>_lang2" value="<?php echo ${"student_award_title".$i."_lang2"} ?>"/>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Title(簡中)</label>
	                            <div class="col-xs-8">
	                                <input type="text" class="form-control" placeholder="Title(簡中)" name="student_award_title<?php echo $i?>_lang3" id="student_award_title<?php echo $i?>_lang3" value="<?php echo ${"student_award_title".$i."_lang3"} ?>"/>
	                            </div>
	                        </div>


	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Detail(Eng)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Award Detail(Eng)" name="student_award_award_detail<?php echo $i?>_lang1" id="student_award_award_detail<?php echo $i?>_lang1" ><?php echo ${"student_award_award_detail".$i."_lang1"}?></textarea>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Detail(繁中)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Award Detail(繁中)" name="student_award_award_detail<?php echo $i?>_lang2" id="student_award_award_detail<?php echo $i?>_lang2" ><?php echo ${"student_award_award_detail".$i."_lang2"}?></textarea>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-xs-2 control-label" style="padding-right:0px">Award Detail(簡中)</label>
	                            <div class="col-xs-8">
	                            	<textarea rows="5" class="form-control" placeholder="Award Detail(簡中)" name="student_award_award_detail<?php echo $i?>_lang3" id="student_award_award_detail<?php echo $i?>_lang3" ><?php echo ${"student_award_award_detail".$i."_lang3"}?></textarea>
	                            </div>
	                        </div>

					</div>

					<?php } ?>
                    

                    
                    	<input type="hidden" name="type" id="type" value="student_award" />
                        <input type="hidden" name="processType"  id="processType" value="">
                        <input type="hidden" name="student_award_id" id="student_award_id" value="<?php echo $student_award_id ?>" />
                        <input type="hidden" name="student_award_lv1" id="student_award_lv1" value="<?php echo $student_award_lv1 ?>" />
                        <input type="hidden" name="student_award_lv2" id="student_award_lv2" value="<?php echo $student_award_lv2 ?>" />
                        <input type="hidden" name="student_award_status" id="student_award_status" value="1" />
                        <input type="hidden" name="student_award_type" id="student_award_status" value="<?php echo $student_award_type?>" />
                        <input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
                        <input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
                    </form>
                    
                    
				</div>
			</div>

			<div class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<!--<button type="button" class="btn btn-primary" id="btn_tchi_to_schi" onclick="convert_tchi_to_schi(); return false;">
						<i class="fa fa-language fa-lg"></i> 繁 &gt; 簡
					</button>

					<button type="button" class="btn btn-primary" id="btn_tchi_to_schi" onclick="convert_tchi_to_schi(); return false;">
						<i class="fa fa-language fa-lg"></i> 簡 &gt; 繁 
					</button>-->
					
					<button type="button" class="btn btn-primary" onclick="previewPost(); return false;">
						<i class="fa fa-eye fa-lg"></i> Preview
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>

		</div>

		<?php $act = "//".$_SERVER['HTTP_HOST']."/en/"; ?>
		<script>function previewPost() {document.getElementById('preview-form').submit();}</script>
		<form id="preview-form" target="_blank" method="get" action="<?php echo $act;?>programmes/award.php" style="display: none;"><input type="text" name="id" value="<?php echo $student_award_id; ?>"><input type="text" name="preview" value="true"></form>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>