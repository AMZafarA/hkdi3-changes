<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="student_award" )

	{

		$id = $_REQUEST['student_award_id'] ?? '';
		$student_award_lv1 = $_REQUEST['student_award_lv1'] ?? '';
		$student_award_lv2 = $_REQUEST['student_award_lv2'] ?? '';

		$type_name = DM::load("department", $student_award_lv1);

		$type_namelv1 = $type_name['department_name_lang1'];

		$type_name = DM::load("department", $student_award_lv2);

		$type_namelv2 = $type_name['department_name_lang1'];

		$type_name = "Student Award";

		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if($type_namelv2){ $type_name .= " - ".$type_namelv2;}

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "student_award_" . $student_award_lv1."_".$student_award_lv2)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;

			for ($i=1; $i < 5; $i++) {
				if(isset($values['removeAimg'.$i])){
					$values['student_award_award_image'.$i] = "";
				}

				if(isset($values['removeSimg'.$i])){
					$values['student_award_student_image'.$i] = "";
				}
			}


			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();

			if($isNew){

				$values['student_award_publish_status'] = "D";

				$values['student_award_status'] = STATUS_ENABLE;

				$values['student_award_created_by'] = $_SESSION['sess_id'];

				$values['student_award_created_date'] = $timestamp;

				$values['student_award_updated_by'] = $_SESSION['sess_id'];

				$values['student_award_updated_date'] = $timestamp;

				$id = DM::insert('student_award', $values);

				if(!file_exists($student_award_path . $id. "/")){
					mkdir($student_award_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('student_award', $id);



				if($ObjRec['student_award_status'] == STATUS_DISABLE)

				{

					header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=F-1002");

					return;

				}


				$values['student_award_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['student_award_updated_by'] = $_SESSION['sess_id'];

				$values['student_award_updated_date'] = $timestamp;



				DM::update('student_award', $values);

			}


			$ObjRec = DM::load('student_award', $id);

			$student_award_img_arr = array('student_award_student_image1','student_award_student_image2','student_award_student_image3','student_award_student_image4','student_award_award_image1','student_award_award_image2','student_award_award_image3','student_award_award_image4');

			foreach($student_award_img_arr as $key=>$val){

				if( isset( $_FILES[$val] ) ) {
					if($_FILES[$val]['tmp_name'] != ""){

						if($ObjRec[$val] != "" && file_exists($student_award_path . $id . "/" . $ObjRec[$val])){
						//	@unlink($student_award_path  . $id . "/" . $ObjRec[$val]);
						}

						$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

						$newname = moveFile($student_award_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

						list($width, $height) = getimagesize($student_award_path . $id . "/".$newname);

							$newWidth ="620";
							$newHeight="620";

						resizeImageByHeight($student_award_path. $id."/". $newname, $newHeight , $student_award_path. $id."/". $newname);

						resizeImageByWidth($student_award_path. $id."/". $newname, $newWidth , $student_award_path. $id."/". $newname);

						resizeAndBg($student_award_path. $id."/". $newname,$student_award_path. $id."/". $newname,$newWidth,$newHeight);
						//cropImage($student_award_path. $id."/". $newname, $newWidth,$newHeight , $student_award_path. $id."/". $newname);


						$values = array();
						$values[$val] = $newname;
						$values['student_award_id'] = $id;

						DM::update('student_award', $values);
						unset($values);

						$ObjRec = DM::load('student_award', $id);
					}
				}
			}



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "student_award '" . $ObjRec['student_award_title1_lang1'] . "' (ID:" . $ObjRec['student_award_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "student_award_" . $student_award_lv1."_".$student_award_lv2, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "student_award_" . $student_award_lv1."_".$student_award_lv2,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['student_award_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "student_award_" . $student_award_lv1."_".$student_award_lv2, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."student_award/student_award.php?student_award_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "student_award_" . $student_award_lv1."_".$student_award_lv2, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "student_award_" . $student_award_lv1."_".$student_award_lv2, "PA", $approval_id);

			}

			header("location:student_award.php?student_award_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('student_award', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['student_award_status'] == STATUS_DISABLE)

			{

				header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("student_award", $id);

			$values['student_award_id'] = $id;
			$values['student_award_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['student_award_status'] = STATUS_DISABLE;
			$values['student_award_updated_by'] = $_SESSION['sess_id'];
			$values['student_award_updated_date'] = getCurrentTimestamp();


			DM::update('student_award', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("student_award_" . $student_award_lv1."_".$student_award_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "student_award '" . $ObjRec['student_award_name_lang1'] . "' (ID:" . $ObjRec['student_award_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "student_award_" . $student_award_lv1."_".$student_award_lv2, "DS");

				$files = array($SYSTEM_HOST."student_award/student_award.php?student_award_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("student_award_" . $student_award_lv1."_".$student_award_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);

				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "student_award_" . $student_award_lv1."_".$student_award_lv2, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "student_award_" . $student_award_lv1."_".$student_award_lv2, "D");
			}





			header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('student_award', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['student_award_status'] == STATUS_ENABLE || $ObjRec['student_award_publish_status'] == 'AL')

			{

				header("location: list.php?student_award_lv1=".$student_award_lv1."&student_award_lv2=".$student_award_lv2."&msg=F-1006");

				return;

			}



			$values['student_award_id'] = $id;

			$values['student_award_publish_status'] = 'D';

			$values['student_award_status'] = STATUS_ENABLE;

			$values['student_award_updated_by'] = $_SESSION['sess_id'];

			$values['student_award_updated_date'] = getCurrentTimestamp();



			DM::update('student_award', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'student_award', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of student_award '" . $ObjRec['student_award_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "student_award_" . $student_award_lv1."_".$student_award_lv2, "WA");


			}



			Logger::log($_SESSION['sess_id'], $timestamp, "student_award '" . $ObjRec['student_award_name_lang1'] . "' (ID:" . $ObjRec['student_award_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "student_award_" . $student_award_lv1."_".$student_award_lv2, "UD");


			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
