<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="peec_details" )

	{

		$id = $_REQUEST['peec_details_id'] ?? '';
		$pid = $_REQUEST['peec_id'] ?? '';
		$peec_details_lv1 = $_REQUEST['peec_details_lv1'] ?? '';
		$peec_details_lv2 = $_REQUEST['peec_details_lv2'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "continuing_edcation")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();
			$values['peec_details_is_outlink'] = $values['peec_details_is_outlink'];

			if($isNew){

				$values['peec_details_publish_status'] = "D";

				$values['peec_details_status'] = STATUS_ENABLE;

				$values['peec_details_created_by'] = $_SESSION['sess_id'];

				$values['peec_details_created_date'] = $timestamp;

				$values['peec_details_updated_by'] = $_SESSION['sess_id'];

				$values['peec_details_updated_date'] = $timestamp;

				$id = DM::insert('peec_details', $values);

				if(!file_exists($peec_details_path . $id. "/")){
					mkdir($peec_details_path . $id . "/", 0775, true);
				}

			} else {

				$ObjRec = DM::load('peec_details', $id);

				$pid = $ObjRec['peec_details_pid'];

				if($ObjRec['peec_details_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}


				$values['peec_details_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['peec_details_updated_by'] = $_SESSION['sess_id'];

				$values['peec_details_updated_date'] = $timestamp;



				DM::update('peec_details', $values);

			}


			$ObjRec = DM::load('peec_details', $id);

			$peec_img_arr = array('peec_details_image', 'peec_details_thumb');

			foreach($peec_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($peec_details_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($peec_details_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($peec_details_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


					list($width, $height) = getimagesize($peec_details_path . $id . "/".$newname);
/*
					$newWidth = '1500';
					$newHeight = '500';

					if($width > $newWidth){
						resizeImageByHeight($peec_details_path. $id."/". $newname, $newWidth , $peec_details_path. $id."/". $newname);
					}

					if($height > $newHeight){
						resizeImageByWidth($peec_details_path. $id."/". $newname, $newHeight , $peec_details_path. $id."/". $newname);
					}

					resizeAndBg($peec_details_path. $id."/". $newname,$peec_details_path. $id."/". $newname,$newWidth,$newHeight);
*/




					$values = array();
					$values[$val] = $newname;
					$values['peec_details_id'] = $id;

					DM::update('peec_details', $values);
					unset($values);

					$ObjRec = DM::load('peec_details', $id);
				}
			}


			if($processType == ""){

				//Logger::log($_SESSION['sess_id'], $timestamp, "peec_details '" . $ObjRec['peec_details_name_lang1'] . "' (ID:" . $ObjRec['peec_details_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification('peec_details', $id, $timestamp, $_SESSION['sess_id'], array("A","E"), 'continuing_edcation', $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "continuing_edcation",$type, $id, $timestamp);

				//Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['peec_details_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification('peec_details', $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."peec_details/peec_details.php?peec_details_id=".$id."&peec_id=".$pid);

				$approval_id = ApprovalService::createApprovalRequest( "continuing_edcation", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("peec_details", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);

			}

			header("location:peec_details.php?peec_details_id=" . $id . "&peec_id=" . $pid . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('peec_details', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['peec_details_status'] == STATUS_DISABLE)

			{

				header("location: list.php?msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("peec_details", $id);

			$values['peec_details_id'] = $id;
			$values['peec_details_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['peec_details_status'] = STATUS_DISABLE;
			$values['peec_details_updated_by'] = $_SESSION['sess_id'];
			$values['peec_details_updated_date'] = getCurrentTimestamp();


			DM::update('peec_details', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("continuing_edcation", $type, $id, $timestamp);

				//Logger::log($_SESSION['sess_id'], $timestamp, "peec_details '" . $ObjRec['peec_details_name_lang1'] . "' (ID:" . $ObjRec['peec_details_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("peec_details", $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", "DS");

				$files = array($SYSTEM_HOST."peec_details/peec_details.php?peec_details_id=".$id."&peec_id=".$pid);

				$approval_id = ApprovalService::createApprovalRequest("continuing_edcation", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("peec_details", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);
			}else{
				NotificationService::sendNotification("peec_details", $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "continuing_edcation", "D");
			}





			header("location: list.php?msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('peec_details', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['peec_details_status'] == STATUS_ENABLE || $ObjRec['peec_details_publish_status'] == 'AL')

			{

				header("location: list.php?msg=F-1006");

				return;

			}



			$values['peec_details_id'] = $id;

			$values['peec_details_publish_status'] = 'D';

			$values['peec_details_status'] = STATUS_ENABLE;

			$values['peec_details_updated_by'] = $_SESSION['sess_id'];

			$values['peec_details_updated_date'] = getCurrentTimestamp();



			DM::update('peec_details', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'peec_details', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of signed '" . $ObjRec['peec_details_company_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'news_' . $news_lv1."_".$news_lv2, "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "signed '" . $ObjRec['peec_details_company_lang1'] . "' (ID:" . $ObjRec['peec_details_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'news_' . $news_lv1."_".$news_lv2, "UD");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
