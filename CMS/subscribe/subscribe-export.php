<?php
	include "../include/config.php";
	include $SYSTEM_BASE . "include/function.php";
	include $SYSTEM_BASE . "session.php";
	include $SYSTEM_BASE . "include/system_message.php";
	include $SYSTEM_BASE . "include/classes/DataMapper.php";
	include $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$page_no = $_REQUEST['page_no'] ?? '';
	$page_size = 10;
	if($page_no == "" || !isInteger($page_no)){
		$page_no = 1;
	}
	$sql = " ( subscription_status = ? OR subscription_status = ? ) ";
	$parameters = array(STATUS_ENABLE, STATUS_DISABLE);

	$total_record = DM::count("subscription", $sql, $parameters);
	$total_page = ceil($total_record / $page_size);
	if($total_page == 0)
		$total_page = 1;
	if($page_no > $total_page){
		$page_no = $total_page;
	}
	$result = DM::findAll('subscription','', 'subscription_id ASC', array());

	$url = $host_name;
	$rel_path = getcwd() . '/list/';
	$filename = time() . '_subscribers_list.csv';
	$filepath = $rel_path . $filename;

	$fp = fopen($filepath, 'w');
	$headers = array("ID","Name","Email","Subscribed On","Status");
	fputcsv($fp, $headers);
	foreach ($result as $subscription) {
		// $fd = array();
		// $fd[] = (int) $subscription["subscription_id"];
		// $fd[] = htmlspecialchars($subscription["subscription_name"]);
		// $fd[] = htmlspecialchars($subscription["subscription_email"]);
		// $fd[] = date("d/m/Y", strtotime($subscription["subscription_created_date"]));
		// $fd[] = $subscription["subscription_status"] == STATUS_ENABLE ? "Active" : "Unsubscribed";
		// fputcsv($fp, $fd);

		$subscription_id = (int) $subscription["subscription_id"];
		$subscription_name = htmlspecialchars($subscription["subscription_name"]);
		$subscription_email = htmlspecialchars($subscription["subscription_email"]);
		$subscription_created_date = date("d/m/Y", strtotime($subscription["subscription_created_date"]));
		$subscription_status = $subscription["subscription_status"] == STATUS_ENABLE ? "Active" : "Unsubscribed";
		$fdd = array($subscription_id , $subscription_name , $subscription_email , $subscription_created_date , $subscription_status);
		fputcsv($fp, $fdd);

	}
	fclose($fp);

	$ret["path"] = $url . "subscribe/list/" . $filename;

	echo json_encode($ret);

?>