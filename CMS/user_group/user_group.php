<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
	header("location: ../dashboard.php?msg=F-1000");
	return;
}

$user_group_id = $_REQUEST['user_group_id'] ?? '';
$msg = $_REQUEST['msg'] ?? '';	

if($user_group_id != ""){
	if(!isInteger($user_group_id))
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	$obj_row = DM::load('user_group', $user_group_id);

	if($obj_row == "")
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	foreach($obj_row as $rKey => $rValue){
		$$rKey = htmlspecialchars($rValue);
	}

	$user_list = DM::findAll(array('group_user', 'user'), " group_user_uid = user_id AND user_status = ? AND group_user_gid = ? ", ' group_user_sequence, user_display_name, group_user_uid ', array(STATUS_ENABLE, $user_group_id)); 

	$last_updated_user = DM::load('user', $user_group_updated_by);

	$editing_rights = DM::findAll("user_group_permission", " ugp_user_group_id = ? AND ugp_role = ? ", ' ugp_permission_key ', array($user_group_id, 'E'));
	$editing_rights = array_column($editing_rights, 'ugp_permission_key');

	$approving_rights = DM::findAll("user_group_permission", " ugp_user_group_id = ? AND ugp_role = ? ", ' ugp_permission_key ', array($user_group_id, 'A'));
	$approving_rights = array_column($approving_rights, 'ugp_permission_key');

	$create_rights = DM::findAll("user_group_permission", " ugp_user_group_id = ? AND ugp_role = ? ", ' ugp_permission_key ', array($user_group_id, 'C'));
	$create_rights = array_column($create_rights, 'ugp_permission_key');
} else {
	foreach($_REQUEST as $rKey => $rValue){
		$$rKey = htmlspecialchars($rValue);
	}
	$user_list = array();
	$editing_rights = array();
	$approving_rights = array();
	$create_rights = array();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once "../header.php" ?>
	<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
	<script type="text/x-tmpl" id="user_panel">
		<span class="user_cell">
			<div>
				<label>
					{%=o.user_display_name %}
					<input type="hidden" name="{%=o.parameter_name%}" value="{%=o.user_id%}"/>
				</label>
				<span>
					<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
				</span>
			</div>
		</span>
	</script>
	<script>
		function checkForm(){				
			var flag=true;
			var errMessage="";

			trimForm("form1");

			if($("#user_group_name").val() == ""){
				flag = false;
				errMessage += "Group Name\n";
			}

			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			document.form1.submit();
		}

		function resetForm(){				
			document.form1.reset();
		}

		function removeItem(item){
			$(item).closest(".user_cell").detach();
		}

		$( document ).ready(function() {
			$("#tabs").tabs({});
			$( "#user_search" ).autocomplete({
				source: function(request, callback){
					var values = [];
					$("input[name='group_user_uid[]']").each(function() {
						values.push($(this).val());
					}).promise().done(function(){
						$.get( "user_search.php", { term:request.term, "user_id": values.join(",")}, 
							function( data ) {callback(data);}, 
							"json");
					});
				}, 
				minLength: 2,
				select: function( event, ui ) {
					if(ui.item){
						var content = tmpl("user_panel", {user_id:ui.item.id,user_display_name:ui.item.value,parameter_name:"group_user_uid[]"});
						$("#user_value").append(content);
						event.preventDefault();
					}
					$("#user_search").val("");
				}
			});
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>
		});
	</script>
	<style>
		.user_cell {
			margin-bottom:7px; display:inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
		}
		.user_cell div {
			margin-top: 8px;
			margin-left: 8px; 
			margin-right: 8px; 
			margin-bottom: 6px;
		}
	</style>
</head>
<body>
	<?php include_once "../menu.php" ?>

	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li><a href="list.php">User Groups</a></li>
				<li class="active"><?php echo $user_group_id == "" ? "Create" : "Update" ?></li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				&nbsp;
			</span>
		</div>
		<div class="row">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-danger" onclick="resetForm(); return false;">
					<i class="fa fa-undo fa-lg"></i> Reset
				</button>
			</div>
		</div>
		<div class="row record_status_row bg-record-status">
			<?php if($user_group_id != "") { ?>
				<div class="col-xs-2">
					Last Updated By
				</div>
				<div class="col-xs-4">
					<?php echo htmlspecialchars($last_updated_user['user_display_name']) ?> @ <?php echo $user_group_updated_date ?>
				</div>
			<?php } ?>
			<div class="col-xs-4" style="display:block">
				&nbsp;
			</div>
		</div>
		<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-xs-2 control-label" style="padding-right:0px">Group Name * :</label>
				<div class="col-xs-10">
					<input type="text" class="form-control" placeholder="User Group Name" name="user_group_name" id="user_group_name" value="<?php echo $user_group_name ?? '' ?>"/>
					<input type="hidden" name="type" value="user_group" />
					<input type="hidden" name="processType" id="processType" value="">
					<input type="hidden" name="user_group_id" value="<?php echo $user_group_id ?>" />
					<input type="hidden" name="user_group_status" value="1" />
				</div>
			</div>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
						<li><a href="#tabs-1">Users</a></li>
						<?php if($user_group_id != 1) { ?>
							<li><a href="#tabs-2">Editing Sections</a></li>
							<li><a href="#tabs-3">Approving Sections</a></li>
						<?php } ?>
					</ul>
					<div id="tabs-1">
						<div class="form-group">
							<label class="col-xs-2 control-label">Search User :</label>
							<div class="col-xs-10">
								<input type="text" class="form-control" placeholder="User Name or User Login or Email Address" name="user_search" id="user_search"/>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-xs-12 control-label">Group User</label>
						</div>
						<div id="user_value" style="padding-bottom: 15px;">
							<?php foreach($user_list as $user){ ?>
								<span class="user_cell">
									<div>
										<label>
											<?php echo htmlspecialchars($user['user_display_name']) ?>
											<input type="hidden" name="group_user_uid[]" value="<?php echo $user['group_user_uid'] ?>"/>
										</label>
										<span>
											<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
												<i class="glyphicon glyphicon-remove"></i>
											</button>
										</span>
									</div>
								</span>
							<?php } ?>
						</div>
					</div>
					<?php if($user_group_id != 1) { ?>
						<div id="tabs-2">
							<div class="row">
								<?php
								$results = DM::findAll("permission", " permission_type = ? ", " permission_seq ", array("S"));

								foreach($results as $row) {
									?>
									<div class="col-xs-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="editing_permission[]" value="<?php echo $row['permission_key'] ?>" <?php echo in_array($row['permission_key'], $editing_rights) ? "checked" : "" ?>/>
												<strong><?php echo htmlspecialchars($row['permission_name']) ?></strong>
											</label>
										</div>
									</div>
									<?php
								}

								unset($results);

								$permissions = DM::findAll("permission", " permission_type = ? ", " permission_name ", array("R"));

								foreach($permissions as $permission) {
									$results = DM::findAll($permission['permission_key'], $permission['permission_key'] . "_status = ? AND " . $permission['permission_parameter_name'] . ' = ? ', $permission['permission_field_name'], array(STATUS_ENABLE, $permission['permission_parameter_value']));

									foreach($results as $row) {
										$permission_key = $permission['permission_key'] . "_" . $row[$permission['permission_key'] . "_id"];
										$permission_name = $row[$permission['permission_field_name']];
										?>
										<div class="col-xs-6">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="editing_permission[]" value="<?php echo $permission_key ?>" <?php echo in_array($permission_key, $editing_rights) ? "checked" : "" ?>/>
													<strong><?php echo htmlspecialchars($permission['permission_name']) . " - " . htmlspecialchars($permission_name) ?></strong>
												</label>
											</div>
										</div>
										<?php
									}
								}
								?>
							</div>
						</div>
						<div id="tabs-3">
							<div class="row">
								<?php
								$results = DM::findAll("permission", " permission_type = ? ", " permission_seq ", array("S"));

								foreach($results as $row) {
									?>
									<div class="col-xs-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="approving_permission[]" value="<?php echo $row['permission_key'] ?>" <?php echo in_array($row['permission_key'], $approving_rights) ? "checked" : "" ?>/>
												<strong><?php echo htmlspecialchars($row['permission_name']) ?></strong>
											</label>
										</div>
									</div>
									<?php
								}

								unset($results);

								$permissions = DM::findAll("permission", " permission_type = ? ", " permission_name ", array("R"));

								foreach($permissions as $permission) {
									$results = DM::findAll($permission['permission_key'], $permission['permission_key'] . "_status = ? AND " . $permission['permission_parameter_name'] . ' = ? ', $permission['permission_field_name'], array(STATUS_ENABLE, $permission['permission_parameter_value']));

									foreach($results as $row) {
										$permission_key = $permission['permission_key'] . "_" . $row[$permission['permission_key'] . "_id"];
										$permission_name = $row[$permission['permission_field_name']];
										?>
										<div class="col-xs-6">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="approving_permission[]" value="<?php echo $permission_key ?>" <?php echo in_array($permission_key, $approving_rights) ? "checked" : "" ?>/>
													<strong><?php echo htmlspecialchars($permission['permission_name']) . " - " . htmlspecialchars($permission_name) ?></strong>
												</label>
											</div>
										</div>
										<?php
									}
								}
								?>
							</div>
						</div>

					<?php } ?>
				</div>
			</div>
		</form>
		<div class="form-group">
			<div class="col-xs-12" style="text-align:right">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-danger" onclick="resetForm(); return false;">
					<i class="fa fa-undo fa-lg"></i> Reset
				</button>
			</div>
		</div>
	</div>
	<!-- End page content -->

	<?php include_once "../footer.php" ?>
</body>
</html>