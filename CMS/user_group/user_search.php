<?php
	include_once "../include/config.php";
	
	if(session_id() == "")
		session_start();
	
	if($_SESSION['sess_login'] == ""){
		echo json_encode(array());
		return;
	}

	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$term = $_REQUEST['term'] ?? '';
	$user_id = $_REQUEST['user_id'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$sql = " user_status = ? AND (user_display_name like ? OR user_login like ? OR user_email like ?)";
	$parameters = array(STATUS_ENABLE, "%" . $term . "%", "%" . $term . "%", "%" . $term . "%");

	$user_list = explode(",", $user_id);
	
	if(is_array($user_list) && count($user_list) > 0){
		$sql .= " AND user_id not in (" . implode(',', array_fill(0, count($user_list), '?'))  . ") ";
		$parameters = array_merge($parameters, $user_list);
	}
	
	$user_list = DM::findAllWithLimit("user", 10, $sql, " user_display_name ", $parameters);
	
	$result = array();
	
	foreach($user_list as $user){
		$item = array();
		$item['id'] = $user['user_id'];
		$item['value'] = $user['user_display_name'];
		$result[] = $item;
	}

	unset($user_list);

	echo json_encode($result);
	unset($result);
?>