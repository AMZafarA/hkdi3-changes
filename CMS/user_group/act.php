<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/cloning/ContentCloningUtility.php";

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="user_group" )
	{
		$id = $_REQUEST['user_group_id'] ?? '';
		
		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
		
		if($processType == "") {
			$values = $_REQUEST;
			
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();
			
			if($isNew)
			{
				$values['user_group_status'] = STATUS_ENABLE;
				$values['user_group_created_by'] = $_SESSION['sess_id'];
				$values['user_group_created_date'] = $timestamp;
				$values['user_group_updated_by'] = $_SESSION['sess_id'];
				$values['user_group_updated_date'] = $timestamp;

				$id = DM::insert('user_group', $values);
			} else {				
				$ObjRec = DM::load('user_group', $id);
				
				if($ObjRec['user_group_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}

				$values['user_group_updated_by'] = $_SESSION['sess_id'];
				$values['user_group_updated_date'] = $timestamp;

				DM::update('user_group', $values);
			}
			
			$ObjRec = DM::load('user_group', $id);
			
			DM::query(" DELETE FROM group_user WHERE group_user_gid = ? ", array($id));
			
			$group_user = $_REQUEST['group_user_uid'] ?? '';
			
			if(is_array($group_user))
				foreach($group_user as $idx => $uid){
					$values = array();
					$values['group_user_uid'] = $uid;
					$values['group_user_gid'] = $id;
					$values['group_user_sequence'] = $idx + 1;

					DM::insert('group_user', $values);
					unset($values);
				}
				
			DM::query(" DELETE FROM user_group_permission WHERE ugp_user_group_id = ? ", array($id));
				
			$editing_permission = $_REQUEST['editing_permission'] ?? '';
			
			if(is_array($editing_permission))
				foreach($editing_permission as $permission_key){

					
					$values = array();
					$values['ugp_permission_key'] = $permission_key;
					$values['ugp_user_group_id'] = $id;
					$values['ugp_role'] = "E";
					
					DM::insert('user_group_permission', $values);
					
					if(strpos($permission_key, 'product')=== false){
					}else{
						$scheme_string = str_replace('product','scheme',$permission_key);
						$values['ugp_permission_key'] = $scheme_string;
						DM::insert('user_group_permission', $values);
					}

					unset($values);



					$values = array();
					$values['ugp_permission_key'] = $permission_key;
					$values['ugp_user_group_id'] = $id;
					$values['ugp_role'] = "C";
					
					DM::insert('user_group_permission', $values);
					
					if(strpos($permission_key, 'product')=== false){
					}else{
						$scheme_string = str_replace('product','scheme',$permission_key);
						$values['ugp_permission_key'] = $scheme_string;
						DM::insert('user_group_permission', $values);
					}

					unset($values);
				}
			
			$approving_permission = $_REQUEST['approving_permission'] ?? '';
			
			if(is_array($approving_permission))
				foreach($approving_permission as $permission_key){
					$values = array();
					$values['ugp_permission_key'] = $permission_key;
					$values['ugp_user_group_id'] = $id;
					$values['ugp_role'] = "A";

					DM::insert('user_group_permission', $values);
					
					if(strpos($permission_key, 'product')=== false){
					}else{
						$scheme_string = str_replace('product','scheme',$permission_key);
						$values['ugp_permission_key'] = $scheme_string;
						DM::insert('user_group_permission', $values);
					}
					
					unset($values);
				}

			$create_permission = $_REQUEST['create_permission'] ?? '';

			if(is_array($create_permission))
				foreach($create_permission as $permission_key){
					$values = array();
					$values['ugp_permission_key'] = $permission_key;
					$values['ugp_user_group_id'] = $id;
					$values['ugp_role'] = "C";

					DM::insert('user_group_permission', $values);
					
					if(strpos($permission_key, 'product')=== false){
					}else{
						$scheme_string = str_replace('product','scheme',$permission_key);
						$values['ugp_permission_key'] = $scheme_string;
						DM::insert('user_group_permission', $values);
					}
					
					unset($values);
				}
				
			if($id == 1){
				//PermissionUtility::rebuildPermission();
			}
			
			Logger::log($_SESSION['sess_id'], $timestamp, "User Group '" . $ObjRec['user_group_name'] . "' (ID:" . $ObjRec['user_group_id'] .") is " . ($isNew ? "created":"updated") . " by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			header("location:user_group.php?user_group_id=" . $id . "&msg=S-1001");
			return;
		} else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('user_group', $id);
			
			if($ObjRec == NULL || $ObjRec == "" || $ObjRec['user_group_id'] == 1)
			{
				header("location: list.php?msg=F-1002");
				return;
			}
			
			if($ObjRec['user_group_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}
			
			$values['user_group_id'] = $id;
			$values['user_group_status'] = STATUS_DISABLE;			
			$values['user_group_updated_by'] = $_SESSION['sess_id'];
			$values['user_group_updated_date'] = $timestamp;
			
			DM::update('user_group', $values);
			Logger::log($_SESSION['sess_id'], $timestamp, "User Group '" . $ObjRec['user_group_name'] . "' (ID:" . $ObjRec['user_group_id'] .") is deleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			header("location: list.php?msg=S-1002");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>