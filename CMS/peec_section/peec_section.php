<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
$section_header = 'peec_section';

if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], $section_header)){
	header("location: ../dashboard.php?msg=F-1000");
	return;
}

$msg = $_REQUEST['msg'] ?? '';
$peec_section_id = $_REQUEST['peec_section_id'] ?? '';
$randomString = $_REQUEST['randomString'] ?? '';

if($peec_section_id != ""){
	if(!isInteger($peec_section_id))
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	$obj_row = DM::load('peec_section', $peec_section_id);

	if($obj_row == "")
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	if(!file_exists($peec_section_path . $peec_section_id)){
		mkdir($peec_section_path . $peec_section_id, 0775, true);
	}

	$_SESSION['RF']['subfolder'] = "peec_section/" . $peec_section_id . "/";

	
	foreach($obj_row as $rKey => $rValue){
		$$rKey = htmlspecialchars($rValue);
	}

} else {

	$peec_section_publish_status = 'D';
	foreach( DM::stub('peec_section') as $rKey => $rValue ) {
		if ( ! isset( $$rKey ) )
			$$rKey = htmlspecialchars($rValue);
	}

	$length = 10;

	$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

	if(!file_exists($peec_section_path . $randomString)){
		mkdir($peec_section_path . $randomString, 0775, true);
	}

	$_SESSION['RF']['subfolder'] = "peec_section/" . $randomString . "/";
}

$peec_section_section = DM::findAll('peec_section_section', ' peec_section_section_status = ? And peec_section_section_pid = ? ', " peec_section_section_sequence ", array(STATUS_ENABLE,$peec_section_id));
$this_peec_section_section = $peec_section_section ? end( $peec_section_section ) : DM::stub( 'peec_section_section' );
foreach ( $this_peec_section_section as $rKey => $rValue ) {
	if ( ! isset( $$rKey ) )
		$$rKey = htmlspecialchars($rValue);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once "../header.php" ?>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/bootstrap-colorpicker.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
	<script type="text/x-tmpl" id="text_block_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-2 control-label">
						Text Section
					</label>
					<label class="col-xs-2 control-label">
						Background Color
					</label>
					<span class="col-xs-2">
						<div class="input-group colorpicker-component peec_section_section_background_color">
							<input type="text" value="<?php echo $peec_section_section_background_color?$peec_section_section_background_color:'#ffffff'?>" class="form-control" name="peec_section_section_background_color"/>
							<span class="input-group-addon"><i></i></span>
						</div>
					</span>
					<span class="col-xs-6" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="T"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo $peec_section_section_name_lang1?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang1" name="peec_section_section_title_lang1" placeholder="Title (English)" value="<?php echo $peec_section_section_title_lang1?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="peec_section_section_content_lang1"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $peec_section_section_name_lang2?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang2" name="peec_section_section_title_lang2" placeholder="Title (繁中)" value="<?php echo $peec_section_section_title_lang2?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="peec_section_section_content_lang2"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $peec_section_section_name_lang3?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang3" name="peec_section_section_title_lang3" placeholder="Title (簡中)" value="<?php echo $peec_section_section_title_lang3?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="peec_section_section_content_lang3"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</script>

	<script type="text/x-tmpl" id="visit_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-6 control-label">
						Visit Us Section
					</label>
					<span class="col-xs-6" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="Visit"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">Location</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-4">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<label class="col-xs-3 control-label" style="padding-right:0px">Location</label>
							<div class="col-xs-5">
								<input type="text" class="form-control" placeholder="Location(for search Latitude and longitude only)" name="address" id="address" value=""/>
							</div>
							<div class="col-xs-3" style="margin-bottom: 10px;">
								<input type="button" class="btn btn-primary" value="Search" onclick="codeAddress()">
							</div>
							<div id="map-canvas{%=o.tmp_id%}" style="width:1000px;height:400px"></div>

						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" style="padding-right:0px">Latitude and longitude</label>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Latitude" name="peec_section_lat" id="peec_section_lat" value="<?php echo $peec_section_lat ?>"/>
							</div>

							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="longitude" name="peec_section_lng" id="peec_section_lng" value="<?php echo $peec_section_lng ?>"/>
							</div>

						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo $peec_section_section_name_lang1?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang1" name="peec_section_section_title_lang1" placeholder="Title (English)" value="<?php echo $peec_section_section_title_lang1?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="peec_section_section_content_lang1"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $peec_section_section_name_lang2?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang2" name="peec_section_section_title_lang2" placeholder="Title (繁中)" value="<?php echo $peec_section_section_title_lang2?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="peec_section_section_content_lang2"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-4">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $peec_section_section_name_lang3?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-title-lang3" name="peec_section_section_title_lang3" placeholder="Title (簡中)" value="<?php echo $peec_section_section_title_lang3?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="peec_section_section_content_lang3"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</script>

	<script type="text/x-tmpl" id="news_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-6 control-label">
						News Section
					</label>
					<span class="col-xs-6" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="N"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo $peec_section_section_name_lang1?>">
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $peec_section_section_name_lang2?>">
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $peec_section_section_name_lang3?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</script>


	<script type="text/x-tmpl" id="project_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-6 control-label">
						Project Section
					</label>
					<span class="col-xs-6" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="P"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo $peec_section_section_name_lang1?>">
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo $peec_section_section_name_lang2?>">
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-{%=o.tmp_id%}-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo $peec_section_section_name_lang3?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</script>


	<script type="text/x-tmpl" id="photo_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-2 control-label">
						Photo Section
					</label>
					<span class="col-xs-10" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="Photo"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-3">簡中</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-4">Photo</a></li>
					</ul>
					<div id="tabs-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_name_lang1" placeholder="Section name (English)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang1" placeholder="Heading (English)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_name_lang2" placeholder="Section name (繁中)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang2" placeholder="Heading (繁中)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_name_lang2" placeholder="Section name (簡中)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang3" placeholder="Heading (簡中)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-4">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-4">
										<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</script>

	<script type="text/x-tmpl" id="acknowledgement_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-2 control-label">
						Acknowledgement Section
					</label>
					<span class="col-xs-10" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="Acknowledgement"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-3">簡中</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-4">Logo</a></li>
					</ul>
					<div id="tabs-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang1" placeholder="Heading (English)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang2" placeholder="Heading (繁中)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang3" placeholder="Heading (簡中)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-4">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-4">
										<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</script>





	<script type="text/x-tmpl" id="Partner_template">
		<div class="form-group file_row" style="margin-bottom:15px;">
			<div class="col-xs-1">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12 control-label" style="height: 34px;">
						Title
					</div>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="peec_section_section_file_title" placeholder="Title" value="{%=o.filename%}"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
				<input type="hidden" name="peec_section_section_file_id" value=""/>
				<input type="hidden" name="peec_section_section_file_lang" value="{%=o.language%}"/>
				<input type="hidden" name="peec_section_section_file_type" value="Partner"/>
				<input type="hidden" name="peec_section_section_file_url" value=""/>
			</div>

		</div>
	</script>

	<script type="text/x-tmpl" id="contact_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-2 control-label">
						Contact Section
					</label>
					<span class="col-xs-10" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="C"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-{%=o.tmp_id%}-1">Info</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-2">English</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-3">繁中</a></li>
						<li><a href="#tabs-{%=o.tmp_id%}-4">簡中</a></li>
					</ul>
					<div id="tabs-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_email" placeholder="E-mail"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_tel" placeholder="Tel"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_content_lang1" placeholder="Address (English)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_content_lang2" placeholder="Address (繁中)"/>
							</div>
						</div>
					</div>
					<div id="tabs-{%=o.tmp_id%}-4">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_content_lang3" placeholder="Address (簡中)"/>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</script>

	<script type="text/x-tmpl" id="collapsible_text_block_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-3 control-label">
						Collapsible Text Section
					</label>
					<span class="col-xs-9" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="peec_section_section_type" value="CT"/>
						<input type="hidden" name="peec_section_section_id" value=""/>
						<input type="hidden" name="peec_section_section_tid" value=""/>
						<input type="hidden" name="peec_section_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" name="peec_section_section_title_lang1" placeholder="Heading (English)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="peec_section_section_content_lang1"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" name="peec_section_section_title_lang2" placeholder="Heading (繁中)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="peec_section_section_content_lang2"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" name="peec_section_section_title_lang3" placeholder="Heading (簡中)"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="peec_section_section_content_lang3"></textarea>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</script>

	<script>

		var file_list = {};
		var counter = 0;

		function checkForm(){

			
			var flag=true;
			var errMessage="";


			$('#loadingModalDialog').on('shown.bs.modal', function (e) {
				var frm = document.form1;
				frm.processType.value = "";
				cloneTextBlock(0);
			})

			$('#loadingModalDialog').modal('show');
		}

		function checkForm2(){

			var flag=true;

			$('#approvalModalDialog').modal('show');
		}

		function cloneTextBlock(idx){
			if($("input[name='peec_section_section_type'][value='T']").length <= idx){
				cloneCollapsibleBlock(0)
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='T']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneTextBlock(++idx);
		}

		function cloneCollapsibleBlock(idx){
			if($("input[name='peec_section_section_type'][value='CT']").length <= idx){
				cloneNewsBlock(0)
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='CT']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','collapsible_text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneCollapsibleBlock(++idx);
		}

		function cloneNewsBlock(idx){
			if($("input[name='peec_section_section_type'][value='N']").length <= idx){
				cloneProjectBlock(0);
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='N']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','news_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneNewsBlock(++idx);
		}

		function cloneProjectBlock(idx){
			if($("input[name='peec_section_section_type'][value='P']").length <= idx){
				clonePhotoBlock(0);
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='P']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','project_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneProjectBlock(++idx);
		}

		function clonePhotoBlock(idx){
			if($("input[name='peec_section_section_type'][value='Photo']").length <= idx){
				cloneAcknowledgementBlock(0);
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='Photo']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));


			var obj = {"peec_section_section_type":"Photo"};
			obj['peec_section_section_id'] = $(frm).find("input[name='peec_section_section_id']").val();
			obj['peec_section_section_name_lang1'] = $(frm).find("input[name='peec_section_section_name_lang1']").val();
			obj['peec_section_section_name_lang2'] = $(frm).find("input[name='peec_section_section_name_lang2']").val();
			obj['peec_section_section_name_lang3'] = $(frm).find("input[name='peec_section_section_name_lang3']").val();
			obj['peec_section_section_title_lang1'] = $(frm).find("input[name='peec_section_section_title_lang1']").val();
			obj['peec_section_section_title_lang2'] = $(frm).find("input[name='peec_section_section_title_lang2']").val();
			obj['peec_section_section_title_lang3'] = $(frm).find("input[name='peec_section_section_title_lang3']").val();

			obj['peec_section_section_tid'] = $(frm).find("input[name='peec_section_section_tid']").val();
			obj['peec_section_section_sequence'] = $(frm).find("input[name='peec_section_section_sequence']").val();
			obj['lang1_files'] = [];

			cloneFileElement(obj, idx, 0);
		}

		function cloneFileElement(dataObj, parentIdx, elementIdx){
			var frm = $("input[name='peec_section_section_type'][value='Photo']:eq(" + parentIdx + ")").closest("form");

			if($(frm).find("div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','photo_section[]').attr('type','hidden').val(data).appendTo('#form1');
				clonePhotoBlock(++parentIdx);
				return;
			}

			var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");

			var fileObj = {};
			fileObj['peec_section_section_file_title'] = $(row).find("input[name='peec_section_section_file_title']").val();
			fileObj['peec_section_section_file_detail'] = $(row).find("input[name='peec_section_section_file_detail']").val();
			fileObj['peec_section_section_file_link'] = $(row).find("input[name='peec_section_section_file_link']").val();
			fileObj['peec_section_section_file_id'] = $(row).find("input[name='peec_section_section_file_id']").val();
			fileObj['peec_section_section_file_lang'] = $(row).find("input[name='peec_section_section_file_lang']").val();
			fileObj['peec_section_section_file_type'] = $(row).find("input[name='peec_section_section_file_type']").val();
			fileObj['peec_section_section_file_url'] = $(row).find("input[name='peec_section_section_file_url']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";

			var fd = new FormData();
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;

					dataObj['lang1_files'].push(fileObj);

					cloneFileElement(dataObj, parentIdx, ++elementIdx);
				}
			});
		}

		function cloneAcknowledgementBlock(idx){ 
			console.log('aaa');
			if($("input[name='peec_section_section_type'][value='Acknowledgement']").length <= idx){
				cloneContactBlock(0);
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='Acknowledgement']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));


			var obj = {"peec_section_section_type":"Acknowledgement"};
			obj['peec_section_section_id'] = $(frm).find("input[name='peec_section_section_id']").val();
			obj['peec_section_section_name_lang1'] = $(frm).find("input[name='peec_section_section_name_lang1']").val();
			obj['peec_section_section_name_lang2'] = $(frm).find("input[name='peec_section_section_name_lang2']").val();
			obj['peec_section_section_name_lang3'] = $(frm).find("input[name='peec_section_section_name_lang3']").val();
			obj['peec_section_section_title_lang1'] = $(frm).find("input[name='peec_section_section_title_lang1']").val();
			obj['peec_section_section_title_lang2'] = $(frm).find("input[name='peec_section_section_title_lang2']").val();
			obj['peec_section_section_title_lang3'] = $(frm).find("input[name='peec_section_section_title_lang3']").val();

			obj['peec_section_section_tid'] = $(frm).find("input[name='peec_section_section_tid']").val();
			obj['peec_section_section_sequence'] = $(frm).find("input[name='peec_section_section_sequence']").val();
			obj['lang1_files'] = [];

			cloneAcknowledgementFileElement(obj, idx, 0);
		}

		function cloneAcknowledgementFileElement(dataObj, parentIdx, elementIdx){
			var frm = $("input[name='peec_section_section_type'][value='Acknowledgement']:eq(" + parentIdx + ")").closest("form");

			if($(frm).find("div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','acknowledgement_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneAcknowledgementBlock(++parentIdx);
				return;
			}

			var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");

			var fileObj = {};
			fileObj['peec_section_section_file_title'] = $(row).find("input[name='peec_section_section_file_title']").val();
			fileObj['peec_section_section_file_detail'] = $(row).find("input[name='peec_section_section_file_detail']").val();
			fileObj['peec_section_section_file_link'] = $(row).find("input[name='peec_section_section_file_link']").val();
			fileObj['peec_section_section_file_id'] = $(row).find("input[name='peec_section_section_file_id']").val();
			fileObj['peec_section_section_file_lang'] = $(row).find("input[name='peec_section_section_file_lang']").val();
			fileObj['peec_section_section_file_type'] = $(row).find("input[name='peec_section_section_file_type']").val();
			fileObj['peec_section_section_file_url'] = $(row).find("input[name='peec_section_section_file_url']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";

			var fd = new FormData();
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;

					dataObj['lang1_files'].push(fileObj);

					cloneAcknowledgementFileElement(dataObj, parentIdx, ++elementIdx);
				}
			});
		}

		function cloneContactBlock(idx){
			if($("input[name='peec_section_section_type'][value='C']").length <= idx){
				cloneVisitBlock(0);
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='C']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','contact_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneContactBlock(++idx);
		}

		function cloneVisitBlock(idx){
			if($("input[name='peec_section_section_type'][value='Visit']").length <= idx){
				document.form1.submit();
				return;
			}

			var frm = $("input[name='peec_section_section_type'][value='Visit']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','visit_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneVisitBlock(++idx);
		}

		function moveUp(button){
			var $current = $(button).closest(".section_row");
			var $previous = $current.prev('.section_row');
			if($previous.length !== 0){
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce().remove();
				});
				$current.insertBefore($previous);
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce(tinymceConfig);
				});
			}
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function moveDown(button){
			var $current = $(button).closest(".section_row");
			var $next = $current.next('.section_row');
			if($next.length !== 0){
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce().remove();
				});
				$current.insertAfter($next);
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce(tinymceConfig);
				});
			}
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function removeRow(button){
			$(button).closest(".section_row").detach();
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function moveFileUp(button){
			var $current = $(button).closest(".file_row");
			var $previous = $current.prev('.file_row');
			if($previous.length !== 0){
				$current.insertBefore($previous);
			}
		}

		function moveFileDown(button){
			var $current = $(button).closest(".file_row");
			var $next = $current.next('.file_row');
			if($next.length !== 0){
				$current.insertAfter($next);
			}
		}

		function removeFileItem(button){
			$(button).closest(".file_row").detach();
		}

		function addFile(fileBrowser, language){
			if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
				return;

			var files = $(fileBrowser)[0].files;

			for(var i = 0; i < files.length; i++){
				var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
				file_list[obj.tmp_id] = files[i];
				var content = tmpl("Partner_template", obj);
				var header_row = $(fileBrowser).closest("div.form-group");

				if($(header_row).is(":last-child")){
					$(content).insertAfter(header_row);
				} else {
					$(content).insertAfter($(header_row).siblings(":last"));
				}
			}
		}

		function addTextBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("text_block_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
			$('.peec_section_section_background_color').colorpicker();
		}

		function addCollapsibleBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("collapsible_text_block_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function addNewsBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("news_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function addProjectBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("project_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function addPhotoBlock(tab_id){
			var tmp_id = "tmp_" + (counter++)  ;
			var obj = {tmp_id:tmp_id};
			var content = tmpl("photo_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-"+tab_id).append(content);
			$("input[name='peec_section_section_tid']").val(tab_id);
			$("#tabs-" + tmp_id).tabs({});
			$("#tabs-" + tmp_id + " input[name='upload_eng_pdf_file']").filestyle({input:false, buttonText: "&nbsp;Choose file", badge:false,iconName: "glyphicon-file"});
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function addAcknowledgementBlock(tab_id){
			var tmp_id = "tmp_" + (counter++)  ;
			var obj = {tmp_id:tmp_id};
			var content = tmpl("acknowledgement_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-"+tab_id).append(content);
			$("input[name='peec_section_section_tid']").val(tab_id);
			$("#tabs-" + tmp_id).tabs({});
			$("#tabs-" + tmp_id + " input[name='upload_eng_pdf_file']").filestyle({input:false, buttonText: "&nbsp;Choose file", badge:false,iconName: "glyphicon-file"});
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}


		function addVisitBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("visit_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
			initialize(null,null,tmp_id);
		}



		function addContactBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("contact_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='peec_section_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});


		}

		function submitForm(){
			document.form1.submit();
		}

		function removeIcon(button){
			$("#remove_icon").val("Y");
			$(button).closest("div").detach();
		}

		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form1;
					frm.processType.value = "submit_for_approval";
					cloneTextBlock(0);
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		$( document ).ready(function() {
			$("#tabs").tabs({});
			$('#peec_section_background_color,#peec_section_text_color,.peec_section_section_background_color').colorpicker();

			$("#upload_icon").filestyle({buttonText: ""});
			$("#peec_section_banner,#peec_section_thumb_banner").filestyle({buttonText: ""});

			$( "#peec_section_from_date,#peec_section_to_date" ).datepicker({"dateFormat" : "yy-mm-dd"});

			$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
				$(element).find("div.col-xs-12[id^='tabs']").tabs({});
				$(element).find('textarea').tinymce(tinymceConfig);
			});

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>
		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzsSOMnI16fb7Uotg0PFRsWCBm8LXF8pw"
	></script>
	<script>
		var geocoder;
		var map;
		var myMarker;
		function initialize(lat=null,lng=null,id) {
			if(lat==null){
				var lat = '22.334';
				var setzoom = 10;
			}else{
				var lat =lat;
				var setzoom = 18;
			}

			if(lng==null){
				var lng ='114.136';
			}else{
				var lng =lng;
			}

			geocoder = new google.maps.Geocoder();
			var latlng = new google.maps.LatLng(lat, lng);
			var mapOptions = {
				zoom: setzoom,
				center: latlng
			}
			map = new google.maps.Map(document.getElementById('map-canvas'+id), mapOptions);

			myMarker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lng),
				draggable: true
			});

			map.setCenter(myMarker.position);
			myMarker.setMap(map);

			google.maps.event.addListener(myMarker, 'dragend', function (evt) {
				recordMarker(evt.latLng);
			});

			google.maps.event.addListener(map, 'rightclick', function (evt) {
				myMarker.setPosition(evt.latLng);
				recordMarker(evt.latLng);
			});
		}

		function recordMarker(evt) {
			console.log(evt);
			document.getElementById('properties_lat').value = evt.lat().toFixed(3);
			document.getElementById('properties_lng').value = evt.lng().toFixed(3);
		}

		function codeAddress() {
			var address = document.getElementById('address').value;
			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);
					myMarker.setPosition(results[0].geometry.location)
					recordMarker(results[0].geometry.location);
					map.setZoom(16);
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		}
	</script>
	<link href="../css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<style>
		.vcenter {
			display: inline-block;
			vertical-align: middle;
			float: none;
		}
		.section_row {
			margin-bottom:10px;
			padding: 7px 15px 4px;
			border: 1px solid #ccc;
			border-radius: 4px;
		}

		#panel {
			position: absolute;
			top: 5px;
			left: 50%;
			margin-left: -180px;
			z-index: 5;
			background-color: #fff;
			padding: 5px;
			border: 1px solid #999;
		}
	</style>
</head>
<body>
	<?php include_once "../menu.php" ?>

	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li><a href="list.php">PEEC Section</a></li>
				<li class="active"><?php echo $peec_section_id == "" ? "Create" : "Update" ?></li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				&nbsp;
			</span>
		</div>

		<div id="top_submit_button_row" class="row" style="padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>


		<div class="row">
			<table class="table" style="margin-bottom: 5px;">
				<tbody>
					<tr class="<?php echo ($peec_section_publish_status == "RCA" || $peec_section_publish_status == "RAA") ? "danger" : "" ?>">
						<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
						<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
							<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $peec_section_publish_status] ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>


		<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td class="col-xs-12" style="vertical-align:middle" colspan="2">
								<div class="row">
									<div class="col-xs-12">
										<a role="button" class="btn btn-default" onclick="addTextBlock(); return false;">
											<i class="fa fa-list fa-lg"></i> Text Section
										</a>
										<a role="button" class="btn btn-default" onclick="addCollapsibleBlock(); return false;">
											<i class="fa fa-text-height fa-lg"></i> Collapsible Text Section
										</a>
										<a role="button" class="btn btn-default" onclick="addPhotoBlock(); return false;">
											<i class="fa fa-list fa-lg"></i> Photo Section
										</a>

										<a role="button" class="btn btn-default" onclick="addVisitBlock(); return false;">
											<i class="fa fa-list fa-lg"></i> Visit Us Section
										</a>

									</div>

								</div>
							</td>
						</tr>



						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Show on the index</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="checkbox" name="peec_section_index_show" id="peec_section_index_show" value="1" <?php echo $peec_section_index_show=='1'?'checked=""':''?>>
							</td>
						</tr>


						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Banner</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="peec_section_banner" name="peec_section_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										(Width: 1920px)
									</p>
								</div>
								<?php if($peec_section_banner != "" && file_exists($peec_section_path . $peec_section_id . "/" . $peec_section_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $peec_section_url . $peec_section_id . "/" . $peec_section_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Thumb</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="peec_section_thumb_banner" name="peec_section_thumb_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										(Width: 290px, Height: 188px)
									</p>
								</div>
								<?php if($peec_section_thumb_banner != "" && file_exists($peec_section_path . $peec_section_id . "/" . $peec_section_thumb_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $peec_section_url . $peec_section_id . "/" . $peec_section_thumb_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (English)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="peec_section_name_lang1" placeholder="Title (English)" value="<?php echo $peec_section_name_lang1?>">
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (繁中)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="peec_section_name_lang2" placeholder="Title (繁中)" value="<?php echo $peec_section_name_lang2?>">
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (簡中)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="peec_section_name_lang3" placeholder="Title (簡中)" value="<?php echo $peec_section_name_lang3?>">
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Date</td>
							<td class="col-xs-6" style="vertical-align:middle">
								<input type="text" id="peec_section_from_date" value="<?php echo $peec_section_from_date?>" class="form-control" placeholder="From Date" name="peec_section_from_date"/>
								<input type="text" id="peec_section_to_date" value="<?php echo $peec_section_to_date?>" class="form-control" placeholder="To Date" name="peec_section_to_date"/>
							</td>

						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Background Color</td>
							<td class="col-xs-3" style="vertical-align:middle">
								<div id="peec_section_background_color" class="input-group colorpicker-component">
									<input type="text" value="<?php echo $peec_section_background_color?$peec_section_background_color:'#ffffff'?>" class="form-control" name="peec_section_background_color"/>
									<span class="input-group-addon"><i></i></span>
								</div>
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Text Color</td>
							<td class="col-xs-3" style="vertical-align:middle">
								<div id="peec_section_text_color" class="input-group colorpicker-component">
									<input type="text" value="<?php echo $peec_section_text_color?$peec_section_text_color:'#000000'?>" class="form-control" name="peec_section_text_color"/>
									<span class="input-group-addon"><i></i></span>
								</div>
							</td>
						</tr>
						<?php if( $peec_section_id == 13 || $peec_section_id == 14 ) { ?>
							<?php if( $peec_section_id == 13 ) { ?>
								<tr>
									<td class="col-xs-2" style="vertical-align:middle">Video Link</td>
									<td class="col-xs-3" style="vertical-align:middle">
										<input type="text" class="form-control" name="peec_section_video_link" placeholder="Video link" value="<?php echo $peec_section_video_link?>">
									</td>
								</tr>
							<?php } ?>
							<?php if( $peec_section_id == 14 ) { ?>
								<tr>
									<td class="col-xs-2" style="vertical-align:middle">Graduate Employment Link</td>
									<td class="col-xs-3" style="vertical-align:middle">
										<input type="text" class="form-control" name="peec_section_link" placeholder="Graduate Employment Link" value="<?php echo $peec_section_link?>">
									</td>
								</tr>
							<?php } ?>
						<?php } ?>


					</tbody>
				</table>
			</div>
			<input type="hidden" name="type" value="peec_section" />
			<input type="hidden" name="processType" id="processType" value="">
			<input type="hidden" name="peec_section_id" value="<?php echo $peec_section_id ?>" />
			<input type="hidden" name="peec_section_type" value="4" />
			<input type="hidden" name="peec_section_status" value="1" />
			<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
			<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
			<input type="hidden" name="randomString" value="<?php echo $randomString ?>" />
			<input type="hidden" name="section_header"  value="<?php echo $section_header ?>" />

		</form>

		<?php $act = "//".$_SERVER['HTTP_HOST']."/en/"; ?>

		<?php foreach($peec_section_section as $value) {
			if($value['peec_section_section_type'] == "T") {
				printTextBlock($value);
			} else if($value['peec_section_section_type'] == "N"){
				printNewsBlock($value);
			}else if($value['peec_section_section_type'] == "P"){
				printProjectBlock($value);
			}else if($value['peec_section_section_type'] == "Photo"){
				printPhotoBlock($value);
			}else if($value['peec_section_section_type'] == "C"){
				printContactBlock($value);
			}else if($value['peec_section_section_type'] == "CT"){
				printCollapsibleTextBlock($value);
			}else if($value['peec_section_section_type'] == "Acknowledgement"){
				printAcknowledgementBlock($value);
			}else if($value['peec_section_section_type'] == "Visit"){
				printVisitBlock($value);
			}
		} ?>


		<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>
	</div>
	<!-- End page content -->
	<?php include_once "../footer.php" ?>
</body>
</html>

<?php
function printTextBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-2 control-label">
					Text Block
				</label>
				<label class="col-xs-2 control-label">
					Background Color
				</label>
				<span class="col-xs-2">
					<div class="input-group colorpicker-component peec_section_section_background_color">
						<input type="text" value="<?php echo $record['peec_section_section_background_color']?$record['peec_section_section_background_color']:'#ffffff'?>" class="form-control" name="peec_section_section_background_color"/>
						<span class="input-group-addon"><i></i></span>
					</div>
				</span>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="T"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang1" name="peec_section_section_title_lang1" placeholder="Title (English)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="peec_section_section_content_lang1"><?php echo htmlspecialchars($record['peec_section_section_content_lang1']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang2" name="peec_section_section_title_lang2" placeholder="Title (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="peec_section_section_content_lang2"><?php echo htmlspecialchars($record['peec_section_section_content_lang2']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang3" name="peec_section_section_title_lang3" placeholder="Title (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="peec_section_section_content_lang3"><?php echo htmlspecialchars($record['peec_section_section_content_lang3']) ?></textarea>
							</div>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	<?php
}

function printNewsBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					News Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="N"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<?php
}

function printProjectBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Project Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="P"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<?php
}

function printContactBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Contact Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="C"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">Info</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="peec_section_section_email" placeholder="E-mail" value="<?php echo htmlspecialchars($record['peec_section_section_email']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="peec_section_section_tel" placeholder="Tel" value="<?php echo htmlspecialchars($record['peec_section_section_tel']) ?>"/>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="peec_section_section_content_lang1" placeholder="Address (English)" value="<?php echo htmlspecialchars($record['peec_section_section_content_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="peec_section_section_content_lang2" placeholder="Address (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_content_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="peec_section_section_content_lang3" placeholder="Address (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_content_lang3']) ?>">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<?php
}

function printPhotoBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	$lang1_files = DM::findAll("peec_section_section_file", " peec_section_section_file_status = ? AND peec_section_section_file_psid = ? AND peec_section_section_file_lang = ? ", " peec_section_section_file_sequence ", array(STATUS_ENABLE, $record['peec_section_section_id'], 'lang1'));

	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Photo Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="Photo"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">Photo</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang1']) ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang1" name="peec_section_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang2']) ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang2" name="peec_section_section_title_lang2" placeholder="Heading (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang3']) ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang3" name="peec_section_section_title_lang3" placeholder="Heading (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang3']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-4">
									<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
								</div>
							</div>
						</div>
					</div>
					<?php
					foreach($lang1_files as $lang1_file) {
						printFileItem($lang1_file);
					}
					?>

				</div>
			</div>
		</form>
	</div>
	<?php
}

function printAcknowledgementBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	$lang1_files = DM::findAll("peec_section_section_file", " peec_section_section_file_status = ? AND peec_section_section_file_psid = ? AND peec_section_section_file_lang = ? ", " peec_section_section_file_sequence ", array(STATUS_ENABLE, $record['peec_section_section_id'], 'lang1'));
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Acknowledgement Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="Acknowledgement"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">Logo</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang1" name="peec_section_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang1']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang2" name="peec_section_section_title_lang2" placeholder="Heading (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang2']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang3" name="peec_section_section_title_lang3" placeholder="Heading (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang3']) ?>">
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-4">
									<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this);">
								</div>
							</div>
						</div>
					</div>
					<?php
					foreach($lang1_files as $lang1_file) {
						printFileItem($lang1_file);
					}
					?>

				</div>
			</div>
		</form>
	</div>
	<?php
}

function printFileItem($fileItem){
	?>
	<?php if($fileItem['peec_section_section_file_type'] == "Partner") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
			<div class="col-xs-1">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12 control-label" style="height: 34px;">
						Title
					</div>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="peec_section_section_file_title" placeholder="Title (English)" value="<?php echo htmlspecialchars($fileItem['peec_section_section_file_title']) ?>"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="../../uploaded_files/peec_section_file/<?php echo $fileItem['peec_section_section_file_pid'] . "/". $fileItem['peec_section_section_file_id'] ?>/<?php echo $fileItem['peec_section_section_file_filename'] ?> " target="_blank">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="peec_section_section_file_id" value="<?php echo htmlspecialchars($fileItem['peec_section_section_file_id']) ?>"/>
				<input type="hidden" name="peec_section_section_file_lang" value="<?php echo htmlspecialchars($fileItem['peec_section_section_file_lang']) ?>"/>
				<input type="hidden" name="peec_section_section_file_type" value="<?php echo htmlspecialchars($fileItem['peec_section_section_file_type']) ?>"/>
				<input type="hidden" name="peec_section_section_file_url" value=""/>
			</div>
		</div>
	<?php } ?>
	<?php
}

function printCollapsibleTextBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-3 control-label">
					Collapsible Text Block
				</label>
				<span class="col-xs-9" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="CT"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>

				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="peec_section_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang1']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="peec_section_section_content_lang1"><?php echo htmlspecialchars($record['peec_section_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="peec_section_section_title_lang2" placeholder="Heading (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang2']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="peec_section_section_content_lang2"><?php echo htmlspecialchars($record['peec_section_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="peec_section_section_title_lang3" placeholder="Heading (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang3']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="peec_section_section_content_lang3"><?php echo htmlspecialchars($record['peec_section_section_content_lang3']) ?></textarea>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	<?php
}

function printVisitBlock($record){
	$row_id = $record['peec_section_section_type'] . '-' . $record['peec_section_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Visit Us Block
				</label>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="peec_section_section_type" value="Visit"/>
					<input type="hidden" name="peec_section_section_id" value="<?php echo $record['peec_section_section_id'] ?>"/>
					<input type="hidden" name="peec_section_section_sequence" value="<?php echo $record['peec_section_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">Location</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-4">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<label class="col-xs-3 control-label" style="padding-right:0px">Location</label>
						<div class="col-xs-5">
							<input type="text" class="form-control" placeholder="Location(for search Latitude and longitude only)" name="address" id="address" value=""/>
						</div>
						<div class="col-xs-3" style="margin-bottom: 10px;">
							<input type="button" class="btn btn-primary" value="Search" onclick="codeAddress()">
						</div>
						<div id="map-canvas<?php echo $row_id ?>" style="width:1000px;height:400px"></div>

					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" style="padding-right:0px">Latitude and longitude</label>
						<div class="col-xs-4">
							<input type="text" class="form-control" placeholder="Latitude" name="peec_section_lat" id="properties_lat" value="<?php echo htmlspecialchars($record['peec_section_lat']) ?>"/>
						</div>

						<div class="col-xs-4">
							<input type="text" class="form-control" placeholder="longitude" name="peec_section_lng" id="properties_lng" value="<?php echo htmlspecialchars($record['peec_section_lng']) ?>"/>
						</div>

					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang1" name="peec_section_section_name_lang1" placeholder="Section name (English)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang1" name="peec_section_section_title_lang1" placeholder="Title (English)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang1']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="peec_section_section_content_lang1"><?php echo htmlspecialchars($record['peec_section_section_content_lang1']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang2" name="peec_section_section_name_lang2" placeholder="Section name (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang2" name="peec_section_section_title_lang2" placeholder="Title (繁中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang2']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="peec_section_section_content_lang2"><?php echo htmlspecialchars($record['peec_section_section_content_lang2']) ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-4">
					<div class="form-group">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-name-lang3" name="peec_section_section_name_lang3" placeholder="Section name (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_name_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input type="text" class="form-control" id="section-row-<?php echo $row_id ?>-title-lang3" name="peec_section_section_title_lang3" placeholder="Title (簡中)" value="<?php echo htmlspecialchars($record['peec_section_section_title_lang3']) ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="peec_section_section_content_lang3"><?php echo htmlspecialchars($record['peec_section_section_content_lang3']) ?></textarea>
							</div>
						</div>
					</div>
				</div>

			</div>
		</form>
		<script>initialize('<?php echo $record['peec_section_lat']?>','<?php echo $record['peec_section_lng']?>','<?php echo $row_id ?>');</script>
	</div>

	<?php
}
?>