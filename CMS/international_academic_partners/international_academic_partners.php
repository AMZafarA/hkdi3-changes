<?php
include "../include/config.php";
include $SYSTEM_BASE . "include/function.php";
include $SYSTEM_BASE . "session.php";
include $SYSTEM_BASE . "include/system_message.php";
include $SYSTEM_BASE . "include/classes/DataMapper.php";
include $SYSTEM_BASE . "include/classes/SystemUtility.php";
include $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

$alumni_id = $_REQUEST['alumni_id'] ?? '';
$alumni_lv1 = $_REQUEST['alumni_lv1'] ?? '';
$alumni_lv2 = $_REQUEST['alumni_lv2'] ?? '';
$msg = $_REQUEST['msg'] ?? '';
$international_academic_partners_id = $_REQUEST['international_academic_partners_id'] ?? '';
if(!isInteger($international_academic_partners_id))
{
	$international_academic_partners_id = "";
}

if($international_academic_partners_id != ""){
	$obj_row = DM::load('international_academic_partners', $international_academic_partners_id);

	if($obj_row == "")
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	foreach($obj_row as $rKey => $rValue){
		$$rKey = htmlspecialchars($rValue);
	}

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "industrial")){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	if(!file_exists($international_academic_partners_path . $international_academic_partners_id)){
		mkdir($international_academic_partners_path . $international_academic_partners_id, 0775, true);
	}

	$_SESSION['RF']['subfolder'] = "international_academic_partners/" . $international_academic_partners_id . "/";


	$last_updated_user = DM::load('user', $international_academic_partners_updated_by);

} else if(!PermissionUtility::canCreateByKey($_SESSION['sess_id'], "industrial")){
	header("location: ../dashboard.php?msg=F-1000");
	return;
} else {
	$international_academic_partners_publish_status = "D";
	foreach( DM::stub('international_academic_partners') as $rKey => $rValue ) {

		if ( ! isset( $$rKey ) ) {
			$$rKey = htmlspecialchars($rValue);
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once "../header.php" ?>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.cookie.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/bootstrap-colorpicker.min.js"></script>
	<link href="<?php echo $SYSTEM_HOST ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">


	<script type="text/x-tmpl" id="PDF_file_template">
		<div class="form-group file_row" style="margin-bottom:15px;">
			<div class="col-xs-1">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12 control-label" style="height: 34px;">
						Alt
					</div>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="international_academic_partners_image_alt" placeholder="Alt" value="{%=o.filename%}"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
				<input type="hidden" name="international_academic_partners_image_id" value=""/>
				<input type="hidden" name="international_academic_partners_image_type" value="I"/>
				<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
			</div>

		</div>
	</script>


	<script type="text/x-tmpl" id="member_file_template">
		<div class="form-group file_row" style="margin-bottom:15px;">


			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-xs-2">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Desc(Eng)
						</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="international_academic_partners_image_desc_lang1" placeholder="Desc(Eng)" value=""/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<div class="form-group">
						<div class="col-xs-12">
							File : {%=o.filename%}
						</div>
					</div>
				</div>
			</div>



			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-xs-2">
					<div class="form-group">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Desc(繁中)
						</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="international_academic_partners_image_desc_lang2" placeholder="Desc(繁中)" value=""/>
						</div>
					</div>
				</div>
			</div>


			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-xs-2">
					<div class="form-group">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Desc(簡中)
						</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="international_academic_partners_image_desc_lang3" placeholder="Desc(簡中)" value=""/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="international_image_id" value="M"/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
				</div>




			</div>

		</div>
	</script>



	<script>
		var file_list = {};
		var counter = 0;

		function checkForm(){
			var flag=true;
			var errMessage="";

			trimForm("form1");

			if($("#press_release_news_date").val() == ""){
				flag = false;
				errMessage += "News Date\n";
			}

			if($("#news_start_show_date").val() == ""){
				flag = false;
				errMessage += "Start Display Date\n";
			}

			if($("#news_eng_title").val() == ""){
				flag = false;
				errMessage += "Title (English)\n";
			}

			if($("#news_chi_title").val() == ""){
				flag = false;
				errMessage += "Title (中文)\n";
			}

			<?php if($news_id == "") { ?>
				if($("#upload_eng_pdf").val() == ""){
					flag = false;
					errMessage += "PDF (English)\n";
				}

				if($("#upload_chi_pdf").val() == ""){
					flag = false;
					errMessage += "PDF (中文)\n";
				}
			<?php } ?>

			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#loadingModalDialog').on('shown.bs.modal', function (e) {
				submitForm();
			})

			$('#loadingModalDialog').modal('show');
		}

		function checkForm2(){
			var flag=true;
			var errMessage="";

			trimForm("form1");

			if($("#news_news_date").val() == ""){
				flag = false;
				errMessage += "News Date\n";
			}

			if($("#news_start_show_date").val() == ""){
				flag = false;
				errMessage += "Start Display Date\n";
			}

			if($("#news_eng_title").val() == ""){
				flag = false;
				errMessage += "Title (English)\n";
			}

			if($("#news_chi_title").val() == ""){
				flag = false;
				errMessage += "Title (中文)\n";
			}

			<?php if($news_id == "") { ?>
				if($("#upload_eng_pdf").val() == ""){
					flag = false;
					errMessage += "PDF (English)\n";
				}

				if($("#upload_chi_pdf").val() == ""){
					flag = false;
					errMessage += "PDF (中文)\n";
				}
			<?php } ?>

			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#approvalModalDialog').modal('show');
		}

		function checkForm3(){
			var flag=true;
			var errMessage="";

			trimForm("form1");


			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#loadingModalDialog').on('shown.bs.modal', function (e) {
				$('#processType').val('submit_for_production');
				submitForm();
			})

			$('#loadingModalDialog').modal('show');
			$('#processType').val('');
		}

		function corp(){
			if (confirm('Are you sure you want to corp the image?')) {
				$('#processType').val('corp');
				document.form1.submit();
			} else {
			    // Do nothing!
			}

		}

		function submitForm(){
			var obj = {"international_academic_partners_image_type":"I"};
			obj['lang1_files'] = [];
			cloneFile(obj,0);

			//document.form1.submit();
		}

		function convert_tchi_to_schi() {
			$("#properties_name_lang3").val(toSimplifiedString($("#properties_name_lang2").val()));
			$("#properties_address_lang3").val(toSimplifiedString($("#properties_address_lang2").val()));
			$("#properties_parking_lang3").val(toSimplifiedString($("#properties_parking_lang2").val()));
			$("#properties_lift_lang3").val(toSimplifiedString($("#properties_lift_lang2").val()));
			$("#properties_floor_height_lang3").val(toSimplifiedString($("#properties_floor_height_lang2").val()));
			$("#properties_floor_loading_lang3").val(toSimplifiedString($("#properties_floor_loading_lang2").val()));
			$("#properties_park_entrance_lang3").val(toSimplifiedString($("#properties_park_entrance_lang2").val()));
			$("#properties_cargo_lift_lang3").val(toSimplifiedString($("#properties_cargo_lift_lang2").val()));
			$("#properties_cargo_lift_max_load_lang3").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang2").val()));
			$("#properties_air_conditioning_lang3").val(toSimplifiedString($("#properties_air_conditioning_lang2").val()));
			$("#properties_fire_services_lang3").val(toSimplifiedString($("#properties_fire_services_lang2").val()));
			$("#properties_security_lang3").val(toSimplifiedString($("#properties_security_lang2").val()));
			$("#properties_transportaion_lang3").val(toSimplifiedString($("#properties_transportaion_lang2").val()));
			$("#properties_enquiries_lang3").val(toSimplifiedString($("#properties_enquiries_lang2").val()));
			$("#properties_enquiries2_lang3").val(toSimplifiedString($("#properties_enquiries2_lang2").val()));
			$("#properties_keywords_lang3").val(toSimplifiedString($("#properties_keywords_lang2").val()));
			$("#properties_description_lang3").val(toSimplifiedString($("#properties_description_lang2").val()));
		};

		function convert_schi_to_tchi() {
			$("#properties_name_lang2").val(toTraditionalString($("#properties_name_lang3").val()));
			$("#properties_address_lang2").val(toTraditionalString($("#properties_address_lang3").val()));
			$("#properties_parking_lang2").val(toTraditionalString($("#properties_parking_lang3").val()));
			$("#properties_lift_lang2").val(toTraditionalString($("#properties_lift_lang3").val()));
			$("#properties_floor_height_lang2").val(toTraditionalString($("#properties_floor_height_lang3").val()));
			$("#properties_floor_loading_lang2").val(toTraditionalString($("#properties_floor_loading_lang3").val()));
			$("#properties_park_entrance_lang2").val(toTraditionalString($("#properties_park_entrance_lang3").val()));
			$("#properties_cargo_lift_lang2").val(toTraditionalString($("#properties_cargo_lift_lang3").val()));
			$("#properties_cargo_lift_max_load_lang2").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang3").val()));
			$("#properties_air_conditioning_lang2").val(toSimplifiedString($("#properties_air_conditioning_lang3").val()));
			$("#properties_fire_services_lang2").val(toSimplifiedString($("#properties_fire_services_lang3").val()));
			$("#properties_security_lang2").val(toSimplifiedString($("#properties_security_lang3").val()));
			$("#properties_transportaion_lang2").val(toTraditionalString($("#properties_transportaion_lang3").val()));
			$("#properties_enquiries_lang2").val(toTraditionalString($("#properties_enquiries_lang3").val()));
			$("#properties_enquiries2_lang2").val(toTraditionalString($("#properties_enquiries2_lang3").val()));
			$("#properties_keywords_lang2").val(toSimplifiedString($("#properties_keywords_lang3").val()));
			$("#properties_description_lang2").val(toSimplifiedString($("#properties_description_lang3").val()));

		};


		function addFile(fileBrowser, language){
			if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
				return;

			var files = $(fileBrowser)[0].files;

			for(var i = 0; i < files.length; i++){
				var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
				file_list[obj.tmp_id] = files[i];
				var content = tmpl("PDF_file_template", obj);
				var header_row = $(fileBrowser).closest("div.form-group");

				if($(header_row).is(":last-child")){
					$(content).insertAfter(header_row);
				} else {
					$(content).insertAfter($(header_row).siblings(":last"));
				}
			}
		}

		function addMember(fileBrowser, language){
			if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
				return;

			var files = $(fileBrowser)[0].files;

			for(var i = 0; i < files.length; i++){
				var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
				file_list[obj.tmp_id] = files[i];
				var content = tmpl("member_file_template", obj);
				var header_row = $(fileBrowser).closest("div.form-group");

				if($(header_row).is(":last-child")){
					$(content).insertAfter(header_row);
				} else {
					$(content).insertAfter($(header_row).siblings(":last"));
				}
			}
		}

		function cloneFile(dataObj, elementIdx){
			var frm = $("#form1");

			if($(frm).find("#tabs-5 div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','file_section[]').attr('type','hidden').val(data).appendTo('#form1');
				//document.form1.submit();
				var obj2 = {"international_academic_partners_image_type":"M"};
				obj2['lang1_files'] = [];
				cloneMemberFile(obj2,0);
				//document.form1.submit();
				return;
			}

			var row = $(frm).find("#tabs-5 div.file_row:eq(" + elementIdx + ")");

			//var files =$("input[name='project_image_filename']")[0].files;

			/*if(files.length == 0){

				return;
			}*/



			var fileObj = {};
			fileObj['international_academic_partners_image_alt'] = $(row).find("input[name='international_academic_partners_image_alt']").val();
			fileObj['international_academic_partners_image_id'] = $(row).find("input[name='international_academic_partners_image_id']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";






			var fd = new FormData();
			//fd.append('upload_file', files[0]);
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;

					dataObj['lang1_files'].push(fileObj);
					cloneFile(dataObj, ++elementIdx);
					//$('<input>').attr('name','file_section[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');

					//document.form1.submit();
				}
			});


		}


		function cloneMemberFile(dataObj, elementIdx){
			var frm = $("#form1");

			if($(frm).find("#tabs-6 div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','file_member_section[]').attr('type','hidden').val(data).appendTo('#form1');
				document.form1.submit();
				return;
			}

			var row = $(frm).find("#tabs-6 div.file_row:eq(" + elementIdx + ")");

			//var files =$("input[name='project_image_filename']")[0].files;

			/*if(files.length == 0){

				return;
			}*/

			var fileObj = {};
			fileObj['international_academic_partners_image_alt'] = $(row).find("input[name='international_academic_partners_image_alt']").val();
			fileObj['international_academic_partners_image_id'] = $(row).find("input[name='international_academic_partners_image_id']").val();
			fileObj['international_academic_partners_image_desc_lang1'] = $(row).find("input[name='international_academic_partners_image_desc_lang1']").val();
			fileObj['international_academic_partners_image_desc_lang2'] = $(row).find("input[name='international_academic_partners_image_desc_lang2']").val();
			fileObj['international_academic_partners_image_desc_lang3'] = $(row).find("input[name='international_academic_partners_image_desc_lang3']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";

			var fd = new FormData();
			//fd.append('upload_file', files[0]);
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;

					dataObj['lang1_files'].push(fileObj);
					cloneMemberFile(dataObj, ++elementIdx);
					//$('<input>').attr('name','file_section[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');

					//document.form1.submit();
				}
			});


		}

		function moveFileUp(button){
			var $current = $(button).closest(".file_row");
			var $previous = $current.prev('.file_row');
			if($previous.length !== 0){
				$current.insertBefore($previous);
			}
		}

		function moveFileDown(button){
			var $current = $(button).closest(".file_row");
			var $next = $current.next('.file_row');
			if($next.length !== 0){
				$current.insertAfter($next);
			}
		}

		function removeFileItem(button){
			$(button).closest(".file_row").detach();
		}


		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form1;
					frm.processType.value = "submit_for_approval";
					submitForm();
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		$( document ).ready(function() {
			$("#tabs").tabs({});

			$("#international_academic_partners_image,#international_academic_partners_image1,#international_academic_partners_thumb").filestyle({buttonText: ""});

			$('.tinymcetextarea').tinymce(tinymceConfig);

			//$("#news_date,#news_start_show_date,#news_end_show_date").datepicker({"dateFormat" : "yy-mm-dd"});



			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>

		});
	</script>
	<style>
		.vcenter {
			display: inline-block;
			vertical-align: middle;
			float: none;
		}
	</style>


</head>
<body>
	<?php include_once "../menu.php" ?>

	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li class="active">International</li>
				<li class="active"><a href="list.php">International Academic Partners</a></li>
				<li class="active"><?php echo $international_academic_partners_id == "" ? "Create" : "Update" ?></li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				&nbsp;
			</span>
		</div>

		<div id="top_submit_button_row" class="row" style="padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>

		<div class="row">
			<table class="table" style="margin-bottom: 5px;">
				<tbody>
					<tr class="<?php echo ($international_academic_partners_publish_status == "RCA" || $international_academic_partners_publish_status == "RAA") ? "danger" : "" ?>">
						<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
						<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
							<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $international_academic_partners_publish_status] ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div id="tabs" class="col-xs-12">
				<ul>
					<li><a href="#tabs-1">ENG</a></li>
					<li><a href="#tabs-2">繁中</a></li>
					<li><a href="#tabs-3">簡中</a></li>
				</ul>

				<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">

					<div id="tabs-1">

						<div class="form-group">
							<label class="col-xs-2 control-label" style="padding-right:0px">Name</label>
							<div class="col-xs-10">
								<input type="text" class="form-control" placeholder="Name" name="international_academic_partners_name_lang1" id="international_academic_partners_name_lang1" value="<?php echo $international_academic_partners_name_lang1 ?>"/>
							</div>
						</div>

					</div>

					<div id="tabs-2">

						<div class="form-group">
							<label class="col-xs-2 control-label" style="padding-right:0px">Name</label>
							<div class="col-xs-10">
								<input type="text" class="form-control" placeholder="Name" name="international_academic_partners_name_lang2" id="international_academic_partners_name_lang2" value="<?php echo $international_academic_partners_name_lang2 ?>"/>
							</div>
						</div>
					</div>


					<div id="tabs-3">

						<div class="form-group">
							<label class="col-xs-2 control-label" style="padding-right:0px">Name</label>
							<div class="col-xs-10">
								<input type="text" class="form-control" placeholder="Name" name="international_academic_partners_name_lang3" id="international_academic_partners_name_lang3" value="<?php echo $international_academic_partners_name_lang3 ?>"/>
							</div>
						</div>
					</div>

					<input type="hidden" name="type" id="type" value="international_academic_partners" />
					<input type="hidden" name="processType"  id="processType" value="">
					<input type="hidden" name="international_academic_partners_id" id="international_academic_partners_id" value="<?php echo $international_academic_partners_id ?>" />
					<input type="hidden" name="international_academic_partners_status" id="international_academic_partners_status" value="1" />
					<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
					<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
				</form>


			</div>
		</div>

		<div class="row" style="padding-top:10px;padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<!--<button type="button" class="btn btn-primary" id="btn_tchi_to_schi" onclick="convert_tchi_to_schi(); return false;">
					<i class="fa fa-language fa-lg"></i> 繁 &gt; 簡
				</button>

				<button type="button" class="btn btn-primary" id="btn_tchi_to_schi" onclick="convert_tchi_to_schi(); return false;">
					<i class="fa fa-language fa-lg"></i> 簡 &gt; 繁
				</button>-->

				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>

	</div>

	<?php $act = "//".$_SERVER['HTTP_HOST']."/en/"; ?>
	<script>function previewPost() {document.getElementById('preview-form').submit();}</script>
	<form id="preview-form" target="_blank" method="get" action="<?php echo $act;?>industrial-international_academic_partnerss/industrial-international_academic_partnerss-details.php" style="display: none;"><input type="text" name="international_academic_partners_id" value="<?php echo $international_academic_partners_id; ?>"><input type="text" name="preview" value="true"></form>
	<!-- End page content -->
	<?php include_once "../footer.php" ?>
</body>
</html>
