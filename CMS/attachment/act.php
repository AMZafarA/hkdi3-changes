<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	$type_name = "Attachment";
	$permission_name = "industrial";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="international" ) {

		$id = $_REQUEST['international_id'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "industrial")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			if($isNew){
				$values['international_publish_status'] = "D";
				$values['international_status'] = STATUS_ENABLE;
				$values['international_created_by'] = $_SESSION['sess_id'];
				$values['international_created_date'] = $timestamp;
				$values['international_updated_by'] = $_SESSION['sess_id'];
				$values['international_updated_date'] = $timestamp;

				$id = DM::insert('international', $values);

				if(!file_exists($international_path . $id. "/")){
					mkdir($international_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('international', $id);



				if($ObjRec['international_status'] == STATUS_DISABLE)

				{

					header("location: list.php?international_type=".$international_type."&msg=F-1002");

					return;

				}


				$values['international_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['international_updated_by'] = $_SESSION['sess_id'];

				$values['international_updated_date'] = $timestamp;



				DM::update('international', $values);

			}


			$ObjRec = DM::load('international', $id);

			$international_img_arr = array('international_image','international_thumb');

			foreach($international_img_arr as $key=>$val){

				if( isset( $_FILES[$val] ) ) {
					if($_FILES[$val]['tmp_name'] != ""){

						if($ObjRec[$val] != "" && file_exists($international_path . $id . "/" . $ObjRec[$val])){
							@unlink($international_path  . $id . "/" . $ObjRec[$val]);
						}

						$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

						$newname = moveFile($international_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


							list($width, $height) = getimagesize($international_path . $id . "/".$newname);

							/*if($width > '1500'){
								resizeImageByWidth($alumni_path. $id."/". $newname, "1500" , $alumni_path. $id."/". $newname);
							}else if($height > '1000'){
								resizeImageByHeight($alumni_path. $id."/". $newname, "1000" , $alumni_path. $id."/". $newname);
							}*/



						$values = array();
						$values[$val] = $newname;
						$values['international_id'] = $id;

						DM::update('international', $values);
						unset($values);

						$ObjRec = DM::load('international', $id);
					}

				}
			}


			//*************add image**************//
			$files = $_REQUEST['file_section'] ?? '';
			$file_section_list = array();
			if($files != "" && is_array($files)){
				foreach($files as $file){
					$file = json_decode($file, true);

					$lang1_files = $file['lang1_files'];
					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){
							if($lang1_file['international_image_id'] == "") {

								$lang1_file['international_image_pid'] = $id;
								$lang1_file['international_image_seq'] = $count++;
								$lang1_file['international_image_filename'] = $lang1_file['filename'];
								$lang1_file['international_image_alt'] = $lang1_file['international_image_alt'];
								$lang1_file['international_image_status'] = STATUS_ENABLE;
								$lang1_file['international_image_created_by'] = $_SESSION['sess_id'];
								$lang1_file['international_image_created_date'] = $timestamp;
								$lang1_file['international_image_updated_by'] = $_SESSION['sess_id'];
								$lang1_file['international_image_updated_date'] = $timestamp;

								$file_section_id = DM::insert('international_image', $lang1_file);

								$file_section_list[] = $file_section_id;

								if(!file_exists($international_path . $id. "/".$file_section_id."/")){
									mkdir($international_path . $id . "/".$file_section_id."/", 0775, true);
								}

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $international_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);


									list($width, $height) = getimagesize($international_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);

									if($width > '1000'){
										resizeImageByWidth($international_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $international_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}else if($height > '1000'){
										resizeImageByHeight($international_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $international_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}


									$crop = $international_path . $id . "/".$file_section_id."/". "crop_".$lang1_file['filename'];
									cropImage($international_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "125","100" , $crop);

							}else{
								$file_section_list[] = $lang1_file['international_image_id'];
								$file_section_id = $lang1_file['file_section_id'];
								$lang1_file['international_image_seq'] = $count++;
								$lang1_file['international_image_update_by'] = $_SESSION['sess_id'];
								$lang1_file['international_image_update_date'] = $timestamp;

								DM::update('international_image', $lang1_file);
							}
						}
					}
				}
			}

			$sql = " international_image_status = ? AND international_image_pid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($file_section_list) > 0){ print_r($file_section_list);
				$sql .= " AND international_image_id NOT IN (" . implode(',', array_fill(0, count($file_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $file_section_list);

			}

			$international_section_files = DM::findAll("international_image", $sql, " international_image_seq ", $parameters);

			foreach($international_section_files as $international_section_file){

				$values = array();
				$values['international_image_id'] = $international_section_file['international_image_id'];
				$values['international_image_status'] = STATUS_DISABLE;
				$values['international_image_update_by'] = $_SESSION['sess_id'];
				$values['international_image_update_date'] = $timestamp;

				DM::update('international_image', $values);

				unset($values);
			}

			//*************add image**************//



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "international '" . $ObjRec['international_name_lang1'] . "' (ID:" . $ObjRec['international_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( $permission_name,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['international_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $permission_name, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."attachment/attachment.php");

				$approval_id = ApprovalService::createApprovalRequest( $permission_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);

			}


				header("location:attachment.php?msg=S-1001");
				return;



		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('international', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?international_lv1=".$international_lv1."&international_lv2=".$international_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['international_status'] == STATUS_DISABLE)

			{

				header("location: list.php?international_lv1=".$international_lv1."&international_lv2=".$international_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("international", $id);

			$values['international_id'] = $id;
			$values['international_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['international_status'] = STATUS_DISABLE;
			$values['international_updated_by'] = $_SESSION['sess_id'];
			$values['international_updated_date'] = getCurrentTimestamp();


			DM::update('international', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("international_" . $international_lv1."_".$international_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "international '" . $ObjRec['international_name_lang1'] . "' (ID:" . $ObjRec['international_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("international_" . $international_lv1."_".$international_lv2, $id, $timestamp, $_SESSION['sess_id'], array("E"), "international_" . $international_lv1."_".$international_lv2, "DS");

				$files = array($SYSTEM_HOST."attachment/attachment.php");

				$approval_id = ApprovalService::createApprovalRequest("international_" . $international_lv1."_".$international_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("international_" . $international_lv1."_".$international_lv2, $id, $timestamp, $_SESSION['sess_id'], array("A"), "international_" . $international_lv1."_".$international_lv2, "PA", $approval_id);
			}





			header("location: list.php?international_lv1=".$international_lv1."&international_lv2=".$international_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('international', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?international_lv1=".$international_lv1."&international_lv2=".$international_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['international_status'] == STATUS_ENABLE || $ObjRec['international_publish_status'] == 'AL')

			{

				header("location: list.php?international_lv1=".$international_lv1."&international_lv2=".$international_lv2."&msg=F-1006");

				return;

			}



			$values['international_id'] = $id;

			$values['international_publish_status'] = 'D';

			$values['international_status'] = STATUS_ENABLE;

			$values['international_updated_by'] = $_SESSION['sess_id'];

			$values['international_updated_date'] = getCurrentTimestamp();



			DM::update('international', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'international', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of international '" . $ObjRec['international_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'product_' . $id, $message);

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "international '" . $ObjRec['international_name_lang1'] . "' (ID:" . $ObjRec['international_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), 'product_' . $id, "Product '" . $ObjRec['product_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>