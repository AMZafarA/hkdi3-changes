<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="continuing_education_group" ) {

		$id = $_REQUEST['continuing_education_group_id'] ?? '';
		$continuing_education_group_lv1 = $_REQUEST['continuing_education_group_lv1'] ?? '';
		$continuing_education_group_lv2 = $_REQUEST['continuing_education_group_lv2'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){ 

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "continuing_edcation")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {			

			$values = $_REQUEST;

			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();

			if($isNew){

				$values['continuing_education_group_publish_status'] = "D";

				$values['continuing_education_group_status'] = STATUS_ENABLE;

				$values['continuing_education_group_created_by'] = $_SESSION['sess_id'];

				$values['continuing_education_group_created_date'] = $timestamp;

				$values['continuing_education_group_updated_by'] = $_SESSION['sess_id'];

				$values['continuing_education_group_updated_date'] = $timestamp;
				
				$id = DM::insert('continuing_education_group', $values);



			} else {

				$ObjRec = DM::load('continuing_education_group', $id);

				

				if($ObjRec['continuing_education_group_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}

				
				$values['continuing_education_group_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['continuing_education_group_updated_by'] = $_SESSION['sess_id'];

				$values['continuing_education_group_updated_date'] = $timestamp;



				DM::update('continuing_education_group', $values);

			}

			
			$ObjRec = DM::load('continuing_education_group', $id);
			



			if($processType == ""){

				//Logger::log($_SESSION['sess_id'], $timestamp, "continuing_education_group '" . $ObjRec['continuing_education_group_name_lang1'] . "' (ID:" . $ObjRec['continuing_education_group_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification('continuing_education_group', $id, $timestamp, $_SESSION['sess_id'], array("A","E"), 'continuing_edcation', $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){ 
				
				ApprovalService::withdrawApproval( "continuing_edcation",$type, $id, $timestamp);
			
				//Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['continuing_education_group_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification('continuing_education_group', $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."continuing_education_group/continuing_education_group.php?continuing_education_group_id=".$id);
				
				$approval_id = ApprovalService::createApprovalRequest( "continuing_edcation", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);				
				NotificationService::sendApprovalRequest("continuing_education_group", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);
				
			}
			
			header("location:continuing_education_group.php?continuing_education_group_id=" . $id . "&msg=S-1001");
			return;
		}

			
			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();

			

			$ObjRec = DM::load('continuing_education_group', $id);

			

			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}

			

			if($ObjRec['continuing_education_group_status'] == STATUS_DISABLE)

			{

				header("location: list.php?msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("continuing_education_group", $id);

			$values['continuing_education_group_id'] = $id;
			$values['continuing_education_group_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['continuing_education_group_status'] = STATUS_DISABLE;			
			$values['continuing_education_group_updated_by'] = $_SESSION['sess_id'];
			$values['continuing_education_group_updated_date'] = getCurrentTimestamp();
			

			DM::update('continuing_education_group', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("continuing_edcation", $type, $id, $timestamp);
			
				//Logger::log($_SESSION['sess_id'], $timestamp, "continuing_education_group '" . $ObjRec['continuing_education_group_name_lang1'] . "' (ID:" . $ObjRec['continuing_education_group_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("continuing_education_group", $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", "DS");
				
				$files = array($SYSTEM_HOST."continuing_education_group/continuing_education_group.php?continuing_education_group_id=".$id);
				
				$approval_id = ApprovalService::createApprovalRequest("continuing_edcation", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("continuing_education_group", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);
			}else{
				NotificationService::sendNotification("continuing_education_group", $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "continuing_edcation", "D");
			}



	

			header("location: list.php?msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {		

			$timestamp = getCurrentTimestamp();

			

			$ObjRec = DM::load('continuing_education_group', $id);

			

			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}

			

			if($ObjRec['continuing_education_group_status'] == STATUS_ENABLE || $ObjRec['continuing_education_group_publish_status'] == 'AL')

			{

				header("location: list.php?msg=F-1006");

				return;

			}

			

			$values['continuing_education_group_id'] = $id;

			$values['continuing_education_group_publish_status'] = 'D';

			$values['continuing_education_group_status'] = STATUS_ENABLE;			

			$values['continuing_education_group_updated_by'] = $_SESSION['sess_id'];

			$values['continuing_education_group_updated_date'] = getCurrentTimestamp();

			

			DM::update('continuing_education_group', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			

			$parameters = array(STATUS_ENABLE, 'continuing_education_group', $id, 'RCA', 'RAA', 'APL');

			

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			

			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;

				

				DM::update('approval', $values);

				unset($values);

				

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));

				

				$recipient_list = array("E", "C");

				

				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}

				

				$message = "";

				

				switch($approval_request['approval_approval_status']){

					case "RCA": 

						$message = "Request for checking";

					break;

					case "RAA": 

						$message = "Request for approval";

					break;

					case "APL": 

						$message = "Pending to launch";

					break;

				}

				

				$message .= " of signed '" . $ObjRec['continuing_education_group_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'news_' . $news_lv1."_".$news_lv2, "WA");

			}

		

			Logger::log($_SESSION['sess_id'], $timestamp, "signed '" . $ObjRec['continuing_education_group_name_lang1'] . "' (ID:" . $ObjRec['continuing_education_group_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'news_' . $news_lv1."_".$news_lv2, "UD");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>