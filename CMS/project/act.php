<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="project" )

	{

		$id = $_REQUEST['project_id'] ?? '';
		$project_lv1 = $_REQUEST['project_lv1'] ?? '';
		$project_lv2 = $_REQUEST['project_lv2'] ?? '';

		if($id){
			$ObjRec = DM::load('project', $id);
			$project_lv1 = $ObjRec['project_lv1'];
			$project_lv2 = $ObjRec['project_lv2'];
		}

		$type_name = DM::findOne("department", " department_id = ?", " department_id ", array($project_lv2));

		$type_namelv2 = $type_name['department_name_lang1'];

		$type_name = DM::findOne("department", " department_id = ?", " department_id ", array($project_lv1));

		$type_namelv1 = $type_name['department_name_lang1'];

		$type_name = "Project";

		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if($type_namelv2){ $type_name .= " - ".$type_namelv2;}

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "project_".$project_lv1."_".$project_lv2)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['project_publish_status'] = "D";

				$values['project_status'] = STATUS_ENABLE;

				$values['project_created_by'] = $_SESSION['sess_id'];

				$values['project_created_date'] = $timestamp;

				$values['project_updated_by'] = $_SESSION['sess_id'];

				$values['project_updated_date'] = $timestamp;



				$id = DM::insert('project', $values);


				if(!file_exists($project_path . $id. "/")){
					mkdir($project_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('project', $id);



				if($ObjRec['project_status'] == STATUS_DISABLE)

				{

					header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=F-1002");

					return;

				}


				$values['project_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['project_updated_by'] = $_SESSION['sess_id'];

				$values['project_updated_date'] = $timestamp;



				DM::update('project', $values);

			}


			$ObjRec = DM::load('project', $id);

			$project_img_arr = array('project_image1','project_image2');

			foreach($project_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($project_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($project_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($project_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($project_path . $id . "/".$newname);

						$newWidth ="638";
						$newHeight="376";

					cropImage($project_path. $id."/". $newname, $newWidth,$newHeight , $project_path. $id."/". $newname);


					$values = array();
					$values[$val] = $newname;
					$values['project_id'] = $id;

					DM::update('project', $values);
					unset($values);

					$ObjRec = DM::load('project', $id);
				}
			}



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "project '" . $ObjRec['project_name_lang1'] . "' (ID:" . $ObjRec['project_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "project_" . $project_lv1."_".$project_lv2, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "project_" . $project_lv1."_".$project_lv2,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['project_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "project_" . $project_lv1."_".$project_lv2, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."project/project.php?project_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "project_" . $project_lv1."_".$project_lv2, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "project_" . $project_lv1."_".$project_lv2, "PA", $approval_id);

			}

			header("location:project.php?project_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('project', $id);
			$project_lv1 =$ObjRec['project_lv1'];
			$project_lv2 = $ObjRec['project_lv2'];



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['project_status'] == STATUS_DISABLE)

			{

				header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("project", $id);

			$values['project_id'] = $id;
			$values['project_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['project_status'] = STATUS_DISABLE;
			$values['project_updated_by'] = $_SESSION['sess_id'];
			$values['project_updated_date'] = getCurrentTimestamp();


			DM::update('project', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("project_" . $project_lv1."_".$project_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "project '" . $ObjRec['project_name_lang1'] . "' (ID:" . $ObjRec['project_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "project_" . $project_lv1."_".$project_lv2, "DS");

				$files = array($SYSTEM_HOST."project/project.php?project_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("project_" . $project_lv1."_".$project_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "project_" . $project_lv1."_".$project_lv2, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "project_" . $project_lv1."_".$project_lv2, "D");
			}





			header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('project', $id);
			$project_lv1 =$ObjRec['project_lv1'];
			$project_lv2 = $ObjRec['project_lv2'];



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['project_status'] == STATUS_ENABLE || $ObjRec['project_publish_status'] == 'AL')

			{

				header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=F-1006");

				return;

			}



			$values['project_id'] = $id;

			$values['project_publish_status'] = 'D';

			$values['project_status'] = STATUS_ENABLE;

			$values['project_updated_by'] = $_SESSION['sess_id'];

			$values['project_updated_date'] = getCurrentTimestamp();



			DM::update('project', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'project', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of project '" . $ObjRec['project_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'project_' . $project_lv1."_".$project_lv2, "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "project '" . $ObjRec['project_name_lang1'] . "' (ID:" . $ObjRec['project_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'project_' . $project_lv1."_".$project_lv2, "UD");



			header("location: list.php?project_lv1=".$project_lv1."&project_lv2=".$project_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
