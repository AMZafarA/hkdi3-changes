<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="account" )
	{
		$account_id = $_REQUEST['account_id'] ?? '';

		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'account')){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}

		if($processType == "") {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('account', $account_status);

			if($ObjRec['account_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			$values = array();
			$values['account_id'] = $_POST['account_id'];
			$values['account_process_status'] = $_POST['account_process_status'];
			$values['account_place_of_birth_require_additional_info'] = @$_POST['account_place_of_birth_require_additional_info'];
			$values['account_nationality_require_additional_info'] = @$_POST['account_nationality_require_additional_info'];
			$values['account_correspondence_address_require_additional_info'] = @$_POST['account_correspondence_address_require_additional_info'];
			$values['account_is_licensed_employee_ce_no'] = @$_POST['account_is_licensed_employee_ce_no'];

			DM::update('account', $values);

			header("location:account.php?account_id=" . $account_id . "&msg=S-1001");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>