<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once "../../web/account_reg/lang/en/main.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'account')){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$account_id = $_REQUEST['account_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';

	if($account_id != ""){
		if(!isInteger($account_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}

		$obj_row = DM::load('account', $account_id);

		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}

		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}

		$account_passport_no = decode($account_passport_no, $reg_encode_key);


		$sql = " (account_id = ? AND account_tax_residency_status = ?) ";
		$parameters = array($account_id, STATUS_ENABLE);
		$account_tax_residencies = DM::findAll("account_tax_residency", $sql, " account_tax_residency_id ", $parameters);

	} else {
		header("location: list.php?msg=F-1002");
		return;
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script>
			$( document ).ready(function() {
				$("#tabs").tabs({});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});

			function checkForm(){
				document.form6.submit();
			}
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}

			.step-title {
				font-size: 24px;
				margin-bottom: 25px;
			}

			.step-subtitle {
				font-size: 18px;
				margin-top: 25px;
				margin-bottom: 10px;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Account</a></li>
					<li class="active">Detail</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
						<li><a href="#tabs-setting">Setting</a></li>
						<li><a href="#tabs-1">Step 1</a></li>
						<li><a href="#tabs-2">Step 2</a></li>
						<li><a href="#tabs-3">Step 3</a></li>
						<li><a href="#tabs-4">Step 4</a></li>
						<li><a href="#tabs-5">Step 5</a></li>
					</ul>

					<div id="tabs-setting">
                        <form class="form-horizontal" role="form" id="form6" name="form6" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title">Account</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-6 control-label">Status</label>
										<div class="col-xs-6">
											<select class="form-control" id="account_process_status" name="account_process_status">
												<?php
													$account_process_status_opts = array(
														array('value' => 'new', 'label' => 'New'),
														array('value' => 'pending', 'label' => 'Pending'),
														array('value' => 'processing', 'label' => 'Processing'),
														array('value' => 'pending_for_clarification', 'label' => 'Pending for clarification'),
														array('value' => 'resubmitted_for_verification', 'label' => 'Resubmitted for verification'),
														array('value' => 'confirmed', 'label' => 'Confirmed'),
														array('value' => 'completed', 'label' => 'Completed'),
														array('value' => 'rejected', 'label' => 'Rejected')
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_process_status_opts); $i++) { ?>
												<option value="<?php echo $account_process_status_opts[$i]['value']; ?>" <?php echo ($account_process_status_opts[$i]['value'] == $account_process_status ? 'selected' : ''); ?>><?php echo $account_process_status_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_personal_information'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="checkbox-inline"><input type="checkbox" name="account_place_of_birth_require_additional_info" value="1" <?php echo ($account_place_of_birth_require_additional_info == '1' ? 'checked' : ''); ?> /><?php echo lang('account_place_of_birth_additional_info'); ?></label>
									</div>
									<div class="form-group">
										<label class="checkbox-inline"><input type="checkbox" name="account_nationality_require_additional_info" value="1" <?php echo ($account_nationality_require_additional_info == '1' ? 'checked' : ''); ?> /><?php echo lang('account_place_of_birth_additional_info'); ?></label>
									</div>
									<div class="form-group">
										<label class="checkbox-inline"><input type="checkbox" name="account_correspondence_address_require_additional_info" value="1" <?php echo ($account_correspondence_address_require_additional_info == '1' ? 'checked' : ''); ?> /><?php echo lang('account_correspondence_address_additional_info'); ?></label>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_declaration_general_info'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_is_licensed_employee_ce_no'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_is_licensed_employee_ce_no" name="account_is_licensed_employee_ce_no" type="text" value="<?php echo $account_is_licensed_employee_ce_no; ?>" />
										</div>
									</div>
								</div>
							</div>


							<input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>

					<div id="tabs-1">
                        <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title"><?php echo lang('account_personal_information'); ?></div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_title'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_title" name="account_title" type="text" value="<?php echo $account_title; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_surname'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_surname" name="account_surname" type="text" value="<?php echo $account_surname; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_given_name'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_given_name" name="account_given_name" type="text" value="<?php echo $account_given_name; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_middle_name'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_middle_name" name="account_middle_name" type="text" value="<?php echo $account_middle_name; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_date_of_birth'); ?></label>
										<div class="col-xs-2">
											<input class="form-control" id="account_day_of_birth" name="account_day_of_birth" type="text" value="<?php echo $account_day_of_birth; ?>" disabled />
										</div>
										<div class="col-xs-2">
											<input class="form-control" id="account_month_of_birth" name="account_month_of_birth" type="text" value="<?php echo $account_month_of_birth; ?>" disabled />
										</div>
										<div class="col-xs-2">
											<input class="form-control" id="account_year_of_birth" name="account_year_of_birth" type="text" value="<?php echo $account_year_of_birth; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_place_of_birth'); ?></label>
										<div class="col-xs-3">
											<input class="form-control" id="account_place_of_birth_city" name="account_place_of_birth_city" type="text" value="<?php echo $account_place_of_birth_city; ?>" disabled />
										</div>
										<div class="col-xs-3">
											<input class="form-control" id="account_place_of_birth_country" name="account_place_of_birth_country" type="text" value="<?php echo $account_place_of_birth_country; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_place_of_birth_additional_info'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_place_of_birth_additional_info" name="account_place_of_birth_additional_info" type="text" value="<?php echo $account_place_of_birth_additional_info; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_nationality'); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="account_nationality" name="account_nationality" disabled>
												<?php
													$account_nationality_opts = array();

													$sql = "SELECT * FROM countries_list ORDER BY ordering, name;";
													$account_nationality_lists = DM::execute($sql);

													foreach ($account_nationality_lists AS $rows => $row){
														$account_nationality_opts[] = array('value' => $row['value'], 'label' => $row['name']);
													}
												?>
												<?php for ($i = 0; $i < sizeof($account_nationality_opts); $i++) { ?>
												<option value="<?php echo $account_nationality_opts[$i]['value']; ?>" <?php echo ($account_nationality_opts[$i]['value'] == $account_nationality ? 'selected' : ''); ?>><?php echo $account_nationality_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_nationality_additional_info'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_nationality_additional_info" name="account_nationality_additional_info" type="text" value="<?php echo $account_nationality_additional_info; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_passport_no'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_passport_no" name="account_passport_no" type="text" value="<?php echo $account_passport_no; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_passport_type'); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="account_passport_type" name="account_passport_type" disabled>
												<?php
													$account_passport_type_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'hkid', 'label' => lang('account_passport_type_hkid')),
														array('value' => 'cnid', 'label' => lang('account_passport_type_cnid')),
														array('value' => 'macauid', 'label' => lang('account_passport_type_macauid')),
														array('value' => 'passport', 'label' => lang('account_passport_type_passport')),
														array('value' => 'others', 'label' => lang('account_passport_type_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_passport_type_opts); $i++) { ?>
												<option value="<?php echo $account_passport_type_opts[$i]['value']; ?>" <?php echo ($account_passport_type_opts[$i]['value'] == $account_passport_type ? 'selected' : ''); ?>><?php echo $account_passport_type_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<?php if ($account_passport_type == 'others') { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<div class="col-xs-6">
											<input class="form-control" id="account_passport_type_other" name="account_passport_type_other" type="text" value="<?php echo $account_passport_type_other; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_marital_status'); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="account_marital_status" name="account_marital_status" disabled>
												<?php
													$account_marital_status_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'single', 'label' => lang('account_marital_status_single')),
														array('value' => 'married_with_child', 'label' => lang('account_marital_status_married_with_child')),
														array('value' => 'married_without_child', 'label' => lang('account_marital_status_married_without_child')),
														array('value' => 'others', 'label' => lang('account_marital_status_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_marital_status_opts); $i++) { ?>
												<option value="<?php echo $account_marital_status_opts[$i]['value']; ?>" <?php echo ($account_marital_status_opts[$i]['value'] == $account_marital_status ? 'selected' : ''); ?>><?php echo $account_marital_status_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_education_level'); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="account_education_level" name="account_education_level" disabled>
												<?php
													$account_education_level_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'primary', 'label' => lang('account_education_level_primary')),
														array('value' => 'secondary', 'label' => lang('account_education_level_secondary')),
														array('value' => 'post_secondary', 'label' => lang('account_education_level_post_secondary')),
														array('value' => 'university', 'label' => lang('account_education_level_university'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_education_level_opts); $i++) { ?>
												<option value="<?php echo $account_education_level_opts[$i]['value']; ?>" <?php echo ($account_education_level_opts[$i]['value'] == $account_education_level ? 'selected' : ''); ?>><?php echo $account_education_level_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_email'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_email" name="account_email" type="text" value="<?php echo $account_email; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_mobile_no'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_mobile_no" name="account_mobile_no" type="text" value="<?php echo '+'.$account_mobile_no_code.' '.$account_mobile_no; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_home_no'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_home_no" name="account_home_no" type="text" value="<?php echo '+'.$account_home_no_code.' '.$account_home_no; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_residential_address'); ?></div>
							<?php $datas = array('account_residential_address_flat', 'account_residential_address_floor', 'account_residential_address_block', 'account_residential_address_house', 'account_residential_address_estate', 'account_residential_address_street', 'account_residential_address_district', 'account_residential_address_state', 'account_residential_address_country', 'account_residential_address_zip_code'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i+=2) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php if (isset($datas[$i+1])) { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i+1]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i+1]; ?>" name="<?php echo $datas[$i+1]; ?>" type="text" value="<?php echo ${$datas[$i+1]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_correspondence_address_additional_info'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_correspondence_address_additional_info" name="account_correspondence_address_additional_info" type="text" value="<?php echo $account_correspondence_address_additional_info; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_permanent_address'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_permanent_address'); ?></label>
										<div class="col-xs-9">
											<select class="form-control" id="account_permanent_address_type" name="account_permanent_address_type" value="<?php echo $account_permanent_address_type; ?>" disabled>
												<?php
													$account_permanent_address_type_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'same', 'label' => lang('account_permanent_address_type_same')),
														array('value' => 'others', 'label' => lang('account_permanent_address_type_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_permanent_address_type_opts); $i++) { ?>
												<option value="<?php echo $account_permanent_address_type_opts[$i]['value']; ?>" <?php echo ($account_permanent_address_type_opts[$i]['value'] == $account_permanent_address_type ? 'selected' : ''); ?>><?php echo $account_permanent_address_type_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<?php $datas = array('account_permanent_address_flat', 'account_permanent_address_floor', 'account_permanent_address_block', 'account_permanent_address_house', 'account_permanent_address_estate', 'account_permanent_address_street', 'account_permanent_address_district', 'account_permanent_address_state', 'account_permanent_address_country', 'account_permanent_address_zip_code'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i+=2) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php if (isset($datas[$i+1])) { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i+1]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i+1]; ?>" name="<?php echo $datas[$i+1]; ?>" type="text" value="<?php echo ${$datas[$i+1]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>

							<div class="row step-subtitle"><?php echo lang('account_correspondence_address'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_correspondence_address'); ?></label>
										<div class="col-xs-9">
											<select class="form-control" id="account_correspondence_address_type" name="account_correspondence_address_type" value="<?php echo $account_correspondence_address_type; ?>" disabled>
												<?php
													$account_correspondence_address_type_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'same', 'label' => lang('account_correspondence_address_type_same')),
														array('value' => 'same_as_work_address', 'label' => lang('account_correspondence_address_type_same_as_work_address')),
														array('value' => 'others', 'label' => lang('account_correspondence_address_type_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_correspondence_address_type_opts); $i++) { ?>
												<option value="<?php echo $account_correspondence_address_type_opts[$i]['value']; ?>" <?php echo ($account_correspondence_address_type_opts[$i]['value'] == $account_correspondence_address_type ? 'selected' : ''); ?>><?php echo $account_correspondence_address_type_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<?php $datas = array('account_correspondence_address_flat', 'account_correspondence_address_floor', 'account_correspondence_address_block', 'account_correspondence_address_house', 'account_correspondence_address_estate', 'account_correspondence_address_street', 'account_correspondence_address_district', 'account_correspondence_address_state', 'account_correspondence_address_country', 'account_correspondence_address_zip_code'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i+=2) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php if (isset($datas[$i+1])) { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i+1]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i+1]; ?>" name="<?php echo $datas[$i+1]; ?>" type="text" value="<?php echo ${$datas[$i+1]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>

							<div class="row step-subtitle"><?php echo lang('account_work_employer'); ?></div>
							<?php $datas = array('account_work_employer_name', 'account_work_business_nature', 'account_work_position', 'account_work_service_year'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i+=2) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php if (isset($datas[$i+1])) { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i+1]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i+1]; ?>" name="<?php echo $datas[$i+1]; ?>" type="text" value="<?php echo ${$datas[$i+1]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>

							<div class="row step-subtitle"><?php echo lang('account_work_office_address'); ?></div>
							<?php $datas = array('account_work_office_address_flat', 'account_work_office_address_floor', 'account_work_office_address_block', 'account_work_office_address_house', 'account_work_office_address_estate', 'account_work_office_address_street', 'account_work_office_address_district', 'account_work_office_address_country'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i+=2) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php if (isset($datas[$i+1])) { ?>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i+1]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i+1]; ?>" name="<?php echo $datas[$i+1]; ?>" type="text" value="<?php echo ${$datas[$i+1]}; ?>" disabled />
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_work_office_tel_no'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_work_office_tel_no" name="account_work_office_tel_no" type="text" value="<?php echo '+'.$account_work_office_tel_no_code.' '.$account_work_office_tel_no; ?>" disabled />
										</div>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_notification'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_notification_method'); ?></label>
										<div class="col-xs-9">
											<?php
												$account_notification_method_opts = array(
													array('value' => 'email', 'label' => lang('account_notification_method_email')),
													array('value' => 'post', 'label' => lang('account_notification_method_post'))
												);
											?>
											<?php for ($i = 0; $i < sizeof($account_notification_method_opts); $i++) { ?>
											<span>
												<input type="radio" id="account_notification_method_<?php echo $account_notification_method_opts[$i]['value']; ?>" name="account_notification_method" value="<?php echo $account_notification_method_opts[$i]['value']; ?>" <?php echo ($account_notification_method == $account_notification_method_opts[$i]['value'] ? 'checked' : ''); ?> disabled />
												<label for="account_notification_method_<?php echo $account_notification_method_opts[$i]['value']; ?>" class="custom-radio__label"><span class="custom-radio__graphic"></span><span class="custom-radio__txt"><?php echo $account_notification_method_opts[$i]['label']; ?></span></label>
											</span>&nbsp;
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-3 control-label"><?php echo lang('account_notification_lang'); ?></label>
										<div class="col-xs-9">
											<?php
												$account_notification_lang_opts = array(
													array('value' => 'en', 'label' => lang('account_notification_lang_en')),
													array('value' => 'tc', 'label' => lang('account_notification_lang_tc'))
												);
											?>
											<?php for ($i = 0; $i < sizeof($account_notification_lang_opts); $i++) { ?>
											<span>
												<input type="radio" id="account_notification_lang_<?php echo $account_notification_lang_opts[$i]['value']; ?>" name="account_notification_lang" value="<?php echo $account_notification_lang_opts[$i]['value']; ?>" <?php echo ($account_notification_lang == $account_notification_lang_opts[$i]['value'] ? 'checked' : ''); ?> disabled />
												<label for="account_notification_lang_<?php echo $account_notification_lang_opts[$i]['value']; ?>" class="custom-radio__label"><span class="custom-radio__graphic"></span><span class="custom-radio__txt"><?php echo $account_notification_lang_opts[$i]['label']; ?></span></label>
											</span>&nbsp;
											<?php } ?>
										</div>
									</div>
								</div>
							</div>

                            <input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>

					<div id="tabs-2">
                        <form class="form-horizontal" role="form" id="form2" name="form2" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title"><?php echo lang('account_finanical_profile'); ?></div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_annual_income'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_annual_income" name="account_annual_income" disabled>
												<?php
													$account_annual_income_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => '10', 'label' => lang('account_annual_income_range_10')),
														array('value' => '20', 'label' => lang('account_annual_income_range_20')),
														array('value' => '30', 'label' => lang('account_annual_income_range_30')),
														array('value' => '40', 'label' => lang('account_annual_income_range_40')),
														array('value' => '50', 'label' => lang('account_annual_income_range_50')),
														array('value' => '60', 'label' => lang('account_annual_income_range_60')),
														array('value' => '70', 'label' => lang('account_annual_income_range_70'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_annual_income_opts); $i++) { ?>
												<option value="<?php echo $account_annual_income_opts[$i]['value']; ?>" <?php echo ($account_annual_income_opts[$i]['value'] == $account_annual_income ? 'selected' : ''); ?>><?php echo $account_annual_income_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_net_assets'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_annual_income" name="account_annual_income" disabled>
												<?php
													$account_net_assets_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => '10', 'label' => lang('account_net_assets_range_10')),
														array('value' => '20', 'label' => lang('account_net_assets_range_20')),
														array('value' => '30', 'label' => lang('account_net_assets_range_30')),
														array('value' => '40', 'label' => lang('account_net_assets_range_40')),
														array('value' => '50', 'label' => lang('account_net_assets_range_50')),
														array('value' => '60', 'label' => lang('account_net_assets_range_60')),
														array('value' => '70', 'label' => lang('account_net_assets_range_70')),
														array('value' => '80', 'label' => lang('account_net_assets_range_80')),
														array('value' => '90', 'label' => lang('account_net_assets_range_90')),
														array('value' => '100', 'label' => lang('account_net_assets_range_100'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_net_assets_opts); $i++) { ?>
												<option value="<?php echo $account_net_assets_opts[$i]['value']; ?>" <?php echo ($account_net_assets_opts[$i]['value'] == $account_net_assets ? 'selected' : ''); ?>><?php echo $account_net_assets_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_liquid_assets'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_liquid_assets" name="account_liquid_assets" disabled>
												<?php
													$account_liquid_assets_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => '10', 'label' => lang('account_liquid_assets_range_10')),
														array('value' => '20', 'label' => lang('account_liquid_assets_range_20')),
														array('value' => '30', 'label' => lang('account_liquid_assets_range_30')),
														array('value' => '40', 'label' => lang('account_liquid_assets_range_40')),
														array('value' => '50', 'label' => lang('account_liquid_assets_range_50')),
														array('value' => '60', 'label' => lang('account_liquid_assets_range_60')),
														array('value' => '70', 'label' => lang('account_liquid_assets_range_70')),
														array('value' => '80', 'label' => lang('account_liquid_assets_range_80')),
														array('value' => '90', 'label' => lang('account_liquid_assets_range_90'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_liquid_assets_opts); $i++) { ?>
												<option value="<?php echo $account_liquid_assets_opts[$i]['value']; ?>" <?php echo ($account_liquid_assets_opts[$i]['value'] == $account_liquid_assets ? 'selected' : ''); ?>><?php echo $account_liquid_assets_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_initial_source_of_wealth'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_initial_source_of_wealth" name="account_initial_source_of_wealth" disabled>
												<?php
													$account_initial_source_of_wealth_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'salary', 'label' => lang('account_initial_source_of_wealth_salary')),
														array('value' => 'savings', 'label' => lang('account_initial_source_of_wealth_savings')),
														array('value' => 'business', 'label' => lang('account_initial_source_of_wealth_business')),
														array('value' => 'investment', 'label' => lang('account_initial_source_of_wealth_investment')),
														array('value' => 'retirement_funds', 'label' => lang('account_initial_source_of_wealth_retirement_funds')),
														array('value' => 'inheritance', 'label' => lang('account_initial_source_of_wealth_inheritance')),
														array('value' => 'others', 'label' => lang('account_initial_source_of_wealth_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_initial_source_of_wealth_opts); $i++) { ?>
												<option value="<?php echo $account_initial_source_of_wealth_opts[$i]['value']; ?>" <?php echo ($account_initial_source_of_wealth_opts[$i]['value'] == $account_initial_source_of_wealth ? 'selected' : ''); ?>><?php echo $account_initial_source_of_wealth_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
										<?php if ($account_initial_source_of_wealth == 'others') { ?>
										<div class="col-xs-4">
											<input class="form-control" id="account_initial_source_of_wealth_other" name="account_initial_source_of_wealth_other" type="text" value="<?php echo $account_initial_source_of_wealth_other; ?>" disabled />
										</div>
										<?php } ?>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_ongoing_source_of_wealth'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_ongoing_source_of_wealth" name="account_ongoing_source_of_wealth" disabled>
												<?php
													$account_ongoing_source_of_wealth_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'same', 'label' => lang('account_ongoing_source_of_wealth_same')),
														array('value' => 'others', 'label' => lang('account_ongoing_source_of_wealth_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_ongoing_source_of_wealth_opts); $i++) { ?>
												<option value="<?php echo $account_ongoing_source_of_wealth_opts[$i]['value']; ?>" <?php echo ($account_ongoing_source_of_wealth_opts[$i]['value'] == $account_ongoing_source_of_wealth ? 'selected' : ''); ?>><?php echo $account_ongoing_source_of_wealth_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
										<?php if ($account_ongoing_source_of_wealth == 'others') { ?>
										<div class="col-xs-4">
											<input class="form-control" id="account_ongoing_source_of_wealth_other" name="account_ongoing_source_of_wealth_other" type="text" value="<?php echo $account_ongoing_source_of_wealth_other; ?>" disabled />
										</div>
										<?php } ?>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_source_of_wealth'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_means_of_fund_transfer'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_means_of_fund_transfer" name="account_means_of_fund_transfer" disabled>
												<?php
													$account_means_of_fund_transfer_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'cheque', 'label' => lang('account_means_of_fund_transfer_cheque')),
														array('value' => 'local_bank', 'label' => lang('account_means_of_fund_transfer_local_bank')),
														array('value' => 'overseas', 'label' => lang('account_means_of_fund_transfer_overseas'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_means_of_fund_transfer_opts); $i++) { ?>
												<option value="<?php echo $account_means_of_fund_transfer_opts[$i]['value']; ?>" <?php echo ($account_means_of_fund_transfer_opts[$i]['value'] == $account_means_of_fund_transfer ? 'selected' : ''); ?>><?php echo $account_means_of_fund_transfer_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-4 control-label"><?php echo lang('account_countries_of_origins_of_funds'); ?></label>
										<div class="col-xs-4">
											<select class="form-control" id="account_countries_of_origins_of_funds" name="account_countries_of_origins_of_funds" disabled>
												<?php
													$account_countries_of_origins_of_funds_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'hk', 'label' => lang('account_countries_of_origins_of_funds_hk')),
														array('value' => 'cn', 'label' => lang('account_countries_of_origins_of_funds_cn')),
														array('value' => 'macau', 'label' => lang('account_countries_of_origins_of_funds_macau')),
														array('value' => 'singapore', 'label' => lang('account_countries_of_origins_of_funds_singapore')),
														array('value' => 'us', 'label' => lang('account_countries_of_origins_of_funds_us')),
														array('value' => 'canada', 'label' => lang('account_countries_of_origins_of_funds_canada')),
														array('value' => 'others', 'label' => lang('account_countries_of_origins_of_funds_others'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_countries_of_origins_of_funds_opts); $i++) { ?>
												<option value="<?php echo $account_countries_of_origins_of_funds_opts[$i]['value']; ?>" <?php echo ($account_countries_of_origins_of_funds_opts[$i]['value'] == $account_countries_of_origins_of_funds ? 'selected' : ''); ?>><?php echo $account_countries_of_origins_of_funds_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
										<?php if ($account_countries_of_origins_of_funds == 'others') { ?>
										<div class="col-xs-4">
											<input class="form-control" id="account_countries_of_origins_of_funds_other" name="account_countries_of_origins_of_funds_other" type="text" value="<?php echo $account_countries_of_origins_of_funds_other; ?>" disabled />
										</div>
										<?php } ?>
									</div>
								</div>
							</div>

                            <input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>

					<div id="tabs-3">
                        <form class="form-horizontal" role="form" id="form3" name="form3" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title"><?php echo lang('account_customer_profilling_questionnaire'); ?></div>

							<div class="row step-subtitle"><?php echo lang('account_knowledge_of_derivatives_products'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<?php
										$questionnaire_opts = array(
											array('value' => 0, 'label' => lang('common_nil')),
											array('value' => 10, 'label' => lang('account_customer_profilling_questionnaire_opt_10')),
											array('value' => 20, 'label' => lang('account_customer_profilling_questionnaire_opt_20')),
											array('value' => 30, 'label' => lang('account_customer_profilling_questionnaire_opt_30')),
											array('value' => 40, 'label' => lang('account_customer_profilling_questionnaire_opt_40'))
										);
									?>
									<?php $datas = array('account_investment_experience_equities', 'account_investment_experience_mutual_funds', 'account_investment_experience_fixed_income_products', 'account_investment_experience_assurance_product'); ?>
									<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" disabled>
												<?php for ($q = 0; $q < sizeof($questionnaire_opts); $q++) { ?>
												<option value="<?php echo $questionnaire_opts[$q]['value']; ?>" <?php echo ($questionnaire_opts[$q]['value'] == ${$datas[$i]} ? 'selected' : ''); ?>><?php echo $questionnaire_opts[$q]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_investment_experience_with_non_complex_investment_products'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<?php $datas = array('account_investment_experience_warrants', 'account_investment_experience_stock_options', 'account_investment_experience_leveraged_forex', 'account_investment_experience_futures_and_options', 'account_investment_experience_hedge_funds', 'account_investment_experience_structured_notes', 'account_investment_experience_otc_swap', 'account_investment_experience_other_derivatives_products'); ?>
									<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<select class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" disabled>
												<?php for ($q = 0; $q < sizeof($questionnaire_opts); $q++) { ?>
												<option value="<?php echo $questionnaire_opts[$q]['value']; ?>" <?php echo ($questionnaire_opts[$q]['value'] == ${$datas[$i]} ? 'selected' : ''); ?>><?php echo $questionnaire_opts[$q]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_investment_experience_complex_products'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div><?php echo lang('account_has_knowledge_of_derivatives_products'); ?></div>
									<div class="">
										<label class="radio-inline"><input type="radio" name="account_has_knowledge_of_derivatives_products_yes" <?php echo ($account_has_knowledge_of_derivatives_products == 'yes' ? 'checked' : ''); ?> disabled /><?php echo lang('common_yes'); ?></label>
										<label class="radio-inline"><input type="radio" name="account_has_knowledge_of_derivatives_products_no" <?php echo ($account_has_knowledge_of_derivatives_products == 'no' ? 'checked' : ''); ?> disabled /><?php echo lang('common_no'); ?></label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div><?php echo lang('account_knowledge_of_derivatives_products_ways'); ?></div>
									<div class="checkbox">
										<label><input type="checkbox" name="account_knowledge_of_derivatives_products_ways_transactions" value="1" <?php echo ($account_knowledge_of_derivatives_products_ways_transactions == '1' ? 'checked' : ''); ?> disabled /><?php echo lang('account_knowledge_of_derivatives_products_ways_transactions'); ?></label>
									</div>

									<div class="checkbox">
										<label><input type="checkbox" name="account_knowledge_of_derivatives_products_ways_courses" value="1" <?php echo ($account_knowledge_of_derivatives_products_ways_courses == '1' ? 'checked' : ''); ?> disabled /><?php echo lang('account_knowledge_of_derivatives_products_ways_courses'); ?></label>
									</div>
									<?php if ($account_knowledge_of_derivatives_products_ways_courses == '1') { ?>
									<div class="form-group">
										<label class="col-xs-6 control-label">Specify</label>
										<div class="col-xs-6">
											<input class="form-control" id="account_knowledge_of_derivatives_products_ways_courses_desc" name="account_knowledge_of_derivatives_products_ways_courses_desc" type="text" value="<?php echo $account_knowledge_of_derivatives_products_ways_courses_desc; ?>" disabled />
										</div>
									</div>
									<?php } ?>

									<div class="checkbox">
										<label><input type="checkbox" name="account_knowledge_of_derivatives_products_ways_work" value="1" <?php echo ($account_knowledge_of_derivatives_products_ways_work == '1' ? 'checked' : ''); ?> disabled /><?php echo lang('account_knowledge_of_derivatives_products_ways_work'); ?></label>
									</div>
									<?php if ($account_knowledge_of_derivatives_products_ways_work == '1') { ?>
									<div class="form-group">
										<label class="col-xs-6 control-label">Specify</label>
										<div class="col-xs-6">
											<input class="form-control" id="account_knowledge_of_derivatives_products_ways_work_desc" name="account_knowledge_of_derivatives_products_ways_work_desc" type="text" value="<?php echo $account_knowledge_of_derivatives_products_ways_work_desc; ?>" disabled />
										</div>
									</div>
									<?php } ?>

									<div class="checkbox">
										<label><input type="checkbox" name="account_knowledge_of_derivatives_products_ways_carry_activities" value="1" <?php echo ($account_knowledge_of_derivatives_products_ways_carry_activities == '1' ? 'checked' : ''); ?> disabled /><?php echo lang('account_knowledge_of_derivatives_products_ways_carry_activities'); ?></label>
									</div>
								</div>
							</div>

							<div class="row step-subtitle"><?php echo lang('account_risk_tolerance_level'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_age_group'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_age_group" name="account_age_group" disabled>
												<?php
													$account_age_group_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 10, 'label' => lang('account_age_group_10')),
														array('value' => 20, 'label' => lang('account_age_group_20')),
														array('value' => 30, 'label' => lang('account_age_group_30')),
														array('value' => 40, 'label' => lang('account_age_group_40')),
														array('value' => 50, 'label' => lang('account_age_group_50'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_age_group_opts); $i++) { ?>
												<option value="<?php echo $account_age_group_opts[$i]['value']; ?>" <?php echo ($account_age_group_opts[$i]['value'] == $account_age_group ? 'selected' : ''); ?>><?php echo $account_age_group_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_education_level_group'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_education_level_group" name="account_education_level_group" disabled>
												<?php
													$account_education_level_group_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'primary', 'label' => lang('account_education_level_group_primary')),
														array('value' => 'secondary', 'label' => lang('account_education_level_group_secondary')),
														array('value' => 'post_secondary', 'label' => lang('account_education_level_group_post_secondary')),
														array('value' => 'university', 'label' => lang('account_education_level_group_university'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_education_level_group_opts); $i++) { ?>
												<option value="<?php echo $account_education_level_group_opts[$i]['value']; ?>" <?php echo ($account_education_level_group_opts[$i]['value'] == $account_education_level_group ? 'selected' : ''); ?>><?php echo $account_education_level_group_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_primary_investment_objective'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_primary_investment_objective" name="account_primary_investment_objective" disabled>
												<?php
													$account_primary_investment_objective_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 'preservation', 'label' => lang('account_primary_investment_objective_preservation')),
														array('value' => 'stable', 'label' => lang('account_primary_investment_objective_stable')),
														array('value' => 'moderate_appreciation', 'label' => lang('account_primary_investment_objective_moderate_appreciation')),
														array('value' => 'moderate_high_appreciation', 'label' => lang('account_primary_investment_objective_moderate_high_appreciation')),
														array('value' => 'max_appreciation', 'label' => lang('account_primary_investment_objective_max_appreciation'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_primary_investment_objective_opts); $i++) { ?>
												<option value="<?php echo $account_primary_investment_objective_opts[$i]['value']; ?>" <?php echo ($account_primary_investment_objective_opts[$i]['value'] == $account_primary_investment_objective ? 'selected' : ''); ?>><?php echo $account_primary_investment_objective_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_investment_percentage'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_investment_percentage" name="account_investment_percentage" disabled>
												<?php
													$account_investment_percentage_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 10, 'label' => lang('account_investment_percentage_10')),
														array('value' => 20, 'label' => lang('account_investment_percentage_20')),
														array('value' => 30, 'label' => lang('account_investment_percentage_30')),
														array('value' => 40, 'label' => lang('account_investment_percentage_40')),
														array('value' => 50, 'label' => lang('account_investment_percentage_50'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_investment_percentage_opts); $i++) { ?>
												<option value="<?php echo $account_investment_percentage_opts[$i]['value']; ?>" <?php echo ($account_investment_percentage_opts[$i]['value'] == $account_investment_percentage ? 'selected' : ''); ?>><?php echo $account_investment_percentage_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_investment_horizon'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_investment_horizon" name="account_investment_horizon" disabled>
												<?php
													$account_investment_horizon_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 10, 'label' => lang('account_investment_horizon_10')),
														array('value' => 20, 'label' => lang('account_investment_horizon_20')),
														array('value' => 30, 'label' => lang('account_investment_horizon_30')),
														array('value' => 40, 'label' => lang('account_investment_horizon_40')),
														array('value' => 50, 'label' => lang('account_investment_horizon_50'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_investment_horizon_opts); $i++) { ?>
												<option value="<?php echo $account_investment_horizon_opts[$i]['value']; ?>" <?php echo ($account_investment_horizon_opts[$i]['value'] == $account_investment_horizon ? 'selected' : ''); ?>><?php echo $account_investment_horizon_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_investment_attitude'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_investment_attitude" name="account_investment_attitude" disabled>
												<?php
													$account_investment_attitude_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 10, 'label' => lang('account_investment_attitude_10')),
														array('value' => 20, 'label' => lang('account_investment_attitude_20')),
														array('value' => 30, 'label' => lang('account_investment_attitude_30')),
														array('value' => 40, 'label' => lang('account_investment_attitude_40')),
														array('value' => 50, 'label' => lang('account_investment_attitude_50'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_investment_attitude_opts); $i++) { ?>
												<option value="<?php echo $account_investment_attitude_opts[$i]['value']; ?>" <?php echo ($account_investment_attitude_opts[$i]['value'] == $account_investment_attitude ? 'selected' : ''); ?>><?php echo $account_investment_attitude_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-xs-12 control-label"><?php echo lang('account_investment_fell_react'); ?></label>
										<div class="col-xs-12">
											<select class="form-control" id="account_investment_fell_react" name="account_investment_fell_react" disabled>
												<?php
													$account_investment_fell_react_opts = array(
														array('value' => '', 'label' => '&nbsp;'),
														array('value' => 10, 'label' => lang('account_investment_fell_react_10')),
														array('value' => 20, 'label' => lang('account_investment_fell_react_20')),
														array('value' => 30, 'label' => lang('account_investment_fell_react_30')),
														array('value' => 40, 'label' => lang('account_investment_fell_react_40')),
														array('value' => 50, 'label' => lang('account_investment_fell_react_50'))
													);
												?>
												<?php for ($i = 0; $i < sizeof($account_investment_fell_react_opts); $i++) { ?>
												<option value="<?php echo $account_investment_fell_react_opts[$i]['value']; ?>" <?php echo ($account_investment_fell_react_opts[$i]['value'] == $account_investment_fell_react ? 'selected' : ''); ?>><?php echo $account_investment_fell_react_opts[$i]['label']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>

					<div id="tabs-4">
                        <form class="form-horizontal" role="form" id="form4" name="form4" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title"><?php echo lang('account_declaration_general_info'); ?></div>
							<div class="row step-subtitle"><?php echo lang('account_identity_declaration'); ?></div>
							<?php $datas = array('account_is_market_director', 'account_is_licensed_employee', 'account_is_relative_of_the_investment_consultant', 'account_is_us_citizen', 'account_is_born_in_us', 'account_is_entrusted_with_prominent_public_functions'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
							<div class="row">
								<div class="col-xs-12">
									<div><?php echo lang($datas[$i]); ?></div>
									<div class="">
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_yes" <?php echo (${$datas[$i]} == 'yes' ? 'checked' : ''); ?> disabled /><?php echo lang('common_yes'); ?></label>
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_no" <?php echo (${$datas[$i]} == 'no' ? 'checked' : ''); ?> disabled /><?php echo lang('common_no'); ?></label>
									</div>
								</div>
							</div>
							<?php if ($datas[$i] == 'account_is_market_director' || $datas[$i] == 'account_is_entrusted_with_prominent_public_functions') { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label">Description</label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>_desc" name="<?php echo $datas[$i]; ?>_desc" type="text" value="<?php echo ${$datas[$i].'_desc'}; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<br />
							<?php } ?>


							<div class="row step-subtitle"><?php echo lang('account_for_margin_account'); ?></div>
							<?php $datas = array('account_is_spouse_margin', 'account_is_control_voting_rights', 'account_is_associated_shk_group'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
							<div class="row">
								<div class="col-xs-12">
									<div><?php echo lang($datas[$i]); ?></div>
									<div class="">
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_yes" <?php echo (${$datas[$i]} == 'yes' ? 'checked' : ''); ?> disabled /><?php echo lang('common_yes'); ?></label>
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_no" <?php echo (${$datas[$i]} == 'no' ? 'checked' : ''); ?> disabled /><?php echo lang('common_no'); ?></label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label">Description</label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>_desc" name="<?php echo $datas[$i]; ?>_desc" type="text" value="<?php echo ${$datas[$i].'_desc'}; ?>" disabled />
										</div>
									</div>
								</div>
							</div><br />
							<?php } ?>


							<div class="row step-subtitle"><?php echo lang('account_general_information'); ?></div>
							<?php $datas = array('account_is_beneficiary', 'account_is_originating_instructions', 'account_is_only_person_with_authority'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
							<div class="row">
								<div class="col-xs-12">
									<div><?php echo lang($datas[$i]); ?></div>
									<div class="checkbox">
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_yes" <?php echo (${$datas[$i]} == 'yes' ? 'checked' : ''); ?> disabled /><?php echo lang('common_yes'); ?></label>
										<label class="radio-inline"><input type="radio" name="<?php echo $datas[$i]; ?>_no" <?php echo (${$datas[$i]} == 'no' ? 'checked' : ''); ?> disabled /><?php echo lang('common_no'); ?></label>
									</div>
								</div>
							</div>
							<?php } ?>

							<?php $datas = array('account_bank_account_for_shk_bank', 'account_bank_account_for_shk_bank_account_no', 'account_bank_account_for_shk_bank_account_holder'); ?>
							<?php for ($i = 0; $i < sizeof($datas); $i++) { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang($datas[$i]); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="<?php echo $datas[$i]; ?>" name="<?php echo $datas[$i]; ?>" type="text" value="<?php echo ${$datas[$i]}; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>

							<div class="row step-subtitle"><?php echo lang('account_personal_data_for_direct_marketing'); ?></div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="checkbox-inline"><input type="checkbox" value="1" <?php echo ($account_object_to_use_personal_data_for_direct_marketing == '1' ? 'checked' : ''); ?> disabled /><?php echo lang('account_object_to_use_personal_data_for_direct_marketing'); ?></label>
									</div>
								</div>
							</div>

							<input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>

					<div id="tabs-5">
                        <form class="form-horizontal" role="form" id="form5" name="form5" action="act.php" method="post" enctype="multipart/form-data">
							<div class="row step-title"><?php echo lang('account_individual_tax_residency'); ?></div>
							<div class="row step-subtitle"><?php echo lang('account_crs_declaration_of_tax_residency'); ?></div>
							<?php for ($i = 0; $i < sizeof($account_tax_residencies); $i++) { ?>
							<div class="row">
								<div class="col-xs-8">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_tax_residency_country'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="" name="account_tax_residency_country[]" type="text" value="<?php echo $account_tax_residencies[$i]['account_tax_residency_country']; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_tax_residency_tin_reason'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="" name="account_tax_residency_tin_reason[]" type="text" value="<?php echo $account_tax_residencies[$i]['account_tax_residency_tin_reason']; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>


							<div class="row step-subtitle"><?php echo lang('account_type_of_controlling_person'); ?></div>
							<div class="row step-subtitle"><?php echo lang('account_legal_person'); ?></div>
							<?php
								$legal_person_opts = array('controlling_ownership', 'exercises_control', 'senior_managing');
							?>
							<?php for ($i = 0; $i < sizeof($legal_person_opts); $i++) { ?>
							<div class="row">
								<div>
									<input id="account_legal_person_<?php echo $legal_person_opts[$i]; ?>" name="account_legal_person_<?php echo $legal_person_opts[$i]; ?>" value="1" type="checkbox" <?php echo (${'account_legal_person_'.$legal_person_opts[$i]} == '1' ? 'checked' : ''); ?> disabled />
									<label for="account_legal_person_<?php echo $legal_person_opts[$i]; ?>"><?php echo lang('account_legal_person_'.$legal_person_opts[$i]); ?></label>
								</div>
							</div>

							<?php if (${'account_legal_person_'.$legal_person_opts[$i]} == '1') { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_legal_person_'.$legal_person_opts[$i].'_entity_name'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_legal_person_<?php echo $legal_person_opts[$i]; ?>_entity_name" name="account_legal_person_<?php echo $legal_person_opts[$i]; ?>_entity_name" type="text" value="<?php echo ${'account_legal_person_'.$legal_person_opts[$i].'_entity_name'}; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_legal_person_'.$legal_person_opts[$i].'_company_number'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_legal_person_<?php echo $legal_person_opts[$i]; ?>_company_number" name="account_legal_person_<?php echo $legal_person_opts[$i]; ?>_company_number" type="text" value="<?php echo ${'account_legal_person_'.$legal_person_opts[$i].'_company_number'}; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<?php } ?>

							<div class="row step-subtitle"><?php echo lang('account_trust'); ?></div>
							<?php
								$trust_opts = array('settlor', 'trustee', 'protector', 'beneficiary', 'other');
							?>
							<?php for ($i = 0; $i < sizeof($trust_opts); $i++) { ?>
							<div class="row">
								<div>
									<input id="account_trust_<?php echo $trust_opts[$i]; ?>" name="account_trust_<?php echo $trust_opts[$i]; ?>" value="1" type="checkbox" <?php echo (${'account_trust_'.$trust_opts[$i]} == '1' ? 'checked' : ''); ?> disabled />
									<label for="account_trust_<?php echo $trust_opts[$i]; ?>"><?php echo lang('account_trust_'.$trust_opts[$i]); ?></label>
								</div>
							</div>

							<?php if (${'account_trust_'.$trust_opts[$i]} == '1') { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_trust_'.$trust_opts[$i].'_entity_name'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_trust_<?php echo $trust_opts[$i]; ?>_entity_name" name="account_trust_<?php echo $trust_opts[$i]; ?>_entity_name" type="text" value="<?php echo ${'account_trust_'.$trust_opts[$i].'_entity_name'}; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_trust_'.$trust_opts[$i].'_company_number'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_trust_<?php echo $trust_opts[$i]; ?>_company_number" name="account_trust_<?php echo $trust_opts[$i]; ?>_company_number" type="text" value="<?php echo ${'account_trust_'.$trust_opts[$i].'_company_number'}; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<?php } ?>


							<div class="row step-subtitle"><?php echo lang('account_legal_other_trust'); ?></div>
							<?php
								$legal_arrangement_other_trust_opts = array('similar_settlor', 'similar_trustee', 'similar_protector', 'similar_beneficiary', 'similar_other');
							?>
							<?php for ($i = 0; $i < sizeof($legal_arrangement_other_trust_opts); $i++) { ?>
							<div class="row">
								<div>
									<input id="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>" name="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>" value="1" type="checkbox" <?php echo (${'account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i]} == '1' ? 'checked' : ''); ?> disabled />
									<label for="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>"><?php echo lang('account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i]); ?></label>
								</div>
							</div>

							<?php if (${'account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i]} == '1') { ?>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i].'_entity_name'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>_entity_name" name="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>_entity_name" type="text" value="<?php echo ${'account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i].'_entity_name'}; ?>" disabled />
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label class="col-xs-6 control-label"><?php echo lang('account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i].'_company_number'); ?></label>
										<div class="col-xs-6">
											<input class="form-control" id="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>_company_number" name="account_legal_other_trust_<?php echo $legal_arrangement_other_trust_opts[$i]; ?>_company_number" type="text" value="<?php echo ${'account_legal_other_trust_'.$legal_arrangement_other_trust_opts[$i].'_company_number'}; ?>" disabled />
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<?php } ?>

							<input type="hidden" name="type" id="type" value="account" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_id ?>" />
                            <input type="hidden" name="account_status" id="account_status" value="1" />
						</form>
					</div>
				</div>
			</div>
			<div class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>