<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'account')) {
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}
	
	$page_no = $_REQUEST['page_no'] ?? '';
	$page_size = 20;
	
	if($page_no == "" || !isInteger($page_no)){
		$page_no = 1;
	}

	$sql = "";//" account_status = ? ";
	$parameters = array();//array(STATUS_ENABLE);

	$total_record = DM::count("account", $sql, $parameters);
	
	$total_page = ceil($total_record / $page_size);
	
	if($total_page == 0)
		$total_page = 1;
	
	if($page_no > $total_page){
		$page_no = $total_page;
	}
	
	$result = DM::findAllWithPaging("account", $page_no, $page_size, $sql, " account_id DESC ", $parameters);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script>
			function deleteRecord(itemId){
				if(confirm("Are you sure you want to delete this record ?")){
					$("#processType").val('delete');
					$("#product_id").val(itemId);
					$('#approvalModalDialog').modal('show');
				}
				
				$('#approvalModalDialog').modal('show');
			}
			
			function undeleteRecord(itemId){
				if(confirm("Are you sure you want to undelete this record ?")){
					$("#processType").val('undelete');
					$("#product_id").val(itemId);
					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form3;
						frm.submit();
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form3;
						frm.submit();
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}
			
			function copyToClipboard(url) {
				  var $temp = $("<input>");
				  $("body").append($temp);
				  $temp.val(url).select();
				  document.execCommand("copy");
				  $temp.remove();
				}

			$( document ).ready(function() {
				
				$("#page_first").bind('click', function(){
					$("#page_no").val(1);
					$("#form1").submit();
				});
				
				$("#page_last").bind('click', function(){
					$("#page_no").val(<?php echo $total_page ?>);
					$("#form1").submit();
				});
				
				$("#page_prev").bind('click', function(){
					$("#page_no").val(<?php echo $page_no == 1 ? 1 : $page_no - 1 ?>);
					$("#form1").submit();
				});
				
				$("#page_next").bind('click', function(){
					$("#page_no").val(<?php echo $page_no < $total_page ? $page_no + 1 : $total_page ?>);
					$("#form1").submit();
				});

				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});

				/*$("#page_first").bind('click', function(){
					searchUtil.toPage(1);
				});
				
				$("#page_last").bind('click', function(){
					searchUtil.toPage(<?php echo $total_page ?>);
				});
				
				$("#page_prev").bind('click', function(){
					searchUtil.toPage(<?php echo $page_no == 1 ? 1 : $page_no - 1 ?>);
				});
				
				$("#page_next").bind('click', function(){
					searchUtil.toPage(<?php echo $page_no < $total_page ? $page_no + 1 : $total_page ?>);
				});*/

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-5">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Account</li>
				</ol>
				<span class="breadcrumb-right col-xs-7">
					
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
				<div class="row">
					<label class="col-xs-12" style="text-align:right">
						Page <?php echo $page_no ?> of <?php echo $total_page ?>
					</label>
				</div>
				<div class="row">
					<table id="menu_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="col-xs-1">#</th>
								<th class="col-xs-1">Mobile No.</th>
								<th class="col-xs-1">Name</th>
								<th class="col-xs-1">Create At</th>
								<th class="col-xs-3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$row_count = 1;
								foreach($result as $row)
								{
							?>
									<tr>
										<td class="col-xs-1">
											<?php echo ($page_no - 1) * $page_size + $row_count ?>
										</td>
										<td class="col-xs-1"><?php echo $row['account_mobile_no_code']; ?> <?php echo $row['account_mobile_no']; ?></td>
										<td class="col-xs-1"><?php echo $row['account_given_name']; ?> <?php echo $row['account_surname']; ?></td>
										<td class="col-xs-1"><?php echo $row['account_created_date']; ?></td>
										<td class="col-xs-3" style="text-align:center">
											<a role="button" class="btn btn-primary btn-sm" href="account.php?account_id=<?php echo $row['account_id'] ?>">
												<i class="fa fa-folder-open"></i> View
											</a>
										</td>
									</tr>
							<?php
								$row_count++;
								}
								unset($resultObj);
							?>
						</tbody>
					</table>
				</div>
				<div class="row" style="float:right">
					<ul class="pager">
						<li>
							<button id="page_first" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-backward"></span> First
							</button>
						</li>
						<li>
							<button id="page_prev" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-left"></span> Previous
							</button>
						</li>
						<li>
							<button id="page_next" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-right"></span> Next
							</button>
						</li>
						<li>
							<button id="page_last" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-forward"></span> Last
							</button>
						</li>
					</ul>
					<input type="hidden" id="page_no" name="page_no" value=""/>
				</div>
			</form>
			<form class="form-horizontal" role="form" id="form3" name="form3" action="act.php" method="post">
				<input type="hidden" id="type" name="type" value="product"/>
				<input type="hidden" id="processType" name="processType" value=""/>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>
