<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$user_id = $_REQUEST['user_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($user_id != ""){
		if(!isInteger($user_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		$obj_row = DM::load('user', $user_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
	} else {
		foreach($_REQUEST as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
		$user_email_notification = "Y";
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script>
			function submitForm(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#user_display_name").val() == ""){
					flag = false;
					errMessage += "User Name\n";
				}
				
				/*if($("input[name='user_email_notification']:checked").val() == "Y"){
					if($("#user_email").val() == ""){
						flag = false;
						errMessage += "Email Address\n";
					}
				}*/
				
				<?php if($user_id != "") { ?>
				if($("#user_password").val() != "" || $("#confirm_password").val() != ""){
				<?php } ?>
					if($("#user_password").val() == ""){
						flag = false;
						errMessage += "Password\n";
					}
					
					if($("#confirm_password").val() == ""){
						flag = false;
						errMessage += "Confirm Password\n";
					}
				<?php if($user_id != "") { ?>
				}
				<?php } ?>
				
				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				/*if(flag && $("input[name='user_email_notification']:checked").val() == "Y" && !isValidEmail($("#user_email").val())){
					flag = false;
					alert("Email address is not valid");
					return;
				}*/
				
				if(flag &&  $("#user_password").val() != "" && ($("#user_password").val().length < 8 || $("#user_password").val().length > 16)){
					flag = false;
					alert("Password should be contain 8 to 16 characters");
					$("#user_password").focus();
					return;
				}
				
				var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}/;
				
				if(flag && !re.test($("#user_password").val())){
					flag = false;
					alert("Password should contain at least one upper case character, one lower case character and one digit");
					$("#user_password").focus();
					return;
				}

				if(flag && $("#user_password").val() != $("#confirm_password").val()){
					flag = false;
					alert("Password does not match with confirm password");
					return;
				}
				
				if(flag &&  $("#confirm_password").val() != "" && ($("#confirm_password").val().length < 8 || $("#confirm_password").val().length > 16)){
					flag = false;
					alert("Confirm new password should contain 8 to 16 characters");
					$("#confirm_password").focus();
					return;
				}
				
				if(flag && !re.test($("#confirm_password").val())){
					flag = false;
					alert("Confirm new password should contain at least one upper case character, one lower case character and one digit");
					$("#confirm_password").focus();
					return;
				}
				
				document.form1.submit();
			}
			
			function resetForm(){				
				location.href = "user.php<?php echo $user_id == "" ? "" : "?user_id=" . $user_id ?>";
			}
			
			/*function notificationUpdate(){
				if($("input[name='user_email_notification']:checked").val() == "Y"){
					$("#user_email").prop('readonly', false);
				} else {
					$("#user_email").prop('readonly', true);
					$("#user_email").val("");
				}
			}*/
			
			$( document ).ready(function() {
				//$("input[name='user_email_notification']").on('click', notificationUpdate);
				//notificationUpdate();
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Users</a></li>
					<li class="active"><?php echo $user_id == "" ? "Create" : "Update" ?></li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post">
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">User Login</label>
					<div class="col-xs-6">
						<input type="text" class="form-control" placeholder="User Login" name="user_login" id="user_login" value="<?php echo htmlspecialchars($user_login ?? '') ?>" <?php echo $user_id == "" ? "" : "readonly='readonly'" ?>/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">User Name</label>
					<div class="col-xs-6">
						<input type="text" class="form-control" placeholder="User Name" name="user_display_name" id="user_display_name" value="<?php echo htmlspecialchars($user_display_name ?? '') ?>"/>
					</div>
				</div>
				<?php /* user request disable this feature 
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Email Notification</label>
					<div class="col-xs-1">
						<div class="radio">
							<label>
								<input type="radio" name="user_email_notification" value="Y" <?php echo $user_email_notification == "Y" ? "checked" : "" ?>/>
								Receive
							</label>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="radio">
							<label>
								<input type="radio" name="user_email_notification" value="N" <?php echo $user_email_notification == "N" ? "checked" : "" ?>/>
								Do Not Receive
							</label>
						</div>
					</div>
				</div>
				*/ ?>


				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Email Address</label>
					<div class="col-xs-6">
						<input type="text" class="form-control" placeholder="Email" name="user_email" id="user_email" value="<?php echo htmlspecialchars($user_email ?? '') ?>"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Password</label>
					<div class="col-xs-6">
						<input type="password" class="form-control" placeholder="Password" name="user_password" id="user_password"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2 col-xs-offset-2 control-label">Confirm Password</label>
					<div class="col-xs-6">
						<input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" id="confirm_password"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12" style="text-align:right">
						<button type="button" class="btn btn-primary" onclick="submitForm(); return false;">
							<i class="fa fa-floppy-o fa-lg"></i> Save
						</button>
						<button type="button" class="btn btn-danger" onclick="resetForm(); return false;">
							<i class="fa fa-undo fa-lg"></i> Reset
						</button>
						<input type="hidden" name="user_email_notification" value="Y" />
						<input type="hidden" name="type" value="user" />
						<input type="hidden" name="processType" id="processType" value="">
						<input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
						<input type="hidden" name="user_status" value="1" />
					</div>
				</div>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>