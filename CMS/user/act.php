<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="user" )
	{
		$id = $_REQUEST['user_id'] ?? '';
		
		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
		
		if($processType == "") {
			$values = $_REQUEST;
			
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();
			
			if($isNew)
			{
				if(DM::count('user', " user_login = ? AND user_status = ? ", array($values['user_login'], STATUS_ENABLE)) > 0){
					header("location:list.php?msg=F-1003");
					return;
				}
				
				$new_salt = strRand(20);

				$values['user_type'] = 'O';
				$values['user_password_hash'] = hash('sha1', $new_salt . $values['user_password']);
				$values['user_password_salt'] = $new_salt;

				$values['user_status'] = STATUS_ENABLE;
				$values['user_created_by'] = $_SESSION['sess_id'];
				$values['user_created_date'] = $timestamp;
				$values['user_updated_by'] = $_SESSION['sess_id'];
				$values['user_updated_date'] = $timestamp;

				$id = DM::insert('user', $values);
			} else {
				unset($values['user_login']);
				
				$ObjRec = DM::load('user', $id);
				
				if($ObjRec['user_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}
				
				if($values['user_password'] == ""){
					unset($values['user_password']);
				} else {
					$new_salt = strRand(20);
					$values['user_password_hash'] = hash('sha1', $new_salt . $values['user_password']);
					$values['user_password_salt'] = $new_salt;
				}
				
				$values['user_updated_by'] = $_SESSION['sess_id'];
				$values['user_updated_date'] = $timestamp;

				DM::update('user', $values);
			}
			
			$ObjRec = DM::load('user', $id);
			
			Logger::log($_SESSION['sess_id'], $timestamp, "User ID '" . $ObjRec['user_login'] . "' (ID:" . $ObjRec['user_id'] .") is " . ($isNew ? "created" : "updated") . " by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			header("location:user.php?user_id=" . $id . "&msg=S-1001");
			return;
		}
	
		if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$ObjRec = DM::load('user', $id);
			
			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}
			
			if($ObjRec['user_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}
			
			$values['user_id'] = $id;
			$values['user_status'] = STATUS_DISABLE;			
			$values['user_updated_by'] = $_SESSION['sess_id'];
			$values['user_updated_date'] = $timestamp;
			
			DM::update('user', $values);
			Logger::log($_SESSION['sess_id'], $timestamp, "User ID '" . $ObjRec['user_login'] . "' (ID:" . $ObjRec['user_id'] .") is deleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			header("location: list.php?msg=S-1002");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>