<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$page_no = $_REQUEST['page_no'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	if($page_no == "" || !isInteger($page_no)){
		$page_no = 1;
	}

	$searchUtil = new SearchUtil('user');
	$searchUtil->addDefaultCriteriaField(' user_status = ? ', STATUS_ENABLE);
	$searchUtil->addDefaultCriteriaField(' user_id != ? ', "1");

	$searchUtil->addSearchableField('Display Name', 'user_display_name', 'text');
	$searchUtil->addSearchableField('Login', 'user_login', 'text');

	$searchParameters = array();

	if(!empty( $_REQUEST['parameter_name'] ) && !empty( $_REQUEST['parameter_operator'] ) && !empty( $_REQUEST['parameter_value'] ) )
		$searchParameters[] = (object) array("parameter_name" => $_REQUEST['parameter_name'] ?? '', "parameter_operator" => $_REQUEST['parameter_operator'] ?? '', "parameter_value" => $_REQUEST['parameter_value']);

	$resultObj = $searchUtil->doSearchPaging($page_no, 10, " user_display_name ", $searchParameters);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script>
			function deleteRecord(id){
				if(confirm("Are you sure you want to delete this record?")){
					location.href = "act.php?type=user&processType=delete&user_id=" + id;
				}
			}

			$( document ).ready(function() {
				var config = <?php echo $searchUtil->exportConfig(); ?>;
				var input = <?php echo json_encode($searchParameters); ?>;
				var searchUtil = new SearchUtil('form1');
				searchUtil.setUp(config, input);

				$("#page_first").bind('click', function(){
					searchUtil.toPage(1);
				});

				$("#page_last").bind('click', function(){
					searchUtil.toPage(<?php echo $resultObj->total_page ?>);
				});

				$("#page_prev").bind('click', function(){
					searchUtil.toPage(<?php echo $resultObj->page_no == 1 ? 1 : $resultObj->page_no - 1 ?>);
				});

				$("#page_next").bind('click', function(){
					searchUtil.toPage(<?php echo $resultObj->page_no < $resultObj->total_page ? $resultObj->page_no + 1 : $resultObj->total_page ?>);
				});

				$("#btn_search").bind('click', function(){
					searchUtil.applyFilter();
				});

				$("#btn_reset").bind('click', function(){
					searchUtil.resetFilter();
				});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Users</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					<button type="button" class="btn btn-default" onclick="location.href='user.php'; return false;">
						<span class="glyphicon glyphicon-plus"></span> New
					</button>
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
				<div class="row" style="padding-bottom:10px;">
					<label class="col-xs-1 control-label">Filter:</label>
					<div class="col-xs-2">
						<select id="show_parameter_name" name="show_parameter_name" class="form-control">
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-xs-2">
						<select id="show_parameter_operator" name="show_parameter_operator" class="form-control">
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-xs-2">
						<input type="text" id="show_parameter_value" name="show_parameter_value" value="" class="form-control"/>
					</div>
					<div class="col-xs-3">
						<div class="row">
							<div class="col-xs-5">
								<button type="button" class="btn btn-default" id="btn_search">
									<span class="fa fa-search-plus"></span> Execute
								</button>
							</div>
							<div class="col-xs-7">
								<?php if(count($searchParameters) > 0){ ?>
									<button type="button" class="btn btn-default" id="btn_reset">
										<span class="fa fa-search-minus"></span> Reset Filter
									</button>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12" style="text-align:right">
						Page <?php echo $resultObj->page_no ?> of <?php echo $resultObj->total_page ?>
					</label>
				</div>
				<div class="row">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="col-xs-1">#</th>
								<th class="col-xs-3">Display Name</th>
								<th class="col-xs-3">Login</th>
								<th class="col-xs-3">Last Login Date</th>
								<th class="col-xs-2">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$row_count = 1;
								foreach($resultObj->result as $row)
								{
									$user_id = $row['user_id'];
									$user_login = htmlspecialchars($row['user_login']);
									$user_display_name = htmlspecialchars($row['user_display_name']);
									$user_last_login = htmlspecialchars($row['user_last_login']);
									$user_type = htmlspecialchars($row['user_type']);
							?>
									<tr>
										<td>
											<?php echo ($resultObj->page_no - 1)*10 + $row_count ?>
										</td>
										<td><?php echo $user_display_name ?></td>
										<td><?php echo $user_login ?></td>
										<td><?php echo $user_last_login == "" ? "N/A" : $user_last_login ?></td>
										<td style="text-align:center">
											<button type="button" class="btn btn-primary btn-sm" onclick="location.href='user.php?user_id=<?php echo $user_id ?>'; return false;">
												<span class="fa fa-folder-open"></span> Edit
											</button>
											<?php if($user_id != 1 && $user_type == 'O') { ?>
												<button type="button" class="btn btn-danger btn-sm" onclick="deleteRecord('<?php echo $user_id ?>'); return false;">
													<span class="glyphicon glyphicon-trash"></span> Delete
												</button>
											<?php } ?>
										</td>
									</tr>
							<?php
								$row_count++;
								}
								unset($resultObj);
							?>
						</tbody>
					</table>
				</div>
				<div class="row" style="float:right">
					<ul class="pager">
						<li>
							<button id="page_first" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-backward"></span> First
							</button>
						</li>
						<li>
							<button id="page_prev" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-left"></span> Previous
							</button>
						</li>
						<li>
							<button id="page_next" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-right"></span> Next
							</button>
						</li>
						<li>
							<button id="page_last" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-forward"></span> Last
							</button>
						</li>
					</ul>
					<input type="hidden" id="parameter_name" name="parameter_name" value=""/>
					<input type="hidden" id="parameter_operator" name="parameter_operator" value=""/>
					<input type="hidden" id="parameter_value" name="parameter_value" value=""/>
					<input type="hidden" id="page_no" name="page_no" value=""/>
				</div>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>