<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);


	$type_name = "issue";

	if($type=="issue" )

	{

		$id = $_REQUEST['issue_id'] ?? '';
		$issue_lv1 = $_REQUEST['issue_lv1'] ?? '';
		$issue_lv2 = $_REQUEST['issue_lv2'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "signed")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();

			if($isNew){

				$values['issue_publish_status'] = "D";

				$values['issue_status'] = STATUS_ENABLE;

				$values['issue_created_by'] = $_SESSION['sess_id'];

				$values['issue_created_date'] = $timestamp;

				$values['issue_updated_by'] = $_SESSION['sess_id'];

				$values['issue_updated_date'] = $timestamp;

				$id = DM::insert('issue', $values);

				if(!file_exists($issue_path . $id. "/")){
					mkdir($issue_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('issue', $id);



				if($ObjRec['issue_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}


				$values['issue_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['issue_updated_by'] = $_SESSION['sess_id'];

				$values['issue_updated_date'] = $timestamp;



				DM::update('issue', $values);

			}


			$ObjRec = DM::load('issue', $id);

			$issue_img_arr = array('issue_cover_image','issue_inside_page_image','issue_pdf');

			foreach($issue_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($issue_path . $id . "/" . $ObjRec[$val])){
						//@unlink($issue_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($issue_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($issue_path . $id . "/".$newname);

					if($val =='issue_cover_image'){
						$newWidth ="400";
						$newHeight="595";
					}else{
						$newWidth ="250";
						$newHeight="570";
					}
					if($val != 'issue_pdf'){
						cropImage($issue_path. $id."/". $newname, $newWidth,$newHeight , $issue_path. $id."/". $newname);

					}


					$values = array();
					$values[$val] = $newname;
					$values['issue_id'] = $id;

					DM::update('issue', $values);
					unset($values);

					$ObjRec = DM::load('issue', $id);
				}
			}



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "issue '" . $ObjRec['issue_name_lang1'] . "' (ID:" . $ObjRec['issue_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "signed", $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "signed",$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['issue_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "signed", $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."issue/issue.php?issue_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "signed", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "signed", "PA", $approval_id);

			}

			header("location:issue.php?issue_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('issue', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['issue_status'] == STATUS_DISABLE)

			{

				header("location: list.php?msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("issue", $id);

			$values['issue_id'] = $id;
			$values['issue_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['issue_status'] = STATUS_DISABLE;
			$values['issue_updated_by'] = $_SESSION['sess_id'];
			$values['issue_updated_date'] = getCurrentTimestamp();


			DM::update('issue', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("signed", $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "issue '" . $ObjRec['issue_name_lang1'] . "' (ID:" . $ObjRec['issue_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "signed", "DS");

				$files = array($SYSTEM_HOST."issue/issue.php?issue_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("signed", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "signed", "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "signed", "D");
			}





			header("location: list.php?msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('issue', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['issue_status'] == STATUS_ENABLE || $ObjRec['issue_publish_status'] == 'AL')

			{

				header("location: list.php?msg=F-1006");

				return;

			}



			$values['issue_id'] = $id;

			$values['issue_publish_status'] = 'D';

			$values['issue_status'] = STATUS_ENABLE;

			$values['issue_updated_by'] = $_SESSION['sess_id'];

			$values['issue_updated_date'] = getCurrentTimestamp();



			DM::update('issue', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'issue', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of signed '" . $ObjRec['issue_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'signed', "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "signed '" . $ObjRec['issue_name_lang1'] . "' (ID:" . $ObjRec['issue_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'signed', "UD");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
