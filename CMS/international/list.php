<?php

	include "../include/config.php";

	include $SYSTEM_BASE . "include/function.php";

	include $SYSTEM_BASE . "session.php";

	include $SYSTEM_BASE . "include/system_message.php";

	include $SYSTEM_BASE . "include/classes/DataMapper.php";

	include $SYSTEM_BASE . "include/classes/SearchUtil.php";

	include $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
	$international_type = $_REQUEST['international_type'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "international") ){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}


	$page_no = $_REQUEST['page_no'] ?? '';
	$page_size = 10;
	
	if($page_no == "" || !isInteger($page_no)){
		$page_no = 1;
	}

	$sql = " ( international_type = ? AND international_status = ? OR (international_status = ? AND international_publish_status in (?, ?))) ";
	$parameters = array($international_type, STATUS_ENABLE, STATUS_DISABLE, 'RCA', 'RAA');
	


	$total_record = DM::count("international", $sql, $parameters);
	
	$total_page = ceil($total_record / $page_size);
	
	if($total_page == 0)
		$total_page = 1;
	
	if($page_no > $total_page){
		$page_no = $total_page;
	}

	$result = DM::findAllWithPaging("international", $page_no, $page_size, $sql, " international_seq DESC ", $parameters);


?>

<!DOCTYPE html>

<html lang="en">

	<head>

		<?php include "../header.php" ?>

		<script language="javascript" src="../js/SearchUtil.js"></script>

		<script>

			function deleteRecord(itemId,itemLv1,itemLv2){

				if(confirm("Are you sure you want to delete this record ?")){

					$("#processType").val('delete');

					$("#international_id").val(itemId);
					$("#international_lv1").val(itemLv1);
					$("#international_lv2").val(itemLv2);

					$('#approvalModalDialog').modal('show');

				}

			}

			

			function undeleteRecord(itemId,itemLv1,itemLv2){

				if(confirm("Are you sure you want to undelete this record ?")){

					$("#processType").val('undelete');

					$("#international_id").val(itemId);
					$("#international_lv1").val(itemLv1);
					$("#international_lv2").val(itemLv2);

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {

						var frm = document.form2;

						frm.submit();

					})

					

					$('#loadingModalDialog').modal('show');

				}

			}

			

			function submitForApproval(){

				if($("#input_launch_date").val() == ""){

					alert("Please select launch date");

				} else {

					$("#approval_remarks").val($("#input_approval_remarks").val());

					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());

					$('#approvalModalDialog').modal('hide');



					$('#loadingModalDialog').on('shown.bs.modal', function (e) {

						var frm = document.form2;

						frm.submit();

					})

					

					$('#loadingModalDialog').modal('show');

				}

			}



			$( document ).ready(function() {
				$("#page_first").bind('click', function(){
					$("#page_no").val(1);
					$("#form1").submit();
				});
				
				$("#page_last").bind('click', function(){
					$("#page_no").val(<?php echo $total_page ?>);
					$("#form1").submit();
				});
				
				$("#page_prev").bind('click', function(){
					$("#page_no").val(<?php echo $page_no == 1 ? 1 : $page_no - 1 ?>);
					$("#form1").submit();
				});
				
				$("#page_next").bind('click', function(){
					$("#page_no").val(<?php echo $page_no < $total_page ? $page_no + 1 : $total_page ?>);
					$("#form1").submit();
				});
				

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>

					setTimeout(function() {

						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});

					}, 5000);

				<?php } ?>

			});

		</script>

		<style>

			.table > tbody > tr > td { vertical-align:middle; }

		</style>

	</head>

	<body>

		<?php include "../menu.php" ?>

		<!-- Begin page content -->

		<div id="content" class="container">

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>

				<div id="alert_row" class="row">

					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">


						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>

					</div>

				</div>

			<?php } ?>

			<div class="row">

				<ol class="breadcrumb-left col-xs-10">

					<li><a href="../dashboard.php">Home</a></li>

					<li class="active">International</li>
					<?php
						if($international_type=='P'){
							// $iType = 'Exchange Programme';
							$iType = 'Sharing from incoming students';
						}else if($international_type=='I'){
							$iType = 'Incoming Students';
						}else if($international_type=='O'){
							// $iType = 'Outgoing Students';
							$iType = 'Sharing from outgoing students';
						}else if($international_type=='S'){
							$iType = 'Scholarship';
						}
					?>
					<li class="active"><?php echo $iType?></li>

				</ol>

				<span class="breadcrumb-right col-xs-2">

					<?php if(PermissionUtility::canCreateByKey($_SESSION['sess_id'], "international")){ ?>

						<button type="button" class="btn btn-default" onclick="location.href='international.php?international_type=<?php echo $international_type?>'; return false;">

							<span class="glyphicon glyphicon-plus"></span> New

						</button>

					<?php } ?>

				</span>

			</div>

			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">

				<div class="row">

					<table class="table table-bordered table-striped">

						<thead>

							<tr>

								<th class="col-xs-1">#</th>

								<th class="col-xs-7">Name</th>
                                
                                <th class="col-xs-2">Priority</th>

								<th class="col-xs-2">&nbsp;</th>

							</tr>

						</thead>

						<tbody>

							<?php

								$counter = 0;

								foreach($result as $international) {

									$counter++;

							?>

								<tr <?php echo $international['international_status'] == STATUS_ENABLE ? "" : "class='warning'" ?>>

									<td>

										<?php echo $counter ?>

									</td>

									<td>

										<?php echo htmlspecialchars($international['international_name_lang1']) ?>

									</td>
                                    <td>

										<?php echo htmlspecialchars($international['international_seq'])  ?>

									</td>

									<td class="col-xs-2" style="text-align:center">

										<?php if($international['international_status'] == STATUS_ENABLE) { ?>

											<a role="button" class="btn btn-primary btn-sm" href="international.php?international_id=<?php echo $international['international_id'] ?>">

												<span class="fa fa-folder-open"></span> Edit

											</a>

										<a role="button" class="btn btn-danger btn-sm" href="#" onclick="deleteRecord('<?php echo $international['international_id'] ?>'); return false;">
													<i class="glyphicon glyphicon-trash"></i> Delete
												</a>
											<?php } else { ?>
												<a role="button" class="btn btn-danger btn-sm" href="#" onclick="undeleteRecord('<?php echo $international['international_id'] ?>'); return false;">
													<i class="glyphicon glyphicon-repeat"></i> Undelete
												</a>
											<?php } ?>

									</td>

								</tr>

							<?php } ?>

						</tbody>

					</table>

				</div>
                
                <div class="row" style="float:right">
					<ul class="pager">
						<li>
							<button id="page_first" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-backward"></span> First
							</button>
						</li>
						<li>
							<button id="page_prev" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-left"></span> Previous
							</button>
						</li>
						<li>
							<button id="page_next" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-chevron-right"></span> Next
							</button>
						</li>
						<li>
							<button id="page_last" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-forward"></span> Last
							</button>
						</li>
					</ul>
					<input type="hidden" id="page_no" name="page_no" value=""/>
				</div>
				<input type="hidden" id="international_type" name="international_type" value="<?php echo $international_type?>"/>
			</form>

			<form class="form-horizontal" role="form" id="form2" name="form2" action="act.php" method="post">

				<input type="hidden" id="type" name="type" value="international"/>

				<input type="hidden" id="processType" name="processType" value=""/>

				<input type="hidden" id="approval_remarks" name="approval_remarks" value=""/>

				<input type="hidden" id="approval_launch_date" name="approval_launch_date" value=""/>

				<input type="hidden" id="international_id" name="international_id" value=""/>
				<input type="hidden" id="international_type" name="international_type" value="<?php echo $international_type?>"/>

			</form>

		</div>

		<!-- End page content -->

		<?php include "../footer.php" ?>

	</body>

</html>