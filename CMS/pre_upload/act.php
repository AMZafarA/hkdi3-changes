<?php

include "../include/config.php";
include "../include/function.php";

session_start();

$values = array();
if($_SESSION['sess_login'] == null || $_SESSION['sess_login'] == "") {
	$values['status'] = "FAIL";
	$values['tmpname'] = "";
	$values['filename'] = "";
	$values['url'] = "";
	@unlink($_FILES['upload_file']['tmp_name']);
} 
if( isset($_FILES['upload_file']) ) {
	if($_FILES['upload_file']['tmp_name'] == "") {
		$values = array();
		$values['status'] = "FAIL";
		$values['tmpname'] = "";
		$values['filename'] = "";
		$values['url'] = "";
	}	
	else {
		$base_name = '';
		if( isset( $_FILES['upload_file'] ) ) {
			$base_name = basename($_FILES['upload_file']['tmp_name']) . "." . strtolower(pathinfo($_FILES['upload_file']['name'],PATHINFO_EXTENSION));
		}

		if(!file_exists($SYSTEM_BASE . "uploaded_files/tmp/")){
			mkdir($SYSTEM_BASE . "uploaded_files/tmp", 0775, true);
		}

		if( isset( $_FILES['upload_file'] ) ) {
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $TEMP_UPLOAD_PATH . $base_name);

			$values = array();
			$values['status'] = "OK";
			$values['tmpname'] = $base_name;
			$values['filename'] = $_FILES['upload_file']['name'];
		}
	}
} 

echo json_encode($values);
?>