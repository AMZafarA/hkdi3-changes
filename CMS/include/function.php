<?php
	function postBack($parameter, $url){
?>
	<html>
		<body onLoad="document.form1.submit();">
			<form name="form1" method="post" action="<?php echo $url ?>">
				<?php foreach($parameter as $rKey => $rValue) { ?>
					<input type="hidden" name="<?php echo $rKey ?>" value="<?php echo htmlspecialchars($rValue) ?>"/>
				<?php } ?>
			</form>
		</body>
	</html>
<?php
	}

	function getCurrentTimestamp(){
		return date("Y-m-d H:i:s");
	}

	function getCurrentDate(){
		return date("Y-m-d");
	}

	function getTimestamp($offset = ''){
		if($offset != '')
			return date('Y-m-d H:i:s', strtotime($offset));
		else
			return date("Y-m-d H:i:s");
	}

	function strRand($length=0, $noCharacters = array())
	{
		$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ";
		$pattern = str_replace($noCharacters, '', $pattern);
		$pattern_len = strlen($pattern);
		$key = "";
		for($i=0;$i<$length;$i++)
		{
			$key .= $pattern[rand(0,$pattern_len-1)]; // $pattern_len-1 for index
		}
		return $key;
	}

	function resizeImage($inputFile, $newWidth, $newHeight, $outputFile)
	{
		list($width, $height) = getimagesize($inputFile);

		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);

		if($width < $height){
			$temp = $newWidth;
			$newWidth = $newHeight;
			$newHeight = $temp;
		}

		if($width > $newWidth && $height > $newHeight) {
			$newHeight = $newWidth / $width * $height;

			$thumb = ImageCreateTrueColor($newWidth,$newHeight);

			$source = createImage($inputFile, $extension);
			
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			
			if (file_exists($outputFile))
			{
				unlink($outputFile);
			}
			
			saveImage($thumb, $outputFile , 100, $extension);
			
			imagedestroy($source);
			imagedestroy($thumb);
		} else {
			copy($inputFile, $outputFile);
		}
	}
	
	function resizeImageByWidthOrHeight($inputFile, $newSize, $outputFile, $forceResize = false){
		list($width, $height) = getimagesize($inputFile);

		if($width >= $height){
			resizeImageByWidth($inputFile, $newSize, $outputFile, $forceResize);
		} else {
			resizeImageByHeight($inputFile, $newSize, $outputFile, $forceResize);		
		}
	}
	
	function resizeImageByWidth($inputFile, $newWidth, $outputFile, $forceResize = false)
	{
		list($width, $height) = getimagesize($inputFile);

		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
		
		if($width >= $newWidth || $forceResize) {

			if($width != $height)
				$newHeight = $newWidth / $width * $height;
			else
				$newHeight = $newWidth;

			$thumb = ImageCreateTrueColor($newWidth,$newHeight);
			
			imageinterlace($thumb, true);
			imagealphablending( $thumb, false );
			imagesavealpha( $thumb, true );

			$source = createImage($inputFile, $extension);
			
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			
			if (file_exists($outputFile))
			{
				unlink($outputFile);
			}
			
			saveImage($thumb, $outputFile, $extension);
			
			imagedestroy($source);
			imagedestroy($thumb);
		} else {
			copy($inputFile, $outputFile);
			$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
			$thumb = createImage($outputFile, $extension);

			imageinterlace($thumb, true);
			imagealphablending( $thumb, false );
			imagesavealpha( $thumb, true );
			
			saveImage($thumb, $outputFile, $extension);
			imagedestroy($thumb);
		}
	}

	function autoCropImage($inputFile, $outputFile)
	{
		list($width, $height) = getimagesize($inputFile);

		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
		
		if($width < $height){
			$targ_w =$width;
			$targ_h =$width;
			$x = 0;
			$y = ($height - $width)/2 ;
		}else{
			$targ_w =$height;
			$targ_h =$height;
			$x = ($width - $height)/2 ;
			$y = 0;
		}
		
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
		$img_r = createImage($inputFile, $extension);
		
		//$img_r = imagecreatefromjpeg($inputFile);
		//$dst_r = imagecreatetruecolor( $targ_w, $targ_h );

		
		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$targ_w,$targ_h,$targ_w,$targ_h);

		saveImage($dst_r, $outputFile , $extension);
		imagedestroy($dst_r);
	}
	
	function resizeImageByHeight($inputFile, $newHeight, $outputFile, $forceResize = false)
	{
		list($width, $height) = getimagesize($inputFile);

		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
		
		if($height >= $newHeight || $forceResize) {

			if($width != $height)
				$newWidth = $newHeight / $height * $width;
			else
				$newWidth = $newHeight;

			$thumb = ImageCreateTrueColor($newWidth,$newHeight);
			
			imageinterlace($thumb, true);
			imagealphablending( $thumb, false );
			imagesavealpha( $thumb, true );

			$source = createImage($inputFile, $extension);
			
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			
			if (file_exists($outputFile))
			{
				unlink($outputFile);
			}
			
			saveImage($thumb, $outputFile, $extension);
			
			imagedestroy($source);
			imagedestroy($thumb);
		} else {
			copy($inputFile, $outputFile);
			
			$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
			$thumb = createImage($outputFile, $extension);

			imageinterlace($thumb, true);
			imagealphablending( $thumb, false );
			imagesavealpha( $thumb, true );
			
			saveImage($thumb, $outputFile, $extension);
			imagedestroy($thumb);
		}
	}

	function cropImage($inputFile, $inputWidth, $inputHeight, $outputFile){
		list($img_width, $img_height) = getimagesize($inputFile);
		
		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
		$source = createImage($inputFile, $extension);

		$thumb = imagecreatetruecolor($inputWidth, $inputHeight);
		
		imageinterlace($thumb, true);
		imagealphablending( $thumb, false );
		imagesavealpha( $thumb, true );

		$width_ratio  = $inputWidth  / $img_width;
		$height_ratio = $inputHeight / $img_height;

		if($width_ratio > $height_ratio) {
			$resized_width  = $inputWidth;
			$resized_height = $img_height * $width_ratio;
		} else {
			$resized_height = $inputHeight;
			$resized_width  = $img_width * $height_ratio;
		}
		// Drop decimal values
		$resized_width  = round($resized_width);
		$resized_height = round($resized_height);

		// Calculations for centering the image
		$offset_width  = round(($inputWidth  - $resized_width) / 2);
		$offset_height = round(($inputHeight - $resized_height) / 2);

		imagecopyresampled($thumb, $source, $offset_width, $offset_height, 0, 0, $resized_width, $resized_height, $img_width, $img_height);
						  
		saveImage($thumb, $outputFile, $extension);

		imagedestroy($thumb);
		imagedestroy($source);
	}

	function resizeAndBg($source_image, $destination, $tn_w, $tn_h, $quality = 100, $wmsource = false)
	{

		$image_info = getimagesize($source_image);
		$width = $image_info[0];
		$height = $image_info[1];
		$mime_type = $image_info["mime"];
		$maxWidth = $tn_w;
		$maxHeight = $tn_h;

		$canvas = imagecreatetruecolor($maxWidth, $maxHeight);
		imagealphablending($canvas, false);
		$background = imagecolorallocatealpha($canvas, 255, 255, 255, 127);
		imagefilledrectangle($canvas, 0, 0, $maxWidth, $maxHeight, $background);
		imagealphablending($canvas, true);

		$leftOffset = ($maxWidth / 2) - ($width / 2);
		$topOffset = ($maxHeight / 2) - ($height / 2);

		switch($mime_type)
		{
		    case "image/jpeg":
		    	$new_image = imagecreatefromjpeg($source_image);
		        imagecopyresampled($canvas, $new_image, $leftOffset, $topOffset, 0, 0, $width, $height, $width, $height);
		        imagealphablending($canvas, true);        
		        imagesavealpha($canvas, true);
		        imagejpeg($canvas, $destination, $quality);
		        imagedestroy($canvas);
		        break;
		    case "image/gif":
		        //GIF code
		        break;
		    case "image/png":
		        $new_image = imagecreatefrompng($source_image);
		        imagecopyresampled($canvas, $new_image, $leftOffset, $topOffset, 0, 0, $width, $height, $width, $height);
		        imagealphablending($canvas, true);        
		        imagesavealpha($canvas, true);
				$quality=9;
		        imagepng($canvas, $destination, $quality);
		        imagedestroy($canvas);        
		        break;
		}


	   /* $info = getimagesize($source_image);
	    $imgtype = image_type_to_mime_type($info[2]);

	    #assuming the mime type is correct
	    switch ($imgtype) {
	        case 'image/jpeg':
	            $source = imagecreatefromjpeg($source_image);
	            break;
	        case 'image/gif':
	            $source = imagecreatefromgif($source_image);
	            break;
	        case 'image/png':
	            $source = imagecreatefrompng($source_image);
	            break;
	        default:
	            die('Invalid image type.');
	    }

	    #Figure out the dimensions of the image and the dimensions of the desired thumbnail
	    $src_w = imagesx($source);
	    $src_h = imagesy($source);


	    #Do some math to figure out which way we'll need to crop the image
	    #to get it proportional to the new size, then crop or adjust as needed

	    $x_ratio = $tn_w / $src_w;
	    $y_ratio = $tn_h / $src_h;

	    if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
	        $new_w = $src_w;
	        $new_h = $src_h;
	    } elseif (($x_ratio * $src_h) < $tn_h) {
	        $new_h = ceil($x_ratio * $src_h);
	        $new_w = $tn_w;
	    } else {
	        $new_w = ceil($y_ratio * $src_w);
	        $new_h = $tn_h;
	    }

	    $newpic = imagecreatetruecolor(round($new_w), round($new_h));
	    imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
	    $final = imagecreatetruecolor($tn_w, $tn_h);
	    $backgroundColor = imagecolorallocate($final, 255, 255, 255);
	    imagefill($final, 0, 0, $backgroundColor);
	    //imagecopyresampled($final, $newpic, 0, 0, ($x_mid - ($tn_w / 2)), ($y_mid - ($tn_h / 2)), $tn_w, $tn_h, $tn_w, $tn_h);
	    imagecopy($final, $newpic, (($tn_w - $new_w)/ 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);


	    if (imagejpeg($final, $destination, $quality)) {
	        return true;
	    }
	    return false;*/
	}
	
	function cropImageCenter($inputFile, $imageSize, $outputFile){
		$thumb = ImageCreateTrueColor($imageSize,$imageSize);
		imageinterlace($thumb, true);
		imagealphablending( $thumb, false );
		imagesavealpha( $thumb, true );

		$extension = pathinfo($inputFile, PATHINFO_EXTENSION);
		$source = createImage($inputFile, $extension);
		
		list($width, $height) = getimagesize($inputFile);
		
		$width_new = $height;
		$height_new = $width;
		
		if($height > $width){
			//cut point by height
			$h_point = (($height - $width) / 2);
			//copy image
			imagecopyresampled($thumb, $source, 0, 0, 0, $h_point, $imageSize, $imageSize, $width, $width);
		}else{
			//cut point by width
			$w_point = (($width - $height) / 2);
			imagecopyresampled($thumb, $source, 0, 0, $w_point, 0, $imageSize, $imageSize, $height, $height);
		}

		saveImage($thumb, $outputFile, $extension);
		imagedestroy($thumb);
		imagedestroy($source);
	}

	function createImage($inputFile, $extension){
		$extension = strtolower($extension);
		if($extension == 'png')
			return imagecreatefrompng($inputFile);
		else if($extension == 'gif')
			return imagecreatefromgif($inputFile);
		else if($extension == 'jpeg' || $extension == 'jpg')
			return imagecreatefromjpeg($inputFile);
	}
	
	function saveImage($imgfile,$outputFile,$extension){
		$extension = strtolower($extension);
		if($extension == 'png')
			imagepng($imgfile,$outputFile, 9);
		else if($extension == 'gif')
			imagegif($imgfile,$outputFile, 95);
		else if($extension == 'jpeg' || $extension == 'jpg')
			imagejpeg($imgfile,$outputFile, 95);
	}

	function IsLeapYear($y)
	{
		$bulis=((mod($y,4)==0) && ((mod ($y,100)<>0) || (mod($y,400)==0)));
		return $bulis;
	}

	function daycount($dc_month, $dc_year)
	{
		switch ($dc_month)
		{
			case  1:
			case  3:
			case  5:
			case  7:
			case  8:
			case 10:
			case 12:
				return 31;
				break;
			case  4:
			case  6:
			case  9:
			case 11:
				return 30;
				break;
			case 2:
				if (IsLeapYear($dc_year)) { return 29; } else { return 28; };
				break;
		}
	}

	function isInteger($input){
	  if(is_array($input))
		  return false;
	  return preg_match('@^[-]?[0-9]+$@',$input) === 1;
	}
	
	function moveFile($path, $prefix, $suffix, $temp_file){
		$new_filename = "";
		while(true){
			$new_filename = hash('sha1', uniqid(hash('sha1', $prefix), true)) . "." . strtolower($suffix);
			if(!file_exists($path . DIRECTORY_SEPARATOR . $new_filename))
				break;
		}

		if(copy($temp_file ,  $path . DIRECTORY_SEPARATOR . $new_filename))
			return $new_filename;
		else
			return false;
	}

	function createRandomDirectory($path){
		$new_dirname = "";
		while(true){
			$new_dirname = hash('sha1', uniqid(hash('sha1', $_SERVER['HTTP_HOST'] . time() . $_SERVER['REMOTE_ADDR']), true));
			if(!file_exists($path . $new_dirname))
				break;
		}

		mkdir($path . $new_dirname);
		return $new_dirname;
	}
	
	function startsWith($haystack, $needle)
	{
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	function endsWith($haystack, $needle)
	{
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
	
	function getClientIp($allow_private_ip = false) {
		 $ipaddress = '';
		 if ($_SERVER['HTTP_CLIENT_IP'])
			 $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		 else if($_SERVER['HTTP_X_FORWARDED_FOR'])
			 $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		 else if($_SERVER['HTTP_X_FORWARDED'])
			 $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		 else if($_SERVER['HTTP_FORWARDED_FOR'])
			 $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		 else if($_SERVER['HTTP_FORWARDED'])
			 $ipaddress = $_SERVER['HTTP_FORWARDED'];
		 else if($_SERVER['REMOTE_ADDR'])
			 $ipaddress = $_SERVER['REMOTE_ADDR'];
		 else
			 $ipaddress = 'UNKNOWN';

		 return $ipaddress; 
	}
	
	function isPublicIP($ip){
		return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE |  FILTER_FLAG_NO_RES_RANGE);
	}
	
	function gzipOutput($content){
		if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) 
			ob_start("ob_gzhandler"); 
		else 
			ob_start();

		echo $content;
		
		ob_flush();
        flush();
		ob_end_flush();
		return;
	}
	
	function apache_module_exists($module)
	{
		return in_array($module, apache_get_modules());
	}
	
	//for development on windows only
	if (!function_exists('sem_get')) {
		function sem_get($key) {
			return fopen(__FILE__ . '.sem.' . $key, 'w+');
		}
		function sem_acquire($sem_id) {
			return flock($sem_id, LOCK_EX);
		}
		function sem_release($sem_id) {
			return flock($sem_id, LOCK_UN);
		}
	}

	if( !function_exists('ftok') )
	{
		function ftok($filename = "", $proj = "")
		{
			if( empty($filename) || !file_exists($filename) )
			{
				return -1;
			}
			else
			{
				$filename = $filename . (string) $proj;
				for($key = array(); sizeof($key) < strlen($filename); $key[] = ord(substr($filename, sizeof($key), 1)));
				return dechex(array_sum($key));
			}
		}
	}
	
	
	
	function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

if(!function_exists("array_column"))
{
    function array_column($array,$column_name)
    {
        return array_map(function($element) use($column_name){return $element[$column_name];}, $array);
    }
}

function rsyncFilesToRemote($local_directory, $remote_host, $remote_user, $remote_keyfile, $remote_directory){
	$local_directory = str_replace("\\", "/", $local_directory);
	$local_directory = str_replace("//", "/", $local_directory);
	$local_directory = substr($local_directory, -1) == "/" ? $local_directory : $local_directory . "/";

	$remote_directory = str_replace("\\", "/", $remote_directory);
	$remote_directory = str_replace("//", "/", $remote_directory);
	$remote_directory = substr($remote_directory, -1) == "/" ? $remote_directory : $remote_directory . "/";
	
	//exec('rsync -rlpgoDvc --delete -e "ssh -o \'UserKnownHostsFile=/dev/null\' -o \'StrictHostKeyChecking=no\' -o \'LogLevel=FATAL\' -l ' . escapeshellarg($remote_user) . " -i " . escapeshellarg($remote_keyfile) . '" ' . escapeshellarg($local_directory) . " " . escapeshellarg($remote_user) . "@" .  escapeshellarg($remote_host) . ":" . escapeshellarg($remote_directory) . " > /dev/null 2>&1");
}

?>
