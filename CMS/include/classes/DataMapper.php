<?php
require_once __DIR__ . '/Table.php';

class DM {
	private static $debugging = FALSE;
	private static $stopOnError = TRUE;
	private static $pdo;
	private static $tables;

	public static function setup($db_host, $db_name, $db_login, $db_password) {
		self::$pdo = new PDO('mysql:host=' . $db_host . ';dbname=' . $db_name . ';charset=utf8mb4', $db_login, $db_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
		self::$pdo->exec("set names utf8mb4");
		self::$pdo->exec("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4' ");
		self::$tables = array();
	}

	private static function log($message, $obj = ""){
		if(self::$debugging){
			echo $message . "<br/>";
			if($obj != ""){
				var_dump($obj);
			}
		}
	}

	public static function shutdown() {
		self::$pdo = null;
		self::$tables = null;
	}

	public static function setDebug($debug){
		self::$debugging = $debug;
	}

	public static function checkConnectionError(){
		if(self::$stopOnError){
			$errorInfo = self::$pdo->errorInfo();

			if($errorInfo[0] != "00000"){
				print_r(debug_backtrace());
				$message = "SQLSTATE error code: "  . $errorInfo[0] . "<br/>";
				$message .= "Driver-specific error code: "  . $errorInfo[1] . "<br/>";
				$message .= "Driver-specific error message: "  . $errorInfo[2];
				die($message);
			}
		}
	}

	public static function checkStatmentError($stmt){
		if(self::$stopOnError){
			$errorInfo = $stmt->errorInfo();

			if($errorInfo[0] != "00000"){
				$message = "SQLSTATE error code: "  . $errorInfo[0] . "<br/>";
				$message .= "Driver-specific error code: "  . $errorInfo[1] . "<br/>";
				$message .= "Driver-specific error message: "  . $errorInfo[2] . "<br/>";
				echo "<pre>";
				print_r(debug_backtrace());
				$stmt->debugDumpParams();
				echo "</pre>";
				die($message);
			}
		}
	}

	private static function executeStatmentQuery($stmt){
		self::log(" In executeStatmentQuery ", $stmt);
		$stmt->execute();
		self::checkStatmentError($stmt);

		$results = NULL;

		if($stmt->rowCount() > 0){
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} else {
			$results = array();
		}

		$stmt->closeCursor();
		return $results;
	}

	private static function checkLoadedTable($table){
		self::log(" loading table meta data ");
		if(gettype($table) == "string"){
			if(!array_key_exists($table, self::$tables))
				self::$tables[$table] = new Table($table);
		} else if(is_array($table)) {
			foreach($table as $key){
				if(!array_key_exists($key, self::$tables))
					self::$tables[$key] = new Table($key);
			}
		}
	}

	public static function execute($sql){
		self::log(" in execute ");
		self::log(" sql : " . $sql);
		$stmt = self::$pdo->query($sql);
		return self::executeStatmentQuery($stmt);
	}

	public static function query($sql, $parameters = array()){
		self::log(" in query ");
		self::log(" sql : " . $sql);

		$stmt = self::$pdo->prepare($sql);

		if ( ! is_array( $parameters ) && strpos( $sql, '?' ) !== false ) $parameters = [ $parameters ];
		for($i = 1; $i <= count($parameters); $i++){
			$stmt->bindValue($i, $parameters[$i - 1]);
		}

		return self::executeStatmentQuery($stmt);
	}

	public static function findAll($table, $condition = "", $orderBy = "", $parameters = array()){
		self::checkLoadedTable($table);

		$sql = " SELECT * FROM ";

		if(gettype($table) == "string")
			$sql .= $table;
		else if(is_array($table)) {
			for($i = 0; $i < count($table); $i++){
				if($i > 0)
					$sql .= " , ";
				$sql .= $table[$i];
			}
		}

		$condition = trim($condition);
		$orderBy = trim($orderBy);

		if($condition != "")
			$sql .= " WHERE " . $condition;

		if($orderBy != "")
			$sql .= " ORDER BY " . $orderBy;

		self::log(" in findAll ");
		self::log(" sql : " . $sql);

		return self::query($sql, $parameters);
	}

	public static function findAllWithPaging($table, $pageNo, $pageSize, $condition = "", $orderBy = "", $parameters = array()){
		self::checkLoadedTable($table);

		$sql = " SELECT * FROM ";

		if(gettype($table) == "string")
			$sql .= $table;
		else if(is_array($table)) {
			for($i = 0; $i < count($table); $i++){
				if($i > 0)
					$sql .= " , ";
				$sql .= $table[$i];
			}
		}

		$condition = trim($condition);
		$orderBy = trim($orderBy);

		if($condition != "")
			$sql .= " WHERE " . $condition;

		if($orderBy != "")
			$sql .= " ORDER BY " . $orderBy;

		$sql .= " LIMIT " . $pageSize . " OFFSET " . (($pageNo - 1) * $pageSize);

		self::log(" in findAllWithPaging ");
		self::log(" sql : " . $sql);

		return self::query($sql, $parameters);
	}

	public static function findAllWithLimit($table, $limit, $condition = "", $orderBy = "", $parameters = array()){
		self::checkLoadedTable($table);

		$sql = " SELECT * FROM ";

		if(gettype($table) == "string")
			$sql .= $table;
		else if(is_array($table)) {
			for($i = 0; $i < count($table); $i++){
				if($i > 0)
					$sql .= " , ";
				$sql .= $table[$i];
			}
		}

		$condition = trim($condition);
		$orderBy = trim($orderBy);

		if($condition != "")
			$sql .= " WHERE " . $condition;

		if($orderBy != "")
			$sql .= " ORDER BY " . $orderBy;

		$sql .= " LIMIT " . $limit;

		self::log(" in findAllWithPaging ");
		self::log(" sql : " . $sql);

		return self::query($sql, $parameters);
	}

	public static function findOne($table, $condition = "", $orderBy = "", $parameters = array()){
		self::checkLoadedTable($table);

		$sql = " SELECT * FROM ";

		if(gettype($table) == "string")
			$sql .= $table;
		else if(is_array($table)) {
			for($i = 0; $i < count($table); $i++){
				if($i > 0)
					$sql .= " , ";
				$sql .= $table[$i];
			}
		}

		$condition = trim($condition);
		$orderBy = trim($orderBy);

		if($condition != "")
			$sql .= " WHERE " . $condition;

		if($orderBy != "")
			$sql .= " ORDER BY " . $orderBy;

		$sql .= " LIMIT 1 ";

		self::log(" in findOne ");
		self::log(" sql : " . $sql);

		$result = self::query($sql, $parameters);

		self::log(" result count : " . count($result));

		return (count($result) > 0) ? $result[0] : NULL;
	}

	public static function count($table, $condition = "", $parameters = array()){
		self::checkLoadedTable($table);

		$sql = " SELECT count(*) as counter FROM ";

		if(gettype($table) == "string")
			$sql .= $table;
		else if(is_array($table)) {
			for($i = 0; $i < count($table); $i++){
				if($i > 0)
					$sql .= " , ";
				$sql .= $table[$i];
			}
		}

		$condition = trim($condition);

		if($condition != "")
			$sql .= " WHERE " . $condition;

		self::log(" in count ");
		self::log(" sql : " . $sql);

		$result = self::query($sql, $parameters);

		return (count($result) > 0) ? $result[0]['counter'] : NULL;
	}

	public static function stub($table){
		self::checkLoadedTable($table);
		$tableMeta = self::$tables[$table];
		$keys = array_keys( $tableMeta->fields );
		$values = array_fill( 0, count( $keys ), '' );
		return array_combine( $keys, $values );
	}

	public static function load($table, $id){
		self::checkLoadedTable($table);

		$tableMeta = self::$tables[$table];

		$sql = " SELECT * FROM " . $table;
		$sql .= " WHERE " . $tableMeta->primaryKeys[0] . " = ? ";

		$stmt = self::$pdo->prepare($sql);
		$stmt->bindValue(1, $id);

		$result = self::executeStatmentQuery($stmt);

		if(count($result) == 0){
			unset($result);
			return NULL;
		}

		$data = $result[0];
		unset($result);

		$tableMeta = self::$tables[$table];

		foreach($data as $rKey => $rValue){
			if(array_key_exists($rKey, $tableMeta->fields)){
				switch($tableMeta->fields[$rKey]->type){
					case "text":
					case "date":
					case "datetime":
					case "time":
						break;
					case "timestamp":
					case "int":
						settype($data[$rKey], 'int');
						break;
					case "float":
						settype($data[$rKey], 'float');
						break;
				}
			}
		}

		return $data;
	}

	public static function insert($table, $values){
		self::checkLoadedTable($table);

		$tableMeta = self::$tables[$table];

		$sql = " INSERT INTO " . $table;

		$keys = array_keys($values);
		$keys = array_intersect($keys, array_keys($tableMeta->fields));
		$keys = array_values($keys);
		//$keys = array_diff($keys,$tableMeta->autoKeys);

		$sql .= " ( " . implode(",", $keys) . " ) ";

		$sql .= " VALUES (:" . implode(" , :", $keys) . " ) ";

		$stmt = self::$pdo->prepare($sql);

		foreach($keys as $key){
			if(in_array($key, $tableMeta->autoKeys) && $values[$key] == ""){
				$stmt->bindValue(":" . $key, null, PDO::PARAM_NULL);
			} else if($values[$key] == "" && $tableMeta->fields[$key]->allow_null) {
				switch($tableMeta->fields[$key]->type){
					case "text":
					case "date":
					case "datetime":
					case "time":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_STR);
					break;
					case "timestamp":
					case "integer":
					case "float":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_INT);
					break;
					default:
						$stmt->bindValue(":" . $key, null, PDO::PARAM_NULL);
					break;
				}
			} else {
				$stmt->bindValue(":" . $key, $values[$key]);
			}
		}

		$stmt->execute();
		self::checkStatmentError($stmt);

		return self::$pdo->lastInsertId();
	}

	public static function update($table, $values){
		self::checkLoadedTable($table);

		$tableMeta = self::$tables[$table];

		$sql = " UPDATE " . $table . " SET ";

		$keys = array_keys($values);
		$keys = array_intersect($keys, array_keys($tableMeta->fields));
		$primary_keys = array_intersect($keys, $tableMeta->primaryKeys);
		$field_keys = array_diff ($keys, $tableMeta->primaryKeys);

		$primary_keys = array_values($primary_keys);
		$field_keys = array_values($field_keys);

		for($i = 0; $i < count($field_keys); $i++){
			if($i > 0)
				$sql .= ", ";
			$sql .= $field_keys[$i] . " = :" . $field_keys[$i];
		}

		$sql .= " WHERE ";

		for($i = 0; $i < count($primary_keys); $i++){
			if($i > 0)
				$sql .= " AND ";
			$sql .= $primary_keys[$i] . " = :" . $primary_keys[$i];
		}

		$stmt = self::$pdo->prepare($sql);

		foreach($keys as $key){
			if($values[$key] == "" && $tableMeta->fields[$key]->allow_null) {
				switch($tableMeta->fields[$key]->type){
					case "text":
					case "date":
					case "datetime":
					case "time":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_STR);
					break;
					case "timestamp":
					case "integer":
					case "float":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_INT);
					break;
					default:
						$stmt->bindValue(":" . $key, null, PDO::PARAM_NULL);
					break;
				}
			} else {
				$stmt->bindValue(":" . $key, $values[$key]);
			}
		}

		$stmt->execute();
		self::checkStatmentError($stmt);

		return $stmt->rowCount();
	}

	public static function replace($table, $values){
		self::checkLoadedTable($table);

		$values = str_replace("/hk-staging/uploaded_files/", "/hk/uploaded_files/", $values);

		$tableMeta = self::$tables[$table];

		$sql = " REPLACE INTO " . $table;

		$keys = array_keys($values);
		$keys = array_intersect($keys, array_keys($tableMeta->fields));
		$keys = array_values($keys);
		//$keys = array_diff($keys,$tableMeta->autoKeys);

		$sql .= " ( " . implode(",", $keys) . " ) ";



		$sql .= " VALUES (:" . implode(" , :", $keys) . " ) ";

		$stmt = self::$pdo->prepare($sql);

		foreach($keys as $key){
			if(in_array($key, $tableMeta->autoKeys) && $values[$key] == ""){
				$stmt->bindValue(":" . $key, null, PDO::PARAM_NULL);
			} else if($values[$key] == "" && $tableMeta->fields[$key]->allow_null) {
				switch($tableMeta->fields[$key]->type){
					case "text":
					case "date":
					case "datetime":
					case "time":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_STR);
					break;
					case "timestamp":
					case "integer":
					case "float":
						$stmt->bindValue(":" . $key, null, PDO::PARAM_INT);
					break;
					default:
						$stmt->bindValue(":" . $key, null, PDO::PARAM_NULL);
					break;
				}
			} else {
				$stmt->bindValue(":" . $key, $values[$key]);
			}
		}

		$stmt->execute();
		self::checkStatmentError($stmt);

		return self::$pdo->lastInsertId();
	}

	public static function tableMaintenance(){
		$result = DM::findAll("information_schema.tables", " table_schema = ?  ", "", array($DB_NAME));

		foreach($result as $row){
			$table_name = $row['TABLE_NAME'];
			DM::execute(" ANALYZE TABLE " . $table_name);
			DM::execute(" OPTIMIZE TABLE " . $table_name);
			DM::execute(" REPAIR TABLE " . $table_name . " QUICK ");
			DM::execute(" REPAIR TABLE " . $table_name . " EXTENDED ");
		}
		unset($result);
	}

	public static function deleteAll($table){
		$sql = " DELETE FROM $table";

		self::log(" deleteAll ");
		self::log(" sql : " . $sql);

		return self::query($sql);
	}

	public static function findColumnHeaders($table, $column){
		$sql = " SELECT DISTINCT($column) FROM $table";

		self::log(" distinct columns ");
		self::log(" sql : " . $sql);

		return self::query($sql);
	}

}
?>