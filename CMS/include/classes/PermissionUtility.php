<?php
require_once("DataMapper.php");
require_once("Mutex.php");

class PermissionUtility {
	
	public static $PERMISSION_TYPE = array("E", "A");
	
	public static function rebuildPermission() {
		$sql = " DELETE FROM user_group_permission WHERE ugp_permission_key like 'product_%' ";
		$sql .= " AND ugp_permission_key not in (SELECT concat('product_', product_id) FROM product ";
		$sql .= " WHERE product_status = ? ) ";
		DM::query($sql, array(STATUS_ENABLE));
		
		foreach(self::$PERMISSION_TYPE as $type){
			$sql = " insert into user_group_permission ";
			$sql .= " select permission_key, ?, ? ";
			$sql .= " from permission ";
			$sql .= " where permission_type = ? ";

			DM::query($sql, array(1, $type, 'S'));
			
			$records = DM::findAll("permission", " permission_type = ? ", "", array('R'));
			
			foreach($records as $record){
				
				$items = DM::findAll($record['permission_key'], $record['permission_parameter_name'] . ' = ? ', '', array($record['permission_parameter_value']));
				
				foreach($items as $item) {
					$values = array();
					$values['ugp_permission_key'] = 'product_' . $item[$record['permission_key'] . '_id'];
					$values['ugp_user_group_id'] = 1;
					$values['ugp_role'] = $type;
					DM::insert('user_group_permission', $values);
					unset($values);
				}
			}
		}
	}
	
	public static function isAdminGroup($user_id){
		return DM::count("group_user", " group_user_uid = ? AND group_user_gid = ? ", array($user_id, 1)) > 0;
	}
	
	public static function canEditByKey($user_id, $permission_key)
	{
		if(self::isAdminGroup($user_id))
			return true;
		
		$sql = " ugp_user_group_id = group_user_gid AND ugp_permission_key = ? AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($permission_key, $user_id, "E")) > 0);
	}
	
	public static function canEditByKeyPrefix($user_id, $permission_key)
	{
		if(self::isAdminGroup($user_id))
			return true;
		
		$permission_key = str_replace("_","\_",$permission_key);

		if(strpos($permission_key, "%") === false){
			$permission_key = $permission_key . '%';
		}
		
		$sql = " ugp_user_group_id = group_user_gid AND ugp_permission_key like ? AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($permission_key, $user_id, "E")) > 0);
	}
	
	public static function canApproveAny($user_id)
	{
		if(self::isAdminGroup($user_id))
			return true;

		$sql = " ugp_user_group_id = group_user_gid AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($user_id, "A")) > 0);
	}
	
	public static function canApproveByKey($user_id, $permission_key)
	{
		if(self::isAdminGroup($user_id))
			return true;

		$sql = " ugp_user_group_id = group_user_gid AND ugp_permission_key = ? AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($permission_key, $user_id, "A")) > 0);
	}

	public static function canCreateAny($user_id)
	{
		if(self::isAdminGroup($user_id))
			return true;

		$sql = " ugp_user_group_id = group_user_gid AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($user_id, "C")) > 0);
	}

	public static function canCreateByKey($user_id, $permission_key)
	{
		if(self::isAdminGroup($user_id))
			return true;
		
		$sql = " ugp_user_group_id = group_user_gid AND ugp_permission_key = ? AND group_user_uid = ? AND ugp_role = ? ";
		return (DM::count(array("user_group_permission", "group_user"), $sql, array($permission_key, $user_id, "C")) > 0);
	}
}
?>