<?php
require_once(__DIR__.'/../config.php');
require_once(__DIR__.'/DataMapper.php');
require_once(__DIR__.'/NotificationService.php');

class ApprovalService {
	
	public static function createApprovalRequest($permission_key, $item_type, $item_id, $item_action, $timestamp, $remarks, $affectedFiles){
		global $SYSTEM_BASE;

		$values = array();				
		$values['approval_submitted_by'] = $_SESSION['sess_id'];
		$values['approval_item_action'] = $item_action;
		$values['approval_permission_key'] = $permission_key;
		$values['approval_item_type'] = $item_type;
		$values['approval_item_id'] = $item_id;
		$values['approval_submitted_remarks'] = $remarks;
		$values['approval_submit_date'] = $timestamp;
		$values['approval_affected_url'] = json_encode($affectedFiles);
		$values['approval_approval_status'] = "RHA";
		$values['approval_status'] = STATUS_ENABLE;
		$values['approval_created_by'] = $_SESSION['sess_id'];
		$values['approval_created_date'] = $timestamp;
		$values['approval_updated_by'] = $_SESSION['sess_id'];
		$values['approval_updated_date'] = $timestamp;

		$approval_id = DM::insert("approval", $values);
		unset($values);
		
		return $approval_id;
	}
	
	public static function withdrawApproval($permission_key, $item_type, $item_id, $timestamp){
		$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? AND approval_permission_key = ?";
		$sql .= " AND approval_approval_status in (?, ?, ?) ";
		
		$parameters = array(STATUS_ENABLE, $item_type, $item_id,$permission_key, 'RCA', 'RAA', 'APL');
		
		$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);
		
		if($item_type=='product'){
			$record = DM::load($item_type, $item_id);
			if($record['product_type'] ==5 ){
				//$item_type = 'HKDI Gallery';
			}
			if($record['product_type'] ==4 || $record['product_type'] ==5){
				$item_type = 'Exhibitons';
			}
			if($record['product_type'] ==3 ){
				$item_type = 'financial';
			}
			if($record['product_type'] ==2 ){
				$item_type = 'promotion';
			}
			
			if($record['product_type'] ==1 ){
				if(strpos($permission_key, 'scheme')!== false){
					$item_type = 'scheme';
				}
			}
		}
		
		foreach($approval_requests as $approval_request){
			$values = array();
			$values['approval_id'] = $approval_request['approval_id'];
			$values['approval_approval_status'] = 'W';
			$values['approval_updated_by'] = $_SESSION['sess_id'];
			$values['approval_updated_date'] = $timestamp;
			
			DM::update('approval', $values);
			unset($values);
			
			self::notificationProcessed($approval_request['approval_id']);
			
			$recipient_list = array("E", "C");
			
			if($approval_request['approval_approval_status'] != "RCA"){
				$recipient_list[] = "A";
			}
			
			$item_action = "";
			
			switch($approval_request['approval_approval_status']){
				case "RCA": 
					$item_action = "WC";
				break;
				case "RAA": 
					$item_action = "WA";
				break;
				case "APL": 
					$item_action = "WL";
				break;
			}
			
			NotificationService::sendNotification($item_type, $item_id, $timestamp, $_SESSION['sess_id'], $recipient_list, $permission_key, $item_action);
		}
	}
	
	public static function notificationProcessed($approval_id){
		$sql = " UPDATE notification SET notification_can_process = ? WHERE notification_approval_id = ? AND notification_can_process = ? ";
		$parameters = array("N", $approval_id, "Y");

		DM::query($sql, $parameters);
	}
}//class

?>