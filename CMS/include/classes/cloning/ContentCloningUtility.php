<?php
require_once(__DIR__.'/../../config.php');


// echo "remote_host : " . $remote_host . "<br><br>";
// echo "remote_user : " . $remote_user . "<br><br>";
// echo "remote_keyfile : " . $remote_keyfile . "<br><br>";
// die("~1");

require_once(__DIR__.'/../ContentSynchronizer.php');
require_once(__DIR__.'/ProgrammesCloningUtility.php');
require_once(__DIR__.'/NewsCloningUtility.php');
require_once(__DIR__.'/ProjectCloningUtility.php');
require_once(__DIR__.'/UniversitiesCloningUtility.php');
require_once(__DIR__.'/TopUpDegreeCloningUtility.php');
require_once(__DIR__.'/ProductCloningUtility.php');
require_once(__DIR__.'/IssueCloningUtility.php');
require_once(__DIR__.'/SignedCloningUtility.php');
require_once(__DIR__.'/StudentawardCloningUtility.php');
require_once(__DIR__.'/AlumniCloningUtility.php');
require_once(__DIR__.'/ExhibitonsCloningUtility.php');
require_once(__DIR__.'/DeptCloningUtility.php');
require_once(__DIR__.'/InternationalCloningUtility.php');
require_once(__DIR__.'/MasterLectureCloningUtility.php');
require_once(__DIR__.'/CollaborationCloningUtility.php');
require_once(__DIR__.'/TalentCloningUtility.php');
require_once(__DIR__.'/RsvpCloningUtility.php');
require_once(__DIR__.'/ContinuingEdcationCloningUtility.php');
require_once(__DIR__.'/ContinuingEducationGroupCloningUtility.php');
require_once(__DIR__.'/ContinuingEducationCourseCloningUtility.php');
require_once(__DIR__.'/PeecPageCloningUtility.php');
require_once(__DIR__.'/PeecMenuCloningUtility.php');
require_once(__DIR__.'/PeecSectionCloningUtility.php');
require_once(__DIR__.'/PeecDetailsCloningUtility.php');
require_once(__DIR__.'/PublicationsProgrammesUtility.php');
require_once(__DIR__.'/PeecCloningUtility.php');
require_once(__DIR__.'/PeecProgrammesCategoriesCloningUtility.php');
require_once(__DIR__.'/PeecProgrammesCloningUtility.php');

abstract class ContentCloningUtility {

	var $systemBase;
	
	function __construct() {
		require_once(__DIR__.'/../../config.php');
		global $SYSTEM_BASE, $PRODUCTION_SYSTEM_BASE, $root_path, $remote_host, $remote_user, $remote_keyfile, $root_path;
		$this->systemBase = $SYSTEM_BASE;
		$this->productionSystemBase = $PRODUCTION_SYSTEM_BASE;
		$this->remoteHost = $remote_host;
		$this->remote_User = $remote_user;
		$this->remoteKeyfile = $remote_keyfile;
		$this->rootPath = $root_path;
	}

	abstract public function cloneToProduction($itemId);

	public function removeDirectory($directory){
		foreach(glob($directory . "/*") as $file)
		{
			if(is_dir($file)) {
				$this->removeDirectory($file);
			} else if(file_exists($file)) {
				unlink($file);
			}
		}
		if(file_exists($directory))
			rmdir($directory);
	}

	public function removeFilesByRegex($directory, $fileRegex){
		foreach(array_filter(glob($directory . "/" . $fileRegex), 'is_file') as $file)
		{
			@unlink($file);
		}
	}

	public function makeDirectory($path){
		if(!file_exists($path)){
			mkdir($path, 0775, true);
		} else if(is_file($path)){
			unlink($path);
			mkdir($path, 0775, true);
		}
	}

	public function copyFile($sourcePath, $destinationPath){
		copy($sourcePath,$destinationPath);
	}

	public function startsWith($haystack, $needle) {
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}

	public function endsWith($haystack, $needle) {
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}

	//updated by ling

	public function removeTempImages($type, $itemId,  $image_arr) {
		return;
		$image_approval_file = $this->rootPath . "uploaded_files/" . $type . "/" .$itemId . "/";
		$approval_filelist = scandir($image_approval_file);
		for($i = 0; $i < count($approval_filelist); $i++ ){
			if(!is_dir($image_approval_file . $approval_filelist[$i])){
				if(!in_array($approval_filelist[$i], $image_arr) && file_exists($image_approval_file .$approval_filelist[$i])){
					unlink($image_approval_file . $approval_filelist[$i]);
				}
			}
		}
	}

	public function removeTempImagesLists($type, $itemId, $image_arr){
		return;
		$id = $type . "_image_id";
		for ($i=0; $i < count($image_arr) ; $i++) {
			$image_dir_arr[$i] = $image_arr[$i][$id];
		}
		$folder_path = $this->rootPath . "uploaded_files/" . $type . "/" .$itemId . "/";
		$allFileList = scandir($folder_path);
		$fileList = array_values(array_diff($allFileList, array('.', '..')));
		for ($i=0; $i < count($fileList) ; $i++) {
			if(is_dir($folder_path . $fileList[$i])){
				$dirList[$i] =  $fileList[$i];
			}
		}
		$removeList = array_values(array_diff($dirList, $image_dir_arr));
		for ($i=0; $i < count($removeList) ; $i++) {
			array_map('unlink', glob($folder_path . $removeList[$i] . "/*.*"));
			rmdir($folder_path . $removeList[$i]);
		}
	}
	//end of updated


	public static function getCloningUtility($type){
		switch($type){
			case "programmes": return new ProgrammesCloningUtility(); break;
			case "news": return new NewsCloningUtility(); break;
			case "project": return new ProjectCloningUtility(); break;
			case "top_up_degree": return new TopUpDegreeCloningUtility(); break;
			case "universities": return new UniversitiesCloningUtility(); break;
			case "product": return new ProductCloningUtility(); break;
			case "issue": return new IssueCloningUtility(); break;
			case "signed": return new SignedCloningUtility(); break;
			case "student_award": return new StudentawardCloningUtility(); break;
			case "alumni": return new AlumniCloningUtility(); break;
			case "exhibitons": return new ExhibitonsCloningUtility(); break;
			case "dept": return new DeptCloningUtility(); break;
			case "international": return new InternationalCloningUtility(); break;
			case "master_lecture": return new MasterLectureCloningUtility(); break;
			case "collaboration": return new CollaborationCloningUtility(); break;
			case "attachment": return new InternationalCloningUtility(); break;
			case "talent": return new TalentCloningUtility(); break;
			case "rsvp": return new RsvpCloningUtility(); break;
			case "continuing_edcation": return new ContinuingEdcationCloningUtility(); break;
			case "continuing_education_group": return new ContinuingEducationGroupCloningUtility(); break;
			case "continuing_education_course": return new ContinuingEducationCourseCloningUtility(); break;
			case "peec": return new PeecCloningUtility(); break;
			case "peec_details": return new PeecDetailsCloningUtility(); break;
			case "peec_menu": return new PeecMenuCloningUtility(); break;
			case "peec_page": return new PeecPageCloningUtility(); break;
			case "peec_section": return new PeecSectionCloningUtility(); break;
			case "publications_programmes": return new PublicationsProgrammesCloningUtility(); break;
			case "peec_programmes_categories": return new PeecProgrammesCategoriesCloningUtility(); break;
			case "peec_programmes": return new PeecProgrammesCloningUtility(); break;
			default: return false;

		}

	}
}
?>
