<?php
class PeecProgrammesCloningUtility extends ContentCloningUtility {

	public function cloneToProduction($itemId){
		$type = "peec_programmes";
		$record = DM::load($type, $itemId);
		DM::replace("p_".$type, $record);

		$product_sections = DM::findAll(" peec_programmes_section ", " peec_programmes_section_pid = ? ", "", array($itemId));
		foreach($product_sections as $product_section){
			DM::replace("p_peec_programmes_section", $product_section);
			
			/*$this->removeDirectory($this->systemBase .  "../p_uploaded_files/product_file/" . $itemId . "/");
			$this->makeDirectory($this->systemBase . "../p_uploaded_files/product_file/" . $itemId . "/" . $productSection['product_section_id'] . "/");
					ContentSynchronizer::synchronizeDirectory($this->systemBase . "../p_uploaded_files/product_file/" . $itemId . "/" . $productSection['product_section_id'] . "/", $this->systemBase . "../p_uploaded_files/product_file/" . $itemId . "/" . $productSection['product_section_id'] . "/");*/
				
		}

		/*$this->removeDirectory($this->productionSystemBase . "uploaded_files/".$type."/" . $itemId . "/");

		if($record[$type.'_status'] == STATUS_ENABLE) {
			$this->makeDirectory($this->productionSystemBase . "uploaded_files/".$type."/" . $itemId . "/");
			ContentSynchronizer::synchronizeDirectory($this->systemBase . "../uploaded_files/".$type."/" . $itemId . "/", $this->productionSystemBase . "uploaded_files/".$type."/" . $itemId . "/");

			rsyncFilesToRemote($this->productionSystemBase . "uploaded_files/".$type."/" . $itemId . "/", $this->remoteHost, $this->remote_User, $this->remoteKeyfile, $this->productionSystemBase . "uploaded_files/".$type."/" . $itemId . "/");
		}*/

		//updated by ling 20190515
	/*	$image_arr = array($record['peec_details_image'], $record['peec_details_thumb']);
		$this->removeTempImages("peec", $itemId, $image_arr);*/
		unset($record);
	}
}
?>
