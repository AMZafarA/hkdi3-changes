<?php
require_once("DataMapper.php");

class MessageUtility {
	public static function buildNotificationFromUser($notification_rec){
		if($notification_rec['notification_action_user'] == 0){
			return "System";
		}
		
		$user = DM::findOne("user", " user_id = ? ", " user_id ", array($notification_rec['notification_action_user']));
		return $user['user_display_name'];
	}

	public static function getItemTypeName($notification_item_type){
		$message = "";

		switch($notification_item_type){
			case "top_up_degree";
				$message = "Programmes - Degree - Top-up Degree";
			break;
			case "universities";
				$message = "Programmes - Degree - Partner Universities";
			break;
			case "master_index";
				$message = "Programmes - Master Degree - Index";
			break;
			case "master_detail";
				$message = "Programmes - Master Degree - Detail";
			break;
			case "issue";
				$message = "Signed";
			break;
			case "international";
				$message = "International";
			break;
			case "collaboration";
				$message = "Collaboration";
			break;
			case "attachment";
				$message = "Attachment";
			break;
			case "talent";
				$message = "Talent";
			break;
			case "master_lecture";
				$message = "Master Lecture";
			break;
			case "rsvp";
				$message = "RSVP";
			break;
			case "continuing_education_course";
				$message = "Continuing Education Course";
			break;
			case "continuing_education_group";
				$message = "Continuing Education Group";
			break;
			case "continuing_edcation";
				$message = "Continuing Education";
			break;
			default:
				$message = $notification_item_type;
		}

		return $message;
	}

	public static function buildNotificationMessage($notification_rec){
		$message = "";
		
		switch($notification_rec['notification_item_action']){
			case "WC" : $message .= "The checking request of "; break;
			case "AC" : $message .= "The checking request of "; break;
			case "WA" : $message .= "The approval request of "; break;
			case "AAS" : $message .= "The approval request of "; break;
			case "AAL" : $message .= "The approval request of "; break;
			case "WP": $message .= "The pending to launch of "; break;
			case "AC": $message .= "The checking request of "; break;
			case "RC": $message .= "The checking request of "; break;
			case "AAS": $message .= "The approval request of "; break;
			case "AAL": $message .= "The approval request of "; break;
			case "RA": $message .= "The approval request of "; break;
		}

		//$message .= self::getItemTypeName($notification_rec['notification_item_type']);
		if(self::getItemName($notification_rec['notification_item_type'], $notification_rec['notification_item_id'])){
		$message .= " '" . self::getItemName($notification_rec['notification_item_type'], $notification_rec['notification_item_id']) . "'";
		}
		
		switch($notification_rec['notification_item_action']){
			//case "CS": $message .= " is created and submitted for approval"; break;
			//case "ES": $message .= " is updated and submitted for approval"; break;
			case "CS": $message .= " is created"; break;
			case "ES": $message .= " is updated"; break;
			case "D": $message .= " is deleted"; break;
			case "DS": $message .= " is deleted and submitted for approval"; break;
			case "PC": $message .= " is pending for checking"; break;
			case "PA": $message .= " is pending for approval"; break;
			case "RO": $message .= " is rolled back"; break;
			case "WC": $message .= " is withdrawn"; break;
			case "WA": $message .= " is withdrawn"; break;
			case "WP": $message .= " is withdrawn"; break;
			case "UD": $message .= " is undeleted"; break;
			case "AC": $message .= " is approved"; break;
			case "RC": $message .= " is rejected"; break;
			case "AAS": $message .= " is approved and scheduled to launch"; break;
			case "AAL": $message .= " is approved and launched"; break;
			case "RA": $message .= " is rejected"; break;
			case "SL": $message .= " is launched by scheduler"; break;
			case "ML": $message .= " is launched"; break;
		}
		
		return $message;
	}

	public static function getItemName($item_type, $item_id){
		$item_type = strtok($item_type, '-');
		$item_type = strtolower(str_replace(' ', '', $item_type));
		switch($item_type){
			case "news": 
				$news_rec = DM::load('news', $item_id);
				return $news_rec['news_name_lang1'];
			break;
			case "programmes": 
				$programmes_rec = DM::load('programmes', $item_id);
				return $programmes_rec['programmes_name_lang1'];
			break;
			case "project": 
				$programmes_rec = DM::load('project', $item_id);
				return $programmes_rec['project_name_lang1'];
			break;
			case "universities": 
				$project_rec = DM::load('universities', $item_id);
				return $project_rec['universities_name_lang1'];
			break;
			case "top_up_degree": 
				$project_rec = DM::load('top_up_degree', $item_id);
				return $project_rec['top_up_degree_name_lang1'];
			break;
			case "master_index": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "master_detail": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "issue": 
				$project_rec = DM::load('issue', $item_id);
				return $project_rec['issue_name_lang1'];
			break;
			case "signed": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "exhibitons": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "edt": 
			case "hkdigallery": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "alumni": 
				$project_rec = DM::load('alumni', $item_id);
				return $project_rec['alumni_name_lang1'];
			break;
			case "department": 
				$project_rec = DM::load('dept', $item_id);
				return $project_rec['dept_name_lang1'];
			break;
			case "collaboration": 
				$project_rec = DM::load('collaboration', $item_id);
				return $project_rec['collaboration_name_lang1'];
			break;
			case "peec_programmes": 
				$project_rec = DM::load('peec_programmes', $item_id);
				return $project_rec['peec_programmes_name_lang1'];
			break;
			case "peec_menu": 
				$project_rec = DM::load('peec_menu', $item_id);
				return $project_rec['peec_menu_name_lang1'];
			break;
			case "peec_section": 
				$project_rec = DM::load('peec_section', $item_id);
				return $project_rec['peec_section_name_lang1'];
			break;
			case "rsvp": 
				$project_rec = DM::load('rsvp', $item_id);
				return $project_rec['rsvp_name_lang1'];
			break;
			case "studentaward": 
			case "student_award":
				$project_rec = DM::load('student_award', $item_id);
				if($project_rec['student_award_student_name1_lang1']){
					$name = $project_rec['student_award_student_name1_lang1'];
				}
				if($project_rec['student_award_student_name2_lang1']){
					if($name){
						$name .="|";
					}
					$name .= $project_rec['student_award_student_name2_lang1'];
				}
				if($project_rec['student_award_student_name3_lang1']){
					if($name){
						$name .="|";
					}
					$name .= $project_rec['student_award_student_name3_lang1'];
				}
				if($project_rec['student_award_student_name4_lang1']){
					if($name){
						$name .="|";
					}
					$name .= $project_rec['student_award_student_name4_lang1'];
				}
				return $name;
			break;
			case "knowledgecentre": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			case "master_lecture": 
				$project_rec = DM::load('master_lecture', $item_id);
				return $project_rec['master_lecture_name_lang1'];
			break;
			case "product": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "research": 
			break;
			case "financial":
			break;
			case "promotion": 
				$product_rec = DM::load('product', $item_id);
				return $product_rec['product_name_lang1'];
			break;
			case "talent": 
				$product_rec = DM::load('talent', $item_id);
				return $product_rec['talent_name_lang1'];
			break;
			case "attachment": 
				$product_rec = DM::load('international', $item_id);
				return $product_rec['international_name_lang1'];
			case "continuing_edcation": 
				$product_rec = DM::load('international', $item_id);
				return 'Continuing Education';
			case "continuing_education_group": 
				$product_rec = DM::load('international', $item_id);
				return 'Continuing Education Group';
			break;
			case "continuing_education_course": 
				$project_rec = DM::load('product', $item_id);
				return $project_rec['product_name_lang1'];
			break;
			case "international": 
				$product_rec = DM::load('international', $item_id);
				/*switch ($product_rec['international_type']) {
					case 'P':
						$type_name = "Exchange Programme - ";
						break;
					case 'O':
						$type_name = "Outgoing Students - ";
						break;
					case 'S':
						$type_name = "Scholarship - ";
						break;
					default:
						# code...
						break;
				}*/
				return ($type_name ?? '').$product_rec['international_name_lang1'];
			break;
			
			case "ipo": 
			break;
			case "faq_category": 
				$faq_category_rec = DM::load('faq_category', $item_id);
				return $faq_category_rec['faq_category_name_lang1'];
			break;
			case "faq": 
				$faq_rec = DM::load('faq', $item_id);
				return $faq_rec['faq_question_lang1'];
			break;
			case "promotion_banner": 
				$promotion_banner_rec = DM::load('promotion_banner', $item_id);
				return $promotion_banner_rec['promotion_banner_id'];
			break;
			case "peec_programmes_categories": 
				$promotion_banner_rec = DM::load('peec_programmes_categories', $item_id);
				return $promotion_banner_rec['peec_programmes_categories_name_lang1'];
			break;
			default:
				//return "Item Type: " . $approval_rec['approval_item_type'];
				return "";
		}
	}
}//class
?>