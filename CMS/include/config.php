<?php
	//error_reporting(E_ALL & ~E_NOTICE);
	//ini_set('display_errors', 'On');
	ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	error_reporting(0);

if (substr( $_SERVER['REMOTE_ADDR'], 0, 6 ) == '172.27'){
	echo "Please logon to VTC VPN.";
	exit;
}


	$backend_project_name ="Hong Kong Design Institute";
	$backend_client_name = "Hong Kong Design Institute - VTC Group";
	$backend_project_url = "http://".$_SERVER['HTTP_HOST']."/";
	$project_company_code = "hkdi";

	//FS
	$DB_HOST = "localhost";
	$DB_NAME = "hkdi3_db1";
	$DB_LOGIN = "hkdi3_db1";
	$DB_PASSWORD = "H!k@091204";

	/*
	//For UAT TEST
	$DB_HOST = "localhost";
	$DB_NAME = "tpcms_staging_uat_schema";
	$DB_LOGIN = "tpcmsstaging";
	$DB_PASSWORD = "tpcmsstaging";
	*/

	$remote_host ="seoweb02";
	$remote_user ="web";
	$remote_keyfile ="/home/web/.ssh/id_rsa";

	//path setting
	$SYSTEM_BASE = realpath(dirname(__FILE__) . "/../") . "/";
//	$SYSTEM_HOST = ($_SERVER["HTTPS"] == "" ? "http://" : "https://") . "" . $_SERVER['SERVER_NAME'] . "/tools/hkdi_v5/CMS/";
	$SYSTEM_HOST = ($_SERVER["HTTPS"] == "" ? "http://" : "https://") . "" . $_SERVER['SERVER_NAME'] . "/CMS/";
	
//	$root_path = "/client/firmstudioextranet/wwwroot/HKDI/wwwroot/";
//	$host_name = "http://".$_SERVER['HTTP_HOST']."/HKDI/wwwroot/";

//$root_path = "/home/hkdi/html/"; // CMS in Official path
$root_path = $SYSTEM_BASE; // CMS in Official path
// $host_name = "https://www.hkdi.edu.hk/"; // CMS in Official Site
$host_name = "https://hkdi3.bcde.digital/CMS/"; // CMS in Official Site
//$root_path = "/home/hkdi_staging/html/"; // CMS in Staging path
//$host_name = "http://172.30.24.85/"; // CMS in Staging Site
	
	
	//$SYSTEM_BASE = realpath(dirname(__FILE__) . "/../") . "/";
	//$PRODUCTION_SYSTEM_BASE = "/home/hkdi/html/";
	$PRODUCTION_SYSTEM_BASE = $SYSTEM_BASE;
	//$SYSTEM_HOST = "https://". $_SERVER['HTTP_X_FORWARDED_SERVER'] . "/hk-staging/CMS/";
	
	//$root_path = "/var/www/vhosts/os_staging_ETNET/hk-staging/";
	//$host_name = "https://".$_SERVER['HTTP_X_FORWARDED_SERVER']."/hk-staging/";
	

	$PRODUCTION_SYSTEM_BASE = "/";	


	$TEMP_UPLOAD_PATH = $SYSTEM_BASE . "uploaded_files/tmp/";
	$TEMP_UPLOAD_URL = $SYSTEM_HOST . "uploaded_files/tmp/";
	
	$programmes_path = $root_path."uploaded_files/programmes/";
	$programmes_url = $host_name."uploaded_files/programmes/";
	
	$news_path = $root_path."uploaded_files/news/";
	$news_url = $host_name."uploaded_files/news/";

	$rsvp_path = $root_path."uploaded_files/rsvp/";
	$rsvp_url = $host_name."uploaded_files/rsvp/";
	
	$project_path = $root_path."uploaded_files/project/";
	$project_url = $host_name."uploaded_files/project/";
	
	$issue_path = $root_path."uploaded_files/issue/";
	$issue_url = $host_name."uploaded_files/issue/";
	
	$signed_path = $root_path."uploaded_files/signed/";
	$signed_url = $host_name."uploaded_files/signed/";

	$universities_path = $root_path."uploaded_files/universities/";
	$universities_url = $host_name."uploaded_files/universities/";

	$top_up_degree_path = $root_path."uploaded_files/top_up_degree/";
	$top_up_degree_url = $host_name."uploaded_files/top_up_degree/";

	$product_path = $root_path."uploaded_files/product/";
	$product_url = $host_name."uploaded_files/product/";

	$student_award_path = $root_path."uploaded_files/student_award/";
	$student_award_url = $host_name."uploaded_files/student_award/";

	$alumni_path = $root_path."uploaded_files/alumni/";
	$alumni_url = $host_name."uploaded_files/alumni/";
	
	$product_file_path = $root_path."uploaded_files/product_file/";
	$product_file_url = $host_name."uploaded_files/product_file/";
	
	$new_collaboration_file_path = $root_path."uploaded_files/collaboration_file/";
	$new_collaboration_file_url = $host_name."uploaded_files/collaboration_file/";

	$continuing_edcation_path = $root_path."uploaded_files/continuing_edcation/";
	$continuing_edcation_url = $host_name."uploaded_files/continuing_edcation/";

	$international_path = $root_path."uploaded_files/international/";
	$international_url = $host_name."uploaded_files/international/";

	$collaboration_path = $root_path."uploaded_files/collaboration/";
	$collaboration_url = $host_name."uploaded_files/collaboration/";

	$new_collaboration_path = $root_path."uploaded_files/collaboration/";
	$new_collaboration_url = $host_name."uploaded_files/collaboration/";

	$master_lecture_path = $root_path."uploaded_files/master_lecture/";
	$master_lecture_url = $host_name."uploaded_files/master_lecture/";

	$dept_path = $root_path."uploaded_files/dept/";
	$dept_url = $host_name."uploaded_files/dept/";

	$talent_path = $root_path."uploaded_files/talent/";
	$talent_url = $host_name."uploaded_files/talent/";

	$global_learning_path = $root_path."uploaded_files/global_learning/";
	$global_learning_url = $host_name."uploaded_files/global_learning/";

	$your_support_matters_path = $root_path."uploaded_files/your_support_matters/";
	$your_support_matters_url = $host_name."uploaded_files/your_support_matters/";

	$scholarship_path = $root_path."uploaded_files/scholarship/";
	$scholarship_url = $host_name."uploaded_files/scholarship/";

	$international_academic_partners_path = $root_path."uploaded_files/international_academic_partners/";
	$international_academic_partners_url = $host_name."uploaded_files/international_academic_partners/";

	$international_academic_partners_section_path = $root_path."uploaded_files/international_academic_partners_section/";
	$international_academic_partners_section_url = $host_name."uploaded_files/international_academic_partners_section/";


	$publications_programmes_path = $root_path."uploaded_files/publications_programmes/";
	$publications_programmes_url = $host_name."uploaded_files/publications_programmes/";

	$peec_menu_path = $root_path."uploaded_files/peec_menu/";
	$peec_menu_url = $host_name."uploaded_files/peec_menu/";

	$peec_menu_section_path = $root_path."uploaded_files/peec_menu_section/";
	$peec_menu_section_url = $host_name."uploaded_files/peec_menu_section/";

	$peec_menu_section_file_path = $root_path."uploaded_files/peec_menu_section_file/";
	$peec_menu_section_file_url = $host_name."uploaded_files/peec_menu_section_file/";
	
	$peec_path = $root_path."uploaded_files/peec/";
	$peec_url = $host_name."uploaded_files/peec/";

	$peec_section_path = $root_path."uploaded_files/peec_section/";
	$peec_section_url = $host_name."uploaded_files/peec_section/";

	$peec_programmes_path = $root_path."uploaded_files/peec_programmes/";
	$peec_programmes_url = $host_name."uploaded_files/peec_programmes/";

	$peec_programmes_section_path = $root_path."uploaded_files/peec_programmes_section/";
	$peec_programmes_section_url = $host_name."uploaded_files/peec_programmes_section/";

	$peec_programmes_section_file_path = $root_path."uploaded_files/peec_programmes_section_file/";
	$peec_programmes_section_file_url = $host_name."uploaded_files/peec_programmes_section_file/";

	$peec_section_file_path = $root_path."uploaded_files/peec_section_file/";
	$peec_section_file_url = $host_name."uploaded_files/peec_section_file/";

	$peec_programmes_categories_path = $root_path."uploaded_files/peec_programmes_categories/";
	$peec_programmes_categories_url = $host_name."uploaded_files/peec_programmes_categories/";


	
	
	$HASHING_KEY = "Mu4rem25aVax5ChaqezaV2xUzuVeqU";
	
	define("STATUS_ENABLE", "1");
	define("STATUS_DISABLE", "0");
	
	$SYSTEM_USER_TYPE['A'] = "Administrator";
	$SYSTEM_USER_TYPE['O'] = "Operator";
	
/*
	//$SYSTEM_EMAIL_ADDRESS = "donotreply@shkf.com";	
	$SYSTEM_SMTP_SERVER = "smtp.gmail.com";
	$smtp_port = "587";
	$smtp_require_auth = true;
	$smtp_require_ssl = true;
	$smtp_username = "onlineform.support@firmstudio.com";
	$smtp_password = "tectectec";
*/

$SYSTEM_EMAIL_ADDRESS = "hkdi-ereply@vtc.edu.hk";
$SYSTEM_EMAIL_DISPLAY_NAME ="HKDI System Emailbox";

$SYSTEM_SMTP_SERVER = $DB_NAME === 'hkdidevelopment' ? "" : "smtp.vtc.edu.hk";
$smtp_port = "25";
$smtp_require_auth = true;
$smtp_require_ssl = false;
$smtp_username = "hkdi-ereply@vtc.edu.hk";
$smtp_password = "EAObm2018a"."2010";
$smtp_security = "tls";

	$ALLOWED_IMAGE_FORMAT = array("jpg", "jpeg", "png", "gif");
	$ALLOWED_IMAGE_FORMAT_MIME = array("image/jpg", "image/jpeg", "image/png", "image/gif");
	
	$MENU_BANNER_WIDTH = 700;
	$MENU_BANNER_HEIGHT = 50;
	
	$SMALL_BANNER_WIDTH = 700;
	$SMALL_BANNER_HEIGHT = 50;
	
	$NEWS_IMAGE_WIDTH = 700;
	$NEWS_IMAGE_HEIGHT = 50;
	
	$IPO_BANNER_WIDTH = 700;
	$IPO_BANNER_HEIGHT = 50;
	
	$PRODUCT_BANNER_WIDTH = 700;
	$PRODUCT_BANNER_HEIGHT = 50;
	
	$PRODUCT_SMALL_BANNER_WIDTH = 700;
	$PRODUCT_SMALL_BANNER_HEIGHT = 50;
	
	$MAIN_BANNER_WIDTH = 700;
	$MAIN_BANNER_HEIGHT = 50;
	
	$MAIN_BANNER_ICON_WIDTH = 700;
	$MAIN_BANNER_ICON_HEIGHT = 50;
	
	$SMALL_BANNER_WIDTH = 700;
	$SMALL_BANNER_HEIGHT = 50;
	
	$PRODUCTION_SERVER_LIST = array();
	$PRODUCTION_SERVER_LIST[] = array("user" => "webadm", "port" => 22, "keyPath" => "include/ssh_key/id_rsa", "host" => "192.168.2.101", "remotePath" => "/opt/hkmc_cms/app/hkmc_prod_remote");
	
	function errHandle($errNo, $errStr, $errFile, $errLine) {
		$msg = "$errStr <br/>in $errFile on line $errLine";
		if ($errNo == E_WARNING) {
			die($msg);
		}
	}

	set_error_handler('errHandle');
	
	$PRODUCT_SHORTCUT_ICONS = array();
	$PRODUCT_SHORTCUT_ICONS["Assessment"] = "icon-assessment.png";
	$PRODUCT_SHORTCUT_ICONS["Calculator"] = "icon-calculator.png";
	$PRODUCT_SHORTCUT_ICONS["Enquiry"] = "icon-enquiry.png";
	$PRODUCT_SHORTCUT_ICONS["Hand"] = "icon-hand.png";
	$PRODUCT_SHORTCUT_ICONS["Mentor"] = "icon-mentor.png";
	$PRODUCT_SHORTCUT_ICONS["Mircofilm"] = "icon-mircofilm.png";
	$PRODUCT_SHORTCUT_ICONS["Pdf"] = "icon-pdf.png";
	$PRODUCT_SHORTCUT_ICONS["Reference"] = "icon-reference.png";
	$PRODUCT_SHORTCUT_ICONS["Tip"] = "icon-tip.png";
	$PRODUCT_SHORTCUT_ICONS["TV"] = "icon-tv.png";
	
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => 'HKDI News', 'lang3' => 'HKDI News'),
	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => 'Student Award', 'lang3' => 'Student Award'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => 'Events', 'lang3' => 'Events'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => 'Public Events', 'lang3' => 'Public Events'),
	array('value' => 'collaborative_projects', 'lang1' => 'Collaborative Projects', 'lang2' => 'Collaborative Projects', 'lang3' => 'Collaborative Projects')
); // sync with wwwroot/include/variable.php

$exhibiton_types = array(
	array('value' => 0, 'key' => 'communication_design', 'lang1' => 'Communication Design', 'lang2' => 'Communication Design', 'lang3' => 'Communication Design'),
	array('value' => 1, 'key' => 'fashion', 'lang1' => 'Fashion', 'lang2' => 'Fashion', 'lang3' => 'Fashion'),
	array('value' => 2, 'key' => 'multi_disciplinary', 'lang1' => 'Multi-disciplinary', 'lang2' => 'Multi-disciplinary', 'lang3' => 'Multi-disciplinary'),
	array('value' => 3, 'key' => 'photography_film', 'lang1' => 'Photography/ Film', 'lang2' => 'Photography/ Film', 'lang3' => 'Photography/ Film'),
	array('value' => 4, 'key' => 'product', 'lang1' => 'Product', 'lang2' => 'Product', 'lang3' => 'Product'),
    array('value' => 5, 'key' => 'spatial', 'lang1' => 'Spatial', 'lang2' => 'Spatial', 'lang3' => 'Spatial')
); // sync with wwwroot/CMS/include/config.php // sync with wwwroot/CMS/include/config.php

$news_id = '';
?>