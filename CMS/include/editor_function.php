<?php
	function generateEditor($editor_name, $width = 600, $height = 400){
?>
	$('<?php echo $editor_name ?>').tinymce({
		// Location of TinyMCE script
		script_url : '../js/tinymce/tinymce.min.js',
		menubar: "tools table format view insert edit",
		schema: "html5",
		width: "<?php echo $width ?>",
		height: "<?php echo $height ?>",
		menubar : false,
		statusbar : false,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste acheck"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table ",
		image_advtab: true,
		relative_urls: false,
		remove_script_host : false,
		convert_urls : true
	});
<?php
	}
	function generateEditorTemplate($editor_name, $width = 600, $height = 400){
?>
	$(<?php echo $editor_name ?>).tinymce({
		// Location of TinyMCE script
		script_url : '../js/tinymce/tinymce.min.js',
		menubar: "tools table format view insert edit",
		schema: "html5",
		width: "<?php echo $width ?>",
		height: "<?php echo $height ?>",
		menubar : false,
		statusbar : false,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste acheck"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table ",
		image_advtab: true,
		relative_urls: false,
		remove_script_host : false,
		convert_urls : true
	});
<?php
	}
?>