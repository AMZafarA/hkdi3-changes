<?php

require_once( __DIR__ . '/config.php' );
require_once( __DIR__ . '/classes/DataMapper.php' );
class RegenerateSitemap {
	public static function regen( $dbcon = array() ) {
		DM::setup($dbcon[0], $dbcon[1], $dbcon[2], $dbcon[3]);

		$directory = '../../';
		$exclude_dir = array( '~en' , 'CMS' , 'css' , 'images' , 'include' , 'js' , 'old' , 'pdf' , 'phpMyAdmin-5.1.1' , 'text_mask' , 'uploaded_files' , 'videos', 'inc_common' , 'inc_home' );
		$exclude_files = array( 'sitemap-generator.php' , 'sitemap-tables.json' , 'sql' , 'bkp' , 'htaccess', 'bk2' , 'news-detail.php' , 'publication.php', 'publication-detail.php' , 'gallery.php' , 'exchange-programme-details.php' , 'outgoing-student-details.php' , 'award.php' , 'programme.php' , '.js' , '.css', 'index_error' );
		$xml = "";

		$filter = function ( $file , $key , $iterator ) use ( $exclude_dir , $exclude_files ) {
			if ( $iterator->hasChildren() && !in_array($file->getFilename(), $exclude_dir)) {
				return true;
			}
			// if( in_array( $file->getFilename() , $exclude_files) ) {
			// 	return false;
			// }
			foreach( $exclude_files as $ef ) {
				if( in_array( $file->getFilename() , $exclude_files) || str_contains( strtolower( $file->getFilename() ) , strtolower( $ef ) ) ) {
					return false;
				}
			}
			return $file->isFile();
		};

		$innerIterator = new RecursiveDirectoryIterator( $directory, RecursiveDirectoryIterator::SKIP_DOTS );
		$iterator = new RecursiveIteratorIterator( new RecursiveCallbackFilterIterator( $innerIterator , $filter ) );

		function url() {
			return ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' ) . '://' . $_SERVER['SERVER_NAME'];
		}
		function renameFile($filename) {
			$ff = $filename;
			$ff = str_replace( array( "./" , "index.php/" , "index.php" , "index.html/" , "index.html" ) , "" , $ff );
			$ff = str_replace( array( "//" , ".." ) , "/" , $ff );
			$ff = htmlentities( $ff );
			return $ff;
		}

		$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

		foreach ($iterator as $pathname => $fileInfo) {
			$xml .= '<url>';
			$xml .= '<loc>' . url() . renameFile( $pathname ) . '/</loc>';
			$xml .= '<lastmod>' . date( DATE_ATOM , time() ) . '</lastmod>';
			$xml .= '<changefreq>daily</changefreq>';
			$xml .= '<priority>1.0</priority>';
			$xml .= '</url>';
		}

		$sm_file = __DIR__ . "/sitemap-tables.json";
		$json = str_replace( "\t", "", str_replace( "\n", " ", file_get_contents( $sm_file ) ) );
		$meta = json_decode( $json );
		$langs = ["en","tc","sc"];

		foreach ($meta as $j) {
			$table = $j->p_table;
			$condition = $j->col_label . "_publish_status = ?";
			$parameters = array();
			$orderBy = ""; 

			$parameters[] = "AL";
			if( $j->conditions ) {
				foreach ( $j->conditions as $ck => $cv ) {
					$condition .= " AND " . $j->col_label . "_" . $ck . " = ?";
					$parameters[] = $cv;
				}
			}

			$results = DM::findAll( $table , $condition , $orderBy , $parameters );
			foreach($results as $result) {
				foreach ($langs as $lang) {
					$xml .= '<url>';
					if( $j->is_slug ) {
						$xml .= '<loc>' . url() . "/" . $lang . "/" . $j->frontend_url . $j->col_label . '</loc>';
					}
					else {
						$xml .= '<loc>' . url() . "/" . $lang . "/" . $j->frontend_url . "?" . $j->parameter_identifier . "=" . $result[$j->col_label . "_id"] . '</loc>';
					}
					$xml .= '<lastmod>' . date( DATE_ATOM , time() ) . '</lastmod>';
					$xml .= '<changefreq>daily</changefreq>';
					$xml .= '<priority>1.0</priority>';
					$xml .= '</url>';
				}
			}
		} 

		$xml .= '</urlset>';

		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = FALSE;
		$dom->loadXML($xml);

		$dom->save('../../sitemap.xml');

		

	}
}

?>