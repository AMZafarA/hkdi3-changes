<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="top_up_degree" )

	{

		$id = $_REQUEST['top_up_degree_id'] ?? '';
		$top_up_degree_lv1 = $_REQUEST['top_up_degree_lv1'] ?? '';
		$top_up_degree_lv2 = $_REQUEST['top_up_degree_lv2'] ?? '';

		$type_name = "top_up_degree";

		if( isset($type_namelv1) ){ $type_name .= " - ".$type_namelv1;}
		if( isset($type_namelv2) ){ $type_name .= " - ".$type_namelv2;}

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "top_up_degree")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['top_up_degree_publish_status'] = "D";

				$values['top_up_degree_status'] = STATUS_ENABLE;

				$values['top_up_degree_created_by'] = $_SESSION['sess_id'];

				$values['top_up_degree_created_date'] = $timestamp;

				$values['top_up_degree_updated_by'] = $_SESSION['sess_id'];

				$values['top_up_degree_updated_date'] = $timestamp;


				$id = DM::insert('top_up_degree', $values);


				if(!file_exists($top_up_degree_path . $id. "/")){
					mkdir($top_up_degree_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('top_up_degree', $id);



				if($ObjRec['top_up_degree_status'] == STATUS_DISABLE)

				{

					header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=F-1002");

					return;

				}


				$values['top_up_degree_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['top_up_degree_updated_by'] = $_SESSION['sess_id'];

				$values['top_up_degree_updated_date'] = $timestamp;



				DM::update('top_up_degree', $values);

			}


			$ObjRec = DM::load('top_up_degree', $id);

			$top_up_degree_img_arr = array('top_up_degree_image1','top_up_degree_image2');

			foreach($top_up_degree_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($top_up_degree_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($top_up_degree_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($top_up_degree_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


						list($width, $height) = getimagesize($top_up_degree_path . $id . "/".$newname);

						if($width > '1000'){
							resizeImageByWidth($top_up_degree_path. $id."/". $newname, "1000" , $top_up_degree_path. $id."/". $newname);
						}else if($height > '500'){
							resizeImageByHeight($top_up_degree_path. $id."/". $newname, "500" , $top_up_degree_path. $id."/". $newname);
						}




					$values = array();
					$values[$val] = $newname;
					$values['top_up_degree_id'] = $id;

					DM::update('top_up_degree', $values);
					unset($values);

					$ObjRec = DM::load('top_up_degree', $id);
				}
			}


			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "top_up_degree '" . $ObjRec['top_up_degree_name_lang1'] . "' (ID:" . $ObjRec['top_up_degree_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "top_up_degree", $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "top_up_degree",$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['top_up_degree_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "top_up_degree", $isNew ? "CS" : "ES");
				$files = array($host_name."en/top_up_degree/top_up_degree.php", $host_name."tc/top_up_degree/top_up_degree.php", $host_name."sc/top_up_degree/top_up_degree.php");

				$approval_id = ApprovalService::createApprovalRequest( "top_up_degree", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "top_up_degree", "PA", $approval_id);

			}

			header("location:top_up_degree.php?top_up_degree_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('top_up_degree', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['top_up_degree_status'] == STATUS_DISABLE)

			{

				header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("top_up_degree", $id);

			$values['top_up_degree_id'] = $id;
			$values['top_up_degree_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['top_up_degree_status'] = STATUS_DISABLE;
			$values['top_up_degree_updated_by'] = $_SESSION['sess_id'];
			$values['top_up_degree_updated_date'] = getCurrentTimestamp();


			DM::update('top_up_degree', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("top_up_degree", $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "top_up_degree '" . $ObjRec['top_up_degree_name_lang1'] . "' (ID:" . $ObjRec['top_up_degree_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "top_up_degree", "DS");

				$files = array($host_name."en/top_up_degree/top_up_degree.php", $host_name."tc/top_up_degree/top_up_degree.php", $host_name."sc/top_up_degree/top_up_degree.php");

				$approval_id = ApprovalService::createApprovalRequest($type_name, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "top_up_degree", "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "top_up_degree", "D");
			}





			header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('top_up_degree', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['top_up_degree_status'] == STATUS_ENABLE || $ObjRec['top_up_degree_publish_status'] == 'AL')

			{

				header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=F-1006");

				return;

			}



			$values['top_up_degree_id'] = $id;

			$values['top_up_degree_publish_status'] = 'D';

			$values['top_up_degree_status'] = STATUS_ENABLE;

			$values['top_up_degree_updated_by'] = $_SESSION['sess_id'];

			$values['top_up_degree_updated_date'] = getCurrentTimestamp();



			DM::update('top_up_degree', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'top_up_degree', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of top_up_degree '" . $ObjRec['top_up_degree_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $type_name, "WA");
			}



			Logger::log($_SESSION['sess_id'], $timestamp, "top_up_degree '" . $ObjRec['top_up_degree_name_lang1'] . "' (ID:" . $ObjRec['top_up_degree_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $type_name, "UD");

			header("location: list.php?top_up_degree_lv1=".$top_up_degree_lv1."&top_up_degree_lv2=".$top_up_degree_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
