<?php
include_once realpath(dirname(__FILE__) . "/include/config.php");
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

if(session_id() == "")
	session_start();
?>
<link href="<?php echo $SYSTEM_HOST ?>css/header.css" rel="stylesheet">
<!-- Fixed navbar -->
<div class="header-holder">
	<div id="header" class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="bg-holder">
			<img src="<?php echo $SYSTEM_HOST ?>images/banner.jpg" class="bg">
		</div>
		<div class="title">
			<div class="container" style="width: 1060px; padding-left: 0px; padding-right: 0px;display: inline-block;text-align: left;">
				<div class="left">
					<h1>Content Management System</h1>
					<p><?php echo $backend_client_name?></p>
				</div>
				<div class="right">
					<p>Hello, <?php echo $_SESSION['sess_name'] ?><br/>Last Login: <?php echo $_SESSION['sess_last_login'] ?></p>
				</div>
			</div>
		</div>
		<div class="container" style="width: 1100px; padding-left: 0px; padding-right: 0px;">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href="<?php echo $SYSTEM_HOST ?>dashboard.php">
							<i class="fa fa-tachometer fa-lg"></i> Dashboard
						</a>
					</li>


					<li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-list fa-lg"></i> Data Records<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'programmes_')||PermissionUtility::canEditByKey($_SESSION['sess_id'], 'ipo')||PermissionUtility::canEditByKey($_SESSION['sess_id'], 'small_banner')||PermissionUtility::canEditByKey($_SESSION['sess_id'], 'news')) {} ?>

							<?php if(PermissionUtility::isAdminGroup($_SESSION['sess_id'])){ ?>
								<li class="dropdown-submenu">
									<a href="javascript:void(0);">
										<i class="fa fa-file-text-o fa-lg"></i> Department
									</a>
									<ul class="dropdown-menu">

										<?php
										$lv1_cat_list = DM::findAll('dept', ' dept_status = ?  and dept_parent_id is ?', 'dept_seq', array(STATUS_ENABLE,null));

										foreach($lv1_cat_list as $lv1_cat){
											?>

											<li class="dropdown-submenu">
												<a href="javascript:void(0);">
													<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv1_cat['dept_name_lang1']?>
												</a>
												<ul class="dropdown-menu">
													<?php
													$lv2_cat_list = DM::findAll('dept', ' dept_status = ?  and dept_parent_id = ?', 'dept_seq', array(STATUS_ENABLE,$lv1_cat['dept_id']));
													foreach($lv2_cat_list as $lv2_cat){

														$dept_menu_link = $lv2_cat['dept_menu_link'];
														$dept_menu_key = $lv2_cat['dept_menu_key'];
														?>
														<li>
															<a href="<?php echo $SYSTEM_HOST ?>dept/dept.php?dept_id=<?php echo $lv2_cat['dept_id']?>">
																<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv2_cat['dept_name_lang1']?>
															</a>
														</li>
													<?php } ?>
												</ul>
											</li>
										<?php } ?>
									</ul>
								</li>
							<?php } ?>

							<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'programmes_') || PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'continuing_edcation') || PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'top_up_degree') || PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'master') ) { ?>
								<li class="dropdown-submenu">
									<a href="javascript:void(0);">
										<i class="fa fa-file-text-o fa-lg"></i> Programmes
									</a>
									<ul class="dropdown-menu">

										<?php
										for ($i=1; $i <= 4; $i++) {
											?>
											<?php
											$lv1_cat_list = DM::findAll('department', ' department_status = ?  and department_id = ?', 'department_seq', array(STATUS_ENABLE,$i));

											foreach($lv1_cat_list as $lv1_cat){
												if($i==1){
													if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'programmes_'.$lv1_cat['department_id'].'_') || $lv1_cat['department_id'] == '16' || $lv1_cat['department_id'] == '21') {
														continue;
													}
												}else if($i==2){
													if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'top_up_degree')) {
														continue;
													}
												}else if($i==3){
													if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'master')) {
														continue;
													}
												}else if($i==4){
													if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'continuing_edcation')) {
														continue;
													}
												}

												?>

												<li class="dropdown-submenu">
													<a href="javascript:void(0);">
														<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv1_cat['department_name_lang1']?>
													</a>
													<ul class="dropdown-menu">
														<?php
														$lv2_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id = ?', 'department_seq', array(STATUS_ENABLE,$lv1_cat['department_id']));
														foreach($lv2_cat_list as $lv2_cat){
															if($i==1){
																if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'programmes_'.$lv1_cat['department_id'].'_'.$lv2_cat['department_id'])) {
																	continue;
																}
															}else if($i==2){
																if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'top_up_degree')) {
																	continue;
																}
															}else if($i==3){
																if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'master')) {
																	continue;
																}
															}else if($i==4){
																if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'continuing_edcation')) {
																	continue;
																}
															}
															$department_menu_link = $lv2_cat['department_menu_link'];
															$department_menu_key = $lv2_cat['department_menu_key'];
															?>
															<li>
																<a href="<?php echo $SYSTEM_HOST ?><?php echo $department_menu_link?>?<?php echo $department_menu_key?>_lv1=<?php echo $lv1_cat['department_id']?>&<?php echo $department_menu_key?>_lv2=<?php echo $lv2_cat['department_id']?>">
																	<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv2_cat['department_name_lang1']?>
																</a>
															</li>
														<?php } ?>
													</ul>
												</li>
											<?php } }?>



										</ul>
									</li>
								<?php } ?>


								<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'news_')) { ?>
									<li class="dropdown-submenu">
										<a href="javascript:void(0);">
											<i class="fa fa-newspaper-o fa-lg"></i> News
										</a>
										<ul class="dropdown-menu">

											<?php
											$dep_arr = array('1','4','16','21','25');
											foreach ($dep_arr as $key => $value) {
												?>

												<?php
												$lv1_cat_list = DM::findAll('department', ' department_status = ?  and department_id = ?', 'department_seq', array(STATUS_ENABLE,$value));
												foreach($lv1_cat_list as $lv1_cat){

													if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'news_'.$lv1_cat['department_id'].'_')) {
														continue;
													}



													if($value == '1' || $value == '16'){
														?>

														<li class="dropdown-submenu">
															<a href="javascript:void(0);">
																<i class="fa fa-newspaper-o fa-lg"></i> <?php echo $lv1_cat['department_name_lang1']?>
															</a>
															<ul class="dropdown-menu">
																<?php
																$lv2_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id = ?', 'department_seq', array(STATUS_ENABLE,$lv1_cat['department_id']));
																foreach($lv2_cat_list as $lv2_cat){
																	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'news_'.$lv1_cat['department_id'].'_'.$lv2_cat['department_id'])) {
																		continue;
																	}
																	?>
																	<li>
																		<a href="<?php echo $SYSTEM_HOST ?>news/list.php?news_lv1=<?php echo $lv1_cat['department_id']?>&news_lv2=<?php echo $lv2_cat['department_id']?>">
																			<i class="fa fa-newspaper-o fa-lg"></i> <?php echo $lv2_cat['department_name_lang1']?>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</li>
													<?php } ?>


													<?php
													if($lv1_cat['department_id'] =='4' || $lv1_cat['department_id'] =='21' || $lv1_cat['department_id'] =='25'){


														?>

														<li>
															<a href="<?php echo $SYSTEM_HOST ?>news/list.php?news_lv1=<?php echo $lv1_cat['department_id']?>&news_lv2=">
																<i class="fa fa-newspaper-o fa-lg"></i> <?php echo $lv1_cat['department_name_lang1']?>
															</a>
														</li>
													<?php } }} ?>



												</ul>
											</li>
										<?php } ?>

		<?php /*if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'news')) { ?>
			<li>
				<a href="<?php echo $SYSTEM_HOST ?>news_type/list.php">
					<i class="fa fa-newspaper-o fa-lg"></i>HKDI
				</a>
			</li>
		<?php } */?>

		<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'project_')) { ?>
			<li class="dropdown-submenu">
				<a href="javascript:void(0);">
					<i class="fa fa-file-text-o fa-lg"></i> Project
				</a>
				<ul class="dropdown-menu">

					<?php
					$lv1_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id is ?', 'department_seq', array(STATUS_ENABLE,null));
					foreach($lv1_cat_list as $lv1_cat){
						if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'project_'.$lv1_cat['department_id'].'_')) {
							continue;
						}

						if($lv1_cat['department_id'] =='1' || $lv1_cat['department_id'] =='16'){
							?>

							<li class="dropdown-submenu">
								<a href="javascript:void(0);">
									<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv1_cat['department_name_lang1']?>
								</a>
								<ul class="dropdown-menu">
									<?php
									$lv2_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id = ?', 'department_seq', array(STATUS_ENABLE,$lv1_cat['department_id']));
									foreach($lv2_cat_list as $lv2_cat){
										if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'project_'.$lv1_cat['department_id'].'_'.$lv2_cat['department_id'])) {
											continue;
										}
										?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>project/list.php?project_lv1=<?php echo $lv1_cat['department_id']?>&project_lv2=<?php echo $lv2_cat['department_id']?>">
												<i class="fa fa-file-text-o fa-lg"></i> <?php echo $lv2_cat['department_name_lang1']?>
											</a>
										</li>
									<?php } ?>
								</ul>
							</li>
						<?php } } ?>



					</ul>
				</li>
			<?php } ?>

			<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'signed')) { ?>
				<li>
					<a href="<?php echo $SYSTEM_HOST ?>issue/list.php">
						<i class="fa fa-book fa-lg"></i>Signed
					</a>
				</li>
			<?php } ?>

			<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'publications_programmes')) { ?>
				<li>
					<a href="<?php echo $SYSTEM_HOST ?>publications_programmes/list.php">
						<i class="fa fa-book fa-lg"></i>Publications - programmes
					</a>
				</li>
			<?php } ?>

			<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'student_award_')) { ?>
				<li class="dropdown-submenu">
					<a href="javascript:void(0);">
						<i class="fa fa-trophy"></i> Student Award
					</a>
					<ul class="dropdown-menu">

						<?php
						$lv1_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id is ?', 'department_seq', array(STATUS_ENABLE,null));
						foreach($lv1_cat_list as $lv1_cat){
							if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'student_award_'.$lv1_cat['department_id'].'_')) {
								continue;
							}

							if($lv1_cat['department_id'] =='1'){
								?>

								<li class="dropdown-submenu">
									<a href="javascript:void(0);">
										<i class="fa fa-trophy"></i> <?php echo $lv1_cat['department_name_lang1']?>
									</a>
									<ul class="dropdown-menu">
										<?php
										$lv2_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id = ?', 'department_seq', array(STATUS_ENABLE,$lv1_cat['department_id']));
										foreach($lv2_cat_list as $lv2_cat){
											if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'student_award_'.$lv1_cat['department_id'].'_'.$lv2_cat['department_id'])) {
												continue;
											}
											?>
											<li>
												<a href="<?php echo $SYSTEM_HOST ?>student_award/list.php?student_award_lv1=<?php echo $lv1_cat['department_id']?>&student_award_lv2=<?php echo $lv2_cat['department_id']?>">
													<i class="fa fa-trophy"></i> <?php echo $lv2_cat['department_name_lang1']?>
												</a>
											</li>
										<?php } ?>
									</ul>
								</li>
							<?php } } ?>



						</ul>
					</li>
				<?php } ?>


				<?php if(PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'alumni_')) { ?>
					<li class="dropdown-submenu">
						<a href="javascript:void(0);">
							<i class="fa fa-user-circle-o "></i> Alumni
						</a>
						<ul class="dropdown-menu">

							<?php
							$lv1_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id is ?', 'department_seq', array(STATUS_ENABLE,null));
							foreach($lv1_cat_list as $lv1_cat){
								if(!PermissionUtility::canEditByKeyPrefix($_SESSION['sess_id'], 'alumni_'.$lv1_cat['department_id'].'_')) {
									continue;
								}

								if($lv1_cat['department_id'] =='1'){
									?>

									<li class="dropdown-submenu">
										<a href="javascript:void(0);">
											<i class="fa fa-user-circle-o "></i> <?php echo $lv1_cat['department_name_lang1']?>
										</a>
										<ul class="dropdown-menu">
											<?php
											$lv2_cat_list = DM::findAll('department', ' department_status = ?  and department_parent_id = ?', 'department_seq', array(STATUS_ENABLE,$lv1_cat['department_id']));
											foreach($lv2_cat_list as $lv2_cat){
												if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'alumni_'.$lv1_cat['department_id'].'_'.$lv2_cat['department_id'])) {
													continue;
												}
												?>
												<li>
													<a href="<?php echo $SYSTEM_HOST ?>alumni/list.php?alumni_lv1=<?php echo $lv1_cat['department_id']?>&alumni_lv2=<?php echo $lv2_cat['department_id']?>">
														<i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo $lv2_cat['department_name_lang1']?>
													</a>
												</li>
											<?php } ?>
										</ul>
									</li>
								<?php } } ?>



							</ul>
						</li>
					<?php } ?>


					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'cimt') ||
						PermissionUtility::canEditByKey($_SESSION['sess_id'], 'desis_lab') ||
						PermissionUtility::canEditByKey($_SESSION['sess_id'], 'media_lab') ||
						PermissionUtility::canEditByKey($_SESSION['sess_id'], 'fashion_archive') ||
						PermissionUtility::canEditByKey($_SESSION['sess_id'], 'cdss')||
						PermissionUtility::canEditByKey($_SESSION['sess_id'], 'ccd')) { ?>

							<li class="dropdown-submenu">
								<a href="javascript:void(0);">
									<i class="fa fa-university "></i> Knowledge Centre
								</a>
								<ul class="dropdown-menu">
									<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'cimt')){ ?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>knowledge/cimt.php">
												<i class="fa fa-university "></i> Centre of Innovative Material and Technology (CIMT)
											</a>
										</li>
									<?php } ?>

									<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'desis_lab')){ ?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>knowledge/desis_lab.php">
												<i class="fa fa-university "></i> HKDI DESIS Lab
											</a>
										</li>
									<?php } ?>

									<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'media_lab')){ ?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>knowledge/media_lab.php">
												<i class="fa fa-university "></i> Media Lab
											</a>
										</li>
									<?php } ?>

									<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'fashion_archive')){ ?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>knowledge/fashion_archive.php">
												<i class="fa fa-university "></i> HKDI Fashion Archive
											</a>
										</li>
									<?php } ?>

									<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'ccd')){ ?>
										<li>
											<a href="<?php echo $SYSTEM_HOST ?>knowledge/ccd.php">
												<i class="fa fa-university "></i> CCD
											</a>
										</li>
									<?php } ?>
								</li>
								<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'cdss')){ ?>
									<li>
										<a href="<?php echo $SYSTEM_HOST ?>knowledge/cdss.php">
											<i class="fa fa-university "></i> Centre of Design Services and Solutions
										</a>
									</li>
								<?php } ?>
							</li>
						</ul>
					<?php }  ?>


					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'exhibitons')) { ?>

						<li class="dropdown-submenu">
							<a href="javascript:void(0);">
								<i class="fa fa-picture-o "></i> Gallery
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>exhibitons/list.php">
										<i class="fa fa-picture-o "></i> Exhibitons
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>gallery/gallery.php">
										<i class="fa fa-university "></i> HKDI Gallery
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>gallery/d_mart.php">
										<i class="fa fa-university "></i> d-Mart
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>gallery/center.php">
										<i class="fa fa-university "></i> Experience Center
									</a>
								</li>
							</ul>
						</li>
					<?php }  ?>


					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'international')) { ?>

						<li class="dropdown-submenu">
							<a href="javascript:void(0);">
								<i class="fa fa-newspaper-o fa-lg"></i> International
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>international/list.php?international_type=P">
										<i class="fa fa-newspaper-o fa-lg"></i> Sharing from incoming students
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>international/list.php?international_type=O">
										<i class="fa fa-newspaper-o fa-lg"></i> Sharing from outgoing Students
									</a>
								</li>
								<li>

									<a href="<?php echo $SYSTEM_HOST ?>international/list.php?international_type=S">
										<i class="fa fa-newspaper-o fa-lg"></i> Scholarship
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>master_lecture/list.php">
										<i class="fa fa-user-circle-o"></i> Master Lecture Series
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>global_learning/global_learning.php?global_learning_id=1">
										<i class="fa fa-book fa-lg"></i> Global Learning
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>international_academic_partners/list.php">
										<i class="fa fa-book fa-lg"></i> International Academic Partners
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>your_support_matters/your_support_matters.php?your_support_matters_id=1">
										<i class="fa fa-book fa-lg"></i> Your Support Matters
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>scholarship/scholarship.php?scholarship_id=1">
										<i class="fa fa-book fa-lg"></i> GL - Scholarship
									</a>
								</li>
							</ul>
						</li>
					<?php }  ?>


					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'industrial')) { ?>

						<li class="dropdown-submenu">
							<a href="javascript:void(0);">
								<i class="fa fa-newspaper-o fa-lg"></i> Industry Collaboration
							</a>
							<ul class="dropdown-menu">

								<li>
									<a href="<?php echo $SYSTEM_HOST ?>collaboration/collaboration.php?new_collaboration_id=1">
										<i class="fa fa-newspaper-o fa-lg"></i> Industrial Attachment
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>news/list.php?news_lv1=25&news_lv2=">
										<i class="fa fa-newspaper-o fa-lg"></i> Collaborative Projects
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>collaboration/collaboration.php?new_collaboration_id=14">
										<i class="fa fa-newspaper-o fa-lg"></i> Graduate Employment
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>collaboration/collaboration.php?new_collaboration_id=13">
										<i class="fa fa-newspaper-o fa-lg"></i> HKDI Star Graduate Scheme
									</a>
								</li>

							</ul>
						</li>
					<?php }  ?>



					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'edt')) { ?>

						<li class="dropdown-submenu">
							<a href="javascript:void(0);">
								<i class="fa fa-newspaper-o fa-lg"></i> edt
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>talent/list.php">
										<i class="fa fa-newspaper-o fa-lg"></i> Talent
									</a>
								</li>

								<li>
									<a href="<?php echo $SYSTEM_HOST ?>edt/list.php">
										<i class="fa fa-newspaper-o fa-lg"></i> edt
									</a>
								</li>

								<li>
									<a href="<?php echo $SYSTEM_HOST ?>info_day/list.php">
										<i class="fa fa-newspaper-o fa-lg"></i> Info Day
									</a>
								</li>

							</ul>
						</li>
					<?php }  ?>


					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'rsvp')) { ?>
						<li>
							<a href="<?php echo $SYSTEM_HOST ?>rsvp/list.php">
								<i class="fa fa-book fa-lg"></i>RSVP
							</a>
						</li>
					<?php } ?>

					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'peec')) { ?>
						<li class="dropdown-submenu">
							<a href="javascript:void(0);">
								<i class="fa fa-book fa-lg"></i> PEEC
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_page/peec_page.php?peec_page_id=1">
										<i class="fa fa-book fa-lg"></i>PEEC Page
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_page/peec_page.php?peec_page_id=2">
										<i class="fa fa-book fa-lg"></i>PEEC Job Page
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_section/list.php">
										<i class="fa fa-book fa-lg"></i>PEEC Sections
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_programmes_categories/list.php">
										<i class="fa fa-book fa-lg"></i>PEEC Programmes Categories
									</a>
								</li>
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_programmes/list.php">
										<i class="fa fa-book fa-lg"></i>PEEC Programmes
									</a>
								</li>
								<!-- <li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_menu/list.php">
										<i class="fa fa-book fa-lg"></i>PEEC Menu
									</a>
								</li> -->
								<!-- <li>
									<a href="<?php echo $SYSTEM_HOST ?>peec_menu/list.php?tier=2">
										<i class="fa fa-book fa-lg"></i>PEEC Menu - Tier 2 - Programs list
									</a>
								</li> -->
								<li>
									<a href="<?php echo $SYSTEM_HOST ?>peecjob_posting/list.php">
										<i class="fa fa-book fa-lg"></i>PEEC Job Posting
									</a>
								</li>

							</ul>
						</li>


					<?php } ?>

					

					<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'subscribe')) { ?>
						<li>
							<a href="<?php echo $SYSTEM_HOST ?>subscribe/list.php">
								<i class="fa fa-book fa-lg"></i>Subscribe
							</a>
						</li>
					<?php } ?>



				</ul>
			</li>
			<?php if(PermissionUtility::canApproveAny($_SESSION['sess_id'])) { ?>
				<li>
					<a href="<?php echo $SYSTEM_HOST ?>approval/list.php">
						<i class="fa fa-check-square-o fa-lg"></i> Content Approval
					</a>
				</li>
			<?php } ?>
			<?php if(PermissionUtility::isAdminGroup($_SESSION['sess_id'])){ ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-wrench fa-lg"></i> System<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<?php if(PermissionUtility::canEditByKey($_SESSION['sess_id'], 'sitemap')) { ?>

						<?php } ?>
						<li>
							<a href="<?php echo $SYSTEM_HOST ?>user/list.php">
								<i class="fa fa-user fa-lg"></i> Users
							</a>
						</li>
						<li>
							<a href="<?php echo $SYSTEM_HOST ?>user_group/list.php">
								<i class="fa fa-users fa-lg"></i> User Groups
							</a>
						</li>
					</ul>
				</li>
			<?php } ?>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li>
				<a href="<?php echo $SYSTEM_HOST ?>info/info.php">
					<i class="fa fa-info-circle fa-lg"></i> Profile
				</a>
			</li>
			<li>
				<a href="<?php echo $SYSTEM_HOST ?>logout.php">
					<i class="fa fa-power-off fa-lg"></i> Logout
				</a>
			</li>
		</ul>
	</div>
</div>
</div>
</div>
