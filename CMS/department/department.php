<?php
	include "../include/config.php";
	include $SYSTEM_BASE . "include/function.php";
	include $SYSTEM_BASE . "session.php";
	include $SYSTEM_BASE . "include/system_message.php";
	include $SYSTEM_BASE . "include/classes/DataMapper.php";
	include $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$department_id = $_REQUEST['department_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';
	
	if(!isInteger($department_id))
	{
		$department_id = "";
	}
	
	if($department_id != ""){
		$obj_row = DM::load('department', $department_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "department_" . $department_id)){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
		
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}

		if(!file_exists($department_path . $department_id)){
			mkdir($department_path . $department_id, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "department/" . $department_id . "/";
		
		$product_section_list = DM::findAll('product_section', ' product_section_status = ? AND product_section_pid = ? ', " product_section_sequence ", array(STATUS_ENABLE, $product_id));
		
		$product_tab_list = DM::findAll('product_tab', ' product_tab_status = ? AND product_tab_pid = ? ', " product_tab_sequence ", array(STATUS_ENABLE, $product_id));
		

		$last_updated_user = DM::load('user', $product_updated_by);

	} else if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){ 
		header("location: ../dashboard.php?msg=F-1000");
		return;
	} else {
		$product_publish_status = "D";
	}
?>
<!DOCTYPE html>
<html lang="en"><head>
		<?php include "../header.php" ?>
        <script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
        <script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.cookie.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/bootstrap-colorpicker.min.js"></script>
		<link href="<?php echo $SYSTEM_HOST ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">
       <script type="text/x-tmpl" id="shortcut_block_template">
			<div id="shortcut-row-{%=o.tmp_id%}" class="row shortcut_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<span class="col-xs-12" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveShortcutUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveShortcutDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeShortcutRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_shortcut_id" value=""/>
							<input type="hidden" name="product_shortcut_sequence" value=""/>
						</span>
					</div>
					<div class="form-group">
						<label class="col-xs-2 control-label">
							Theme Color
						</label>
						<span class="col-xs-2">
							<div class="input-group color_code">
								<input type="text" value="" name="product_shortcut_color" class="form-control" />
								<span class="input-group-addon"><i></i></span>
							</div>
						</span>
					</div>
					<div class="form-group">
						<label class="col-xs-2 control-label">
							Caption (English)
						</label>
						<div class="col-xs-10">
							<input type="text" class="form-control" name="product_shortcut_eng_caption" placeholder="Caption (English)"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-2 control-label">
							Caption (中文)
						</label>
						<div class="col-xs-10">
							<input type="text" class="form-control" name="product_shortcut_chi_caption" placeholder="Caption (中文)"/>
						</div>
					</div>
			
					<div class="form-group file_row">
						<label class="col-xs-2 control-label">
							Pdf (English)
						</label>
						<div class="col-xs-5">
							<input type="file" name="upload_english_pdf" accept=".pdf"/>
						</div>
					</div>
					<div class="form-group file_row">
						<label class="col-xs-2 control-label">
							Pdf (中文)
						</label>
						<div class="col-xs-5">
							<input type="file" name="upload_chinese_pdf" accept=".pdf"/>
						</div>
					</div>
					<div class="form-group url_row">
						<label class="col-xs-2 control-label">
							Url (English)
						</label>
						<div class="col-xs-10">
							<input type="text" class="form-control" name="product_shortcut_eng_url" placeholder="Url (English)"/>
						</div>
					</div>
					<div class="form-group url_row">
						<label class="col-xs-2 control-label">
							Url (中文)
						</label>
						<div class="col-xs-10">
							<input type="text" class="form-control" name="product_shortcut_chi_url" placeholder="Url (中文)"/>
						</div>
					</div>
				</form>
			</div>
		</script>
		<script type="text/x-tmpl" id="text_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							Text Section
						</label>
						<label class="col-xs-2 control-label">
							Background Color
						</label>
						<span class="col-xs-2">
							<div class="input-group color_code">
								<select class="form-control" name="product_section_background_color">
									<option value="0">White</option>
									<option value="1">Red</option>
								</select>
							</div>
						</span>
						<span class="col-xs-6" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="T"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">中文</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</script>
		<script type="text/x-tmpl" id="collapsible_text_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-3 control-label">
							Collapsible Text Section
						</label>
						<span class="col-xs-9" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="CT"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">中文</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</script>
		<script type="text/x-tmpl" id="file_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							File Section
						</label>
						<span class="col-xs-10" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="F"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-{%=o.tmp_id%}-2">中文</a></li>
						</ul>
						<div id="tabs-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-7">
									<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)"/>
								</div>
								<div class="col-xs-5">
									<div class="row">
										<div class="col-xs-4">
											<input type="file" name="upload_eng_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-folder-open" data-badge="false" multiple onchange="fileSelected(this, 'lang1');">
										</div>
										<div class="col-xs-4">
											<input type="file" name="upload_eng_video_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-film" data-badge="false" multiple onchange="VideoSelected(this, 'lang1');">
										</div>
										<div class="col-xs-4">
											<input type="file" name="upload_eng_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this, 'lang1');">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tabs-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-7">
									<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)"/>
								</div>
								<div class="col-xs-5">
									<div class="row">
										<div class="col-xs-4">
											<input type="file" name="upload_chi_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-folder-open" data-badge="false" multiple onchange="fileSelected(this, 'lang2');">
										</div>
										<div class="col-xs-4">
											<input type="file" name="upload_chi_video_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-film" data-badge="false" multiple onchange="VideoSelected(this, 'lang2');">
										</div>
										<div class="col-xs-4">
											<input type="file" name="upload_chi_pdf_file" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this, 'lang2');">
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</form>
			</div>
		</script>
		<script type="text/x-tmpl" id="file_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Title
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Detail
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Hyperlink
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_title" placeholder="Title" value="{%=o.filename%}"/>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_detail" placeholder="Detail" value=""/>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_link" placeholder="Hyperlink" value=""/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="product_section_file_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
					<input type="hidden" name="product_section_file_type" value="F"/>
					<input type="hidden" name="product_section_file_url" value=""/>
				</div>
				
			</div>
		</script>
        <script type="text/x-tmpl" id="video_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Title
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Youtube
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Youku
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_title" placeholder="Title" value="{%=o.filename%}"/>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_detail" placeholder="Youtube ID" value=""/>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_link" placeholder="Youku ID" value=""/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="product_section_file_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
					<input type="hidden" name="product_section_file_type" value="V"/>
					<input type="hidden" name="product_section_file_url" value=""/>
				</div>
				
			</div>
		</script>
        <script type="text/x-tmpl" id="PDF_file_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Title
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_title" placeholder="Title" value="{%=o.filename%}"/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i>
					</a>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="product_section_file_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
					<input type="hidden" name="product_section_file_type" value="P"/>
					<input type="hidden" name="product_section_file_url" value=""/>
				</div>
				
			</div>
		</script>
		<script type="text/x-tmpl" id="url_template">
			<div class="form-group file_row" style="margin-bottom:15px;">
				<div class="col-xs-1">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Caption
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12 control-label" style="height: 34px;">
							Url
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="form-group" style="margin-bottom:5px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_caption" placeholder="Caption" value=""/>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_file_url" placeholder="Url" value=""/>
						</div>
					</div>
				</div>
				<div class="col-xs-3" style="text-align:left;margin-top:20px">
					<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="product_section_file_id" value=""/>
					<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
					<input type="hidden" name="product_section_file_type" value="U"/>
				</div>
			</div>
		</script>
		<script>
			var file_list = {};
			var counter = 0;

			function checkForm(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#product_eng_name").val() == ""){
					errMessage += "Product Name (English)\n";
					flag = false;
				}
				
				if($("#product_chi_name").val() == ""){
					errMessage += "Product Name (中文)\n";
					flag = false;
				}
				
				if($("#product_type").val() == ""){
					errMessage += "Product Type\n";
					flag = false;
				}

				if(!flag){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$("input[name='product_section_file_caption']").each(function(idx, element){
					$(element).val(trim($(element).val()));
					if($(element).val() == ""){
						$("#tabs").tabs("option", "active", 2);
						$(element).focus();
						alert("Please fill in caption");
						flag = false;
						return false;
					}
				}).promise().done(function(){
					if(!flag)
						return;
					$("input[type='text'][name='product_section_file_url']").each(function(idx, element){
						$(element).val(trim($(element).val()));
						if($(element).val() == ""){
							$("#tabs").tabs("option", "active", 2);
							$(element).focus();
							alert("Please fill in url");
							flag = false;
							return false;
						}
					}).promise().done(function(){
						if(!flag)
							return;
						$('#loadingModalDialog').on('shown.bs.modal', function (e) {
							var frm = document.form1;
							frm.processType.value = "";
							cloneShortcut(0);
						})
						
						$('#loadingModalDialog').modal('show');
					});
				});
			}
			
			function checkForm2(){
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				
				if($("#product_eng_name").val() == ""){
					errMessage += "Product Name (English)\n";
					flag = false;
				}
				
				if($("#product_chi_name").val() == ""){
					errMessage += "Product Name (中文)\n";
					flag = false;
				}
				
				if($("#product_type").val() == ""){
					errMessage += "Product Type\n";
					flag = false;
				}
				
				if(!flag){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}

				$("input[name='product_section_file_caption']").each(function(idx, element){
					$(element).val(trim($(element).val()));
					if($(element).val() == ""){
						$("#tabs").tabs("option", "active", 2);
						$(element).focus();
						alert("Please fill in caption");
						flag = false;
						return false;
					}
				}).promise().done(function(){
					if(!flag)
						return;
					$("input[type='text'][name='product_section_file_url']").each(function(idx, element){
						$(element).val(trim($(element).val()));
						if($(element).val() == ""){
							$("#tabs").tabs("option", "active", 2);
							$(element).focus();
							alert("Please fill in url");
							flag = false;
							return false;
						}
					}).promise().done(function(){
						if(!flag)
							return;
						$('#approvalModalDialog').modal('show');
					});
				});
			}
			
			function cloneTextBlock(idx){
				if($("input[name='product_section_type'][value='T']").length <= idx){
					cloneCollapsibleBlock(0);
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='T']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneTextBlock(++idx);
			}
			
			function cloneCollapsibleBlock(idx){
				if($("input[name='product_section_type'][value='CT']").length <= idx){
					cloneFileBlock(0);
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='CT']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','collapsible_text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneCollapsibleBlock(++idx);
			}
			
			function cloneFileBlock(idx){
				if($("input[name='product_section_type'][value='F']").length <= idx){
					document.form1.submit();
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='F']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var obj = {"product_section_type":"F"};				
				obj['product_section_id'] = $(frm).find("input[name='product_section_id']").val();
				obj['product_section_title_lang1'] = $(frm).find("input[name='product_section_title_lang1']").val();
				obj['product_section_title_lang2'] = $(frm).find("input[name='product_section_title_lang2']").val();
				obj['product_section_title_lang3'] = $(frm).find("input[name='product_section_title_lang3']").val();
				obj['product_section_tid'] = $(frm).find("input[name='product_section_tid']").val();
				obj['product_section_sequence'] = $(frm).find("input[name='product_section_sequence']").val();
				obj['lang1_files'] = [];
				obj['lang2_files'] = [];
				obj['lang3_files'] = [];
				
				cloneFileElement(obj, idx, 0);
			}
			
			function cloneFileElement(dataObj, parentIdx, elementIdx){
				var frm = $("input[name='product_section_type'][value='F']:eq(" + parentIdx + ")").closest("form");

				if($(frm).find("div.file_row").length <= elementIdx){
					var data = JSON.stringify(dataObj);
					$('<input>').attr('name','file_block_section[]').attr('type','hidden').val(data).appendTo('#form1');					
					cloneFileBlock(++parentIdx);
					return;
				}
				
				var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");
				
				if($(row).find("input[name='tmp_id']").val() == "" || $(row).find("input[name='product_section_file_type']").val() == "U"){
					var fileObj = {};
					fileObj['product_section_file_title'] = $(row).find("input[name='product_section_file_title']").val();
					fileObj['product_section_file_detail'] = $(row).find("input[name='product_section_file_detail']").val();
					fileObj['product_section_file_link'] = $(row).find("input[name='product_section_file_link']").val();
					fileObj['product_section_file_id'] = $(row).find("input[name='product_section_file_id']").val();
					fileObj['product_section_file_lang'] = $(row).find("input[name='product_section_file_lang']").val();
					fileObj['product_section_file_type'] = $(row).find("input[name='product_section_file_type']").val();
					fileObj['product_section_file_url'] = $(row).find("input[name='product_section_file_url']").val();
					fileObj['tmp_filename'] = "";
					fileObj['filename'] = "";
					
					if(fileObj['product_section_file_lang'] == "lang1"){
						dataObj['lang1_files'].push(fileObj);
					} else if(fileObj['product_section_file_lang'] == "lang2") {
						dataObj['lang2_files'].push(fileObj);
					}else if(fileObj['product_section_file_lang'] == "lang3") {
						dataObj['lang3_files'].push(fileObj);
					}
					cloneFileElement(dataObj, parentIdx, ++elementIdx);
				} else {
					var fileObj = {};
					fileObj['product_section_file_title'] = $(row).find("input[name='product_section_file_title']").val();
					fileObj['product_section_file_detail'] = $(row).find("input[name='product_section_file_detail']").val();
					fileObj['product_section_file_link'] = $(row).find("input[name='product_section_file_link']").val();
					fileObj['product_section_file_id'] = $(row).find("input[name='product_section_file_id']").val();
					fileObj['product_section_file_lang'] = $(row).find("input[name='product_section_file_lang']").val();
					fileObj['product_section_file_type'] = $(row).find("input[name='product_section_file_type']").val();
					fileObj['product_section_file_url'] = $(row).find("input[name='product_section_file_url']").val();
					fileObj['tmp_filename'] = "";
					fileObj['filename'] = "";

					var fd = new FormData();
					fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);
					
					$.ajax({
						url: "../pre_upload/act.php",
						data: fd,
						processData: false,
						contentType: false,
						type: 'POST',
						dataType : "json",
						success: function(data) {
							fileObj['tmp_filename'] = data.tmpname;
							fileObj['filename'] = data.filename;
							if(fileObj['product_section_file_lang'] == "lang1"){
								dataObj['lang1_files'].push(fileObj);
							} else if(fileObj['product_section_file_lang'] == "lang2") {
								dataObj['lang2_files'].push(fileObj);
							}else if(fileObj['product_section_file_lang'] == "lang3") {
								dataObj['lang3_files'].push(fileObj);
							}
							cloneFileElement(dataObj, parentIdx, ++elementIdx);
						}
					});
				}
			}
			
			function cloneShortcut(idx){
				if($("div.shortcut_row").length <= idx){
					cloneTextBlock(0);
					return;
				}
				
				var frm = $("div.shortcut_row:eq(" + idx + ")").find("form");
				trimForm($(frm).attr("id"));
				
				if($(frm).find("select[name='product_shortcut_type']").val() == "L"){
					var dataObj = cloneFormToObject(frm);
					$('<input>').attr('name','shortcut[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');
					cloneShortcut(++idx);
				} else {
					var dataObj = cloneFormToObject(frm);
					
					cloneShortcutFile(frm, "upload_english_pdf", function(data){						
						if(data) {
							dataObj['english_filename'] = data.filename;
							dataObj['english_tmp_filename'] = data.tmpname;
						}

						cloneShortcutFile(frm, "upload_chinese_pdf", function(data){
							if(data){
								dataObj['chinese_filename'] = data.filename;
								dataObj['chinese_tmp_filename'] = data.tmpname;
							}
							
							$('<input>').attr('name','shortcut[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');
							cloneShortcut(++idx);
						});
					});
				}
			}
			
			function cloneShortcutFile(form, name, callback){
				var files = $(form).find("input[name='" + name + "']")[0].files;
				
				if($(form).find("input[name='product_shortcut_id']").val() != "" && files.length == 0){
					callback();
					return;
				}

				var fd = new FormData();
				fd.append('upload_file', files[0]);
				
				$.ajax({
					url: "../pre_upload/act.php",
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					dataType : "json",
					success: function(data) {
						callback(data);
					}
				});
			}
			
			function moveUp(button){
				var $current = $(button).closest(".section_row");
				var $previous = $current.prev('.section_row');
				if($previous.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertBefore($previous);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveDown(button){
				var $current = $(button).closest(".section_row");
				var $next = $current.next('.section_row');
				if($next.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertAfter($next);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function removeRow(button){
				$(button).closest(".section_row").detach();
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveFileUp(button){
				var $current = $(button).closest(".file_row");
				var $previous = $current.prev('.file_row');
				if($previous.length !== 0){
					$current.insertBefore($previous);
				}
			}
			
			function moveFileDown(button){
				var $current = $(button).closest(".file_row");
				var $next = $current.next('.file_row');
				if($next.length !== 0){
					$current.insertAfter($next);
				}
			}
			
			function removeFileItem(button){
				$(button).closest(".file_row").detach();				
			}
			
			function moveShortcutUp(button){
				var $current = $(button).closest(".shortcut_row");
				var $previous = $current.prev('.shortcut_row');
				if($previous.length !== 0){
					$current.insertBefore($previous);
				}
				$("input[name='product_shortcut_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveShortcutDown(button){
				var $current = $(button).closest(".shortcut_row");
				var $next = $current.next('.shortcut_row');
				if($next.length !== 0){
					$current.insertAfter($next);
				}
				$("input[name='product_shortcut_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function removeShortcutRow(button){
				$(button).closest(".shortcut_row").detach();
				$("input[name='product_shortcut_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addTextBlock(tab_id){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				var cookieName = "mycookie";
				$("#tabs-tmp-" + tmp_id).tabs({
					
					
    selected : ($.cookie(cookieName) || 0),
    select : function(e, ui) {
        $.cookie(cookieName, ui.index);
    }
        });
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addCollapsibleBlock(tab_id){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("collapsible_text_block_template", obj);
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addFileBlock(tab_id){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("file_block_template", obj);
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				$("#tabs-" + tmp_id).tabs({});
				$("#tabs-" + tmp_id + " input[name='upload_eng_file']").filestyle({input:false, buttonText: "&nbsp;Add Detail", badge:false});
				$("#tabs-" + tmp_id + " input[name='upload_eng_video_file']").filestyle({input:false, buttonText: "&nbsp;Add Video", badge:false,iconName: "glyphicon-film"});
				$("#tabs-" + tmp_id + " input[name='upload_eng_pdf_file']").filestyle({input:false, buttonText: "&nbsp;Add File", badge:false,iconName: "glyphicon-file"});
				$("#tabs-" + tmp_id + " input[name='upload_chi_file']").filestyle({input:false, buttonText: "&nbsp;Add Detail", badge:false});
				$("#tabs-" + tmp_id + " input[name='upload_chi_video_file']").filestyle({input:false, buttonText: "&nbsp;Add Video", badge:false,iconName: "glyphicon-film"});
				$("#tabs-" + tmp_id + " input[name='upload_chi_pdf_file']").filestyle({input:false, buttonText: "&nbsp;Add File", badge:false,iconName: "glyphicon-file"});
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addUrl(button, language){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id, "language": language};
				var content = tmpl("url_template", obj);
				var header_row = $(button).closest("div.form-group");
				
				if($(header_row).is(":last-child")){
					$(content).insertAfter(header_row);
				} else {
					$(content).insertAfter($(header_row).siblings(":last"));
				}
			}
			
			function fileSelected(fileBrowser, language){
				if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
						return;
				
				var files = $(fileBrowser)[0].files;
				
				for(var i = 0; i < files.length; i++){
					var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
					file_list[obj.tmp_id] = files[i];
					var content = tmpl("file_template", obj);
					var header_row = $(fileBrowser).closest("div.form-group");
					
					if($(header_row).is(":last-child")){
						$(content).insertAfter(header_row);
					} else {
						$(content).insertAfter($(header_row).siblings(":last"));
					}
				}
			}
			
			function VideoSelected(fileBrowser, language){
				if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
						return;
				
				var files = $(fileBrowser)[0].files;
				
				for(var i = 0; i < files.length; i++){
					var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
					file_list[obj.tmp_id] = files[i];
					var content = tmpl("video_template", obj);
					var header_row = $(fileBrowser).closest("div.form-group");
					
					if($(header_row).is(":last-child")){
						$(content).insertAfter(header_row);
					} else {
						$(content).insertAfter($(header_row).siblings(":last"));
					}
				}
			}
			
			function addFile(fileBrowser, language){
				if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
						return;
				
				var files = $(fileBrowser)[0].files;
				
				for(var i = 0; i < files.length; i++){
					var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
					file_list[obj.tmp_id] = files[i];
					var content = tmpl("PDF_file_template", obj);
					var header_row = $(fileBrowser).closest("div.form-group");
					
					if($(header_row).is(":last-child")){
						$(content).insertAfter(header_row);
					} else {
						$(content).insertAfter($(header_row).siblings(":last"));
					}
				}
			}
			
			
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						cloneShortcut(0);
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			function addShortCut(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("shortcut_block_template", obj);
				$("#tabs-2").append(content);
				$("#" + $(content).attr("id") + " input:file").filestyle({buttonText: "", badge:false});
				$("#" + $(content).attr("id")).find("div.file_row").hide();
				$("#" + $(content).attr("id")).find("div.url_row").hide();
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_shortcut_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function shortcutTypeChanged(link){
				var form = $(link).closest("form");
				var type = $(form).find("select[name='product_shortcut_type']").val();
				if(type == ""){
					$(form).find("div.file_row").hide();
					$(form).find("div.url_row").hide();
				} else if(type == "F"){
					$(form).find("div.file_row").show();
					$(form).find("div.url_row").hide();					
				} else if(type == "L"){
					$(form).find("div.file_row").hide();
					$(form).find("div.url_row").show();
				}
			}
			
			$( document ).ready(function() {
				var cookieName = "mycookie";
				$("#tabs").tabs({
					
					
    active : ($.cookie(cookieName) || 0),
    activate: function (event, ui) {
		var act = $("#tabs").tabs("option", "active");
		$.cookie(cookieName,act);
       // $.cookie(cookieName,ui.newPanel.selector);
    }
        });
				
				$("#products_banner").filestyle({buttonText: ""});
				$("#upload_large_banner").filestyle({buttonText: ""});				

				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});
				
				$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
					$(element).find("div.col-xs-12[id^='tabs']").tabs({});
					$(element).find('textarea').tinymce(tinymceConfig);
				});
				
				$("div.shortcut_row").each(function(idx, element){
					var form = $(element).find("form");
					$("#" + $(form).attr("id") + " input:file").filestyle({buttonText: "", badge:false});
					var type = $(form).find("select[name='product_shortcut_type']").val();
					if(type == ""){
						$(form).find("div.file_row").hide();
						$(form).find("div.url_row").hide();
					} else if(type == "F"){
						$(form).find("div.file_row").show();
						$(form).find("div.url_row").hide();					
					} else if(type == "L"){
						$(form).find("div.file_row").hide();
						$(form).find("div.url_row").show();
					}
				});
				
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.shortcut_row, .section_row {
				margin-bottom:10px; 
				padding: 7px 15px 4px;
				border: 1px solid #ccc;
				border-radius: 4px;
			}
		</style>
	</head>
	<body>
		<?php include "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-5">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Content</a></li>
					<li class="active"><?php echo $product_id == "" ? "Create" : "Update" ?></li>
				</ol>
				<span class="breadcrumb-right col-xs-7">
				</span>
			</div>
			<div id="top_submit_button_row" class="row">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
						<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
							<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
						</button>
				</div>
			</div>
			<div class="row record_status_row <?php echo ($product_publish_status == "RCA" || $product_publish_status == "RAA") ? "bg-danger" : "bg-record-status" ?>">
				<div class="col-xs-2">Publish Status:</div>
				<div class="col-xs-4">
					<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $product_publish_status] ?>
				</div>
				<?php if($product_id != "") { ?>
					<div class="col-xs-2">
						Last Updated By
					</div>
					<div class="col-xs-4">
						<?php echo htmlspecialchars($last_updated_user['user_display_name']) ?> @ <?php echo $product_updated_date ?>
					</div>
				<?php } ?>
			</div>
            <?php if($product_id != "") { ?>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
                    	
                        <?php
							foreach($product_tab_list as $product_tab) {
						?>
								<li><a href="#tabs-<?php echo $product_tab['product_tab_id']?>"><?php echo $product_tab['product_tab_name_lang1']?></a></li>
                        <?php
							} 
						?>

					</ul>
					
					
                    <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="type" value="product" />
                    <input type="hidden" name="processType" id="processType" value="">
                    <input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
                    <input type="hidden" name="product_status" value="1" />
                    <input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
                    <input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
                           </form> 
                    <?php foreach($product_tab_list as $product_tab) { ?>
                    <div id="tabs-<?php echo $product_tab['product_tab_id']?>">
							<div class="row">
								<table class="table table-bordered table-striped">
									<tbody>
										<tr>
											<td class="col-xs-12" style="vertical-align:middle">
												<div class="row">
													<div class="col-xs-6">
														<a role="button" class="btn btn-default" onclick="addTextBlock(<?php echo $product_tab['product_tab_id']?>); return false;">
															<i class="fa fa-font fa-lg"></i> Text Section
														</a>
														<a role="button" class="btn btn-default" onclick="addCollapsibleBlock(<?php echo $product_tab['product_tab_id']?>); return false;">
															<i class="fa fa-text-height fa-lg"></i> Collapsible Text Section
														</a>
														<a role="button" class="btn btn-default" onclick="addFileBlock(<?php echo $product_tab['product_tab_id']?>); return false;">
															<i class="fa fa-file-o fa-lg"></i> File Section
														</a>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php foreach($product_section_list as $product_section) {
								if($product_section['product_section_tid']==$product_tab['product_tab_id']){
									if($product_section['product_section_type'] == "T") {
										printTextBlock($product_section);
									} else if($product_section['product_section_type'] == "CT"){
										printCollapsibleTextBlock($product_section);
									} else if($product_section['product_section_type'] == "F"){
										printFileBlock($product_section);
									}
								}
							} ?>
						</div>
						<?php } ?>
						
					<?php } ?>
                    
                    
				</div>
			</div>
			<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
						<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
							<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
						</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include "../footer.php" ?>
	</body>
</html>
<?php
	function printTextBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-2 control-label">
                    Text Block
                </label>
            	<label class="col-xs-2 control-label">
                    Background Color
                </label>
                <span class="col-xs-2">
                    <div class="input-group color_code">
                        <select class="form-control" name="product_section_background_color">
                            <option value="0" <?php echo "0" == $record['product_section_background_color'] ? "selected" : "" ?> >White</option>
                            <option value="1" <?php echo "1" == $record['product_section_background_color'] ? "selected" : "" ?>>Red</option>
                        </select>
                    </div>
                </span>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="T"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">中文</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['product_section_title_lang1']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)" value="<?php echo htmlspecialchars($record['product_section_title_lang2']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
                
			</div>
		</form>
	</div>
<?php
	}  
	function printCollapsibleTextBlock($record){
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-3 control-label">
                    Collapsible Text Block
                </label>
				<span class="col-xs-9" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="CT"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">中文</a></li>

				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['product_section_title_lang1']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)" value="<?php echo htmlspecialchars($record['product_section_title_lang2']) ?>"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
                
			</div>
		</form>
	</div>
<?php
	}
	function printFileBlock($record){
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
		$lang1_files = DM::findAll("product_section_file", " product_section_file_status = ? AND product_section_file_psid = ? AND product_section_file_lang = ? ", " product_section_file_sequence ", array(STATUS_ENABLE, $record['product_section_id'], 'lang1'));
		$lang2_files = DM::findAll("product_section_file", " product_section_file_status = ? AND product_section_file_psid = ? AND product_section_file_lang = ? ", " product_section_file_sequence ", array(STATUS_ENABLE, $record['product_section_id'], 'lang2'));
		$lang3_files = DM::findAll("product_section_file", " product_section_file_status = ? AND product_section_file_psid = ? AND product_section_file_lang = ? ", " product_section_file_sequence ", array(STATUS_ENABLE, $record['product_section_id'], 'lang3'));
?>
	<div id="section-row-<?php echo $row_id ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-2 control-label">
                    File Section
                </label>
				<span class="col-xs-10" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="F"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">中文</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-7">
							<input type="text" class="form-control" name="product_section_title_lang1" placeholder="Heading (English)" value="<?php echo htmlspecialchars($record['product_section_title_lang1']) ?>"/>
						</div>
						<div class="col-xs-5">
							<div class="row">
								<div class="col-xs-4">
									<input type="file" name="upload_eng_file" class="filestyle" data-buttonText="&nbsp;Add Detail" data-input="false" data-iconName="glyphicon glyphicon-folder-open" data-badge="false" multiple onchange="fileSelected(this, 'lang1');">
								</div>
                                <div class="col-xs-4">
									<input type="file" name="upload_eng_video_file" class="filestyle" data-buttonText="&nbsp;Add Video" data-input="false" data-iconName="glyphicon glyphicon-film" data-badge="false" multiple onchange="VideoSelected(this, 'lang1');">
								</div>
								<div class="col-xs-4">
                                	<input type="file" name="upload_eng_pdf_file" class="filestyle" data-buttonText="&nbsp;Add File" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this, 'lang1');">
                                  
								</div>
							</div>
						</div>
					</div>
					<?php
						foreach($lang1_files as $lang1_file) { 
							printFileItem($lang1_file);
						}
					?>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-7">
							<input class="form-control" name="product_section_title_lang2" placeholder="Heading (中文)" value="<?php echo htmlspecialchars($record['product_section_title_lang2']) ?>"/>
						</div>
						<div class="col-xs-5">
							<div class="row">
								<div class="col-xs-4">
									<input type="file" name="upload_chi_file" class="filestyle" data-buttonText="&nbsp;Add Detail" data-input="false" data-iconName="glyphicon glyphicon-folder-open" data-badge="false" multiple onchange="fileSelected(this, 'lang2');">
								</div>
                                <div class="col-xs-4">
									<input type="file" name="upload_chi_video_file" class="filestyle" data-buttonText="&nbsp;Add Video" data-input="false" data-iconName="glyphicon glyphicon-film" data-badge="false" multiple onchange="VideoSelected(this, 'lang2');">
								</div>
								<div class="col-xs-4">
                                	<input type="file" name="upload_chi_pdf_file" class="filestyle" data-buttonText="&nbsp;Add File" data-input="false" data-iconName="glyphicon glyphicon-file" data-badge="false" multiple onchange="addFile(this, 'lang2');">
								</div>
							</div>
						</div>
					</div>
					<?php
						foreach($lang2_files as $lang2_file) { 
							printFileItem($lang2_file);
						} 
					?>
				</div>
               
			</div>
		</form>
	</div>
<?php
	}

	function printShortcutBlock($shortcut) {
		global $PRODUCT_SHORTCUT_ICONS, $SYSTEM_BASE;
?>
	<div id="shortcut-row-s-<?php echo $shortcut['product_shortcut_id'] ?>" class="row shortcut_row">
		<form class="form-horizontal" role="form" id="form-tmp-s-<?php echo $shortcut['product_shortcut_id'] ?>" name="form-tmp-s-<?php echo $shortcut['product_shortcut_id'] ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<span class="col-xs-12" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveShortcutUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveShortcutDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeShortcutRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_shortcut_id" value="<?php echo $shortcut['product_shortcut_id'] ?>"/>
					<input type="hidden" name="product_shortcut_sequence" value="<?php echo $shortcut['product_shortcut_sequence'] ?>"/>
				</span>
			</div>
			<div class="form-group">
				<label class="col-xs-2 control-label">
					Theme Color
				</label>
				<span class="col-xs-2">
					<div class="input-group color_code">
						<input type="text" name="product_shortcut_color" class="form-control" value="<?php echo htmlspecialchars($shortcut['product_shortcut_color']) ?>"/>
						<span class="input-group-addon"><i></i></span>
					</div>
				</span>
			</div>
			<div class="form-group">
				<label class="col-xs-2 control-label">
					Caption (English)
				</label>
				<div class="col-xs-10">
					<input type="text" class="form-control" name="product_shortcut_eng_caption" placeholder="Caption (English)" value="<?php echo htmlspecialchars($shortcut['product_shortcut_eng_caption']) ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-2 control-label">
					Caption (中文)
				</label>
				<div class="col-xs-10">
					<input type="text" class="form-control" name="product_shortcut_chi_caption" placeholder="Caption (中文)" value="<?php echo htmlspecialchars($shortcut['product_shortcut_chi_caption']) ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-2 control-label">
					Icon
				</label>
				<div class="col-xs-4">
					<select class="form-control" name="product_shortcut_icon">
						<option value="">Select</option>
						<?php foreach($PRODUCT_SHORTCUT_ICONS as $key => $filename) { ?>
							<option value="<?php echo $key ?>" <?php echo $key == $shortcut['product_shortcut_icon'] ? "selected" : "" ?>><?php echo htmlspecialchars($key) ?></option>
						<?php } ?>
					</select>
				</div>
				<label class="col-xs-2 control-label">
					Link Type
				</label>
				<div class="col-xs-4">
					<select class="form-control" name="product_shortcut_type" onchange="shortcutTypeChanged(this); return false;">
						<option value="">Select</option>
						<option value="F" <?php echo $shortcut['product_shortcut_type'] == "F" ? "selected" : "" ?>>File Download</option>
						<option value="L" <?php echo $shortcut['product_shortcut_type'] == "L" ? "selected" : "" ?>>Link</option>
					</select>
				</div>
			</div>
			<div class="form-group file_row">
				<label class="col-xs-2 control-label">
					Pdf (English)
				</label>
				<div class="col-xs-5">
					<input type="file" name="upload_english_pdf" accept=".pdf"/>
				</div>
				<?php if($shortcut['product_shortcut_type'] == "F" && $shortcut['product_shortcut_eng_file'] != "" && file_exists($SYSTEM_BASE . "uploaded_files/product_shortcut/" . $shortcut['product_shortcut_pid'] . "/" . $shortcut['product_shortcut_id'] . "/eng/" . $shortcut['product_shortcut_eng_file'])) { ?>
					<div class="col-xs-5">
						<a role="button" class="btn btn-default" href="<?php echo "/download/product_shortcut/" . $shortcut['product_shortcut_id'] . "/eng/" . $shortcut['product_shortcut_eng_file'] ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
					</div>
				<?php } ?>
			</div>
			<div class="form-group file_row">
				<label class="col-xs-2 control-label">
					Pdf (中文)
				</label>
				<div class="col-xs-5">
					<input type="file" name="upload_chinese_pdf" accept=".pdf"/>
				</div>
				<?php if($shortcut['product_shortcut_type'] == "F" && $shortcut['product_shortcut_chi_file'] != "" && file_exists($SYSTEM_BASE . "uploaded_files/product_shortcut/" . $shortcut['product_shortcut_pid'] . "/" . $shortcut['product_shortcut_id'] . "/chi/" . $shortcut['product_shortcut_chi_file'])) { ?>
					<div class="col-xs-5">
						<a role="button" class="btn btn-default" href="<?php echo "/download/product_shortcut/" . $shortcut['product_shortcut_id'] . "/chi/" . $shortcut['product_shortcut_chi_file'] ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
					</div>
				<?php } ?>
			</div>
			<div class="form-group url_row">
				<label class="col-xs-2 control-label">
					Url (English)
				</label>
				<div class="col-xs-10">
					<input type="text" class="form-control" name="product_shortcut_eng_url" placeholder="Url (English)" value="<?php echo htmlspecialchars($shortcut['product_shortcut_eng_url']) ?>"/>
				</div>
			</div>
			<div class="form-group url_row">
				<label class="col-xs-2 control-label">
					Url (中文)
				</label>
				<div class="col-xs-10">
					<input type="text" class="form-control" name="product_shortcut_chi_url" placeholder="Url (中文)" value="<?php echo htmlspecialchars($shortcut['product_shortcut_chi_url']) ?>"/>
				</div>
			</div>
		</form>
	</div>
<?php
	}
	
	function printFileItem($fileItem){
?>
	<?php if($fileItem['product_section_file_type'] == "F") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
            <div class="col-xs-1">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Title
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Detail
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Hyperlink
                    </div>
                </div>
            </div>
			<div class="col-xs-8">
            	<div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_title" placeholder="Title (English)" value="<?php echo htmlspecialchars($fileItem['product_section_file_title']) ?>"/>
					</div>
				</div>
                <div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_detail" placeholder="Detail (English)" value="<?php echo htmlspecialchars($fileItem['product_section_file_detail']) ?>"/>
					</div>
				</div>
                <div class="form-group" style="margin-bottom:0px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_link" placeholder="Hyperlink" value="<?php echo htmlspecialchars($fileItem['product_section_file_link']) ?>"/>
					</div>
				</div> 
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="../../uploaded_files/product_file/<?php echo $fileItem['product_section_file_pid'] . "/". $fileItem['product_section_file_id'] ?>/<?php echo $fileItem['product_section_file_filename'] ?> " target="_blank">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="product_section_file_id" value="<?php echo htmlspecialchars($fileItem['product_section_file_id']) ?>"/>
				<input type="hidden" name="product_section_file_lang" value="<?php echo htmlspecialchars($fileItem['product_section_file_lang']) ?>"/>
				<input type="hidden" name="product_section_file_type" value="<?php echo htmlspecialchars($fileItem['product_section_file_type']) ?>"/>
				<input type="hidden" name="product_section_file_url" value=""/>
			</div>
		</div>
	<?php } else if($fileItem['product_section_file_type'] == "U") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
			<div class="col-xs-1">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12 control-label" style="height: 34px;">
						Caption
					</div>
				</div>
				<div class="form-group" style="margin-bottom:0px;">
					<div class="col-xs-12 control-label" style="height: 34px;">
						Url
					</div>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="form-group" style="margin-bottom:5px;">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="product_section_file_caption" placeholder="Caption" value="<?php echo htmlspecialchars($fileItem['product_section_file_caption']) ?>"/>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:0px;">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="product_section_file_url" placeholder="Url" value="<?php echo htmlspecialchars($fileItem['product_section_file_url']) ?>"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left;margin-top:20px">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="product_section_file_id" value="<?php echo htmlspecialchars($fileItem['product_section_file_id']) ?>"/>
				<input type="hidden" name="product_section_file_lang" value="<?php echo htmlspecialchars($fileItem['product_section_file_lang']) ?>"/>
				<input type="hidden" name="product_section_file_type" value="<?php echo htmlspecialchars($fileItem['product_section_file_type']) ?>"/>
			</div>
		</div>
	<?php } else if($fileItem['product_section_file_type'] == "P") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
            <div class="col-xs-1">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Title
                    </div>
                </div>
            </div>
			<div class="col-xs-8">
            	<div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_title" placeholder="Title (English)" value="<?php echo htmlspecialchars($fileItem['product_section_file_title']) ?>"/>
					</div>
				</div>
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="../../uploaded_files/product_file/<?php echo $fileItem['product_section_file_pid'] . "/". $fileItem['product_section_file_id'] ?>/<?php echo $fileItem['product_section_file_filename'] ?> " target="_blank">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="product_section_file_id" value="<?php echo htmlspecialchars($fileItem['product_section_file_id']) ?>"/>
				<input type="hidden" name="product_section_file_lang" value="<?php echo htmlspecialchars($fileItem['product_section_file_lang']) ?>"/>
				<input type="hidden" name="product_section_file_type" value="<?php echo htmlspecialchars($fileItem['product_section_file_type']) ?>"/>
				<input type="hidden" name="product_section_file_url" value=""/>
			</div>
		</div>
	<?php } else if($fileItem['product_section_file_type'] == "V") { ?>
		<div class="form-group file_row" style="margin-bottom:15px;">
            <div class="col-xs-1">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Title
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Youtube
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0px;">
                    <div class="col-xs-12 control-label" style="height: 34px;">
                        Youku
                    </div>
                </div>
            </div>
			<div class="col-xs-8">
            	<div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_title" placeholder="Title (English)" value="<?php echo htmlspecialchars($fileItem['product_section_file_title']) ?>"/>
					</div>
				</div>
                <div class="form-group" style="margin-bottom:5px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_detail" placeholder="Youtube ID" value="<?php echo htmlspecialchars($fileItem['product_section_file_detail']) ?>"/>
					</div>
				</div> 
                <div class="form-group" style="margin-bottom:0px;">
                	<div class="col-xs-12">
				<input type="text" class="form-control" name="product_section_file_link" placeholder="Youku ID" value="<?php echo htmlspecialchars($fileItem['product_section_file_link']) ?>"/>
					</div>
				</div> 
			</div>
			<div class="col-xs-3" style="text-align:left">
				<button type="button" class="btn btn-default" onclick="moveFileUp(this); return false;">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</button>
				<button type="button" class="btn btn-default" onclick="moveFileDown(this); return false;">
					<i class="glyphicon glyphicon-arrow-down"></i>
				</button>
				<a role="button" class="btn btn-default" href="../../uploaded_files/product_file/<?php echo $fileItem['product_section_file_pid'] . "/". $fileItem['product_section_file_id'] ?>/<?php echo $fileItem['product_section_file_filename'] ?> " target="_blank">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value=""/>
				<input type="hidden" name="product_section_file_id" value="<?php echo htmlspecialchars($fileItem['product_section_file_id']) ?>"/>
				<input type="hidden" name="product_section_file_lang" value="<?php echo htmlspecialchars($fileItem['product_section_file_lang']) ?>"/>
				<input type="hidden" name="product_section_file_type" value="<?php echo htmlspecialchars($fileItem['product_section_file_type']) ?>"/>
				<input type="hidden" name="product_section_file_url" value=""/>
			</div>
		</div>
	<?php } ?>
<?php
	}
?>