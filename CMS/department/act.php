<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="product" ) {

		$id = $_REQUEST['product_id'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "product_" . $id)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}
		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['product_publish_status'] = "D";

				$values['product_status'] = STATUS_ENABLE;

				$values['product_created_by'] = $_SESSION['sess_id'];

				$values['product_created_date'] = $timestamp;

				$values['product_updated_by'] = $_SESSION['sess_id'];

				$values['product_updated_date'] = $timestamp;

				$values['product_type'] = "1";



				$id = DM::insert('product', $values);


				if(!file_exists($product_file_path . $id. "/")){
					mkdir($product_file_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('product', $id);



				if($ObjRec['product_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}


				$values['product_type'] = "1";

				$values['product_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['product_updated_by'] = $_SESSION['sess_id'];

				$values['product_updated_date'] = $timestamp;



				DM::update('product', $values);

			}



			$product_section_list = array();



			$text_block_section = $_REQUEST['text_block_section'] ?? '';

			$collapsible_text_block_section = $_REQUEST['collapsible_text_block_section'] ?? '';

			$file_block_section = $_REQUEST['file_block_section'] ?? '';



			if($text_block_section != "" && is_array($text_block_section)){

				foreach($text_block_section as $text_block){

					$text_block = json_decode($text_block, true);



					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$values['product_type'] = "1";

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;



						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;

					} else {

						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;



						DM::update('product_section', $text_block);

					}

				}

			}



			if($collapsible_text_block_section != "" && is_array($collapsible_text_block_section)){

				foreach($collapsible_text_block_section as $collapsible_text_block){

					$collapsible_text_block = json_decode($collapsible_text_block, true);



					if($collapsible_text_block['product_section_id'] == "") {

						unset($collapsible_text_block['product_section_id']);

						$collapsible_text_block['product_section_pid'] = $id;

						$collapsible_text_block['product_section_status'] = STATUS_ENABLE;

						$collapsible_text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$collapsible_text_block['product_section_created_date'] = $timestamp;

						$collapsible_text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$collapsible_text_block['product_section_updated_date'] = $timestamp;



						$product_section_id = DM::insert('product_section', $collapsible_text_block);

						$product_section_list[] = $product_section_id;

					} else {

						$product_section_list[] = $collapsible_text_block['product_section_id'];

						$collapsible_text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$collapsible_text_block['product_section_updated_date'] = $timestamp;



						DM::update('product_section', $collapsible_text_block);

					}

				}

			}



			if($file_block_section != "" && is_array($file_block_section)){

				foreach($file_block_section as $file_block){

					$file_block = json_decode($file_block, true);



					if($file_block['product_section_id'] == "") {

						unset($file_block['product_section_id']);

						$file_block['product_section_pid'] = $id;

						$file_block['product_section_content_lang1'] = "";

						$file_block['product_section_content_lang2'] = "";

						$file_block['product_section_content_lang3'] = "";

						$file_block['product_section_status'] = STATUS_ENABLE;

						$file_block['product_section_created_by'] = $_SESSION['sess_id'];

						$file_block['product_section_created_date'] = $timestamp;

						$file_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$file_block['product_section_updated_date'] = $timestamp;



						$product_section_id = DM::insert('product_section', $file_block);

						$product_section_list[] = $product_section_id;

					} else {

						$product_section_list[] = $file_block['product_section_id'];

						$product_section_id = $file_block['product_section_id'];

						$file_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$file_block['product_section_updated_date'] = $timestamp;



						DM::update('product_section', $file_block);

					}



					$product_section_file_list = array();

					$lang1_files = $file_block['lang1_files'];

					$lang2_files = $file_block['lang2_files'];

					$lang3_files = $file_block['lang3_files'];



					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){

							if($lang1_file['product_section_file_id'] == "") {

								unset($lang1_file['product_section_file_id']);

								$lang1_file['product_section_file_psid'] = $product_section_id;

								$lang1_file['product_section_file_pid'] = $id;

								$lang1_file['product_section_file_lang'] = 'lang1';

								$lang1_file['product_section_file_sequence'] = $count++;

								$lang1_file['product_section_file_filename'] = $lang1_file['filename'];

								$lang1_file['product_section_file_status'] = STATUS_ENABLE;

								$lang1_file['product_section_file_created_by'] = $_SESSION['sess_id'];

								$lang1_file['product_section_file_created_date'] = $timestamp;

								$lang1_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang1_file['product_section_file_updated_date'] = $timestamp;



								$product_section_file_id = DM::insert('product_section_file', $lang1_file);

								$product_section_file_list[] = $product_section_file_id;

								if(!file_exists($product_file_path . $id. "/".$product_section_file_id."/")){
									mkdir($product_file_path . $id . "/".$product_section_file_id."/", 0775, true);
								}


								if($lang1_file['product_section_file_type'] == "F" || $lang1_file['product_section_file_type'] == "V" || $lang1_file['product_section_file_type'] == "P")

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $product_file_path . $id . "/".$product_section_file_id."/" . $lang1_file['filename']);

							} else {

								$product_section_file_list[] = $lang1_file['product_section_file_id'];

								$product_section_file_id = $lang1_file['product_section_file_id'];

								$lang1_file['product_section_file_lang'] = 'lang1';

								$lang1_file['product_section_file_sequence'] = $count++;

								$lang1_file['product_section_file_sequence'] = $count++;

								$lang1_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang1_file['product_section_file_updated_date'] = $timestamp;



								DM::update('product_section_file', $lang1_file);

							}

						}

					}



					if($lang2_files != "" && is_array($lang2_files)){

						$count = 1;

						foreach($lang2_files as $lang2_file){

							if($lang2_file['product_section_file_id'] == "") {

								unset($lang2_file['product_section_file_id']);

								$lang2_file['product_section_file_psid'] = $product_section_id;

								$lang2_file['product_section_file_pid'] = $id;

								$lang2_file['product_section_file_lang'] = 'lang2';

								$lang2_file['product_section_file_sequence'] = $count++;

								$lang2_file['product_section_file_filename'] = $lang2_file['filename'];

								$lang2_file['product_section_file_status'] = STATUS_ENABLE;

								$lang2_file['product_section_file_created_by'] = $_SESSION['sess_id'];

								$lang2_file['product_section_file_created_date'] = $timestamp;

								$lang2_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang2_file['product_section_file_updated_date'] = $timestamp;



								$product_section_file_id = DM::insert('product_section_file', $lang2_file);

								$product_section_file_list[] = $product_section_file_id;

								if(!file_exists($product_file_path . $id. "/".$product_section_file_id."/")){
									mkdir($product_file_path . $id . "/".$product_section_file_id."/", 0775, true);
								}


								if($lang2_file['product_section_file_type'] == "F" || $lang2_file['product_section_file_type'] == "V" || $lang1_file['product_section_file_type'] == "P")

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang2_file['tmp_filename'], $product_file_path . $id . "/".$product_section_file_id."/" . $lang2_file['filename']);

							} else {

								$product_section_file_list[] = $lang2_file['product_section_file_id'];

								$product_section_file_id = $lang2_file['product_section_file_id'];

								$lang2_file['product_section_file_lang'] = 'lang2';

								$lang2_file['product_section_file_sequence'] = $count++;

								$lang2_file['product_section_file_sequence'] = $count++;

								$lang2_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang2_file['product_section_file_updated_date'] = $timestamp;



								DM::update('product_section_file', $lang2_file);

							}

						}

					}

					if($lang3_files != "" && is_array($lang3_files)){

						$count = 1;

						foreach($lang3_files as $lang3_file){

							if($lang3_file['product_section_file_id'] == "") {

								unset($lang3_file['product_section_file_id']);

								$lang3_file['product_section_file_psid'] = $product_section_id;

								$lang3_file['product_section_file_pid'] = $id;

								$lang3_file['product_section_file_lang'] = 'lang3';

								$lang3_file['product_section_file_sequence'] = $count++;

								$lang3_file['product_section_file_filename'] = $lang3_file['filename'];

								$lang3_file['product_section_file_status'] = STATUS_ENABLE;

								$lang3_file['product_section_file_created_by'] = $_SESSION['sess_id'];

								$lang3_file['product_section_file_created_date'] = $timestamp;

								$lang3_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang3_file['product_section_file_updated_date'] = $timestamp;



								$product_section_file_id = DM::insert('product_section_file', $lang3_file);

								$product_section_file_list[] = $product_section_file_id;

								if(!file_exists($product_file_path . $id. "/".$product_section_file_id."/")){
									mkdir($product_file_path . $id . "/".$product_section_file_id."/", 0775, true);
								}


								if($lang3_file['product_section_file_type'] == "F" || $lang3_file['product_section_file_type'] == "V" || $lang1_file['product_section_file_type'] == "P")

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang3_file['tmp_filename'], $product_file_path . $id . "/".$product_section_file_id."/" . $lang3_file['filename']);

							} else {

								$product_section_file_list[] = $lang3_file['product_section_file_id'];

								$product_section_file_id = $lang3_file['product_section_file_id'];

								$lang3_file['product_section_file_lang'] = 'lang3';

								$lang3_file['product_section_file_sequence'] = $count++;

								$lang3_file['product_section_file_sequence'] = $count++;

								$lang3_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

								$lang3_file['product_section_file_updated_date'] = $timestamp;



								DM::update('product_section_file', $lang3_file);

							}

						}

					}



					$sql = " product_section_file_status = ? AND product_section_file_psid = ? ";


					$parameters = array(STATUS_ENABLE, $product_section_id);



					if(count($product_section_file_list) > 0){

						$sql .= " AND product_section_file_id NOT IN (" . implode(',', array_fill(0, count($product_section_file_list), '?')) . ") ";

						$parameters = array_merge($parameters, $product_section_file_list);

					}



					$product_section_files = DM::findAll("product_section_file", $sql, " product_section_file_sequence ", $parameters);



					foreach($product_section_files as $product_section_file){

						$values = array();

						$values['product_section_file_id'] = $product_section_file['product_section_file_id'];

						$values['product_section_file_status'] = STATUS_DISABLE;

						$values['product_section_file_updated_by'] = $_SESSION['sess_id'];

						$values['product_section_file_updated_date'] = $timestamp;



						DM::update('product_section_file', $values);

						unset($values);

					}



					unset($product_section_file_list);

				}

			}



			$sql = " product_section_status = ? AND product_section_pid = ? ";

			$parameters = array(STATUS_ENABLE, $id);



			if(count($product_section_list) > 0){

				$sql .= " AND product_section_id NOT IN (" . implode(',', array_fill(0, count($product_section_list), '?')) . ") ";

				$parameters = array_merge($parameters, $product_section_list);

			}



			$product_sections = DM::findAll("product_section", $sql, " product_section_sequence ", $parameters);



			foreach($product_sections as $product_section){

				$values = array();

				$values['product_section_id'] = $product_section['product_section_id'];

				$values['product_section_status'] = STATUS_DISABLE;

				$values['product_section_updated_by'] = $_SESSION['sess_id'];

				$values['product_section_updated_date'] = $timestamp;



				DM::update('product_section', $values);

				unset($values);

			}



			unset($product_sections);



			$sql = " product_section_file_status = ? AND product_section_file_pid = ?";

			$parameters = array(STATUS_ENABLE,$id);



			if(count($product_section_list) > 0){

				$sql .= " AND product_section_file_psid NOT IN (" . implode(',', array_fill(0, count($product_section_list), '?')) . ") ";

				$parameters = array_merge($parameters, $product_section_list);

			}



			$product_section_files = DM::findAll("product_section_file", $sql, " product_section_file_id ", $parameters);



			foreach($product_section_files as $product_section_file){

				$values = array();

				$values['product_section_file_id'] = $product_section_file['product_section_file_id'];

				$values['product_section_file_status'] = STATUS_DISABLE;

				$values['product_section_file_updated_by'] = $_SESSION['sess_id'];

				$values['product_section_file_updated_date'] = $timestamp;



				DM::update('product_section_file', $values);

				unset($values);

			}



			unset($product_section_files);

			unset($product_section_list);



			//PermissionUtility::rebuildPermission();



			NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_".$id, $isNew ? "CS" : "ES");

			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "Product '" . $ObjRec['product_eng_name'] . "' (ID:" . $ObjRec['product_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_" . $id, "Product '" . $ObjRec['product_eng_name'] . "' is updated by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "product_".$id,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['product_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_".$id, $isNew ? "CS" : "ES");
				$files = array($host_name."web/en/investment/index.php", $host_name."web/tc/investment/index.php", $host_name."web/sc/investment/index.php",$host_name."web/en/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/tc/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/sc/investment/product.php?product=".$ObjRec['product_name_lang1']);

				$approval_id = ApprovalService::createApprovalRequest( "product_".$id, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("product", $id, $timestamp, $_SESSION['sess_id'], array("A"), "product_".$id, "PA", $approval_id);

			}

			header("location:products.php?product_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('product', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['product_status'] == STATUS_DISABLE)

			{

				header("location: list.php?msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord('product', $id);

			$values['product_id'] = $id;
			$values['product_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['product_status'] = STATUS_DISABLE;
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = getCurrentTimestamp();


			DM::update('product', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("product_".$id, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Product '" . $ObjRec['product_name_lang1'] . "' (ID:" . $ObjRec['product_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("product_".$id, $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_".$id, "DS");

				$files = array($host_name."web/en/investment/index.php", $host_name."web/tc/investment/index.php", $host_name."web/sc/investment/index.php",$host_name."web/en/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/tc/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/sc/investment/product.php?product=".$ObjRec['product_name_lang1']);

				$approval_id = ApprovalService::createApprovalRequest("product_".$id, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("product_".$id, $id, $timestamp, $_SESSION['sess_id'], array("A"), "product_".$id, "PA", $approval_id);
			}





			header("location: list.php?msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('product', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}



			if($ObjRec['product_status'] == STATUS_ENABLE || $ObjRec['product_publish_status'] == 'AL')

			{

				header("location: list.php?msg=F-1006");

				return;

			}



			$values['product_id'] = $id;

			$values['product_publish_status'] = 'D';

			$values['product_status'] = STATUS_ENABLE;

			$values['product_updated_by'] = $_SESSION['sess_id'];

			$values['product_updated_date'] = getCurrentTimestamp();



			DM::update('product', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'product', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of product '" . $ObjRec['product_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'product_' . $id, $message);

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "Product '" . $ObjRec['product_eng_name'] . "' (ID:" . $ObjRec['product_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), 'product_' . $id, "Product '" . $ObjRec['product_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>