<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/MessageUtility.php";

	$msg = $_REQUEST['msg'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	


	$approval_approval_status = '';
	$user = array();
	$user_type = '';
	$user_approver = DM::findOne( 'group_user', "group_user_uid = ? AND group_user_gid = ?", "", array($_SESSION['sess_id'], 24));
	$user_approver_head = DM::findOne( 'group_user', "group_user_uid = ? AND group_user_gid = ?", "", array($_SESSION['sess_id'], 40));

	if( empty($user_approver) && empty($user_approver_head) ) {
		header("location: ../dashboard.php?msg=F-1000");
	}
	else {
		if( ( !empty($user_approver) && empty($user_approver_head) ) || ( !empty($user_approver) && !empty($user_approver_head) ) ) {
			$user_type = 'Approver';
			$user = $user_approver;
			$approval_approval_status = 'RAA';
		}
		elseif( empty($user_approver) && !empty($user_approver_head) ) {
			$user_type = 'Approver Head';
			$user = $user_approver_head;
			$approval_approval_status = 'RHA';
		}
		else {
			header("location: ../dashboard.php?msg=F-1000");
		}
	}

	$sql = " approval_submitted_by = user_id AND approval_status = ? ";
	$sql .= " AND approval_permission_key in ( ";
	$sql .= " SELECT ugp_permission_key FROM user_group_permission, group_user, user_group ";
	$sql .= " WHERE user_group_status = ? AND user_group_id = group_user_gid ";
	$sql .= " AND user_group_id = ugp_user_group_id AND group_user_uid = ? AND ugp_role = ? ";
	$sql .= " ) AND approval_approval_status = ? ";

	
	$parameters = array(STATUS_ENABLE, STATUS_ENABLE, $_SESSION['sess_id'], 'A', $approval_approval_status);
	$approval_list = DM::findAll(array("approval", "user"), $sql, " approval_submit_date DESC ", $parameters);
	
	/*
	$parameters = array(STATUS_ENABLE, STATUS_ENABLE, $_SESSION['sess_id'], 'A', "APL");
	$pending_list = DM::findAll(array("approval", "user"), $sql, " approval_submit_date DESC ", $parameters);
	*/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script>
			function launchItem(approval_id, item_name){
				if(confirm("Are you sure you want to launch " + item_name + " ?")){
					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						location.href = "act.php?type=approval&processType=launch&approval_id=" + approval_id;
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Content Approval</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
				<div class="row">
					<h4 class="cols-xs-12" style="margin-bottom: 5px; margin-top: 5px;">Pending Approval Request</h4>
				</div>
				<div class="row">
					<?php if(count($approval_list) == 0) { ?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="col-xs-12" style="text-align:center">No Pending Approval Request</th>
								</tr>
							</thead>
						</table>
					<?php } else { ?>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="col-xs-2">Submit Date</th>
									<th class="col-xs-2">Submitted By</th>
									<th class="col-xs-7">Updated Item</th>
									<th class="col-xs-1">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$row_count = 0;
									foreach($approval_list as $row) {
										$approval_id = $row['approval_id'];
										$approval_submit_date = htmlspecialchars($row['approval_submit_date']);
										$user_display_name = htmlspecialchars($row['user_display_name']);
										$approval_item_type = $row['approval_item_type'];
										$approval_item_id = $row['approval_item_id'];
										$approval_menu_id = $row['approval_menu_id'] ?? 0;
										
										if($approval_item_type =="product" && $approval_item_id=="2"){
											$item_name ="Research";
										}
										elseif($approval_item_type == "peec_page") {
											$item_name ="PEEC Page";
										}
										else{
											//$item_name = htmlspecialchars(MessageUtility::getItemTypeName($row['approval_item_type']) . " '" . MessageUtility::getItemName($row['approval_item_type'], $row['approval_item_id']) . "'");
											$item_name = " '" . MessageUtility::getItemName($row['approval_item_type'], $row['approval_item_id']) . "'";
										}
								?>
										<tr>
											<td><?php echo $approval_submit_date ?></td>
											<td><?php echo $user_display_name ?></td>
											<td><?php echo $item_name ?></td>
											<td>
												<button type="button" class="btn btn-default btn-sm" onclick="location.href='approval.php?approval_id=<?php echo $approval_id ?>'; return false;">
													<span class="fa fa-cog"></span> Process
												</button>
											</td>
										</tr>
								<?php
									$row_count++;
									}
									unset($resultObj);
								?>
							</tbody>
						</table>
					<?php } ?>
				</div>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>