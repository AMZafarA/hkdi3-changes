<?php
include "../include/config.php";
include $SYSTEM_BASE . "include/function.php";
include $SYSTEM_BASE . "session.php";
include $SYSTEM_BASE . "include/system_message.php";
include $SYSTEM_BASE . "include/classes/DataMapper.php";
include $SYSTEM_BASE . "include/classes/Logger.php";
// include $SYSTEM_BASE . "include/classes/Mutex.php";
include $SYSTEM_BASE . "include/classes/SystemUtility.php";
// include $SYSTEM_BASE . "include/classes/MessageUtility.php";
include $SYSTEM_BASE . "include/classes/NotificationService.php";
// include $SYSTEM_BASE . "include/classes/ContentSynchronizer.php";
include $SYSTEM_BASE . "include/classes/PermissionUtility.php";
include $SYSTEM_BASE . "include/classes/cloning/ContentCloningUtility.php";
include $SYSTEM_BASE . "include/classes/ApprovalService.php";


$type = $_REQUEST['type'] ?? '';
$processType = $_REQUEST['processType'] ?? '';

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

if($type=="approval" )
{
	$approval_id = $_REQUEST['approval_id'] ?? '';

	$ObjRec = DM::load('approval', $approval_id);

	if($ObjRec == NULL || $ObjRec == "" || $ObjRec['approval_status'] != STATUS_ENABLE) {
		header("location: list.php?msg=F-1002");
		return;
	}

	if($processType == "approverUpdate" && $ObjRec['approval_approval_status'] == "RAA"){

		if(!PermissionUtility::canApproveByKey($_SESSION['sess_id'], $ObjRec['approval_permission_key'])){
			header("location: list.php?msg=F-1000");
			return;
		}

	} 
	elseif($processType == "launch" && $ObjRec['approval_approval_status'] == "APL"){
		if(!PermissionUtility::canApproveByKey($_SESSION['sess_id'], $ObjRec['approval_permission_key'])){
			header("location: list.php?msg=F-1000");
			return;
		}
	} 
	elseif($processType == "approverHeadUpdate" && ($ObjRec['approval_approval_status'] == "RAA" || $ObjRec['approval_approval_status'] == "RHA")){
		if(!PermissionUtility::canApproveByKey($_SESSION['sess_id'], $ObjRec['approval_permission_key'])){
			header("location: list.php?msg=F-1000");
			return;
		}
	} 
	else {
		header("location: list.php?msg=F-1000");
		return;
	}

	$timestamp = getCurrentTimestamp();

	if($processType == "approverUpdate"){
		ignore_user_abort(true);
		set_time_limit(0);

		if($ObjRec['approval_approval_status'] != "RAA"){
			header("location: list.php?msg=F-1017");
			return;
		}

		if($_REQUEST['approval_approval_status'] != "APL" &&  $_REQUEST['approval_approval_status'] != "AL" &&  $_REQUEST['approval_approval_status'] != "AR"){
			header("location: list.php?msg=F-1017");
			return;
		}

		$values = array();
		$values['approval_id'] = $approval_id;
		$values['approval_approver_reply_date'] = $timestamp;
		$values['approval_approver_reply'] = $_REQUEST['approval_approval_status'] == "AR" ? "R" : "A";
		$values['approval_approver_reply_by'] = $_SESSION['sess_id'];
		$values['approval_approver_remarks'] = $_REQUEST['approval_approver_remarks'] ?? '';
		$values['approval_approval_status'] = $_REQUEST['approval_approval_status'] ?? '';
		$values['approval_updated_by'] = $_SESSION['sess_id'];
		$values['approval_updated_date'] = $timestamp;

		if($_REQUEST['approval_approval_status'] == "AL"){
			$values['approval_launched_date'] = $timestamp;
		}

		DM::update('approval', $values);
		unset($values);

			//update item accordingly
		$values = array();
		$values[$ObjRec['approval_item_type'] . '_publish_status'] = $_REQUEST['approval_approval_status'] == "AR" ? "D" : $_REQUEST['approval_approval_status'] ?? '';
		if($_REQUEST['approval_approval_status'] == "AR")
		{
			$status =  STATUS_ENABLE;
			$values[$ObjRec['approval_item_type'] . '_status'] =$status;
		}

		$values[$ObjRec['approval_item_type'] . '_id'] = $ObjRec['approval_item_id'];

		DM::update($ObjRec['approval_item_type'], $values);
		unset($values);

		ApprovalService::notificationProcessed($approval_id);

		$item_name = htmlspecialchars(MessageUtility::getItemTypeName($ObjRec['approval_item_type']) . " '" . MessageUtility::getItemName($ObjRec['approval_item_type'], $ObjRec['approval_item_id']) . "'");

		$log_message = "Approval request for " . $item_name . " (ID:" . $ObjRec['approval_item_id'] .") ";

		if($_REQUEST['approval_approval_status'] == "AR"){
			$log_message .= "is rejected";
		} elseif($_REQUEST['approval_approval_status'] == "APL"){
			$log_message .= "is approved and scheduled for launch";
		} elseif($_REQUEST['approval_approval_status'] == "AL"){
			$log_message .= "is approved and launched";
		}

		$log_message .= " by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")";
		Logger::log($_SESSION['sess_id'], $timestamp, $log_message);

		if($_REQUEST['approval_approval_status'] == "AR"){
			$item_action = "RA";
		} elseif($_REQUEST['approval_approval_status'] == "APL"){
			$item_action = "AAS";
		} elseif($_REQUEST['approval_approval_status'] == "AL"){
			$item_action = "AAL";
		}

		$approval_item_type = $ObjRec['approval_item_type'];

		if($approval_item_type=='international'){
			if($ObjRec['approval_permission_key'] =="industrial"){
				$approval_item_type = "Attachment";
			}
		}

		if($approval_item_type=='product'){
			if(strpos($ObjRec['approval_permission_key'], 'scheme') !== false){
				$approval_item_type = "scheme";
			}
			if($ObjRec['approval_permission_key'] =="programmes"){
				$approval_item_type = "programmes";
			}
			if($ObjRec['approval_permission_key'] =="news"){
				$approval_item_type = "news";
			}
			if($ObjRec['approval_permission_key'] =="master"){
				$approval_item_type = "master_index";
			}
			if($ObjRec['approval_permission_key'] =="Exhibitons"){
				$approval_item_type = "Exhibitons";
			}
			if($ObjRec['approval_permission_key'] =="continuing_edcation"){
				$approval_item_type = "continuing_education_course";
			}
			if($ObjRec['approval_permission_key'] =="HKDI Gallery"){
				$approval_item_type = "HKDI Gallery";
			}
			if($ObjRec['approval_permission_key'] =="edt"){
				$approval_item_type = "edt";
			}
			if($ObjRec['approval_permission_key'] =="cimt"){
				$approval_item_type = "Knowledge Centre - Centre of Innovative Material and Technology";
			}

		}

		$section_arr = array('news','project','programmes');

		foreach($section_arr as $key=>$val){
			if($approval_item_type==$val){

				$obj = DM::load($val, $ObjRec['approval_item_id']);
				$lv1 = $obj[$val.'_lv1'];
				$lv2 = $obj[$val.'_lv2'];

				$type_name = DM::load("department", $lv1);

				$type_namelv1 = $type_name['department_name_lang1'];

				$type_name = DM::load("department", $lv2);

				$type_namelv2 = $type_name['department_name_lang1'] ?? '';

				$approval_item_type = ucfirst($val);

				if($type_namelv1){ $approval_item_type .= " - ".$type_namelv1;}
				if($type_namelv2){ $approval_item_type .= " - ".$type_namelv2;}

			}
		}




		NotificationService::sendNotification($approval_item_type, $ObjRec['approval_item_id'], $timestamp, $_SESSION['sess_id'], array("E", "C", 'A'), $ObjRec['approval_permission_key'], $item_action);

		$approval_item_type = $ObjRec['approval_item_type'];

		if($approval_item_type=='product'){
			if(strpos($ObjRec['approval_permission_key'], 'scheme') !== false){
				$approval_item_type = "scheme";
			}
		}

		$cloning_utility = ContentCloningUtility::getCloningUtility($approval_item_type);

		$mutex = new Mutex("prod_content");
		$mutex->getLock();

		if($_REQUEST['approval_approval_status'] == "AL"){
			$cloning_utility->cloneToProduction($ObjRec['approval_item_id']);
			$url = "list.php?msg=S-1017";
		} elseif($_REQUEST['approval_approval_status'] == "AR"){
			$url = "list.php?msg=S-1018";
		}

		$mutex->releaseLock();

		header("location: " . $url);
		return;
	}
	elseif ($processType == "approverHeadUpdate") {

		ignore_user_abort(true);
		set_time_limit(0);

		$values = array();
		$values['approval_id'] = $approval_id;
		$values['approval_approver_head_reply_date'] = $timestamp;
		$values['approval_approver_head_reply'] = $_REQUEST['approval_approval_status'] == "AR" ? "R" : "A";
		$values['approval_approver_head_reply_by'] = $_SESSION['sess_id'];
		$values['approval_approver_head_remarks'] = $_REQUEST['approval_approver_remarks'] ?? '';
		$values['approval_approval_status'] = "RAA";
		$values['approval_updated_by'] = $_SESSION['sess_id'];
		$values['approval_updated_date'] = $timestamp;

		DM::update('approval', $values);
		unset($values);
		header("location: list.php?msg=S-1017");
	}
}


die("!2<br><br><br>");
header("location:../dashboard.php?msg=F-1001");
return;
?>