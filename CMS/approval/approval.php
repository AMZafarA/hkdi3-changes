<?php
	include "../include/config.php";
	include $SYSTEM_BASE . "include/function.php";
	include $SYSTEM_BASE . "session.php";
	include $SYSTEM_BASE . "include/system_message.php";
	include $SYSTEM_BASE . "include/classes/DataMapper.php";
	include $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include $SYSTEM_BASE . "include/classes/MessageUtility.php";
	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$approval_id = $_REQUEST['approval_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';

	if(!isInteger($approval_id))
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	$obj_row = DM::load('approval', $approval_id);

	if($obj_row == "" || $obj_row['approval_status'] == STATUS_DISABLE)
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	if($obj_row['approval_approval_status'] == "RAA" || $obj_row['approval_approval_status'] == "RHA" || $obj_row['approval_approval_status'] == "APL"){
		if(!PermissionUtility::canApproveByKey($_SESSION['sess_id'], $obj_row['approval_permission_key'])){
			header("location: list.php?msg=F-1000");
			return;
		}
	} else {
		header("location: list.php?msg=F-1000");
		return;
	}

	foreach($obj_row as $rKey => $rValue){
		if($rKey != "approval_affected_url")
			$$rKey = htmlspecialchars($rValue);
		else
			$$rKey = $rValue;
	}

	$getItemName = '';
	if(MessageUtility::getItemName($obj_row['approval_item_type'], $obj_row['approval_item_id'])){
		$getItemName = " '" . MessageUtility::getItemName($obj_row['approval_item_type'], $obj_row['approval_item_id']) . "'";
	}

	$approval_item_type = $obj_row['approval_item_type'];
	if($approval_item_type=='product'){
		if($obj_row['approval_permission_key'] =="programmes"){
			$approval_item_type = "Programmes";
		}
		if($obj_row['approval_permission_key'] =="research"){
			$approval_item_type = "research";
			$getItemName = '';
		}
		if($obj_row['approval_permission_key'] =="financial"){
			$approval_item_type = "financial";
			$getItemName = '';
		}
		if($obj_row['approval_permission_key'] =="promotion"){
			$approval_item_type = "promotion";
			$getItemName = '';
		}
		if($obj_row['approval_permission_key'] =="news"){
			$approval_item_type = "news";
			$getItemName = '';
		}
	}



	//$item_name = htmlspecialchars(MessageUtility::getItemTypeName($approval_item_type) . $getItemName);

	$item_name =  $getItemName;

	$submitted_by = DM::load('user', $approval_submitted_by);

	$user_login = $submitted_by['user_login'];
	$user_display_name = $submitted_by['user_display_name'];

	if($approval_affected_url != "")
		$approval_affected_url = json_decode($approval_affected_url, true);

	if($approval_approval_status == "RAA" && !empty($obj_row["approval_approver_reply_by"])) {
		$submitted_by = DM::load('user', $approval_checker_reply_by);

		$checker_user_login = $submitted_by['user_login'];
		$checker_user_display_name = $submitted_by['user_display_name'];
	}

	$approve_head_approver = 'approver';
	$approver_head_user_display_name = 'N/A';
	$approver_head_user_login = 'N/A';
	$approver_head = array();
	if( $approval_approver_head_reply_by != NULL ) {
		$approver_head = DM::findOne( 'group_user', "group_user_uid = ?", "", array($approval_approver_head_reply_by));
		if($approver_head["group_user_gid"] == 40) {
			$approve_head_approver = 'head';
			$approver_head_details = DM::load('user', $approval_approver_head_reply_by);
			$approver_head_user_login = $approver_head_details['user_login'];
			$approver_head_user_display_name = $approver_head_details['user_display_name'];
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include "../header.php" ?>
		<script>
			function formSubmit(processType, result){
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					$("#processType").val(processType);
					$("#approval_approval_status").val(result);
					document.form1.submit();
				})

				$('#loadingModalDialog').modal('show');
			}

			$( document ).ready(function() {
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
	</head>
	<body>
		<?php include "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Content Approval</a></li>
					<li class="active">Process <?php echo $approval_approval_status == "RCA" ? "Checking" : "Approval" ?></li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
			<div class="row">
				<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post">
					<div class="form-group">
						<label class="col-xs-2">Updated Item :</label>
						<div class="col-xs-10">
							<?php echo $item_name ?>
						</div>
						<input type="hidden" name="type" value="approval" />
						<input type="hidden" name="processType" id="processType" value="">
						<input type="hidden" name="approval_approval_status" id="approval_approval_status" value="">
						<input type="hidden" name="approval_id" value="<?php echo $approval_id ?>" />
						<input type="hidden" name="approval_status" value="1" />
					</div>
					<div class="form-group">
						<label class="col-xs-2">Status :</label>
						<div class="col-xs-10">
							<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $approval_approval_status] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-2">CMS Url :</label>
						<div class="col-xs-10">
							<?php if(is_array($approval_affected_url)) { ?>
								<?php foreach($approval_affected_url as $url){ ?>
									<a href="<?php echo $url?>" target="_blank"  ><?php echo $url ?></a><br/>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-2">Submitted By :</label>
						<div class="col-xs-4">
							<?php echo htmlspecialchars($user_display_name) ?> (<?php echo htmlspecialchars($user_login) ?>)
						</div>
						<label class="col-xs-2">Submitted Date :</label>
						<div class="col-xs-4">
							<?php echo $approval_submit_date ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-2">Submitted Remarks :</label>
						<div class="col-xs-10">
							<?php echo nl2br($approval_submitted_remarks) ?>
						</div>
					</div>


					<?php if( $approval_approver_head_reply_by != NULL ) { ?>
						<div class="form-group">
							<label class="col-xs-2">Approved (Head) By :</label>
							<div class="col-xs-4">
								<?php echo htmlspecialchars($approver_head_user_display_name) ?> (<?php echo htmlspecialchars($approver_head_user_login) ?>)
							</div>
							<label class="col-xs-2">Approved (Head) Date :</label>
							<div class="col-xs-4">
								<?php echo $approval_approver_head_reply_date != NULL ? $approval_approver_head_reply_date : 'N/A'; ?>
							</div>
						</div>
					<?php } ?>

						<!--<div class="form-group">
							<label class="col-xs-2" style="padding-right: 0px;">Approver's Remarks :</label>
							<div class="col-xs-10">
								<textarea id="approval_approver_remarks" name="approval_approver_remarks" class="form-control" rows="8" style="resize:none;"></textarea>
							</div>
						</div>-->

				</form>
			</div>
			<div class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right">
					<?php if($approval_approval_status == "RCA") { ?>
						<button type="button" class="btn btn-primary" onclick="formSubmit('checkerUpdate', 'RAA'); return false;">
							<i class="glyphicon glyphicon-ok"></i> Approve and Submit for Approval
						</button>
						<button type="button" class="btn btn-warning" onclick="formSubmit('checkerUpdate', 'CR'); return false;">
							<i class="glyphicon glyphicon-remove"></i> Reject Request
						</button>
					<?php } elseif($approval_approval_status == "RHA") { ?>
						<button type="button" class="btn btn-primary" onclick="formSubmit('approverHeadUpdate', 'AHL'); return false;">
							<i class="glyphicon glyphicon-ok"></i> Approve Request
						</button>
						<button type="button" class="btn btn-warning" onclick="formSubmit('approverHeadUpdate', 'AHR'); return false;">
							<i class="glyphicon glyphicon-remove"></i> Reject Request
						</button>
					<?php } else { ?>
						<button type="button" class="btn btn-primary" onclick="formSubmit('approverUpdate', 'AL'); return false;">
							<i class="glyphicon glyphicon-ok"></i> Approve and Launch Now
						</button>
						<button type="button" class="btn btn-warning" onclick="formSubmit('approverUpdate', 'AR'); return false;">
							<i class="glyphicon glyphicon-remove"></i> Reject Request
						</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- End page content -->

		<?php include "../footer.php" ?>
	</body>
</html>