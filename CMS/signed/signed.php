<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

$product_signed_id = $_REQUEST['product_signed_id'] ?? '';

if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "signed")){
	header("location: ../dashboard.php?msg=F-1000");
	return;
}

$msg = $_REQUEST['msg'] ?? '';
$product_id = $_REQUEST['product_id'] ?? '';
$randomString = $_REQUEST['randomString'] ?? '';
$master_lv1 = $_REQUEST['master_lv1'] ?? '';
$master_lv2 = $_REQUEST['master_lv2'] ?? '';

if($product_id != ""){
	if(!isInteger($product_id))
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	$obj_row = DM::load('product', $product_id);

	if($obj_row == "")
	{
		header("location: list.php?msg=F-1002");
		return;
	}

	if(!file_exists($product_path . $product_id)){
		mkdir($product_path . $product_id, 0775, true);
	}

	$_SESSION['RF']['subfolder'] = "product/" . $product_id . "/";

	/*$menu = DM::findOne("menu", " menu_status = ? AND menu_link_type = ? AND menu_link_page_id = ? ", " menu_id ", array(STATUS_ENABLE, 'U', $page_id));

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "page_" . $menu['menu_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}*/

	foreach($obj_row as $rKey => $rValue){
		$$rKey = htmlspecialchars($rValue);
	}

	/* if(!file_exists($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id)){
		mkdir($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id, 0775, true);
	}*/

} else {
	/*header("location: list.php?msg=F-1002");
	return;*/
	$product_publish_status = 'D';
	foreach( DM::stub('product') as $rKey => $rValue ) {
		if ( ! isset( $$rKey ) )
			$$rKey = htmlspecialchars($rValue);
	}

	$length = 10;
	$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

	if(!file_exists($product_path . $randomString)){
		mkdir($product_path . $randomString, 0775, true);
	}

	$_SESSION['RF']['subfolder'] = "product/" . $randomString . "/";
}

$product_section = DM::findAll('product_section', ' product_section_status = ? And product_section_pid = ? ', " product_section_sequence ", array(STATUS_ENABLE,$product_id));
$this_product_section = $product_section ? end( $product_section ) : DM::stub( 'product_section' );
foreach ( $this_product_section as $rKey => $rValue ) {
	if ( ! isset( $$rKey ) )
		$$rKey = htmlspecialchars($rValue);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once "../header.php" ?>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
	<script type="text/x-tmpl" id="text_block_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-6 control-label">
						Text Section
					</label>
					<span class="col-xs-6" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="product_section_type" value="T"/>
						<input type="hidden" name="product_section_id" value=""/>
						<input type="hidden" name="product_section_tid" value=""/>
						<input type="hidden" name="product_section_sequence" value=""/>
					</span>
				</div>
				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="product_section_content_lang3"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</div>
			</form>
		</div>
	</script>


	<script type="text/x-tmpl" id="text_img_block_template">
		<div id="section-row-{%=o.tmp_id%}" class="row section_row">
			<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-bottom: 5px;">
					<label class="col-xs-2 control-label">
						Text and image Block
					</label>
					<label class="col-xs-1 control-label">
						Image
					</label>
					<div class="col-xs-5">
						<input type="file" id="product-row-{%=o.tmp_id%}-image" name="product_section_image" class="product_section_image" accept="image/*" onchange="fileSelected(this);">
						<p style="padding-top:5px;padding-left:5px;">
							<!--(Width: <?php echo $NEWS_IMAGE_WIDTH ?>px, Height: <?php echo $NEWS_IMAGE_HEIGHT ?>px)-->
						</p>
					</div>
					<?php if($product_section_image != "" && file_exists($product_path . $product_id . "/" . $product_section_image)) { ?>
						<div class='col-xs-2'>
							<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_section_image ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
						</div>
					<?php } ?>

					<span class="col-xs-4" style="text-align:right">
						<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
							<i class="glyphicon glyphicon-arrow-up"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
							<i class="glyphicon glyphicon-arrow-down"></i>
						</button>
						<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<input type="hidden" name="product_section_type" value="TI"/>
						<input type="hidden" name="product_section_id" value=""/>
						<input type="hidden" name="product_section_tid" value=""/>
						<input type="hidden" name="product_section_sequence" value=""/>
					</span>
				</div>

				<div class="form-group">
					<label class="col-xs-2 control-label" style="padding-right:0px">Image Position</label>
					<div class="col-xs-6">
						<input type="radio" name="product_section_image_position" value="1" checked > Left
						<input type="radio" name="product_section_image_position" value="2"> Right
					</div>
				</div>


				<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
					<ul>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
						<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
					</ul>
					<div id="tabs-tmp-{%=o.tmp_id%}-1">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-2">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
							</div>
						</div>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}-3">
						<div class="form-group">
							<div class="col-xs-12">
								<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="product_section_content_lang3"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</div>
			</form>
		</div>
	</script>


	<script type="text/x-tmpl" id="file_template">
		<div class="form-group file_row" style="margin-bottom:15px;">

			<div class="col-xs-3" style="text-align:left">
				<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
					<i class="glyphicon glyphicon-download-alt"></i>
				</a>
				<button type="button" class="btn btn-default" onclick="removeFileItem(this); return false;">
					<i class="glyphicon glyphicon-remove"></i>
				</button>
				<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
				<input type="hidden" name="project_image_id" value=""/>
				<input type="hidden" name="product_section_file_lang" value="{%=o.language%}"/>
			</div>

		</div>
	</script>

	<script>

		var file_list = {};
		var counter = 0;

		function checkForm(){
			var flag=true;
			var errMessage="";


			$('#loadingModalDialog').on('shown.bs.modal', function (e) {

				//var obj = {"product_section_type":"F"};
				//obj['product_section_image'] = [];
				//cloneFile(obj,0);

				var frm = document.form1;
				frm.processType.value = "";
				cloneTextBlock(0);


			})

			$('#loadingModalDialog').modal('show');

		}

		function checkForm2(){
			var flag=true;

			$('#approvalModalDialog').modal('show');

		}


		function cloneTextBlock(idx){
			if($("input[name='product_section_type'][value='T']").length <= idx){
				cloneTextImageBlock(0);
				return;
			}

			var frm = $("input[name='product_section_type'][value='T']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			var data = JSON.stringify(cloneFormToObject(frm));
			$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
			cloneTextBlock(++idx);
		}

		function cloneTextImageBlock(idx){
			if($("input[name='product_section_type'][value='TI']").length <= idx){
				document.form1.submit();
				return;
			}

			var frm = $("input[name='product_section_type'][value='TI']:eq(" + idx + ")").closest("form");
			trimForm($(frm).attr("id"));

			if($(frm).find("div.file_row").length > 0){
				var row = $(frm).find("div.file_row:eq(0)");

				var fileObj = {};
				fileObj['project_image_alt'] = $(row).find("input[name='project_image_alt']").val();
				fileObj['project_image_id'] = $(row).find("input[name='project_image_id']").val();
				fileObj['tmp_filename'] = "";
				fileObj['filename'] = "";

				var fd = new FormData();
				fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

				$.ajax({
					url: "../pre_upload/act.php",
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					dataType : "json",
					success: function(data) {
						fileObj['tmp_filename'] = data.tmpname;
						fileObj['filename'] = data.filename;
						$('<input>').attr('name','tmp_filename').attr('type','hidden').val(data.tmpname).appendTo(frm);
						$('<input>').attr('name','filename').attr('type','hidden').val(data.filename).appendTo(frm);

						var data = JSON.stringify(cloneFormToObject(frm));
						$('<input>').attr('name','text_image_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
						cloneTextImageBlock(++idx);
					}
				});
			}else{
				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','text_image_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneTextImageBlock(++idx);
			}
			/*if($(frm).find("div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','file_section[]').attr('type','hidden').val(data).appendTo('#content');
				//document.form1.submit();
				return;
			}*/


		}


		function cloneFile(dataObj, elementIdx){
			var frm = $("#content");

			if($(frm).find("div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','file_section[]').attr('type','hidden').val(data).appendTo('#content');
				//document.form1.submit();
				return;
			}

			var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");

			//var files =$("input[name='project_image_filename']")[0].files;

			/*if(files.length == 0){

				return;
			}*/


			var fileObj = {};
			fileObj['project_image_alt'] = $(row).find("input[name='project_image_alt']").val();
			fileObj['project_image_id'] = $(row).find("input[name='project_image_id']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";


			var fd = new FormData();
			//fd.append('upload_file', files[0]);
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;
					console.log(data.filename);
					dataObj['product_section_image'].push(fileObj);
					cloneFile(dataObj, ++elementIdx);
					//$('<input>').attr('name','file_section[]').attr('type','hidden').val(JSON.stringify(dataObj)).appendTo('#form1');

					//document.form1.submit();
				}
			});
		}


		function fileSelected(fileBrowser){
			if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
				return;

			var files = $(fileBrowser)[0].files;
			for(var i = 0; i < files.length; i++){
				var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++)};
				file_list[obj.tmp_id] = files[i];
				var content = tmpl("file_template", obj);
				//var header_row = $(fileBrowser).closest("div.col-xs-5");
				$(fileBrowser).append("div.col-xs-5").html(content);

			}
		}



		function moveUp(button){
			var $current = $(button).closest(".section_row");
			var $previous = $current.prev('.section_row');
			if($previous.length !== 0){
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce().remove();
				});
				$current.insertBefore($previous);
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce(tinymceConfig);
				});
			}
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function moveDown(button){
			var $current = $(button).closest(".section_row");
			var $next = $current.next('.section_row');
			if($next.length !== 0){
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce().remove();
				});
				$current.insertAfter($next);
				$current.find("textarea").each(function(idx, element){
					$(element).tinymce(tinymceConfig);
				});
			}
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function removeRow(button){
			$(button).closest(".section_row").detach();
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		/*function addTextBlock(tab_id){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("text_block_template", obj);
			$("#tabs-"+tab_id).append(content);
			$("input[name='product_section_tid']").val(tab_id);
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}*/

		function addTextBlock(){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("text_block_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
		}

		function addTextImageBlock(tab_id){
			var tmp_id = "tmp_" + (counter++);
			var obj = {tmp_id:tmp_id};
			var content = tmpl("text_img_block_template", obj);
			$(content).insertBefore("#submit_button_row");
			$("#tabs-tmp-" + tmp_id).tabs({});
			$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
			$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			$("input[name='product_section_sequence']").each(function(idx, element){
				$(element).val(idx + 1);
			});
			$(".product_section_image").filestyle({buttonText: ""});
		}

		function submitForm(){
			document.form1.submit();
		}

		function removeIcon(button){
			$("#remove_icon").val("Y");
			$(button).closest("div").detach();
		}

		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form1;
					frm.processType.value = "submit_for_approval";
					cloneTextBlock(0);
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		$( document ).ready(function() {
			$("#tabs").tabs({});

			$("#upload_icon").filestyle({buttonText: ""});
			$("#product_banner,.product_section_image,#product_thumb_banner").filestyle({buttonText: ""});

			$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});

			$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
				$(element).find("div.col-xs-12[id^='tabs']").tabs({});
				$(element).find('textarea').tinymce(tinymceConfig);
			});

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>
		});
	</script>
	<style>
		.vcenter {
			display: inline-block;
			vertical-align: middle;
			float: none;
		}
		.section_row {
			margin-bottom:10px;
			padding: 7px 15px 4px;
			border: 1px solid #ccc;
			border-radius: 4px;
		}
	</style>
</head>
<body>
	<?php include_once "../menu.php" ?>

	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li class="active"><a href="../issue/list.php">Signed</a></li>

				<?php
				$issue = DM::load("issue",$product_signed_id );
				?>

				<li class="active"><a href="list.php?product_signed_id=<?php echo $product_signed_id?>"><?php echo $issue['issue_name_lang1']?></a></li>

				<li class="active"><?php echo $product_id == "" ? "Create" : "Update" ?></li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				&nbsp;
			</span>
		</div>

		<div id="top_submit_button_row" class="row" style="padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="previewPost(); return false;">
					<i class="fa fa-eye fa-lg"></i> Preview
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>

		<div class="row">
			<table class="table" style="margin-bottom: 5px;">
				<tbody>
					<tr class="<?php echo ($product_publish_status == "RCA" || $product_publish_status == "RAA") ? "danger" : "" ?>">
						<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
						<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
							<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $product_publish_status] ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td class="col-xs-12" style="vertical-align:middle" colspan="2">
								<div class="row">
									<div class="col-xs-2">
										<a role="button" class="btn btn-default" onclick="addTextBlock(); return false;">
											<i class="fa fa-list fa-lg"></i> Text Section
										</a>
									</div>
									<div class="col-xs-2">
										<a role="button" class="btn btn-default" onclick="addTextImageBlock(); return false;">
											<i class="fa fa-list fa-lg"></i> Text and Image Section
										</a>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Signed Cover</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="product_banner" name="product_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										(Width: 614px, Height: 570px)
									</p>
								</div>
								<?php if($product_banner != "" && file_exists($product_path . $product_id . "/" . $product_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
							</td>
						</tr>

						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Signed Thumb</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="product_thumb_banner" name="product_thumb_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										(Width: 1277px, Height: 479px)
									</p>
								</div>
								<?php if($product_thumb_banner != "" && file_exists($product_path . $product_id . "/" . $product_thumb_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_thumb_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (English)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="product_name_lang1" placeholder="Title (English)" value="<?php echo $product_name_lang1?>">
							</td>
						</tr>
						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (繁中)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="product_name_lang2" placeholder="Title (繁中)" value="<?php echo $product_name_lang2?>">
							</td>
						</tr>
						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Title (簡中)</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="product_name_lang3" placeholder="Title (簡中)" value="<?php echo $product_name_lang3?>">
							</td>
						</tr>
						<tr>
							<td class="col-xs-2" style="vertical-align:middle">Priority</td>
							<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" name="product_seq" placeholder="Priority" value="<?php echo $product_seq?>">
							</td>
						</tr>


					</tbody>
				</table>
			</div>
			<input type="hidden" name="type" value="product" />
			<input type="hidden" name="processType" id="processType" value="">
			<input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
			<input type="hidden" name="product_type" value="2" />
			<input type="hidden" name="product_status" value="1" />
			<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
			<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
			<input type="hidden" name="randomString" value="<?php echo $randomString ?>" />
			<input type="hidden" name="master_lv1" id="master_lv1" value="<?php echo $master_lv1 ?>" />
			<input type="hidden" name="master_lv2" id="master_lv2" value="<?php echo $master_lv2 ?>" />
			<input type="hidden" name="product_signed_id" id="product_signed_id" value="<?php echo $product_signed_id ?>" />


		</form>
		
		<?php $act = "//".$_SERVER['HTTP_HOST']."/en/"; ?>
		<script>function previewPost() {document.getElementById('preview-form').submit();}</script>
		<form id="preview-form" target="_blank" method="get" action="<?php echo $act;?>news/publication-detail.php" style="display: none;"><input type="text" name="product_id" value="<?php echo $product_id; ?>"><input type="text" name="preview" value="true"></form>

		<?php foreach($product_section as $value) {
			if($value['product_section_type'] == "T") {
				printTextBlock($value);
			} else if($value['product_section_type'] == "TI"){
				printTextImageBlock($value,$product_path,$product_url);
			}
		}?>


		<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="previewPost(); return false;">
					<i class="fa fa-eye fa-lg"></i> Preview
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button>
			</div>
		</div>
	</div>

	<!-- End page content -->
	<?php include_once "../footer.php" ?>
</body>
</html>

<?php
function printTextBlock($record){
	$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-6 control-label">
					Text Block
				</label>

				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="T"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="product_section_content_lang3"><?php echo htmlspecialchars($record['product_section_content_lang3']) ?></textarea>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	<?php
}
?>


<?php
function printTextImageBlock($record,$product_path,$product_url) {
	global $NEWS_IMAGE_WIDTH, $NEWS_IMAGE_HEIGHT;
	$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
	?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
				<label class="col-xs-2 control-label">
					Text and image Block
				</label>
				<label class="col-xs-1 control-label">
					Image
				</label>
				<div class="col-xs-5">
					<input type="file" id="product_section_image-<?php echo $row_id  ?>" name="product_section_image" class="product_section_image" accept="image/*" onchange="fileSelected(this);"/>
					<p style="padding-top:5px;padding-left:5px;">
						<!--(Width: <?php echo $NEWS_IMAGE_WIDTH ?>px, Height: <?php echo $NEWS_IMAGE_HEIGHT ?>px)-->
					</p>
				</div>
				<div class='col-xs-1'>
					<?php if($record['product_section_image'] != "" && file_exists($product_path . $record['product_section_pid'] . "/" .$record['product_section_id']. "/" . $record['product_section_image'])) { ?>

						<a role="button" class="btn btn-default" href="<?php echo $product_url . $record['product_section_pid'] . "/" .$record['product_section_id']. "/" . $record['product_section_image'] ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
					<?php } ?>
				</div>
				<span class="col-xs-3" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="TI"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>

			<div class="form-group">
				<label class="col-xs-2 control-label" style="padding-right:0px">Image Position</label>
				<div class="col-xs-6">
					<input type="radio" name="product_section_image_position" value="1" <?php echo $record['product_section_image_position']=='1'?'checked':'' ?> > Left
					<input type="radio" name="product_section_image_position" value="2" <?php echo $record['product_section_image_position']=='2'?'checked':'' ?>> Right
				</div>
			</div>

			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="product_section_content_lang3"><?php echo htmlspecialchars($record['product_section_content_lang3']) ?></textarea>
						</div>
					</div>
				</div>

			</div>
		</form>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</div>
	<?php
}
?>