<?php



	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$type_name = "Talent";
	$permission_name = "edt";

	if($type=="talent" )

	{

		$id = $_REQUEST['talent_id'] ?? '';


		$type_name = "Talent";


		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "edt")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}





		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['talent_publish_status'] = "D";

				$values['talent_status'] = STATUS_ENABLE;

				$values['talent_created_by'] = $_SESSION['sess_id'];

				$values['talent_created_date'] = $timestamp;

				$values['talent_updated_by'] = $_SESSION['sess_id'];

				$values['talent_updated_date'] = $timestamp;

				$values['talent_type'] = "1";



				$id = DM::insert('talent', $values);


				if(!file_exists($talent_path . $id. "/")){
					mkdir($talent_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('talent', $id);



				if($ObjRec['talent_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}


				$values['talent_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['talent_updated_by'] = $_SESSION['sess_id'];

				$values['talent_updated_date'] = $timestamp;



				DM::update('talent', $values);

			}


			$ObjRec = DM::load('talent', $id);

			$talent_img_arr = array('talent_image','talent_thumb');

			foreach($talent_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($talent_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($talent_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($talent_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


						list($width, $height) = getimagesize($talent_path . $id . "/".$newname);

						if($val =='talent_image'){
							$newWidth ="50";
							$newHeight="80";
						}else{
							$newWidth ="410";
							$newHeight="290";
						}

						cropImage($talent_path. $id."/". $newname, $newWidth,$newHeight , $talent_path. $id."/". $newname);

						/*if($width > '1500'){
							resizeImageByWidth($talent_path. $id."/". $newname, "1500" , $talent_path. $id."/". $newname);
						}else if($height > '1000'){
							resizeImageByHeight($talent_path. $id."/". $newname, "1000" , $talent_path. $id."/". $newname);
						}*/



					$values = array();
					$values[$val] = $newname;
					$values['talent_id'] = $id;

					DM::update('talent', $values);
					unset($values);

					$ObjRec = DM::load('talent', $id);
				}
			}


			//*************add image**************//
			$files = $_REQUEST['file_section'] ?? '';
			if($files != "" && is_array($files)){
				foreach($files as $file){
					$file = json_decode($file, true);

					$lang1_files = $file['lang1_files'];
					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){
							if($lang1_file['talent_image_id'] == "") {

								$lang1_file['talent_image_pid'] = $id;
								$lang1_file['talent_image_seq'] = $count++;
								$lang1_file['talent_image_filename'] = $lang1_file['filename'];
								$lang1_file['talent_image_alt'] = $lang1_file['talent_image_alt'];
								$lang1_file['talent_image_status'] = STATUS_ENABLE;
								$lang1_file['talent_image_created_by'] = $_SESSION['sess_id'];
								$lang1_file['talent_image_created_date'] = $timestamp;
								$lang1_file['talent_image_updated_by'] = $_SESSION['sess_id'];
								$lang1_file['talent_image_updated_date'] = $timestamp;

								$file_section_id = DM::insert('talent_image', $lang1_file);

								$file_section_list[] = $file_section_id;

								if(!file_exists($talent_path . $id. "/".$file_section_id."/")){
									mkdir($talent_path . $id . "/".$file_section_id."/", 0775, true);
								}

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);


									list($width, $height) = getimagesize($talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);

									/*if($width > '1000'){
										resizeImageByWidth($talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}else if($height > '1000'){
										resizeImageByHeight($talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}*/

									$newWidth ="640";
									$newHeight="770";


									//cropImage($talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], $newWidth,$newHeight , $$talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename'],);


									$crop = $talent_path . $id . "/".$file_section_id."/". "crop_".$lang1_file['filename'];
									//cropImage($talent_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "125","100" , $crop);

							}else{
								$file_section_list[] = $lang1_file['talent_image_id'];
								$file_section_id = $lang1_file['file_section_id'] ?? '';
								$lang1_file['talent_image_seq'] = $count++;
								$lang1_file['talent_image_update_by'] = $_SESSION['sess_id'];
								$lang1_file['talent_image_update_date'] = $timestamp;

								DM::update('talent_image', $lang1_file);
							}
						}
					}
				}
			}

			$sql = " talent_image_status = ? AND talent_image_pid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($file_section_list) > 0){ print_r($file_section_list);
				$sql .= " AND talent_image_id NOT IN (" . implode(',', array_fill(0, count($file_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $file_section_list);

			}

			$talent_section_files = DM::findAll("talent_image", $sql, " talent_image_seq ", $parameters);

			foreach($talent_section_files as $talent_section_file){

				$values = array();
				$values['talent_image_id'] = $talent_section_file['talent_image_id'];
				$values['talent_image_status'] = STATUS_DISABLE;
				$values['talent_image_update_by'] = $_SESSION['sess_id'];
				$values['talent_image_update_date'] = $timestamp;

				DM::update('talent_image', $values);

				unset($values);
			}

			//*************add image**************//



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "talent '" . $ObjRec['talent_name_lang1'] . "' (ID:" . $ObjRec['talent_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( $permission_name,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['master_lecture_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $permission_name, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."talent/talent.php?talent_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( $permission_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);

			}

			header("location:talent.php?talent_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('talent', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['talent_status'] == STATUS_DISABLE)

			{

				header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("talent", $id);

			$values['talent_id'] = $id;
			$values['talent_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['talent_status'] = STATUS_DISABLE;
			$values['talent_updated_by'] = $_SESSION['sess_id'];
			$values['talent_updated_date'] = getCurrentTimestamp();


			DM::update('talent', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval($permission_name, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "master_lecture '" . $ObjRec['master_lecture_name_lang1'] . "' (ID:" . $ObjRec['master_lecture_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $$permission_name, "DS");

				$files = array($SYSTEM_HOST."talent/talent.php?talent_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest($permission_name, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, "D");
			}





			header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('talent', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['talent_status'] == STATUS_ENABLE || $ObjRec['talent_publish_status'] == 'AL')

			{

				header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=F-1006");

				return;

			}



			$values['talent_id'] = $id;

			$values['talent_publish_status'] = 'D';

			$values['talent_status'] = STATUS_ENABLE;

			$values['talent_updated_by'] = $_SESSION['sess_id'];

			$values['talent_updated_date'] = getCurrentTimestamp();



			DM::update('talent', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'talent', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of talent '" . $ObjRec['talent_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $permission_name, "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "talent '" . $ObjRec['talent_name_lang1'] . "' (ID:" . $ObjRec['talent_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $permission_name, "WA");



			header("location: list.php?talent_lv1=".$talent_lv1."&talent_lv2=".$talent_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
