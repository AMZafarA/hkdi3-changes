<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="news" )

	{

		$id = $_REQUEST['news_id'] ?? '';
		$news_lv1 = $_REQUEST['news_lv1'] ?? '';
		$news_lv2 = $_REQUEST['news_lv2'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "news_" . $news_lv1."_".$news_lv2)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();

			$values['news_index_show'] = $values['news_index_show'];

			if($isNew){

				$values['news_publish_status'] = "D";

				$values['news_status'] = STATUS_ENABLE;

				$values['news_created_by'] = $_SESSION['sess_id'];

				$values['news_created_date'] = $timestamp;

				$values['news_updated_by'] = $_SESSION['sess_id'];

				$values['news_updated_date'] = $timestamp;

				$id = DM::insert('news', $values);

				if(!file_exists($news_path . $id. "/")){
					mkdir($news_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('news', $id);



				if($ObjRec['news_status'] == STATUS_DISABLE)

				{

					header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=F-1002");

					return;

				}


				$values['news_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['news_updated_by'] = $_SESSION['sess_id'];

				$values['news_updated_date'] = $timestamp;



				DM::update('news', $values);

			}


			$ObjRec = DM::load('news', $id);

			$news_img_arr = array('news_image1','news_image2','news_thumb');

			foreach($news_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($news_path . $id . "/" . $ObjRec[$val])){
//@unlink($news_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($news_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($news_path . $id . "/".$newname);

					if($val =='news_thumb'){
						$newWidth ="323";
						$newHeight="265";
					}else{
						$newWidth ="699";
						$newHeight="462";
					}

					cropImage($news_path. $id."/". $newname, $newWidth,$newHeight , $news_path. $id."/". $newname);


					$values = array();
					$values[$val] = $newname;
					$values['news_id'] = $id;

					DM::update('news', $values);
					unset($values);

					$ObjRec = DM::load('news', $id);
				}
			}



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "news '" . $ObjRec['news_name_lang1'] . "' (ID:" . $ObjRec['news_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_" . $id, "Product '" . $ObjRec['product_eng_name'] . "' is updated by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "news_" . $news_lv1."_".$news_lv2,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['news_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), "product_".$id, $isNew ? "CS" : "ES");
				$files = array($host_name."en/news/news.php", $host_name."tc/news/news.php", $host_name."sc/news/news.php");

				$approval_id = ApprovalService::createApprovalRequest( "news_" . $news_lv1."_".$news_lv2, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				//NotificationService::sendApprovalRequest("product", $id, $timestamp, $_SESSION['sess_id'], array("A"), "product_".$id, "PA", $approval_id);

			}

			header("location:news.php?news_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('news', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['news_status'] == STATUS_DISABLE)

			{

				header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("news", $id);

			$values['news_id'] = $id;
			$values['news_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['news_status'] = STATUS_DISABLE;
			$values['news_updated_by'] = $_SESSION['sess_id'];
			$values['news_updated_date'] = getCurrentTimestamp();


			DM::update('news', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("news_" . $news_lv1."_".$news_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "news '" . $ObjRec['news_name_lang1'] . "' (ID:" . $ObjRec['news_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("news_" . $news_lv1."_".$news_lv2, $id, $timestamp, $_SESSION['sess_id'], array("E"), "news_" . $news_lv1."_".$news_lv2, "DS");

				$files = array($host_name."en/news/news.php", $host_name."tc/news/news.php", $host_name."sc/news/news.php");

				$approval_id = ApprovalService::createApprovalRequest("news_" . $news_lv1."_".$news_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("news_" . $news_lv1."_".$news_lv2, $id, $timestamp, $_SESSION['sess_id'], array("A"), "news_" . $news_lv1."_".$news_lv2, "PA", $approval_id);
			}





			header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('news', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['news_status'] == STATUS_ENABLE || $ObjRec['news_publish_status'] == 'AL')

			{

				header("location: list.php?news_lv1=".$news_lv1."&news_lv2=".$news_lv2."&msg=F-1006");

				return;

			}



			$values['news_id'] = $id;

			$values['news_publish_status'] = 'D';

			$values['news_status'] = STATUS_ENABLE;

			$values['news_updated_by'] = $_SESSION['sess_id'];

			$values['news_updated_date'] = getCurrentTimestamp();



			DM::update('news', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'news', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of news '" . $ObjRec['news_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'product_' . $id, $message);

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "news '" . $ObjRec['news_name_lang1'] . "' (ID:" . $ObjRec['news_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), 'product_' . $id, "Product '" . $ObjRec['product_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
