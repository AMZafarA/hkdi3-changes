<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="continuing_edcation" ) {

		$id = $_REQUEST['continuing_edcation_id'] ?? '';
		$continuing_edcation_lv1 = $_REQUEST['continuing_edcation_lv1'] ?? '';
		$continuing_edcation_lv2 = $_REQUEST['continuing_edcation_lv2'] ?? '';

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "continuing_edcation")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['continuing_edcation_publish_status'] = "D";

				$values['continuing_edcation_status'] = STATUS_ENABLE;

				$values['continuing_edcation_created_by'] = $_SESSION['sess_id'];

				$values['continuing_edcation_created_date'] = $timestamp;

				$values['continuing_edcation_updated_by'] = $_SESSION['sess_id'];

				$values['continuing_edcation_updated_date'] = $timestamp;

				$values['continuing_edcation_type'] = "1";



				$id = DM::insert('continuing_edcation', $values);


				if(!file_exists($continuing_edcation_path . $id. "/")){
					mkdir($continuing_edcation_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('continuing_edcation', $id);



				if($ObjRec['continuing_edcation_status'] == STATUS_DISABLE)

				{

					header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=F-1002");

					return;

				}


				$values['continuing_edcation_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['continuing_edcation_updated_by'] = $_SESSION['sess_id'];

				$values['continuing_edcation_updated_date'] = $timestamp;



				DM::update('continuing_edcation', $values);

			}


			$ObjRec = DM::load('continuing_edcation', $id);

			$continuing_edcation_img_arr = array('continuing_edcation_image','continuing_edcation_thumb');

			foreach($continuing_edcation_img_arr as $key=>$val){
				if ( isset( $_FILES[$val] ) ) {
					if($_FILES[$val]['tmp_name'] != ""){

						if($ObjRec[$val] != "" && file_exists($continuing_edcation_path . $id . "/" . $ObjRec[$val])){
						//	@unlink($continuing_edcation_path  . $id . "/" . $ObjRec[$val]);
						}

						$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

						$newname = moveFile($continuing_edcation_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


							list($width, $height) = getimagesize($continuing_edcation_path . $id . "/".$newname);

							/*if($width > '1500'){
								resizeImageByWidth($continuing_edcation_path. $id."/". $newname, "1500" , $continuing_edcation_path. $id."/". $newname);
							}else if($height > '1000'){
								resizeImageByHeight($continuing_edcation_path. $id."/". $newname, "1000" , $continuing_edcation_path. $id."/". $newname);
							}*/



						$values = array();
						$values[$val] = $newname;
						$values['continuing_edcation_id'] = $id;

						DM::update('continuing_edcation', $values);
						unset($values);

						$ObjRec = DM::load('continuing_edcation', $id);
					}
				}
			}


			//*************add image**************//
			$files = $_REQUEST['file_section'] ?? '';
			if($files != "" && is_array($files)){
				foreach($files as $file){
					$file = json_decode($file, true);

					$lang1_files = $file['lang1_files'];
					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){
							if($lang1_file['continuing_edcation_image_id'] == "" || $lang1_file['filename'] !="") {

								$lang1_file['continuing_edcation_image_pid'] = $id;
								$lang1_file['continuing_edcation_image_seq'] = $count++;
								$lang1_file['continuing_edcation_image_filename'] = $lang1_file['filename'];
								$lang1_file['continuing_edcation_image_alt'] = $lang1_file['continuing_edcation_image_alt'];
								$lang1_file['continuing_edcation_image_status'] = STATUS_ENABLE;
								$lang1_file['continuing_edcation_image_created_by'] = $_SESSION['sess_id'];
								$lang1_file['continuing_edcation_image_created_date'] = $timestamp;
								$lang1_file['continuing_edcation_image_updated_by'] = $_SESSION['sess_id'];
								$lang1_file['continuing_edcation_image_updated_date'] = $timestamp;

								$file_section_id = DM::insert('continuing_edcation_image', $lang1_file);

								$file_section_list[] = $file_section_id;

								if(!file_exists($continuing_edcation_path . $id. "/".$file_section_id."/")){
									mkdir($continuing_edcation_path . $id . "/".$file_section_id."/", 0775, true);
								}

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);


									list($width, $height) = getimagesize($continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);

									if($width > '1000'){
										resizeImageByWidth($continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}else if($height > '1000'){
										resizeImageByHeight($continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "1000" , $continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}


									$crop = $continuing_edcation_path . $id . "/".$file_section_id."/". "crop_".$lang1_file['filename'];
									cropImage($continuing_edcation_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "125","100" , $crop);

							}else{
								$file_section_list[] = $lang1_file['continuing_edcation_image_id'];
								$file_section_id = $lang1_file['file_section_id'] ?? '';
								$lang1_file['continuing_edcation_image_seq'] = $count++;
								$lang1_file['continuing_edcation_image_update_by'] = $_SESSION['sess_id'];
								$lang1_file['continuing_edcation_image_update_date'] = $timestamp;

								DM::update('continuing_edcation_image', $lang1_file);
							}
						}
					}
				}
			}

			$sql = " continuing_edcation_image_status = ? AND continuing_edcation_image_pid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($file_section_list) > 0){ print_r($file_section_list);
				$sql .= " AND continuing_edcation_image_id NOT IN (" . implode(',', array_fill(0, count($file_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $file_section_list);

			}

			$continuing_edcation_section_files = DM::findAll("continuing_edcation_image", $sql, " continuing_edcation_image_seq ", $parameters);

			foreach($continuing_edcation_section_files as $continuing_edcation_section_file){

				$values = array();
				$values['continuing_edcation_image_id'] = $continuing_edcation_section_file['continuing_edcation_image_id'];
				$values['continuing_edcation_image_status'] = STATUS_DISABLE;
				$values['continuing_edcation_image_update_by'] = $_SESSION['sess_id'];
				$values['continuing_edcation_image_update_date'] = $timestamp;

				DM::update('continuing_edcation_image', $values);

				unset($values);
			}

			//*************add image**************//



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "continuing_edcation '" . $ObjRec['continuing_edcation_name_lang1'] . "' (ID:" . $ObjRec['continuing_edcation_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification('continuing_edcation', $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $id, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "continuing_edcation",$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['continuing_edcation_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("continuing_edcation", $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."continuing_edcation/continuing_edcation.php?continuing_edcation_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "continuing_edcation", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("continuing_edcation", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);

			}

			header("location:continuing_edcation.php?continuing_edcation_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('continuing_edcation', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['continuing_edcation_status'] == STATUS_DISABLE)

			{

				header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("continuing_edcation", $id);

			$values['continuing_edcation_id'] = $id;
			$values['continuing_edcation_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['continuing_edcation_status'] = STATUS_DISABLE;
			$values['continuing_edcation_updated_by'] = $_SESSION['sess_id'];
			$values['continuing_edcation_updated_date'] = getCurrentTimestamp();


			DM::update('continuing_edcation', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("continuing_edcation", $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "continuing_edcation '" . $ObjRec['continuing_edcation_name_lang1'] . "' (ID:" . $ObjRec['continuing_edcation_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("continuing_edcation", $id, $timestamp, $_SESSION['sess_id'], array("E"), "continuing_edcation", "DS");

				$files = array($host_name."en/continuing_edcation/continuing_edcation.php", $host_name."tc/continuing_edcation/continuing_edcation.php", $host_name."sc/continuing_edcation/continuing_edcation.php");

				$approval_id = ApprovalService::createApprovalRequest("continuing_edcation", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("continuing_edcation", $id, $timestamp, $_SESSION['sess_id'], array("A"), "continuing_edcation", "PA", $approval_id);
			}





			header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('continuing_edcation', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['continuing_edcation_status'] == STATUS_ENABLE || $ObjRec['continuing_edcation_publish_status'] == 'AL')

			{

				header("location: list.php?continuing_edcation_lv1=".$continuing_edcation_lv1."&continuing_edcation_lv2=".$continuing_edcation_lv2."&msg=F-1006");

				return;

			}



			$values['continuing_edcation_id'] = $id;

			$values['continuing_edcation_publish_status'] = 'D';

			$values['continuing_edcation_status'] = STATUS_ENABLE;

			$values['continuing_edcation_updated_by'] = $_SESSION['sess_id'];

			$values['continuing_edcation_updated_date'] = getCurrentTimestamp();



			DM::update('continuing_edcation', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'continuing_edcation', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of continuing_edcation '" . $ObjRec['continuing_edcation_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'product_' . $id, $message);

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "continuing_edcation '" . $ObjRec['continuing_edcation_name_lang1'] . "' (ID:" . $ObjRec['continuing_edcation_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], array("E"), 'product_' . $id, "Product '" . $ObjRec['product_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");



			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
