<?php

@ini_set('memory_limit','1024M');
ini_set('max_execution_time', 6000);
ini_set('gd.jpeg_ignore_warning', 1);
@ini_set("display_errors","On");

	include "../include/config.php";
	include $SYSTEM_BASE . "include/function.php";

	include $SYSTEM_BASE . "session.php";

	include $SYSTEM_BASE . "include/classes/DataMapper.php";
	include $SYSTEM_BASE . "include/classes/PHPExcel.php";
	include $SYSTEM_BASE . "include/classes/SearchUtil.php";

	include $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include $SYSTEM_BASE . "include/classes/PermissionUtility.php";



	$rsvp_id = $_REQUEST['rsvp_id'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "rsvp") ){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$result = DM::findAll('rsvp_reg','rsvp_id = ?','rsvp_id',array($rsvp_id));


$objPHPExcel = new PHPExcel(); 

 //實例化PHPExcel類，等同於在桌面上創建一個ecxel表格
     //$objPHPExcel->createSheet();  //創建新的內置表    執行一次創建一個新的一頁
     //$objPHPExcel->setActiveSheetIndex(1);//把新創建的的sheet設定微當前活動sheet
     $objSheet=$objPHPExcel->getActiveSheet();//獲取當前活動sheet的操作對象
    $objSheet->setTitle('dome2');

$arr=array(

        array('Title','First Name','Last Name','Company','Phone','Email','Job Title','address','Message','no. of ticket','Waitlist','Language','Reg Date')

    );

foreach ($result as $key => $value) {
	if($value['rsvp_reg_waitlist']){
		$rsvp_reg_waitlist = 'Y';
	}else{
		$rsvp_reg_waitlist = 'N';
	}

	if($value['rsvp_reg_lang'] == 'lang1'){
		$rsvp_reg_lang = 'EN';
	}else if($value['rsvp_reg_lang'] == 'lang2'){
		$rsvp_reg_lang = 'TC';
	}else if($value['rsvp_reg_lang'] == 'lang3'){
		$rsvp_reg_lang = 'SC';
	}

$arr[] = array($value['rsvp_reg_title'],$value['rsvp_reg_first_name'],$value['rsvp_reg_last_name'],$value['rsvp_reg_company'],$value['rsvp_reg_phone'],$value['rsvp_reg_email'],$value['rsvp_reg_job_title'],$value['rsvp_reg_address'],$value['rsvp_reg_message'],$value['rsvp_reg_no_ticket'],$rsvp_reg_waitlist ,$rsvp_reg_lang,$value['rsvp_reg_date']);
}

$objSheet->fromArray($arr);//直接加載數據塊來實現填充數據

    $objWrite=PHPExcel_IOFactory::createWriter($objPHPExcel,"Excel2007");//按照指定格式生成excel文檔
    //$objWrite->save($dir."/demo_3.xlsx");//保存到當前文檔夾下
     
     browser_export("Excel2007",'excel.xlsx');  //不保存在當前文檔夾下，直接輸出至瀏覽器
    $objWrite->save('php://output');           //保存
     
     function browser_export($type,$filename){  //聲明一個方法  判斷保存 保存格式
         if($type=='Excel5'){ 
             header('Content-Type: application/vnd.ms-excel');
        }else{
             header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        }
         header('Content-Disposition: attachment;filename="'.$filename.'"');//告訴瀏覽器 輸出的文檔名稱
        header('Cache-Control: max-age=0');//禁止緩存
    }

	exit;

			
	


	


	$reader= PHPExcel_IOFactory::createReaderForFile($excel_file );
	$reader->setReadDataOnly(true);
	$excel= $reader->load($excel_file );
	
	$sheet=$excel->getActiveSheet();
	$sheet->getCell('A8')->getValue();
	
	
	$sql = "SHOW COLUMNS FROM $exDB FROM jma";
	$query = mysql_query($sql, $link);
	while ($row = mysql_fetch_assoc($query)) {
		$field[] = $row['Field'];
	}
	
	$i=0;
	foreach ($sheet->getRowIterator() as $row) { 
		if($i !=0){
			$cellIterator = $row->getCellIterator(); 
			$cellIterator->setIterateOnlyExistingCells(false);
			
			$a=0;
			foreach ($cellIterator as $cell) { 
				if(substr($cell->getValue(), 0,1)=="'"){
					$value = substr($cell->getValue(), 0,1);
				}else{
					$value =$cell->getValue();
				}
				
				if (strlen(trim($value)) == 0){
					$value='';
				}
				
				$array[$i][$a] =$value;
				
				$a++;
			}
		}$i++;
	}
			
			
	foreach( $array as $key=>$tmp){
		$id_arr[] = $tmp[0];
		
		$exhibitor_result = $obj->GetResult(" Company_ID='".$tmp[0]."'");
		$ttl_record = mysql_num_rows($exhibitor_result);
		$exhibitor_row = mysql_fetch_array($exhibitor_result);
		$Last_edit_date = $exhibitor_row['Last_edit_date'];
		$Last_edit_time = $exhibitor_row['Last_edit_time'];
		
		if(!$ttl_record){
			$status = 1;
		}else{
			if($tmp[29].$tmp[30] !=$exhibitor_row['Last_edit_date'].$exhibitor_row['Last_edit_time'] && $tmp[29]!=''){
				$status = 2;
				//echo "A:".$tmp[31].$tmp[32]."A2:".$exhibitor_row['Last_edit_date'].$exhibitor_row['Last_edit_time']."<br />";
			}else{
				$status = 3;
				//echo "B:".$tmp[31].$tmp[32]."<br />";
			}
		}
		
		
		/*if($status==1 || $status==2){
				$id = $tmp[0];
				if(!file_exists($exhibitor_path . $id . "/"))
				{
					mkdir($exhibitor_path .$id . "/", 0775, true);
		
				}
			for ($j = 28; $j <= 30; $j++) {
				if (strlen(trim($tmp[$j])) != 0){
					$remote_file = $remote_path.$tmp[$j];
					$local_file = $exhibitor_path . $id."/".$tmp[$j];
					$upload = ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY);
					//$upload = copy($remote_file,$local_file);
					if($upload){ 
						$thumb_file = file_name($tmp[$j]). "_thumb.".file_ext($tmp[$j]);
						$index_file = file_name($tmp[$j]). "_index.".file_ext($tmp[$j]);
						$large_file = file_name($tmp[$j]). "_large.".file_ext($tmp[$j]);
						
						$thumb_file = $exhibitor_path. $id."/". $thumb_file;
						$index_file = $exhibitor_path. $id."/". $index_file;
						$large_file = $exhibitor_path. $id."/". $large_file;
						
						list($width, $height) = getimagesize($local_file);
						if($width / $height >= 1){
							resizeImage($local_file, "600" , $large_file);
			
							if($width < '600'){//echo $width;
								copy($local_file,$large_file);
							}
						}else{
							resizeImagebyHeight($local_file, "600" , $large_file);
							if($height < '600'){//echo $height;
								copy($local_file,$large_file);
							}
						}
							
						autoBackgroundColor($local_file, $index_file,"230", "230");
						autoBackgroundColor($local_file, $thumb_file,"160", "160");
					}
				}
						
			}
		}*/
		
		foreach( $field as $key1=>$tmp_val){
			$values[$tmp_val] = mysql_real_escape_string($tmp[$key1]);
		}
		$values['create_date'] = date("Y-m-d H:i:s");
		
		if($status==1){
			$obj->InsertRecord($values);
			$download_photo = true;
		}
		if($status==2){
			$obj->UpdateRecord1($values);
			$download_photo = true;
		}

	}
	
	
	/*$exhibitor_sql = "select * from $exDB";
	$exhibitor_result = mysql_query($exhibitor_sql, $link);
	while($exhibitor_row = mysql_fetch_array($exhibitor_result)){
		$Company_ID = $exhibitor_row['Company_ID'];
		if (!in_array($Company_ID, $id_arr)) {
			$del ="DELETE FROM $exDB WHERE Company_ID='".$Company_ID."'";
			$result1 = mysql_query($del, $link);
		}
	}*/
	
	


	if($download_photo){
	
	$i =1;
	foreach ($sheet->getRowIterator() as $row) { 
		if($i !=1){
			$cellIterator = $row->getCellIterator(); 
			$cellIterator->setIterateOnlyExistingCells(false);
			$a= 0;
			$values ='';
			foreach ($cellIterator as $cell) { 
				if(substr($cell->getValue(), 0,1)=="'"){
					$value = substr($cell->getValue(), 0,1);
				}else{
					$value =$cell->getValue();
				}
				
				if($a ==0){
					$id = $value;
					if(!file_exists($exhibitor_path . $value . "/"))
					{
						mkdir($exhibitor_path .$value . "/", 0775, true);
			
					}
				}
				
				if($a >= 26 && $a<=28){
					if (strlen(trim($value)) != 0){
						$remote_file = $remote_path.$value;
						$local_file = $exhibitor_path . $id."/".$value;
						$upload = ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY);
						//$upload = copy($remote_file,$local_file);
						if($upload){ 
							$thumb_file = file_name($value). "_thumb.".file_ext($value);
							$index_file = file_name($value). "_index.".file_ext($value);
							$large_file = file_name($value). "_large.".file_ext($value);
							
							$thumb_file = $exhibitor_path. $id."/". $thumb_file;
							$index_file = $exhibitor_path. $id."/". $index_file;
							$large_file = $exhibitor_path. $id."/". $large_file;
							
							list($width, $height) = getimagesize($local_file);
							if($width / $height >= 1){
								resizeImage($local_file, "600" , $large_file);
				
								if($width < '600'){//echo $width;
									copy($local_file,$large_file);
								}
							}else{
								resizeImagebyHeight($local_file, "600" , $large_file);
								if($height < '600'){//echo $height;
									copy($local_file,$large_file);
								}
							}
								
							autoBackgroundColor($local_file, $index_file,"230", "230");
							autoBackgroundColor($local_file, $thumb_file,"160", "160");
						}
					}
							
				}
				
				$values[$field[$a]] = mysql_real_escape_string($value);
				$a++;
			}
			
			$values['create_date'] = date("Y-m-d H:i:s");
			//$obj->InsertRecord($values);
		}
	$i++;
	}
}
	
	
	header("location:excel.php");
	exit;
	

				 
				 
				 
				//FTP connect
				/*$conn_id = ftp_connect($ftp_server)or die("Could not connect to $ftp_server");
				$login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);
				ftp_pasv($conn_id, true);*/

				 //insert record
				$i = 1;
				
				foreach( $array as $tmp){
//echo utf8_decode($tmp['9']);
// echo iconv_strlen($tmp['9'], 'UTF-8')."<br />";


 

					$sql = "SHOW COLUMNS FROM exhibitor FROM jma";
					$query = mysql_query($sql, $link);
					while ($row = mysql_fetch_assoc($query)) {
						if($i==8){
							$str = (strlen($tmp[$i])!==strlen(utf8_decode(($tmp[$i]))))? $tmp[$i]: utf8_encode(($tmp[$i]));
							$values[$row['Field']] = mysql_real_escape_string($str);
						}else{
							$values[$row['Field']] = mysql_real_escape_string($tmp[$i]);
						}
						
						if($i==1||$i==11||$i==12||$i==18||$i==20||$i==22||$i==24||$i==25||$i==27||$i==34||$i==35||$i==37||$i==38||$i==39){
							$values[$row['Field']] = str_replace("'","",$tmp[$i]);
						}
						//echo $tmp['8'];
						//Create Folder
						if(!file_exists($exhibitor_path . str_replace("'","",$tmp[1]) . "/"))
							{
								mkdir($exhibitor_path . str_replace("'","",$tmp[1]) . "/", 0775, true);
					
							}
						$i++;
					}
					$new_id[]=str_replace("'","",$tmp[1]);
					
					$exhibitor_result = $obj->GetResult(" Company_ID='".str_replace("'","",$tmp[1])."'");
					$ttl_record = mysql_num_rows($exhibitor_result);
					$exhibitor_row = mysql_fetch_array($exhibitor_result);
					$Last_edit_date = $exhibitor_row['Last_edit_date'];
					$Last_edit_time = $exhibitor_row['Last_edit_time'];
					
					
					//$remote_path =$root_path.'jma/images/';
					if($values['Last_edit_date'].$values['Last_edit_time'] !=$exhibitor_row['Last_edit_date'].$exhibitor_row['Last_edit_time']){
						//echo $values['Last_edit_date'].$values['Last_edit_time'].":".$exhibitor_row['Last_edit_date'].$exhibitor_row['Last_edit_time']."<br />";
						
					//FTP GET
						for ($j = 29; $j <= 31; $j++) {
							if(str_replace(' ','',$tmp[$j]) !=""){
								$remote_file = $remote_path.$tmp[$j];
								$local_file = $exhibitor_path . str_replace("'","",$tmp[1])."/".$tmp[$j];
								//$upload = ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY);
								$upload = copy($remote_file,$local_file);
								//echo $remote_file." | ".$local_file."<br />";
								if($upload){ 
									$thumb_file = file_name($tmp[$j]). "_thumb.".file_ext($tmp[$j]);
									$index_file = file_name($tmp[$j]). "_index.".file_ext($tmp[$j]);
									$large_file = file_name($tmp[$j]). "_large.".file_ext($tmp[$j]);
									//echo $index_file;
									$thumb_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $thumb_file;
									$index_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $index_file;
									$large_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $large_file;
									
									list($width, $height) = getimagesize($local_file);
									if($width / $height >= 1){
										resizeImage($local_file, "600" , $large_file);
						
										if($width < '600'){//echo $width;
											copy($local_file,$large_file);
										}
									}else{
										resizeImagebyHeight($local_file, "600" , $large_file);
										if($height < '600'){//echo $height;
											copy($local_file,$large_file);
										}
									}
										
									//resizeImage($local_file, "230" , $index_file);
									//resizeImage($local_file, "160" , $thumb_file);
									autoBackgroundColor($local_file, $index_file,"230", "230");
									autoBackgroundColor($local_file, $thumb_file,"160", "160");
								}
							}
						}
						
						$values['create_date'] = date("Y-m-d H:i:s");
						$obj->UpdateRecord1($values);
					}
					
					if(!$ttl_record){ 
						for ($j = 29; $j <= 31; $j++) {
							if(str_replace(' ','',$tmp[$j]) !=""){
								$remote_file = $remote_path.$tmp[$j];
								$local_file = $exhibitor_path . str_replace("'","",$tmp[1])."/".$tmp[$j];
								//$upload = ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY);
								$upload = copy($remote_file,$local_file);
								
								if($upload){ 
									$thumb_file = file_name($tmp[$j]). "_thumb.".file_ext($tmp[$j]);
									$index_file = file_name($tmp[$j]). "_index.".file_ext($tmp[$j]);
									$large_file = file_name($tmp[$j]). "_large.".file_ext($tmp[$j]);
									//echo $index_file;
									$thumb_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $thumb_file;
									$index_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $index_file;
									$large_file = $exhibitor_path. str_replace("'","",$tmp[1])."/". $large_file;
									
									list($width, $height) = getimagesize($local_file);
									if($width / $height >= 1){
										resizeImage($local_file, "600" , $large_file);
						
										if($width < '600'){//echo $width;
											copy($local_file,$large_file);
										}
									}else{
										resizeImagebyHeight($local_file, "600" , $large_file);
										if($height < '600'){//echo $height;
											copy($local_file,$large_file);
										}
									}
										
									//resizeImage($local_file, "230" , $index_file);
									//resizeImage($local_file, "160" , $thumb_file);
									autoBackgroundColor($local_file, $index_file,"230", "230");
									autoBackgroundColor($local_file, $thumb_file,"160", "160");
								}
							}
						}
						$values['create_date'] = date("Y-m-d H:i:s");
						$obj->InsertRecord($values);
						
					}else{
						$values['create_date'] = date("Y-m-d H:i:s");
						$obj->UpdateRecord1($values);
					}
					$i = 1;
					
				}
				
				$exhibitor_sql = "select * from exhibitor";
				$exhibitor_result = mysql_query($exhibitor_sql, $link);
				while($exhibitor_row = mysql_fetch_array($exhibitor_result)){
					$Company_ID = $exhibitor_row['Company_ID'];
					if (!in_array($Company_ID, $new_id)) {
						$del ="DELETE FROM exhibitor WHERE Company_ID=".$Company_ID;
						$result1 = mysql_query($del, $link);
					}
				}
				


?>