<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';

	

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="rsvp" )

	{

		$id = $_REQUEST['rsvp_id'] ?? '';

		$type_name = "RSVP";

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){ 

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "rsvp")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {			

			$values = $_REQUEST;

			

			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();

			if($isNew){

				$values['rsvp_publish_status'] = "D";

				$values['rsvp_status'] = STATUS_ENABLE;

				$values['rsvp_created_by'] = $_SESSION['sess_id'];

				$values['rsvp_created_date'] = $timestamp;

				$values['rsvp_updated_by'] = $_SESSION['sess_id'];

				$values['rsvp_updated_date'] = $timestamp;
				
				$id = DM::insert('rsvp', $values);

				rename($rsvp_path . $_REQUEST['randomString'] . "/", $rsvp_path . $id . "/");

				if(!file_exists($rsvp_path . $id. "/")){
					mkdir($rsvp_path . $id . "/", 0775, true);
				}

				$ObjRec = DM::load('rsvp', $id);
				$values = array();
				$values['rsvp_id'] = $id;
				$values['rsvp_detail_lang1'] = str_replace($rsvp_path . $_REQUEST['randomString'] ?? '', $rsvp_path . $id, $ObjRec['rsvp_detail_lang1']);
				$values['rsvp_detail_lang2'] = str_replace($rsvp_path . $_REQUEST['randomString'] ?? '', $rsvp_path . $id, $ObjRec['rsvp_detail_lang2']);
				$values['rsvp_detail_lang3'] = str_replace($rsvp_path . $_REQUEST['randomString'] ?? '', $rsvp_path . $id, $ObjRec['rsvp_detail_lang3']);
				DM::update('rsvp', $values);
				unset($values);


			} else {

				$ObjRec = DM::load('rsvp', $id);

				

				if($ObjRec['rsvp_status'] == STATUS_DISABLE)

				{

					header("location: list.php?msg=F-1002");

					return;

				}

				
				$values['rsvp_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['rsvp_updated_by'] = $_SESSION['sess_id'];

				$values['rsvp_updated_date'] = $timestamp;



				DM::update('rsvp', $values);

			}

			
			$ObjRec = DM::load('rsvp', $id);
			
			$rsvp_img_arr = array('rsvp_banner');
			
			foreach($rsvp_img_arr as $key=>$val){
				
				if($_FILES[$val]['tmp_name'] != ""){
						
					if($ObjRec[$val] != "" && file_exists($rsvp_path . $id . "/" . $ObjRec[$val])){
						@unlink($rsvp_path  . $id . "/" . $ObjRec[$val]);
					}
						
					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($rsvp_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

					list($width, $height) = getimagesize($rsvp_path . $id . "/".$newname);
					
						$newWidth ="1920";
						//$newHeight="462";

					resizeImageByWidth($rsvp_path. $id."/". $newname, $newWidth , $rsvp_path. $id."/". $newname);
						
			
					$values = array();
					$values[$val] = $newname;
					$values['rsvp_id'] = $id;
						
					DM::update('rsvp', $values);
					unset($values);
						
					$ObjRec = DM::load('rsvp', $id);
				}
			}



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "rsvp '" . $ObjRec['rsvp_name_lang1'] . "' (ID:" . $ObjRec['rsvp_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "rsvp", $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){ 
				
				ApprovalService::withdrawApproval( "rsvp",$type, $id, $timestamp);
			
				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['rsvp_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "rsvp", $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."rsvp/rsvp.php?rsvp_id=".$id);
				
				$approval_id = ApprovalService::createApprovalRequest( "rsvp", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);				
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "rsvp", "PA", $approval_id);
				
			}
			
			header("location:rsvp.php?rsvp_id=" . $id . "&msg=S-1001");
			return;
		}

			
			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();

			

			$ObjRec = DM::load('rsvp', $id);

			

			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}

			

			if($ObjRec['rsvp_status'] == STATUS_DISABLE)

			{

				header("location: list.php?msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("rsvp", $id);

			$values['rsvp_id'] = $id;
			$values['rsvp_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['rsvp_status'] = STATUS_DISABLE;			
			$values['rsvp_updated_by'] = $_SESSION['sess_id'];
			$values['rsvp_updated_date'] = getCurrentTimestamp();
			

			DM::update('rsvp', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("rsvp", $type, $id, $timestamp);
			
				Logger::log($_SESSION['sess_id'], $timestamp, "rsvp '" . $ObjRec['rsvp_name_lang1'] . "' (ID:" . $ObjRec['rsvp_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "nrsvp", "DS");
				
				$files = array($SYSTEM_HOST."rsvp/rsvp.php?rsvp_id=".$id);
				
				$approval_id = ApprovalService::createApprovalRequest("rsvp", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);				
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "rsvp", "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "rsvp", "D");
			}


			header("location: list.php?msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {		

			$timestamp = getCurrentTimestamp();

			

			$ObjRec = DM::load('rsvp', $id);

			

			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?msg=F-1002");

				return;

			}

			

			if($ObjRec['rsvp_status'] == STATUS_ENABLE || $ObjRec['rsvp_publish_status'] == 'AL')

			{

				header("location: list.php?msg=F-1006");

				return;

			}

			

			$values['rsvp_id'] = $id;

			$values['rsvp_publish_status'] = 'D';

			$values['rsvp_status'] = STATUS_ENABLE;			

			$values['rsvp_updated_by'] = $_SESSION['sess_id'];

			$values['rsvp_updated_date'] = getCurrentTimestamp();

			

			DM::update('rsvp', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			

			$parameters = array(STATUS_ENABLE, 'rsvp', $id, 'RCA', 'RAA', 'APL');

			

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			

			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;

				

				DM::update('approval', $values);

				unset($values);

				

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));

				

				$recipient_list = array("E", "C");

				

				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}

				

				$message = "";

				

				switch($approval_request['approval_approval_status']){

					case "RCA": 

						$message = "Request for checking";

					break;

					case "RAA": 

						$message = "Request for approval";

					break;

					case "APL": 

						$message = "Pending to launch";

					break;

				}

				

				$message .= " of rsvp '" . $ObjRec['rsvp_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'rsvp', "WA");

			}

		

			Logger::log($_SESSION['sess_id'], $timestamp, "rsvp '" . $ObjRec['rsvp_name_lang1'] . "' (ID:" . $ObjRec['rsvp_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'rsvp', "UD");


			header("location: list.php?msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>