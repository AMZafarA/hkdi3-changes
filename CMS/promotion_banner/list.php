<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'promotion_banner')){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$sql = " promotion_banner_status = ? ";
	$parameters = array(STATUS_ENABLE);

	$promotion_banner_list = DM::findAll("promotion_banner", $sql, " promotion_banner_sequence desc", $parameters);
	
	$sql = " (promotion_banner_status = ? OR (promotion_banner_status = ? AND promotion_banner_publish_status in (?,?)) ) ";
	$parameters = array(STATUS_ENABLE, STATUS_DISABLE, 'RCA', 'RAA');
	$promotion_banner_list = DM::findAll("promotion_banner", $sql, " promotion_banner_sequence desc", $parameters);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
        <script type="text/x-tmpl" id="alert_template">
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert {%=o.alert_type%}" role="alert">
					<span class="{%=o.icon%}"></span> {%=o.message%}
				</div>
			</div>
		</script>
		<script>
			function updateNumbering(){
				$("#menu_table tbody tr").each(function(index, element){
					$(element).find("td:eq(0)").html("<?php echo $parent_idx ?>" + (index+1));
				});
			}
			
			function saveSequence(){
				$.post( "act.php", $( "#form1" ).serialize(),
					function( data ) {
						if($("#alert_row").length > 0) $("#alert_row").detach();
						var content = tmpl("alert_template", data);
						$("#content").prepend(content);
						setTimeout(function() {
							$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
						}, 5000);
					}, "json"
				);
			}
			
			
			function deleteRecord(itemId,delectAct){
				if(confirm("Are you sure you want to delete this record ?")){
					$("#processType").val('delete');
					$("#promotion_banner_id").val(itemId);
					if(delectAct=='N'){
						$('#loadingModalDialog').on('shown.bs.modal', function (e) {
							var frm = document.form3;
							frm.submit();
						})
						$('#loadingModalDialog').modal('show');
					}else{
						$('#approvalModalDialog').modal('show');
					}
				}
			}
			
			function undeleteRecord(itemId){
				if(confirm("Are you sure you want to undelete this record ?")){
					$("#processType").val('undelete');
					$("#promotion_banner_id").val(itemId);
					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form3;
						frm.submit();
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}
			
			function submitForApproval(){
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form3;
					frm.submit();
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			var fixHelper = function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			};
			
			$( document ).ready(function() {
				
				/*$( "#menu_table tbody" ).sortable({
					items: "tr",
					helper: fixHelper,
					update:  function( event, ui ) {
						saveSequence();
						updateNumbering();
					}
				});*/
				
				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});
				
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-5">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Main Banners</li>
				</ol>
				<span class="breadcrumb-right col-xs-7">
					&nbsp;
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
                <div class="row">
                    <h4 class="col-xs-6" style="padding-left: 0px;">Promotion Banner<div style="font-size: 11px;"><!--(You may Drag &amp; Drop to re-order the sequence of records)--></div></h4>
                    
                    <div class="col-xs-6" style="text-align:right">
                        <button type="button" class="btn btn-default" onclick="location.href='promotion_banner.php'; return false;">
                            <span class="glyphicon glyphicon-plus"></span> New
                        </button>
                    </div>
                </div>
				<div class="row">
					<table id="menu_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="col-xs-1">#</th>
								<th class="col-xs-7">Promotion Banner Name</th>
                                <th class="col-xs-2">Priority</th>
								<th class="col-xs-3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$row_count = 1;
								foreach($promotion_banner_list as $row)
								{
									$promotion_banner_id = $row['promotion_banner_id'];
									$promotion_banner_sequence = htmlspecialchars($row['promotion_banner_sequence']);
									$promotion_banner_banner_filename = htmlspecialchars($row['promotion_banner_banner_filename']);
									$promotion_banner_status = htmlspecialchars($row['promotion_banner_status']);
									
									$thumb = $promotion_banner_url. $promotion_banner_id."/". "thumb.".pathinfo($promotion_banner_banner_filename,PATHINFO_EXTENSION);
									
									$isPublished = SystemUtility::isPublishedRecord('promotion_banner', $promotion_banner_id);
							?>
									<tr>
										<td class="col-xs-1">
											<?php echo $row_count ?>
										</td>
										<td class="col-xs-7"><img src="<?php echo $thumb ?>"></td>
                                        <td class="col-xs-2"><?php echo $promotion_banner_sequence ?></td>
										<td class="col-xs-3">
                                        <?php if($promotion_banner_status == STATUS_ENABLE) { ?>
											<a role="button" class="btn btn-primary btn-sm" href="promotion_banner.php?promotion_banner_id=<?php echo $promotion_banner_id ?>">
												<i class="fa fa-folder-open"></i> Edit
											</a>
                                            <a role="button" class="btn btn-danger btn-sm" href="#" onclick="deleteRecord('<?php echo $promotion_banner_id ?>', '<?php echo $isPublished ? "P" : "N" ?>'); return false;">
												<i class="glyphicon glyphicon-trash"></i> Delete
											</a>
											<?php } else { ?>
												<a role="button" class="btn btn-danger btn-sm" href="#" onclick="undeleteRecord('<?php echo $promotion_banner_id ?>'); return false;">
													<i class="glyphicon glyphicon-repeat"></i> Undelete
												</a>
											<?php } ?>
                                            <input type="hidden" name="promotion_banner_id[]" value="<?php echo $promotion_banner_id ?>"/>
										</td>
									</tr>
							<?php
								$row_count++;
								}
								unset($resultObj);
							?>
						</tbody>
					</table>
				</div>
                <input type="hidden" name="type" value="promotion_banner"/>
				<input type="hidden" name="processType" value="updateSequence"/>
			</form>
            <form class="form-horizontal" role="form" id="form3" name="form3" action="act.php" method="post">
				<input type="hidden" id="type" name="type" value="promotion_banner"/>
				<input type="hidden" id="processType" name="processType" value=""/>
				<input type="hidden" id="approval_remarks" name="approval_remarks" value=""/>
				<input type="hidden" id="promotion_banner_id" name="promotion_banner_id" value=""/>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>