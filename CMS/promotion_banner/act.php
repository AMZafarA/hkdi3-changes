<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="promotion_banner" )
	{
		$id = $_REQUEST['promotion_banner_id'] ?? '';

		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'promotion_banner')){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}

		if($processType == "" || $processType == "submit_for_approval") {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('promotion_banner', $id);

			if($ObjRec['promotion_banner_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			$values = $_REQUEST;

			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			//unset($values['promotion_banner_caption_lang1']);

			if($isNew){
				$values['promotion_banner_publish_status'] = "D";
				$values['promotion_banner_status'] = STATUS_ENABLE;
				$values['promotion_banner_created_by'] = $_SESSION['sess_id'];
				$values['promotion_banner_created_date'] = $timestamp;
				$values['promotion_banner_updated_by'] = $_SESSION['sess_id'];
				$values['promotion_banner_updated_date'] = $timestamp;

				$id = DM::insert('promotion_banner', $values);
			} else {
				$ObjRec = DM::load('promotion_banner', $id);

				if($ObjRec['promotion_banner_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}

				$values['promotion_banner_publish_status'] = $processType == "submit_for_approval" ? "RAA" : 'D';
				$values['promotion_banner_updated_by'] = $_SESSION['sess_id'];
				$values['promotion_banner_updated_date'] = $timestamp;

				DM::update('promotion_banner', $values);
			}

			if(!file_exists($promotion_banner_path . $id. "/")){
				mkdir($promotion_banner_path . $id . "/", 0775, true);
			}

			$ObjRec = DM::load('promotion_banner', $id);

			if($_FILES['upload_banner']['tmp_name'] != ""){
				if($ObjRec['promotion_banner_banner_filename'] != "" && file_exists($promotion_banner_path . $id . "/" . $ObjRec['promotion_banner_banner_filename'])){
			//		@unlink($promotion_banner_path  . $id . "/" . $ObjRec['promotion_banner_banner_filename']);
				}

				$newname = moveFile($promotion_banner_path . $id . "/", $id, pathinfo($_FILES['upload_banner']['name'],PATHINFO_EXTENSION) , $_FILES['upload_banner']['tmp_name']);

				$thumb = $promotion_banner_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
				resizeImageByHeight($promotion_banner_path. $id."/". $newname, "100" , $thumb);

				$values = array();
				$values['promotion_banner_banner_filename'] = $newname;
				$values['promotion_banner_id'] = $id;

				DM::update('promotion_banner', $values);
				unset($values);

				$ObjRec = DM::load('promotion_banner', $id);
			}

			if($_FILES['upload_excel']['tmp_name'] != ""){
				if($ObjRec['promotion_banner_excel_filename'] != "" && file_exists($promotion_banner_path . $id . "/" . $ObjRec['promotion_banner_excel_filename'])){
			//		@unlink($promotion_banner_path  . $id . "/" . $ObjRec['promotion_banner_excel_filename']);
				}

				$newname = moveFile($promotion_banner_path . $id . "/", $id, pathinfo($_FILES['upload_banner']['name'],PATHINFO_EXTENSION) , $_FILES['upload_banner']['tmp_name']);

				$values = array();
				$values['promotion_banner_excel_filename'] = $newname;
				$values['promotion_banner_id'] = $id;

				DM::update('promotion_banner', $values);
				unset($values);

				$ObjRec = DM::load('promotion_banner', $id);
			}

			NotificationService::sendNotification($type, $id, $timestamp, $_SESSION['sess_id'], array("E"), $type, $isNew ? "CS" : "ES");

			if($processType == "submit_for_approval"){

			// withdraw previous pending for approval

			// array("E") E: Editor , A: Approver, 1st promotion_banner is table name, 2nd promotion_banner permission key

				ApprovalService::withdrawApproval($type, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Promotion Banner (ID:" . $ObjRec['promotion_banner_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification($type, $id, $timestamp, $_SESSION['sess_id'], array("E"), $type, $isNew ? "CS" : "ES");

				$files = array($host_name."web/en/index.php", $host_name."web/tc/index.php", $host_name."web/sc/index.php");

				$approval_id = ApprovalService::createApprovalRequest($type, $type, $id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type, $id, $timestamp, $_SESSION['sess_id'], array("A"), $type, "PA", $approval_id);
			}

			header("location:promotion_banner.php?promotion_banner_id=" . $id . "&msg=S-1001");
			return;
		} else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('promotion_banner', $id);

			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			if($ObjRec['mail_banner_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}

			$isPublished = SystemUtility::isPublishedRecord('promotion_banner', $id);

			$values['promotion_banner_id'] = $id;
			$values['promotion_banner_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['promotion_banner_status'] = STATUS_DISABLE;
			$values['promotion_banner_updated_by'] = $_SESSION['sess_id'];
			$values['promotion_banner_updated_date'] = getCurrentTimestamp();

			DM::update('promotion_banner', $values);
			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("promotion_banner", "promotion_banner", $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Promotion Banner (ID:" . $ObjRec['promotion_banner_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("promotion_banner", $id, $timestamp, $_SESSION['sess_id'], array("E"), "promotion_banner", "DS");

				$files = array($host_name."web/en/index.php", $host_name."web/tc/index.php", $host_name."web/sc/index.php");


				$approval_id = ApprovalService::createApprovalRequest("promotion_banner", "promotion_banner", $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("promotion_banner", $id, $timestamp, $_SESSION['sess_id'], array("A"), "promotion_banner", "PA", $approval_id);
			}

			header("location: list.php?msg=S-1002");
			return;
		} else if($processType == "undelete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('promotion_banner', $id);

			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			if($ObjRec['promotion_banner_status'] == STATUS_ENABLE || ($ObjRec['promotion_banner_status'] == STATUS_DISABLE && $ObjRec['promotion_banner_publish_status'] == 'AL'))
			{
				header("location: list.php?msg=F-1006");
				return;
			}

			$values['promotion_banner_id'] = $id;
			$values['promotion_banner_publish_status'] = 'D';
			$values['promotion_banner_status'] = STATUS_ENABLE;
			$values['promotion_banner_updated_by'] = $_SESSION['sess_id'];
			$values['promotion_banner_updated_date'] = getCurrentTimestamp();

			DM::update('promotion_banner', $values);
			unset($values);

			ApprovalService::withdrawApproval("promotion_banner", "promotion_banner", $id, $timestamp);

			Logger::log($_SESSION['sess_id'], $timestamp, "main Banner '" . $ObjRec['promotion_banner_caption_lang1'] . "' (ID:" . $ObjRec['promotion_banner_id'] .") is undeleted by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification("promotion_banner", $id, $timestamp, $_SESSION['sess_id'], array("E", "A"), "promotion_banner", "UD");

			header("location: list.php?msg=S-1003");
			return;
		} else if($processType == "updateSequence"){
			$promotion_banner_list = $_REQUEST['promotion_banner_id'] ?? '';

			if($promotion_banner_list != "" && is_array($promotion_banner_list)){
				$counter = 1;
				$timestamp = getCurrentTimestamp();

				foreach($promotion_banner_list as $promotion_banner_id){
					$ObjRec = DM::load('promotion_banner', $promotion_banner_id);

					if($ObjRec == NULL || $ObjRec == "" || $ObjRec['promotion_banner_status'] != STATUS_ENABLE){
						continue;
					}

					$values = array();
					$values['promotion_banner_id'] = $promotion_banner_id;
					$values['promotion_banner_sequence'] = $counter;
					$values['promotion_banner_updated_by'] = $_SESSION['sess_id'];
					$values['promotion_banner_updated_date'] = $timestamp;

					DM::update('promotion_banner', $values);
					unset($values);
					$counter++;
				}
			}

			echo json_encode(array("result"=>"ok", "alert_type"=>"alert-success", "icon"=>"glyphicon glyphicon-ok", "message"=>$SYSTEM_MESSAGE['S-1001'] ));
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>
