<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'promotion_banner')){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$promotion_banner_id = $_REQUEST['promotion_banner_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($promotion_banner_id != ""){
		if(!isInteger($promotion_banner_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		$obj_row = DM::load('promotion_banner', $promotion_banner_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
		
/*		if(!file_exists($SYSTEM_BASE . "uploaded_files/promotion_banner/" . $promotion_banner_id)){
			mkdir($SYSTEM_BASE . "uploaded_files/promotion_banner/" . $promotion_banner_id, 0775, true);
		}*/
		
	} else {
		/*header("location: list.php?msg=F-1002");
		return;*/
		$promotion_banner_publish_status = 'D';
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script>
			function checkForm(){		
				var flag=true;
				var errMessage="";
				
				trimForm("form1");

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					submitForm();
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			function checkForm2(){
				var flag=true;
				var errMessage="";
				
				trimForm("form1");

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}
				
				$('#approvalModalDialog').modal('show');
			}
			
			function submitForm(){
				document.form1.submit();
			}
			
			function removeIcon(button){
				$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						submitForm();
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				$("#tabs").tabs({});

				$("#upload_icon").filestyle({buttonText: ""});				
				$("#upload_banner").filestyle({buttonText: ""});
				$("#upload_excel").filestyle({buttonText: ""});
				$("#btn_preview").on('click',function(){$("#mask").show("fast")})
				
				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Promotion Banners</a></li>
					<li class="active">Update</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
			<div class="row">
				<table class="table" style="margin-bottom: 5px;">
					<tbody>
						<tr class="<?php echo ($promotion_banner_publish_status == "RCA" || $promotion_banner_publish_status == "RAA") ? "danger" : "" ?>">
							<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
							<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
								<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $promotion_banner_publish_status] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
						<li><a href="#tabs-1">Banner Info</a></li>
					</ul>
					<div id="tabs-1">
						<!--<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">-->
                        <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
							<!--<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Banner Name</label>
								<div class="col-xs-10">
									<input type="text" class="form-control" placeholder="Banner Name" name="promotion_banner_name" id="promotion_banner_name" value="<?php echo $promotion_banner_name ?>" readonly/>
									
								</div>
							</div>-->
                            
							<!--<div class="form-group">
								<label class="col-xs-2 control-label">Icon</label>
								<div class="col-xs-5">
									<input type="file" id="upload_icon" name="upload_icon" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										(Width: <?php echo $promotion_banner_ICON_WIDTH ?>px, Height: <?php echo $promotion_banner_ICON_HEIGHT ?>px)
									</p>
								</div>
								<?php if($promotion_banner_icon_filename != "" && file_exists($SYSTEM_BASE . "uploaded_files/promotion_banner/" . $promotion_banner_id . "/" . $promotion_banner_icon_filename)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $SYSTEM_HOST . "download/promotion_banner/" . $promotion_banner_id . "/icon/" . $promotion_banner_icon_filename ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
										<a role="button" class="btn btn-default" href="#" onclick="removeIcon(this); return false;"><i class="glyphicon glyphicon-remove"></i> Remove</a>
									</div>
								<?php } ?>
							</div>-->
                            
                            
							<div class="form-group">
								<label class="col-xs-2 control-label">Banner</label>
								<div class="col-xs-5">
									<input type="file" id="upload_banner" name="upload_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										<!--(Width: <?php echo $promotion_banner_WIDTH ?>px, Height: <?php echo $promotion_banner_HEIGHT ?>px)-->
									</p>
								</div>
								<?php if($promotion_banner_banner_filename != "" && file_exists($promotion_banner_path . $promotion_banner_id . "/" . $promotion_banner_banner_filename)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $promotion_banner_url . $promotion_banner_id  . "/" .$promotion_banner_banner_filename ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
                                        <a role="button" class="btn btn-default" id="btn_preview" href="#"><i class="glyphicon glyphicon-search"></i> Preview with Mask</a>
									</div>
								<?php } ?>

							</div>
							
							<div class="form-group">
								<label class="col-xs-2 control-label">Excel</label>
								<div class="col-xs-5">
									<input type="file" id="upload_excel" name="upload_excel" accept=".xls,.xlsx"/>
									<p style="padding-top:5px;padding-left:5px;">
										<!--(Width: <?php echo $promotion_banner_WIDTH ?>px, Height: <?php echo $promotion_banner_HEIGHT ?>px)-->
									</p>
								</div>
								<?php if($promotion_banner_excel_filename != "" && file_exists($promotion_banner_path . $promotion_banner_id . "/" . $promotion_banner_excel_filename)) { ?>
									<div class='col-xs-5'>
										exist
									</div>
								<?php } ?>
							</div>
							
							<div class="form-group">
                                <label class="col-xs-2 control-label" style="padding-right:0px">Display Priority</label>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" placeholder="Priority" name="promotion_banner_sequence" id="promotion_banner_sequence" value="<?php echo $promotion_banner_sequence ?>"/>
                                </div>
                            </div>
                            
                            
                            <input type="hidden" name="type" id="type" value="promotion_banner" />
                            <input type="hidden" name="processType"  id="processType" value="">
                            <input type="hidden" name="promotion_banner_id" id="promotion_banner_id" value="<?php echo $promotion_banner_id ?>" />
                            <input type="hidden" name="promotion_banner_status" id="promotion_banner_status" value="1" />
                            <input type="hidden" name="remove_icon" id="remove_icon" value="N" />
                            <input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
                            <input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
						</form>
					</div>
				</div>
			</div>
			<div class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>