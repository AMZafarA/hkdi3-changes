<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$sql = " menu_status = ? AND menu_type = ? ";
	$parameters = array(STATUS_ENABLE, 'N');

	$menu_list = DM::findAll("menu", $sql, " menu_sequence ", $parameters);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script type="text/x-tmpl" id="alert_template">
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert {%=o.alert_type%}" role="alert">
					<span class="{%=o.icon%}"></span> {%=o.message%}
				</div>
			</div>
		</script>
		<script>
			function generateDevContent(){
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					location.href = "act.php?type=menu&processType=generateDevelopment";
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			function generateProdContent(){
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					location.href = "act.php?type=menu&processType=generateProduction";
				})
				
				$('#loadingModalDialog').modal('show');
			}

			$( document ).ready(function() {
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-5">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Other Components</li>
				</ol>
				<span class="breadcrumb-right col-xs-7">
					<button type="button" class="btn btn-default" onclick="generateDevContent(); return false;">
						<span class="glyphicon glyphicon-transfer"></span> Generate Other Components (Dev.)
					</button>
					<button type="button" class="btn btn-default" onclick="generateProdContent(); return false;">
						<span class="glyphicon glyphicon-transfer"></span> Generate Other Components (Prod)
					</button>
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
				<div class="row">
					<table id="menu_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="col-xs-1">#</th>
								<th class="col-xs-10">Menu Name</th>
								<th class="col-xs-1">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$row_count = 1;
								foreach($menu_list as $row)
								{
									$menu_id = $row['menu_id'];
									$menu_eng_name = htmlspecialchars($row['menu_eng_name']);
							?>
									<tr>
										<td class="col-xs-1">
											<?php echo $parent_idx ?><?php echo $row_count ?>
										</td>
										<td class="col-xs-10"><?php echo $menu_eng_name ?></td>
										<td class="col-xs-1">
											<a role="button" class="btn btn-primary btn-sm" href="menu.php?menu_id=<?php echo $menu_id ?>">
												<i class="fa fa-folder-open"></i> Edit
											</a>
										</td>
									</tr>
							<?php
								$row_count++;
								}
								unset($resultObj);
							?>
						</tbody>
					</table>
				</div>
				<input type="hidden" name="type" value="menu"/>
				<input type="hidden" name="processType" value="updateSequence"/>
				<input type="hidden" name="menu_parent_id" value="<?php echo $menu_parent_id ?>"/>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>