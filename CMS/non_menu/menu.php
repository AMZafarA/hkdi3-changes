<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$menu_id = $_REQUEST['menu_id'] ?? '';
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($menu_id != ""){
		if(!isInteger($menu_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		$obj_row = DM::load('menu', $menu_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
	} else {
		foreach($_REQUEST as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
	}
	
	$system_page_list = DM::findAll('system_page', ' system_page_status = ? ', ' system_page_id ', array(STATUS_ENABLE));
	
	$eng_menu_res_list = DM::findAll("menu_res", " menu_res_status = ? AND menu_res_mid = ? AND menu_res_type = ? " , " menu_res_sequence ", array(STATUS_ENABLE, $menu_id, "E"));
	
	$chi_menu_res_list = DM::findAll("menu_res", " menu_res_status = ? AND menu_res_mid = ? AND menu_res_type = ? " , " menu_res_sequence ", array(STATUS_ENABLE, $menu_id, "C"));

	$editor_user_group_list = DM::findAll(array("menu_user_group", "user_group"), " user_group_id = menu_user_group_ugid AND user_group_status = ? AND menu_user_group_mid = ? AND menu_user_group_type = ? " , " menu_user_group_sequence ", array(STATUS_ENABLE, $menu_id, "E"));
	
	$checker_user_group_list = DM::findAll(array("menu_user_group", "user_group"), " user_group_id = menu_user_group_ugid AND user_group_status = ? AND menu_user_group_mid = ? AND menu_user_group_type = ? " , " menu_user_group_sequence ", array(STATUS_ENABLE, $menu_id, "C"));
	
	$approver_user_group_list = DM::findAll(array("menu_user_group", "user_group"), " user_group_id = menu_user_group_ugid AND user_group_status = ? AND menu_user_group_mid = ? AND menu_user_group_type = ? " , " menu_user_group_sequence ", array(STATUS_ENABLE, $menu_id, "A"));	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script type="text/x-tmpl" id="user_group_panel">
			<span class="user_group_cell">
				<div>
					<label>
						{%=o.user_group_name%}
						<input type="hidden" name="{%=o.parameter_name%}" value="{%=o.user_group_id%}"/>
					</label>
					<span>
						<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
							<i class="glyphicon glyphicon-remove"></i>
						</button>
					</span>
				</div>
			</span>
		</script>
		<script type="text/x-tmpl" id="menu_res_panel">
			<div class="form-group res_row" style="margin-bottom:5px;">
				<div class="col-xs-5 control-label vcenter">
					{%=o.filename%}
				</div>
				<div class="col-xs-2 vcenter">
					<div class="checkbox" style="text-align:center">
						<label>
							<input type="checkbox" name="menu_res_show_in_dev" value="Y"/>
						</label>
					</div>
				</div>
				<div class="col-xs-2 vcenter">
					<div class="checkbox" style="text-align:center">
						<label>
							<input type="checkbox"name="menu_res_show_in_prod" value="Y"/>
						</label>
					</div>
				</div>
				<div class="vcenter" style="text-align:left">
					<a role="button" class="btn btn-default" href="#" target="_blank" disabled="disabled">
						<i class="glyphicon glyphicon-download-alt"></i> Download
					</a>
					<button type="button" class="btn btn-default" onclick="removeResItem(this); return false;">
						<i class="glyphicon glyphicon-remove"></i> Remove
					</button>
					<input type="hidden" name="tmp_id" value="{%=o.tmp_id%}"/>
					<input type="hidden" name="menu_res_id" value=""/>
				</div>
			</div>
		</script>
		<script>
			var file_list = {};
			var counter = 0;

			function checkForm(){				
				var flag=true;
				var errMessage="";
				
				trimForm("form1");
				trimForm("form2");
				trimForm("form3");
				
				if($("#menu_eng_name").val() == ""){
					flag = false;
					errMessage += "Menu Name (English)\n";
				}
				
				if($("#menu_chi_name").val() == ""){
					flag = false;
					errMessage += "Menu Name (中文)\n";
				}
				
				if($("#menu_link_type").val() == "") {
					flag = false;
					errMessage += "Link Type\n";
				}

				if($("#menu_link_type").val() == "S"){
					if($("#menu_link_system_page_id").val() == "") {
						flag = false;
						errMessage += "Record Type\n";
					}
				}
				
				if($("#menu_link_type").val() == "U" || $("#menu_link_type").val() == "S" || $("#menu_link_type").val() == "D"){
					if($("#menu_output_filename").val() == "") {
						flag = false;
						errMessage += "Filename\n";
					}
				}
				
				if($("#menu_link_type").val() == "E"){
					if($("#menu_link_eng_url").val() == "") {
						flag = false;
						errMessage += "URL (English)\n";
					}
					
					if($("#menu_link_chi_url").val() == "") {
						flag = false;
						errMessage += "URL (中文)\n";
					}
				}

				if(flag==false){
					errMessage = "Please fill in the following information:\n" + errMessage;
					alert(errMessage);
					return;
				}

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					if($("#eng_res_form input[name='menu_res_id']").length > 0)
						cloneEngResForm(0);
					else if($("#chi_res_form input[name='menu_res_id']").length > 0)
						cloneChiResForm(0);
					else
						submitForm();
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			function submitForm(){
				cloneForm("form2", "form1");
				cloneForm("form3", "form1");
				document.form1.submit();
			}
			
			function resetForm(){
				<?php if($menu_id == "") { ?>
					location.href = "menu.php<?php echo $menu_parent_id != "" ? "?menu_parent_id=" . $menu_parent_id : "" ?>";
				<?php } else { ?>
					location.href = "menu.php?menu_id=<?php echo $menu_id?>";
				<?php } ?>
			}
			
			<?php foreach(array("eng", "chi") as $lang) { ?>
				function clone<?php echo ucfirst($lang) ?>ResForm(idx){
					if($("#<?php echo $lang ?>_res_form input[name='menu_res_id']").length <= idx){
						<?php if($lang == "eng") { ?>
							cloneChiResForm(0);
						<?php } else { ?>
							submitForm();
						<?php } ?>
						return;
					}

					if($("#<?php echo $lang ?>_res_form input[name='menu_res_id']:eq(" + idx + ")").val() != ""){
						clone<?php echo ucfirst($lang) ?>ResForm(++idx);
						return;
					}

					var tmp_id = $("#<?php echo $lang ?>_res_form input[name='tmp_id']:eq(" + idx + ")").val();
					var menu_res_id = $("#<?php echo $lang ?>_res_form input[name='menu_res_id']:eq(" + idx + ")").val();
					var menu_res_in_dev = $("#<?php echo $lang ?>_res_form input[name='menu_res_show_in_dev']:eq(" + idx + ")").prop("checked") ? "Y" : "N";
					var menu_res_in_prod = $("#<?php echo $lang ?>_res_form input[name='menu_res_show_in_prod']:eq(" + idx + ")").prop("checked") ? "Y" : "N";

					var fd = new FormData();
					fd.append('upload_file', file_list[tmp_id]);
					
					$.ajax({
						url: "../pre_upload/act.php",
						data: fd,
						processData: false,
						contentType: false,
						type: 'POST',
						dataType : "json",
						success: function(data) {
							var obj = {menu_res_id:menu_res_id,filename:data.filename,tmp_filename:data.tmpname,show_in_dev:menu_res_in_dev,show_in_prod:menu_res_in_prod};
							$('<input>').attr('name','menu_res_<?php echo $lang ?>_item[]').attr('type','hidden').val(JSON.stringify(obj)).appendTo('#form1');
							clone<?php echo ucfirst($lang) ?>ResForm(++idx);
						}
					});
				}
			<?php } ?>

			function menu_link_type_changed(callback){
				$("#row_filename").hide();
				$("#row_system").hide();
				$("#row_sub_system").hide();
				$("#row_external").hide();

				if($("#menu_link_type").val() == "U"){
					$("#row_filename").show();
					$("#menu_link_system_page_id").val("");
					$("#menu_link_eng_url").val("");
					$("#menu_link_chi_url").val("");
				} else if($("#menu_link_type").val() == "S"){
					$("#row_filename").show();
					$("#row_system").show();
					$("#menu_link_eng_url").val("");
					$("#menu_link_chi_url").val("");
				} else if($("#menu_link_type").val() == "E"){
					$("#row_external").show();
					$("#menu_output_filename").val("");
					$("#menu_link_system_page_id").val("");
				} else if($("#menu_link_type").val() == "D"){
					$("#row_filename").show();
					$("#menu_link_system_page_id").val("");
					$("#menu_link_eng_url").val("");
					$("#menu_link_chi_url").val("");
				}
				
				if(callback) callback();
			}
			
			function menu_link_item_changed(){
				if($("#menu_link_system_page_id").val() == "11"){
					$("#row_sub_system").show();
				} else {
					$("#row_sub_system").hide();
					$("#menu_link_system_item_id").val("");
				}
			}
			
			function removeItem(item){
				$(item).closest(".user_group_cell").detach();
			}
			
			function removeResItem(item){
				$form_group = $(item).closest(".form-group");
				
				var tmp_id = $form_group.find("input[name='tmp_id']").val();

				if(tmp_id != ""){
					delete file_list[tmp_id];
				}

				$(item).closest(".form-group").detach();
			}
			
			$( document ).ready(function() {
				$("#tabs").tabs({});
				menu_link_type_changed(function(){
					<?php if($menu_link_type == "S")  { ?>
						$("#menu_link_system_page_id").val(<?php echo $menu_link_system_page_id ?>);
					<?php } ?>
				});
				
				$("#upload_dev_banner").filestyle({buttonText: ""});
				$("#upload_prod_banner").filestyle({buttonText: ""});
				
				<?php foreach(array("chi", "eng") as $lang) { ?>
					$("#upload_<?php echo $lang ?>_resource").on('change', function(){
						if(!$("#upload_<?php echo $lang ?>_resource")[0].files.length || $("#upload_<?php echo $lang ?>_resource")[0].files.length == 0)
								return;
						
						var files = $("#upload_<?php echo $lang ?>_resource")[0].files;
						
						for(var i = 0; i < files.length; i++){
							var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++)};
							file_list[obj.tmp_id] = files[i];
							var content = tmpl("menu_res_panel", obj);
							$("#<?php echo $lang ?>_res_form").append(content);
						}
					});
					
					$( "#<?php echo $lang ?>_res_form" ).sortable({
						items: "div.res_row"
					}).disableSelection();
				<?php } ?>
				
				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>

				$("#menu_eng_name").change(function(){
					if($("#menu_id").val()=="")
					{
						//define file name
						 var ext = ($("#menu_parent_id").val()=="")?"/index.html":".html";
						 var key=($.trim($("#default_folder_path").val() + $("#menu_eng_name").val())).toLowerCase() + ext;
					     key = key.replace(/ /g,"_");//convert %20 into _
					    
					     key=key.replace(/'/g,"");//convert ' into 
					     key=key.replace(/"/g,"");//convert ' into 
						
					     $("#menu_output_filename").val(key);
					}//
				});//change

				<?php foreach(array("editor", "checker", "approver") as $actor) { ?>
					$( "#<?php echo $actor ?>_group_search" ).autocomplete({
						source: function(request, callback){
							var values = [];
							$("input[name='menu_user_group_<?php echo $actor ?>[]']").each(function() {
								values.push($(this).val());
							}).promise().done(function(){
								$.get( "user_group_search.php", { term:request.term, "user_group_id": values.join(",")}, 
									function( data ) {callback(data);}, 
								"json");
							});
						}, 
						minLength: 2,
						select: function( event, ui ) {
							if(ui.item){
								var content = tmpl("user_group_panel", {user_group_id:ui.item.id,user_group_name:ui.item.value,parameter_name:"menu_user_group_<?php echo $actor ?>[]"});
								$("#<?php echo $actor ?>_group_value").append(content);
								event.preventDefault();
							}
							$("#<?php echo $actor ?>_group_search").val("");
						}
					});
				<?php } ?>
			});
		</script>
		<style>
			.user_group_cell {
				margin-bottom:7px; padding: 7px 15px 4px;display:inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
			}
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Other Components</a></li>
					<li class="active"><?php echo $menu_id == "" ? "Create" : "Update" ?></li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
			<div class="row">
				<div id="tabs" class="col-xs-12">
					<ul>
						<li><a href="#tabs-1">Menu Definition</a></li>
						<li><a href="#tabs-2">English Resources</a></li>
						<li><a href="#tabs-3">Chinese Resources</a></li>
					</ul>
					<div id="tabs-1">
						<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-xs-3 control-label" style="padding-right:0px">Menu Name (English) * :</label>
								<div class="col-xs-9">
									<input type="text" class="form-control" placeholder="Menu Name (English)" name="menu_eng_name" id="menu_eng_name" value="<?php echo $menu_eng_name ?>" readonly="readonly"/>
									<input type="hidden" name="type" id="type" value="menu" />
									<input type="hidden" name="processType"  id="processType" value="">
									<input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id ?>" />
									<input type="hidden" name="menu_status" id="menu_status" value="1" />
									<input type="hidden" name="menu_parent_id" id="menu_parent_id" value="<?php echo $menu_parent_id ?>" />
									<input type="hidden" name="menu_type" id="menu_type" value="N" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" style="padding-right:0px">Menu Name (中文) * :</label>
								<div class="col-xs-9">
									<input type="text" class="form-control" placeholder="Menu Name (中文)" name="menu_chi_name" id="menu_chi_name" value="<?php echo $menu_chi_name ?>"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-3 checkbox">
									<label>
										<input type="checkbox" id="menu_show_in_dev" name="menu_show_in_dev" value="Y" <?php echo $menu_show_in_dev == "Y" ? "checked" : "" ?>/>
										<strong>Show in Development</strong>
									</label>
								</div>
								<div class="col-xs-4 checkbox">
									<label>
										<input type="checkbox" id="menu_show_in_prod" name="menu_show_in_prod" value="Y" <?php echo $menu_show_in_prod == "Y" ? "checked" : "" ?>/>
										<strong>Show in Production</strong>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-2 control-label">Banner (Dev) :</label>
								<div class="col-xs-5">
									<input type="file" id="upload_dev_banner" name="upload_dev_banner" accept="image/*"/>
								</div>
								<?php if($menu_dev_banner != "" && file_exists($SYSTEM_BASE . "uploaded_files/menu/" . $menu_id . "/" . $menu_dev_banner)) { ?>
									<div class='col-xs-2'>
										<a role="button" class="btn btn-default" href="<?php echo $SYSTEM_HOST . "download/menu/" . $menu_id . "/dev/" . $menu_dev_banner ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
								<div class='col-xs-3 control-label' style="padding-left:0px">
									(Width: <?php echo $MENU_BANNER_WIDTH ?>px, Height: <?php echo $MENU_BANNER_HEIGHT ?>px)
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-2 control-label">Banner (Prod) :</label>
								<div class="col-xs-5">
									<input type="file" id="upload_prod_banner" name="upload_prod_banner" accept="image/*"/>
								</div>
								<?php if($menu_prod_banner != "" && file_exists($SYSTEM_BASE . "uploaded_files/menu/" . $menu_id . "/" . $menu_prod_banner)) { ?>
									<div class='col-xs-2'>
										<a role="button" class="btn btn-default" href="<?php echo $SYSTEM_HOST . "download/menu/" . $menu_id . "/prod/" . $menu_prod_banner ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
								<div class='col-xs-3 control-label' style="padding-left:0px">
									(Width: <?php echo $MENU_BANNER_WIDTH ?>px, Height: <?php echo $MENU_BANNER_HEIGHT ?>px)
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-2 control-label">Link Type :</label>
								<div class="col-xs-3">
									<select id="menu_link_type" name="menu_link_type" class="form-control" onchange="menu_link_type_changed(); return false;">
										<option value="">Select</option>
										<option value="E" <?php echo $menu_link_type=="E"?"selected":""?>>External Link</option>
										<option value="U" <?php echo $menu_link_type=="U"?"selected":""?>>Page</option>
										<option value="S" <?php echo $menu_link_type=="S"?"selected":""?>>Record-based Page</option>
										<option value="D" <?php echo $menu_link_type=="D"?"selected":""?>>Predefined Page</option>
										<option value="B" <?php echo $menu_link_type=="B"?"selected":""?>>Empty Link</option>
									</select>
								</div>
								<div class="col-xs-4 checkbox">
									<label>
										<input type="checkbox" id="menu_link_open_new_browser" name="menu_link_open_new_browser" value="Y" <?php echo $menu_link_open_new_browser == "Y" ? "checked" : "" ?>/>
										<strong>Open in New Browser</strong>
									</label>
								</div>
							</div>
							<div id="row_system">
								<div class="form-group">
									<label class="col-xs-2 control-label">Record Type :</label>
									<div class="col-xs-10">
										<select id="menu_link_system_page_id" name="menu_link_system_page_id" class="form-control" onchange="menu_link_item_changed(); return false;">
											<option value="">Select</option>
											<?php foreach($system_page_list as $system_page){ ?>
												<option value="<?php echo $system_page['system_page_id'] ?>"><?php echo htmlspecialchars($system_page['system_page_name']) ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div id="row_external">
								<div class="form-group">
									<label class="col-xs-2 control-label">URL (English) :</label>
									<div class="col-xs-10">
										<input type="text" class="form-control" placeholder="URL (English)" name="menu_link_eng_url" id="menu_link_eng_url" value="<?php echo $menu_link_eng_url ?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">URL (中文) :</label>
									<div class="col-xs-10">
										<input type="text" class="form-control" placeholder="URL (中文)" name="menu_link_chi_url" id="menu_link_chi_url" value="<?php echo $menu_link_chi_url ?>"/>
									</div>
								</div>
							</div>
							<div id="row_filename">
								<div class="form-group">
									<label class="col-xs-2 control-label">Filename :</label>
									<div class="col-xs-10">
										<input type="text" class="form-control" placeholder="Filename" name="menu_output_filename" id="menu_output_filename" value="<?php echo $menu_output_filename ?>"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">Editor Group :</label>
								<div class="col-xs-9">
									<input type="text" class="form-control" placeholder="Editor Group" name="editor_group_search" id="editor_group_search"/>
								</div>
							</div>
							<div id="editor_group_value" style="padding-bottom: 15px;">
								<?php foreach($editor_user_group_list as $editor_user_group){ ?>
									<span class="user_group_cell">
										<div>
											<label>
												<?php echo htmlspecialchars($editor_user_group['user_group_name']) ?>
												<input type="hidden" name="menu_user_group_editor[]" value="<?php echo $editor_user_group['menu_user_group_ugid'] ?>"/>
											</label>
											<span>
												<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
													<i class="glyphicon glyphicon-remove"></i>
												</button>
											</span>
										</div>
									</span>
								<?php } ?>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">Checker Group :</label>
								<div class="col-xs-9">
									<input type="text" class="form-control" placeholder="Checker Group" name="checker_group_search" id="checker_group_search"/>
								</div>
							</div>
							<div id="checker_group_value" style="padding-bottom: 15px;">
								<?php foreach($checker_user_group_list as $checker_user_group){ ?>
									<span class="user_group_cell">
										<div>
											<label>
												<?php echo htmlspecialchars($checker_user_group['user_group_name']) ?>
												<input type="hidden" name="menu_user_group_checker[]" value="<?php echo $checker_user_group['menu_user_group_ugid'] ?>"/>
											</label>
											<span>
												<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
													<i class="glyphicon glyphicon-remove"></i>
												</button>
											</span>
										</div>
									</span>
								<?php } ?>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">Approver Group :</label>
								<div class="col-xs-9">
									<input type="text" class="form-control" placeholder="Approver Group" name="approver_group_search" id="approver_group_search"/>
								</div>
							</div>
							<div id="approver_group_value" style="padding-bottom: 15px;">
								<?php foreach($approver_user_group_list as $approver_user_group){ ?>
									<span class="user_group_cell">
										<div>
											<label>
												<?php echo htmlspecialchars($approver_user_group['user_group_name']) ?>
												<input type="hidden" name="menu_user_group_approver[]" value="<?php echo $approver_user_group['menu_user_group_ugid'] ?>"/>
											</label>
											<span>
												<button type="button" class="btn btn-default btn-xs" onclick="removeItem(this); return false;">
													<i class="glyphicon glyphicon-remove"></i>
												</button>
											</span>
										</div>
									</span>
								<?php } ?>
							</div>
						</form>
					</div>
					<div id="tabs-2">
						<form class="form-horizontal" role="form" id="form2" name="form2" action="act.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Meta Tag:</label>
								<div class="col-xs-10">
									<textarea id="menu_eng_meta_tag" name="menu_eng_meta_tag" class="form-control" style="resize:none;"><?php echo $menu_eng_meta_tag ?></textarea>
								</div>
							</div>
						</form>
						<form class="form-horizontal" role="form" id="eng_res_form" name="eng_res_form" action="act.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Resource files:</label>
								<div class="col-xs-10">
									<input type="file" id="upload_eng_resource" name="upload_eng_resource" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-plus" data-badge="false" multiple accept=".css,.js">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px;">
								<div class="col-xs-5 vcenter">
									Filename
								</div>
								<div class="col-xs-2 vcenter" style="text-align:center">
									Show in Dev
								</div>
								<div class="col-xs-2 vcenter" style="text-align:center">
									Show in Prod
								</div>
								<div class="vcenter" style="text-align:left">
									Action
								</div>
							</div>
							<?php foreach($eng_menu_res_list as $menu_res) { ?>
								<div class="form-group res_row" style="margin-bottom:5px;">
									<div class="col-xs-5 control-label vcenter">
										<?php echo htmlspecialchars($menu_res['menu_res_filename']) ?>
									</div>
									<div class="col-xs-2 vcenter">
										<div class="checkbox" style="text-align:center">
											<label>
												<input type="checkbox" name="menu_res_show_in_dev" value="Y" <?php echo $menu_res['menu_res_show_in_dev'] == "Y" ? "checked" : "" ?>/>
											</label>
										</div>
									</div>
									<div class="col-xs-2 vcenter">
										<div class="checkbox" style="text-align:center">
											<label>
												<input type="checkbox"name="menu_res_show_in_prod" value="Y" <?php echo $menu_res['menu_res_show_in_dev'] == "Y" ? "checked" : "" ?>/>
											</label>
										</div>
									</div>
									<div class="vcenter" style="text-align:left">
										<a role="button" class="btn btn-default" href="<?php echo $SYSTEM_HOST . "download/menu_res/" . $menu_res['menu_res_id'] . "/" . $menu_res['menu_res_filename'] ?>" target="_blank">
											<i class="glyphicon glyphicon-download-alt"></i> Download
										</a>
										<button type="button" class="btn btn-default" onclick="removeResItem(this); return false;">
											<i class="glyphicon glyphicon-remove"></i> Remove
										</button>
										<input type="hidden" name="tmp_id" value=""/>
										<input type="hidden" name="menu_res_id" value="<?php echo $menu['menu_res_id'] ?>"/>
									</div>
								</div>
							<?php } ?>
						</form>
					</div>
					<div id="tabs-3">
						<form class="form-horizontal" role="form" id="form3" name="form3" action="act.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Meta Tag:</label>
								<div class="col-xs-10">
									<textarea id="menu_chi_meta_tag" name="menu_chi_meta_tag" class="form-control" style="resize:none;"><?php echo $menu_chi_meta_tag ?></textarea>
								</div>
							</div>
						</form>
						<form class="form-horizontal" role="form" id="chi_res_form" name="chi_res_form" action="act.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Resource files:</label>
								<div class="col-xs-10">
									<input type="file" id="upload_chi_resource" name="upload_chi_resource" class="filestyle" data-input="false" data-iconName="glyphicon glyphicon-plus" data-badge="false" multiple accept=".css,.js">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px;">
								<div class="col-xs-5 vcenter">
									Filename
								</div>
								<div class="col-xs-2 vcenter" style="text-align:center">
									Show in Dev
								</div>
								<div class="col-xs-2 vcenter" style="text-align:center">
									Show in Prod
								</div>
								<div class="vcenter" style="text-align:left">
									Action
								</div>
							</div>
							<?php foreach($chi_menu_res_list as $menu_res) { ?>
								<div class="form-group res_row" style="margin-bottom:5px;">
									<div class="col-xs-5 control-label vcenter">
										<?php echo htmlspecialchars($menu_res['menu_res_filename']) ?>
									</div>
									<div class="col-xs-2 vcenter">
										<div class="checkbox" style="text-align:center">
											<label>
												<input type="checkbox" name="menu_res_show_in_dev" value="Y" <?php echo $menu_res['menu_res_show_in_dev'] == "Y" ? "checked" : "" ?>/>
											</label>
										</div>
									</div>
									<div class="col-xs-2 vcenter">
										<div class="checkbox" style="text-align:center">
											<label>
												<input type="checkbox"name="menu_res_show_in_prod" value="Y" <?php echo $menu_res['menu_res_show_in_dev'] == "Y" ? "checked" : "" ?>/>
											</label>
										</div>
									</div>
									<div class="vcenter" style="text-align:left">
										<a role="button" class="btn btn-default" href="<?php echo $SYSTEM_HOST . "download/menu_res/" . $menu_res['menu_res_id'] . "/" . $menu_res['menu_res_filename'] ?>" target="_blank">
											<i class="glyphicon glyphicon-download-alt"></i> Download
										</a>
										<button type="button" class="btn btn-default" onclick="removeResItem(this); return false;">
											<i class="glyphicon glyphicon-remove"></i> Remove
										</button>
										<input type="hidden" name="tmp_id" value=""/>
										<input type="hidden" name="menu_res_id" value="<?php echo $menu['menu_res_id'] ?>"/>
									</div>
								</div>
							<?php } ?>
						</form>
					</div>
				</div>
			</div>
			<div class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-danger" onclick="resetForm(); return false;">
						<i class="fa fa-undo fa-lg"></i> Reset
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>