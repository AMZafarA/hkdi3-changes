<?php
	include_once "../include/config.php";
	
	if(session_id() == "")
		session_start();
	
	if($_SESSION['sess_login'] == ""){
		echo json_encode(array());
		return;
	}

	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$term = $_REQUEST['term'] ?? '';
	$user_group_id = $_REQUEST['user_group_id'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$sql = " user_group_status = ? AND user_group_name like ? ";
	$parameters = array(STATUS_ENABLE, "%" . $term . "%");

	$user_group_list = explode(",", $user_group_id);
	
	if(is_array($user_group_list) && count($user_group_list) > 0){
		$sql .= " AND user_group_id not in (" . implode(',', array_fill(0, count($user_group_list), '?'))  . ") ";
		$parameters = array_merge($parameters, $user_group_list);
	}
	
	$user_group_list = DM::findAll("user_group", $sql, " user_group_name ", $parameters);
	
	$result = array();
	
	foreach($user_group_list as $user_group){
		$item = array();
		$item['id'] = $user_group['user_group_id'];
		$item['value'] = $user_group['user_group_name'];
		$result[] = $item;
	}

	unset($user_group_list);

	echo json_encode($result);
	unset($result);
?>