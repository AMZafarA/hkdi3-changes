<?php
include "../include/config.php";
include $SYSTEM_BASE . "include/function.php";
include $SYSTEM_BASE . "session.php";
include $SYSTEM_BASE . "include/system_message.php";
include $SYSTEM_BASE . "include/classes/DataMapper.php";
include $SYSTEM_BASE . "include/classes/SearchUtil.php";
include $SYSTEM_BASE . "include/classes/SystemUtility.php";
include $SYSTEM_BASE . "include/classes/PermissionUtility.php";
$msg = $_REQUEST['msg'] ?? '';
$international_academic_partners_lv1 = $_REQUEST['international_academic_partners_lv1'] ?? '';
$international_academic_partners_lv2 = $_REQUEST['international_academic_partners_lv2'] ?? '';
DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "industrial") ){
	header("location: ../dashboard.php?msg=F-1000");
	return;
}

$page_no = $_REQUEST['page_no'] ?? '';
$international_academic_partners_id = $_REQUEST['international_academic_partners_id'] ?? '';
$page_size = 10;
if(!$international_academic_partners_id) {
	header("location: list.php?msg=F-1002");
	return;
}
if($page_no == "" || !isInteger($page_no)){
	$page_no = 1;
}
$sql = " ( international_academic_partners_id = ? AND international_academic_partners_section_status = ? OR (international_academic_partners_section_status = ? AND international_academic_partners_section_publish_status in (?, ?))) ";
$parameters = array($international_academic_partners_id, STATUS_ENABLE, STATUS_DISABLE, 'RCA', 'RAA');

$total_record = DM::count("international_academic_partners_section", $sql, $parameters);
$total_page = ceil($total_record / $page_size);
if($total_page == 0)
	$total_page = 1;
if($page_no > $total_page){
	$page_no = $total_page;
}
$result = DM::findAllWithPaging("international_academic_partners_section", $page_no, $page_size, $sql, " international_academic_partners_section_seq DESC ", $parameters);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "../header.php" ?>
	<script language="javascript" src="../js/SearchUtil.js"></script>
	<script>
		function deleteRecord(itemId,itemLv1,itemLv2){
			if(confirm("Are you sure you want to delete this record ?")){
				$("#processType").val('delete');
				$("#international_academic_partners_id").val(itemId);
				$("#international_academic_partners_lv1").val(itemLv1);
				$("#international_academic_partners_lv2").val(itemLv2);
				$('#approvalModalDialog').modal('show');
			}
		}

		function undeleteRecord(itemId,itemLv1,itemLv2){
			if(confirm("Are you sure you want to undelete this record ?")){
				$("#processType").val('undelete');
				$("#international_academic_partners_id").val(itemId);
				$("#international_academic_partners_lv1").val(itemLv1);
				$("#international_academic_partners_lv2").val(itemLv2);
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form2;
					frm.submit();
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form2;
					frm.submit();
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		$( document ).ready(function() {
			$("#page_first").bind('click', function(){
				$("#page_no").val(1);
				$("#form1").submit();
			});
			$("#page_last").bind('click', function(){
				$("#page_no").val(<?php echo $total_page ?>);
				$("#form1").submit();
			});
			$("#page_prev").bind('click', function(){
				$("#page_no").val(<?php echo $page_no == 1 ? 1 : $page_no - 1 ?>);
				$("#form1").submit();
			});
			$("#page_next").bind('click', function(){
				$("#page_no").val(<?php echo $page_no < $total_page ? $page_no + 1 : $total_page ?>);
				$("#form1").submit();
			});

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>
		});
	</script>
	<style>
		.table > tbody > tr > td { vertical-align:middle; }
	</style>
</head>
<body>
	<?php include "../menu.php" ?>
	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">

					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li class="active">Industry</li>
				<li class="active">International Academic Partners</li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				<?php if(PermissionUtility::canCreateByKey($_SESSION['sess_id'], "industrial")){ ?>
					<button type="button" class="btn btn-default" onclick="location.href='international_academic_partners_section.php'; return false;">
						<span class="glyphicon glyphicon-plus"></span> New
					</button>
				<?php } ?>
			</span>
		</div>
		<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
			<div class="row">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="col-xs-1">#</th>
							<th class="col-xs-9">Name</th>
							<th class="col-xs-2"></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$counter = 0;
						foreach($result as $international_academic_partners) {
							$counter++;
							?>
							<tr <?php echo $international_academic_partners['international_academic_partners_section_status'] == STATUS_ENABLE ? "" : "class='warning'" ?>>
								<td>
									<?php echo $counter ?>
								</td>
								<td>
									<?php echo htmlspecialchars($international_academic_partners['international_academic_partners_section_name_lang1']) ?>
								</td>
								<td>
									<a role="button" class="btn btn-primary btn-sm" href="international_academic_partners_section.php?international_academic_partners_section_id=<?php echo $international_academic_partners['international_academic_partners_section_id'] ?>">
										<span class="fa fa-folder-open"></span> Edit
									</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="row" style="float:right">
				<ul class="pager">
					<li>
						<button id="page_first" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-backward"></span> First
						</button>
					</li>
					<li>
						<button id="page_prev" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-chevron-left"></span> Previous
						</button>
					</li>
					<li>
						<button id="page_next" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-chevron-right"></span> Next
						</button>
					</li>
					<li>
						<button id="page_last" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-forward"></span> Last
						</button>
					</li>
				</ul>
				<input type="hidden" id="page_no" name="page_no" value=""/>
			</div>
		</form>
		<form class="form-horizontal" role="form" id="form2" name="form2" action="act.php" method="post">
			<input type="hidden" id="type" name="type" value="international_academic_partners"/>
			<input type="hidden" id="processType" name="processType" value=""/>
			<input type="hidden" id="approval_remarks" name="approval_remarks" value=""/>
			<input type="hidden" id="approval_launch_date" name="approval_launch_date" value=""/>
			<input type="hidden" id="international_academic_partners_id" name="international_academic_partners_id" value=""/>
		</form>
	</div>
	<!-- End page content -->
	<?php include "../footer.php" ?>
</body>
</html>