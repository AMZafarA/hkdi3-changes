<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/Logger.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);


$type = $_REQUEST['type'] ?? '';
$processType = $_REQUEST['processType'] ?? '';
DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
$type_name = "international_academic_partners_section";
$permission_name = "international";
if($type=="international_academic_partners_section" )
{
	$id = $_REQUEST['international_academic_partners_section_id'] ?? '';
	$international_academic_partners_section_lv1 = $_REQUEST['international_academic_partners_section_lv1'] ?? '';
	$international_academic_partners_section_lv2 = $_REQUEST['international_academic_partners_section_lv2'] ?? '';
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "international")){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
	}

	if($processType == "" || $processType == "submit_for_approval") {
		$values = $_REQUEST;
		$isNew = !isInteger($id) || $id == 0;
		$timestamp = getCurrentTimestamp();

		if($isNew){
			$values['international_academic_partners_section_publish_status'] = "D";
			$values['international_academic_partners_section_status'] = STATUS_ENABLE;
			$values['international_academic_partners_section_created_by'] = $_SESSION['sess_id'];
			$values['international_academic_partners_section_created_date'] = $timestamp;
			$values['international_academic_partners_section_updated_by'] = $_SESSION['sess_id'];
			$values['international_academic_partners_section_updated_date'] = $timestamp;
			$values['international_academic_partners_section_type'] = "1";

			$id = DM::insert('international_academic_partners_section', $values);

			if(!file_exists($international_academic_partners_section_path . $id. "/")){
				mkdir($international_academic_partners_section_path . $id . "/", 0775, true);
			}

		} else {
			$ObjRec = DM::load('international_academic_partners_section', $id);

			if($ObjRec['international_academic_partners_section_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			$values['international_academic_partners_section_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';
			$values['international_academic_partners_section_updated_by'] = $_SESSION['sess_id'];
			$values['international_academic_partners_section_updated_date'] = $timestamp;

			DM::update('international_academic_partners_section', $values);
		}

		$ObjRec = DM::load('international_academic_partners_section', $id);
		$international_academic_partners_section_img_arr = array();
		foreach($international_academic_partners_section_img_arr as $key=>$val){
			if($_FILES[$val]['tmp_name'] != ""){
				if($ObjRec[$val] != "" && file_exists($international_academic_partners_section_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($international_academic_partners_section_path  . $id . "/" . $ObjRec[$val]);
				}
				$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);
				$newname = moveFile($international_academic_partners_section_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

				list($width, $height) = getimagesize($international_academic_partners_section_path . $id . "/".$newname);
				if($val =='international_academic_partners_section_thumb'){
					$newWidth ="550";
					$newHeight="550";
				}else{
					$newWidth ="699";
					$newHeight="462";
				}
				cropImage($international_academic_partners_section_path. $id."/". $newname, $newWidth,$newHeight , $international_academic_partners_section_path. $id."/". $newname);
												/*if($width > '1500'){
							resizeImageByWidth($alumni_path. $id."/". $newname, "1500" , $alumni_path. $id."/". $newname);
						}else if($height > '1000'){
							resizeImageByHeight($alumni_path. $id."/". $newname, "1000" , $alumni_path. $id."/". $newname);
						}*/

						$values = array();
						$values[$val] = $newname;
						$values['international_academic_partners_section_id'] = $id;
						DM::update('international_academic_partners_section', $values);
						unset($values);
						$ObjRec = DM::load('international_academic_partners_section', $id);
					}
				}

				if($processType == ""){
					Logger::log($_SESSION['sess_id'], $timestamp, "international_academic_partners_section '" . $ObjRec['international_academic_partners_section_name_lang1'] . "' (ID:" . $ObjRec['international_academic_partners_section_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
					NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, $isNew ? "CS" : "ES");
				} else if($processType == "submit_for_approval"){
					ApprovalService::withdrawApproval( $permission_name,$type, $id, $timestamp);
					Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['master_lecture_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
					NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $permission_name, $isNew ? "CS" : "ES");
					$files = array($SYSTEM_HOST."international_academic_partners_section/international_academic_partners_section.php?international_academic_partners_section_id=".$id);
					$approval_id = ApprovalService::createApprovalRequest( $permission_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
					NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);
				}
				header("location:international_academic_partners_section.php?international_academic_partners_section_id=" . $id . "&msg=S-1001");
				return;
			}

			else if($processType == "delete" && isInteger($id)) {
				$timestamp = getCurrentTimestamp();

				$ObjRec = DM::load('international_academic_partners_section', $id);

				if($ObjRec == NULL || $ObjRec == "")
				{
					header("location: list.php?international_academic_partners_section_lv1=".$international_academic_partners_section_lv1."&international_academic_partners_section_lv2=".$international_academic_partners_section_lv2."&msg=F-1002");
					return;
				}

				if($ObjRec['international_academic_partners_section_status'] == STATUS_DISABLE)
				{
					header("location: list.php?international_academic_partners_section_lv1=".$international_academic_partners_section_lv1."&international_academic_partners_section_lv2=".$international_academic_partners_section_lv2."&msg=F-1004");
					return;
				}
				$isPublished = SystemUtility::isPublishedRecord("international_academic_partners_section", $id);
				$values['international_academic_partners_section_id'] = $id;
				$values['international_academic_partners_section_publish_status'] = $isPublished ? 'RAA' : 'AL';
				$values['international_academic_partners_section_status'] = STATUS_DISABLE;
				$values['international_academic_partners_section_updated_by'] = $_SESSION['sess_id'];
				$values['international_academic_partners_section_updated_date'] = getCurrentTimestamp();

				DM::update('international_academic_partners_section', $values);
				unset($values);
				if($isPublished){
					ApprovalService::withdrawApproval($permission_name, $type, $id, $timestamp);
					Logger::log($_SESSION['sess_id'], $timestamp, "master_lecture '" . $ObjRec['master_lecture_name_lang1'] . "' (ID:" . $ObjRec['master_lecture_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
					NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $$permission_name, "DS");
					$files = array($SYSTEM_HOST."international_academic_partners_section/international_academic_partners_section.php?international_academic_partners_section_id=".$id);
					$approval_id = ApprovalService::createApprovalRequest($permission_name, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
					NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), $permission_name, "PA", $approval_id);
				}else{
					NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), $permission_name, "D");
				}


				header("location: list.php?international_academic_partners_section_lv1=".$international_academic_partners_section_lv1."&international_academic_partners_section_lv2=".$international_academic_partners_section_lv2."&msg=S-1002");
				return;
			} else if($processType == "undelete" && isInteger($id)) {
				$timestamp = getCurrentTimestamp();

				$ObjRec = DM::load('international_academic_partners_section', $id);

				if($ObjRec == NULL || $ObjRec == "")
				{
					header("location: list.php?international_academic_partners_section_lv1=".$international_academic_partners_section_lv1."&international_academic_partners_section_lv2=".$international_academic_partners_section_lv2."&msg=F-1002");
					return;
				}

				if($ObjRec['international_academic_partners_section_status'] == STATUS_ENABLE || $ObjRec['international_academic_partners_section_publish_status'] == 'AL')
				{
					header("location: list.php?international_academic_partners_section_lv1=".$international_academic_partners_section_lv1."&international_academic_partners_section_lv2=".$international_academic_partners_section_lv2."&msg=F-1006");
					return;
				}

				$values['international_academic_partners_section_id'] = $id;
				$values['international_academic_partners_section_publish_status'] = 'D';
				$values['international_academic_partners_section_status'] = STATUS_ENABLE;
				$values['international_academic_partners_section_updated_by'] = $_SESSION['sess_id'];
				$values['international_academic_partners_section_updated_date'] = getCurrentTimestamp();

				DM::update('international_academic_partners_section', $values);
				unset($values);


				$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
				$sql .= " AND approval_approval_status in (?, ?, ?) ";

				$parameters = array(STATUS_ENABLE, 'international_academic_partners_section', $id, 'RCA', 'RAA', 'APL');

				$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

				foreach($approval_requests as $approval_request){
					$values = array();
					$values['approval_id'] = $approval_request['approval_id'];
					$values['approval_approval_status'] = 'W';
					$values['approval_updated_by'] = $_SESSION['sess_id'];
					$values['approval_updated_date'] = $timestamp;

					DM::update('approval', $values);
					unset($values);

					$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
					DM::query($sql, array('N', $approval_request['approval_id']));

					$recipient_list = array("E", "C");

					if($approval_request['approval_approval_status'] != "RCA"){
						$recipient_list[] = "A";
					}

					$message = "";

					switch($approval_request['approval_approval_status']){
						case "RCA":
						$message = "Request for checking";
						break;
						case "RAA":
						$message = "Request for approval";
						break;
						case "APL":
						$message = "Pending to launch";
						break;
					}

					$message .= " of international_academic_partners_section '" . $ObjRec['international_academic_partners_section_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

					NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $permission_name, "WA");
				}

				Logger::log($_SESSION['sess_id'], $timestamp, "international_academic_partners_section '" . $ObjRec['international_academic_partners_section_name_lang1'] . "' (ID:" . $ObjRec['international_academic_partners_section_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $permission_name, "WA");

				header("location: list.php?msg=S-1003");
				return;
			}
		}

		header("location:../dashboard.php?msg=F-1001");
		return;
		?>
