<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/Mutex.php";
	include_once $SYSTEM_BASE . "include/classes/generator/ContentGenerator.php";
	include_once $SYSTEM_BASE . "include/classes/generator/MenuGenerator.php";
	include_once $SYSTEM_BASE . "include/classes/ContentSynchronizer.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="menu" )
	{
		$id = isset($_REQUEST['menu_id']) ? $_REQUEST['menu_id'] : "";

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}

		if($processType == "") {
			$values = $_REQUEST;

			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			if($values['menu_show_in_dev'] != "Y"){
				$values['menu_show_in_dev'] = "N";
			}

			if($values['menu_show_in_prod'] != "Y"){
				$values['menu_show_in_prod'] = "N";
			}

			if($values['menu_inherit_parent'] != "Y"){
				$values['menu_inherit_parent'] = "N";
			}

			if($values['menu_link_open_new_browser'] != "Y"){
				$values['menu_link_open_new_browser'] = "N";
			}

			if($values['menu_link_type'] != "U"){
				$values['menu_link_page_id'] = "";
			}

			if($values['menu_link_type'] != "S"){
				$values['menu_link_system_page_id'] = "";
			}

			if($isNew)
			{
				if($values['menu_link_type'] == "U"){
					$page_values = array();
					$page_values['page_current_revision'] = 1;
					$page_values['page_name'] = $values['menu_eng_name'];
					$page_values['page_eng_content'] = "";
					$page_values['page_tchi_content'] = "";
					$page_values['page_publish_status'] = 'D';
					$page_values['page_status'] = STATUS_ENABLE;
					$page_values['page_created_by'] = $_SESSION['sess_id'];
					$page_values['page_created_date'] = $timestamp;
					$page_values['page_updated_by'] = $_SESSION['sess_id'];
					$page_values['page_updated_date'] = $timestamp;
					$page_id = DM::insert('page', $page_values);

					if(!file_exists($SYSTEM_BASE . "uploaded_files/page/" . $page_id)){
						mkdir($SYSTEM_BASE . "uploaded_files/page/" . $page_id, 0775, true);
					}

					if(!file_exists($SYSTEM_BASE . "uploaded_files/page_file/" . $page_id)){
						mkdir($SYSTEM_BASE . "uploaded_files/page_file/" . $page_id, 0775, true);
					}

					$values['menu_link_page_id'] = $page_id;

					unset($page_values);
				}

				$values['menu_dev_banner'] = "";
				$values['menu_prod_banner'] = "";
				$values['menu_type'] = 'M';
				if($_REQUEST['menu_parent_id'] == "")
					$values['menu_sequence'] = DM::count("menu", " menu_status = ? AND menu_parent_id is null ", array(STATUS_ENABLE)) + 1;
				else
					$values['menu_sequence'] = DM::count("menu", " menu_status = ? AND menu_parent_id = ? ", array(STATUS_ENABLE, $_REQUEST['menu_parent_id'])) + 1;

				$values['menu_status'] = STATUS_ENABLE;
				$values['menu_created_by'] = $_SESSION['sess_id'];
				$values['menu_created_date'] = $timestamp;
				$values['menu_updated_by'] = $_SESSION['sess_id'];
				$values['menu_updated_date'] = $timestamp;

				$id = DM::insert('menu', $values);

				Logger::log($_SESSION['sess_id'], $timestamp, "Menu " . $values['menu_eng_name'] . " (ID:" . $id . ") is created by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			} else {
				$ObjRec = DM::load('menu', $id);

				if($ObjRec['menu_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}

				if($ObjRec['menu_link_type'] != "U" && $values['menu_link_type'] == "U"){
					$page_values = array();
					$page_values['page_current_revision'] = 1;
					$page_values['page_name'] = $values['menu_eng_name'];
					$page_values['page_publish_status'] = 'D';
					$page_values['page_status'] = STATUS_ENABLE;
					$page_values['page_created_by'] = $_SESSION['sess_id'];
					$page_values['page_created_date'] = $timestamp;
					$page_values['page_updated_by'] = $_SESSION['sess_id'];
					$page_values['page_updated_date'] = $timestamp;
					$page_id = DM::insert('page', $page_values);

					if(!file_exists($SYSTEM_BASE . "uploaded_files/page/" . $page_id)){
						mkdir($SYSTEM_BASE . "uploaded_files/page/" . $page_id, 0775, true);
					}

					if(!file_exists($SYSTEM_BASE . "uploaded_files/page_file/" . $page_id)){
						mkdir($SYSTEM_BASE . "uploaded_files/page_file/" . $page_id, 0775, true);
					}

					$values['menu_link_page_id'] = $page_id;
					unset($page_values);
				} else if($ObjRec['menu_link_type'] == "U" && $values['menu_link_type'] != "U"){
					$page_values = array();
					$page_values['page_id'] = $ObjRec['menu_link_page_id'];
					$page_values['page_status'] = STATUS_DISABLE;
					$page_values['page_updated_by'] = $_SESSION['sess_id'];
					$page_values['page_updated_date'] = $timestamp;
					$page_id = DM::update('page', $page_values);

					$values['menu_link_page_id'] = "";
					unset($page_values);
				}

				$values['menu_updated_by'] = $_SESSION['sess_id'];
				$values['menu_updated_date'] = $timestamp;

				DM::update('menu', $values);

				Logger::log($_SESSION['sess_id'], $timestamp, "Menu " . $values['menu_eng_name'] . " (ID:" . $id . ") is updated by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			}

			unset($values);

			$ObjRec = DM::load('menu', $id);

			if(!file_exists($SYSTEM_BASE . "uploaded_files/menu/" . $id)){
				mkdir($SYSTEM_BASE . "uploaded_files/menu/" . $id, 0775, true);
			}

			if(!file_exists($SYSTEM_BASE . "uploaded_files/menu_res/" . $id)){
				mkdir($SYSTEM_BASE . "uploaded_files/menu_res/" . $id, 0775, true);
			}

			DM::query(" DELETE FROM menu_user_group WHERE menu_user_group_mid = ? ", array($id));

			if($ObjRec['menu_inherit_parent'] == "N"){
				foreach(array("editor", "checker", "approver") as $actor){
					$menu_user_group_list = $_REQUEST['menu_user_group_' . $actor] ?? '';

					if($menu_user_group_list == "" || !is_array($menu_user_group_list))
						continue;

					$counter = 1;
					foreach($menu_user_group_list as $user_group_id){
						$values = array();
						$values['menu_user_group_type'] = strtoupper(substr($actor, 0, 1));
						$values['menu_user_group_mid'] = $id;
						$values['menu_user_group_ugid'] = $user_group_id;
						$values['menu_user_group_sequence'] = $counter;

						DM::insert('menu_user_group', $values);
						unset($values);
						$counter++;
					}
				}
			}

			foreach(array("dev", "prod") as $banner_type) {
				if($_FILES['upload_' . $banner_type . '_banner']['tmp_name'] != ""){
					$ObjRec = DM::load('menu', $id);

					if($ObjRec['menu_' . $banner_type . '_banner'] != "" && file_exists($SYSTEM_BASE . "uploaded_files/menu/" . $id . "/" . $ObjRec['menu_' . $banner_type . '_banner'])){
			//			@unlink($SYSTEM_BASE . "uploaded_files/menu/" . $id . "/" . $ObjRec['menu_' . $banner_type . '_banner']);
					}

					$newname = moveFile($SYSTEM_BASE . "uploaded_files/menu/" . $id . "/", $id, pathinfo($_FILES['upload_' . $banner_type . '_banner']['name'],PATHINFO_EXTENSION) , $_FILES['upload_' . $banner_type . '_banner']['tmp_name']);

					$values = array();
					$values['menu_' . $banner_type . '_banner'] = $newname;
					$values['menu_id'] = $id;

					DM::update('menu', $values);
					unset($values);
				}
			}

			if($ObjRec['menu_link_type'] == 'S' && $ObjRec['menu_link_system_page_id'] == '11'){
				foreach(array("dev", "prod") as $banner_type) {
					if($ObjRec['menu_' . $banner_type . '_banner'] != "" && file_exists($SYSTEM_BASE . "uploaded_files/menu/" . $id . "/" . $ObjRec['menu_' . $banner_type . '_banner'])){
					//	@unlink($SYSTEM_BASE . "uploaded_files/menu/" . $id . "/" . $ObjRec['menu_' . $banner_type . '_banner']);
					}

					$values = array();
					$values['menu_' . $banner_type . '_banner'] = "";
					$values['menu_id'] = $id;

					DM::update('menu', $values);
					unset($values);
				}
			}

			//handle menu res files
			$menu_res_list = $_REQUEST['menu_res_eng_item'] ?? '';
			$menu_res_id_list = array();

			if($menu_res_list != "" && is_array($menu_res_list)){
				$counter = 1;
				foreach($menu_res_list as $data){
					$menu_res = json_decode($data, true);

					if($menu_res['menu_res_id'] == ""){
						$values = array();
						$values['menu_res_mid'] = $id;
						$values['menu_res_type'] = 'E';
						$values['menu_res_sequence'] = $counter;
						$values['menu_res_filename'] = $menu_res['filename'];
						$values['menu_res_show_in_dev'] = $menu_res['show_in_dev'] == "Y" ? "Y" : "N";
						$values['menu_res_show_in_prod'] = $menu_res['show_in_prod'] == "Y" ? "Y" : "N";
						$values['menu_res_status'] = STATUS_ENABLE;
						$values['menu_res_created_by'] = $_SESSION['sess_id'];
						$values['menu_res_created_date'] = $timestamp;
						$values['menu_res_updated_by'] = $_SESSION['sess_id'];
						$values['menu_res_updated_date'] = $timestamp;

						$menu_res_id = DM::insert('menu_res', $values);
						copy($SYSTEM_BASE . "uploaded_files/tmp/" . $menu_res['tmp_filename'], $SYSTEM_BASE . "uploaded_files/menu_res/" . $id . "/" . $menu_res_id . ".dat");
						$menu_res_id_list[] = $menu_res_id;
						unset($values);
					} else {
						$values = array();
						$values['menu_res_id'] = $menu_res['menu_res_id'];
						$values['menu_res_sequence'] = $counter;
						$values['menu_res_show_in_dev'] = $menu_res['show_in_dev'] == "Y" ? "Y" : "N";
						$values['menu_res_show_in_prod'] = $menu_res['show_in_prod'] == "Y" ? "Y" : "N";
						$values['menu_res_updated_by'] = $_SESSION['sess_id'];
						$values['menu_res_updated_date'] = $timestamp;

						DM::update('menu_res', $values);
						$menu_res_id_list[] = $menu_res['menu_res_id'];
						unset($values);
					}

					$counter++;
				}
			}

			$menu_res_list = $_REQUEST['menu_res_chi_item'] ?? '';

			if($menu_res_list != "" && is_array($menu_res_list)){
				$counter = 1;
				foreach($menu_res_list as $data){
					$menu_res = json_decode($data, true);

					if($menu_res['menu_res_id'] == ""){
						$values = array();
						$values['menu_res_mid'] = $id;
						$values['menu_res_type'] = 'C';
						$values['menu_res_sequence'] = $counter;
						$values['menu_res_filename'] = $menu_res['filename'];
						$values['menu_res_show_in_dev'] = $menu_res['show_in_dev'] == "Y" ? "Y" : "N";
						$values['menu_res_show_in_prod'] = $menu_res['show_in_prod'] == "Y" ? "Y" : "N";
						$values['menu_res_status'] = STATUS_ENABLE;
						$values['menu_res_created_by'] = $_SESSION['sess_id'];
						$values['menu_res_created_date'] = $timestamp;
						$values['menu_res_updated_by'] = $_SESSION['sess_id'];
						$values['menu_res_updated_date'] = $timestamp;

						$menu_res_id = DM::insert('menu_res', $values);
						copy($SYSTEM_BASE . "uploaded_files/tmp/" . $menu_res['tmp_filename'], $SYSTEM_BASE . "uploaded_files/menu_res/" . $id . "/" . $menu_res_id . ".dat");
						$menu_res_id_list[] = $menu_res_id;
						unset($values);
					} else {
						$values = array();
						$values['menu_res_id'] = $menu_res['menu_res_id'];
						$values['menu_res_sequence'] = $counter;
						$values['menu_res_show_in_dev'] = $menu_res['show_in_dev'] == "Y" ? "Y" : "N";
						$values['menu_res_show_in_prod'] = $menu_res['show_in_prod'] == "Y" ? "Y" : "N";
						$values['menu_res_updated_by'] = $_SESSION['sess_id'];
						$values['menu_res_updated_date'] = $timestamp;
						DM::update('menu_res', $values);
						$menu_res_id_list[] = $menu_res['menu_res_id'];
						unset($values);
					}

					$counter++;
				}
			}

			$sql = " menu_res_status = ? AND menu_res_mid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($menu_res_id_list) > 0){
				$sql .= " AND menu_res_id NOT IN (" . implode(',', array_fill(0, count($menu_res_id_list), '?')) . ") ";
				$parameters = array_merge($parameters, $menu_res_id_list);
			}

			$menu_res_list = DM::findAll("menu_res", $sql, " menu_res_sequence ", $parameters);

			foreach($menu_res_list as $menu_res){
				$values = array();
				$values['menu_res_id'] = $menu_res['menu_res_id'];
				$values['menu_res_status'] = STATUS_DISABLE;
				$values['menu_res_updated_by'] = $_SESSION['sess_id'];
				$values['menu_res_updated_date'] = $timestamp;

				$menu_res_id = DM::update('menu_res', $values);
				unset($values);
			}

			unset($menu_res_list);

			PermissionUtility::rebuildPermission();

			header("location:menu.php?menu_id=" . $id . "&msg=S-1001");
			return;
		}

		if($processType == "delete" && isInteger($id)) {
			$ObjRec = DM::load('menu', $id);

			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			if($ObjRec['menu_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}

			$timestamp = getCurrentTimestamp();

			if($ObjRec['menu_link_type'] == "U"){
				$values = array();
				$values['page_id'] = $ObjRec['menu_link_page_id'];
				$values['page_status'] = STATUS_DISABLE;
				$values['page_updated_by'] = $_SESSION['sess_id'];
				$values['page_updated_date'] = $timestamp;

				DM::update('page', $values);
				unset($values);
			}

			recursiveDisable($ObjRec, $_SESSION['sess_id'], $timestamp);
			PermissionUtility::rebuildPermission();

			$url = "list.php?" . ($ObjRec['menu_parent_id'] == "" ? "" : "menu_parent_id=" . $ObjRec['menu_parent_id'] . "&") . "msg=S-1002";

			header("location: " . $url);
			return;
		}

		if($processType == "updateSequence"){
			$menu_list = $_REQUEST['menu_id'] ?? '';
			$menu_parent_id = $_REQUEST['menu_parent_id'] ?? '';

			if($menu_list != "" && is_array($menu_list)){
				$counter = 1;
				$timestamp = getCurrentTimestamp();

				foreach($menu_list as $menu_id){
					$ObjRec = DM::load('menu', $menu_id);

					if($ObjRec == NULL || $ObjRec == "" || $ObjRec['menu_status'] != STATUS_ENABLE){
						continue;
					}

					$values = array();
					$values['menu_id'] = $menu_id;
					$values['menu_sequence'] = $counter;
					$values['menu_updated_by'] = $_SESSION['sess_id'];
					$values['menu_updated_date'] = $timestamp;

					DM::update('menu', $values);
					unset($values);
					$counter++;
				}
			}

			echo json_encode(array("result"=>"ok", "alert_type"=>"alert-success", "icon"=>"glyphicon glyphicon-ok", "message"=>$SYSTEM_MESSAGE['S-1001'] ));
			return;
		}

		if($processType == "generateDevelopment"){
			ignore_user_abort(true);
			set_time_limit(0);

			$mutex = new Mutex("dev_content");
			$mutex->getLock();

			$menuGenerator = new MenuGenerator(ContentGenerator::VERSION_DEV, $SYSTEM_DEVELOPMENT_BASE);
			$menuGenerator->generate(null, null);

			$mutex->releaseLock();

			header("location:list.php?msg=S-1007");
			return;
		}

		if($processType == "generateProduction"){
			ignore_user_abort(true);
			set_time_limit(0);

			$mutex = new Mutex("prod_content");
			$mutex->getLock();

			$menuGenerator = new MenuGenerator(ContentGenerator::VERSION_PROD, $SYSTEM_PRE_PRODUCTION_BASE);
			$menuGenerator->generate(null, null);

			ContentSynchronizer::synchronizeDirectory($SYSTEM_PRE_PRODUCTION_BASE, $SYSTEM_PRODUCTION_BASE);
			ContentSynchronizer::synchronizeDirectoryToRemote($SYSTEM_PRODUCTION_BASE, $PRODUCTION_SERVER_LIST);

			$mutex->releaseLock();

			header("location:list.php?msg=S-1008");
			return;
		}
	}

	header("location:../dashboard.php?msg=F-1001");
	return;

	function recursiveDisable($parent_menu, $user_id, $timestamp){

		$values = array();
		$values['menu_id'] = $parent_menu['menu_id'];
		$values['menu_status'] = STATUS_DISABLE;
		$values['menu_updated_by'] = $user_id;
		$values['menu_updated_date'] = $timestamp;

		DM::update('menu', $values);
		unset($values);

		Logger::log($_SESSION['sess_id'], $timestamp, "Menu " . $parent_menu['menu_eng_name'] . " (ID:" . $id . ") is deleted by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

		$menus = DM::findAll("menu", " menu_status = ? AND menu_parent_id = ? ", " menu_sequence ", array(STATUS_ENABLE, $menu_id));

		foreach($menus as $menu){
			if($menu['menu_link_type'] == "U"){
				$values = array();
				$values['page_id'] = $menu['menu_link_page_id'];
				$values['page_status'] = STATUS_DISABLE;
				$values['page_updated_by'] = $_SESSION['sess_id'];
				$values['page_updated_date'] = $timestamp;

				DM::update('page', $values);
				unset($values);
			}

			recursiveDisable($menu['menu_id'], $user_id, $timestamp);
		}

		unset($menus);
	}
?>
