<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SearchUtil.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	$msg = $_REQUEST['msg'] ?? '';
	$menu_parent_id = $_REQUEST['menu_parent_id'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}
	
	if(!isInteger($menu_parent_id)){
		$menu_parent_id = "";
	}

	$sql = " menu_status = ? AND menu_type = ? ";
	$parameters = array(STATUS_ENABLE, 'M');
	
	if($menu_parent_id != ""){
		$sql .= " AND menu_parent_id = ? ";
		$parameters[] = $menu_parent_id;
	} else {
		$sql .= " AND menu_parent_id is null ";
	}

	$menu_list = DM::findAll("menu", $sql, " menu_sequence ", $parameters);
	
	$parent_list = array();
	
	while($menu_parent_id != ""){
		$parent = DM::load("menu", $menu_parent_id);
		$parent_list[] = $parent;
		$menu_parent_id = $parent['menu_parent_id'];
	}
	
	$parent_list = array_reverse($parent_list);
	
	$parent_idx = "";
	
	foreach($parent_list as $parent){
		$parent_idx .= $parent['menu_sequence'] . ".";
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script language="javascript" src="../js/SearchUtil.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
		<script type="text/x-tmpl" id="alert_template">
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert {%=o.alert_type%}" role="alert">
					<span class="{%=o.icon%}"></span> {%=o.message%}
				</div>
			</div>
		</script>
		<script>
			function deleteRecord(id){
				if(confirm("Are you sure you want to delete this record?")){
					location.href = "act.php?type=menu&processType=delete&menu_id=" + id;
				}
			}
			
			function updateNumbering(){
				$("#menu_table tbody tr").each(function(index, element){
					$(element).find("td:eq(0)").html("<?php echo $parent_idx ?>" + (index+1));
				});
			}
			
			function saveSequence(){
				$.post( "act.php", $( "#form1" ).serialize(),
					function( data ) {
						if($("#alert_row").length > 0) $("#alert_row").detach();
						var content = tmpl("alert_template", data);
						$("#content").prepend(content);
						setTimeout(function() {
							$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
						}, 5000);
					}, "json"
				);
			}
			
			function generateDevContent(){
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					location.href = "act.php?type=menu&processType=generateDevelopment";
				})
				
				$('#loadingModalDialog').modal('show');
			}
			
			function generateProdContent(){
				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					location.href = "act.php?type=menu&processType=generateProduction";
				})
				
				$('#loadingModalDialog').modal('show');
			}

			$( document ).ready(function() {
				$( "#menu_table tbody" ).sortable({
					items: "tr",
					stop:  function( event, ui ) {
						saveSequence();
						updateNumbering();
					}
				});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.table > tbody > tr > td { vertical-align:middle; }
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>
		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-5">
					<li><a href="../dashboard.php">Home</a></li>
					<li class="active">Website Structure</li>
				</ol>
				<span class="breadcrumb-right col-xs-7">
					<button type="button" class="btn btn-default" onclick="generateDevContent(); return false;">
						<span class="glyphicon glyphicon-transfer"></span> Generate Structure (Dev.)
					</button>
					<button type="button" class="btn btn-default" onclick="generateProdContent(); return false;">
						<span class="glyphicon glyphicon-transfer"></span> Generate Structure (Prod)
					</button>
					<button type="button" class="btn btn-default" onclick="location.href='menu.php?menu_parent_id=<?php echo $_REQUEST['menu_parent_id'] ?>'; return false;">
						<span class="glyphicon glyphicon-plus"></span> New
					</button>
				</span>
			</div>
			<form class="form-horizontal" role="form" id="form1" name="form1" action="list.php" method="post">
				<div class="row" style="padding-bottom:10px;">
					<label class="col-xs-12 control-label">
						URL: / <a href="list.php">Home</a> /
						<?php foreach($parent_list as $parent) { ?>
							<a href="list.php?menu_parent_id=<?php echo $parent['menu_id'] ?>"><?php echo htmlspecialchars($parent['menu_eng_name']) ?></a> /
						<?php } ?>
					</label>
				</div>
				<div class="row">
					<table id="menu_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="col-xs-1">#</th>
								<th class="col-xs-4">Menu Name</th>
								<th class="col-xs-1">In Dev.</th>
								<th class="col-xs-1">In Prod.</th>
								<th class="col-xs-3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$row_count = 1;
								foreach($menu_list as $row)
								{
									$menu_id = $row['menu_id'];
									$menu_eng_name = htmlspecialchars($row['menu_eng_name']);
							?>
									<tr>
										<td class="col-xs-1">
											<?php echo $parent_idx ?><?php echo $row_count ?>
										</td>
										<td class="col-xs-4"><?php echo $menu_eng_name ?></td>
										<td class="col-xs-1"><?php echo $row['menu_show_in_dev'] == "Y" ? "Yes" : "" ?></td>
										<td class="col-xs-1"><?php echo $row['menu_show_in_prod'] == "Y" ? "Yes" : "" ?></td>
										<td class="col-xs-3">
											<a role="button" class="btn btn-primary btn-sm" href="list.php?menu_parent_id=<?php echo $menu_id ?>">
												<i class="fa fa-level-down"></i> To Sub-Level
											</a>
											<a role="button" class="btn btn-primary btn-sm" href="menu.php?menu_id=<?php echo $menu_id ?>">
												<i class="fa fa-folder-open"></i> Edit
											</a>
											<a role="button" class="btn btn-danger btn-sm" href="#" onclick="deleteRecord('<?php echo $menu_id ?>'); return false;">
												<i class="glyphicon glyphicon-trash"></i> Delete
											</a>
											<input type="hidden" name="menu_id[]" value="<?php echo $menu_id ?>"/>
										</td>
									</tr>
							<?php
								$row_count++;
								}
								unset($resultObj);
							?>
						</tbody>
					</table>
				</div>
				<input type="hidden" name="type" value="menu"/>
				<input type="hidden" name="processType" value="updateSequence"/>
				<input type="hidden" name="menu_parent_id" value="<?php echo $menu_parent_id ?>"/>
			</form>
		</div>
		<!-- End page content -->

		<?php include_once "../footer.php" ?>
	</body>
</html>