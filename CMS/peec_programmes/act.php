<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/Logger.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";
include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
RegenerateSitemap::regen($dbcon);
$type = $_REQUEST['type'] ?? '';
$processType = $_REQUEST['processType'] ?? '';
$section_header = $_REQUEST['section_header'] ?? '';
DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
$master_lv1 = $_REQUEST['master_lv1'] ?? '';
$master_lv2 = $_REQUEST['master_lv2'] ?? '';
$type_name = "peec_programmes";
if($type=="peec_programmes" ) {
	
	$id = $_REQUEST['peec_programmes_id'] ?? '';
	$randomString = $_REQUEST['randomString'] ?? '';
	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "peec_programmes")){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}
	if($processType == "" || $processType == "submit_for_approval") {
		$timestamp = getCurrentTimestamp();
		if($id != "") {
			$ObjRec = DM::load('peec_programmes', $id);
			if($ObjRec['peec_programmes_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}
		}
		$values = $_REQUEST;
		$isNew = !isInteger($id) || $id == 0;
		$timestamp = getCurrentTimestamp();
		$values['peec_programmes_index_show'] = $values['peec_programmes_index_show'] ?? '';
						//unset($values['small_banner_name']);
		if($isNew){
			$values['peec_programmes_publish_status'] = "D";
			$values['peec_programmes_status'] = STATUS_ENABLE;
			$values['peec_programmes_created_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_created_date'] = $timestamp;
			$values['peec_programmes_updated_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_updated_date'] = $timestamp;
			$id = DM::insert('peec_programmes', $values);
			if(!file_exists($peec_programmes_path . $id. "/")){
				mkdir($peec_programmes_path . $id . "/", 0775, true);
			}
			rename ($peec_programmes_path.$randomString, $peec_programmes_path.$id);

		} else {
			$ObjRec = DM::load('peec_programmes', $id);
			if($ObjRec['peec_programmes_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}
			$values['peec_programmes_publish_status'] = $processType == "submit_for_approval" ? "RAA" : 'D';
			$values['peec_programmes_updated_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_updated_date'] = $timestamp;
			DM::update('peec_programmes', $values);
		}
		$page_section_list = array();
		$text_block_section = $_REQUEST['text_block_section'] ?? '';
		$collapsible_text_block_section = $_REQUEST['collapsible_text_block_section'] ?? '';
		$news_section = $_REQUEST['news_section'] ?? '';
		$project_section = $_REQUEST['project_section'] ?? '';
		$photo_section = $_REQUEST['photo_section'] ?? '';
		$contact_section = $_REQUEST['contact_section'] ?? '';
		$acknowledgement_section = $_REQUEST['acknowledgement_section'] ?? '';
		$visit_section = $_REQUEST['visit_section'] ?? '';
		$peec_programmes_section_list = array();
		if($visit_section != "" && is_array($visit_section)){
			foreach($visit_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;
				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}

			$peec_programmes_section_file_list = array();
			$lang1_files = $text_block['lang1_files'] ?? '';
			if($lang1_files != "" && is_array($lang1_files)){
				$count = 1;
				foreach($lang1_files as $lang1_file){
					if($lang1_file['peec_programmes_section_file_id'] == "") {
						unset($lang1_file['peec_programmes_section_file_id']);
						$lang1_file['peec_programmes_section_file_psid'] = $peec_programmes_section_id;
						$lang1_file['peec_programmes_section_file_pid'] = $id;
						$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
						$lang1_file['peec_programmes_section_file_sequence'] = $count++;
						$lang1_file['peec_programmes_section_file_filename'] = $lang1_file['filename'];
						$lang1_file['peec_programmes_section_file_status'] = STATUS_ENABLE;
						$lang1_file['peec_programmes_section_file_created_by'] = $_SESSION['sess_id'];
						$lang1_file['peec_programmes_section_file_created_date'] = $timestamp;
						$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
						$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;

						$peec_programmes_section_file_id = DM::insert('peec_programmes_section_file', $lang1_file);
						$peec_programmes_section_file_list[] = $peec_programmes_section_file_id;
						if(!file_exists($peec_programmes_file_path . $id. "/".$peec_programmes_section_file_id."/")){
							mkdir($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/", 0775, true);
						}

						if($lang1_file['peec_programmes_section_file_type'] == "Partner"){
							copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
							list($width, $height) = getimagesize($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
							if($width > '1000'){
								resizeImageByWidth($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "1000" , $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
							}else if($height > '1000'){
								resizeImageByHeight($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "1000" , $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
							}
							$crop = $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/". "crop_".$lang1_file['filename'];
							cropImage($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "300","300" , $crop);
						}
					} else {
						$peec_programmes_section_file_list[] = $lang1_file['peec_programmes_section_file_id'];
						$peec_programmes_section_file_id = $lang1_file['peec_programmes_section_file_id'];
						$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
						$lang1_file['peec_programmes_section_file_sequence'] = $count++;
						$lang1_file['peec_programmes_section_file_sequence'] = $count++;
						$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
						$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;

						DM::update('peec_programmes_section_file', $lang1_file);
					}
				}
			}
		}

		if($collapsible_text_block_section != "" && is_array($collapsible_text_block_section)){
			foreach($collapsible_text_block_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}
		}
		if($text_block_section != "" && is_array($text_block_section)){
			foreach($text_block_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}
		}
		if($news_section != "" && is_array($news_section)){
			foreach($news_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;
				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}
		}
		if($project_section != "" && is_array($project_section)){
			foreach($project_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}
		}

		if($photo_section != "" && is_array($photo_section)){
			foreach($photo_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$peec_programmes_section_id = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}

				$peec_programmes_section_file_list = array();
				$lang1_files = $text_block['lang1_files'];
				if($lang1_files != "" && is_array($lang1_files)){
					$count = 1;
					foreach($lang1_files as $lang1_file){
						if($lang1_file['peec_programmes_section_file_id'] == "") {
							unset($lang1_file['peec_programmes_section_file_id']);
							$lang1_file['peec_programmes_section_file_psid'] = $peec_programmes_section_id;
							$lang1_file['peec_programmes_section_file_pid'] = $id;
							$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_filename'] = $lang1_file['filename'];
							$lang1_file['peec_programmes_section_file_status'] = STATUS_ENABLE;
							$lang1_file['peec_programmes_section_file_created_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_created_date'] = $timestamp;
							$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;
							echo "IN ".$peec_programmes_section_id."|".$lang1_file['filename']."<br><br>";
							$peec_programmes_section_file_id = DM::insert('peec_programmes_section_file', $lang1_file);
							$peec_programmes_section_file_list[] = $peec_programmes_section_file_id;
							if(!file_exists($peec_programmes_file_path . $id. "/".$peec_programmes_section_file_id."/")){
								mkdir($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/", 0775, true);
							}

							if($lang1_file['peec_programmes_section_file_type'] == "Partner"){
								copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
								list($width, $height) = getimagesize($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
								if($width > '1000'){
									resizeImageByWidth($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "1000" , $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
								}else if($height > '1000'){
									resizeImageByHeight($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "1000" , $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
								}
								$crop = $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/". "crop_".$lang1_file['filename'];
								cropImage($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename'], "300","300" , $crop);
							}
						} else {
							$peec_programmes_section_file_list[] = $lang1_file['peec_programmes_section_file_id'];
							$peec_programmes_section_file_id = $lang1_file['peec_programmes_section_file_id'];
							$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;

							DM::update('peec_programmes_section_file', $lang1_file);
							echo "UP ".$peec_programmes_section_id."|".($lang1_file['filename'] ?? '')."<br><br>";
						}
					}
				}
				$sql = " peec_programmes_section_file_status = ? AND peec_programmes_section_file_psid = ? ";

				$parameters = array(STATUS_ENABLE, $peec_programmes_section_id);

				if(count($peec_programmes_section_file_list) > 0){
					$sql .= " AND peec_programmes_section_file_id NOT IN (" . implode(',', array_fill(0, count($peec_programmes_section_file_list), '?')) . ") ";
					$parameters = array_merge($parameters, $peec_programmes_section_file_list);
				}

				$peec_programmes_section_files = DM::findAll("peec_programmes_section_file", $sql, " peec_programmes_section_file_sequence ", $parameters);

				foreach($peec_programmes_section_files as $peec_programmes_section_file){
					$values = array();
					$values['peec_programmes_section_file_id'] = $peec_programmes_section_file['peec_programmes_section_file_id'];
					$values['peec_programmes_section_file_status'] = STATUS_DISABLE;
					$values['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
					$values['peec_programmes_section_file_updated_date'] = $timestamp;

					DM::update('peec_programmes_section_file', $values);
					unset($values);
				}

				unset($peec_programmes_section_file_list);
			}
		}

		if($acknowledgement_section != "" && is_array($acknowledgement_section)){
			foreach($acknowledgement_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$peec_programmes_section_id = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}


				$peec_programmes_section_file_list = array();
				$lang1_files = $text_block['lang1_files'];
				if($lang1_files != "" && is_array($lang1_files)){
					$count = 1;
					foreach($lang1_files as $lang1_file){
						if($lang1_file['peec_programmes_section_file_id'] == "") {
							unset($lang1_file['peec_programmes_section_file_id']);
							$lang1_file['peec_programmes_section_file_psid'] = $peec_programmes_section_id;
							$lang1_file['peec_programmes_section_file_pid'] = $id;
							$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_filename'] = $lang1_file['filename'];
							$lang1_file['peec_programmes_section_file_status'] = STATUS_ENABLE;
							$lang1_file['peec_programmes_section_file_created_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_created_date'] = $timestamp;
							$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;
							echo "IN ".$peec_programmes_section_id."|".$lang1_file['filename']."<br><br>";
							$peec_programmes_section_file_id = DM::insert('peec_programmes_section_file', $lang1_file);
							$peec_programmes_section_file_list[] = $peec_programmes_section_file_id;
							if(!file_exists($peec_programmes_file_path . $id. "/".$peec_programmes_section_file_id."/")){
								mkdir($peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/", 0775, true);
							}

							if($lang1_file['peec_programmes_section_file_type'] == "Partner"){
								copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $peec_programmes_file_path . $id . "/".$peec_programmes_section_file_id."/" . $lang1_file['filename']);
							}
						} else {
							$peec_programmes_section_file_list[] = $lang1_file['peec_programmes_section_file_id'];
							$peec_programmes_section_file_id = $lang1_file['peec_programmes_section_file_id'];
							$lang1_file['peec_programmes_section_file_lang'] = 'lang1';
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_sequence'] = $count++;
							$lang1_file['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
							$lang1_file['peec_programmes_section_file_updated_date'] = $timestamp;

							DM::update('peec_programmes_section_file', $lang1_file);
							echo "UP ".$peec_programmes_section_id."|".$lang1_file['filename']."<br><br>";
						}
					}
				}
				$sql = " peec_programmes_section_file_status = ? AND peec_programmes_section_file_psid = ? ";

				$parameters = array(STATUS_ENABLE, $peec_programmes_section_id);

				if(count($peec_programmes_section_file_list) > 0){
					$sql .= " AND peec_programmes_section_file_id NOT IN (" . implode(',', array_fill(0, count($peec_programmes_section_file_list), '?')) . ") ";
					$parameters = array_merge($parameters, $peec_programmes_section_file_list);
				}

				$peec_programmes_section_files = DM::findAll("peec_programmes_section_file", $sql, " peec_programmes_section_file_sequence ", $parameters);

				foreach($peec_programmes_section_files as $peec_programmes_section_file){
					$values = array();
					$values['peec_programmes_section_file_id'] = $peec_programmes_section_file['peec_programmes_section_file_id'];
					$values['peec_programmes_section_file_status'] = STATUS_DISABLE;
					$values['peec_programmes_section_file_updated_by'] = $_SESSION['sess_id'];
					$values['peec_programmes_section_file_updated_date'] = $timestamp;

					DM::update('peec_programmes_section_file', $values);
					unset($values);
				}

				unset($peec_programmes_section_file_list);
			}
		}


		if($contact_section != "" && is_array($contact_section)){
			foreach($contact_section as $text_block){
				$text_block = json_decode($text_block, true);
				if($text_block['peec_programmes_section_id'] == "") {
					unset($text_block['peec_programmes_section_id']);
					$text_block['peec_programmes_section_pid'] = $id;
					$text_block['peec_programmes_section_status'] = STATUS_ENABLE;
					$text_block['peec_programmes_section_created_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_created_date'] = $timestamp;
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					$text_block['peec_programmes_section_content_lang1'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang1']);
					$text_block['peec_programmes_section_content_lang2'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang2']);
					$text_block['peec_programmes_section_content_lang3'] = str_replace($randomString,$id,$text_block['peec_programmes_section_content_lang3']);
					$peec_programmes_section_id = DM::insert('peec_programmes_section', $text_block);
					$peec_programmes_section_list[] = $peec_programmes_section_id;

				} else {
					$peec_programmes_section_list[] = $text_block['peec_programmes_section_id'];
					$text_block['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
					$text_block['peec_programmes_section_updated_date'] = $timestamp;
					DM::update('peec_programmes_section', $text_block);
				}
			}
		}

		$sql = " peec_programmes_section_status = ? AND peec_programmes_section_pid = ?";
		$parameters = array(STATUS_ENABLE,$id);
		if(count($peec_programmes_section_list) > 0){
			$sql .= " AND peec_programmes_section_id NOT IN (" . implode(',', array_fill(0, count($peec_programmes_section_list), '?')) . ") ";
			$parameters = array_merge($parameters, $peec_programmes_section_list);
		}
		$peec_programmes_sections = DM::findAll("peec_programmes_section", $sql, " peec_programmes_section_sequence ", $parameters);
		foreach($peec_programmes_sections as $peec_programmes_section){
			$values = array();
			$values['peec_programmes_section_id'] = $peec_programmes_section['peec_programmes_section_id'];
			$values['peec_programmes_section_status'] = STATUS_DISABLE;
			$values['peec_programmes_section_updated_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_section_updated_date'] = $timestamp;
			DM::update('peec_programmes_section', $values);
			unset($values);
		}
		unset($peec_programmes_sections);

		if(!file_exists($peec_programmes_path . $id. "/")){
			mkdir($peec_programmes_path . $id . "/", 0775, true);
		}

		$ObjRec = DM::load('peec_programmes', $id);
		if($_FILES['peec_programmes_banner']['tmp_name'] != ""){
				//$ObjRec = DM::load('peec_programmes', $id);
			if($ObjRec['peec_programmes_banner'] != "" && file_exists($peec_programmes_path . $id . "/" . $ObjRec['peec_programmes_banner'])){
				//	@unlink($peec_programmes_path  . $id . "/" . $ObjRec['peec_programmes_banner']);
			}
			if($ObjRec['peec_programmes_banner'] != "" && file_exists($peec_programmes_path . $id . "/crop_" . $ObjRec['peec_programmes_banner'])){
				//	@unlink($peec_programmes_path  . $id . "/crop_" . $ObjRec['peec_programmes_banner']);
			}
			$newname = moveFile($peec_programmes_path . $id . "/", $id, pathinfo($_FILES['peec_programmes_banner']['name'],PATHINFO_EXTENSION) , $_FILES['peec_programmes_banner']['tmp_name']);
			$thumb = $peec_programmes_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
			$crop = $peec_programmes_path. $id."/". "crop_".$newname;
			resizeImageByHeight($peec_programmes_path. $id."/". $newname, "100" , $thumb);
			cropImage($peec_programmes_path. $id."/". $newname, "420","260" , $crop);
			$values = array();
			$values['peec_programmes_banner'] = $newname;
			$values['peec_programmes_id'] = $id;
			DM::update('peec_programmes', $values);
			unset($values);
			$ObjRec = DM::load('peec_programmes', $id);
		}

		if($_FILES['peec_programmes_thumb_banner']['tmp_name'] != ""){
				//$ObjRec = DM::load('peec_programmes', $id);
			if($ObjRec['peec_programmes_thumb_banner'] != "" && file_exists($peec_programmes_path . $id . "/" . $ObjRec['peec_programmes_thumb_banner'])){
			//		@unlink($peec_programmes_path  . $id . "/" . $ObjRec['peec_programmes_thumb_banner']);
			}
			if($ObjRec['peec_programmes_thumb_banner'] != "" && file_exists($peec_programmes_path . $id . "/crop_" . $ObjRec['peec_programmes_thumb_banner'])){
			//		@unlink($peec_programmes_path  . $id . "/crop_" . $ObjRec['peec_programmes_thumb_banner']);
			}
			$newname = moveFile($peec_programmes_path . $id . "/", $id, pathinfo($_FILES['peec_programmes_thumb_banner']['name'],PATHINFO_EXTENSION) , $_FILES['peec_programmes_thumb_banner']['tmp_name']);
			$thumb = $peec_programmes_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
			$crop = $peec_programmes_path. $id."/". "crop_".$newname;
			resizeImageByHeight($peec_programmes_path. $id."/". $newname, "100" , $thumb);
			cropImage($peec_programmes_path. $id."/". $newname, "420","260" , $crop);
			$values = array();
			$values['peec_programmes_thumb_banner'] = $newname;
			$values['peec_programmes_id'] = $id;
			DM::update('peec_programmes', $values);
			unset($values);
			$ObjRec = DM::load('peec_programmes', $id);
		}
			 //NotificationService::sendNotification("peec_programmes", $id, $timestamp, $_SESSION['sess_id'], array("E"), "peec_programmes", $isNew ? "CS" : "ES");
		if($processType == ""){
				// Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['peec_programmes_id'] .") is updated by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
				// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec_programmes", $isNew ? "CS" : "ES");
		}

		else if($processType == "submit_for_approval"){
			ApprovalService::withdrawApproval($type_name,$type, $id, $timestamp);
								// Logger::log($_SESSION['sess_id'], $timestamp, "Master (ID:" . $ObjRec['peec_programmes_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
								// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "peec_programmes", $isNew ? "CS" : "ES");
			$files = array($SYSTEM_HOST."peec_programmes/peec_programmes.php?peec_programmes_id=".$id);
			$approval_id = ApprovalService::createApprovalRequest( $type_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				// NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "peec_programmes", "PA", $approval_id);
		}
		header("location: peec_programmes.php?peec_programmes_id=".$id."&msg=S-1001");
		return;
	}

	else if($processType == "delete" && isInteger($id)) {
		$timestamp = getCurrentTimestamp();
		$delectAct = $_REQUEST['delectAct'] ?? '';
		$ObjRec = DM::load('peec_programmes', $id);
		if($ObjRec == NULL || $ObjRec == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		if($ObjRec['mail_banner_status'] == STATUS_DISABLE)
		{
			header("location: list.php?msg=F-1004");
			return;
		}
		$values['peec_programmes_id'] = $id;
			//$values['peec_programmes_publish_status'] = $delectAct=='approval'?'RAA':'APL';
		$values['peec_programmes_publish_status'] = $isPublished ? 'RAA' : 'AL';
		$values['peec_programmes_status'] = STATUS_DISABLE;
		$values['peec_programmes_updated_by'] = $_SESSION['sess_id'];
		$values['peec_programmes_updated_date'] = getCurrentTimestamp();
		DM::update('peec_programmes', $values);
		unset($values);
		if($isPublished){
			ApprovalService::withdrawApproval($type_name, $type, $id, $timestamp);
								// Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes (ID:" . $ObjRec['peec_programmes_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
								// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec_programmes", "DS");
			$files = array($SYSTEM_HOST."peec_programmes/peec_programmes.php?peec_programmes_id=".$id);
			$approval_id = ApprovalService::createApprovalRequest("peec_programmes", $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				// NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "peec_programmes", "PA", $approval_id);
		}else{
				// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec_programmes", "D");
		}
						/*if($delectAct=='approval'){
			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";
						$parameters = array(STATUS_ENABLE, 'peec_programmes', $id, 'RCA', 'RAA', 'APL');
						$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);
						foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;
								DM::update('approval', $values);
				unset($values);
								$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));
								$recipient_list = array("E", "C");
								if($approval_request['approval_approval_status'] != "RAA"){
					$recipient_list[] = "A";
				}
								$message = "";
								switch($approval_request['approval_approval_status']){
					case "RCA":
						$message = "Request for checking";
					break;
					case "RAA":
						$message = "Request for approval";
					break;
					case "APL":
						$message = "Pending to launch";
					break;
				}
								$message .= " of main banner '" . $ObjRec['peec_programmes_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";
								//NotificationService::sendNotification("peec_programmes", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "peec_programmes", $message);
			}
						Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes (ID:" . $ObjRec['peec_programmes_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], array("E"), "ipo", "main Banner '" . $ObjRec['ipo_eng_name'] . "' is deleted and submitted for checking by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");
						$values = array();
						$files = array("/en/master/index.php", "/tc/master/index.php");
						foreach($files as $idx => $value){
				$files[$idx] = str_replace("//", "/", $value);
			}
						$values['approval_submitted_by'] = $_SESSION['sess_id'];
			$values['approval_item_action'] = 'D';
			$values['approval_permission_key'] = 'peec_programmes_scheme';
			$values['approval_item_type'] = 'peec_programmes_scheme';
			$values['approval_item_id'] = $id;
			$values['approval_launch_date'] = $_REQUEST['approval_launch_date'] ?? '';
			$values['approval_submitted_remarks'] = $_REQUEST['approval_remarks'] ?? '';
			$values['approval_submit_date'] = $timestamp;
			$values['approval_affected_url'] = json_encode($files);
			$values['approval_approval_status'] = "RAA";
			$values['approval_status'] = STATUS_ENABLE;
			$values['approval_created_by'] = $_SESSION['sess_id'];
			$values['approval_created_date'] = $timestamp;
			$values['approval_updated_by'] = $_SESSION['sess_id'];
			$values['approval_updated_date'] = $timestamp;
						$approval_id = DM::insert("approval", $values);
			unset($values);
						//NotificationService::sendCheckingRequest("ipo", $id, $timestamp, $_SESSION['sess_id'], array("C"), 'ipo', $approval_id, "main Banner '" . $ObjRec['ipo_eng_name'] . "' is pending for checking ");
		}*/
		header("location: list.php?msg=S-1002");
		return;
	}

	else if($processType == "undelete" && isInteger($id)) {
		$timestamp = getCurrentTimestamp();
		$ObjRec = DM::load('peec_programmes', $id);
		if($ObjRec == NULL || $ObjRec == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		if($ObjRec['peec_programmes_status'] == STATUS_ENABLE)
		{
			header("location: list.php?msg=F-1006");
			return;
		}
		$values['peec_programmes_id'] = $id;
		$values['peec_programmes_publish_status'] = 'D';
		$values['peec_programmes_status'] = STATUS_ENABLE;
		$values['peec_programmes_updated_by'] = $_SESSION['sess_id'];
		$values['peec_programmes_updated_date'] = getCurrentTimestamp();
		DM::update('peec_programmes', $values);
		unset($values);
		$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
		$sql .= " AND approval_approval_status in (?, ?, ?) ";
		$parameters = array(STATUS_ENABLE, 'peec_programmes', $id, 'RCA', 'RAA', 'APL');
		$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);
		foreach($approval_requests as $approval_request){
			$values = array();
			$values['approval_id'] = $approval_request['approval_id'];
			$values['approval_approval_status'] = 'W';
			$values['approval_updated_by'] = $_SESSION['sess_id'];
			$values['approval_updated_date'] = $timestamp;
			DM::update('approval', $values);
			unset($values);
			$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
			DM::query($sql, array('N', $approval_request['approval_id']));
			$recipient_list = array("E", "C");
			if($approval_request['approval_approval_status'] != "RCA"){
				$recipient_list[] = "A";
			}
			$message = "";
			switch($approval_request['approval_approval_status']){
				case "RCA":
				$message = "Request for checking";
				break;
				case "RAA":
				$message = "Request for approval";
				break;
				case "APL":
				$message = "Pending to launch";
				break;
			}
			$message .= " of main banner '" . $ObjRec['ipo_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";
								// NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'peec_programmes', "WA");
		}
						// Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes (ID:" . $ObjRec['peec_programmes_id'] .") is undeleted by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], array("E"), "ipo", "main Banner '" . $ObjRec['ipo_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");
		header("location: list.php?msg=S-1003");
		return;
	}

	if($processType == "updateSequence"){
		$peec_programmes_tab = $_REQUEST['ipo_id'] ?? '';
		if($peec_programmes_tab != "" && is_array($peec_programmes_tab)){
			$counter = 1;
			$timestamp = getCurrentTimestamp();
			foreach($peec_programmes_tab as $peec_programmes_id){
				$ObjRec = DM::load('peec_programmes', $peec_programmes_id);
				if($ObjRec == NULL || $ObjRec == "" || $ObjRec['peec_programmes_status'] != STATUS_ENABLE){
					continue;
				}
				$values = array();
				$values['peec_programmes_id'] = $peec_programmes_id;
				$values['peec_programmes_sequence'] = $counter;
				$values['peec_programmes_updated_by'] = $_SESSION['sess_id'];
				$values['peec_programmes_updated_date'] = $timestamp;
				DM::update('peec_programmes', $values);
				unset($values);
				$counter++;
			}
		}
		echo json_encode(array("result"=>"ok", "alert_type"=>"alert-success", "icon"=>"glyphicon glyphicon-ok", "message"=>$SYSTEM_MESSAGE['S-1001'] ));
		return;
	}
}
header("location:../dashboard.php?msg=F-1001");
return;
?>
