<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);



	if($type=="universities" )

	{

		$id = $_REQUEST['universities_id'] ?? '';
		$universities_lv1 = $_REQUEST['universities_lv1'] ?? '';
		$universities_lv2 = $_REQUEST['universities_lv2'] ?? '';

		$type_name = "universities";

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "top_up_degree")){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['universities_publish_status'] = "D";

				$values['universities_status'] = STATUS_ENABLE;

				$values['universities_created_by'] = $_SESSION['sess_id'];

				$values['universities_created_date'] = $timestamp;

				$values['universities_updated_by'] = $_SESSION['sess_id'];

				$values['universities_updated_date'] = $timestamp;


				$id = DM::insert('universities', $values);


				if(!file_exists($universities_path . $id. "/")){
					mkdir($universities_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('universities', $id);



				if($ObjRec['universities_status'] == STATUS_DISABLE)

				{

					header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=F-1002");

					return;

				}


				$values['universities_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['universities_updated_by'] = $_SESSION['sess_id'];

				$values['universities_updated_date'] = $timestamp;



				DM::update('universities', $values);

			}


			$ObjRec = DM::load('universities', $id);

			$universities_img_arr = array('universities_image','universities_inside_logo');

			foreach($universities_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($universities_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($universities_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($universities_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


						list($width, $height) = getimagesize($universities_path . $id . "/".$newname);

						if($val =='universities_image'){
							$newWidth ="306";
							$newHeight="165";
							cropImage($universities_path. $id."/". $newname, $newWidth,$newHeight , $universities_path. $id."/". $newname);
						}else{
							$newHeight="100";
							resizeImageByHeight($universities_path. $id."/". $newname, $newHeight , $universities_path. $id."/". $newname);
						}





					$values = array();
					$values[$val] = $newname;
					$values['universities_id'] = $id;

					DM::update('universities', $values);
					unset($values);

					$ObjRec = DM::load('universities', $id);
				}
			}


			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "universities '" . $ObjRec['universities_name_lang1'] . "' (ID:" . $ObjRec['universities_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "top_up_degree", $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval("top_up_degree",$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['universities_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "top_up_degree", $isNew ? "CS" : "ES");
				$files = array($host_name."en/universities/universities.php", $host_name."tc/universities/universities.php", $host_name."sc/universities/universities.php");

				$approval_id = ApprovalService::createApprovalRequest( "top_up_degree", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "top_up_degree", "PA", $approval_id);

			}

			header("location:universities.php?universities_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('universities', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['universities_status'] == STATUS_DISABLE)

			{

				header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("universities", $id);

			$values['universities_id'] = $id;
			$values['universities_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['universities_status'] = STATUS_DISABLE;
			$values['universities_updated_by'] = $_SESSION['sess_id'];
			$values['universities_updated_date'] = getCurrentTimestamp();


			DM::update('universities', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval($type_name, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "universities '" . $ObjRec['universities_name_lang1'] . "' (ID:" . $ObjRec['universities_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "top_up_degree", "DS");

				$files = array($host_name."en/universities/universities.php", $host_name."tc/universities/universities.php", $host_name."sc/universities/universities.php");

				$approval_id = ApprovalService::createApprovalRequest($type_name, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "top_up_degree", "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "top_up_degree", "D");
			}





			header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('universities', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['universities_status'] == STATUS_ENABLE || $ObjRec['universities_publish_status'] == 'AL')

			{

				header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=F-1006");

				return;

			}



			$values['universities_id'] = $id;

			$values['universities_publish_status'] = 'D';

			$values['universities_status'] = STATUS_ENABLE;

			$values['universities_updated_by'] = $_SESSION['sess_id'];

			$values['universities_updated_date'] = getCurrentTimestamp();



			DM::update('universities', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'universities', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of universities '" . $ObjRec['universities_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "top_up_degree", "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "universities '" . $ObjRec['universities_name_lang1'] . "' (ID:" . $ObjRec['universities_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "top_up_degree", "UD");



			header("location: list.php?universities_lv1=".$universities_lv1."&universities_lv2=".$universities_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
