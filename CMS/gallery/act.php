<?php



	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';
	$redirect_section = $_REQUEST['redirect_section'] ?? '';
	$section = $_REQUEST['section'] ?? '';
	$Permission_section = $_REQUEST['Permission_section'] ?? '';

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$type_name = $Permission_section;

	if($type=="product" )
	{
		$id = $_REQUEST['product_id'] ?? '';
		$randomString = $_REQUEST['randomString'] ?? '';

		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "exhibitons")){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}

		if($processType == "" || $processType == "submit_for_approval") {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('product', $id);

			if($ObjRec['product_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			$values = $_REQUEST;

			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			//unset($values['small_banner_name']);
			if($isNew){
				$values['product_publish_status'] = "D";
				$values['product_status'] = STATUS_ENABLE;
				$values['product_created_by'] = $_SESSION['sess_id'];
				$values['product_created_date'] = $timestamp;
				$values['product_updated_by'] = $_SESSION['sess_id'];
				$values['product_updated_date'] = $timestamp;

				$id = DM::insert('product', $values);

				if(!file_exists($product_path . $id. "/")){
					mkdir($product_path . $id . "/", 0775, true);
				}

				rename ($product_path.$randomString, $product_path.$id);


			} else {
				$ObjRec = DM::load('product', $id);

				if($ObjRec['product_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}

			$values['product_publish_status'] = $processType == "submit_for_approval" ? "RAA" : 'D';
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = $timestamp;

				DM::update('product', $values);
			}
			$page_section_list = array();

			$text_block_section = $_REQUEST['text_block_section'] ?? '';
			$news_section = $_REQUEST['news_section'] ?? '';
			$project_section = $_REQUEST['project_section'] ?? '';
			$photo_section = $_REQUEST['photo_section'] ?? '';
			$contact_section = $_REQUEST['contact_section'] ?? '';
			$acknowledgement_section = $_REQUEST['acknowledgement_section'] ?? '';
			$visit_section = $_REQUEST['visit_section'] ?? '';

			if($visit_section != "" && is_array($visit_section)){
				foreach($visit_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}

			if($text_block_section != "" && is_array($text_block_section)){
				foreach($text_block_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}

			if($news_section != "" && is_array($news_section)){
				foreach($news_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;

					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}

			if($project_section != "" && is_array($project_section)){
				foreach($project_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}


			if($photo_section != "" && is_array($photo_section)){
				foreach($photo_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$product_section_id = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}


			if($acknowledgement_section != "" && is_array($acknowledgement_section)){
				foreach($acknowledgement_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$product_section_id = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}

			$product_section_file_list = array();

			$lang1_files = $text_block['lang1_files'] ?? '';

			if($lang1_files != "" && is_array($lang1_files)){

				$count = 1;

				foreach($lang1_files as $lang1_file){

					if($lang1_file['product_section_file_id'] == "") {

						unset($lang1_file['product_section_file_id']);

						$lang1_file['product_section_file_psid'] = $product_section_id;

						$lang1_file['product_section_file_pid'] = $id;

						$lang1_file['product_section_file_lang'] = 'lang1';

						$lang1_file['product_section_file_sequence'] = $count++;

						$lang1_file['product_section_file_filename'] = $lang1_file['filename'];

						$lang1_file['product_section_file_status'] = STATUS_ENABLE;

						$lang1_file['product_section_file_created_by'] = $_SESSION['sess_id'];

						$lang1_file['product_section_file_created_date'] = $timestamp;

						$lang1_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

						$lang1_file['product_section_file_updated_date'] = $timestamp;



						$product_section_file_id = DM::insert('product_section_file', $lang1_file);

						$product_section_file_list[] = $product_section_file_id;

						if(!file_exists($product_file_path . $id. "/".$product_section_file_id."/")){
							mkdir($product_file_path . $id . "/".$product_section_file_id."/", 0775, true);
						}


						if($lang1_file['product_section_file_type'] == "Partner"){

							copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $product_file_path . $id . "/".$product_section_file_id."/" . $lang1_file['filename']);
						}

					} else {

						$product_section_file_list[] = $lang1_file['product_section_file_id'];

						$product_section_file_id = $lang1_file['product_section_file_id'];

						$lang1_file['product_section_file_lang'] = 'lang1';

						$lang1_file['product_section_file_sequence'] = $count++;

						$lang1_file['product_section_file_sequence'] = $count++;

						$lang1_file['product_section_file_updated_by'] = $_SESSION['sess_id'];

						$lang1_file['product_section_file_updated_date'] = $timestamp;



						DM::update('product_section_file', $lang1_file);

					}

				}

			}



			if($contact_section != "" && is_array($contact_section)){
				foreach($contact_section as $text_block){
					$text_block = json_decode($text_block, true);

					if($text_block['product_section_id'] == "") {

						unset($text_block['product_section_id']);

						$text_block['product_section_pid'] = $id;

						$text_block['product_section_status'] = STATUS_ENABLE;

						$text_block['product_section_created_by'] = $_SESSION['sess_id'];

						$text_block['product_section_created_date'] = $timestamp;

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						$text_block['product_section_content_lang1'] = str_replace($randomString,$id,$text_block['product_section_content_lang1']);
						$text_block['product_section_content_lang2'] = str_replace($randomString,$id,$text_block['product_section_content_lang2']);
						$text_block['product_section_content_lang3'] = str_replace($randomString,$id,$text_block['product_section_content_lang3']);

						$product_section_id = DM::insert('product_section', $text_block);

						$product_section_list[] = $product_section_id;



					} else {
						$product_section_list[] = $text_block['product_section_id'];

						$text_block['product_section_updated_by'] = $_SESSION['sess_id'];

						$text_block['product_section_updated_date'] = $timestamp;

						DM::update('product_section', $text_block);
					}
				}
			}


			$sql = " product_section_status = ? AND product_section_pid = ?";
			$parameters = array(STATUS_ENABLE,$id);

			if(count($product_section_list) > 0){
				$sql .= " AND product_section_id NOT IN (" . implode(',', array_fill(0, count($product_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $product_section_list);
			}

			$product_sections = DM::findAll("product_section", $sql, " product_section_sequence ", $parameters);

			foreach($product_sections as $product_section){
				$values = array();
				$values['product_section_id'] = $product_section['product_section_id'];
				$values['product_section_status'] = STATUS_DISABLE;
				$values['product_section_updated_by'] = $_SESSION['sess_id'];
				$values['product_section_updated_date'] = $timestamp;

				DM::update('product_section', $values);
				unset($values);
			}

			unset($product_sections);


			if(!file_exists($product_path . $id. "/")){
				mkdir($product_path . $id . "/", 0775, true);
			}


			$ObjRec = DM::load('product', $id);

			if($_FILES['product_banner']['tmp_name'] != ""){
				//$ObjRec = DM::load('product', $id);

				if($ObjRec['product_banner'] != "" && file_exists($product_path . $id . "/" . $ObjRec['product_banner'])){
				//	@unlink($product_path  . $id . "/" . $ObjRec['product_banner']);
				}
				if($ObjRec['product_banner'] != "" && file_exists($product_path . $id . "/crop_" . $ObjRec['product_banner'])){
				//	@unlink($product_path  . $id . "/crop_" . $ObjRec['product_banner']);
				}

				$newname = moveFile($product_path . $id . "/", $id, pathinfo($_FILES['product_banner']['name'],PATHINFO_EXTENSION) , $_FILES['product_banner']['tmp_name']);

				$thumb = $product_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
				$crop = $product_path. $id."/". "crop_".$newname;
				resizeImageByHeight($product_path. $id."/". $newname, "100" , $thumb);
				cropImage($product_path. $id."/". $newname, "420","260" , $crop);

				$values = array();
				$values['product_banner'] = $newname;
				$values['product_id'] = $id;

				DM::update('product', $values);
				unset($values);

				$ObjRec = DM::load('product', $id);
			}


			if($_FILES['product_thumb_banner']['tmp_name'] != ""){
				//$ObjRec = DM::load('product', $id);

				if($ObjRec['product_thumb_banner'] != "" && file_exists($product_path . $id . "/" . $ObjRec['product_thumb_banner'])){
				//	@unlink($product_path  . $id . "/" . $ObjRec['product_thumb_banner']);
				}
				if($ObjRec['product_thumb_banner'] != "" && file_exists($product_path . $id . "/crop_" . $ObjRec['product_thumb_banner'])){
				//	@unlink($product_path  . $id . "/crop_" . $ObjRec['product_thumb_banner']);
				}

				$newname = moveFile($product_path . $id . "/", $id, pathinfo($_FILES['product_thumb_banner']['name'],PATHINFO_EXTENSION) , $_FILES['product_thumb_banner']['tmp_name']);

				$thumb = $product_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
				$crop = $product_path. $id."/". "crop_".$newname;
				resizeImageByHeight($product_path. $id."/". $newname, "100" , $thumb);
				cropImage($product_path. $id."/". $newname, "420","260" , $crop);

				$values = array();
				$values['product_thumb_banner'] = $newname;
				$values['product_id'] = $id;

				DM::update('product', $values);
				unset($values);

				$ObjRec = DM::load('product', $id);
			}
			// NotificationService::sendNotification("master", $id, $timestamp, $_SESSION['sess_id'], array("E"), "master", $isNew ? "CS" : "ES");

			if($processType == ""){
				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['product_id'] .") is updated by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "exhibitons", $isNew ? "CS" : "ES");
			}


			else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval('exhibitons',$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Master (ID:" . $ObjRec['product_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "exhibitons", $isNew ? "CS" : "ES");

				$files = array($SYSTEM_HOST."gallery/".$redirect_section);

				$approval_id = ApprovalService::createApprovalRequest( $type_name, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "exhibitons", "PA", $approval_id);

			}

			header("location: ".$redirect_section."?msg=S-1001");
			return;
		}


		else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$delectAct = $_REQUEST['delectAct'] ?? '';

			$ObjRec = DM::load('product', $id);

			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			if($ObjRec['mail_banner_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}

			$values['product_id'] = $id;
			$values['product_publish_status'] = $delectAct=='approval'?'RAA':'APL';
			$values['product_status'] = STATUS_DISABLE;
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = getCurrentTimestamp();

			DM::update('product', $values);
			unset($values);

			if($delectAct=='approval'){
			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			$parameters = array(STATUS_ENABLE, 'product', $id, 'RCA', 'RAA', 'APL');

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;

				DM::update('approval', $values);
				unset($values);

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));

				$recipient_list = array("E", "C");

				if($approval_request['approval_approval_status'] != "RAA"){
					$recipient_list[] = "A";
				}

				$message = "";

				switch($approval_request['approval_approval_status']){
					case "RCA":
						$message = "Request for checking";
					break;
					case "RAA":
						$message = "Request for approval";
					break;
					case "APL":
						$message = "Pending to launch";
					break;
				}

				$message .= " of main banner '" . $ObjRec['product_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				//NotificationService::sendNotification("product", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "product", $message);
			}

			Logger::log($_SESSION['sess_id'], $timestamp, "Product (ID:" . $ObjRec['product_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], array("E"), "ipo", "main Banner '" . $ObjRec['ipo_eng_name'] . "' is deleted and submitted for checking by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");

			$values = array();

			$files = array($SYSTEM_HOST."gallery/".$redirect_section);

			foreach($files as $idx => $value){
				$files[$idx] = str_replace("//", "/", $value);
			}

			$values['approval_submitted_by'] = $_SESSION['sess_id'];
			$values['approval_item_action'] = 'D';
			$values['approval_permission_key'] = 'product_scheme';
			$values['approval_item_type'] = 'product_scheme';
			$values['approval_item_id'] = $id;
			$values['approval_launch_date'] = $_REQUEST['approval_launch_date'] ?? '';
			$values['approval_submitted_remarks'] = $_REQUEST['approval_remarks'] ?? '';
			$values['approval_submit_date'] = $timestamp;
			$values['approval_affected_url'] = json_encode($files);
			$values['approval_approval_status'] = "RAA";
			$values['approval_status'] = STATUS_ENABLE;
			$values['approval_created_by'] = $_SESSION['sess_id'];
			$values['approval_created_date'] = $timestamp;
			$values['approval_updated_by'] = $_SESSION['sess_id'];
			$values['approval_updated_date'] = $timestamp;

			$approval_id = DM::insert("approval", $values);
			unset($values);

			//NotificationService::sendCheckingRequest("ipo", $id, $timestamp, $_SESSION['sess_id'], array("C"), 'ipo', $approval_id, "main Banner '" . $ObjRec['ipo_eng_name'] . "' is pending for checking ");

		}
			header("location: list.php?msg=S-1002");
			return;
		}



		else if($processType == "undelete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();

			$ObjRec = DM::load('product', $id);

			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			if($ObjRec['product_status'] == STATUS_ENABLE)
			{
				header("location: list.php?msg=F-1006");
				return;
			}

			$values['product_id'] = $id;
			$values['product_publish_status'] = 'D';
			$values['product_status'] = STATUS_ENABLE;
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = getCurrentTimestamp();

			DM::update('product', $values);
			unset($values);

			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";

			$parameters = array(STATUS_ENABLE, 'product', $id, 'RCA', 'RAA', 'APL');

			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

			foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;

				DM::update('approval', $values);
				unset($values);

				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));

				$recipient_list = array("E", "C");

				if($approval_request['approval_approval_status'] != "RCA"){
					$recipient_list[] = "A";
				}

				$message = "";

				switch($approval_request['approval_approval_status']){
					case "RCA":
						$message = "Request for checking";
					break;
					case "RAA":
						$message = "Request for approval";
					break;
					case "APL":
						$message = "Pending to launch";
					break;
				}

				$message .= " of main banner '" . $ObjRec['ipo_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

				//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "ipo", $message);
			}

			Logger::log($_SESSION['sess_id'], $timestamp, "Product (ID:" . $ObjRec['product_id'] .") is undeleted by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], array("E"), "ipo", "main Banner '" . $ObjRec['ipo_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");

			header("location: list.php?msg=S-1003");
			return;
		}


		if($processType == "updateSequence"){
			$product_tab = $_REQUEST['ipo_id'] ?? '';

			if($product_tab != "" && is_array($product_tab)){
				$counter = 1;
				$timestamp = getCurrentTimestamp();

				foreach($product_tab as $product_id){
					$ObjRec = DM::load('product', $product_id);

					if($ObjRec == NULL || $ObjRec == "" || $ObjRec['product_status'] != STATUS_ENABLE){
						continue;
					}

					$values = array();
					$values['product_id'] = $product_id;
					$values['product_sequence'] = $counter;
					$values['product_updated_by'] = $_SESSION['sess_id'];
					$values['product_updated_date'] = $timestamp;

					DM::update('product', $values);
					unset($values);
					$counter++;
				}
			}

			echo json_encode(array("result"=>"ok", "alert_type"=>"alert-success", "icon"=>"glyphicon glyphicon-ok", "message"=>$SYSTEM_MESSAGE['S-1001'] ));
			return;
		}

	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>
