<?php

	include_once "../include/config.php";

	include_once $SYSTEM_BASE . "include/function.php";

	include_once $SYSTEM_BASE . "session.php";

	include_once $SYSTEM_BASE . "include/system_message.php";

	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";

	include_once $SYSTEM_BASE . "include/classes/Logger.php";

	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";

	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";

	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);



	$type = $_REQUEST['type'] ?? '';

	$processType = $_REQUEST['processType'] ?? '';



	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);


	if($type=="programmes" )

	{

		$id = $_REQUEST['programmes_id'] ?? '';
		$programmes_lv1 = $_REQUEST['programmes_lv1'] ?? '';
		$programmes_lv2 = $_REQUEST['programmes_lv2'] ?? '';

		$type_name = DM::findOne("department", " department_id = ?", " department_id ", array($programmes_lv2));

		$type_name = DM::findOne("department", " department_id = ?", " department_id ", array($programmes_lv2));

		$type_namelv2 = $type_name['department_name_lang1'];

		$type_name = DM::findOne("department", " department_id = ?", " department_id ", array($programmes_lv1));

		$type_namelv1 = $type_name['department_name_lang1'];

		$type_name = "Programmes";

		if($type_namelv1){ $type_name .= " - ".$type_namelv1;}
		if($type_namelv2){ $type_name .= " - ".$type_namelv2;}

		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){

			if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "programmes_" . $programmes_lv1."_".$programmes_lv2)){

				header("location: ../dashboard.php?msg=F-1000");

				return;

			}

		}



		if($processType == "" || $processType == "submit_for_approval") {

			$values = $_REQUEST;



			$isNew = !isInteger($id) || $id == 0;

			$timestamp = getCurrentTimestamp();


			if($isNew){

				$values['programmes_publish_status'] = "D";

				$values['programmes_status'] = STATUS_ENABLE;

				$values['programmes_created_by'] = $_SESSION['sess_id'];

				$values['programmes_created_date'] = $timestamp;

				$values['programmes_updated_by'] = $_SESSION['sess_id'];

				$values['programmes_updated_date'] = $timestamp;

				$values['programmes_type'] = "1";



				$id = DM::insert('programmes', $values);


				if(!file_exists($programmes_path . $id. "/")){
					mkdir($programmes_path . $id . "/", 0775, true);
				}


			} else {

				$ObjRec = DM::load('programmes', $id);



				if($ObjRec['programmes_status'] == STATUS_DISABLE)

				{

					header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1002");

					return;

				}


				$values['programmes_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';

				$values['programmes_updated_by'] = $_SESSION['sess_id'];

				$values['programmes_updated_date'] = $timestamp;



				DM::update('programmes', $values);

			}


			$ObjRec = DM::load('programmes', $id);

			$programmes_img_arr = array('programmes_banner');

			foreach($programmes_img_arr as $key=>$val){

				if($_FILES[$val]['tmp_name'] != ""){

					if($ObjRec[$val] != "" && file_exists($programmes_path . $id . "/" . $ObjRec[$val])){
						//@unlink($programmes_path  . $id . "/" . $ObjRec[$val]);
					}

					$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);

					$newname = moveFile($programmes_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);


					list($width, $height) = getimagesize($programmes_path . $id . "/".$newname);

					$newWidth = '1500';
					$newHeight = '500';

					if($width > $newWidth){
						resizeImageByHeight($programmes_path. $id."/". $newname, $newWidth , $programmes_path. $id."/". $newname);
					}

					if($height > $newHeight){
						resizeImageByWidth($programmes_path. $id."/". $newname, $newHeight , $programmes_path. $id."/". $newname);
					}

					resizeAndBg($programmes_path. $id."/". $newname,$programmes_path. $id."/". $newname,$newWidth,$newHeight);





					$values = array();
					$values[$val] = $newname;
					$values['programmes_id'] = $id;

					DM::update('programmes', $values);
					unset($values);

					$ObjRec = DM::load('programmes', $id);
				}
			}


			//*************add image**************//
			$files = $_REQUEST['file_section'] ?? '';
			$file_section_list = array();
			if($files != "" && is_array($files)){
				foreach($files as $file){
					$file = json_decode($file, true);

					$lang1_files = $file['lang1_files'];
					if($lang1_files != "" && is_array($lang1_files)){

						$count = 1;

						foreach($lang1_files as $lang1_file){
							if($lang1_file['programmes_image_id'] == "") {

								$lang1_file['programmes_image_pid'] = $id;
								$lang1_file['programmes_image_seq'] = $count++;
								$lang1_file['programmes_image_filename'] = $lang1_file['filename'];
								$lang1_file['programmes_image_alt'] = $lang1_file['programmes_image_alt'];
								$lang1_file['programmes_image_status'] = STATUS_ENABLE;
								$lang1_file['programmes_image_created_by'] = $_SESSION['sess_id'];
								$lang1_file['programmes_image_created_date'] = $timestamp;
								$lang1_file['programmes_image_updated_by'] = $_SESSION['sess_id'];
								$lang1_file['programmes_image_updated_date'] = $timestamp;

								$file_section_id = DM::insert('programmes_image', $lang1_file);

								$file_section_list[] = $file_section_id;

								if(!file_exists($programmes_path . $id. "/".$file_section_id."/")){
									mkdir($programmes_path . $id . "/".$file_section_id."/", 0775, true);
								}

									copy($SYSTEM_BASE . "uploaded_files/tmp/" . $lang1_file['tmp_filename'], $programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);


									list($width, $height) = getimagesize($programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);

									$newWidth = '1000';
									$newHeight = '1000';

									if($width > $newWidth){
										resizeImageByHeight($programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], $newWidth , $programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}

									if($height > $newHeight){
										resizeImageByWidth($programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], $newHeight , $programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename']);
									}

									resizeAndBg($programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename'],$programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename'],$newWidth,$newHeight);


									$crop = $programmes_path . $id . "/".$file_section_id."/". "crop_".$lang1_file['filename'];
									cropImage($programmes_path . $id . "/".$file_section_id."/" . $lang1_file['filename'], "125","100" , $crop);

							}else{
								$file_section_list[] = $lang1_file['programmes_image_id'];
								$file_section_id = $lang1_file['file_section_id'] ?? '';
								$lang1_file['programmes_image_seq'] = $count++;
								$lang1_file['programmes_image_update_by'] = $_SESSION['sess_id'];
								$lang1_file['programmes_image_update_date'] = $timestamp;

								DM::update('programmes_image', $lang1_file);
							}
						}
					}
				}
			}

			$sql = " programmes_image_status = ? AND programmes_image_pid = ? ";
			$parameters = array(STATUS_ENABLE, $id);

			if(count($file_section_list) > 0){ print_r($file_section_list);
				$sql .= " AND programmes_image_id NOT IN (" . implode(',', array_fill(0, count($file_section_list), '?')) . ") ";
				$parameters = array_merge($parameters, $file_section_list);

			}

			$programmes_section_files = DM::findAll("programmes_image", $sql, " programmes_image_seq ", $parameters);

			foreach($programmes_section_files as $programmes_section_file){

				$values = array();
				$values['programmes_image_id'] = $programmes_section_file['programmes_image_id'];
				$values['programmes_image_status'] = STATUS_DISABLE;
				$values['programmes_image_update_by'] = $_SESSION['sess_id'];
				$values['programmes_image_update_date'] = $timestamp;

				DM::update('programmes_image', $values);

				unset($values);
			}

			//*************add image**************//



			if($processType == ""){

				Logger::log($_SESSION['sess_id'], $timestamp, "programmes '" . $ObjRec['programmes_name_lang1'] . "' (ID:" . $ObjRec['programmes_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, $isNew ? "CS" : "ES");

			} else if($processType == "submit_for_approval"){

				ApprovalService::withdrawApproval( "programmes_" . $programmes_lv1."_".$programmes_lv2,$type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['programmes_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, $isNew ? "CS" : "ES");
				$files = array($SYSTEM_HOST."programmes/programmes.php?programmes_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest( "programmes_" . $programmes_lv1."_".$programmes_lv2, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "PA", $approval_id);

			}

			header("location:programmes.php?programmes_id=" . $id . "&msg=S-1001");
			return;
		}


			else if($processType == "delete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('programmes', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['programmes_status'] == STATUS_DISABLE)

			{

				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1004");

				return;

			}

			$isPublished = SystemUtility::isPublishedRecord("programmes", $id);

			$values['programmes_id'] = $id;
			$values['programmes_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['programmes_status'] = STATUS_DISABLE;
			$values['programmes_updated_by'] = $_SESSION['sess_id'];
			$values['programmes_updated_date'] = getCurrentTimestamp();


			DM::update('programmes', $values);

			unset($values);

			if($isPublished){
				ApprovalService::withdrawApproval("programmes_" . $programmes_lv1."_".$programmes_lv2, $type, $id, $timestamp);

				Logger::log($_SESSION['sess_id'], $timestamp, "Programmes '" . $ObjRec['programmes_name_lang1'] . "' (ID:" . $ObjRec['programmes_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "DS");

				$files = array($SYSTEM_HOST."programmes/programmes.php?programmes_id=".$id);

				$approval_id = ApprovalService::createApprovalRequest("programmes_" . $programmes_lv1."_".$programmes_lv2, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
				NotificationService::sendApprovalRequest("Programmes - Higher Diploma - ".$type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "PA", $approval_id);
			}else{
				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "programmes_" . $programmes_lv1."_".$programmes_lv2, "D");
			}





			header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=S-1002");

			return;

		} else if($processType == "undelete" && isInteger($id)) {

			$timestamp = getCurrentTimestamp();



			$ObjRec = DM::load('programmes', $id);



			if($ObjRec == NULL || $ObjRec == "")

			{

				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1002");

				return;

			}



			if($ObjRec['programmes_status'] == STATUS_ENABLE || $ObjRec['programmes_publish_status'] == 'AL')

			{

				header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=F-1006");

				return;

			}



			$values['programmes_id'] = $id;

			$values['programmes_publish_status'] = 'D';

			$values['programmes_status'] = STATUS_ENABLE;

			$values['programmes_updated_by'] = $_SESSION['sess_id'];

			$values['programmes_updated_date'] = getCurrentTimestamp();



			DM::update('programmes', $values);

			unset($values);




			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";

			$sql .= " AND approval_approval_status in (?, ?, ?) ";



			$parameters = array(STATUS_ENABLE, 'programmes', $id, 'RCA', 'RAA', 'APL');



			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);



			foreach($approval_requests as $approval_request){

				$values = array();

				$values['approval_id'] = $approval_request['approval_id'];

				$values['approval_approval_status'] = 'W';

				$values['approval_updated_by'] = $_SESSION['sess_id'];

				$values['approval_updated_date'] = $timestamp;



				DM::update('approval', $values);

				unset($values);



				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";

				DM::query($sql, array('N', $approval_request['approval_id']));



				$recipient_list = array("E", "C");



				if($approval_request['approval_approval_status'] != "RCA"){

					$recipient_list[] = "A";

				}



				$message = "";



				switch($approval_request['approval_approval_status']){

					case "RCA":

						$message = "Request for checking";

					break;

					case "RAA":

						$message = "Request for approval";

					break;

					case "APL":

						$message = "Pending to launch";

					break;

				}



				$message .= " of programmes '" . $ObjRec['programmes_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";



				NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, 'programmes_' . $programmes_lv1."_".$programmes_lv2, "WA");

			}



			Logger::log($_SESSION['sess_id'], $timestamp, "Programmes '" . $ObjRec['programmes_name_lang1'] . "' (ID:" . $ObjRec['programmes_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), 'programmes_' . $programmes_lv1."_".$programmes_lv2, "UD");

			header("location: list.php?programmes_lv1=".$programmes_lv1."&programmes_lv2=".$programmes_lv2."&msg=S-1003");

			return;

		}

	}



	header("location:../dashboard.php?msg=F-1001");

	return;

?>
