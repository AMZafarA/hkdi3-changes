<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/Logger.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
	include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";

	include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
	$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
	RegenerateSitemap::regen($dbcon);

	$type = $_REQUEST['type'] ?? '';
	$processType = $_REQUEST['processType'] ?? '';
	
	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	if($type=="product" )
	{
		$id = $_REQUEST['product_id'] ?? '';
		
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'scheme_' . $id)){
				header("location: ../dashboard.php?msg=F-1000");
				return;
			}

		if($processType == "" || $processType == "submit_for_approval") {			
			$timestamp = getCurrentTimestamp();
			
			$ObjRec = DM::load('product', $id);
			
			if($ObjRec['product_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1002");
				return;
			}

			$values = $_REQUEST;
			
			$isNew = !isInteger($id) || $id == 0;
			$timestamp = getCurrentTimestamp();

			//unset($values['small_banner_name']);
			if($isNew){
				$values['product_publish_status'] = "D";
				$values['product_status'] = STATUS_ENABLE;
				$values['product_created_by'] = $_SESSION['sess_id'];
				$values['product_created_date'] = $timestamp;
				$values['product_updated_by'] = $_SESSION['sess_id'];
				$values['product_updated_date'] = $timestamp;

				$id = DM::insert('product', $values);
				
				$permission['ugp_permission_key'] = "product_".$id;
				$permission['ugp_user_group_id'] = "1";
				$permission['ugp_role'] = "A";
				DM::insert('user_group_permission', $permission);
				$permission['ugp_permission_key'] = "product_".$id;
				$permission['ugp_user_group_id'] = "1";
				$permission['ugp_role'] = "E";
				DM::insert('user_group_permission', $permission);
				$permission['ugp_permission_key'] = "scheme_".$id;
				$permission['ugp_user_group_id'] = "1";
				$permission['ugp_role'] = "A";
				DM::insert('user_group_permission', $permission);
				$permission['ugp_permission_key'] = "scheme_".$id;
				$permission['ugp_user_group_id'] = "1";
				$permission['ugp_role'] = "E";
				DM::insert('user_group_permission', $permission);
			} else {
				$ObjRec = DM::load('product', $id);
				
				if($ObjRec['product_status'] == STATUS_DISABLE)
				{
					header("location: list.php?msg=F-1002");
					return;
				}

			$values['product_publish_status'] = $processType == "submit_for_approval" ? "RAA" : 'D';
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = $timestamp;

				DM::update('product', $values);
			}
			$page_section_list = array();

			$text_block_section = $_REQUEST['text_block_section'] ?? '';
			
			if($text_block_section != "" && is_array($text_block_section)){
				foreach($text_block_section as $text_block){
					$text_block = json_decode($text_block, true);
					
					if($text_block['product_tab_id'] == "") {
						unset($text_block['product_tab_id']);
						$text_block['product_tab_pid'] = $id;
						$text_block['product_tab_status'] = STATUS_ENABLE;
						$text_block['product_tab_created_by'] = $_SESSION['sess_id'];
						$text_block['product_tab_created_date'] = $timestamp;
						$text_block['product_tab_updated_by'] = $_SESSION['sess_id'];
						$text_block['product_tab_updated_date'] = $timestamp;
						
						$page_section_id = DM::insert('product_tab', $text_block);
						$product_tab_list[] = $product_tab_id;
					} else {
						$product_tab_list[] = $text_block['product_tab_id'];
						$text_block['product_tab_updated_by'] = $_SESSION['sess_id'];
						$text_block['product_tab_updated_date'] = $timestamp;
						
						DM::update('product_tab', $text_block);
					}
				}
			}
			
			
			$sql = " product_tab_status = ? AND product_tab_pid = ?"; 
			$parameters = array(STATUS_ENABLE,$id);
			
			if(count($product_tab_list) > 0){
				$sql .= " AND product_tab_id NOT IN (" . implode(',', array_fill(0, count($product_tab_list), '?')) . ") ";
				$parameters = array_merge($parameters, $product_tab_list);
			}

			$product_tabs = DM::findAll("product_tab", $sql, " product_tab_sequence ", $parameters);
			
			foreach($product_tabs as $product_tab){
				$values = array();
				$values['product_tab_id'] = $product_tab['product_tab_id'];
				$values['product_tab_status'] = STATUS_DISABLE;
				$values['product_tab_updated_by'] = $_SESSION['sess_id'];
				$values['product_tab_updated_date'] = $timestamp;
				
				DM::update('product_tab', $values);
				unset($values);
			}

			unset($product_tabs);
			

			if(!file_exists($product_path . $id. "/")){
				mkdir($product_path . $id . "/", 0775, true);
			}
			
			
			$ObjRec = DM::load('product', $id);
			
			if($_FILES['product_banner']['tmp_name'] != ""){
				//$ObjRec = DM::load('product', $id);
				
				if($ObjRec['product_banner'] != "" && file_exists($product_path . $id . "/" . $ObjRec['product_banner'])){
					@unlink($product_path  . $id . "/" . $ObjRec['product_banner']);
				}
				
				if($ObjRec['product_banner'] != "" && file_exists($product_path . $id . "/crop_" . $ObjRec['product_banner'])){
					@unlink($product_path  . $id . "/crop_" . $ObjRec['product_banner']);
				}
				
				$newname = moveFile($product_path . $id . "/", $id, pathinfo($_FILES['product_banner']['name'],PATHINFO_EXTENSION) , $_FILES['product_banner']['tmp_name']);
				

				$thumb = $product_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
				$crop = $product_path. $id."/". "crop_".$newname;
				resizeImageByHeight($product_path. $id."/". $newname, "100" , $thumb);
				cropImage($product_path. $id."/". $newname, "420","260" , $crop);

				$values = array();
				$values['product_banner'] = $newname;
				$values['product_id'] = $id;

				
				DM::update('product', $values);
				unset($values);
				
				$ObjRec = DM::load('product', $id);
			}
			
			
			
			if($_FILES['product_thumb_banner']['tmp_name'] != ""){
				if($ObjRec['product_thumb_banner'] != "" && file_exists($product_path . $id . "/" . $ObjRec['product_thumb_banner'])){
						@unlink($product_path  . $id . "/" . $ObjRec['product_thumb_banner']);
					}
					if($ObjRec['product_thumb_banner'] != "" && file_exists($product_path . $id . "/crop_" . $ObjRec['product_thumb_banner'])){
						@unlink($product_path  . $id . "/crop_" . $ObjRec['product_thumb_banner']);
					}
					
					$newname = moveFile($product_path . $id . "/", $id, pathinfo($_FILES['product_thumb_banner']['name'],PATHINFO_EXTENSION) , $_FILES['product_thumb_banner']['tmp_name']);
					
					$thumb = $product_path. $id."/". "thumb.".pathinfo($newname,PATHINFO_EXTENSION);
					$crop = $product_path. $id."/". "crop_".$newname;
					resizeImageByHeight($product_path. $id."/". $newname, "100" , $thumb);
					cropImage($product_path. $id."/". $newname, "420","260" , $crop);
	
					$values = array();
					$values['product_thumb_banner'] = $newname;
					$values['product_id'] = $id;
					
					DM::update('product', $values);
					unset($values);
			}
			 
NotificationService::sendNotification("scheme", $id, $timestamp, $_SESSION['sess_id'], array("E"), "scheme_".$id, $isNew ? "CS" : "ES");
			if($processType == ""){
				Logger::log($_SESSION['sess_id'], $timestamp, "Product Scheme (ID:" . $ObjRec['product_id'] .") is updated by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
				//NotificationService::sendNotification("main_banner", $id, $timestamp, $_SESSION['sess_id'], array("E"), "main_banner", "Main Banner '" . $ObjRec['main_banner_name'] . "' is updated by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");
			} 
			
			
			else if($processType == "submit_for_approval"){ 
				
				ApprovalService::withdrawApproval( "scheme_".$id,$type, $id, $timestamp);
			
				Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['product_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				//NotificationService::sendNotification("scheme", $id, $timestamp, $_SESSION['sess_id'], array("E"), "scheme_".$id, $isNew ? "CS" : "ES");
				$files = array($host_name."web/en/investment/index.php", $host_name."web/tc/investment/index.php", $host_name."web/sc/investment/index.php",$host_name."web/en/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/tc/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/sc/investment/product.php?product=".$ObjRec['product_name_lang1']);
				
				$approval_id = ApprovalService::createApprovalRequest( "scheme_".$id, $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);				
				NotificationService::sendApprovalRequest("scheme", $id, $timestamp, $_SESSION['sess_id'], array("A"), "scheme_".$id, "PA", $approval_id);
				
			}
			
			header("location:product.php?product_id=" . $id . "&msg=S-1001");
			return;
		}
		
		
		else if($processType == "delete" && isInteger($id)) {
			$timestamp = getCurrentTimestamp();
			$delectAct = $_REQUEST['delectAct'] ?? '';
			
			$ObjRec = DM::load('product', $id);
			
			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}
			
			if($ObjRec['mail_banner_status'] == STATUS_DISABLE)
			{
				header("location: list.php?msg=F-1004");
				return;
			}
			$isPublished = SystemUtility::isPublishedRecord('product', $id);
			
			$values['product_id'] = $id;
			$values['product_publish_status'] = $isPublished ? 'RAA' : 'AL';
			$values['product_status'] = STATUS_DISABLE;			
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = getCurrentTimestamp();
			
			DM::update('product', $values);
			unset($values);
			
			if($isPublished){
				ApprovalService::withdrawApproval("scheme_".$id, $type, $id, $timestamp);
			
				Logger::log($_SESSION['sess_id'], $timestamp, "Product '" . $ObjRec['product_name_lang1'] . "' (ID:" . $ObjRec['product_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");

				NotificationService::sendNotification("scheme_".$id, $id, $timestamp, $_SESSION['sess_id'], array("E"), "scheme_".$id, "DS");
				
				$files = array($host_name."web/en/investment/index.php", $host_name."web/tc/investment/index.php", $host_name."web/sc/investment/index.php",$host_name."web/en/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/tc/investment/product.php?product=".$ObjRec['product_name_lang1'], $host_name."web/sc/investment/product.php?product=".$ObjRec['product_name_lang1']);
				
				$approval_id = ApprovalService::createApprovalRequest("scheme_".$id, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);				
				NotificationService::sendApprovalRequest("scheme_".$id, $id, $timestamp, $_SESSION['sess_id'], array("A"), "scheme_".$id, "PA", $approval_id);
			}
			header("location: list.php?msg=S-1002");
			return;
		} 
		
		
		
		else if($processType == "undelete" && isInteger($id)) {		
			$timestamp = getCurrentTimestamp();
			
			$ObjRec = DM::load('product', $id);
			
			if($ObjRec == NULL || $ObjRec == "")
			{
				header("location: list.php?msg=F-1002");
				return;
			}
			
			if($ObjRec['product_status'] == STATUS_ENABLE)
			{
				header("location: list.php?msg=F-1006");
				return;
			}
			
			$values['product_id'] = $id;
			$values['product_publish_status'] = 'D';
			$values['product_status'] = STATUS_ENABLE;			
			$values['product_updated_by'] = $_SESSION['sess_id'];
			$values['product_updated_date'] = getCurrentTimestamp();
			
			DM::update('product', $values);
			unset($values);

			$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
			$sql .= " AND approval_approval_status in (?, ?, ?) ";
			
			$parameters = array(STATUS_ENABLE, 'product', $id, 'RCA', 'RAA', 'APL');
			
			$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);
			
			foreach($approval_requests as $approval_request){
				$values = array();
				$values['approval_id'] = $approval_request['approval_id'];
				$values['approval_approval_status'] = 'W';
				$values['approval_updated_by'] = $_SESSION['sess_id'];
				$values['approval_updated_date'] = $timestamp;
				
				DM::update('approval', $values);
				unset($values);
				
				$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
				DM::query($sql, array('N', $approval_request['approval_id']));
				
				$recipient_list = array("E", "C");
				
				if($approval_request['approval_approval_status'] != "RCA"){
					$recipient_list[] = "A";
				}
				
				$message = "";
				
				switch($approval_request['approval_approval_status']){
					case "RCA": 
						$message = "Request for checking";
					break;
					case "RAA": 
						$message = "Request for approval";
					break;
					case "APL": 
						$message = "Pending to launch";
					break;
				}
				
				$message .= " of main banner '" . $ObjRec['ipo_eng_name'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";
				
				//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], $recipient_list, "ipo", $message);
			}
		
			Logger::log($_SESSION['sess_id'], $timestamp, "Product (ID:" . $ObjRec['product_id'] .") is undeleted by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			//NotificationService::sendNotification("ipo", $id, $timestamp, $_SESSION['sess_id'], array("E"), "ipo", "main Banner '" . $ObjRec['ipo_eng_name'] . "' is undeleted by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")");

			header("location: list.php?msg=S-1003");
			return;
		}
		
		
		if($processType == "updateSequence"){
			$product_tab = $_REQUEST['ipo_id'] ?? '';
			
			if($product_tab != "" && is_array($product_tab)){
				$counter = 1;
				$timestamp = getCurrentTimestamp();
				
				foreach($product_tab as $product_id){
					$ObjRec = DM::load('product', $product_id);
					
					if($ObjRec == NULL || $ObjRec == "" || $ObjRec['product_status'] != STATUS_ENABLE){
						continue;
					}
					
					$values = array();
					$values['product_id'] = $product_id;
					$values['product_sequence'] = $counter;
					$values['product_updated_by'] = $_SESSION['sess_id'];
					$values['product_updated_date'] = $timestamp;
					
					DM::update('product', $values);
					unset($values);
					$counter++;
				}
			}
			
			echo json_encode(array("result"=>"ok", "alert_type"=>"alert-success", "icon"=>"glyphicon glyphicon-ok", "message"=>$SYSTEM_MESSAGE['S-1001'] ));
			return;
		}
		
	}

	header("location:../dashboard.php?msg=F-1001");
	return;
?>