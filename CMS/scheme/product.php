<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$product_id = $_REQUEST['product_id'] ?? '';	
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($product_id != ""){
		
		if(!isInteger($product_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
			
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], 'scheme_' . $product_id)){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
			
		$obj_row = DM::load('product', $product_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		/*$menu = DM::findOne("menu", " menu_status = ? AND menu_link_type = ? AND menu_link_page_id = ? ", " menu_id ", array(STATUS_ENABLE, 'U', $page_id));
	
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "page_" . $menu['menu_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}*/
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
		
/*		if(!file_exists($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id)){
			mkdir($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id, 0775, true);
		}*/
		
	} else {
		/*header("location: list.php?msg=F-1002");
		return;*/
		
		if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
		
		$product_publish_status = 'D';
	}
	
	
	$product_tab = DM::findAll('product_tab', ' product_tab_status = ? And product_tab_pid = ? ', " product_tab_sequence ", array(STATUS_ENABLE,$product_id));
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
        <script type="text/x-tmpl" id="text_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">

						<span class="col-xs-12" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="page_section_type" value="T"/>
							<input type="hidden" name="product_tab_sequence" value=""/>
							<input type="hidden" name="product_tab_id" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">Tab Information</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							
							
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Name (English)</label>
								<div class="col-xs-10">
									<input type="text" class="form-control" placeholder="Tab Name (English)" id="product_tab_name_lang1" name="product_tab_name_lang1"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-2 control-label" style="padding-right:0px">Name (中文)</label>
								<div class="col-xs-10">
									<input type="text" class="form-control" placeholder="Tab Name (中文)" id="product_tab_name_lang2" name="product_tab_name_lang2"/>
								</div>
							</div>
							
						</div>
					</div>
				</form>
			</div>
		</script>
        
		<script>

			var file_list = {};
			var counter = 0;
			
			function checkForm(){				
				var flag=true;
				var errMessage="";
				

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');

			}
			
			function checkForm2(){
				var flag=true;				
				
					$('#approvalModalDialog').modal('show');

			}
			
			function cloneTextBlock(idx){
				if($("input[name='page_section_type'][value='T']").length <= idx){
					document.form1.submit();
					return;
				}
				
				var frm = $("input[name='page_section_type'][value='T']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneTextBlock(++idx);
			}
			
			function moveUp(button){
				var $current = $(button).closest(".section_row");
				var $previous = $current.prev('.section_row');
				if($previous.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertBefore($previous);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_tab_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveDown(button){
				var $current = $(button).closest(".section_row");
				var $next = $current.next('.section_row');
				if($next.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertAfter($next);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfig);
					});
				}
				$("input[name='product_tab_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function removeRow(button){
				$(button).closest(".section_row").detach();
				$("input[name='product_tab_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function addTextBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_tab_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function submitForm(){
				document.form1.submit();
			}
			
			function removeIcon(button){
				$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				$("#tabs").tabs({});

				$("#upload_icon").filestyle({buttonText: ""});				
				$("#product_banner").filestyle({buttonText: ""});
				$("#product_thumb_banner").filestyle({buttonText: ""});
				$("#btn_preview").on('click',function(){$("#mask").show("fast")})
				
				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});
				
				$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
					$(element).find("div.col-xs-12[id^='tabs']").tabs({});
				});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
			.section_row {
				margin-bottom:10px; 
				padding: 7px 15px 4px;
				border: 1px solid #ccc;
				border-radius: 4px;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li><a href="list.php">Scheme</a></li>
					<li class="active">Update</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
            
            <div id="top_submit_button_row" class="row" style="padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
            
            
			<div class="row">
				<table class="table" style="margin-bottom: 5px;">
					<tbody>
						<tr class="<?php echo ($product_publish_status == "RCA" || $product_publish_status == "RAA") ? "danger" : "" ?>">
							<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
							<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
								<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $product_publish_status] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
            
            
            <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td class="col-xs-12" style="vertical-align:middle" colspan="2">
									<div class="row">
										<div class="col-xs-6">
											<a role="button" class="btn btn-default" onclick="addTextBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Tab
											</a>
										</div>
									</div>
								</td>
							</tr>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Banner</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="product_banner" name="product_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										<!--(Width: <?php echo $PRODUCT_BANNER_WIDTH ?>px, Height: <?php echo $PRODUCT_BANNER_HEIGHT ?>px)-->
									</p>
								</div>
								<?php if($product_banner != "" && file_exists($product_path . $product_id . "/" . $product_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
                                        <a role="button" class="btn btn-default" id="btn_preview" href="#"><i class="glyphicon glyphicon-search"></i> Preview with Mask</a>
									</div>
								<?php } ?>
								</td>
                            </tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Banner Thumb</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div style="width:41.66666667%;float: left;">
									<input type="file" id="product_thumb_banner" name="product_thumb_banner" accept="image/*"/>
									<p style="padding-top:5px;padding-left:5px;">
										<!--(Width: <?php echo $PRODUCT_BANNER_WIDTH ?>px, Height: <?php echo $PRODUCT_BANNER_HEIGHT ?>px)-->
									</p>
								</div>
								<?php if($product_thumb_banner != "" && file_exists($product_path . $product_id . "/" . $product_thumb_banner)) { ?>
									<div class='col-xs-5'>
										<a role="button" class="btn btn-default" href="<?php echo $product_url . $product_id . "/" . $product_thumb_banner?>" target="_blank"><i class="glyphicon glyphicon-search"></i> View</a>
									</div>
								<?php } ?>
								</td>
                            </tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Banner Alignment</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <div style="width:21.66666667%;float: left;" class="input-group">
									<input type="text" class="form-control" placeholder="Alignment" name="product_banner_alignment" id="product_banner_alignment" value="<?php echo $product_banner_alignment ?>"/>
                                    <span class="input-group-addon">%</span>
								</div>
                                </td>
                            </tr>
                                
                                
                                
                                <?php if($product_banner != "" && file_exists($product_path . $product_id . "/" . $product_banner)) { ?>
                                <tr id="mask">
                                <td class="col-xs-2" style="vertical-align:middle">Preview</td>
								<td class="col-xs-10" style="vertical-align:middle" >
									<div class="form-group"  style="margin-bottom: 60px;">
										<img src="<?php echo $product_url . $product_id  . "/" .$product_banner ?>" width="915" style="
    width: 920px;
">
										<img src="/images/main_banner_mask.png" width="915" style="width: 920px;">
									</div>
								<!--</div>-->
                                </td>
                                </tr>
								<?php } ?>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Image description(English):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Image description(English)" id="product_banner_alt_lang1" name="product_banner_alt_lang1" value="<?php echo $product_banner_alt_lang1 ?>"/>
								</div>
								</td>
                            </tr>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Image description(中文):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Image description(中文)" id="product_banner_alt_lang2" name="product_banner_alt_lang2" value="<?php echo $product_banner_alt_lang2 ?>"/>
								</div>
								</td>
                            </tr>
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Display Priority</td>
								<td class="col-xs-8" style="vertical-align:middle">
								<input type="text" class="form-control" placeholder="Priority" name="product_seq" id="product_seq" value="<?php echo $product_seq ?>"/>
								</td>
							</tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Title(English):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Title(English)" id="product_meta_title_lang1" name="product_meta_title_lang1" value="<?php echo $product_meta_title_lang1 ?>"/>
								</div>
								</td>
                            </tr>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Title(中文):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Title(中文)" id="product_meta_title_lang2" name="product_meta_title_lang2" value="<?php echo $product_meta_title_lang2 ?>"/>
								</div>
								</td>
                            </tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Keywords(English):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Keywords(English)" id="product_keywords_lang1" name="product_keywords_lang1" value="<?php echo $product_keywords_lang1 ?>"/>
								</div>
								</td>
                            </tr>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Keywords(中文):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Keywords(中文)" id="product_keywords_lang2" name="product_keywords_lang2" value="<?php echo $product_keywords_lang2 ?>"/>
								</div>
								</td>
                            </tr>
                            
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Description(English):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Description(English)" id="product_meta_desc_lang1" name="product_meta_desc_lang1" value="<?php echo $product_meta_desc_lang1 ?>"/>
								</div>
								</td>
                            </tr>
                            <tr>
                            	<td class="col-xs-2" style="vertical-align:middle">Meta Description(中文):</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<div>
									<input type="text" class="form-control" placeholder="Meta Description(中文)" id="product_meta_desc_lang2" name="product_meta_desc_lang2" value="<?php echo $product_meta_desc_lang2 ?>"/>
								</div>
								</td>
                            </tr>
                            
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Name (English)</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" placeholder="Product Name (English)" name="product_name_lang1" id="product_name_lang1" value="<?php echo $product_name_lang1 ?>"/>
								</td>
							</tr>
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Name (中文)</td>
								<td class="col-xs-10" style="vertical-align:middle">
								<input type="text" class="form-control" placeholder="Product Name (中文)" name="product_name_lang2" id="product_name_lang2" value="<?php echo $product_name_lang2 ?>"/>
								</td>
							</tr>

                            
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Introduction (English)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <textarea id="product_detail_lang1" name="product_detail_lang1" class="form-control" placeholder="Product introduction (English)" style="resize:none" rows="5"><?php echo $product_detail_lang1 ?></textarea>
								</td>
							</tr>
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Introduction (中文)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <textarea id="product_detail_lang2" name="product_detail_lang2" class="form-control" placeholder="Product introduction (中文)" style="resize:none" rows="5"><?php echo $product_detail_lang2 ?></textarea>
								</td>
							</tr>

						</tbody>
					</table>
				<input type="hidden" name="type" value="product" />
				<input type="hidden" name="processType" id="processType" value="">
				<input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
                <input type="hidden" name="product_type" value="1" />
				<input type="hidden" name="product_status" value="1" />
				<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
				<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
			</form>
            
            <?php foreach($product_tab as $value) {
					printTextBlock($value);
			} ?>
            
			
			<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>

<?php
	function printTextBlock($record){ 
		$row_id = $record['product_tab_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
                <span class="col-xs-12" style="text-align:right">
                    <button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </button>
                    <button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
                        <i class="glyphicon glyphicon-arrow-down"></i>
                    </button>
                    <button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </button>
                    <input type="hidden" name="page_section_type" value="T"/>
                    <input type="hidden" name="product_tab_sequence" value="<?php echo $record['product_tab_sequence'] ?>"/>
                    <input type="hidden" name="product_tab_id" value="<?php echo $record['product_tab_id'] ?>"/>
                </span>
            </div>
            
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">Tab Information</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
                    
                    <div class="form-group">
                        <label class="col-xs-2 control-label" style="padding-right:0px">Name (English)</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Name (English)" id="product_tab_name_lang1" name="product_tab_name_lang1" value="<?php echo htmlspecialchars($record['product_tab_name_lang1']) ?>"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-xs-2 control-label" style="padding-right:0px">Name (中文)</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Name (中文)" id="product_tab_name_lang2" name="product_tab_name_lang2" value="<?php echo htmlspecialchars($record['product_tab_name_lang2']) ?>"/>
                        </div>
                    </div>
                    
                    
                    
				</div>
			</div>
		</form>
	</div>
<?php
	}  
?>