<?php
include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/Logger.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";
include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
RegenerateSitemap::regen($dbcon);

$type = $_REQUEST['type'] ?? '';
$processType = $_REQUEST['processType'] ?? '';

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

if($type=="peec_programmes_categories" )
{
	$id = $_REQUEST['peec_programmes_categories_id'] ?? '';
	$peec_programmes_categories_lv1 = $_REQUEST['peec_programmes_categories_lv1'] ?? '';
	$peec_programmes_categories_lv2 = $_REQUEST['peec_programmes_categories_lv2'] ?? '';
	$type_name = "peec_programmes_categories";
	if( isset($type_namelv1) ){ $type_name .= " - ".$type_namelv1;}
	if( isset($type_namelv2) ){ $type_name .= " - ".$type_namelv2;}
	if(!PermissionUtility::isAdminGroup($_SESSION['sess_id'])){
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "peec_programmes_categories")){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}
	}
	if($processType == "" || $processType == "submit_for_approval") {
		$values = $_REQUEST;

		$isNew = !isInteger($id) || $id == 0;
		$timestamp = getCurrentTimestamp();

		if($isNew){
			$values['peec_programmes_categories_publish_status'] = "D";
			$values['peec_programmes_categories_status'] = STATUS_ENABLE;
			$values['peec_programmes_categories_created_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_categories_created_date'] = $timestamp;
			$values['peec_programmes_categories_updated_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_categories_updated_date'] = $timestamp;

			$id = DM::insert('peec_programmes_categories', $values);

			if(!file_exists($peec_programmes_categories_path . $id. "/")){
				mkdir($peec_programmes_categories_path . $id . "/", 0775, true);
			}

		} else {
			$ObjRec = DM::load('peec_programmes_categories', $id);

			if($ObjRec['peec_programmes_categories_status'] == STATUS_DISABLE)
			{
				header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=F-1002");
				return;
			}

			$values['peec_programmes_categories_publish_status'] = $processType == "submit_for_approval" ? "RCA" : 'D';
			$values['peec_programmes_categories_updated_by'] = $_SESSION['sess_id'];
			$values['peec_programmes_categories_updated_date'] = $timestamp;

			DM::update('peec_programmes_categories', $values);
		}

		$ObjRec = DM::load('peec_programmes_categories', $id);
		$peec_programmes_categories_img_arr = array('peec_programmes_categories_banner');
		foreach($peec_programmes_categories_img_arr as $key=>$val){
			if($_FILES[$val]['tmp_name'] != ""){
				if($ObjRec[$val] != "" && file_exists($peec_programmes_categories_path . $id . "/" . $ObjRec[$val])){
					//	@unlink($peec_programmes_categories_path  . $id . "/" . $ObjRec[$val]);
				}
				$newfilename = pathinfo($_FILES[$val]['name'],PATHINFO_FILENAME);
				$newname = moveFile($peec_programmes_categories_path . $id . "/", $id, pathinfo($_FILES[$val]['name'],PATHINFO_EXTENSION) , $_FILES[$val]['tmp_name']);

				list($width, $height) = getimagesize($peec_programmes_categories_path . $id . "/".$newname);
				if($width > '1000'){
					resizeImageByWidth($peec_programmes_categories_path. $id."/". $newname, "1000" , $peec_programmes_categories_path. $id."/". $newname);
				}else if($height > '500'){
					resizeImageByHeight($peec_programmes_categories_path. $id."/". $newname, "500" , $peec_programmes_categories_path. $id."/". $newname);
				}


				$values = array();
				$values[$val] = $newname;
				$values['peec_programmes_categories_id'] = $id;
				DM::update('peec_programmes_categories', $values);
				unset($values);
				$ObjRec = DM::load('peec_programmes_categories', $id);
			}
		}

		if($processType == ""){
			Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes_categories '" . $ObjRec['peec_programmes_categories_name_lang1'] . "' (ID:" . $ObjRec['peec_programmes_categories_id'] .") is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec_programmes_categories", $isNew ? "CS" : "ES");
		} else if($processType == "submit_for_approval"){
			ApprovalService::withdrawApproval( "peec_programmes_categories",$type, $id, $timestamp);
			Logger::log($_SESSION['sess_id'], $timestamp, "promotion (ID:" . $ObjRec['peec_programmes_categories_id'] .") is updated and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "peec_programmes_categories", $isNew ? "CS" : "ES");
			$files = array($SYSTEM_HOST."peec_programmes_categories/peec_programmes_categories.php?peec_programmes_categories_id=".$id);
			$approval_id = ApprovalService::createApprovalRequest( "peec_programmes_categories", $type,$id, $isNew ? "C" : "U", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
			NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "peec_programmes_categories", "PA", $approval_id);
		}
		header("location:peec_programmes_categories.php?peec_programmes_categories_id=" . $id . "&msg=S-1001");
		return;
	}

	else if($processType == "delete" && isInteger($id)) {
		$timestamp = getCurrentTimestamp();

		$ObjRec = DM::load('peec_programmes_categories', $id);

		if($ObjRec == NULL || $ObjRec == "")
		{
			header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=F-1002");
			return;
		}

		if($ObjRec['peec_programmes_categories_status'] == STATUS_DISABLE)
		{
			header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=F-1004");
			return;
		}
		$isPublished = SystemUtility::isPublishedRecord("peec_programmes_categories", $id);
		$values['peec_programmes_categories_id'] = $id;
		$values['peec_programmes_categories_publish_status'] = $isPublished ? 'RAA' : 'AL';
		$values['peec_programmes_categories_status'] = STATUS_DISABLE;
		$values['peec_programmes_categories_updated_by'] = $_SESSION['sess_id'];
		$values['peec_programmes_categories_updated_date'] = getCurrentTimestamp();

		DM::update('peec_programmes_categories', $values);
		unset($values);
		if($isPublished){
			ApprovalService::withdrawApproval("peec_programmes_categories", $type, $id, $timestamp);
			Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes_categories '" . $ObjRec['peec_programmes_categories_name_lang1'] . "' (ID:" . $ObjRec['peec_programmes_categories_id'] .") is deleted and submitted for checking by user " . $_SESSION['sess_login'] . "(ID: " . $_SESSION['sess_id'] . ")");
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), "peec_programmes_categories", "DS");
			$files = array($SYSTEM_HOST."peec_programmes_categories/peec_programmes_categories.php?peec_programmes_categories_id=".$id);
			$approval_id = ApprovalService::createApprovalRequest($type_name, $type, $id, "D", $timestamp, $_REQUEST['approval_remarks'] ?? '', $files);
			NotificationService::sendApprovalRequest($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A"), "peec_programmes_categories", "PA", $approval_id);
		}else{
			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("A","E"), "peec_programmes_categories", "D");
		}


		header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=S-1002");
		return;
	} else if($processType == "undelete" && isInteger($id)) {
		$timestamp = getCurrentTimestamp();

		$ObjRec = DM::load('peec_programmes_categories', $id);

		if($ObjRec == NULL || $ObjRec == "")
		{
			header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=F-1002");
			return;
		}

		if($ObjRec['peec_programmes_categories_status'] == STATUS_ENABLE || $ObjRec['peec_programmes_categories_publish_status'] == 'AL')
		{
			header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=F-1006");
			return;
		}

		$values['peec_programmes_categories_id'] = $id;
		$values['peec_programmes_categories_publish_status'] = 'D';
		$values['peec_programmes_categories_status'] = STATUS_ENABLE;
		$values['peec_programmes_categories_updated_by'] = $_SESSION['sess_id'];
		$values['peec_programmes_categories_updated_date'] = getCurrentTimestamp();

		DM::update('peec_programmes_categories', $values);
		unset($values);


		$sql = " approval_status = ? AND approval_item_type = ? AND approval_item_id = ? ";
		$sql .= " AND approval_approval_status in (?, ?, ?) ";

		$parameters = array(STATUS_ENABLE, 'peec_programmes_categories', $id, 'RCA', 'RAA', 'APL');

		$approval_requests = DM::findAll("approval", $sql, " approval_submit_date ", $parameters);

		foreach($approval_requests as $approval_request){
			$values = array();
			$values['approval_id'] = $approval_request['approval_id'];
			$values['approval_approval_status'] = 'W';
			$values['approval_updated_by'] = $_SESSION['sess_id'];
			$values['approval_updated_date'] = $timestamp;

			DM::update('approval', $values);
			unset($values);

			$sql = " UPDATE system_message SET system_message_can_process = ? WHERE system_message_approval_id = ? ";
			DM::query($sql, array('N', $approval_request['approval_id']));

			$recipient_list = array("E", "C");

			if($approval_request['approval_approval_status'] != "RCA"){
				$recipient_list[] = "A";
			}

			$message = "";

			switch($approval_request['approval_approval_status']){
				case "RCA":
				$message = "Request for checking";
				break;
				case "RAA":
				$message = "Request for approval";
				break;
				case "APL":
				$message = "Pending to launch";
				break;
			}

			$message .= " of peec_programmes_categories '" . $ObjRec['peec_programmes_categories_name_lang1'] . "' is withdrawn by user " . $_SESSION['sess_name'] . " (" . $_SESSION['sess_login'] . ")";

			NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], $recipient_list, $type_name, "WA");
		}

		Logger::log($_SESSION['sess_id'], $timestamp, "peec_programmes_categories '" . $ObjRec['peec_programmes_categories_name_lang1'] . "' (ID:" . $ObjRec['peec_programmes_categories_id'] .") is undeleted by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");
		NotificationService::sendNotification($type_name, $id, $timestamp, $_SESSION['sess_id'], array("E"), $type_name, "UD");
		header("location: list.php?peec_programmes_categories_lv1=".$peec_programmes_categories_lv1."&peec_programmes_categories_lv2=".$peec_programmes_categories_lv2."&msg=S-1003");
		return;
	}
}

header("location:../dashboard.php?msg=F-1001");
return;
?>
