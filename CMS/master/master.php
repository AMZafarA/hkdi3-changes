<?php
	include_once "../include/config.php";
	include_once $SYSTEM_BASE . "include/function.php";
	include_once $SYSTEM_BASE . "session.php";
	include_once $SYSTEM_BASE . "include/system_message.php";
	include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
	include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
	include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";

	DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

	$master_lv1 = $_REQUEST['master_lv1'] ?? '';
	$master_lv2 = $_REQUEST['master_lv2'] ?? '';


	if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "master")){
		header("location: ../dashboard.php?msg=F-1000");
		return;
	}

	$product_id = "1";	
	$msg = $_REQUEST['msg'] ?? '';	
	
	if($product_id != ""){
		if(!isInteger($product_id))
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		$obj_row = DM::load('product', $product_id);
		
		if($obj_row == "")
		{
			header("location: list.php?msg=F-1002");
			return;
		}
		
		if(!file_exists($product_path . $product_id)){
			mkdir($product_path . $product_id, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "product/" . $product_id . "/";
		
		/*$menu = DM::findOne("menu", " menu_status = ? AND menu_link_type = ? AND menu_link_page_id = ? ", " menu_id ", array(STATUS_ENABLE, 'U', $page_id));
	
		if(!PermissionUtility::canEditByKey($_SESSION['sess_id'], "page_" . $menu['menu_id'])){
			header("location: ../dashboard.php?msg=F-1000");
			return;
		}*/
		
		foreach($obj_row as $rKey => $rValue){
			$$rKey = htmlspecialchars($rValue);
		}
		
/*		if(!file_exists($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id)){
			mkdir($SYSTEM_BASE . "uploaded_files/main_banner/" . $main_banner_id, 0775, true);
		}*/
		
	} else {
		/*header("location: list.php?msg=F-1002");
		return;*/
		$product_publish_status = 'D';
		foreach( DM::stub('product') as $rKey => $rValue )
			if ( ! isset( $$rKey ) )
				$$rKey = htmlspecialchars($rValue);
		
		$length = 10;

		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		
		if(!file_exists($product_path . $randomString)){
			mkdir($product_path . $randomString, 0775, true);
		}
		
		$_SESSION['RF']['subfolder'] = "product/" . $randomString . "/";
	}
	
	
	$product_section = DM::findAll('product_section', ' product_section_status = ? And product_section_pid = ? ', " product_section_sequence ", array(STATUS_ENABLE,$product_id));
	$this_product_section = $product_section ? end( $product_section ) : DM::stub( 'product_section' );
	foreach ( $this_product_section as $rKey => $rValue )
		if ( ! isset( $$rKey ) )
			$$rKey = htmlspecialchars($rValue);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once "../header.php" ?>
        <script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
		<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
        <script type="text/x-tmpl" id="text_block_template">
			<div id="section-row-{%=o.tmp_id%}" class="row section_row">
				<form class="form-horizontal" role="form" id="form-tmp-{%=o.tmp_id%}" name="form-tmp-{%=o.tmp_id%}" action="act.php" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-bottom: 5px;">
						<label class="col-xs-2 control-label">
							Text Section
						</label>
						<label class="col-xs-2 control-label">
							Background Color
						</label>
						<span class="col-xs-2">
							<div class="input-group color_code">
								<select class="form-control" name="product_section_background_color">
									<option value="0">White</option>
									<option value="1">Red</option>
								</select>
							</div>
						</span>
						<span class="col-xs-6" style="text-align:right">
							<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
								<i class="glyphicon glyphicon-arrow-up"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
								<i class="glyphicon glyphicon-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<input type="hidden" name="product_section_type" value="T"/>
							<input type="hidden" name="product_section_id" value=""/>
							<input type="hidden" name="product_section_tid" value=""/>
							<input type="hidden" name="product_section_sequence" value=""/>
						</span>
					</div>
					<div id="tabs-tmp-{%=o.tmp_id%}" class="col-xs-12">
						<ul>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-1">English</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-2">繁中</a></li>
							<li><a href="#tabs-tmp-{%=o.tmp_id%}-3">簡中</a></li>
						</ul>
						<div id="tabs-tmp-{%=o.tmp_id%}-1">
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang1" name="product_section_content_lang1"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-2">
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang2" name="product_section_content_lang2"></textarea>
								</div>
							</div>
						</div>
						<div id="tabs-tmp-{%=o.tmp_id%}-3">
							<div class="form-group">
								<div class="col-xs-12">
									<textarea class="form-control" id="section-row-{%=o.tmp_id%}-content-lang3" name="product_section_content_lang3"></textarea>
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-xs-12" style="height:15px">&nbsp;</div>
					</div>
				</form>
			</div>
		</script>
        
		<script>

			var file_list = {};
			var counter = 0;
			
			function checkForm(){				
				var flag=true;
				var errMessage="";
				

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');

			}
			
			function checkForm2(){
				var flag=true;				
				
					$('#approvalModalDialog').modal('show');

			}
			
			
			function cloneTextBlock(idx){
				if($("input[name='product_section_type'][value='T']").length <= idx){
					document.form1.submit();
					return;
				}
				
				var frm = $("input[name='product_section_type'][value='T']:eq(" + idx + ")").closest("form");
				trimForm($(frm).attr("id"));

				var data = JSON.stringify(cloneFormToObject(frm));
				$('<input>').attr('name','text_block_section[]').attr('type','hidden').val(data).appendTo('#form1');
				cloneTextBlock(++idx);
			}
			
			
			
			function moveUp(button){
				var $current = $(button).closest(".section_row");
				var $previous = $current.prev('.section_row');
				if($previous.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertBefore($previous);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfigMaster);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function moveDown(button){
				var $current = $(button).closest(".section_row");
				var $next = $current.next('.section_row');
				if($next.length !== 0){
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce().remove();
					});
					$current.insertAfter($next);
					$current.find("textarea").each(function(idx, element){
						$(element).tinymce(tinymceConfigMaster);
					});
				}
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function removeRow(button){
				$(button).closest(".section_row").detach();
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			/*function addTextBlock(tab_id){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				$("#tabs-"+tab_id).append(content);
				$("input[name='product_section_tid']").val(tab_id);
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfig);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}*/
			
			function addTextBlock(){
				var tmp_id = "tmp_" + (counter++);
				var obj = {tmp_id:tmp_id};
				var content = tmpl("text_block_template", obj);
				$(content).insertBefore("#submit_button_row");
				$("#tabs-tmp-" + tmp_id).tabs({});
				$('#section-row-' + tmp_id + ' textarea').tinymce(tinymceConfigMaster);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				$("input[name='product_section_sequence']").each(function(idx, element){
					$(element).val(idx + 1);
				});
			}
			
			function submitForm(){
				document.form1.submit();
			}
			
			function removeIcon(button){
				$("#remove_icon").val("Y");
				$(button).closest("div").detach();
			}
			
			function submitForApproval(){
				if($("#input_launch_date").val() == ""){
					alert("Please select launch date");
				} else {
					$("#approval_remarks").val($("#input_approval_remarks").val());
					$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
					$('#approvalModalDialog').modal('hide');

					$('#loadingModalDialog').on('shown.bs.modal', function (e) {
						var frm = document.form1;
						frm.processType.value = "submit_for_approval";
						cloneTextBlock(0);
					})
					
					$('#loadingModalDialog').modal('show');
				}
			}

			$( document ).ready(function() {
				$("#tabs").tabs({});

				$("#upload_icon").filestyle({buttonText: ""});				
				$("#product_banner").filestyle({buttonText: ""});
				
				$( "#input_launch_date" ).datepicker({"dateFormat" : "yy-mm-dd"});
				
				$("form[name!='form1'][name!='approval_submit_form']").each(function(idx, element){
					$(element).find("div.col-xs-12[id^='tabs']").tabs({});
					$(element).find('textarea').tinymce(tinymceConfigMaster);
				});

				<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
					setTimeout(function() {
						$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
					}, 5000);
				<?php } ?>
			});
		</script>
		<style>
			.vcenter {
				display: inline-block;
				vertical-align: middle;
				float: none;
			}
			.section_row {
				margin-bottom:10px; 
				padding: 7px 15px 4px;
				border: 1px solid #ccc;
				border-radius: 4px;
			}
		</style>
	</head>
	<body>
		<?php include_once "../menu.php" ?>

		<!-- Begin page content -->
		<div id="content" class="container">
			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				<div id="alert_row" class="row">
					<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
						<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<ol class="breadcrumb-left col-xs-10">
					<li><a href="../dashboard.php">Home</a></li>
					<li>Master - Index Page</li>
					<li class="active">Update</li>
				</ol>
				<span class="breadcrumb-right col-xs-2">
					&nbsp;
				</span>
			</div>
            
            <div id="top_submit_button_row" class="row" style="padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
            
            
			<div class="row">
				<table class="table" style="margin-bottom: 5px;">
					<tbody>
						<tr class="<?php echo ($product_publish_status == "RCA" || $product_publish_status == "RAA") ? "danger" : "" ?>">
							<td class="col-xs-2" style="vertical-align:middle;border-top-style:none;">Publish Status:</td>
							<td class="col-xs-11" style="vertical-align:middle;border-top-style:none;">
								<?php echo $SYSTEM_MESSAGE['PUBLISH_STATUS_' . $product_publish_status] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
            
            
            <form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
				<div class="row">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td class="col-xs-12" style="vertical-align:middle" colspan="2">
									<div class="row">
										<div class="col-xs-6">
											<a role="button" class="btn btn-default" onclick="addTextBlock(); return false;">
												<i class="fa fa-list fa-lg"></i> Text Section
											</a>
										</div>
									</div>
								</td>
							</tr>
                            
                            
                            <tr>
								<td class="col-xs-2" style="vertical-align:middle">Title (English)</td>
								<td class="col-xs-10" style="vertical-align:middle">
                                <input type="text" class="form-control" name="product_name_lang1" placeholder="Title (English)" value="<?php echo $product_name_lang1?>">
								</td>
							</tr>
                           

						</tbody>
					</table>
                    </div>
				<input type="hidden" name="type" value="product" />
				<input type="hidden" name="processType" id="processType" value="">
				<input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
                <input type="hidden" name="product_type" value="1" />
				<input type="hidden" name="product_status" value="1" />
				<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
				<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
                <input type="hidden" name="randomString" value="<?php echo $randomString ?>" />
                <input type="hidden" name="master_lv1" id="master_lv1" value="<?php echo $master_lv1 ?>" />
                <input type="hidden" name="master_lv2" id="master_lv2" value="<?php echo $master_lv2 ?>" />
                
			</form>
            
            <?php foreach($product_section as $value) {
					printTextBlock($value);
			} ?>
            
			
			<div id="submit_button_row" class="row" style="padding-top:10px;padding-bottom:10px">
				<div class="col-xs-12" style="text-align:right;padding-right:0px;">
					<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save
					</button>
					<button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
						<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
					</button>
				</div>
			</div>
		</div>
		<!-- End page content -->
		<?php include_once "../footer.php" ?>
	</body>
</html>

<?php
	function printTextBlock($record){ 
		$row_id = $record['product_section_type'] . '-' . $record['product_section_id'];
?>
	<div id="section-row-<?php echo $row_id  ?>" class="row section_row">
		<form class="form-horizontal" role="form" id="form-<?php echo $row_id ?>" name="form-<?php echo $row_id ?>" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row" style="margin-bottom: 5px;">
            	<label class="col-xs-2 control-label">
                    Text Block
                </label>
            	<label class="col-xs-2 control-label">
                    Background Color
                </label>
                <span class="col-xs-2">
                    <div class="input-group color_code">
                        <select class="form-control" name="product_section_background_color">
                            <option value="0" <?php echo "0" == $record['product_section_background_color'] ? "selected" : "" ?> >White</option>
                            <option value="1" <?php echo "1" == $record['product_section_background_color'] ? "selected" : "" ?>>Red</option>
                        </select>
                    </div>
                </span>
				<span class="col-xs-6" style="text-align:right">
					<button type="button" class="btn btn-default" onclick="moveUp(this); return false;">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="moveDown(this); return false;">
						<i class="glyphicon glyphicon-arrow-down"></i>
					</button>
					<button type="button" class="btn btn-default" onclick="removeRow(this); return false;">
						<i class="glyphicon glyphicon-trash"></i>
					</button>
					<input type="hidden" name="product_section_type" value="T"/>
					<input type="hidden" name="product_section_id" value="<?php echo $record['product_section_id'] ?>"/>
					<input type="hidden" name="product_section_sequence" value="<?php echo $record['product_section_sequence'] ?>"/>
				</span>
			</div>
			<div id="tabs-<?php echo $row_id ?>" class="col-xs-12">
				<ul>
					<li><a href="#tabs-<?php echo $row_id ?>-1">English</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-2">繁中</a></li>
					<li><a href="#tabs-<?php echo $row_id ?>-3">簡中</a></li>
				</ul>
				<div id="tabs-<?php echo $row_id ?>-1">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang1" name="product_section_content_lang1"><?php echo htmlspecialchars($record['product_section_content_lang1']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-2">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang2" name="product_section_content_lang2"><?php echo htmlspecialchars($record['product_section_content_lang2']) ?></textarea>
						</div>
					</div>
				</div>
				<div id="tabs-<?php echo $row_id ?>-3">
					<div class="form-group">
						<div class="col-xs-12">
							<textarea class="form-control" id="section-row-<?php echo $row_id ?>-content-lang3" name="product_section_content_lang3"><?php echo htmlspecialchars($record['product_section_content_lang3']) ?></textarea>
						</div>
					</div>
				</div>
                
			</div>
		</form>
	</div>
<?php
	}   
?>