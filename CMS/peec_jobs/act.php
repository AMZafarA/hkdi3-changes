<?php

include_once "../include/config.php";
include_once $SYSTEM_BASE . "include/function.php";
include_once $SYSTEM_BASE . "session.php";
include_once $SYSTEM_BASE . "include/system_message.php";
include_once $SYSTEM_BASE . "include/classes/DataMapper.php";
include_once $SYSTEM_BASE . "include/classes/Logger.php";
include_once $SYSTEM_BASE . "include/classes/SystemUtility.php";
include_once $SYSTEM_BASE . "include/classes/NotificationService.php";
include_once $SYSTEM_BASE . "include/classes/PermissionUtility.php";
include_once $SYSTEM_BASE . "include/classes/ApprovalService.php";
include_once $SYSTEM_BASE . "include/RegenerateSitemap.php";
$dbcon = array($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);
RegenerateSitemap::regen($dbcon);

$type = $_REQUEST['type'] ?? '';
$processType = $_REQUEST['processType'] ?? '';

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

DM::deleteAll("peecjob_form");

$form_fields = $_POST["form_fields"];
$form_fields = json_decode($form_fields, true);
foreach($form_fields as $ff) {
	foreach($ff as $f) {
		$values["peecjob_form_page"] = $f["page"];
		$values["peecjob_form_label"] = $f["label"];
		$values["peecjob_form_type"] = $f["type"];
		$values["peecjob_form_short_description"] = $f["short_description"];
		$values["peecjob_form_required"] = isset($f["required"]) && $f["required"] == "Yes" ? 1 : 0;
		$values["peecjob_form_status"] = isset($f["status"]) && $f["status"] == "Yes" ? 1 : 0;
		$values["peecjob_form_choices"] = json_encode($f["choices"], true);
		$values["peecjob_form_publish_status"] = "AL";
		DM::insert('peecjob_form', $values);
	}
}

$timestamp = getCurrentTimestamp();

Logger::log($_SESSION['sess_id'], $timestamp, "peec job form is updated by user " . $_SESSION['sess_login'] . " (ID: " . $_SESSION['sess_id'] . ")");

header("location:../peec_jobs/peec_jobs.php?msg=S-1001");
return;
?>
