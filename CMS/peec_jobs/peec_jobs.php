<?php
include "../include/config.php";
include $SYSTEM_BASE . "include/function.php";
include $SYSTEM_BASE . "session.php";
include $SYSTEM_BASE . "include/system_message.php";
include $SYSTEM_BASE . "include/classes/DataMapper.php";
include $SYSTEM_BASE . "include/classes/SystemUtility.php";
include $SYSTEM_BASE . "include/classes/PermissionUtility.php";

DM::setup($DB_HOST, $DB_NAME, $DB_LOGIN, $DB_PASSWORD);

$page1 = DM::findAll("peecjob_form", "peecjob_form_page = ?", "peecjob_form_id ASC", array(1));
$page2 = DM::findAll("peecjob_form", "peecjob_form_page = ?", "peecjob_form_id ASC", array(2));

$msg = $_REQUEST['msg'] ?? '';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once "../header.php" ?>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/tinymceConfig.js"></script>
	<script type="text/javascript" src="<?php echo $SYSTEM_HOST ?>js/jquery.cookie.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/tmpl.min.js"></script>
	<script src="<?php echo $SYSTEM_HOST ?>js/bootstrap-colorpicker.min.js"></script>
	<link href="<?php echo $SYSTEM_HOST ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">


	<script>
		var file_list = {};
		var counter = 0;

		function checkForm(){				
			var flag=true;
			var errMessage="";

			trimForm("form1");

			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#loadingModalDialog').on('shown.bs.modal', function (e) {
				submitForm();
			})

			$('#loadingModalDialog').modal('show');
		}

		function checkForm2(){
			var flag=true;
			var errMessage="";

			trimForm("form1");

			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#approvalModalDialog').modal('show');
		}

		function checkForm3(){				
			var flag=true;
			var errMessage="";

			trimForm("form1");


			if(flag==false){
				errMessage = "Please fill in the following information:\n" + errMessage;
				alert(errMessage);
				return;
			}

			$('#loadingModalDialog').on('shown.bs.modal', function (e) {
				$('#processType').val('submit_for_production');
				submitForm();
			})

			$('#loadingModalDialog').modal('show');
			$('#processType').val('');
		}

		function corp(){
			if (confirm('Are you sure you want to corp the image?')) {
				$('#processType').val('corp');
				document.form1.submit();
			} else {
			}

		}

		function submitForm(){

			generateFieldsJSON();

			var obj = {"product_section_type":"F"};		
			obj['lang1_files'] = [];
			cloneFile(obj,0);	
		}

		function convert_tchi_to_schi() {
			$("#properties_name_lang3").val(toSimplifiedString($("#properties_name_lang2").val()));
			$("#properties_address_lang3").val(toSimplifiedString($("#properties_address_lang2").val()));
			$("#properties_parking_lang3").val(toSimplifiedString($("#properties_parking_lang2").val()));
			$("#properties_lift_lang3").val(toSimplifiedString($("#properties_lift_lang2").val()));
			$("#properties_floor_height_lang3").val(toSimplifiedString($("#properties_floor_height_lang2").val()));
			$("#properties_floor_loading_lang3").val(toSimplifiedString($("#properties_floor_loading_lang2").val()));
			$("#properties_park_entrance_lang3").val(toSimplifiedString($("#properties_park_entrance_lang2").val()));
			$("#properties_cargo_lift_lang3").val(toSimplifiedString($("#properties_cargo_lift_lang2").val()));
			$("#properties_cargo_lift_max_load_lang3").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang2").val()));
			$("#properties_air_conditioning_lang3").val(toSimplifiedString($("#properties_air_conditioning_lang2").val()));
			$("#properties_fire_services_lang3").val(toSimplifiedString($("#properties_fire_services_lang2").val()));
			$("#properties_security_lang3").val(toSimplifiedString($("#properties_security_lang2").val()));
			$("#properties_transportaion_lang3").val(toSimplifiedString($("#properties_transportaion_lang2").val()));
			$("#properties_enquiries_lang3").val(toSimplifiedString($("#properties_enquiries_lang2").val()));
			$("#properties_enquiries2_lang3").val(toSimplifiedString($("#properties_enquiries2_lang2").val()));
			$("#properties_keywords_lang3").val(toSimplifiedString($("#properties_keywords_lang2").val()));
			$("#properties_description_lang3").val(toSimplifiedString($("#properties_description_lang2").val()));
		};

		function convert_schi_to_tchi() {
			$("#properties_name_lang2").val(toTraditionalString($("#properties_name_lang3").val()));
			$("#properties_address_lang2").val(toTraditionalString($("#properties_address_lang3").val()));
			$("#properties_parking_lang2").val(toTraditionalString($("#properties_parking_lang3").val()));
			$("#properties_lift_lang2").val(toTraditionalString($("#properties_lift_lang3").val()));
			$("#properties_floor_height_lang2").val(toTraditionalString($("#properties_floor_height_lang3").val()));
			$("#properties_floor_loading_lang2").val(toTraditionalString($("#properties_floor_loading_lang3").val()));
			$("#properties_park_entrance_lang2").val(toTraditionalString($("#properties_park_entrance_lang3").val()));
			$("#properties_cargo_lift_lang2").val(toTraditionalString($("#properties_cargo_lift_lang3").val()));
			$("#properties_cargo_lift_max_load_lang2").val(toSimplifiedString($("#properties_cargo_lift_max_load_lang3").val()));
			$("#properties_air_conditioning_lang2").val(toSimplifiedString($("#properties_air_conditioning_lang3").val()));
			$("#properties_fire_services_lang2").val(toSimplifiedString($("#properties_fire_services_lang3").val()));
			$("#properties_security_lang2").val(toSimplifiedString($("#properties_security_lang3").val()));
			$("#properties_transportaion_lang2").val(toTraditionalString($("#properties_transportaion_lang3").val()));
			$("#properties_enquiries_lang2").val(toTraditionalString($("#properties_enquiries_lang3").val()));
			$("#properties_enquiries2_lang2").val(toTraditionalString($("#properties_enquiries2_lang3").val()));
			$("#properties_keywords_lang2").val(toSimplifiedString($("#properties_keywords_lang3").val()));
			$("#properties_description_lang2").val(toSimplifiedString($("#properties_description_lang3").val()));

		};


		function addFile(fileBrowser, language){
			if(!$(fileBrowser)[0].files.length || $(fileBrowser)[0].files.length == 0)
				return;

			var files = $(fileBrowser)[0].files;

			for(var i = 0; i < files.length; i++){
				var obj = {filename:files[i].name, tmp_id:"tmp_" + (counter++), "language": language};
				file_list[obj.tmp_id] = files[i];
				var content = tmpl("PDF_file_template", obj);
				var header_row = $(fileBrowser).closest("div.form-group");

				if($(header_row).is(":last-child")){
					$(content).insertAfter(header_row);
				} else {
					$(content).insertAfter($(header_row).siblings(":last"));
				}
			}
		}

		function moveFileUp(button){
			var $current = $(button).closest(".file_row");
			var $previous = $current.prev('.file_row');
			if($previous.length !== 0){
				$current.insertBefore($previous);
			}
		}

		function moveFileDown(button){
			var $current = $(button).closest(".file_row");
			var $next = $current.next('.file_row');
			if($next.length !== 0){
				$current.insertAfter($next);
			}
		}

		function removeFileItem(button){
			$(button).closest(".file_row").detach();				
		}


		function submitForApproval(){
			if($("#input_launch_date").val() == ""){
				alert("Please select launch date");
			} else {
				$("#approval_remarks").val($("#input_approval_remarks").val());
				$("#approval_launch_date").val($("#input_launch_date").val() + " " + $("#input_launch_time").val());
				$('#approvalModalDialog').modal('hide');

				$('#loadingModalDialog').on('shown.bs.modal', function (e) {
					var frm = document.form1;
					frm.processType.value = "submit_for_approval";
					submitForm();
				})

				$('#loadingModalDialog').modal('show');
			}
		}

		function cloneFile(dataObj, elementIdx){
			var frm = $("#form1");

			if($(frm).find("div.file_row").length <= elementIdx){
				var data = JSON.stringify(dataObj);
				$('<input>').attr('name','file_section[]').attr('type','hidden').val(data).appendTo('#form1');
				document.form1.submit();
				return;
			}

			var row = $(frm).find("div.file_row:eq(" + elementIdx + ")");



			var fileObj = {};
			fileObj['programmes_image_alt'] = $(row).find("input[name='programmes_image_alt']").val();
			fileObj['programmes_image_id'] = $(row).find("input[name='programmes_image_id']").val();
			fileObj['tmp_filename'] = "";
			fileObj['filename'] = "";


			var fd = new FormData();
			fd.append('upload_file', file_list[$(row).find("input[name='tmp_id']").val()]);

			$.ajax({
				url: "../pre_upload/act.php",
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType : "json",
				success: function(data) {
					fileObj['tmp_filename'] = data.tmpname;
					fileObj['filename'] = data.filename;

					dataObj['lang1_files'].push(fileObj);
					cloneFile(dataObj, ++elementIdx);

				}
			});


		}

		$( document ).ready(function() {

			<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
				setTimeout(function() {
					$("#alert_row").slideUp(1000, function(){$("#alert_row").detach();});
				}, 5000);
			<?php } ?>

		});
	</script>
	<style>
		.vcenter {
			display: inline-block;
			vertical-align: middle;
			float: none;
		}
	</style>


</head>
<body>
	<?php include_once "../menu.php" ?>

	<!-- Begin page content -->
	<div id="content" class="container">
		<?php if($msg != "" && array_key_exists($msg, $SYSTEM_MESSAGE)) { ?>
			<div id="alert_row" class="row">
				<div class="col-xs-6 col-xs-offset-3 alert <?php echo startsWith($msg, "F-") ? "alert-danger" : "alert-success" ?>" role="alert">
					<span class="glyphicon <?php echo startsWith($msg, "F-") ? "glyphicon-remove" : "glyphicon-ok" ?>"></span> <?php echo $SYSTEM_MESSAGE[$msg] ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<ol class="breadcrumb-left col-xs-10">
				<li><a href="../dashboard.php">Home</a></li>
				<li class="active">PEEC</li>


				<?php
				$department = DM::load("department", 27);
				?>
				<li class="active"><?php echo $department['department_name_lang1']?></li>

				<li class="active">Update</li>
			</ol>
			<span class="breadcrumb-right col-xs-2">
				&nbsp;
			</span>
		</div>

		<div id="top_submit_button_row" class="row" style="padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<!-- <button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button> -->
			</div>
		</div>
		<form class="form-horizontal" role="form" id="form1" name="form1" action="act.php" method="post" enctype="multipart/form-data">
			<div class="row">
				<h3>Part A: Details of the Job Post</h3>
			</div>
			<div class="row part-fields" id="part-a-fields">
				<?php
				$i1 = 1;
				foreach ($page1 as $p1) {
					?>
					<div class="input-field-row" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>">

						<h4><strong>Question: <?php echo $i1; ?></strong></h4>

						<!-- input label -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Label</label>
							<div class="col-xs-10">
								<input type="text" class="input-label" name="input-label-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-label-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" value="<?php echo $p1["peecjob_form_label"]; ?>">
							</div>
						</div>

						<!-- input type -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Type</label>
							<div class="col-xs-10">
								<select class="input-type" name="input-type-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-type-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>">
									<option value="text" <?php echo $p1["peecjob_form_label"] == "text" ? "selected" : ""; ?> >Text</option>
									<option value="number" <?php echo $p1["peecjob_form_label"] == "number" ? "selected" : ""; ?> >Number</option>
									<option value="email" <?php echo $p1["peecjob_form_label"] == "email" ? "selected" : ""; ?> >Email</option>
									<option value="date" <?php echo $p1["peecjob_form_label"] == "date" ? "selected" : ""; ?> >Date</option>
									<option value="textarea" <?php echo $p1["peecjob_form_label"] == "textarea" ? "selected" : ""; ?> >Textarea</option>
									<option value="checkbox" <?php echo $p1["peecjob_form_label"] == "checkbox" ? "selected" : ""; ?> >Checkbox</option>
									<option value="radio" <?php echo $p1["peecjob_form_label"] == "radio" ? "selected" : ""; ?> >Radio</option>
									<option value="dropdown" <?php echo $p1["peecjob_form_label"] == "dropdown" ? "selected" : ""; ?> >Dropdown</option>
								</select>
							</div>
						</div>

						<!-- input required -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Required</label>
							<div class="col-xs-10">
								<input type="radio" class="input-required" name="input-radio-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-required-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" value="Yes" <?php echo $p1["peecjob_form_required"] == 1 ? "checked" : ""; ?>> Yes
								<input type="radio" class="input-required" name="input-radio-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-required-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" value="No" style="margin-left: 10px;" <?php echo $p1["peecjob_form_required"] == 0 ? "checked" : ""; ?>> No
							</div>
						</div>

						<!-- input enabled -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Enabled</label>
							<div class="col-xs-10">
								<input type="radio" class="input-status" name="input-status-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-enabled-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" value="Yes" <?php echo $p1["peecjob_form_status"] == 1 ? "checked" : ""; ?>> Yes
								<input type="radio" class="input-status" name="input-status-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-enabled-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" value="No" style="margin-left: 10px;" <?php echo $p1["peecjob_form_status"] == 0 ? "checked" : ""; ?>> No
							</div>
						</div>

						<!-- input short description -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Short Description</label>
							<div class="col-xs-10">
								<textarea class="input-short-description" name="input-short-description-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-identifier="part-a-short-description-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>"><?php echo $p1["peecjob_form_short_description"]; ?></textarea>
							</div>
						</div>

						<!-- input checkbox, dropdown and radio choices -->
						<div <?php echo $p1["peecjob_form_type"] == "checkbox" || $p1["peecjob_form_type"] == "radio" || $p1["peecjob_form_type"] == "dropdown" ? '' : 'style="display: none;"'; ?> class="choices-container" data-identifier="part-a-choices-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>">
							<h4><strong>Choices:</strong></h4>
							<div class="choices-div" data-identifier="part-a-choices-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>" data-index="<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>">
								<?php
								$p1_choices = json_decode($p1["peecjob_form_choices"], true);
								if(!empty($p1_choices)) {
									foreach ($p1_choices as $p1c) {
										?>
										<div class="option">
											<div class="form-group">
												<label class="col-xs-2 control-label">Label</label>
												<div class="col-xs-10">
													<input type="text" class="label-text" value="<?php echo $p1c["label"]; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-2 control-label">Value</label>
												<div class="col-xs-10">
													<input type="text" class="value-text" value="<?php echo $p1c["value"]; ?>">
												</div>
											</div>
										</div>
										<hr style="border-color:#ccc;">
										<?php
									}
								}
								?>
							</div>
							<button type="button" class="add-choice btn btn-primary" data-container="part-a-choices-<?php echo $p1["peecjob_form_page"] . '-' . $i1; ?>">Add Choice</button>
						</div>

					</div>
					<?php
					$i1++;
				}
				?>
			</div>
			<div class="row" style="margin: 40px 0;">
				<button type="button" class="add-field btn btn-secondary" id="add-part-a-field" onclick="addfield('part-a-fields')">Add Field</button>
			</div>

			<div class="row">
				<h3>Part B: Company Contact for HKDI-PEEC</h3>
			</div>
			<div class="row part-fields" id="part-b-fields">
				<?php
				$i2 = 1;
				foreach ($page2 as $p2) {
					?>
					<div class="input-field-row" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>">

						<h4><strong>Question: <?php echo $i2; ?></strong></h4>

						<!-- input label -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Label</label>
							<div class="col-xs-10">
								<input type="text" class="input-label" name="input-label-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-label-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" value="<?php echo $p2["peecjob_form_label"]; ?>">
							</div>
						</div>

						<!-- input type -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Type</label>
							<div class="col-xs-10">
								<select class="input-type" name="input-type-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-type-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>">
									<option value="text" <?php echo $p2["peecjob_form_label"] == "text" ? "selected" : ""; ?> >Text</option>
									<option value="number" <?php echo $p2["peecjob_form_label"] == "number" ? "selected" : ""; ?> >Number</option>
									<option value="email" <?php echo $p2["peecjob_form_label"] == "email" ? "selected" : ""; ?> >Email</option>
									<option value="date" <?php echo $p2["peecjob_form_label"] == "date" ? "selected" : ""; ?> >Date</option>
									<option value="textarea" <?php echo $p2["peecjob_form_label"] == "textarea" ? "selected" : ""; ?> >Textarea</option>
									<option value="checkbox" <?php echo $p2["peecjob_form_label"] == "checkbox" ? "selected" : ""; ?> >Checkbox</option>
									<option value="radio" <?php echo $p2["peecjob_form_label"] == "radio" ? "selected" : ""; ?> >Radio</option>
									<option value="dropdown" <?php echo $p2["peecjob_form_label"] == "dropdown" ? "selected" : ""; ?> >Dropdown</option>
								</select>
							</div>
						</div>

						<!-- input required -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Required</label>
							<div class="col-xs-10">
								<input type="radio" class="input-required" name="input-radio-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-required-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" value="Yes" <?php echo $p2["peecjob_form_required"] == 1 ? "checked" : ""; ?>> Yes
								<input type="radio" class="input-required" name="input-radio-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-required-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" value="No" style="margin-left: 10px;" <?php echo $p2["peecjob_form_required"] == 0 ? "checked" : ""; ?>> No
							</div>
						</div>

						<!-- input enabled -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Enabled</label>
							<div class="col-xs-10">
								<input type="radio" class="input-status" name="input-status-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-enabled-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" value="Yes" <?php echo $p2["peecjob_form_status"] == 1 ? "checked" : ""; ?>> Yes
								<input type="radio" class="input-status" name="input-status-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-enabled-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" value="No" style="margin-left: 10px;" <?php echo $p2["peecjob_form_status"] == 0 ? "checked" : ""; ?>> No
							</div>
						</div>

						<!-- input short description -->
						<div class="form-group">
							<label class="col-xs-2 control-label">Short Description</label>
							<div class="col-xs-10">
								<textarea class="input-short-description" name="input-short-description-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-identifier="part-a-short-description-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>"><?php echo $p2["peecjob_form_short_description"]; ?></textarea>
							</div>
						</div>

						<!-- input checkbox, dropdown and radio choices -->
						<div <?php echo $p2["peecjob_form_type"] == "checkbox" || $p2["peecjob_form_type"] == "radio" || $p2["peecjob_form_type"] == "dropdown" ? '' : 'style="display: none;"'; ?> class="choices-container" data-identifier="part-a-choices-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>">
							<h4><strong>Choices:</strong></h4>
							<div class="choices-div" data-identifier="part-a-choices-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>" data-index="<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>">
								<?php
								$p2_choices = json_decode($p2["peecjob_form_choices"], true);
								if(!empty($p2_choices)) {
									foreach ($p2_choices as $p2c) {
										?>
										<div class="option">
											<div class="form-group">
												<label class="col-xs-2 control-label">Label</label>
												<div class="col-xs-10">
													<input type="text" class="label-text" value="<?php echo $p2c["label"]; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-2 control-label">Value</label>
												<div class="col-xs-10">
													<input type="text" class="value-text" value="<?php echo $p2c["value"]; ?>">
												</div>
											</div>
										</div>
										<hr style="border-color:#ccc;">
										<?php
									}
								}
								?>
							</div>
							<button type="button" class="add-choice btn btn-primary" data-container="part-a-choices-<?php echo $p2["peecjob_form_page"] . '-' . $i2; ?>">Add Choice</button>
						</div>

					</div>
					<?php
					$i2++;
				}
				?>
			</div>
			<div class="row" style="margin: 40px 0;">
				<button type="button" class="add-field btn btn-secondary" id="add-part-b-field" onclick="addfield('part-b-fields')">Add Field</button>
			</div>
			<input type="hidden" name="form_fields" id="form_fields">
			<input type="hidden" name="type" id="type" value="peecjob_form" />
			<input type="hidden" name="processType"  id="processType" value="">
			<input type="hidden" id="approval_remarks" name="approval_remarks" value="" />
			<input type="hidden" id="approval_launch_date" name="approval_launch_date" value="" />
		</form>

		<div class="row" style="padding-top:10px;padding-bottom:10px">
			<div class="col-xs-12" style="text-align:right;padding-right:0px;">
				<button type="button" class="btn btn-primary" onclick="checkForm(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save
				</button>
				<!-- <button type="button" class="btn btn-primary" onclick="checkForm2(); return false;">
					<i class="fa fa-floppy-o fa-lg"></i> Save &amp; Submit for Approval
				</button> -->
			</div>
		</div>

	</div>


	<style>
		button.add-field {
			background: burlywood;
			color: #fff;
		}
		div.part-fields {
			display: flex;
			flex-flow: wrap;
		}
		.input-field-row {
			width: 100%;
			display: flex;
			flex-flow: wrap;
		}
		.input-field-row .form-group {
			display: flex;
			align-items: center;
		}
		.input-field-row .form-group select, .input-field-row .form-group input[type="text"] {
			height: 30px;
			min-width: 200px;
			padding: 0 10px;
		}
		.input-field-row > * {
			width: 100%;
		}
		label.col-xs-2.control-label {
			margin-bottom: 0;
		}
		.input-field-row .form-group textarea {
			min-width: 200px;
			min-height: 100px;
			padding: 10px;
		}
	</style>
	<script>

		function addfield(container) {

			var count1 = jQuery('div#' + container + ' div.input-field-row').length;
			var index1 = count1 + 1;
			var html = '';

			html += '<div class="input-field-row" data-index="' + index1 + '">';

			html += '<h4><strong>Question: ' + index1 + '</strong></h4>';

			// input label
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Label</label>';
			html += '<div class="col-xs-10">';
			html += '<input type="text" class="input-label" name="input-label-' + index1 + '" data-identifier="part-a-label-' + index1 + '" data-index="' + index1 + '">';
			html += '</div>';
			html += '</div>';

			// input type
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Type</label>';
			html += '<div class="col-xs-10">';
			html += '<select class="input-type" name="input-type-' + index1 + '" data-identifier="part-a-type-' + index1 + '" data-index="' + index1 + '">';
			html += '<option value="text">Text</option>';
			html += '<option value="number">Number</option>';
			html += '<option value="email">Email</option>';
			html += '<option value="date">Date</option>';
			html += '<option value="textarea">Textarea</option>';
			html += '<option value="checkbox">Checkbox</option>';
			html += '<option value="radio">Radio</option>';
			html += '<option value="dropdown">Dropdown</option>';
			html += '</select>';
			html += '</div>';
			html += '</div>';

			// input required
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Required</label>';
			html += '<div class="col-xs-10">';
			html += '<input type="radio" class="input-required" name="input-radio-' + index1 + '" data-identifier="part-a-required-' + index1 + '" data-index="' + index1 + '" value="Yes" checked> Yes';
			html += '<input type="radio" class="input-required" name="input-radio-' + index1 + '" data-identifier="part-a-required-' + index1 + '" data-index="' + index1 + '" value="No" style="margin-left: 10px;"> No';
			html += '</div>';
			html += '</div>';

			// input enabled
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Enabled</label>';
			html += '<div class="col-xs-10">';
			html += '<input type="radio" class="input-status" name="input-status-' + index1 + '" data-identifier="part-a-enabled-' + index1 + '" data-index="' + index1 + '" value="Yes" checked> Yes';
			html += '<input type="radio" class="input-status" name="input-status-' + index1 + '" data-identifier="part-a-enabled-' + index1 + '" data-index="' + index1 + '" value="No" style="margin-left: 10px;"> No';
			html += '</div>';
			html += '</div>';

			// input short description
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Short Description</label>';
			html += '<div class="col-xs-10">';
			html += '<textarea class="input-short-description" name="input-short-description-' + index1 + '" data-identifier="part-a-short-description-' + index1 + '" data-index="' + index1 + '"></textarea>';
			html += '</div>';
			html += '</div>';

			// input checkbox, dropdown and radio choices
			html += '<div style="display: none;" class="choices-container" data-identifier="part-a-choices-' + index1 + '" data-index="' + index1 + '">';
			html += '<h4><strong>Choices:</strong></h4>';
			html += '<div class="choices-div" data-identifier="part-a-choices-' + index1 + '" data-index="' + index1 + '"></div>';
			html += '<button type="button" class="add-choice btn btn-primary" data-container="part-a-choices-' + index1 + '">Add Choice</button>';
			html += '</div>';

			html += '</div>';

			jQuery(html).appendTo('div#' + container + '');
		}

		// add choice
		jQuery(document).on("click","button.add-choice",function(e) {
			e.preventDefault();
			var btn = jQuery(this);
			var container = btn.attr('data-container')
			var html = '';
			html += '<div class="option">';
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Label</label>';
			html += '<div class="col-xs-10">';
			html += '<input type="text" class="label-text">';
			html += '</div>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label class="col-xs-2 control-label">Value</label>';
			html += '<div class="col-xs-10">';
			html += '<input type="text" class="value-text">';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '<hr style="border-color:#ccc;">';
			jQuery(html).appendTo('div.choices-div[data-identifier="' + container + '"]');
		});

		jQuery(document).on("change","select.input-type",function(e) {
			var sel = jQuery(this)
			var val = sel.val()
			var container = sel.closest('div.input-field-row').find('div.choices-container')
			if( val == "checkbox" || val == "radio" || val == "dropdown" ) {
				container.show()
			}
			else {
				container.hide()
			}
		});

		jQuery(document).on("keyup","input.value-text",function(e) {
			var new_val = jQuery(this).val().replace(/\s/g, "-").toLowerCase();
			jQuery(this).val(new_val);
		});

		function generateFieldsJSON() {
			var fd = [];

			var page = 1;
			jQuery('div.part-fields').each(function() {
				var pf = jQuery(this);
				var arr = [];
				pf.find('div.input-field-row').each(function() {
					var fr = jQuery(this);		
					var json = {};

					var label = fr.find('input.input-label').val()
					var type = fr.find('select.input-type').val()
					var required = fr.find('input.input-required:checked').val()
					var status = fr.find('input.input-status:checked').val()
					var short_description = fr.find('textarea.input-short-description').val()
					var choices = [];
					if( type == "checkbox" || type == "radio" || type == "dropdown" ) {
						fr.find('div.choices-div div.option').each(function() {
							var opt = jQuery(this);
							var choice = {};
							choice.label = opt.find('input.label-text').val()
							choice.value = opt.find('input.value-text').val()
							choices.push(choice)
						});
					}

					json.page = page;
					json.label = label;
					json.type = type;
					json.required = required;
					json.status = status;
					json.short_description = short_description;
					json.choices = choices;

					arr.push(json)
				});
				page++;
				fd.push(arr)
			});

			var sfd = JSON.stringify(fd);
			console.log(sfd)
			jQuery('input#form_fields').val(sfd);
		}
	</script>

	<!-- End page content -->
	<?php include_once "../footer.php" ?>
</body>
</html>