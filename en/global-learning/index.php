﻿<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "Global Learning";
$section = 'international';
$subsection = 'international';
$sub_nav = 'international';

$breadcrumb_arr['Global Learning'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$exchange_programme_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'P'));
$incoming_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'I'));
$outgoing_student_list = DM::findAllWithLimit($DB_STATUS.'international', 3,'international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'O'));
$scholarship_list = DM::findAll($DB_STATUS.'international','international_status = ? AND international_type = ?', 'international_seq DESC', array(STATUS_ENABLE, 'S'));


$global_learning = DM::findOne($DB_STATUS.'global_learning','global_learning_id = ?', '', array("1"));
$global_learning_detail_lang1 = $global_learning["global_learning_detail_lang1"];


$your_support_matters = DM::findOne($DB_STATUS.'your_support_matters','your_support_matters_id = ?', '', array("1"));
$your_support_matters_before_lang1 = $your_support_matters["your_support_matters_before_lang1"];
$your_support_matters_detail_lang1 = $your_support_matters["your_support_matters_detail_lang1"];

$scholarship = DM::findOne($DB_STATUS.'scholarship','scholarship_id = ?', '', array("1"));
$scholarship_detail_lang1 = $scholarship["scholarship_detail_lang1"];

$international_academic_partners = DM::findAll($DB_STATUS.'international_academic_partners','international_academic_partners_status = ?', 'international_academic_partners_id ASC', array(STATUS_ENABLE));

//$scholarship_list = DM::findAll($DB_STATUS.'international_image', 'international_image_status = ? AND international_image_pid = ?', 'international_image_seq', array(STATUS_ENABLE, '1'));

$master_lecture_list = DM::findAll($DB_STATUS.'master_lecture', 'master_lecture_status = ?', 'master_lecture_seq', array(STATUS_ENABLE));
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/international.css" type="text/css" />

	<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
	<style>
		.programme-project .hidden-block h3:after{
			display:none;
		}
		.underline-link{
			text-decoration: underline !important;
		}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access">
		<?php echo $page_title?>
	</h1>
	<main>
		<div class="top-divide-line"></div>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="<?php echo $subsection ?>">
			<section class="international-info">
				<h2>Global Learning</h2>
				<?php echo $global_learning_detail_lang1; ?>
			</section>
			<section id="international-student-exchange-programme" class="gallery-exhib">

				<section class="international-info" style="padding-top:0;z-index: 30;">
					<h2 style="margin:0 0 30px;">
						Sharing from
						<span>Incoming Students</span>
					</h2>
					<p>If your home institution has a <a href="javascript:scrollToElemTop($('#international-academic-partners'), 300);" class="underline-link">Memorandum of Understanding and/or an exchange agreement</a> with us, you are welcome to apply for academic exchange at HKDI for one semester.  Check out our <a href="http://www.hkdi.edu.hk/images/global-learning/Credit-bearingInboundExchangeFactsheet_AY1920.pdf" class="underline-link" target="_blank">credit-bearing</a> and <a href="http://www.hkdi.edu.hk/images/global-learning/Non-credit-bearingInboundExchangeFactsheets_AY1920.pdf" class="underline-link" target="_blank">non-credit-bearing</a> schemes to learn more about our incoming exchange programme.</p>
				</section>
				<div class="video-bg">
					<div class="video-bg__holder">
						<?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
				<div class="gallery-exhib__wrapper">
					<div class="cards cards--3-for-dt">
						<div class="cards__outer">
							<div class="cards__inner">
								<?php foreach ($exchange_programme_list AS $exchange_programme) { ?>
									<div class="card is-collapsed" style="margin-top: 0!important;">
										<div class="card__inner ani">
											<div class="card__img">
												<?php if ($exchange_programme['international_image'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_image'])) { ?>
													<img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_image']; ?>" alt="" />
												<?php } ?>
											</div>
											<div class="card__ppl">
												<?php if ($exchange_programme['international_thumb'] != '' && file_exists($international_path.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb'])) { ?>
													<div class="card__ppl-pic">
														<img src="<?php echo $international_url.$exchange_programme['international_id'].'/'.$exchange_programme['international_thumb']; ?>" alt="" />
													</div>
												<?php } ?>

												<div class="card__ppl-txt">
													<strong class="card__ppl-name"><?php echo $exchange_programme['international_name_'.$lang]; ?></strong>
													<!--<p class="card__ppl-title"></p>-->
												</div>
											</div>
											<div class="card__content">
												<p class="desc"><?php echo $exchange_programme['international_desc_'.$lang]; ?></p>
												<a href="exchange-programme-details.php?international_id=<?php echo $exchange_programme['international_id']; ?>" class="btn">More</a>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="sharing-from-outgoing-students" class="gallery-exhib">

				<section class="international-info" style="padding-top:0;">
					<h2 style="margin:0 0 30px;">Sharing from
						<span>Outgoing Students</span>
					</h2>


					<p>Full-time HKDI students who are interested in the outgoing exchange programme may <a href="mailto:tonyliu829@vtc.edu.hk" class="underline-link">contact us</a> and refer to the list of travelling <a href="javascript:scrollToElemTop($('#scholarship'), 300);"><u>scholarship</u></a> for more information.</p>
				</section>

				<div class="gallery-exhib__wrapper">
					<div class="cards cards--3-for-dt">
						<div class="cards__outer">
							<div class="cards__inner">
								<?php foreach ($outgoing_student_list AS $outgoing_student) { ?>
									<div class="card is-collapsed" style="margin-top: 0!important;">
										<div class="card__inner ani">
											<div class="card__img">
												<?php if ($outgoing_student['international_image'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_image'])) { ?>
													<img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_image']; ?>" alt="" />
												<?php } ?>
											</div>
											<div class="card__ppl">
												<?php if ($outgoing_student['international_thumb'] != '' && file_exists($international_path.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb'])) { ?>
													<div class="card__ppl-pic">
														<img src="<?php echo $international_url.$outgoing_student['international_id'].'/'.$outgoing_student['international_thumb']; ?>" alt="" />
													</div>
												<?php } ?>
												<div class="card__ppl-txt">
													<strong class="card__ppl-name"><?php echo $outgoing_student['international_name_'.$lang]; ?></strong>
													<p class="card__ppl-title"><?php echo $outgoing_student['international_job_title_'.$lang]; ?></p>
												</div>
											</div>
											<div class="card__content">
												<p class="desc"><?php echo $outgoing_student['international_desc_'.$lang]; ?></p>
												<a href="outgoing-student-details.php?international_id=<?php echo $outgoing_student['international_id']; ?>" class="btn">More</a>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="international-academic-partners" class="international-academic-partners">
				<div class="inner">
					<h2>International Academic Partners</h2>
					<?php
					foreach($international_academic_partners as $iap) {
						?>
						<section class="programme-project ani" style="width:100%; margin-bottom:30px;">
							<div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
								<h3 class="detail init-detail"><?php echo $iap["international_academic_partners_name_lang1"]; ?></h3>
								<div class="hidden">

									<?php

									$iap_sections = DM::findAll($DB_STATUS.'international_academic_partners_section', 'international_academic_partners_id = ? AND international_academic_partners_section_status = ?', 'international_academic_partners_section_id ASC', array($iap["international_academic_partners_id"],STATUS_ENABLE));

									foreach ($iap_sections as $iaps) {

									?>

										<h4 class="programme-country"><?php echo $iaps["international_academic_partners_section_name_lang1"]; ?></h4>
										<?php 
										$iaps_detail = str_replace("p>", "li>", $iaps["international_academic_partners_section_detail_lang1"]);

										?>
										<ul class="init-ul">
											<?php echo $iaps_detail; ?> 
										</ul>

									<?php

									}

									?>
								
								</div>
							</div>
						</section>
						<?php
					}
					?>
				</div>
			</section>

			<section id="scholarship" class="scholarship">
				<div class="inner">
					<h2>Scholarship and Donation</h2>
					<div class="inner_content">
						<h3>Your Support Matters</h3>
						<p><?php echo $your_support_matters_before_lang1; ?></p>
					</div>

					<section class="programme-project ani" style="width:100%; margin-bottom:30px;">
						<div class="hidden-block" style="border:0;color:#FFF;padding: 0; animation:none;opacity:1;">
							<h3 style="margin:0;color:#FFF;padding: 0;font-size:16px;" class="detail">+ Details</h3>
							<div class="hidden">
								<?php echo $your_support_matters_detail_lang1; ?>
							</div>
						</div>
					</section>

					<div class="inner_content">
						<h3>Scholarship</h3>
						<p><?php echo $scholarship_detail_lang1; ?></p>
					</div>
					<?php foreach ($scholarship_list AS $scholarship) { ?>
						<?php
						if($scholarship['international_name_'.$lang] && !$scholarship['international_desc_'.$lang]){
							?>
							<div style="margin-top: 15px;">
								<h3 style="color: #FFF;"><?php echo $scholarship['international_name_'.$lang]?></h3>
							</div>
							<?php
						} else {
							?>
							<div class="scholarship-block">
								<div class="txt">

									<p ><strong><?php echo $scholarship['international_name_'.$lang]?></strong></p>
									<p><?php echo nl2br($scholarship['international_desc_'.$lang]); ?></p>
								</div>
							</div>
							<?php
						}
						?>
					<?php } ?>

				</div>
			</section>
			<section id="master-lecture-series" class="gallery-exhib profile">
				<section class="international-info">
					<h2 style="margin-left:0; margin-right:0; margin-bottom:30px;">Master Lecture
						<span>Series</span>
					</h2>
					<p>HKDI regularly invites design masters, academics and professionals from various design disciplines around the world to share with our students their inspiration, experience, perspectives and cultural insights.</p>
				</section>
				<div class="gallery-exhib__wrapper">
					<div class="cards cards--4-for-dt">
						<div class="cards__outer">
							<div class="cards__inner">
								<?php foreach ($master_lecture_list AS $master_lecture) { ?>
									<div class="card is-collapsed"  style="margin-top:0; margin-bottom: 60px;">
										<div class="card__inner">
											<div class="card__content" style="margin-top:0;">
												<a href="#" class="btn js-expander">
													<?php 
													if (file_exists($master_lecture_path.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'])) {
														$ml_image = $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'];
													}
													else if(file_exists(str_replace("uploaded_files","CMS/uploaded_files",$master_lecture_path).$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'])) {
														$ml_image = str_replace("uploaded_files","CMS/uploaded_files",$master_lecture_url).$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'];
													}
													?>
													<img src="<?php echo $ml_image; ?>" alt="" />
													<h4><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h4>
													<p><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
												</a>
											</div>
										</div>
										<div class="card__expander">
											<div class="profile-block">
												<div class="profile-block__detail">
													<div class="img-holder">
														<?php 
														if (file_exists($master_lecture_path.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'])) {
															$ml_image = $master_lecture_url.$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'];
														}
														else if(file_exists(str_replace("uploaded_files","CMS/uploaded_files",$master_lecture_path).$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'])) {
															$ml_image = str_replace("uploaded_files","CMS/uploaded_files",$master_lecture_url).$master_lecture['master_lecture_id'].'/'.$master_lecture['master_lecture_image'];
														}
														?>
														<img src="<?php echo $ml_image; ?>" alt="" />
													</div>
													<div class="text">
														<h3 class="profile-block__name"><?php echo $master_lecture['master_lecture_name_'.$lang]; ?></h3>
														<p class="profile-block__title"><?php echo $master_lecture['master_lecture_job_title_'.$lang]; ?></p>
														<div class="profile-block__desc">
															<p><?php echo $master_lecture['master_lecture_detail_'.$lang]; ?></p>
														</div>
													</div>
												</div>
												<?php $master_lecture_images = DM::findAll($DB_STATUS.'master_lecture_image', 'master_lecture_image_status = ? AND master_lecture_image_pid = ?', 'master_lecture_image_seq', array(STATUS_ENABLE, $master_lecture['master_lecture_id'])); ?>
												<div class="profile-block__gallery">
													<div class="profile-block__gallery-slider swiper-container-fade swiper-container-horizontal swiper-container-free-mode swiper-container-ios">
														<div class="swiper-wrapper" style="transition-duration: 0ms;">

															<?php foreach ($master_lecture_images AS $master_lecture_image) { ?>
																<div class="profile-block__slide swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 361px; transition-duration: 0ms; opacity: 1; transform: translate3d(-361px, 0px, 0px);">
																	<img src="<?php echo $master_lecture_url.$master_lecture_image['master_lecture_image_pid'].'/'.$master_lecture_image['master_lecture_image_id'].'/'.$master_lecture_image['master_lecture_image_filename']; ?>" alt="<?php echo $master_lecture_image['master_lecture_image_alt']; ?>" />
																</div>
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="fp-nav" class="custom-sidenav right">
			<ul>
				<li>
					<a href="#international-student-exchange-programme">
						<span></span>
					</a>
					<div class="fp-tooltip right">Sharing from Incoming Students</div>
				</li>
				<li>
					<a href="#sharing-from-outgoing-students">
						<span></span>
					</a>
					<div class="fp-tooltip right">Sharing from Outgoing Students:</div>
				</li>
				<li>
					<a href="#international-academic-partners">
						<span></span>
					</a>
					<div class="fp-tooltip right">International academic partners</div>
				</li>
				<li>
					<a href="#scholarship">
						<span></span>
					</a>
					<div class="fp-tooltip right">Scholarship</div>
				</li>
				<li>
					<a href="#master-lecture-series">
						<span></span>
					</a>
					<div class="fp-tooltip right">Master Lecture Series</div>
				</li>
			</ul>
		</div>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

</html>
