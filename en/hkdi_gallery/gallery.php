<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'HKDI Gallery';
$section = 'gallery';
$subsection = '';
$sub_nav = '';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

// $product_id = @$_REQUEST['product_id'] ?? '';
// $product_slug = @$_REQUEST['product_slug'] ?? '';
$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$product_slug = array_pop($uriSegments);
// $product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_id = ?', '', array(STATUS_ENABLE, $product_id));
$product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_slug = ?', '', array(STATUS_ENABLE, $product_slug));
$product_id = $product["product_id"] ?? 0;

if($product['product_type'] !='4' || !$product){
    header("location: index.php");
    exit();
}


$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, $product_id));

$lat = '22.3057314';
$lng = '114.2512759';
foreach ($product_sections AS $product_section)
{
	if ($product_section['product_section_type'] == 'Visit')
	{
		$lat = $product_section['product_lat'];
		$lng = $product_section['product_lng'];
	}
}

function GenTitle($title){
    $title_arr = explode(" ", $title);
    $count_arr = count($title_arr);

    $h2_title = str_replace($title_arr[$count_arr-1],"",$title);

    if($count_arr ==1){
        return "<h2>".$title."</h2>";
    }else{
        return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
    }

}


$breadcrumb_arr['HKDI Gallery'] =$host_name_with_lang.'hkdi_gallery/';
$breadcrumb_arr[$product['product_name_'.$lang]] ='';

$product_background_color = $product['product_background_color'];
$product_text_color = $product['product_text_color'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
        <!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN""></script>-->
        <script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
        <script>
            $(document).ready(function () {
                //initMap();
                var mySwiper = new Swiper ('.swiper-container', {
					pagination: {
    					el: '.swiper-pagination',
    					type: 'bullets',
  					},
					paginationClickable: true,
					autoHeight: true
				});
            });

            function initMap() {
            	var stylez = [{
      				featureType: "all",
      				elementType: "all",
      				stylers: [
        				{ saturation: -100 }
      				]
    			}];

    			var imageTag = '<?php echo $host_name?>images/common/locator.png';
    			var tag = new google.maps.MarkerImage(
		            imageTag,
		            null, /* size is determined at runtime */
		            null, /* origin is 0,0 */
		            null, /* anchor is bottom center of the scaled image */
		            new google.maps.Size(50, 73)
		            );

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                    center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),

                    styles: stylez
                });
    
                var marker;
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
                    map: map,
                    icon: tag
                });
            }
        </script>
		<style>
		.page-gallery .inner-top-banner .sub-menu{
			background:<?php echo $product_background_color?>;
			color:<?php echo $product_text_color?>;
		}
		.inner-top-banner .sub-menu a{
			color:<?php echo $product_text_color?>;
		}
		.page-gallery .photos .col a:nth-child(3):after,
		.page-gallery .photos .col a:nth-child(5):before{
			background-color:<?php echo $product_background_color?>;
		}
.page-gallery .access-map h2,
		.page-gallery .photos h2 span,
		.page-gallery .about h2 span{
			color:<?php echo $product_background_color?>;
		}
		</style>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
        	<div class="top-divide-line"></div>
        	<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
        	<div class="top-nav">
            	<div class="top-nav-left">
            		<!--<a class="back" href="index.php">Back</a>-->
            	</div>
                <div class="top-nav-center"><h4 class="img-heading"><img src="<?php echo "//".$_SERVER['HTTP_HOST']."/" ?>images/gallery/hkdi-gallery.svg" /></h4></div>
                <div class="top-nav-right"><a class="border-btn" href="http://www.facebook.com/hkdi.gallery">HKDI Gallery Facebook Page</a></div>
            </div>
                <section class="inner-top-banner">
			<?php if ($product['product_banner'] != '' && file_exists($product_path.$product['product_id'].'/'.$product['product_banner'])) { ?>
                	<div class="banner-visual"><img src="<?php echo $product_url.$product['product_id'].'/'.$product['product_banner']; ?>" /></div>
			<?php } ?>
                    <nav class="sub-menu">
                    	<div>
				<?php foreach ($product_sections AS $product_section) { ?>
				<?php 	if ($product_section['product_section_type'] == 'CT' || $product_section['product_section_type'] == 'Acknowledgement') { continue; } ?>
				<a href="#section-<?php echo $product_section['product_section_id']; ?>"><?php echo $product_section['product_section_name_'.$lang]; ?></a>
				<?php } ?>
                    	</div>
                    </nav>
                </section>
		
		<?php for ($i = 0; $i < sizeof($product_sections); $i++) { ?>
		<?php	$product_section = $product_sections[$i]; ?>
		<?php 	if ($product_section['product_section_type'] == 'T') { ?>
		<div style="padding:0.5px 0;background-color: <?php echo $product_section['product_section_background_color']?>;">
		<section class="about section-<?php echo $product_section['product_section_id']; ?>">
			<?php echo GenTitle($product_section['product_section_title_'.$lang]);?>
			<?php echo $product_section['product_section_content_'.$lang]; ?>
		</section>
		</div>
		<?php generateBlock($product_sections, $i); ?>
		<?php	} else if ($product_section['product_section_type'] == 'Photo') { ?>
		<?php		$photos = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $product_section['product_section_id'])); ?>
		<section class="photos section-<?php echo $product_section['product_section_id']; ?>">
                	<?php echo GenTitle($product_section['product_section_title_'.$lang]);?>
                        <div class="swiper-container">
                        	<div class="swiper-wrapper">
					<?php $photo_count = 0; ?>
					<?php foreach ($photos AS $photo) { ?>
					<?php	if ($photo_count % 8 == 0) { ?>
					<div class="swiper-slide col">
					<?php	} ?>
					<a href="<?php echo $product_file_url.$photo['product_section_file_pid'].'/'.$photo['product_section_file_id'].'/'.$photo['product_section_file_filename']; ?>"><img src="<?php echo $product_file_url.$photo['product_section_file_pid'].'/'.$photo['product_section_file_id'].'/'.'crop_'.$photo['product_section_file_filename']; ?>" /></a>

					<?php	if ($photo_count % 8 == 7) { ?>
					</div>
					<?php 	} ?>
					<?php	$photo_count++; ?>
					<?php } ?>
    				</div>
                            
                            <div class="swiper-pagination"></div>
                        </div>
                </section>
		<!--<div class="acknowledgement"><?php generateBlock($product_sections, $i); ?></div>-->
		<?php	} else if ($product_section['product_section_type'] == 'Visit') { ?>
		<section class="access-map section-<?php echo $product_section['product_section_id']; ?>">
			<div class="access-info">
				<?php echo GenTitle($product_section['product_section_title_'.$lang]);?>
				<?php echo $product_section['product_section_content_'.$lang]; ?>
			</div>
                	<div id="map" class="map">
                		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.2681261428024!2d114.25116361467325!3d22.305697185320742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340403efc89ad0bd%3A0xd45da19c45e22088!2sHKDI%20Gallery!5e0!3m2!1sen!2s!4v1643109476328!5m2!1sen!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                	</div>
                </section>
		<!--<div class="acknowledgement"><?php generateBlock($product_sections, $i); ?></div>-->
		<?php 	} ?>
		<?php } ?>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>

<?php
function generateBlock($product_sections, $loopI)
{
	global $lang, $product_file_url;
	for ($i = ($loopI + 1); $i < sizeof($product_sections); $i++) {
		$product_section = $product_sections[$i];
		if ($product_section['product_section_type'] == 'Acknowledgement') {
			$product_section_files = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $product_section['product_section_id']));
?>
                	<div class="block">
	                        <p><?php echo $product_section['product_section_title_'.$lang]; ?></p>
				<?php foreach ($product_section_files AS $product_section_file) { ?>
	                        <img src="<?php echo $product_file_url.$product_section_file['product_section_file_pid'].'/'.$product_section_file['product_section_file_id'].'/'.$product_section_file['product_section_file_filename']; ?>" />
				<?php } ?>
                	</div>
<?php		} else if ($product_section['product_section_type'] == 'CT') { ?>
			<div class="hidden-block">
				<h3><?php echo $product_section['product_section_title_'.$lang]; ?></h3>
				<div class="hidden">
					<?php echo $product_section['product_section_content_'.$lang]; ?>			
				</div>
			</div>
<?php		} else { 
			break;
		}
	}
}
?>
