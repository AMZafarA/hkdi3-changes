<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'HKDI Gallery';
$section = 'gallery';
$subsection = 'alumni';
$sub_nav = 'alumni';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$exhib_space_arr['20'] = 'hkdi-gallery';
$exhib_space_arr['21'] = 'd-mart';
$exhib_space_arr['22'] = 'exp-center';

$highlight_show = 5;

$gallery_highlight_list = DM::findAllWithLimit($DB_STATUS.'product', $highlight_show,'product_status = ? AND product_type = ? and product_index_show = ?', 'product_seq desc', array(STATUS_ENABLE, '4',STATUS_ENABLE));


$breadcrumb_arr['HKDI Gallery'] ='';

?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>

		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN" "></script>
        	<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js "></script>
		<script>
			$(document).ready(function() {
				initMap();
			});

			function initMap() {

				var mapStyles = [{
						"stylers": [{
							"saturation": -100
						}]
					},
					{
						"featureType": "water",
						"stylers": [{
								"lightness": -30
							},
							{
								"gamma": 0.91
							}
						]
					}
				];



				<?php
					foreach ($exhib_space_arr as $key => $value) {
						$result_v = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'Visit'));
						$product_lng = $result_v['product_lng'];
						$product_lat = $result_v['product_lat'];

						if($product_lng && $product_lat){

				?>
				var map2 = new google.maps.Map(document.getElementById('map'), {
					zoom: 18,
					center: new google.maps.LatLng(<?php echo $product_lat?>, <?php echo $product_lng?>),
					styles: mapStyles
				});
				var marker2;
				marker2 = new google.maps.Marker({
					position: new google.maps.LatLng(<?php echo $product_lat?>, <?php echo $product_lng?>),
					map: map2
				});
				<?php } } ?>

			}
		</script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>HKDI Gallery</strong>
								<br>Presents</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">The HKDI Gallery promotes design education in Hong Kong by hosting international exhibitions of the highest quality and serves as a hub for the exploration of design.</p>
						<hr>
						<a href="http://www.facebook.com/hkdi.gallery" class="btn">HKDI Gallery Facebook Page ></a>
					</div>
				</div>
			</div>
			<section class="gallery-exhib">
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
				<div class="gallery-exhib__wrapper">
					<h3 class="gallery-exhib__title ani">
						<strong>20/21</strong>
<?php
/*
<strong><?php echo date('y')-1?>/<?php echo date('y')?></strong>
*/
?>
						<br>Exhibitions
					</h3>
					<div class="gallery-exhib__items ani">
						<?php
							foreach ($gallery_highlight_list AS $gallery) {
						?>
						<a href="gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="gallery-exhib__item">
							<img src="<?php echo $product_url.$gallery['product_id'].'/'.$gallery['product_thumb_banner']; ?>" alt="" />
							<div class="gallery-exhib__item-txt">
								<h4 class="gallery-exhib__item-title"><?php echo $gallery['product_name_'.$lang]; ?></h4>
								<time class="gallery-exhib__item-date"><?php echo date('j.n.y', strtotime($gallery['product_from_date'])); ?> - <?php echo date('j.n.y', strtotime($gallery['product_to_date'])); ?></time>
							</div>
						</a>
						<?php } ?>
						<!--
						<div class="gallery-exhib__item">
							<img src="<?php echo $img_url?>gallery/img-gallery-exhibition-4.jpg" alt="" />
							<div class="gallery-exhib__item-txt">
								<h4 class="gallery-exhib__item-title">Japanese Poster Artists – Cherry Blossom and Asceticism</h4>
								<time class="gallery-exhib__item-date">26.8.16 - 2.2.17</time>
							</div>
						</div>
						-->
					</div>
				</div>
			</section>

			<section class="exhib-space">
				<div class="exhib-space__intro is-active">
					<div class="exhib-space__wrapper">
						<div class="page-head__title">
							<h2>
								<strong>Exhibition Space</strong>
							</h2>
						</div>
						<div class="exhib-space__portal">
							<a href="#exhib-space--hkdi-gallery" class="exhib-space__portal-btn">HKDI Gallery ></a>
							<a href="#exhib-space--d-mart" class="exhib-space__portal-btn">d-Mart ></a>
							<a href="#exhib-space--exp-center" class="exhib-space__portal-btn">Experience Center ></a>
						</div>
					</div>
				</div>

				<?php
					foreach ($exhib_space_arr as $key => $value) {
						$result = DM::findOne($DB_STATUS.'product', ' product_id = ?', " ", array($key));
						$result_t = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'T'));
						$result_v = DM::findOne($DB_STATUS.'product_section', ' product_section_pid = ? and product_section_type = ?', " ", array($key,'Visit'));
						$product_lng = $result_v['product_lng'];
						$product_lat = $result_v['product_lat'];

						$product_id = $result['product_id'];
						$image1 = $result['product_banner'];
						$image2 = $result['product_thumb_banner'];

						if ($image1 != '' && file_exists($product_path.$product_id.'/'.$image1)){
							$image1 = $product_url.$product_id.'/'.$image1;
						}

						if ($image2 != '' && file_exists($product_path.$product_id.'/'.$image2)){
							$image2 = $product_url.$product_id.'/'.$image2;
						}

				?>
				<div id="exhib-space--<?php echo $value?>" class="exhib-space__content-holder">
					<div class="exhib-space__wrapper">
						<div class="exhib-space__control">
							<a href="#" class="exhib-space__back-btn">&lt;&nbsp;Back</a>
						</div>
						<div class="exhib-space__content">
							<div class="exhib-space__loc-item exhib-space__detail">
								<?php echo $result_t['product_section_content_'.$lang]?>
							</div>
							<?php if ($image1){?>
							<div class="exhib-space__loc-item">
								<img src="<?php echo $image1?>" alt="" />
							</div>
							<?php } ?>

							<?php if($product_lng && $product_lat){ ?>
							<div class="exhib-space__loc-item">
								<div class="exhib-space__map-holder">
									<!--img class="exhib-space__map-placeholder" src="<?php echo $img_url?>gallery/img-gallery-map-1.jpg" alt="" /-->
									<div id="exhib-space-map-<?php echo $value?>" class="exhib-space__map"></div>
								</div>
							</div>
							<?php } ?>

							<?php if ($image2){?>
							<div class="exhib-space__loc-item">
								<img src="<?php echo $image2?>" alt="" />
							</div>
							<?php } ?>
						</div>
						<!--<div class="exhib-space__control">
							<a href="#" class="btn btn--white">Enquiry</a>
						</div>-->
					</div>
				</div>
				<?php } ?>

			</section>
<?php
/*
			<section class="exhib-contact">
				<div class="exhib-contact__wrapper">
					<div class="sec-intro">
						<h2 class="sec-intro__title">
							<strong>Contact</strong> Us</h2>
					</div>
					<div class="contact-form">
						<div class="contact-form__info">
							<h3 class="contact-form__info-title">Contact</h3>
							<div class="contact-form__info-row">
								<div class="contact-form__info-txt">
									<p>Hong Kong Design Institute,
										<br>3 King Ling Road, Tseung Kwan O, HK</p>
								</div>
								<div class="contact-form__info-contact">
									<a href="mailto:hkdi-gallery@vtc.edu.hk" class="contact-form__contact-item">hkdi-gallery@vtc.edu.hk</a>
									<a href="tel:39282894" class="contact-form__contact-item">Tel 852-3928 2566</a>
								</div>
							</div>
						</div>
						<form class="contact-form__form form-container">

							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_name" placeHolder="Name" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_msg" placeHolder="Message" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<br>
										<div class="custom-checkbox">
											<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty " />
											<label for="contact_book"><strong>Book a Visit</strong></label>
										</div>
									</div>
								</div>
							</div>
							<div class="tobe-shown" data-trigger="#contact_book">
								<div class="field-row">
									<div class="field field--1-1 field--mb-1-1">
										<div class="field__holder">
											<div class="field__inline-txt">
												<strong>Proposed booking date(s)</strong>
											</div>
											<div class="field__date-fields">
												<div class="field__date-field">
													<div class="field__date-label">From:</div>
													<input type="text" name="booking_date_from" class="form-check--empty datepicker datepicker--booking" />
												</div>
												<div class="field__date-field">
													<div class="field__date-label">To:</div>
													<input type="text" name="booking_date_to" class="form-check--empty datepicker datepicker--booking" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="field-row">
									<div class="field field--1-2 field--mb-1-1">
										<div class="field__holder">
											<input type="text" name="booking_participants" placeHolder="Estimated number of participants" class="form-check--empty" />
										</div>
									</div>
								</div>
							</div>

							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="captcha">
											<div class="captcha__img">
												<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
											</div>
											<div class="captcha__control">
												<div class="captcha__input">
													<input type="text" name="captcha" class="form-check--empty" />
												</div>
												<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="btn-row">
								<div class="err-msg-holder">
									<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
									<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
								</div>
								<div class="btn-row btn-row--al-hl">
									<a href="#" class="btn btn-send">
										<strong>Send</strong>
									</a>
								</div>
							</div>
						</form>
						<div class="contact-form__success-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
						</div>
					</div>
				</div>
			</section>
*/
?>

			<section class="loc-map">
				<div class="loc-map__detail-holder">
					<div class="loc-map__detail">
						<h2 class="loc-map__title">Location</h2>
						<div class="loc-map__detail-items">
							<div class="loc-map__detail-item">
								<p class="loc-map__detail-decs">
									Hong Kong Design Institute,
									<br />3 King Ling Road, Tseung Kwan O, HK
								</p>
							</div>
						</div>
					</div>
				</div>
				<div id="map" class="loc-map__map-holder">
					<div id="map" class="loc-map__map"></div>
				</div>
			</section>
		</main>
		<?php //include $inc_lang_path . "inc_common/inc_form_err_msg.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
