<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$inc_root_path = '../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '';
$section = 'home';
$subsection = 'index';
$sub_nav = 'home';
$cat = 2;
$highlight_show = 5;
if (isset($_GET['cat'])) {
	$cat = $_GET['cat'];
}

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$DFS_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,'5'));

$AIP_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,'6'));

$CDM_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,'7'));

$FID_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,'8'));

$DDM_list = DM::findAll($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE,'22'));

$news_limit = 8;
$news_list = DM::findAllWithLimit($DB_STATUS.'news', $news_limit, 'news_status = ? AND news_index_show = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)', "news_date DESC", array(STATUS_ENABLE,'1',$today,$today));

$gallery_limit = 5;
$gallery_highlight_list = DM::findAllWithLimit($DB_STATUS.'product', $highlight_show,'product_status = ? AND product_type = ? and product_index_show = ?', 'product_seq desc', array(STATUS_ENABLE, '4',STATUS_ENABLE));
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
</head>
<body class="page-<?php echo $section ?> at-home-top" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>" data-fullpage-index="1">
	<?php include "./inc_common/inc_common_top.php"; ?>
	<?php include "./inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<div id="fullpage" class="fullpage">
			<?php include "./inc_home/inc_home_home-top.php"; ?>
			<?php if ($cat == 1) { ?>
				<?php include "./inc_home/inc_home_academic-programmes.php"; ?>
				<?php include "./inc_home/inc_home_knowledge-centres.php"; ?>
				<?php include "./inc_home/inc_home_latest-news.php"; ?>
				<?php include "./inc_home/inc_home_hkdi-gallery.php"; ?>
				<?php include "./inc_home/inc_home_home-others.php"; ?>
			<?php } else if ($cat == 2) { ?>
				<?php include "./inc_home/inc_home_knowledge-centres.php"; ?>
				<?php include "./inc_home/inc_home_academic-programmes.php"; ?>
				<?php include "./inc_home/inc_home_hkdi-gallery.php"; ?>
				<?php include "./inc_home/inc_home_latest-news.php"; ?>
				<?php include "./inc_home/inc_home_home-others.php"; ?>
			<?php } else if ($cat == 3) { ?>
				<?php include "./inc_home/inc_home_latest-news.php"; ?>
				<?php include "./inc_home/inc_home_hkdi-gallery.php"; ?>
				<?php include "./inc_home/inc_home_academic-programmes.php"; ?>
				<?php include "./inc_home/inc_home_knowledge-centres.php"; ?>
				<?php include "./inc_home/inc_home_home-others.php"; ?>
			<?php } ?>
			<div class="fullpage__footer section fp-auto-height">
				<?php include "./inc_common/inc_footer.php"; ?>
			</div>
		</div>
		<div class="video-bg">
			<div class="video-bg__holder">
				<?php include "./inc_common/inc_video-source.php"; ?>
			</div>
		</div>
	</main>
	<?php include "./inc_common/inc_common_bottom.php"; ?>

	<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">

		<div class="toast-header toast_1" onclick="javascript:$('.toast_1').slideUp();">

			<button type="button" class="ml-2 mb-1 close float-right toast_1" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="toast-body toast_1">
			<a href="https://www.hkdi.edu.hk/EDT2021"><img src="/images/home/image-home-toast-edt2021_en.jpg?<?php echo time();?>" style="margin-bottom: 5px;"/></a>

		</div>

		<div class="toast-header toast_2" onclick="javascript:$('.toast_2').slideUp();">

			<button type="button" class="ml-2 mb-1 close float-right" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="toast-body toast_2">

			<a href="https://www.vtc.edu.hk/admission/tc/"><img src="/images/home/tmp_b2.jpg"/></a>
		</div>


	</div>

	<script>

		var captureOutboundLink = function(url) {
			console.log("GA" + url);
			gtag('event', 'click', {
				'event_category': 'outbound',
				'event_label': url,
				'transport_type': 'beacon',
				'event_callback': function(){document.location = url;}
			});
		}
		$(function(){

			$('.ob_traffic').click(function(){
				captureOutboundLink($(this).prop('href'));
				return false;
			});
		});
	</script>

	<script>
		$(function(){
			setTimeout(function(){
				$('.toast_1').slideUp();
			}, 6000);
			setTimeout(function(){
				$('.toast_2').slideUp();
			}, 6500);
		});
	</script>

	<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">

		<div class="toast-header toast_1" onclick="javascript:$('.toast_1').slideUp();">

			<button type="button" class="ml-2 mb-1 close float-right toast_1" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="toast-body toast_1">
			<a href="https://www.hkdi.edu.hk/EDT2021"><img src="/images/home/image-home-toast-edt2021_en.jpg?<?php echo time();?>" style="margin-bottom: 5px;"/></a>

		</div>

		<div class="toast-header toast_2" onclick="javascript:$('.toast_2').slideUp();">

			<button type="button" class="ml-2 mb-1 close float-right" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="toast-body toast_2">

			<a href="https://www.vtc.edu.hk/admission/en/programme/s6/higher-diploma/#design" class="ob_traffic"><img src="/images/home/image-home-toast-admission_en.jpg"/></a>
		</div>


	</div>



	<style>
		.toast_2{
			display: none!important;
		}
		.toast {
			display: none!important;
			right: 0;
			top: 0px;
			position: absolute;
			z-index: 999;
			width: 60vh;
			max-width: 100%;
			font-size: .875rem;
			pointer-events: auto;
			background-color: rgba(255,255,255,.85);
			background-clip: padding-box;
			border: 1px solid rgba(0,0,0,.1);
			box-shadow: 0 0.5rem 1rem rgb(0 0 0 / 15%);
			border-radius: .25rem;
		}.toast-header {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-align: center;
			align-items: center;
			padding: .25rem .75rem;
			color: #6c757d;
			background-color: rgba(255,255,255,.25);
			background-clip: padding-box;
			border-bottom: 1px solid rgba(0,0,0,.05);
		}.toast-body {
			padding: .15rem;
		}button.close {

			background-color: transparent;
			border: 0;
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;

		}.close {
			float: right;
			font-size: 1.5rem;
			font-weight: 700;
			line-height: 1;
			color: #000;
			text-shadow: 0 1px 0 #fff;
			opacity: .5;
		}

		@media (max-width: 380px) {
			.toast {width: 100%;}
		}
		#onesplash{
			animation: appear .5s ease-in-out 1;
		}


		.splash{

			bottom: 20px;
			font-family: 'Graphik';
			letter-spacing: -.5vw;
			font-size: 7vw;
			line-height: 6.5vw;
			font-family: 'Graphik';
			font-weight: bold;
			color: white;

			margin-top: 22px;
			-webkit-animation: neon7 1.5s ease-in-out infinite alternate;
			-moz-animation: neon7 1.5s ease-in-out infinite alternate;
			animation: neon7 1.5s ease-in-out infinite alternate;

		}
		.splash span{
			color: #fad502;
		}
		@-webkit-keyframes appear {
			from {
				opacity: 0;
			}
			to {
				opacity: 1;
			}
		}
	</style>
</body>
</html>
