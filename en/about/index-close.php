<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'About';
$section = 'about';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$breadcrumb_arr['About Us'] =''; 
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
       		<link rel="stylesheet" href="<?php echo $host_name?>css/industrial-collaborations-details.css" type="text/css" />
		<script src="<?php echo $host_name?>js/about.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access">
			<?php echo $page_title?>
		</h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>About
								<strong>HKDI</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs page-tabs--no-bottom-pad">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-about-us" class="page-tabs__btn is-active">
							<span>About</span>
						</a>
						<a href="#tabs-about-campus" class="page-tabs__btn" id="btn-tabs-about-campus">
							<span>About Campus</span>
						</a>
						<a href="#tabs-publication" class="page-tabs__btn" id="btn-tabs-publication">
							<span>Publication</span>
						</a>
						<a href="#tabs-experience" class="page-tabs__btn" id="btn-tabs-experience">
							<span>Experience</span>
						</a>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__bg-deco"></div>
					<div class="page-tabs__wrapper">
						<div id="tabs-about-us" class="page-tabs__content is-active">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>About</strong> Us</h2>
										</div>
										<div class="txt-editor">
											<h4>The Hong Kong Design Institute (HKDI) is a leading design institution under the VTC Group. It provides high-quality education to cultivate knowledge and professionalism, and produces emerging talents who underpin the creative industries in Hong Kong.</h4>
											<p>With years of experience in design education, HKDI brings together the strengths of four design departments – Architecture, Interior and Product Design, Communication Design and Digital Media, Design Foundation Studies and Fashion and Image Design – to offer 20 design programmes. Highly flexible in articulation and advanced education, HKDI offer students a three-year university degree pathway from a Higher Diploma to Bachelor degrees offered by reputable universities in the UK.</p>
											<p>HKDI adopts a “think-and-do” approach through its contemporary curriculum and active collaborations with local and international academic and industry partners. Students are given opportunities to acquire hands-on experience and participate in global exchange programmes to expand their horizons while enhancing their creative thinking and social sensitivity. </p>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="about-campus">
													<strong>Award-winning Campus &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="publication">
													<strong>HKDI Publication &gt;</strong>
												</a>
											</div>
											<div class="btn-row btn-row--al-hl">
												<a href="#" class="btn tabs-trigger" data-tabs="experience">
													<strong>Experience Design &gt;</strong>
												</a>
											</div>
											<!--<div class="btn-row btn-row--al-hl">
												<a href="../edt/edt-info-day.php" class="btn">
													<strong>HKDI Info Day &gt;</strong>
												</a>
											</div>-->
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/about_image.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<div class="industrial-collaborations-details">
								<section class="membership">
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											<strong>Professional</strong> Membership</h2>
									</div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											Professional Organisations:</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ico-D_120x120.png" />
						                        <div class="txt">
						                            <p><strong>ICO-D</strong></p>
						                            <p>International Council of Design</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IDA_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IDA</strong></p>
						                            <p>International Design Alliance</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IFFTI_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFFTI </strong></p>
						                            <p>International Foundation of Fashion Technology Institutes</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/ifi_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IFI</strong></p>
						                            <p>International Federation of Interior Architects/Designers</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/IIID_120x120.png" />
						                        <div class="txt">
						                            <p><strong>IIID</strong></p>
						                            <p>International Institute of Information Design</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>TDAA</strong></p>
						                            <p>The Design Alliance Asia</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/WDO_120x120.png" />
						                        <div class="txt">
						                            <p><strong>WDO</strong></p>
						                            <p>World Design Organization</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											Academic Institutions:</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/Cumulus_120x120.png" />
						                        <div class="txt">
						                            <p><strong>CUMULUS</strong></p>
						                            <p>International Association of Universities and Colleges of Art, Design and Media</p>
						                        </div>
						                    </div>
									<div class="article-detail__head">
										<h2 class="article-detail__title">
											Research Organisation:</h2>
									</div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/DESIS_120x120.png" />
						                        <div class="txt">
						                            <p><strong>DESIS Network</strong></p>
						                            <p>Design for Social Innovation and Sustainability</p>
						                        </div>
						                    </div>
						                    <div class="membership-block">
						                        <img src="<?php echo $inc_root_path ?>images/about/publication/membership/blank.png" />
						                        <div class="txt">
						                            <p><strong>Design ED Asia</strong></p>
						                            <p>Asian Design Education</p>
						                        </div>
						                    </div>
								</section>
							</div>
							<section class="sec-basic sec-basic--white">
								<div class="page-tabs__bg"></div>
								<div class="txt-editor">
									<h3 class="txt-editor__title-light">
										<strong>Message from</strong>
										<br>VTC Executive Director</h3>
									<h4>
										<strong>Welcome to the Hong Kong Design Institute (HKDI) website. As a leading provider of design education in the region,
											the HKDI is committed to offering excellent learning opportunities to keep pace with the growing demand of the
											creative industries.</strong>
									</h4>
									<div class="txt-editor__grp">
										<h4>
											<strong>A Dynamic Learning Environment</strong>
										</h4>
										<p>The HKDI commenced the 2010/11 academic year in its award-winning new campus in Tiu Keng Leng. The campus is dedicated
											to providing the most dynamic learning environment for quality design education.</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>Creativity in Action</strong>
										</h4>
										<p>The HKDI’s comprehensive design programmes include activities to nurture your creative thinking and decision-making
											skills, cultural sensitivity and global perspective. The HKDI experience is based on a “think-and-do” approach
											that will allow you to develop your passion for design while imparting the skills and knowledge needed for success.</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>Quality Programmes and Student Support</strong>
										</h4>
										<p>Our curriculum is designed with input from industry leaders to ensure its relevance and responsiveness to industry
											developments. HKDI programmes are quality assured by the Hong Kong Council for the Accreditation of Academic and
											Vocational Qualifications.</p>
										<p>Our students and alumni are provided with career development advisory and support services. Through articulation
											arrangements with renowned overseas universities, HKDI Higher Diploma graduates can pursue Top-up Degree courses
											locally and attain degree qualifications within one or two years.</p>
									</div>
									<div class="txt-editor__grp">
										<h4>
											<strong>A Valued Choice to Success</strong>
										</h4>
										<p>Design can offer you a rewarding and fulfilling career. We look forward to having you as a member of the HKDI family.</p>
									</div>
									<br>
									<div class="txt-editor__grp">
										<h4>
											<strong>Mrs Carrie Yau, GBS, JP</strong>
										</h4>
										<p>
											<strong>Executive Director</strong>
											<br>Vocational Training Council</p>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-about-campus" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Design</strong> Concept</h2>
										</div>
										<div class="txt-editor">
											<h4>The HKDI campus is designed to provide an open and dynamic environment conducive to learning and innovative exploration. It is equipped with state-of-the-art workshops, galleries, fabrication facilities and a learning resources centre dedicated to creating the most dynamic learning environment for quality design education in the region.</h4>
											<p>Themed as the White Sheet, the campus design by French architects Coldéfy &amp; Associés was the award-winning project in the HKDI International Architectural Design Competition in 2006/2007.</p>
											<p>The White Sheet is a metaphorical presentation of creativity. It floats in the air and connects all other parts of the campus, expressing HKDI’s multidisciplinarity and objectives. The radical, light and transparent architectural design encourages reflection on the combination of multiple and opposite situations, allowing each functional element to be read immediately from the exterior.</p>
											<p>The entire composition, created by interpenetration between the interlinked elements, including the building base, the podium, the “sky city”, the towers and the landscaped roof, defines the Institute as a timeless building and reveals its objectives of promoting synergy, publicity and interactivity. Its enlightening design stimulates and inspires future designers.</p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-design-concept-1.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Education</strong> Facilities</h2>
										</div>
										<div class="txt-editor">
											<h4>HKDI houses over a hundred workshops and studios to cater for design students’ needs across disciplines. Four knowledge centres also advocate cross-disciplinary design education and encourage students’ participation in innovative and social research projects to push forward their design thinking.</h4>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cill.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Centre for Independent Language Learning (CILL)</h3>
													<p class="thumb-desc__desc">The Centre for Independent Language Learning creates an inviting and comfortable atmosphere for students to improve their language skills. It provides independent learning materials such as CD-ROMs, DVDs, books, magazines and board games, together with a flexible setting for hosting activities ranging from workshops and parties to performances and competitions.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_ml.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Media Lab</h3>
													<p class="thumb-desc__desc">The Media Lab nurtures and enhances the integration of innovative ideas and media technologies, as well as interactions between education, applied research, professional training and industry application.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_cimt.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Centre for Innovative Material and Technology (CIMT)</h3>
													<p class="thumb-desc__desc">CIMT is a comprehensive material archive and interactive learning platform designed to facilitate the exchange of material knowledge and associated applications amongst students, faculty, designers and manufacturers.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_lrc.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Learning Resources Centre (LRC)</h3>
													<p class="thumb-desc__desc">Far more than just a library, the Learning Resources Centre houses over 75,000 books, periodicals and audio-visual materials, and is a place of reflection and interaction where students are encouraged to openly discuss what they have been learning. Zone 24, a dedicated area supported by the Learning Resources Centre, is an always-open area where students can study, hold discussions and collaborate on projects.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/edu_fa.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Fashion Archive</h3>
													<p class="thumb-desc__desc">The HKDI Fashion Archive supports the use of real fashion products as teaching and learning tools, boasting a newly renovated 360 square metre space that houses around 1,500 historical fashion items. Visitors can access the iconoclastic fashion items to understand the design, culture and history behind them via the interactive platform made available by the HKDI Fashion Archive.</p>
												</div>
											</div>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Sport</strong> Facilities</h2>
										</div>
										<div class="thumb-desc">
										<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_swimming_pool.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Swimming Pool</h3>
													
													<p class="thumb-desc__desc">
													<span style="color:#ffffff;font-weight:bold;">
													<u>Seasonal Closure</u><br>
													HKDI & IVE(Lee Wai Lee) Swimming Pool is closed in the winter from 1 November 2018 till early April 2019.
													</span>
													</p>
													
													<p class="thumb-desc__desc">Opens every Tuesday to Sunday between April to October.<br>
													<strong>Campus sessions:</strong><br>
													Tue – Fri | 0930 – 2100 (1330 – 1430 exclusive)<br>
													Sat, Sun, Public Holidays | 0800 – 1100; 1300 – 1600; 1800 – 2100<br><br>
													<strong>Charging Session for Public:</strong><br>
													Tue – Fri | 1800 – 2100 <br>
													Sat, Sun, Public Holidays | 0800 – 1100; 1300 – 1600; 1800 – 2100<br>
													Admission Fee:  Adult at HK$20 | Children under 12 or senior citizens above 60 at HK$10<br>
													Enquiry: 3928 2222 / 3928 2000 (office hours) 3928 2999 (non-office hours and in case of adverse weather)
													</p>
													
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/campus/sport_basketball_court.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Basketball Court</h3>
													
													<p class="thumb-desc__desc">
													<span style="color:#ffffff;font-weight:bold;">
													Due to the earlier adverse weather, basketball court is closed for urgent repairs. Sorry for the inconvenience caused.
													</span>
													</p>
													
													<p class="thumb-desc__desc">
													<strong>Campus sessions:</strong><br>
													Mon – Fri | 0830 – 1900 <br>
													Sat | 0930 – 1200 <br><br>
													<strong>Charging Session for Public:</strong><br>
													Mon – Fri | 1900 – 2100 <br>
													Sat | 1300 – 2100 <br>
													Sun and Public Holidays | 0900 – 2100<br>
													Hire Charges: HK$80 per hour <br>
													Enquiry: 3928 2222 / 3928 2000 (office hours) 3928 2999 (non-office hours and in case of adverse weather)
													</p>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width">
								<div class="content-wrapper">
									<div class="article-detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Other</strong> Venues</h2>
										</div>
										<div class="thumb-desc">
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-1.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">VTC Auditorium</h3>
													<p class="thumb-desc__desc">The VTC Auditorium accommodates over 700 people, with specially designed acoustics to enhance performances, conferences and other gatherings. This 963 square metre venue with disabled access is a focal point of HKDI’s interactions with the community.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-2.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Experience Centre</h3>
													<p class="thumb-desc__desc">Experience Centre is a convertible area with up to 170 sqm for mini events or exhibitions. The light installation “Infinite Constellation” is specially designed for Hong Kong Pavilion at 2010 Shanghai Expo by HKDI students.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-3.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">HKDI Gallery</h3>
													<p class="thumb-desc__desc">The HKDI Gallery provides up to 600 square metres of exhibition space to showcase the breadth of design generated by and associated with HKDI. Open to the public, the venue will host exhibition, trade- and industry-related events and displays of student work.</p>
													<!--<a href="#" class="thumb-desc__link">HKDI Gallery Website ></a>-->
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-4.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Design Boulevard</h3>
													<p class="thumb-desc__desc">Completing the Podium, the Design Boulevard invites the public into the campus. Stretching over 125 m, this open area offers access to the auditorium, exhibition spaces and common facilities. Conceived as a “breathing space” for the campus, the Design Boulevard evokes the feeling of a party culture, with space for student activities and the interaction of creative minds.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-other-venues-5.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">d-mart</h3>
													<p class="thumb-desc__desc">d-mart provides a 1,040 square metre venue for showcasing the breadth of design generated by or associated with HKDI. Open to the public, the venue hosts exhibition, trade- and industry-related events and displays of student work.</p>
												</div>
											</div>
											<div class="thumb-desc__item">
												<img class="thumb-desc__item-img" src="<?php echo $img_url?>about/img-about-multi-purpose-hall.jpg" alt="" />
												<div class="thumb-desc__txt">
													<h3 class="thumb-desc__title">Multi-purpose Hall</h3>
													<p class="thumb-desc__desc">Multi-purpose hall is a convertible venue on the ground floor of Hong Kong Design Institute with an area of 338.6 square metres. It is primarily used for institute’s teaching and learning purpose, and it also serves various types of seminars and workshops.<br>For enquiries, please email: <a href="mailto:hkdi-booking@vtc.edu.hk">hkdi-booking@vtc.edu.hk</a></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="booking_enquiry">
								<div class="article-detail article-detail--green" name="booking">
									<div class="content-wrapper">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Booking</strong> Enquiry
										</div>
										<div class="txt-editor">
											<h4>Please read the Notice to Applicants before you proceed.</h4>
										</div>
										<div class="contact-form">
											<form class="contact-form__form form-container" id="form-booking" action="<?php echo $host_name; ?>send-form.php" method="POST">

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>Proposed booking date(s)</strong>
															</div>
															<div class="field__date-fields">
																<div class="field__date-field">
																	<div class="field__date-label">From:</div>
																	<input type="text" name="booking_from_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
																<div class="field__date-field">
																	<div class="field__date-label">To:</div>
																	<input type="text" name="booking_to_date" class="form-check--empty datepicker datepicker--booking" />
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="field__inline-txt">
																<strong>Proposed venue(s) of booking</strong>
															</div>
															<div class="field__checkbox-holder">
																<div id="booking-checks" class="field__checkbox-fields form-check--empty">
																	<div class="custom-checkbox">
																		<input id="booking_checkbox1" data-limit="289" type="checkbox" name="booking_lecture_theatre_289" value="y" />
																		<label for="booking_checkbox1">Lecture Theatre (289 seats)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox2" data-limit="120" type="checkbox" name="booking_lecture_theatre_120" value="y" />
																		<label for="booking_checkbox2">Lecture Theatre (120 seats)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox3" data-limit="80" type="checkbox" name="booking_function_room" value="y" />
																		<label for="booking_checkbox3">Function Room (A002-A003) (80 seats)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox4" data-limit="740" type="checkbox" name="booking_vtc_auditorium" value="y" />
																		<label for="booking_checkbox4">VTC Auditorium (740 seats)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox5" type="checkbox" name="booking_hkdi_gallery" value="y" />
																		<label for="booking_checkbox5">HKDI Gallery</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox6" type="checkbox" name="booking_dmart" value="y" />
																		<label for="booking_checkbox6">d-mart</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox7" data-limit="50" type="checkbox" name="booking_experience_center" value="y" />
																		<label for="booking_checkbox7">Experience Centre (50 seats)</label>
																	</div>
																	<div class="custom-checkbox">
																		<input id="booking_checkbox8" type="checkbox" name="booking_design_beulevard" value="y" />
																		<label for="booking_checkbox8">Design Boulevard</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>


													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>Purpose of the event</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="booking_purpose" class="is-black form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="number" name="booking_participants" placeHolder="Estimated number of participants" class="form-check--empty form-check--max" />
															<span class="field__inline-txt booking-max-num-holder is-hidden">(Max. <span id="booking-max-num"></span>)</span>
														</div>
													</div>
												</div>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">Details of Applicant</h3>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_organisation" placeHolder="Organisation" class="form-check--empty" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_contact_person" placeHolder="Contact Person" class="form-check--empty" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_tel" placeHolder="Telephone" maxlength="25" class="form-check--empty form-check--tel" />
														</div>
													</div>
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_fax" placeHolder="Fax" maxlength="25" class="form-check--option form-check--tel" />
														</div>
													</div>
												</div>
												<div class="field-row">
													<div class="field field--1-2 field--mb-1-1">
														<div class="field__holder">
															<input type="text" name="booking_email" placeHolder="Email" class="form-check--empty form-check--email" />
														</div>
													</div>
												</div>
												<br>
												<div class="contact-form__info">
													<h3 class="contact-form__info-title">Verification Code</h3>
												</div>
												<p class="contact-form__desc">Please enter the number in the image</p>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<div class="captcha">
																<div class="captcha__img">
																	<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																</div>
																<div class="captcha__control">
																	<div class="captcha__input">
																		<input type="text" name="captcha" class="form-check--empty" />
																	</div>
																	<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="contact-form__notes">
													<div class="contact-form__notes-title">Notice to Applicants:</div>
													<ol class="contact-form__notes-list">
														<li>Applications are normally accepted up to four months in advance and at the latest, one month before the start date of the hiring period.</li>
														<li>Only applications made by a company or an organisation registered in Hong Kong will be accepted. Applications by individuals will not be considered.</li>
														<li>For enquiries about this form, please call 3928 2761.</li>
														<li>Events requiring longer lead-time for planning and preparation can be applied by <a href="mailto:hkdi-booking@vtc.edu.hk" class="underline-link">email</a> with event details and proposal.</li>
													</ol>
													
												</div>
												<div class="field-row">
													<div class="field field--1-1 field--mb-1-1">
														<div class="field__holder">
															<br>
															<div class="custom-checkbox form-check--tnc">
																<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty" />
																<label for="contact_book">I agree and accept the terms in “VTC Hire of Accommodation in Council Premises Term and Condition of Hire”
																	<a href="<?php echo $img_url?>about/VTC_Hire_of_Accommodation_in_Council_Premises_TC.PDF" class="underline-link" target="_blank">(PDF)</a> and “Guidelines for Event Organisation in HKDI & IVE(LWL)” 
																	<a href="<?php echo $img_url?>about/Guidelines_for_Event_Organisation_in_HKDI_ IVE(LWL).pdf" class="underline-link" target="_blank">(PDF)</a>.
																</label>
															</div>
														</div>
													</div>
												</div>

												<div class="btn-row">
													<div class="err-msg-holder">
														<p class="err-msg err-msg--empty">This field is required.</p>
														<p class="err-msg err-msg--tel">Please enter a valid mobile phone number </p>
														<p class="err-msg err-msg--email">E-mail is required field.</p>
														<p class="err-msg err-msg--email-format">Format of your email is invalid</p>
														<p class="err-msg err-msg--phone">Phone is required field.</p>
														<p class="err-msg err-msg--phone-format">Format of your phone number is invalid.</p>
														<p class="err-msg err-msg--tnc">You must accept the Terms and Conditions.</p>
														<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
														<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														<p class="err-msg err-msg--max">Exceeded the max. no. of seats.</p>
														<p class="err-msg err-msg-datepicker">Invalid date range </p>
													</div>
													<div class="btn-row btn-row--al-hl">
														<a href="#" class="btn btn-submit">
															<strong>Send</strong>
														</a>
														<a href="#" class="btn btn-reset">
															<strong>Reset</strong>
														</a>
													</div>
												</div>

												<input type="hidden" name="lang" value="<?php echo $lang?>">
											</form>
											<div class="contact-form__success-msg">
												<h3 class="desc-subtitle">Thank you!</h3>
												<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-publication" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__head">
									<h2 class="article-detail__title">
										<strong>About</strong> Us</h2>
								</div>
								<div class="txt-editor">
									<h4>
										<strong>HKDI produces publications to showcase work by our emerging design talents, report on the design research projects conducted by lecturers, visiting fellows and students, and publish programme details. Three times a year, HKDI also publishes a design magazine, SIGNED. </strong>
									</h4>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>Programme Details</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/HKDI_prospectus_201819.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/HKDI_prospectus_201819_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Prospectus 2018/19</strong>
											</h3>
											<!--<p class="publication-list__item-desc">Merry So</p>-->
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>Signed Magazine</strong>
									</h3>
									<!--<div class="publication-list__items">
										<?php for ($i = 17; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>ISSUE No. <?php echo $i; ?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<?php } ?>
									</div>-->
									<div class="publication-list__items">
									<?php
										$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));

										foreach ($issue_list AS $issue) {
											$issue_id = $issue['issue_id'];
											$issue_cover_image = $issue['issue_cover_image'];
											$issue_name = $issue['issue_name_'.$lang];

											if ($issue['issue_cover_image'] != '' && file_exists($issue_path.$issue_id."/".$issue_cover_image)) { 
												$issue_cover_image = $issue_url.$issue_id."/".$issue_cover_image;
											}
									?>

										<a href="<?php echo $host_name_with_lang."news/publication.php?issue_id=".$issue_id?>" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $issue_cover_image?>" alt="" />
											<h3 class="publication-list__item-title">
												<strong><?php echo $issue_name?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									<?php for ($i = 14; $i > 0; $i--) { ?>
										<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>ISSUE No. <?php echo $i; ?></strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									<?php } ?>

									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>Academic/ Research</strong>
									</h3>
									<div class="publication-list__items">
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Conversation from Open Design Forum 2014 by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Conversation from Open Design Forum 2014 by DESIS Lab</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Cumulus Hong Kong 2016 Working Papers by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Cumulus Hong Kong 2016 Working Papers by HKDI</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Fine Dying  by DESIS Lab.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Fine Dying  by DESIS Lab</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Deisgn for Action by DESIS Lab_compressed.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Deisgn for Action by DESIS Lab_compressed</strong>
											</h3>
										</div>
										<div href="#" class="publication-list__item">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/academic/Open Design for E-very-thing by HKDI.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Open Design for E-very-thing by HKDI</strong>
											</h3>
										</div>
									</div>
								</div>
								<div class="publication-list">
									<h3 class="publication-list__title">
										<strong>Showcase</strong>
									</h3>
									<div class="publication-list__items">
										<a href="<?php echo $img_url?>about/publication/DZ_design_content_web.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle3_cover.jpg" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Third Issue, April 2016</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/dazzle2013.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle2_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>Second Issue, August 2013</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
										<a href="<?php echo $img_url?>about/publication/Dazzle.pdf" class="publication-list__item" target="_blank">
											<img class="publication-list__item-img" src="<?php echo $img_url?>about/publication/dazzle_cover.gif" alt="" />
											<h3 class="publication-list__item-title">
												<strong>First Issue, June 2011</strong>
											</h3>
											<div class="btn-arrow"></div>
										</a>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-experience" class="page-tabs__content">
							<section class="article-detail">
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="txt-editor">
											<h4>
												<strong>Secondary school students can participate in a tailor-made schedule of activities to give them a better understanding of design education at HKDI and possible careers in the design industry.  </strong>
											</h4>
										</div>
									</div>
								</div>
								<div class="article-detail__detail-row">
									<div class="article-detail__detail">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Campus</strong> Tour</h2>
										</div>
										<div class="txt-editor">
											<p>
												Guided campus tours allow students to experience the atmosphere in this educational environment and learn more about the campus. Visiting students are shown the campus facilities, including the Learning Resources Centre and Knowledge Centres, the workshops and studios of different design departments, showcase displays and seasonal exhibitions in collaboration with international partners.
											</p>
											<p>Each tour for 20-25 students accompanied by at least one teacher or staff member lasts 45 minutes. A tailor-made itinerary highlighting particular workshops or design programmes can be arranged with at least 3 weeks’ notice. </p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Taster</strong> Programme</h2>
										</div>
										<div class="txt-editor">
											<p>
												Each design department provides a unique taster programme for students to gain a taste of learning at HKDI, with a focus on different design processes. Communication Design and Digital Media introduces 3D printing or 360° video shooting; Design Foundation Studies demonstrates how to create art using everyday electronic devices; Fashion and Image Design lets students experience the fashion, image and brand design process; while Architecture, Interior and Product Design teaches students about interior and exterior product design using 3D spatial design knowledge. 
											</p>
											<p>Each taster programme for 18-25 students accompanied by at least one teacher or staff member lasts around 1.5 hours. Applications should be made one month in advance and activities are subject to workshop availability. 
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Career</strong> Talks </h2>
										</div>
										<div class="txt-editor">
											<p>In these career talks, HKDI lecturers provide programme information and admission requirements and discuss career prospects in the design industry. Describing current student projects and industry collaborations, lecturers also share fun and interesting teaching methods and knowledge exchange programmes. 
											</p>
											<p>Interested schools are advised to indicate the academic programmes in which they are interested. Each talk lasts 45-60 minutes, subject to further negotiation. HKDI student work can be showcased, depending on availability.
											</p>
										</div>
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Alumni</strong> Sharing</h2>
										</div>
										<div class="txt-editor">
											<p>Outstanding alumni who have received academic and industry recognition participate in sharing and Q&amp;A sessions with secondary school students. Through this interaction, students learn about the educational environment at HKDI, career prospects and solutions to career planning from experienced graduates not much older than themselves. 
											</p>
											<p>Interested schools are advised to indicate the academic programmes in which they are interested. Each sharing session of 40 minutes is followed by an open Q&amp;A session. A maximum of two alumni can attend each activity. A moderator from the secondary school should lead the students in asking related and effective questions.
											</p>
											<p>Interested teachers should fill out the form below and submit it three weeks before the proposed visit. Please read the Notes to Applicants carefully before submitting your application. </p>
										</div>
									</div>
									<div class="article-detail__gallery">
										<div class="article-detail__gallery-item">
											<img src="<?php echo $img_url?>about/img-about-taster.jpg" alt="" />
											<div class="article-detail__gallery-deco"></div>
										</div>
									</div>
								</div>
							</section>
							<section class="page-tabs__full-width" id="guided_tour_application_form" style="background-image:url(<?php echo $img_url?>about/img-bg-education-facilities.jpg);">
								<div class="content-wrapper">
									<div class="article-detail article-detail--dark">
										<div class="article-detail__head">
											<h2 class="article-detail__title">
												<strong>Guided Tour</strong> Application Form</h2>
										</div>
										<div class="txt-editor">
											<h4>Welcome to visit our campus and facilities to learn more about our latest programmes and services. Please read carefully the Notes to Applicants before submitting this application. Please submit the completed application form three weeks in advance of the proposed date of visit. (Email: eao-dilwl@vtc.edu.hk) A confirmation email will be sent to you within seven working days upon receiving your submission.</h4>
											<div class="contact-form contact-form--txt-white">
												<form class="contact-form__form form-container" id="form-application">
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>Dates of visit</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date1" placeholder="1st priority" class="form-check--empty datepicker datepicker--application" />
																	</div>
																	<div class="field__date-field">
																		<input type="text" name="application_form_visit_date2" placeholder="2nd priority" class="form-check--empty datepicker datepicker--application" />
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>Time of visit</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_visit_time">
																				<option value="10:00 - 11:00">10:00 - 11:00</option>
																				<option value="11:00 - 12:00">11:00 - 12:00</option>
																				<option value="14:30 - 15:30">14:30 - 15:30</option>
																				<option value="15:30 - 16:30">15:30 - 16:30</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>No. of visitors</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<input type="number" name="application_form_visitors_no" placeholder="No. of visitors" class="form-check--empty form-check--num" />
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>Delegation List, Visitors' Profile</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_list" class="form-check--empty" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>Language</strong>
																</div>
																<div class="field__date-fields">
																	<div class="field__date-field">
																		<div class="custom-select">
																			<select name="application_form_list_lang">
																				<option value="Cantonese">Cantonese</option>
																				<option value="English">English</option>
																				<option value="Chinese">Chinese</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="field__inline-txt">
																	<strong>Objective</strong>
																</div>
																<div class="field__checkbox-holder">
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj1" type="radio" name="application_form_obj_academic_collaboration" value="y" checked/>
																			<label for="apply_obj1">Academic Collaboration</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj2" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj2">Industry Visit</label>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj3" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj3">School Visit, please indicate the grade</label>
																			<div data-ref="apply_obj3" class="radio-subs radio-child-container">
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj4" type="checkbox" name="application_form_obj_form4" value="y" />
																					<label for="apply_obj4">Form 4</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj5" type="checkbox" name="application_form_obj_form5" value="y" />
																					<label for="apply_obj5">Form 5</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<input id="apply_obj6" type="checkbox" name="application_form_obj_form6" value="y" />
																					<label for="apply_obj6">Form 6</label>
																				</div>
																				<div class="custom-checkbox custom-checkbox--inline">
																					<label for="apply_others">Others</label>	
																					<input type="text" name="application_form_obj_others" class="custom-checkbox__txt-input" />
																				</div>
																			</div>
																		</div>
																		<div class="custom-radio custom-radio--white">
																			<input id="apply_obj7" type="radio" name="application_form_obj_academic_collaboration" value="y" />
																			<label for="apply_obj7">Others (please specify):</label>
																			<span  data-ref="apply_obj7" class="radio-subs">
																			<input type="text" name="application_form_obj_others_desc" class="custom-checkbox__txt-input form-check--empty" />
																			</span>
																		</div>
																</div>
															</div>
														</div>
													</div>

													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>Details of Applicant</strong>
														</h3>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_organisation" placeHolder="Name of Organisation" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<div class="custom-select">
																	<select name="application_form_title">
																		<option>Title</option>
																		<option value="Mr">Mr</option>
																		<option value="Mrs">Mrs</option>
																		<option value="Ms">Ms</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact_person" placeHolder="Contact Person" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_position" placeHolder="Position" class="form-check--empty" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_contact" placeHolder="Contact Person No." class="form-check--empty form-check--tel" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_mobile" placeHolder="Mobile Phone No." class="form-check--option form-check--tel" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="application_form_email" placeHolder="Email" class="form-check--empty form-check--email" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1 field--textarea">
															<div class="field__holder">
																<div class="field__inline-txt field__textarea-title">
																	<strong>Remarks</strong>
																</div>
																<div class="field__date-fields field__textarea-input">
																	<div class="field__date-field">
																		<textarea name="application_form_remarks" class="form-check--option" rows="4" cols="50"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<br>
													<div class="contact-form__info">
														<h3 class="contact-form__info-title">
															<strong>Verification Code</strong>
														</h3>
													</div>
													<p class="contact-form__desc">Please enter the number in the image</p>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="captcha">
																	<div class="captcha__img">
																		<img id="application_form_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																	</div>
																	<div class="captcha__control">
																		<div class="captcha__input">
																			<input type="text" name="captcha" class="form-check--empty" />
																		</div>
																		<a href="#" class="captcha__btn" onClick="document.getElementById('application_form_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="contact-form__notes">
														<div class="contact-form__notes-title">Notes:</div>
														<p class="contact-form__notes-desc">(Please read these service provisions carefully before submitting an application.)</p>
														<ol class="contact-form__notes-list">
															<li>Application should be submitted at least three weeks in advance of the proposed day of visit. (Email : <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>)</li>
															<li>Members of public are welcome to visit HKDI and IVE (Lee Wai Lee) campus, while guided campus tours are generally
																offered to schools, academic institutions, organizations and industries only. Prospective students and industrial
																partners will get to learn more about campus facilities, programmes, student life, academic and industrial
																collaboration as well as the institute’s development.</li>
															<li>Please specify the grade(s) or level(s) of study of the students for school visits, e.g. S5, S6.</li>
															<li>Applicants are required to provide the list of visitors and/or supporting document(s) with their applications.</li>
															<li>Please do not resubmit an application if it has already been successfully submitted.</li>
															<li>Campus Visit will not be available on Saturdays, Sundays and Public Holidays.</li>
															<li>Campus Visit will be cancelled if Amber Rainstorm Signal or Typhoon Signal No. 3 is hoisted.</li>
															<li>The personal data provided will be used by HKDI & IVE (Lee Wai Lee) to process the visit application and for
																related internal reporting purposes.</li>
															<li>HKDI & IVE (Lee Wai Lee) will acknowledge via email receipt of your application and notify you of the result.
																If you do not hear from us seven working days after submitting an application and all the required document(s),
																please contact our External Affairs Office on (852) 3928 2561 or email us at <a href="mailto:eao-dilwl@vtc.edu.hk" class="underline-link">eao-dilwl@vtc.edu.hk</a>.</li>
															<li>HKDI & IVE (Lee Wai Lee) reserves the right to consider applications at its sole discretion. If any information
																provided by the applicant is found to be untrue, HKDI & IVE (Lee Wai Lee) may reject the application or withdraw
																any approval given.</li>
															<li>In case of cancellation of campus visit, the applicant should inform the EAO staff in charge in writing at
																least two working days before the proposed date of visit.</li>
														</ol>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<br>
																<div class="custom-checkbox form-check--tnc">
																	<input id="apply_tnc" type="checkbox" name="apply_tnc" class="form-check--empty " />
																	<label for="apply_tnc">I agree and accept the rules and conditions as stated in the "Campus Visit – Notes to Applicants".</label>
																</div>
															</div>
														</div>
													</div>

													<div class="btn-row">
														<div class="err-msg-holder">
															<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
															<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														</div>
														<div class="btn-row btn-row--al-hl">
															<a href="#" class="btn btn-submit">
																<strong>Send</strong>
															</a>
															<a href="#" class="btn btn-reset">
																<strong>Reset</strong>
															</a>
														</div>
													</div>

													<input type="hidden" name="lang" value="<?php echo $lang?>">
												</form>
												<div class="contact-form__success-msg">
													<h3 class="desc-subtitle">Thank you!</h3>
													<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div id="tabs-professional-membership" class="page-tabs__content">
							
						</div>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>
				<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
