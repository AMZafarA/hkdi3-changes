<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$host_name_without_lang = substr($host_name_with_lang,0,-3);
$page_title = 'Search';
$section = 'search';
$subsection = '';
$sub_nav = '';
$char_limit = 250;

$langs = array("en" => "lang1" , "tc" => "lang2" , "sc" => "lang3");


$breadcrumb_arr['Search'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$search_text = @$_POST['search_text'];

if ($search_text != '')
{

	$dynamic_pages = array(
		array(
			"table" => $DB_STATUS . "news",
			"conditions" => "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND (news_name_lang1 LIKE ? OR news_name_lang2 LIKE ? OR news_name_lang3 LIKE ? OR news_detail_lang1 LIKE ? OR news_detail_lang2 LIKE ? OR news_detail_lang3 LIKE ?)",
			"order_by" => "news_date DESC",
			"parameters" => array(STATUS_ENABLE,$today,$today,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "news_name_",
			"detail" => "news_detail_",
			"url" => "news/news-detail.php",
			"url_param" => "news_id",
			"url_param_value" => "news_id"
		),
		array(
			"table" => $DB_STATUS . "product",
			"conditions" => "product_status = ? AND product_type = ? AND (product_from_date <= ? OR product_from_date IS NULL) AND (product_to_date >= ? OR product_to_date IS NULL) AND (product_name_lang1 LIKE ? OR product_name_lang2 LIKE ? OR product_name_lang3 LIKE ? OR product_detail_lang1 LIKE ? OR product_detail_lang2 LIKE ? OR product_detail_lang3 LIKE ?)",
			"order_by" => "product_date DESC",
			"parameters" => array(STATUS_ENABLE,2,$today,$today,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "product_name_",
			"detail" => "product_detail_",
			"url" => "news/publication-detail.php",
			"url_param" => "product_id",
			"url_param_value" => "product_id"
		),
		array(
			"table" => $DB_STATUS . "programmes",
			"conditions" => "programmes_status = ? AND (programmes_name_lang1 LIKE ? OR programmes_name_lang2 LIKE ? OR programmes_name_lang3 LIKE ? OR programmes_detail_lang1 LIKE ? OR programmes_detail_lang2 LIKE ? OR programmes_detail_lang3 LIKE ?)",
			"order_by" => "",
			"parameters" => array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "programmes_name_",
			"detail" => "programmes_detail_",
			"url" => "programmes/programme.php",
			"url_param" => "programmes_id",
			"url_param_value" => "programmes_id"
		),
		array(
			"table" => $DB_STATUS . "top_up_degree",
			"conditions" => "top_up_degree_status = ? AND (top_up_degree_name_lang1 LIKE ? OR top_up_degree_name_lang2 LIKE ? OR top_up_degree_name_lang3 LIKE ? OR top_up_degree_detail_lang1 LIKE ? OR top_up_degree_detail_lang2 LIKE ? OR top_up_degree_detail_lang3 LIKE ?)",
			"order_by" => "",
			"parameters" => array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "top_up_degree_name_",
			"detail" => "top_up_degree_detail_",
			"url" => "programmes/bachelor-degree-detail.php",
			"url_param" => "degree_id",
			"url_param_value" => "top_up_degree_id",
		),
		array(
			"table" => $DB_STATUS . "international",
			"conditions" => "international_status = ? AND international_type = ? AND (international_name_lang1 LIKE ? OR international_name_lang2 LIKE ? OR international_name_lang3 LIKE ? OR international_detail_lang1 LIKE ? OR international_detail_lang2 LIKE ? OR international_detail_lang3 LIKE ?)",
			"order_by" => "",
			"parameters" => array(STATUS_ENABLE,"O",'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "international_name_",
			"detail" => "international_detail_",
			"url" => "global-learning/outgoing-student-details.php",
			"url_param" => "international_id",
			"url_param_value" => "international_id",
		),
		array(
			"table" => $DB_STATUS . "product",
			"conditions" => "product_status = ? AND product_type = ? AND (product_name_lang1 LIKE ? OR product_name_lang2 LIKE ? OR product_name_lang3 LIKE ? OR product_detail_lang1 LIKE ? OR product_detail_lang2 LIKE ? OR product_detail_lang3 LIKE ?)",
			"order_by" => "product_seq DESC, product_date DESC",
			"parameters" => array(STATUS_ENABLE,'4','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "product_name_",
			"detail" => "product_detail_",
			"url" => "hkdi_gallery/exhibition",
			"url_param" => "",
			"url_param_value" => "product_slug",
		),
		array(
			"table" => $DB_STATUS . "project",
			"conditions" => "project_publish_status = ? AND project_status = ? AND (project_name_lang1 LIKE ? OR project_name_lang2 LIKE ? OR project_name_lang3 LIKE ? OR project_detail_lang1 LIKE ? OR project_detail_lang2 LIKE ? OR project_detail_lang3 LIKE ?)",
			"order_by" => "project_seq DESC",
			"parameters" => array('AL',STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "project_name_",
			"detail" => "project_detail_",
			"url" => "knowledge-centre/detail.php",
			"url_param" => "product_id",
			"url_param_value" => "project_id",
		),
		array(
			"table" => $DB_STATUS . "international",
			"conditions" => " international_type = ? AND international_status = ? AND (international_name_lang1 LIKE ? OR international_name_lang2 LIKE ? OR international_name_lang3 LIKE ? OR international_detail_lang1 LIKE ? OR international_detail_lang2 LIKE ? OR international_detail_lang3 LIKE ?)",
			"order_by" => "international_seq DESC",
			"parameters" => array('P', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "international_name_",
			"detail" => "international_detail_",
			"url" => "global-learning/exchange-programme-details.php",
			"url_param" => "international_id",
			"url_param_value" => "international_id",
		),
		array(
			"table" => $DB_STATUS . "international",
			"conditions" => " international_type = ? AND international_status = ? AND (international_name_lang1 LIKE ? OR international_name_lang2 LIKE ? OR international_name_lang3 LIKE ? OR international_detail_lang1 LIKE ? OR international_detail_lang2 LIKE ? OR international_detail_lang3 LIKE ?)",
			"order_by" => "international_seq DESC",
			"parameters" => array('O', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "international_name_",
			"detail" => "international_detail_",
			"url" => "global-learning/outgoing-student-details.php",
			"url_param" => "international_id",
			"url_param_value" => "international_id",
		),
		array(
			"table" => $DB_STATUS . "international",
			"conditions" => " international_type = ? AND international_status = ? AND (international_name_lang1 LIKE ? OR international_name_lang2 LIKE ? OR international_name_lang3 LIKE ? OR international_detail_lang1 LIKE ? OR international_detail_lang2 LIKE ? OR international_detail_lang3 LIKE ?)",
			"order_by" => "international_seq DESC",
			"parameters" => array('S', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "international_name_",
			"detail" => "international_detail_",
			"url" => "knowledge-centre/detail.php",
			"url_param" => "product_id",
			"url_param_value" => "international_id",
		),
		array(
			"table" => $DB_STATUS . "international",
			"conditions" => " international_type = ? AND international_status = ? AND (international_name_lang1 LIKE ? OR international_name_lang2 LIKE ? OR international_name_lang3 LIKE ? OR international_detail_lang1 LIKE ? OR international_detail_lang2 LIKE ? OR international_detail_lang3 LIKE ?)",
			"order_by" => "international_seq DESC",
			"parameters" => array('I', STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "international_name_",
			"detail" => "international_detail_",
			"url" => "global-learning/incoming-student-details.php",
			"url_param" => "international_id",
			"url_param_value" => "international_id",
		),
		array(
			"table" => $DB_STATUS . "rsvp",
			"conditions" => "rsvp_publish_status = ? AND rsvp_status = ? AND (rsvp_name_lang1 LIKE ? OR rsvp_name_lang2 LIKE ? OR rsvp_name_lang3 LIKE ? OR rsvp_detail_lang1 LIKE ? OR rsvp_detail_lang2 LIKE ? OR rsvp_detail_lang3 LIKE ?)",
			"order_by" => "rsvp_created_by DESC",
			"parameters" => array('AL',STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "rsvp_name_",
			"detail" => "rsvp_detail_",
			"url" => "news/event.php",
			"url_param" => "name",
			"url_param_value" => "rsvp_url_alias",
		),
		array(
			"table" => "new_collaboration",
			"conditions" => "new_collaboration_status = ? AND (new_collaboration_name_lang1 LIKE ? OR new_collaboration_name_lang2 LIKE ? OR new_collaboration_name_lang3 LIKE ? OR new_collaboration_detail_lang1 LIKE ? OR new_collaboration_detail_lang2 LIKE ? OR new_collaboration_detail_lang3 LIKE ?)",
			"order_by" => "",
			"parameters" => array(STATUS_ENABLE,'%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%','%'.$search_text.'%'),
			"name" => "new_collaboration_name_",
			"detail" => "new_collaboration_detail_",
			"url" => "collaboration/",
			"url_param" => "",
			"url_param_value" => "",
		)
	);


	// dynamic pages (without id) from database
	$c_pages = array(
		array(
			"url" => 'knowledge-centre/ccd.php',
			"title" => 'CCD',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 8 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 23 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/cdss.php',
			"title" => 'CDSS',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 8 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 23 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/cimt.php',
			"title" => 'CIMT',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 4 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 17 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/desis_lab.php',
			"title" => 'Desis Lab',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 5 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 18 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/fashion_archive.php',
			"title" => 'Fashion Archive',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 7 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 20 , '%'.$search_text.'%' )
				)
			)
		),
		array(
			"url" => 'knowledge-centre/media_lab.php',
			"title" => 'Media Lab',
			"search" => array(
				array(
					"table" => 'product_section',
					"text" => 'product_section_content_',
					"conditions" => "product_section_pid = ? AND product_section_status = ? AND ( product_section_name_$lang LIKE ? OR product_section_content_$lang LIKE ? )",
					"parameters" => array( 6 , STATUS_ENABLE , '%'.$search_text.'%' , '%'.$search_text.'%' )
				),
				array(
					"table" => 'project',
					"text" => 'project_detail_',
					"conditions" => "project_status = ? AND project_lv1 = ? AND project_lv2 = ? AND project_detail_$lang LIKE ?",
					"parameters" => array( STATUS_ENABLE, 16 , 19 , '%'.$search_text.'%' )
				)
			)
		)
	);

	$c_count_pages = 0;
	$c_result_pages = array();
	foreach ( $c_pages as $page ) {
		$f = false;
		foreach ($page["search"] as $p) {
			if( !$f ) {
				$s = DM::findOne($DB_STATUS.$p["table"], $p["conditions"], "", $p["parameters"]);
				if( !empty( $s ) )  {
					$f = true;
					$cp = array();
					$cp["title"] = $page["title"];
					$cp["url"] = $page["url"];
					$content = remove_all($s[$p["text"].$lang]);
					$pos = strpos ( strtolower( $content ) , strtolower( $search_text ) );
					if ( $pos !== false ) {
						$p = array();
						$count_pages++;
						$_c = $content;
						$og_pos = strpos( strip_tags( strtolower( $_c ) ) , strtolower( $search_text ) );
						$pos_1 = ( $og_pos - $char_limit <= 0 ) ? 0 : $og_pos - $char_limit;
						$pos_2 = ( $og_pos + $char_limit > strlen( $_c ) ) ? strlen( $_c ) : $og_pos + $char_limit;
						$cp["text"] = substr( $_c , $pos_1 , $pos_2 );
					} 
					$c_result_pages[] = $cp;
					$c_count_pages++;
				}
			}
		}
	}

}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/search.css" type="text/css" />
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title page-head__title--big">
						<h2><strong>Search</strong></h2>
					</div>
				</div>
			</div>
		</div>
		<form class="search-field" action="index.php" method="POST"><input class="field" type="search" name="search_text" placeholder="Enter Keywords" value="<?php echo $search_text; ?>" /><input class="submit-btn" type="button" /></form>
		<section class="search-result">

			<p class="search-result__keywords"><strong>Keywords:</strong> <span class="keywords"><?php echo $search_text; ?></span> (<span id="total_count"></span> Results)</p>

			<?php 
			$total_count = 0;
			foreach ($dynamic_pages as $row) {
				$list_all = DM::findAll($row["table"], $row["conditions"], $row["order_by"], $row["parameters"]);
				foreach ($list_all as $list) {
					foreach ($langs as $l_key => $l_val) {
						$title = strip_tags($list[$row["name"] . $l_val]);
						$detail = strip_tags($list[$row["detail"] . $l_val]);
						$title_lower = strip_tags(strtolower($list[$row["name"] . $l_val]));
						$detail_lower = strip_tags(strtolower($list[$row["detail"] . $l_val]));
						$search_text_lower = strtolower($search_text);
						$link = $host_name_without_lang . $l_key . "/" . $row["url"];
						if($row["url_param"]) {
							$link .= "?" . $row["url_param"] . "=";
						} 
						if($row["url_param_value"]) {
							$link .= $list[$row["url_param_value"]];
						}
						$t = generateLink( $title , $detail , $link , $search_text );
						echo '<a class="result" href="' . $host_name_without_lang . $l_key . "/" . $row["url"] . "?" . $row["url_param"] . "=" . $list[$row["url_param_value"]] . '">' . $t . '</a>';
						$total_count++;
					}
				}
				// foreach ($langs as $l_key => $l_val) {
				// 	$list_all = array();
				// 	$conditions = implode($l_val, $row["conditions"]);
				// 	$list_all = DM::findAll($row["table"], $conditions, $row["order_by"], $row["parameters"]);
				// 	foreach ($list_all as $list) {
				// 		$title = strip_tags($list[$row["name"] . $l_val]);
				// 		$detail = strip_tags($list[$row["detail"] . $l_val]);
				// 		$title_lower = strip_tags(strtolower($list[$row["name"] . $l_val]));
				// 		$detail_lower = strip_tags(strtolower($list[$row["detail"] . $l_val]));
				// 		$search_text_lower = strtolower($search_text);
				// 		$link = $host_name_without_lang . $l_key . "/" . $row["url"];
				// 		if($row["url_param"]) {
				// 			$link .= "?" . $row["url_param"] . "=";
				// 		} 
				// 		if($row["url_param_value"]) {
				// 			$link .= $list[$row["url_param_value"]];
				// 		}
				// 		if( strpos($title_lower, $search_text_lower) !== false || strpos($detail_lower, $search_text_lower) !== false ) {
				// 			$t = generateLink( $title , $detail , $link , $search_text );
				// 			echo '<a class="result" href="' . $host_name_without_lang . $l_key . "/" . $row["url"] . "?" . $row["url_param"] . "=" . $list[$row["url_param_value"]] . '">' . $t . '</a>';
				// 			$total_count++;
				// 		}
				// 	}
				// }
			} 
			?>

			<?php foreach ($c_result_pages as $page) {
				$total_count += $c_count_pages;
				$url = $host_name_with_lang . $page["url"];
				$t = $page["title"];
				if( !empty( $page["text"] ) )
					$t .= '<br><small class="link_text">' . $page["text"] . '</small>';
				?>
				<a class="result" href="<?php echo $url;?>"><?php echo $t; ?></a>
			<?php } ?>



		</section>

		<script>document.getElementById("total_count").innerText = "<?php echo $total_count; ?>"</script>


		<?php
		function generateLink( $title , $detail , $link , $search ) {
			$ret = '<span class="link_title">' . $title . "</span>"; 
			if (strpos(strtolower($title), strtolower($search)) == 0) {
				$og_pos = strpos(strip_tags(strtolower($detail)), strtolower($search));
				$pos_1 = ($og_pos-$char_limit <= 0) ? 0 : $og_pos-$char_limit;
				$pos_2 = ($og_pos+$char_limit > strlen($detail)) ? strlen($detail) : $og_pos+$char_limit;
				$new = substr(strip_tags($detail), $pos_1, $pos_2);
				$new = mb_convert_encoding($new, "UTF-8");
				$ret .= '<br><small class="link_text">' . $new . '</small>';
				// $ret .= '<br><small class="link_text">' . mb_substr(strip_tags($detail), $pos_1, $pos_2, "UTF-8") . '</small>';
			}
			return $ret;
		}

		function remove_all($content) {
			$content = preg_replace('/<\?php[\s\S]+?\?>/', '', $content);
			$content = preg_replace('/<script[\s\S]+?<\/script>/', '', $content);
			$content = preg_replace('/<head[\s\S]+?<\/head>/', '', $content);
			$content = preg_replace('/<header[\s\S]+?<\/header>/', '', $content);
			$content = preg_replace('/<nav[\s\S]+?<\/nav>/', '', $content);
			$content = preg_replace('/<footer[\s\S]+?<\/footer>/', '', $content);
			$content = preg_replace('/<link[\s\S]+?>/', '', $content);
			$content = strip_tags($content);
			return $content;
		}
		?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>


	<style>
		span.highlighted {background: yellow;}
		small.link_text {font-weight: normal !important; margin-top: 10px; display: block;}
	</style>
	<script>
		$(document).ready(function() {
			var search = '<?php echo $search_text; ?>';
			var regex = new RegExp(search, 'gi')

			var elems = [ "link_title" , "link_text" ];
			for (var i = 0; i < elems.length; i++) {
				var elem = document.getElementsByClassName(elems[i]);
				for (var j = 0; j < elem.length; j++) {
					var result = elem[j].innerText.replace(regex, function(response) {
						return "<span style='background-color: yellow;'>" + response + "</span>"
					});
					if( elems[i] == "link_text" )
						if(result != "")
							result = "... " + result + " ..."
					elem[j].innerHTML = result
				}
			}

		});
	</script>


</body>

</html>