<?php
$inc_root_path = '../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Style Demo';
$section = 'style-demo';
$subsection = 'index';
$sub_nav = 'style-demo';
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/style-demo.css" type="text/css" />
    </head>

    <body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="style-demo">
                <br>
                <h2 class="style-demo__title-lv1">[Title LV 1] Lorem ipsum dolor sit amet</h2>
                <br>
                <h3 class="style-demo__title-lv2">[Title LV 2] Lorem ipsum dolor sit amet</h3>
                <br>
                <h4 class="style-demo__title-lv3">[Title LV 3] Lorem ipsum dolor sit amet</h4>
                <br>
                <h5 class="style-demo__title-lv4">[Title LV 4] Lorem ipsum dolor sit amet</h5>
                <br>
                <p class="style-demo__txt-p">[Content Text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus tincidunt lorem sit
                    amet euismod.</p>
                <br>
                <p class="style-demo__txt-s">[Smaller Text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus tincidunt lorem sit
                    amet euismod.</p>
            </div>
        </main>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>

    </html>