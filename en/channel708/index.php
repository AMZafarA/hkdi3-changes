<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Contact';
$section = 'contact';
$subsection = '';
$sub_nav = '';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>Radio</strong> 708
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
			<h3>Mission Statement</h3>

			<p>Produced on the HKDI/IVE(LWL) campus, Radio 708 is a collaboration among students from many different areas of study. It has also been a joint effort supported by several departments. The Language Centre, Department of Information Technology and the Technical Support Unit have all come together to help make this project a success. It is a platform for our students to share their passion and expertise in a fun way. It is student oriented. Our students are responsible for all production duties, including content development, recording and sound editing.</p>

			<p>The name “Radio 708” speaks to the unique role the Centre for Independent Language Learning (C.I.L.L.), located in room B708, plays here at HKDI/IVE(LWL). Radio 708 was envisioned as an extension of this space, where any interested student can feel comfortable sharing with and learning from their classmates, while gaining valuable trade-specific experience, improving their confidence and language skills, and having fun. Please contact Kent Foran at kentforan@vtc.edu.hk or William Lai at williamlyy@vtc.edu.hk if you have any questions or would like to get involved.</p>

			<p><a href="http://dilwl-radio708.vtc.edu.hk/" target="_blank">Website</a></p>
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>