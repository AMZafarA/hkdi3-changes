<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Industry Collaboration';
$section = 'industrial';
$subsection = '';
$sub_nav = '';

$new_collaboration_cms_url = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_url );
$new_collaboration_cms_path = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_path );
$new_collaboration_file_cms_url = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_file_url );

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);
$conditions = "news_status = ? AND news_type = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, 'collaborative_projects', $today, $today);
// $conditions = "news_lv1 = ? AND news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
// $parameters = array(25, STATUS_ENABLE, $today, $today);
$news_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$industrial_attachment = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(1, STATUS_ENABLE));
$collaborative_projects = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, " news_date DESC, news_id DESC", $parameters);
$cp_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
$hkdi_star_graduate_scheme = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(13, STATUS_ENABLE));
$graduate_employment = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(14, STATUS_ENABLE));

$industrial_attachment_sections = DM::findAll('new_collaboration_section', 'new_collaboration_section_pid = ? AND new_collaboration_section_status = ?', '', array(1, STATUS_ENABLE));
$hkdi_star_graduate_scheme_sections = DM::findAll('new_collaboration_section', 'new_collaboration_section_pid = ? AND new_collaboration_section_status = ?', '', array(13, STATUS_ENABLE));

$lat = '22.3057314';
$lng = '114.2512759';

function GenTitle($title){
	$title_arr = explode(" ", $title);
	$count_arr = count($title_arr);
	$h2_title = str_replace($title_arr[$count_arr-1],"",$title);

	if($count_arr ==1){
		return "<h2>".$title."</h2>";
	}else{
		return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
	}


}


$breadcrumb_arr['Industry Collaboration'] = '';

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
	<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN""></script>-->
	<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
	<script>
		$(document).ready(function () {
                //initMap();
                var mySwiper = new Swiper ('.swiper-container', {
                	pagination: {
                		el: '.swiper-pagination',
                		type: 'bullets',
                	},
                	paginationClickable: true,
                	autoHeight: true
                });
            });

		function initMap() {
			var stylez = [{
				featureType: "all",
				elementType: "all",
				stylers: [
				{ saturation: -100 }
				]
			}];

			var imageTag = '<?php echo $host_name?>images/common/locator.png';
			var tag = new google.maps.MarkerImage(
				imageTag,
				null, /* size is determined at runtime */
				null, /* origin is 0,0 */
				null, /* anchor is bottom center of the scaled image */
				new google.maps.Size(50, 73)
				);

			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 14,
				center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),

				styles: stylez
			});

			var marker;
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
				map: map,
				icon: tag
			});
		}
	</script>
	<style>
		.page-gallery .inner-top-banner .sub-menu{
			background:<?php echo $product_background_color?>;
			color:<?php echo $product_text_color?>;
		}
		.sub-menu{
			background: #4a92cd !important;
			color: #000 !important;
		}
		.inner-top-banner .sub-menu a{
			color:#000 !important;
		}
		.page-gallery .photos .col a:nth-child(3):after,
		.page-gallery .photos .col a:nth-child(5):before{
			background-color:<?php echo $product_background_color?>;
		}
		.page-gallery .access-map h2,
		.page-gallery .photos h2 span,
		.page-gallery .about h2 span{
			color:<?php echo $product_background_color?>;
		}
		.photos h2 {
			width: 80%;
			margin: auto;
			line-height: 1em;
			margin-bottom: 40px;
		}
		
		.access-map h2, .photos h2 span, .about h2 span {
			color: #4a92cd;
			font-weight: 100;
			display: block;
			margin-left: 15%;
		}
		.about {
			position: relative;
			width: 80%;
			margin: 100px auto;
		}
		.hidden-block {
			max-width: 80%;
			margin: 0 auto;
		}
		section.programme-news a.block {
			margin: 0 5px 30px !important;
		}
		section.programme-news h2 span {
			color: #000 !important;
		}
		section#industrial-attachment > div > h2 {
			color: #fff;
		}
		section#graduate-employment h2 span {
			color: #fff;
		}
		h2 span {
			color: #4a92cd;
			font-weight: 100;
			display: block;
			margin-left: 15%;
		}
	</style>
</head>
<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<div class="top-divide-line"></div>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<!-- <section class="inner-top-banner">
			<?php if ($industrial_attachment["new_collaboration_banner"] != '' && file_exists($new_collaboration_cms_path . $industrial_attachment["new_collaboration_id"] . '/' . $industrial_attachment["new_collaboration_banner"])) { ?>
				<div class="banner-visual"><img src="<?php echo $new_collaboration_cms_url . $industrial_attachment["new_collaboration_id"] . '/' . $industrial_attachment["new_collaboration_banner"]; ?>" /></div>
			<?php } ?>
		</section> -->



		<!-- content start -->



		<section id="industrial-attachment" style="background: <?php echo $industrial_attachment["new_collaboration_background_color"]; ?>; padding: 40px 0;">
			<div class="content-wrapper">
				<?php
				// echo GenTitle($industrial_attachment['new_collaboration_name_'.$lang]);
				echo '<h2>' . $industrial_attachment['new_collaboration_name_'.$lang] . '</h2>';
				foreach ($industrial_attachment_sections as $section) {
					if ($section["new_collaboration_section_type"] == "T") {
						echo $section['new_collaboration_section_content_'.$lang];
					}
				}
				?>
			</div>
		</section>


		<section class="programme-news ani" style="padding: 40px 0px;">
			<?php echo GenTitle("Collaborative Projects"); ?>
			<?php foreach ($collaborative_projects AS $news) { ?>
				<?php if ($news['news_thumb'] != '' && ( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) || file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) ) { ?>
					<a class="block" href="../news/news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
						<?php if( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) ) { ?>
							<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
						<?php } else if( file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) { ?>
							<img src="<?php echo str_replace("uploaded_files","CMS/uploaded_files",$news_url).$news['news_id']."/".$news['news_thumb']; ?>" />
						<?php } ?>
						<div class="txt">
							<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
							<p><?php echo $news['news_name_'.$lang]; ?></p>
							<p class="btn-arrow"></p>
						</div>
					</a>
				<?php } else { ?>
					<a class="block" href="../news/news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
						<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
						<div class="table">
							<div class="cell">
								<p>
									<strong>Latest News</strong>
								</p>
								<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
							</div>
							<p class="btn-arrow"></p>
						</div>
					</a>
				<?php } ?>
			<?php } ?>
		</section>
		<?php if ($cp_count > $news_page_size) { ?>
			<div class="pagination">
				<?php if ($is_curr_tab && $page > 1) { ?>
					<a href="index.php?tab=all&page=<?php echo ($page-1); ?>" class="pagination__btn-prev">Previous</a>
				<?php } ?>
				<div class="pagination__pages">
					<div class="pagination__current">
						<div class="pagination__current-pg"><?php echo ($tab == 'all' ? $page : 1); ?></div>
					</div>
					<span>of</span>
					<div class="pagination__total"><?php echo ceil($cp_count / $news_page_size); ?></div>
				</div>
				<?php if ($cp_count > $news_page_size * ($is_curr_tab ? $page : 1)) { ?>
					<a href="index.php?tab=all&page=<?php echo ($is_curr_tab ? $page+1 : 2); ?>" class="pagination__btn-next">Next</a>
				<?php } ?>
			</div>
		<?php } ?>


		<section id="graduate-employment" style="background: <?php echo $graduate_employment["new_collaboration_background_color"]; ?>; padding: 40px 0;">
			<div class="content-wrapper">
				<?php
				echo GenTitle($graduate_employment['new_collaboration_name_'.$lang]);
				?>
				<p>To visit <strong>Graduate Employment</strong>, click <a href="<?php echo $graduate_employment["new_collaboration_link"]; ?>" target="_blank"><u>here</u></a>.</p>
			</div>
		</section>



		<section id="hkdi-star-graduate-scheme" style="background: <?php echo $hkdi_star_graduate_scheme["new_collaboration_background_color"]; ?>; padding: 40px 0;">
			<div class="content-wrapper">
				<?php
				echo GenTitle($hkdi_star_graduate_scheme['new_collaboration_name_'.$lang]);
				?>
				<section class="article-detail">
					<div class="article-detail__detail-row">
						<div class="article-detail__detail">
							<?php
							foreach ($hkdi_star_graduate_scheme_sections as $section) {
								if ($section["new_collaboration_section_type"] == "T") {
									echo $section['new_collaboration_section_content_'.$lang];
								}
							}
							?>
						</div>
						<div class="article-detail__gallery">
							<?php
							if( ! empty( $hkdi_star_graduate_scheme["new_collaboration_video_link"] ) ) {
								?>
								<video width="100%" height="auto" controls><source src="<?php echo $hkdi_star_graduate_scheme['new_collaboration_video_link']; ?>" type="video/mp4"></video>
								<?php
							}
							?>
						</div>
					</section>
				</div>
			</div>
		</section>




		<!-- content end -->



	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>
</html>