<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Industrial';
$section = 'industrial';
$subsection = '';
$sub_nav = '';

$new_collaboration_cms_url = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_url );
$new_collaboration_cms_path = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_path );
$new_collaboration_file_cms_url = str_replace( "uploaded_files" , "CMS/uploaded_files" , $new_collaboration_file_url );

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$industrial = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(1, STATUS_ENABLE));
$hkfyg_video_workshop = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(2, STATUS_ENABLE));
$mrjrxhkdi = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(3, STATUS_ENABLE));
$online_exhibition = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(4, STATUS_ENABLE));
$exhibition_photos = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(5, STATUS_ENABLE));
$virtual_guided_tour = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(6, STATUS_ENABLE));
$highlight_exhibits = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(7, STATUS_ENABLE));
$learning_resources = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(8, STATUS_ENABLE));
$press_release = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(9, STATUS_ENABLE));
$events_public_services = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(10, STATUS_ENABLE));
$visit_us = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(11, STATUS_ENABLE));
$acknowledgement = DM::findOne('new_collaboration', 'new_collaboration_id = ? AND new_collaboration_status = ?', '', array(12, STATUS_ENABLE));

$sections_for_anchors = array($industrial, $hkfyg_video_workshop, $mrjrxhkdi, $online_exhibition, $exhibition_photos, $virtual_guided_tour, $highlight_exhibits, $learning_resources, $press_release, $events_public_services, $visit_us, $acknowledgement);
$sections_to_display = array($industrial, $hkfyg_video_workshop, $mrjrxhkdi, $online_exhibition, $exhibition_photos, $virtual_guided_tour, $highlight_exhibits, $learning_resources, $press_release, $events_public_services, $visit_us, $acknowledgement);

$lat = '22.3057314';
$lng = '114.2512759';

function GenTitle($title){
	$title_arr = explode(" ", $title);
	$count_arr = count($title_arr);

	$h2_title = str_replace($title_arr[$count_arr-1],"",$title);

	if($count_arr ==1){
		return "<h2>".$title."</h2>";
	}else{
		return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
	}

}


$breadcrumb_arr['Industrial'] = '';

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
	<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtjFpOf59RvimOvgbIkEkwGtJmDqv3xw&language=EN""></script>-->
	<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
	<script>
		$(document).ready(function () {
                //initMap();
                var mySwiper = new Swiper ('.swiper-container', {
                	pagination: {
                		el: '.swiper-pagination',
                		type: 'bullets',
                	},
                	paginationClickable: true,
                	autoHeight: true
                });
            });

		function initMap() {
			var stylez = [{
				featureType: "all",
				elementType: "all",
				stylers: [
				{ saturation: -100 }
				]
			}];

			var imageTag = '<?php echo $host_name?>images/common/locator.png';
			var tag = new google.maps.MarkerImage(
				imageTag,
				null, /* size is determined at runtime */
				null, /* origin is 0,0 */
				null, /* anchor is bottom center of the scaled image */
				new google.maps.Size(50, 73)
				);

			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 14,
				center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),

				styles: stylez
			});

			var marker;
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
				map: map,
				icon: tag
			});
		}
	</script>
	<style>
		.page-gallery .inner-top-banner .sub-menu{
			background:<?php echo $product_background_color?>;
			color:<?php echo $product_text_color?>;
		}
		.sub-menu{
			background: #4a92cd !important;
			color: #000 !important;
		}
		.inner-top-banner .sub-menu a{
			color:#000 !important;
		}
		.page-gallery .photos .col a:nth-child(3):after,
		.page-gallery .photos .col a:nth-child(5):before{
			background-color:<?php echo $product_background_color?>;
		}
		.page-gallery .access-map h2,
		.page-gallery .photos h2 span,
		.page-gallery .about h2 span{
			color:<?php echo $product_background_color?>;
		}
		.photos h2 {
			width: 80%;
			margin: auto;
			line-height: 1em;
			margin-bottom: 40px;
		}
		
		.access-map h2, .photos h2 span, .about h2 span {
			color: #4a92cd;
			font-weight: 100;
			display: block;
			margin-left: 15%;
		}
		.about {
			position: relative;
			width: 80%;
			margin: 100px auto;
		}
		.hidden-block {
			max-width: 80%;
			margin: 0 auto;
		}
	</style>
</head>
<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<div class="top-divide-line"></div>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<section class="inner-top-banner">
			<?php if ($industrial["new_collaboration_banner"] != '' && file_exists($new_collaboration_cms_path . $industrial["new_collaboration_id"] . '/' . $industrial["new_collaboration_banner"])) { ?>
				<div class="banner-visual"><img src="<?php echo $new_collaboration_cms_url . $industrial["new_collaboration_id"] . '/' . $industrial["new_collaboration_banner"]; ?>" /></div>
			<?php } ?>
			<nav class="sub-menu">
				<div>
					<?php foreach ($sections_for_anchors AS $anchor) { ?>
						<a href="#section-<?php echo $anchor['new_collaboration_id']; ?>"><?php echo $anchor['new_collaboration_name_'.$lang]; ?></a>
					<?php } ?>
				</div>
			</nav>
		</section>

		<?php
		foreach ($sections_to_display as $s) {
			$_sections = DM::findAll('new_collaboration_section', 'new_collaboration_section_pid = ? AND new_collaboration_section_status = ?', '', array($s["new_collaboration_id"], STATUS_ENABLE));
			for ($i = 0; $i < sizeof($_sections); $i++) { ?>
				<?php $ncs = $_sections[$i]; ?>
				<?php if ($ncs['new_collaboration_section_type'] == 'T') { ?>
					<div style="padding:0.5px 0;background-color: <?php echo $ncs['new_collaboration_section_background_color']?>;">
						<section class="about section-<?php echo $s["new_collaboration_id"]; ?>">
							<?php echo GenTitle($ncs['new_collaboration_section_title_'.$lang]);?>
							<?php echo $ncs['new_collaboration_section_content_'.$lang]; ?>
						</section>
					</div>
				<?php } else if ($ncs['new_collaboration_section_type'] == 'CT') { ?>
					<div class="hidden-block">
						<h3><?php echo $ncs['new_collaboration_section_title_'.$lang]; ?></h3>
						<div class="hidden">
							<?php echo $ncs['new_collaboration_section_content_'.$lang]; ?>			
						</div>
					</div>
				<?php } else if ($ncs['new_collaboration_section_type'] == 'Photo') { ?>
					<?php $photos = DM::findAll('new_collaboration_section_file', 'new_collaboration_section_file_status = ? AND new_collaboration_section_file_psid = ?', 'new_collaboration_section_file_sequence', array(STATUS_ENABLE, $ncs["new_collaboration_section_id"])); ?>
					<section class="photos section-<?php echo $s["new_collaboration_id"]; ?>">
						<?php echo GenTitle($ncs['new_collaboration_section_title_'.$lang]);?>
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<?php $photo_count = 0; ?>
								<?php foreach ($photos AS $photo) { ?>
									<?php if ($photo_count % 8 == 0) { ?>
										<div class="swiper-slide col">
										<?php } ?>
										<a href="<?php echo $new_collaboration_file_cms_url.$photo['new_collaboration_section_file_pid'].'/'.$photo['new_collaboration_section_file_id'].'/'.$photo['new_collaboration_section_file_filename']; ?>"><img src="<?php echo $new_collaboration_file_cms_url.$photo['new_collaboration_section_file_pid'].'/'.$photo['new_collaboration_section_file_id'].'/'.'crop_'.$photo['new_collaboration_section_file_filename']; ?>" /></a>

										<?php if ($photo_count % 8 == 7) { ?>
										</div>
									<?php } ?>
									<?php $photo_count++; ?>
								<?php } ?>
							</div>

							<div class="swiper-pagination"></div>
						</div>
					</section>
				<?php } elseif ($ncs['new_collaboration_section_type'] == 'Visit') { ?>
					<section class="access-map section-<?php echo $s["new_collaboration_id"]; ?>">
						<div class="access-info">
							<?php echo GenTitle($ncs['new_collaboration_section_title_'.$lang]);?>
							<?php echo $ncs['new_collaboration_section_content_'.$lang]; ?>
						</div>
						<div id="map" class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.2681261428024!2d114.25116361467325!3d22.305697185320742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340403efc89ad0bd%3A0xd45da19c45e22088!2sHKDI%20Gallery!5e0!3m2!1sen!2s!4v1643109476328!5m2!1sen!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
					</section>
				<?php } ?>
			<?php } ?>
		<?php } ?>

	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>
</html>