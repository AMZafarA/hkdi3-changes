<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Programmes';
$section = 'programmes';
$subsection = 'alumni';
$sub_nav = 'alumni';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$talent_list = DM::findAllWithLimit($DB_STATUS.'talent', 6, ' talent_status = ? ', " talent_seq DESC", array(STATUS_ENABLE));


$product = DM::findOne($DB_STATUS.'product', 'product_status = ? and product_type = ? ', 'product_id desc', array(STATUS_ENABLE, '6'));

$product_banner = $product['product_thumb_banner'];
$product_id =  $product['product_id'];
if($product_banner){
	$product_banner  = $product_url.$product_id."/".$product_banner;
}

$breadcrumb_arr['Emerging Design Talent'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        	<script type="text/javascript" src="<?php echo $host_name?>js/edt.js "></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>Emerging</strong>
								<br>Design
								<br>Talent
							</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">Talent – the best designers exude it and you can benefit from it. This page showcases the outstanding HKDI alumni you can collaborate with, and highlights the annual Emerging Design Talent graduation show you can experience. </p>
					</div>
				</div>
			</div>
			<section class="gallery-exhib">
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
				<div class="gallery-exhib__wrapper">
					<div class="cards cards--3-for-dt">
						<div class="cards__outer">
							<div class="cards__inner">
								<?php foreach ($talent_list AS $talent) { ?>
								<?php 	$talent_image_list = DM::findAll($DB_STATUS.'talent_image', ' talent_image_status = ? AND talent_image_pid = ?', " talent_image_seq", array(STATUS_ENABLE, $talent['talent_id'])); ?>
								<div class="card is-collapsed">
									<div class="card__inner">
										<div class="card__img">
											<?php if ($talent['talent_thumb'] != '' && file_exists($talent_path.$talent['talent_id'].'/'.$talent['talent_thumb'])) { ?>
											<img src="<?php echo $talent_url.$talent['talent_id'].'/'.$talent['talent_thumb']; ?>" alt="" />
											<?php } ?>
										</div>
										<div class="card__ppl">
											<div class="card__ppl-pic">
												<?php if ($talent['talent_image'] != '' && file_exists($talent_path.$talent['talent_id'].'/'.$talent['talent_image'])) { ?>
												<img src="<?php echo $talent_url.$talent['talent_id'].'/'.$talent['talent_image']; ?>" alt="" />
												<?php } ?>
											</div>
											<div class="card__ppl-txt">
												<strong class="card__ppl-name"><?php echo $talent['talent_name_'.$lang]; ?></strong>
												<p class="card__ppl-title"><?php echo $talent['talent_job_title_'.$lang]; ?></p>
											</div>
										</div>
										<div class="card__content">
											<p class="desc">Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
											<a href="#" class="btn js-expander">More</a>
										</div>
									</div>
									<div class="card__expander">
										<div class="profile-block">
											<div class="profile-block__detail">
												<h3 class="profile-block__name"><?php echo $talent['talent_name_'.$lang]; ?></h3>
												<p class="profile-block__title"><?php echo $talent['talent_job_title_'.$lang]; ?></p>
												<div class="profile-block__desc">
													<p><?php echo $talent['talent_desc_'.$lang]; ?></p>
												</div>
												<?php echo $talent['talent_detail_'.$lang]; ?>
												<div class="profile-block__control">
												<?php if($talent['talent_email']){ ?>
													<div class="profile-block__control-item">
														<a href="mailto:<?php echo $talent['talent_email']?>" class="btn btn--solid-white profile-block__btn-switch">Contact</a>
													</div>
												<?php } ?>
													<!--
													<div class="profile-block__control-item">
														<h4 class="profile-block__control-title">Share</h4>
														<a href="#" class="btn-share btn-share--white btn-share--fb"></a>
														<a href="#" class="btn-share btn-share--white btn-share--email"></a>
														<a href="#" class="btn-share btn-share--white btn-share--tw"></a>
													</div>
												-->
												</div>
											</div>
											<div class="profile-block__gallery">
												<div class="profile-block__gallery-slider">
													<div class="swiper-wrapper">
														<?php foreach ($talent_image_list AS $talent_image) { ?>
														<div class="profile-block__slide swiper-slide">
															<img src="<?php echo $talent_url.$talent['talent_id'].'/'.$talent_image['talent_image_id'].'/'.$talent_image['talent_image_filename']; ?>" alt="" />
														</div>
														<?php } ?>
													</div>
													<div class="profile-block__gallery-pagination"></div>
												</div>
											</div>
										</div>
										<div class="profile-block__contact">
											<div class="sec-intro sec-intro--txt-white">
												<h2 class="sec-intro__sub-title">
													<strong>Contact</strong> Form</h2>
											</div>
											<div class="contact-form">
												<form class="contact-form__form form-container">
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="contact_name" placeHolder="Name" class="form-check--empty" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
															</div>
														</div>
														<div class="field field--1-2 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email" />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<input type="text" name="contact_inquiry" placeHolder="Inquiry" class="form-check--empty " />
															</div>
														</div>
													</div>
													<div class="field-row">
														<div class="field field--1-1 field--mb-1-1">
															<div class="field__holder">
																<div class="captcha">
																	<div class="captcha__img">
																		<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
																	</div>
																	<div class="captcha__control">
																		<div class="captcha__input">
																			<input type="text" name="captcha" class="form-check--empty" />
																		</div>
																		<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="btn-row">
														<div class="err-msg-holder">
															<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
															<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
														</div>
														<div class="btn-row btn-row--al-hl">
															<a href="<?php echo $host_name_with_lang ?>" class="btn btn--solid-white btn-send">Send</a>
															<a href="<?php echo $host_name_with_lang ?>" class="btn btn--white profile-block__btn-switch">Cancel</a>
														</div>
													</div>
												</form>
												<div class="contact-form__success-msg">
													<h3 class="desc-subtitle">Thank you!</h3>
													<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="gallery-exhib__btn-row btn-row">
						<!--<a href="#" class="btn">More</a>-->
					</div>
				</div>
			</section>
			<?php if($product_banner){ ?>
			<section class="banner">
				<div class="banner-visual">
					<a href="edt.php"><img style="display:block;width:100%;" src="<?php echo $product_banner ?>" /></a>
				</div>
			</section>
			<?php } ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>