<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'News';
$section = 'news';
$subsection = 'publication';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$preview = @$_REQUEST["preview"] ?? false;
$issue_id = @$_REQUEST['issue_id'] ?? '';
if($issue_id){
	if($preview) {
		$issue = DM::findOne($DB_STATUS.'issue', ' issue_id = ?', '', array($issue_id));
	}
	else {
		$issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ? AND issue_id = ?', '', array(STATUS_ENABLE, $issue_id));
	}
}else{
	if($preview) {
		$issue = DM::findOne($DB_STATUS.'issue', '', 'issue_date DESC', array());
	}
	else {
		$issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ? ', 'issue_date DESC', array(STATUS_ENABLE));
	}
}
$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));
$latest_product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_signed_id = ? AND product_type = ?', 'product_seq desc', array(STATUS_ENABLE, $issue['issue_id'], '2'));
$product_list = DM::findAll($DB_STATUS.'product', 'product_status = ? AND product_signed_id = ? AND product_type = ? AND product_id <> ?', 'product_seq desc', array(STATUS_ENABLE, $issue['issue_id'], '2', $latest_product['product_id']));


$breadcrumb_arr['News'] =$host_name_with_lang.'news/';
$breadcrumb_arr[$issue['issue_name_'.$lang]] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script>
			$(document).ready(function() {
				$('#issue_selector').change(function() {
					window.location.href = "publication.php?issue_id=" + $(this).find('option:selected').data('issue_id');
				});
			});
		</script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<section class="publication-entry">
				<div class="page-head">
					<div class="page-head__wrapper">
						<div class="page-head__title-holder">
							<div class="page-head__title">
								<h2>
									<strong>SIGNED</strong>
								</h2>
							</div>
							<div class="page-head__subtitle">
								<h3>
									<strong>The Magazine of HKDI</strong>
								</h3>
							</div>
							<div class="publication-entry__control">
								<div class="custom-select">
									<select id="issue_selector">
										<?php foreach ($issue_list AS $temp_issue) { ?>
										<option value="<?php echo $temp_issue['issue_id']; ?>" data-issue_id="<?php echo $temp_issue['issue_id']; ?>" <?php echo ($temp_issue['issue_id'] == $issue_id ? 'selected' : ''); ?>><?php echo $temp_issue['issue_name_'.$lang]; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="page-head__tag-holder">
							<p class="page-head__tag">First published by the HKDI in December 2011, SIGNED is a magazine with an international perspective that features
								outstanding work in product, interior, communication, digital, fashion and image design. Each issue consists of world-class
								features that reveal the fruits of HKDI’s commitment to creativity, sustainability and multicultural scholarship.</p>
						</div>
					</div>
				</div>
				<div class="content-wrapper">
					<div class="publication-entry__intro">
						<div class="publication-entry__thumb" style="<?php if ($issue['issue_inside_page_image'] != '' && file_exists($issue_path.$issue['issue_id']."/".$issue['issue_inside_page_image'])) { ?>background-image:url(<?php echo $issue_url.$issue['issue_id']."/".$issue['issue_inside_page_image']; ?>);<?php } ?>">
							<?php if ($issue['issue_inside_page_image'] != '' && file_exists($issue_path.$issue['issue_id']."/".$issue['issue_inside_page_image'])) { ?>
							<img src="<?php echo $issue_url.$issue['issue_id']."/".$issue['issue_inside_page_image']; ?>" alt="" />
							<?php } ?>
						</div>
						<div class="publication-entry__hightlight">
							<div class="publication-entry__hightlight-img">
								<?php if ($latest_product['product_banner'] != '' && file_exists($product_path.$latest_product['product_id']."/".$latest_product['product_banner'])) {  ?>
								<img src="<?php echo $product_url.$latest_product['product_id']."/".$latest_product['product_banner']; ?>" alt="" />
								<?php } ?>
							</div>
							<div class="publication-entry__hightlight-txt">
								<h3 class="publication-entry__hightlight-title"><?php echo $latest_product['product_name_'.$lang]; ?></h3>
								<a href="publication-detail.php?product_id=<?php echo $latest_product['product_id']; ?>" class="btn-arrow"></a>
							</div>
						</div>
					</div>
					<hr>
					<div class="publication-entry__list">
						<section class="programme-news ani">
							<?php foreach ($product_list AS $temp_product) { ?>
							<a class="block" href="publication-detail.php?product_id=<?php echo $temp_product['product_id']; ?>">
								<?php if ($temp_product['product_banner'] != '' && file_exists($product_path.$temp_product['product_id']."/".$temp_product['product_banner'])) {  ?>
								<img src="<?php echo $product_url.$temp_product['product_id']."/".$temp_product['product_banner']; ?>" alt="" />
								<?php } ?>
								<div class="txt">
									<p class="date">Latest News | <?php echo date('j F Y', strtotime($issue['issue_date'])); ?></p>
									<p><?php echo $temp_product['product_name_'.$lang]; ?></p>
									<p class="btn-arrow"></p>
								</div>
							</a>
							<?php } ?>
						</section>
					</div>
				</div>
			</section>

			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>