<section class="exhib-contact">
				<div class="exhib-contact__wrapper">
					<div class="sec-intro">
						<h2 class="sec-intro__title">
							<strong>RSVP/ Enquiry</strong>
					</div>
					<div class="contact-form">
						<div class="contact-form__info">
							<h3 class="contact-form__info-title">Contact</h3>
							<div class="contact-form__info-row">
								<?php if($rsvp_address){ ?>
								<div class="contact-form__info-txt">
									<p><?php echo $rsvp_address?></p>
								</div>
								<?php } ?>

								<?php if($rsvp_mail || $rsvp_tel){ ?>
								<div class="contact-form__info-contact">
									<?php if($rsvp_mail){ ?>
									<a href="mailto:<?php echo $rsvp_mail?>" class="contact-form__contact-item"><?php echo $rsvp_mail?></a>
									<?php } ?>
									<?php if($rsvp_tel){ ?>
									<a href="tel:39282894" class="contact-form__contact-item">Tel <?php echo $rsvp_tel?></a>
									<?php } ?>
								</div>
								<?php } ?>
							</div>
						</div>
						<form class="contact-form__form form-container">
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="field__inline-txt">
											<strong>Title</strong>
										</div>
										<div class="field__inline-field">
											<div class="custom-select">
												<select name="contact_title">
													<option value="mr">Mr.</option>
													<option value="mrs">Mrs.</option>
													<option value="ms">Ms.</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_first_name" placeHolder="Fist Name" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_last_name" placeHolder="Last Name" class="form-check--empty" />
									</div>
								</div>
								
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_position" placeHolder="Position" class="form-check--empty" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email" />
									</div>
								</div>
							</div>

							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_msg" placeHolder="Message" />
									</div>
								</div>
							</div>
							<!--<br>
							<p class="contact-form__desc">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel2" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="field__inline-txt">
											<strong>No. of ticket(s)</strong>
										</div>
										<div class="field__inline-field">
											<div class="custom-select">
												<select name="contact_no_ticket">
													<option value="1">1</option>
													<option value="2">2</option>
												</select>
											</div>
										</div>
										<div class="field__inline-txt">(1 ticket is offered: additional ticket will be subject to availability.)</div>
									</div>
								</div>
							</div>-->
							<!--<p class="contact-form__tnc">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor
								sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie
								malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Proin eget tortor risus. Quisque velit nisi,
								pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan
								id imperdiet et, porttitor at sem. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet
								et, porttitor at sem.</p>-->
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="captcha">
											<div class="captcha__img">
												<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
											</div>
											<div class="captcha__control">
												<div class="captcha__input">
													<input type="text" name="captcha" class="form-check--empty" placeholder="Captcha" />
												</div>
												<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<br />
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<h3 class="contact-form__info-title">Notes for registration</h3>
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<ol class="num-list">
											<li>The personal data collected will only be used by HKDI & IVE (Lee Wai Lee) to process registration to the captioned event and for related internal reporting purposes.</li>
											<li>HKDI & IVE (Lee Wai Lee) reserves the right to accept any registration at its sole discretion. If any information provided by the applicant is found to be untrue, HKDI & IVE (Lee Wai Lee) may reject the registration and withdraw any confirmation given.</li>
											<li>In case of cancellation of registration, registrant should inform HKDI & IVE (Lee Wai Lee) in writing at least two working days before the event by emailing to <a class="content-link" href="mailto:eao-dilwl@vtc.edu.hk"><u>eao-dilwl@vtc.edu.hk</u></a>.</li>
										</ol>
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<div class="custom-checkbox form-check--empty">
											<input id="P_Agree1" type="checkbox" name="P_Agree1" />
											<label for="P_Agree1"><strong>I agree and accept the rules and notes as stated above. </strong></label>
										</div>
									</div>
								</div>
							</div>
							<div class="btn-row">
								<?php include $inc_lang_path."inc_common/inc_form_err_msg.php"; ?>
								<div class="err-msg-holder">
									<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
									<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
								</div>
								<div class="btn-row btn-row--al-hl">
									<a href="<?php echo $host_name_with_lang ?>" class="btn btn-send">
										<strong>Send</strong>
									</a>
								</div>
							</div>
							<input type="hidden" name="rsvp_id" value="<?php echo $rsvp_id?>">
							<input type="hidden" name="lang" value="<?php echo $lang?>">
						</form>
						
						<div class="contact-form__success-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">Thank you for your registration. We look forward to meeting you in person!<br><br>Should you have any enquiry, please do not hesitate to contact us.</p>
						</div>
						<div class="contact-form__waiting-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">Thank you for showing interest in our event. It has received overwhelmed support and is fully booked. Your new registration will be included on the waiting list. Our colleague will contact you soon if we are allowed to accommodate more guests.</p>
						</div>
					</div>
				</div>
			</section>