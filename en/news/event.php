<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'News';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';

$expired = false;

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$name = @$_REQUEST['name'] ?? '';

$rsvp = DM::findOne($DB_STATUS.'rsvp', ' rsvp_status = ? AND rsvp_url_alias = ? AND rsvp_display_from_date <= ? ', "rsvp_id DESC", array(STATUS_ENABLE,$name,$today));

if ($name == '' || $rsvp === NULL)
{
	header("location: ../index.php");
	exit();
}

$rsvp_id = $rsvp['rsvp_id'];
$rsvp_banner = $rsvp['rsvp_banner'];
if ($rsvp_banner  || file_exists($rsvp_path.$rsvp_id."/".$rsvp_banner)){
	$rsvp_banner = $rsvp_url.$rsvp_id."/".$rsvp_banner;
}

$rsvp_name = $rsvp['rsvp_name_'.$lang];
$rsvp_detail = $rsvp['rsvp_detail_'.$lang];
$rsvp_expired_detail = $rsvp['rsvp_expired_detail_'.$lang];
$rsvp_address = nl2br($rsvp['rsvp_address_'.$lang]);
$rsvp_type = $rsvp['rsvp_type'];
$rsvp_mail = $rsvp['rsvp_mail'];
$rsvp_tel = $rsvp['rsvp_tel'];
$rsvp_display_to_date = $rsvp['rsvp_display_to_date'];


if($rsvp_display_to_date < $today){
	$expired = true;
}

switch ($rsvp_type) {
	case '0':
		$form = 'formA';
		break;
	case '1':
		$form = 'formB';
		break;
	case '2':
		$form = 'formC';
		break;
	case '3':
		$form = 'formD';
		break;
	default:
		# code...
		break;
}

$page_title = $rsvp_name;
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/event.js "></script>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<!--<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>-->
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>EVENT</strong> RSVP</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="event-banner">
				<img src="<?php echo $rsvp_banner?>" />
			</section>
			<?php
				if($expired){
			?>
			<section class="event-intro">
				<div class="content-wrapper">
					<?php echo $rsvp_expired_detail?>
				</div>
			</section>
			<?php
				}else{
					if($rsvp_detail){
			?>
						<section class="event-intro">
							<div class="content-wrapper">
								<h2 class="event-intro__title">Event Description</h2>
								<?php echo $rsvp_detail?>
							</div>
						</section>
					<?php } ?>
			<?php include $inc_lang_path . "news/inc_form/".$form.".php"; ?>
		<?php } ?>

			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>