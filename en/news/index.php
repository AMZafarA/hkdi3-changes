<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'News';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';

$breadcrumb_arr['News'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);

$conditions = "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, $today, $today);
$news_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, " news_date DESC, news_id DESC", $parameters);
$total_news_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => '最新動態', 'lang3' => '最新动态'),
//	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => '學生得獎作品', 'lang3' => '学生得奖作品'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => '活動', 'lang3' => '活动'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => '其他活動', 'lang3' => '其他活动')
);
for ($i = 0; $i < sizeof($news_types); $i++)
{
	$news_page_no = ($tab == $news_types[$i]['value'] ? $page : 1);
	$parameters2 = array_merge($parameters, array($news_types[$i]['value']));
	// combine hkdi_news and student_award
	// $news_types[0] = hkdi_news;
	if($i == 0){
	$conditions2 = $conditions." AND (news_type = ? OR news_type = ?)";
	array_push($parameters2, 'student_award');
	}else{
		$conditions2 = $conditions." AND news_type = ?";
	}
	$temp_news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions2, " news_date DESC, news_id DESC", $parameters2);
	$temp_news_total_count = DM::count($DB_STATUS.'news', $conditions2, $parameters2);
	$news_types[$i]['list'] = $temp_news_list;
	$news_types[$i]['total_count'] = $temp_news_total_count;
}


$latest_issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));
$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ? AND issue_id <> ?', 'issue_date DESC', array(STATUS_ENABLE, $latest_issue['issue_id']));
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>HKDI</strong> NEWS</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-all" class="page-tabs__btn <?php echo ($tab == 'all' ? 'is-active' : ''); ?>">
							<span>ALL</span>
						</a>
						<?php foreach ($news_types AS $news_type) { ?>
						<?php	if (sizeof($news_type['list']) == 0) { continue; } ?>
						<a href="#tabs-<?php echo $news_type['value']; ?>" class="page-tabs__btn <?php echo ($tab == $news_type['value'] ? 'is-active' : ''); ?>">
							<span><?php echo $news_type[$lang]; ?></span>
						</a>
						<?php } ?>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__wrapper">
						<?php $is_curr_tab = ($tab == '' || $tab == 'all' ? TRUE : FALSE); ?>
						<div id="tabs-all" class="page-tabs__content <?php echo ($tab == 'all' ? 'is-active' : ''); ?>">
							<section class="programme-news ani">
								<?php foreach ($news_list AS $news) { ?>
								<?php if ($news['news_thumb'] != '' && ( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) || file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) ) { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<?php if( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) ) { ?>
										<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
									<?php } else if( file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) { ?>
										<img src="<?php echo str_replace("uploaded_files","CMS/uploaded_files",$news_url).$news['news_id']."/".$news['news_thumb']; ?>" />
									<?php } ?>
									<div class="txt">
										<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
										<p><?php echo $news['news_name_'.$lang]; ?></p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} else { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
									<div class="table">
										<div class="cell">
											<p>
												<strong>Latest News</strong>
											</p>
											<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
										</div>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} ?>
								<?php } ?>
							</section>

							<?php if ($total_news_count > $news_page_size) { ?>
							<div class="pagination">
								<?php if ($is_curr_tab && $page > 1) { ?>
								<a href="index.php?tab=all&page=<?php echo ($page-1); ?>" class="pagination__btn-prev">Previous</a>
								<?php } ?>
								<div class="pagination__pages">
									<div class="pagination__current">
										<div class="pagination__current-pg"><?php echo ($tab == 'all' ? $page : 1); ?></div>
									</div>
									<span>of</span>
									<div class="pagination__total"><?php echo ceil($total_news_count / $news_page_size); ?></div>
								</div>
								<?php if ($total_news_count > $news_page_size * ($is_curr_tab ? $page : 1)) { ?>
								<a href="index.php?tab=all&page=<?php echo ($is_curr_tab ? $page+1 : 2); ?>" class="pagination__btn-next">Next</a>
								<?php } ?>
							</div>
							<?php } ?>
						</div>

						<?php foreach ($news_types AS $news_type) { ?>
						<?php 	$is_curr_tab = ($tab == $news_type['value'] ? TRUE : FALSE); ?>
						<div id="tabs-<?php echo $news_type['value']; ?>" class="page-tabs__content <?php echo ($tab == $news_type['value'] ? 'is-active' : ''); ?>">
							<section class="programme-news ani">
								<?php foreach ($news_type['list'] AS $news) { ?>
								<?php if ($news['news_thumb'] != '' && ( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) || file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) ) { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&news_type=<?php echo $news['news_type']; ?>&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<?php if( file_exists($news_path.$news['news_id']."/".$news['news_thumb']) ) { ?>
										<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
									<?php } else if( file_exists(str_replace("uploaded_files","CMS/uploaded_files",$news_path).$news['news_id']."/".$news['news_thumb']) ) { ?>
										<img src="<?php echo str_replace("uploaded_files","CMS/uploaded_files",$news_url).$news['news_id']."/".$news['news_thumb']; ?>" />
									<?php } ?>
									<div class="txt">
										<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
										<p><?php echo $news['news_name_'.$lang]; ?></p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} else { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&news_type=<?php echo $news['news_type']; ?>&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
									<div class="table">
										<div class="cell">
											<p>
												<strong>Latest News</strong>
											</p>
											<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
										</div>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} ?>
								<?php } ?>
							</section>

							<?php if ($news_type['total_count'] > $news_page_size) { ?>
							<div class="pagination">
								<?php if ($is_curr_tab && $page > 1) { ?>
								<a href="index.php?tab=<?php echo $news_type['value']; ?>&page=<?php echo ($page-1); ?>" class="pagination__btn-prev">Previous</a>
								<?php } ?>
								<div class="pagination__pages">
									<div class="pagination__current">
										<div class="pagination__current-pg"><?php echo ($tab == $news_type['value'] ? $page : 1); ?></div>
									</div>
									<span>of</span>
									<div class="pagination__total"><?php echo ceil($news_type['total_count'] / $news_page_size); ?></div>
								</div>
								<?php if ($news_type['total_count'] > $news_page_size * ($is_curr_tab ? $page : 1)) { ?>
								<a href="index.php?tab=<?php echo $news_type['value']; ?>&page=<?php echo ($is_curr_tab ? $page+1 : 2); ?>" class="pagination__btn-next">Next</a>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>
			<?php /*
			<section class="sec-publication">
				<div class="content-wrapper">
					<div class="sec-publication__intro">
						<div class="sec-publication__detail">
							<div class="sec-intro">
								<h2 class="sec-intro__title">
									<strong>Publication:</strong>
									Signed
								</h2>
							</div>
							<h3 class="sec-publication__subtitle">The Magazine of HKDI</h3>
							<p class="sec-publication__desc">First published by the HKDI in December 2011, SIGNED is a magazine with an international perspective that features
								outstanding work in product, interior, communication, digital, fashion and image design. Each issue consists of world-class
								features that reveal the fruits of HKDI’s commitment to creativity, sustainability and multicultural scholarship.</p>
							<div class="sec-publication__control">
								<div class="sec-publication__subtitle"><?php echo $latest_issue['issue_name_'.$lang]; ?></div>
								<div class="sec-publication__btn">
									<a href="publication.php?issue_id=<?php echo $latest_issue['issue_id']; ?>" class="btn">
										<strong>Read</strong>
									</a>
								</div>
							</div>
						</div>
						<div class="sec-publication__img">
							<?php if ($latest_issue['issue_cover_image'] != '' && file_exists($issue_path.$latest_issue['issue_id']."/".$latest_issue['issue_cover_image'])) {  ?>
							<img src="<?php echo $issue_url.$latest_issue['issue_id']."/".$latest_issue['issue_cover_image']; ?>" />
							<?php } ?>
						</div>
					</div>
					<hr />
					<div class="sec-publication__list">
						<div class="sec-publication__list-control">
							<div class="sec-publication__list-pagination swiper-pagination"></div>
						</div>
						<div class="sec-publication__list-slider">
							<div class="swiper-wrapper">
								<?php foreach ($issue_list AS $issue) { ?>
								<a href="publication.php?issue_id=<?php echo $issue['issue_id']; ?>" class="sec-publication__list-slide swiper-slide">
									<?php if ($issue['issue_cover_image'] != '' && file_exists($issue_path.$issue['issue_id']."/".$issue['issue_cover_image'])) {  ?>
									<img src="<?php echo $issue_url.$issue['issue_id']."/".$issue['issue_cover_image']; ?>" />
									<?php } ?>
									<div class="sec-publication__list-txt">
										<h3 class="sec-publication__list-title"><?php echo $issue['issue_name_'.$lang]; ?></h3>
										<time class="sec-publication__list-date"><?php echo date('j.n.y', strtotime($news['news_date'])); ?></time>
									</div>
								</a>
								<?php } ?>
							</div>
						</div>
					</div>
			</section>
			*/ ?>
			<section class="sec-publication">
				<div class="content-wrapper">
					<div class="sec-publication__intro">
						<div class="sec-publication__detail">
							<div class="sec-intro">
								<h2 class="sec-intro__title">
									<strong>Publication:</strong>
									Signed
								</h2>
							</div>
							<?php
								$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));

								$issue_id = $issue_list[0]['issue_id'];
								$issue_cover_image = $issue_list[0]['issue_cover_image'];
								$issue_name = $issue_list[0]['issue_name_'.$lang];

								if ($issue_list[0]['issue_cover_image'] != '' && file_exists($issue_path.$issue_id."/".$issue_cover_image)) {
									$issue_cover_image = $issue_url.$issue_id."/".$issue_cover_image;
								}
							?>
							<h3 class="sec-publication__subtitle">The Magazine of HKDI</h3>
							<p class="sec-publication__desc">First published by the HKDI in December 2011, SIGNED is a magazine with an international perspective that features outstanding work in product, interior, communication, digital, fashion and image design. Each issue consists of world-class features that reveal the fruits of HKDI’s commitment to creativity, sustainability and multicultural scholarship.</p>
							<div class="sec-publication__control">
								<div class="sec-publication__subtitle"><?php echo $issue_name?></div>
								<div class="sec-publication__btn">
									<a href="<?php echo $host_name_with_lang."news/publication.php?issue_id=".$issue_id?>" class="btn" >
										<strong>Read</strong>
									</a>
								</div>
							</div>
						</div>
						<div class="sec-publication__img">
							<img src="<?php echo $issue_cover_image?>" />
						</div>
					</div>
					<hr />
					<div class="sec-publication__list">
						<div class="sec-publication__list-control">
							<div class="sec-publication__list-pagination swiper-pagination"></div>
						</div>
						<div class="sec-publication__list-slider">
							<div class="swiper-wrapper">
								<!--<?php for ($i = 17; $i > 0; $i--) { ?>
								<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="sec-publication__list-slide swiper-slide" target="_blank">
									<img src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" />
									<div class="sec-publication__list-txt">
										<h3 class="sec-publication__list-title">ISSUE No. <?php echo $i; ?></h3>
									</div>
								</a>
								<?php } ?>-->

								<?php

									foreach ($issue_list AS $issue) {
										$issue_id = $issue['issue_id'];
										$issue_cover_image = $issue['issue_cover_image'];
										$issue_name = $issue['issue_name_'.$lang];

										if ($issue['issue_cover_image'] != '' && file_exists($issue_path.$issue_id."/".$issue_cover_image)) {
											$issue_cover_image = $issue_url.$issue_id."/".$issue_cover_image;
										}
								?>
									<a href="<?php echo $host_name_with_lang."news/publication.php?issue_id=".$issue_id?>" class="sec-publication__list-slide swiper-slide" >
										<img src="<?php echo $issue_cover_image?>" />
										<div class="sec-publication__list-txt">
											<h3 class="sec-publication__list-title"><?php echo $issue_name?></h3>
										</div>
									</a>
								<?php } ?>


								<?php for ($i = 14; $i > 0; $i--) { ?>
								<a href="http://www.hkdi.edu.hk/signed/?m=3&v=<?php echo $i; ?>" class="sec-publication__list-slide swiper-slide" target="_blank">
									<img src="http://www.hkdi.edu.hk/signed/jpg/<?php echo $i; ?>/s/thumb/1.jpg" />
									<div class="sec-publication__list-txt">
										<h3 class="sec-publication__list-title">ISSUE No. <?php echo $i; ?></h3>
									</div>
								</a>
								<?php } ?>

							</div>
						</div>
					</div>
			</section>
			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
