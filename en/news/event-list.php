<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'News';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page page-event-list" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>EVENT</strong> RSVP</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="cards cards--4-for-dt cards--event">
				<div class="cards__outer">
					<div class="cards__inner">
						<!--1-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-1.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 1-->
						<!--2-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-2.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 2-->
						<!--3-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-3.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 3-->
						<!--4-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-4.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 4-->
						<!--5-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-5.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 5-->
						<!--6-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-6.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 6-->
						<!--7-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-7.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 7-->
						<!--8-->
						<div class="card is-collapsed">
							<div class="card__inner">
								<div class="programme-news ani">
									<a class="block js-expander" href="#">
										<img src="<?php echo $inc_root_path ?>images/news/img-publication-img-8.jpg" />
										<div class="txt">
											<p class="date">Latest News | 28 June 2017</p>
											<p>Five HKDI graduates win DFA Hong Kong Young Design Talent Awards</p>
											<p class="btn-arrow"></p>
										</div>
									</a>
								</div>
							</div>
							<div class="card__expander">
								<div class="card__desc-box">
									<div class="video-bg">
										<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
										</div>
									</div>
									<section class="event-intro">
										<div class="content-wrapper">
											<h2 class="event-intro__title">Event Description</h2>
											<h3 class="event-intro__subtitle"><span class="txt-highlight txt-highlight--black">FASHION LEAGUE 2017 POP-UP STORE EXHIBITION & INTERACTIVE WORKSHOP</span></h3>
											<p class="event-intro__info">
												Date: 6 – 14 June [10:30am – 7pm]
												<br> Venue: 1/F Water Zone, ELEMENTS shopping mall, Kowloon
											</p>
											<p class="event-intro__desc">The Hong Kong Design Institute Fashion League 2017 Pop-up Store Exhibition & Interactive Workshop is the consolidated
												efforts of Fashion Branding and Buying graduates and students. The exhibition demonstrates to the public the
												career paths of students from Higher Diploma in Fashion Branding and Buying, and Higher Diploma in Fashion Media
												Design. Graduates from the programmes will share their learning experience interactively with participants. The
												Opening Ceremony will be held at 5pm on 9 June.</p>
											<p class="event-intro__info">RSVP/ enquiry contact:
												<a href="tel:39282900">Ms Bobo Chan at 3928 2900/</a>
												<a href="mailto:bobochan@vtc.edu.hk">bobochan@vtc.edu.hk</a>
											</p>
										</div>
									</section>
								</div>
							</div>
						</div>
						<!--END 8-->

					</div>
				</div>
			</div>



			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>