<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '';
$section = '';
$subsection = '';
$sub_nav = '';

DM::setup($db_host, $db_name, $db_username, $db_pwd);
$product_id = @$_REQUEST['product_id'] ?? '';
$product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_id = ?', '', array(STATUS_ENABLE, $product_id));
$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, $product_id));
$news_lv2 = '';
switch ($product_id) {
	case 4:
		$news_lv2 = 17;
		$contactBg = '#ebcc3b';
		break;
	case 5:
		$news_lv2 = 18;
		$contactBg = '#79698f';
		break;
	case 6:
		$news_lv2 = 19;
		$contactBg = '#3d9f9b';
		break;
	case 7:
		$news_lv2 = 20;
		$contactBg = '#b77070';
		break;
	case 8:	//ccd
		$news_lv2 = 23;
		$contactBg = '#ebcc3b';
		break;
	case 153:	//cdss
		$news_lv2 = 24;
		$contactBg = '#aeaeae';
		break;
}
$news_page_no = (@$_REQUEST['news_page_no'] == '' ? 1 : $_REQUEST['news_page_no']);


$breadcrumb_arr['Knowledge Centres'] =$host_name_with_lang.'knowledge-centre';
$breadcrumb_arr[$product['product_name_'.$lang]] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
		<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
		<script type="text/javascript" src="<?php echo $host_name?>js/knowledge_centre.js"></script>
		<style>
			.contact-us{
				background: <?php echo $contactBg; ?> !important;
			}
			/*#section-11{
				background:#ebcc3b !important;
			}
			#section-37{
				background:#b77070 !important;
			}
			#section-58{
				background:#79698f !important;
			}
			#section-60{
				background:#3d9f9b !important;
			}*/
		</style>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="top-nav">
				<div class="top-nav-left">
					<!--<a class="back" href="index.php">Back</a>-->
				</div>
				<!--<div class="top-nav-center">
					<div class="top-nav__title"><?php echo $product['product_name_'.$lang]; ?></div>
				</div>-->
			</div>
			<section class="inner-top-banner">
				<div class="banner-logo">
					<?php if ($product['product_banner'] != '' && file_exists($product_path.$product['product_id'].'/'.$product['product_banner'])) { ?>
					<img src="<?php echo $product_url.$product['product_id'].'/'.$product['product_banner']; ?>" />
					<?php } ?>
				</div>
				<nav class="sub-menu">
					<div>
						<?php foreach ($product_sections AS $product_section) { ?>
						<a href="#section-<?php echo $product_section['product_section_id']; ?>">
							<?php if ($product_section['product_section_type'] == 'C') { ?>
							Contact Us
							<?php } else { ?>
							<?php echo $product_section['product_section_name_'.$lang]; ?>
							<?php } ?>
						</a>
						<?php } ?>
					</div>
				</nav>
			</section>
			<?php foreach ($product_sections AS $product_section) { ?>
			<?php 	echo generationSection($product_section, $product, $news_lv2, $news_page_no); ?>
			<?php } ?>

		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
<?php
	function generationSection($section, $product, $news_lv2, $news_page_no = 1) {
		global $lang, $today, $product_file_path, $product_file_url, $project_path, $project_url, $news_path, $news_url, $news_page_size, $host_name, $news_types_arr;
		$product_id = $product['product_id'];
		if ($section['product_section_type'] == 'T') {?>
			<section class="sec-basic sec-basic--black" id="section-<?php echo $section['product_section_id']; ?>" style="background-color: <?php echo $section['product_section_background_color']; ?>">
				<div class="content-wrapper">
					<div class="about-title">
						<strong><?php echo $section['product_section_title_'.$lang]; ?></strong>
					</div>
					<div class="sec-about__txt"><?php echo $section['product_section_content_'.$lang]; ?></div>
				</div>
			</section>
<?php		} else if ($section['product_section_type'] == 'Team') { ?>
<?php 			$logos = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $section['product_section_id'])); ?>
			<section class="sec-ack sec-ack--black" id="section-<?php echo $section['product_section_id']; ?>">
				<div class="content-wrapper">
					<h2 class="sec-ack__title">
						<strong style="color:#08f0f0;">HKDI <?php echo $product['product_name_'.$lang]; ?></strong>
						<br> Partners
					</h2>
					<div class="sec-ack__row">
						<?php foreach ($logos AS $logo) { ?>
						<?php 	if ($logo['product_section_file_filename'] == '' || !file_exists($product_file_path.$logo['product_section_file_pid'].'/'.$logo['product_section_file_id'].'/'.$logo['product_section_file_filename'])) { continue; } ?>
						<div class="sec-ack__item">
							<img src="<?php echo $product_file_url.$logo['product_section_file_pid'].'/'.$logo['product_section_file_id'].'/'.$logo['product_section_file_filename']; ?>" />
						</div>
						<?php } ?>
					</div>
				</div>
			</section>
<?php		} else if ($section['product_section_type'] == 'C') { ?>
	<section class="sec-basic contact-us"  style="background-color: <?php echo $section['product_section_background_color']; ?>!important"  id="section-<?php echo $section['product_section_id']; ?>">
				<div class="content-wrapper">
					<div class="sec-intro">
						<h2 class="sec-intro__title">
							<strong>Contact</strong> Us</h2>
					</div>
					<div class="contact-form">
						<div class="contact-form__info">
							<h3 class="contact-form__info-title">Contact</h3>
							<div class="contact-form__info-row">
								<div class="contact-form__info-txt">
									<p><?php echo $section['product_section_content_'.$lang]; ?></p>
								</div>
								<div class="contact-form__info-contact">
									<a href="mailto:<?php echo $section['product_section_email']; ?>" class="contact-form__contact-item">E-mail <?php echo $section['product_section_email']; ?></a>
									<a href="tel:39282894" class="contact-form__contact-item">Tel <?php echo $section['product_section_tel']; ?></a>
								</div>
							</div>
							<?php echo $section['product_section_title_lang1']?>
						</div>
						<form class="contact-form__form form-container" id="form-contact">
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_name" placeHolder="Name" class="form-check--empty" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_company" placeHolder="Company" class="form-check--empty" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_tel" placeHolder="Phone" maxlength="18" class="form-check--empty form-check--tel" />
									</div>
								</div>
								<div class="field field--1-2 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_email" placeHolder="Email" class="form-check--empty form-check--email" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<input type="text" name="contact_msg" placeHolder="Message" />
									</div>
								</div>
							</div>
							<div class="field-row">
								<div class="field field--1-1 field--mb-1-1">
									<div class="field__holder">
										<br>
										<div class="custom-checkbox">
											<input id="contact_book" type="checkbox" name="contact_inquiry" class="form-check--empty " />
											<label for="contact_book"><strong>Book a Visit</strong></label>
										</div>
									</div>
								</div>
							</div>
							<div class="tobe-shown" data-trigger="#contact_book">
								<div class="field-row">
									<div class="field field--1-1 field--mb-1-1">
										<div class="field__holder">
											<div class="field__inline-txt">
												<strong>Proposed booking date(s)</strong>
											</div>
											<div class="field__date-fields">
												<div class="field__date-field">
													<div class="field__date-label">From:</div>
													<input type="text" name="booking_date_from" class="form-check--empty" />
												</div>
												<div class="field__date-field">
													<div class="field__date-label">To:</div>
													<input type="text" name="booking_date_to" class="form-check--empty" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="field-row">
									<div class="field field--1-2 field--mb-1-1">
										<div class="field__holder">
											<input type="text" name="booking_participants" placeHolder="Estimated number of participants" class="form-check--empty" />
										</div>
									</div>
								</div>
							</div>

							<div class="captcha">
								<div class="captcha__img">
									<img id="captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
								</div>
								<div class="captcha__control">
									<div class="captcha__input">
										<input type="text" name="captcha" class="form-check--empty" placeholder="Captcha" />
									</div>
									<a href="#" class="captcha__btn" onClick="document.getElementById('captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
								</div>
							</div>
							<div class="btn-row">
								<div class="err-msg-holder">
									<p class="err-msg err-msg--empty">This field is required.</p>
									<p class="err-msg err-msg--tel">Please enter a valid mobile phone number </p>
									<p class="err-msg err-msg--email">E-mail is required field.</p>
									<p class="err-msg err-msg--email-format">Format of your email is invalid</p>
									<p class="err-msg err-msg--phone">Phone is required field.</p>
									<p class="err-msg err-msg--phone-format">Format of your phone number is invalid.</p>
									<p class="err-msg err-msg--tnc">You must accept the Terms and Conditions.</p>
									<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
									<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
								</div>
								<div class="btn-row btn-row--al-hl">
									<a href="#" class="btn btn-submit">
										<strong>Send</strong>
									</a>
								</div>
							</div>
							<input type="hidden" name="product_section_id" value="<?php echo $section['product_section_id']; ?>" />
							<input type="hidden" name="product_section_subject" value="<?php echo $product_section_subject; ?>" />

						</form>
						<div class="contact-form__success-msg">
							<h3 class="desc-subtitle">Thank you!</h3>
							<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
						</div>
					</div>
				</div>
			</section>
<?php		} else if ($section['product_section_type'] == 'P') { ?>
<?php 			$projects = DM::findAll($DB_STATUS.'project', 'project_status = ? AND project_lv1 = ? AND project_lv2 = ?', 'project_seq', array(STATUS_ENABLE, '16', $news_lv2)); ?>
			<section class="sec-projects" id="section-<?php echo $section['product_section_id']; ?>">
				<div class="content-wrapper">
					<h2 class="sec-projects__title"><?php echo $section['product_section_title_'.$lang]?></h2>
					<?php foreach ($projects AS $project) { ?>
					<div class="hidden-block">
						<h3><?php echo $project['project_name_'.$lang]; ?></h3>
						<div class="hidden">
							<p class="title">
								<span class="txt-highlight" style="background:#000;"><?php echo $project['project_name_'.$lang]; ?></span>
							</p>
							<div class="img-holder">
								<?php if ($project['project_image1'] != '' && file_exists($project_path.$project['project_id'].'/'.$project['project_image1'])) { ?>
								<img src="<?php echo $project_url.$project['project_id'].'/'.$project['project_image1']; ?>" />
								<?php } ?>
								<?php if ($project['project_image2'] != '' && file_exists($project_path.$project['project_id'].'/'.$project['project_image2'])) { ?>
								<img src="<?php echo $project_url.$project['project_id'].'/'.$project['project_image2']; ?>" />
								<?php } ?>
								<?php echo $project['project_detail_'.$lang]; ?>
							</div>

						</div>
					</div>
					<?php } ?>
				</div>
			</section>
<?php		} else if ($section['product_section_type'] == 'N') { ?>
			<?php
				$conditions = "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL) AND news_lv1 = ? AND news_lv2 = ?";
				$parameters = array(STATUS_ENABLE,$today,$today,'16',$news_lv2);
				$news_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
				$news_total_page_no = ceil($news_count / $news_page_size);
				$news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, "news_date DESC", $parameters);
			?>
			<section class="sec-basic" id="section-<?php echo $section['product_section_id']; ?>">
				<div class="content-wrapper">
					<div class="about-title">
						<strong><?php echo $section['product_section_title_'.$lang]; ?></strong>
						<!--<span class="about-title__tag">Events</span>-->
					</div>
					<div class="article-detail">
						<div class="programme-news ani">
							<?php foreach ($news_list AS $news) { ?>
							<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>">
								<img src="<?php echo $news_url.$news['news_id'].'/'.$news['news_thumb']; ?>" />
								<div class="txt">
									<?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?>
										<p><?php echo $news['news_name_'.$lang]; ?></p>
									<p class="btn-arrow"></p>
								</div>
							</a>
							<?php } ?>
						</div>
						<?php if ($news_count > $news_page_size) { ?>
						<div class="pagination">
							<?php if ($news_page_no > 1) { ?>
							<a href="detail.php?product_id=<?php echo $product_id; ?>&news_page_no=<?php echo ($news_page_no-1); ?>" class="pagination__btn-prev">Previous</a>
							<?php } ?>
							<div class="pagination__pages">
								<div class="pagination__current">
									<div class="pagination__current-pg"><?php echo $news_page_no; ?></div>
								</div>
								<span>of</span>
								<div class="pagination__total"><?php echo $news_total_page_no; ?></div>
							</div>
							<?php if ($news_page_no < $news_total_page_no) { ?>
							<a href="detail.php?product_id=<?php echo $product_id; ?>&news_page_no=<?php echo ($news_page_no+1); ?>" class="pagination__btn-next">Next</a>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</section>
<?php		}
	}
?>
