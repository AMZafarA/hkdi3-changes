<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Knowledge Centres';
$section = 'knowledge-centre';
$subsection = 'knowledge-centre';
$sub_nav = 'knowledge-centre';

//$breadcrumb_arr['Knowledge Centre'] ='';
$breadcrumb_arr['Knowledge Centres'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$product_cimt = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('4'));
$product_desis_lab = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('5'));
$product_media_lab = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('6'));
$product_fashion_archive = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('7'));
$product_ccd = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('8'));
$product_cdss = DM::findOne($DB_STATUS.'product', 'product_id = ?', '', array('153'));

?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>KNOWLEDGE</strong>
								<br>CENTRES</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">Through its Knowledge Centres, HKDI offers students access to interdisciplinary platforms and enables them to engage in projects with industry partners. </p>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='ccd.php'" style="background-image:url(<?php echo $product_url.$product_ccd['product_id'].'/'.$product_ccd['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>Centre for Communication Design</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_ccd['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="ccd.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='cdss.php'" style="background-image:url(<?php echo $product_url.$product_cdss['product_id'].'/'.$product_cdss['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>Centre of Design Services and Solutions</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_cdss['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="cdss.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='cimt.php'" style="background-image:url(<?php echo $product_url.$product_cimt['product_id'].'/'.$product_cimt['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>Centre of Innovative Material and Technology</span>
							<!--span>C</span>
							<span>I</span>
							<span>M</span>
							<span>T</span-->
						</h3>
						<p class="img-portal__desc"><?php echo $product_cimt['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="cimt.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='desis_lab.php'" style="background-image:url(<?php echo $product_url.$product_desis_lab['product_id'].'/'.$product_desis_lab['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>DESIS</span><span> Lab</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_desis_lab['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="desis_lab.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='fashion_archive.php'" style="background-image:url(<?php echo $product_url.$product_fashion_archive['product_id'].'/'.$product_fashion_archive['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>Fashion</span><span> Archive</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_fashion_archive['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="fashion_archive.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>
			<div class="img-portal" onclick="location.href='media_lab.php'" style="background-image:url(<?php echo $product_url.$product_media_lab['product_id'].'/'.$product_media_lab['product_thumb_banner']; ?>);">
				<div class="img-portal__wrapper content-wrapper">
					<div class="img-portal__txt">
						<h3 class="img-portal__title">
							<span>Media</span><span> Lab</span>
						</h3>
						<p class="img-portal__desc"><?php echo $product_media_lab['product_detail_'.$lang]?></p>
					</div>
					<div class="img-portal__btn">
						<a href="media_lab.php" class="btn btn--white">Detail</a>
					</div>
				</div>
			</div>


		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
