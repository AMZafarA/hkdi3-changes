<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Unsubscribe';
$section = 'unsubscribe';
$subsection = 'unsubscribe';
$sub_nav = 'unsubscribe';

$breadcrumb_arr['Unsubscribe'] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<style>
		.field-row .field label {display: flex; flex-flow: wrap; align-items: center;}
		.field-row .field input[type="checkbox"] {width: 20px;margin-right: 10px;}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title page-head__title--big">
						<h2>
							<strong>Unsubscribe</strong>
						</h2>
					</div>
				</div>
			</div>
		</div>
		<section class="unsubscribe-section">
			<p class="hide-after-submission">We are sorry to hear you no longer wish to receive our email newsletters. Please provide below information and submit.</p>
			<form class="unsubscribe-form__form form-container" id="unsubscribe-form">
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
							<label>
								<input type="checkbox" name="unsubscribe_checkbox" value="1">
								<p>Unsubscribe from all email communications</p>
							</label>
						</div>
					</div>
				</div>
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
							<input type="text" name="subscription_email" placeholder="Email" class="form-check--empty form-check--email">
						</div>
					</div>
				</div>
				<div class="field-row">
					<div class="field field--1-1 field--mb-1-1">
						<div class="field__holder">
								<div class="captcha">
									<div class="captcha__img">
										<img id="booking_captcha" src="https://hkdi3.bcde.digital/include/securimage/securimage_show.php" alt="CAPTCHA Image">
									</div>
									<div class="captcha__control">
										<div class="captcha__input">
											<input type="text" name="captcha" class="form-check--empty">
										</div>
										<a href="#" class="captcha__btn" onclick="document.getElementById('booking_captcha').src = 'https://hkdi3.bcde.digital//include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="err-msg-holder">
					<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
					<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
					<p class="err-msg err-msg--user-not-found">User not found. Please confirm email.</p>
					<p class="err-msg err-msg--select-checkbox">Check box to unsubscribe from all email communications.</p>
				</div>
				<div class="btn-row">
					<div class="btn-row btn-row--al-hl">
						<a href="#" class="btn btn-send">
							<strong>Submit</strong>
						</a>
					</div>
				</div>
			</form>
			<div class="unsubscribe-form--contact-form__success-msg" style="display: none;">
				<p class="desc-l">You have successfully unsubscribed.</p>
			</div>
		</section>
	</main>
	<?php //include $inc_lang_path . "inc_common/inc_form_err_msg.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

</html>
