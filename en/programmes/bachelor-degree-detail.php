<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
//$page_title = "BA (Hons) Fine Art";
$section = 'programmes';
$subsection = 'bachelor-degree';
$sub_nav = 'bachelor-degree';


DM::setup($db_host, $db_name, $db_username, $db_pwd);

$degree_id = $_REQUEST['degree_id'] ?? '';
$preview = @$_REQUEST["preview"] ?? false;
if($preview) {
    $degree = DM::findOne($DB_STATUS.'top_up_degree', ' top_up_degree_id = ?', " top_up_degree_seq desc", array($degree_id ));
}
else {
    $degree = DM::findOne($DB_STATUS.'top_up_degree', ' top_up_degree_status = ? and top_up_degree_id = ?', " top_up_degree_seq desc", array(STATUS_ENABLE,$degree_id ));
}

if(!$degree){
    header("location: bachelor-degree.php");
}

$top_up_degree_id = $degree['top_up_degree_id'];
$top_up_degree_univeristy_id = $degree['top_up_degree_univeristy_id'];
$top_up_degree_name = $degree['top_up_degree_name_'.$lang];
$top_up_degree_detail_title = $degree['top_up_degree_detail_title_'.$lang];
$top_up_degree_detail_title_bold = $degree['top_up_degree_detail_title_bold_'.$lang];
$top_up_degree_detail = $degree['top_up_degree_detail_'.$lang];
$top_up_degree_reg_no = $degree['top_up_degree_reg_no_'.$lang];
$top_up_degree_image1 = $degree['top_up_degree_image1'];
$top_up_degree_image2 = $degree['top_up_degree_image2'];
if($top_up_degree_image1){
    $top_up_degree_image1 = $top_up_degree_url . $top_up_degree_id . "/" . $top_up_degree_image1;
}
if($top_up_degree_image2){
    $top_up_degree_image2 = $top_up_degree_url . $top_up_degree_id . "/" . $top_up_degree_image2;
}

$universities = DM::findOne($DB_STATUS.'universities', ' universities_status = ? and universities_id = ?', " universities_seq desc", array(STATUS_ENABLE,$top_up_degree_univeristy_id));

$universities_id = $universities['universities_id'];
$universities_name = $universities['universities_name_'.$lang];
$universities_detail = $universities['universities_detail_'.$lang];
$universities_image = $universities['universities_image'];
if($universities_image){
    $universities_image = $universities_url . $universities_id . "/" . $universities_image;
}

$universities_inside_logo = $universities['universities_inside_logo'];
if($universities_inside_logo){
    $universities_inside_logo = $universities_url . $universities_id . "/" . $universities_inside_logo;
}

$page_title = $top_up_degree_name;

$breadcrumb_arr['Design Programmes'] =$host_name_with_lang.'programmes/#tabs-dg'; 
$breadcrumb_arr['Degree'] =$host_name_with_lang.'programmes/#tabs-dg';
$breadcrumb_arr[$top_up_degree_name] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/ba-details.css" type="text/css" />
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
            <div class="<?php echo $subsection ?>">
            	<section class="degree-details">
                	<h2><?php echo $top_up_degree_detail_title_bold?> <span><?php echo $top_up_degree_detail_title?></span></h2>
                    <p><small><?php echo $top_up_degree_reg_no?></small></p>
                    <?php echo $top_up_degree_detail?>
                </section>
                
            	<section class="visual-holder">
                    <?php if($top_up_degree_image1){ ?>
                    <img src="<?php echo $top_up_degree_image1 ?>" />
                    <?php }if($top_up_degree_image2){ ?>
                    <img src="<?php echo $top_up_degree_image2 ?>" />
                    <?php } ?>
                </section>
                
                <section class="school-info">
                    <div>
                        <p><small>Programme organized by</small></p>
                        <?php if($universities_inside_logo){ ?>
                        <img src="<?php echo $universities_inside_logo?>" />
                        <?php } ?>
                        <!--<h3><?php echo $universities_name?></h3>-->
                        <?php echo $universities_detail?>
                    </div>
                </section>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
