<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Programmes';
$section = 'programmes';
$subsection = 'course';
$sub_nav = 'course';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>Professional
									<br>Course in Fashion
									<br>Denim Design</strong>
							</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<div class="page-head__logo">
							<span class="page-head__logo-label">Co-organiser</span>
							<img class="page-head__logo-img" src="<?php echo $inc_root_path ?>images/programme/course/logo01.jpg" />
						</div>
						<div class="page-head__logo">
							<span class="page-head__logo-label">Sponsor</span>
							<img class="page-head__logo-img" src="<?php echo $inc_root_path ?>images/programme/course/logo02.jpg" />
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail ani">
				<div class="content-wrapper">
					<div class="txt-editor ani">
						<p>
							<strong>Denim</strong> is regarded as the "King of Fashion". Since its appearance hundred years ago, the everlasting styles
							are embraced with new materials and technology into a variety of designs. The course provides students an introduction
							to the world of denim, from history, material knowledge and classification, design and trend to production.</p>
						<p>A visit to denim factory would be arranged for an understanding of the manufacturing process. On completion of the
							course, students would be able to understand the design and development of denim and be able to experiment with basic
							denim design techniques and material handles. </p>
					</div>
				</div>
			</div>
			<div class="float-gallery ani">
				<div class="float-gallery__wrapper">
					<div class="float-gallery__banner">
						<img src="<?php echo $inc_root_path ?>images/programme/course/banner01.jpg" />
					</div>
				</div>
			</div>
			<div class="sec-basic">
				<div class="content-wrapper">
				<div class="lesson-info">
						<div class="lesson-info__row ani">
							<h3>Lesson 1</h3>
							<p>
								<strong>Lecture:</strong>
							</p>
							<p>• Introduction to Denim - history and trends • Material knowledge - material properties and construction</p>
							<p>
								<strong>Tutorial:</strong>
							</p>
							<p>Showcase of material samples and terminologies and classifications</p>
							<p>
								<strong>Date: Fri, 5 May, 7pm-10pm</strong>
							</p>
						</div>
						<!-- lesson-row -->
						<!-- lesson-row -->
						<div class="lesson-info__row ani">
							<h3>Lesson 2</h3>
							<p>
								<strong>Lecture:</strong>
							</p>
							<p>• Introduction to Denim - history and trends • Material knowledge - material properties and construction</p>
							<p>
								<strong>Tutorial:</strong>
							</p>
							<p>Showcase of material samples and terminologies and classifications</p>
							<p>
								<strong>Date: Fri, 5 May, 7pm-10pm</strong>
							</p>
						</div>
						<!-- lesson-row -->
						<!-- lesson-row -->
						<div class="lesson-info__row ani">
							<h3>Lesson 3</h3>
							<p>
								<strong>Lecture:</strong>
							</p>
							<p>• Introduction to Denim - history and trends • Material knowledge - material properties and construction</p>
							<p>
								<strong>Tutorial:</strong>
							</p>
							<p>Showcase of material samples and terminologies and classifications</p>
							<p>
								<strong>Date: Fri, 5 May, 7pm-10pm</strong>
							</p>
						</div>
						<!-- lesson-row -->
					</div>
				</div>
			</div>

			
			<section class="pc-app pc-app--venue ani">
					<div class="content-wrapper">
						<div class="pc-app__item">
							<h3>Venue:</h3>
							<p>HKDI / Other Companies</p>
						</div>
						<div class="pc-app__item">
							<h3>Course Fee:</h3>
							<p>HK$3,420
								<br> HK$3,250
								<small>(Early bird discount - 5% off) (Deadline: 25 Apr 2017)</small>
								<br> HK$3,080
								<small>(VTC staff, students, alumni; members of co-organiser, sponsor or supporting organisation discount - 10% off) (Deadline:
									25 Apr 2017)</small>
								<br>
								<small>* The course fee is not inclusive of the round-trip transportation fees from Hong Kong to China for Lesson 5.</small>
							</p>
						</div>
					</div>
				</section>
				<section class="pc-app pc-app--application ani">
				<div class="content-wrapper">
						<div class="pc-app__item">
							<h3>Application:</h3>
							<a href="#">Online Application</a>
							<br/>
							<a href="#">Payment Methods</a>
							<br/>
							<a href="#">Course Leaflet</a>
						</div>
						<div class="pc-app__item">
							<h3>Financial Aids:</h3>
							<p>Students can apply for
								<a href="#">NLS loan</a> to pay for the full course fee</p>
						</div>
					</div>
				</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>