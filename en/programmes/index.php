<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Programmes';
$section = 'programmes';
$subsection = 'index';
$sub_nav = 'index';

$breadcrumb_arr['Design Programmes'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$AIP_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 6));
$CDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 7));
$DFS_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 5));
$FID_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 8));
$DDM_first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, 22));


$DFS_dept = DM::load($DB_STATUS.'dept', '5');
$AIP_dept = DM::load($DB_STATUS.'dept', '6');
$CDM_dept = DM::load($DB_STATUS.'dept', '7');
$FID_dept = DM::load($DB_STATUS.'dept', '8');
$DDM_dept = DM::load($DB_STATUS.'dept', '22');

if ($DFS_dept['dept_image'] != '' && file_exists($dept_path.$DFS_dept['dept_id']."/".$DFS_dept['dept_image'])) {
	$DFS_image = $dept_url.$DFS_dept['dept_id']."/".$DFS_dept['dept_image'];
}
if ($AIP_dept['dept_image'] != '' && file_exists($dept_path.$AIP_dept['dept_id']."/".$AIP_dept['dept_image'])) {
	$AIP_image = $dept_url.$AIP_dept['dept_id']."/".$AIP_dept['dept_image'];
}

if ($CDM_dept['dept_image'] != '' && file_exists($dept_path.$CDM_dept['dept_id']."/".$CDM_dept['dept_image'])) {
	$CDM_image = $dept_url.$CDM_dept['dept_id']."/".$CDM_dept['dept_image'];
}

if ($FID_dept['dept_image'] != '' && file_exists($dept_path.$FID_dept['dept_id']."/".$FID_dept['dept_image'])) {
	$FID_image = $dept_url.$FID_dept['dept_id']."/".$FID_dept['dept_image'];
}

if ($DDM_dept['dept_image'] != '' && file_exists($dept_path.$DDM_dept['dept_id']."/".$DDM_dept['dept_image'])) {
	$DDM_image = $dept_url.$DDM_dept['dept_id']."/".$DDM_dept['dept_image'];
}
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>Design</strong>
								<br>PROGRAMMES</h2>
						</div>
					</div>
					<div class="page-head__tag-holder">
						<p class="page-head__tag">Putting creativity into action</p>
					</div>
				</div>
			</div>
			<div class="page-tabs">
				<div class="page-tabs__btns">
					<div class="page-tabs__wrapper">
						<a href="#tabs-hd" class="page-tabs__btn is-active">
							<span>Higher Diploma</span>
						</a>
						<a href="#tabs-dg" class="page-tabs__btn">
							<span>Degree</span>
						</a>
						<a href="#tabs-md" class="page-tabs__btn">
							<span>Master Degree</span>
						</a>
						<a target="_blank" href="http://www.hkdi.edu.hk/peec/" class="page-tabs__btn is-link">
<?php
/*
						<a href="<?php echo $host_name_with_lang?>continuing-education/" class="page-tabs__btn is-link">
*/
?>
							<span>Continuing Education</span>
						</a>
					</div>
				</div>
				<div class="page-tabs__contents">
					<div class="page-tabs__wrapper">
						<div id="tabs-hd" class="page-tabs__content is-active">
							<div class="programme-portal">
								<div class="programme-intro">
									<h2 class="programme-intro__title">
										<strong>Higher</strong> Diploma</h2>
									<div class="programme-intro__desc">
										<p>Together, HKDI’s academic departments offer over 20 different programmes.</p>
										<p>
											<b><i>
“The employment rate of HKDI full-time Higher Diploma graduates of 2018 had exceeded the target rate of 90%, and the Further Study Rate was 37%.”										</b></i>

										</p>
									</div>
								</div>
								<div id="pp-cdd" class="programme-portal__item ani">
									<!-- <a href="programme.php?programmes_id=<?php echo $AIP_first_programme['programmes_id']; ?>"> -->
									<a href="AIP/<?php echo $AIP_first_programme['programmes_slug']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $AIP_image?>" alt="" />
											<h2 class="programme-portal__item-txt">Architecture, Interior & Product Design</h2>
										</div>
									</a>
								</div>
								<div id="pp-dfs" class="programme-portal__item ani">
										<a href="DCD/<?php echo $CDM_first_programme['programmes_slug']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $CDM_image?>" alt="" />
											<h2 class="programme-portal__item-txt">Communication Design</h2>
										</div>
									</a>
								</div>
								<!--div id="pp-aip" class="programme-portal__item ani">
									<a href="programme.php?programmes_id=<?php echo $DFS_first_programme['programmes_id']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $DFS_image?>" alt="" />
											<h2 class="programme-portal__item-txt">Design Foundation Studies</h2>
										</div>
									</a>
								</div-->
								<div id="pp-fid" class="programme-portal__item ani">
									<a href="DDM/<?php echo $DDM_first_programme['programmes_slug']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $DDM_image;?>" alt="" />
											<h2 class="programme-portal__item-txt">Digital Media</h2>
										</div>
									</a>
								</div>
								<div id="pp-ddm" class="programme-portal__item ani">
									<a href="FID/<?php echo $FID_first_programme['programmes_slug']; ?>">
										<div class="programme-portal__item-body">
											<img class="programme-portal__item-img" src="<?php echo $FID_image;?>" alt="" />
											<h2 class="programme-portal__item-txt">Fashion & Image Design</h2>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div id="tabs-dg" class="page-tabs__content">
							<div class="programme-intro">
								<h2 class="programme-intro__title">
									<strong>Degree</strong>
								</h2>
								<div class="programme-intro__desc">
									<p>Students can earn a Bachelor’s degree (Hons) in only three years by first graduating from a two-year Higher Diploma programme and then completing a final-year Degree programme.
									</p>
								</div>
							</div>
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
										     	HKDSE
										     </div>
										</div>
									</div>
									<a href="javascript:toTabsOne();" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 1 & 2
											<br>Higher Diploma
										     </div>
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 3 Degree
										     </div>
										</div>
									</a>
<!-- 									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 4 Master
										     </div>
										</div>
									</a> -->
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">Bachelor's Degree</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI offers many degree programmes through partnerships with various renowned UK universities. By first completing a two-year Higher Diploma and then a final-year Degree programme, students can earn a Bachelor’s degree (Hons) in just three years.</p>
									</div>
								</a>
							</div>
						</div>
						<div id="tabs-md" class="page-tabs__content">
<!-- 							<div class="programme-intro">
								<h2 class="programme-intro__title">
									<strong>Master</strong> Degree
								</h2>
								<div class="programme-intro__desc">
									<p>Students can earn a Master’s degree in as little as four years by first graduating from a two-year Higher Diploma programme and a final-year Degree programme before completing a one-year Master’s programme.
									</p>
								</div>
							</div> -->
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
										     	HKDSE
										     </div>
										</div>
									</div>
									<a href="javascript:$('.page-tabs__btn').eq(0).click();" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 1 & 2
											<br>Higher Diploma
										     </div>
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 3 Degree
										     </div>
										</div>
									</a>
<!-- 									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
										     <div class="clear_flex">
											Year 4 Master
										     </div>
										</div>
									</a> -->
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">Master Degree</h3>
									</div>
									<!-- <div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI offers a Master of Arts in Design Management in partnership with the UK’s renowned Birmingham City University. By first completing a two-year Higher Diploma and then a final-year Bachelor’s Degree (Hons) programme, students can earn a Master of Arts Degree in just four (full-time) or five (part-time) years.</p>
									</div> -->
</a>
							</div>
						</div>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>

			<?php /* ?>
            <div class="video-bg">
                <div class="video-bg__holder">
                    <video autoplay loop muted playsinline webkit-playsinline data-keeplaying="true" poster="<?php echo $img_url ?>video/video-smoke.jpg">
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/mp4" />
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/webm" />
                        <source src="<?php echo $inc_root_path ?>videos/video-smoke.mp4" type="video/ogg" />
                    </video>
                </div>
            </div>
            <?php */ ?>
				<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>
	<script>
		function toTabsOne(){
			$('.page-tabs__btn').eq(0).click();
		}
	</script>
	</html>
