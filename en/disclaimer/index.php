<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Contact';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['Disclaimer'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>Disclaimer</strong>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
				<p>The material contained in this Website has been produced by the Hong Kong Design Institute (a member institution under the VTC Group) in accordance with its current practices and policies and with the benefit of information currently available to it and all reasonable efforts have been made to ensure the accuracy of the contents of the pages of the website at the time of preparation.</p>

				<p>HKDI regularly reviews the website and where appropriate will update pages to reflect change in circumstances. Notwithstanding all efforts made by HKDI to ensure the accuracy of the website, no responsibility or liability is accepted by HKDI in respect of any use or reference to the website, or for any inaccuracies, omissions, mis-statements or errors in the said material, or for any economic or other loss which may be directly or indirectly sustained by any visitor to the website or other person who obtains access to the material on the website.</p>

				<h3>Copyright</h3> 
				<p>The pages and contents of this website ("documents") are the copyright of HKDI. Permission to use documents from this website is granted, provided that (1) the below copyright notice appears on all copies and that both the copyright notice and this permission notice appear, (2) use of such documents from this website is for information and non-commercial or personal use only and they shall not be copies or posted in whole or in part on any network computer or broadcast in any media, and (3) no modifications or any documents are made. Use for any other purpose in expressly prohibited by law.</p>

				<h3>Personal Data</h3>
				<p>This website provides you an opportunity to e-mail HKDI for further information by providing us certain personal data and asking any specific questions. As a general policy HKDI values all personal information provided by visitors to our website. Failure to supply personal data as requested on our website may result in HKDI being unable to provide you further information relating to our services.</p>
				
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>