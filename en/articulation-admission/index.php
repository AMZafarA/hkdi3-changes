<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Articulation & Education';
$section = 'continuing-education';
$subsection = 'detail';
$sub_nav = 'detail';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$id = $_REQUEST['id'] ?? '';
$dept = DM::load($DB_STATUS.'dept', $id);
$dept_name = $dept['dept_name_'.$lang];

if(!$id){
    header("location: ../index.php");
}


$first_programme = DM::findOne($DB_STATUS.'programmes', ' programmes_status = ? and programmes_lv2 = ?', " programmes_seq desc", array(STATUS_ENABLE, $id ));

$breadcrumb_arr['Design Programmes'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr['Higher Diploma'] =$host_name_with_lang.'programmes/#tabs-hd'; 
$breadcrumb_arr[$dept_name] =$host_name_with_lang.'programmes/programme.php?programmes_id='.$first_programme['programmes_id'];
$breadcrumb_arr['Admission'] =''; 
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<script type="text/javascript" src="<?php echo $host_name?>js/programme.js"></script>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<div class="page-head">
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>ARTICULATION &</strong>
								<br>ADMISSION
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>Articulation</strong> Pathway
					</h2>
					<div class="txt-editor">
						<h4>
							<strong>HKDI offers a dynamic articulation pathway for students studying design. After 2-year study at HKDI, students can
								either get promoted to a 1-year top-up degree offered by UK universities, or enter Year 2/3 in local universities.
							</strong>
						</h4>
						<p>For students without a full certificate in HKDSE, they may enter a 1-year Diploma of Foundation Studies (Design) first,
							and be admitted to HKDI’s higher diploma programmes after graduation.</p>
						<?php /*
						<a href="#" class="btn">
							<strong>Read more</strong>
						</a>
						*/ ?>
					</div>
				</div>
			</div>
			<div class="color-flow">
				<div class="content-wrapper">
					<div class="color-flow__items">
						<div id="color-flow--hkdse" class="color-flow__item">
								<div class="color-flow__item-title">HKDSE</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--hd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-hkdi.png" alt="" />
								<div class="color-flow__item-title">Higher Diploma</div>
								<div class="color-flow__item-label">2 years</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--bd" class="color-flow__item">
								<img class="color-flow__item-img" src="<?php echo $img_url?>articulation-admission/img-aa-logo-uk.png" alt="" />
								<div class="color-flow__item-title">UK Bachelor's Degree</div>
								<div class="color-flow__item-label">1 year</div>
						</div>
						<div class="color-flow__item-arrow"></div>
						<div id="color-flow--md" class="color-flow__item">
								<div class="color-flow__item-title">Master's Degree</div>
								<div class="color-flow__item-label">1-2 year</div>
						</div>
					</div>
				</div>
			</div>
			<div class="article-detail article-detail--txt-green" style="background:#000000;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>Tuition</strong> fee
					</h2>
					<div class="txt-editor">
						<ul>
							<li>
								<p>Tuition fees are subject to annual review. The tuition fee levels for AY 2019/20 will be reviewed subject to inflationary adjustments and other relevant factors </p>
							</li>
							<li>
								<p>The tuition fees for the 2019/20 academic year will be announced on the Admissions Homepage at www.vtc.edu.hk/admission at a later stage.</p>
							</li>
							<li>
								<p>The study duration of Higher Diploma programmes is normally 2 years. The tuition fee is payable in two instalments each year.</p>
							</li>
							<li>
								<p>The study duration of Diploma of Foundation Studies and Diploma of Vocational Education programmes is normally 1 year. The tuition fee is payable in two instalments. </p>
							</li>
						</ul>
						<p>
							<strong>AY 2018/19 Tuition Fees For
								<br>Full-Time Programmes Are Listed Below</strong>
						</p>

						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>Programmes</strong>
									</th>
									<th>
										<strong>1st Year</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
									<th>
										<strong>2nd Year</strong>
										<small>Tuition fees per year (HK$) for reference only</small>
									</th>
								</tr>
								<tr>
									<td>Higher Diploma (Subvented Programmes)</td>
									<td>$31,570</td>
									<td>$31,570</td>
								</tr>
								<tr>
									<td>Higher Diploma (Self-financed Programmes)</td>
									<td>$56,600</td>
									<td>$56,600</td>
								</tr>
								<tr>
									<td>Diploma of Foundation Studies/<br>Diploma of Vocational Education (Subvented Programmes)</td>
									<td>$20,500</td>
									<td>-</td>
								</tr>
								<tr>
									<td>Diploma of Foundation Studies (Self-financed Programmes)</td>
									<td>$27,000</td>
									<td>-</td>
								</tr>
							</table>
						</div>
						<div class="txt-editor__notes">
							<p>
								<strong>
									<small>NOTES</small>
								</strong>
							</p>
							<ol>
								<li>In addition to tuition fees, students will be required to pay other fees, such as caution money, students’ union annual 
									fees and English Module Benchmarking Fee. Students of Higher Diploma Programmes will be required to pay the fee
									for the study packages of English modules.</li>
								<li>All new students are required to pay the 1st installment of tuition fees and other fees by Early August 2018.</li>
								<li>Some students may be required to study bridging modules or enhancement programmes to support their study; or to attend
									additional training and industrial attachments, for which separate fees will be charged.</li>
								<li>Students of Diploma of Foundation Studies programmes may choose to take the optional module “Foundation Mathematics
									III” with a separate tuition fee.</li>
							</ol>
						</div>

					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#00f7c1;">
				<div class="content-wrapper">
					<h2 class="article-detail__title">
						<strong>Admission</strong> period & method
					</h2>
					<div class="txt-editor">
						<p>
							<!-- 2018/19 HKDI Higher Diploma starts to receive application from 24 November 2017. -->
							<br> Application website:
							<a href="#">
								<strong><a href="http://www.vtc.edu.hk/admission/en/">www.vtc.edu.hk/admission</a></strong>
							</a>
						</p>
						<p>
							<strong>General Entrance Requirement for HKDI Higher Diploma*:</strong>
						</p>
						<ul>
							<li>Five HKDSE subjects at Level 2 or above, including English Language and Chinese Language; OR</li>
							<li>VTC Foundation Diploma (Level 3) / Diploma of Foundation Studies; OR</li>
							<li>VTC Diploma in Vocational Education / Diploma of Vocational Education; OR</li>
							<li>Yi Jin Diploma / Diploma Yi Jin; OR</li>
							<li>Equivalent</li>
						</ul>
					</div>
					<br />
					<p>For more details, please <a href="http://www.vtc.edu.hk/admission/en/" class="btn"><strong>click here</strong></a></p>
				</div>
			</div>

			<div class="article-detail">
				<div class="content-wrapper">
					<div class="txt-editor__notes">
						<p>
							<strong>Notes*</strong>
						</p>
						<ol>
							<li>The study duration of Higher Diploma programmes is normally 2 years.</li>
							<li>An "Attained” and an “Attained with Distinction" in an HKDSE Applied Learning (ApL) subject (Category B subjects)
								are regarded as equivalent to an HKDSE subject at "Level 2” and “Level 3" respectively, and a maximum of two ApL
								subjects (excluding ApL(C)) can be counted for admission purpose. Only the results of relevant ApL subjects will
								be considered in the application for Degree programmes. Please refer to the specific entrance requirements of Degree
								programmes.
							</li>
							<li>A "Grade D or E” and a “Grade C or above" in an HKDSE Other Language subject (Category C subjects) are regarded as
								equivalent to an HKDSE subject at "Level 2” and “Level 3" respectively, and one Other Language subject can be counted
								for admission purpose.</li>
							<li>Holders of Diploma in Vocational Education / Diploma of Vocational Education (DVE) award upon successful completion
								of prescribed modules are eligible to apply for Higher Diploma programmes.</li>
							<li>Some programmes are not applicable to holders of Diploma in Vocational Education / Diploma of Vocational Education
								(DVE) / Yi Jin Diploma / Diploma Yi Jin. Some programmes may have other specific entrance requirements. Please refer
								to the programme details and specific requirements of individual programmes.</li>
							<li>Offering of study place is subject to the applicants’ academic qualifications, interview / test performance (if applicable),
								other learning experience and achievements, and availability of study places.</li>
						</ol>
						<br>
						<p>
							<strong>Important</strong> Notes</p>
						<ul>
							<li>Applicants can apply with the results of the same public examination obtained in different years. The best grade of
								multiple attempts on a particular subject will be assessed for admission.</li>
							<li>Public examination results of the old and new academic structures will not normally be considered in combination in
								the assessment for admission.</li>
							<li>If non-Chinese speaking applicants meet the specific circumstances*, the alternative language results below will be
								considered individually in the application:</li>
						</ul>
						<br>
						<div class="compare-table">
							<table>
								<tr>
									<th>
										<strong>Alternative Language Qualifications</strong>
									</th>
									<th>
										<strong>For Degree Programmes</strong>
									</th>
									<th>
										<strong>For Higher Diploma Programmes</strong>
									</th>
								</tr>
								<tr>
									<td>HKDSE Applied Learning Chinese</td>
									<td>Attained</td>
									<td>Attained</td>
								</tr>
								<tr>
									<td>GCSE / IGCSE / GCE O-level Chiense Language</td>
									<td>Grade C</td>
									<td>Grade D</td>
								</tr>
								<tr>
									<td>GCE AS-level Chiense Language</td>
									<td>Grade C</td>
									<td>Grade E</td>
								</tr>
								<tr>
									<td>GCE A-level Chiense Language</td>
									<td>Grade E</td>
									<td>Grade E</td>
								</tr>
							</table>
							<br>
							<p>
								<small>* The specific circumstances are 1) The applicant has learnt Chinese Language for less than six years while receiving
									primary and secondary education. This caters specifically to applicants who have a late start in the learning of
									Chinese Language (e.g. due to their settlement in Hong Kong well past the entry level) or who have been educated
									in Hong Kong sporadically; OR 2) The applicant has learnt Chinese Language for six years or more in schools, but
									has been taught an adapted and simpler Chinese Language curriculum not normally applicable to the majority of students
									in the local schools.</small>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="article-detail" style="background:#f6f6f6;">
				<div class="content-wrapper">
					<div class="txt-editor">
						<h3 style="color:#00f7c1;">
							<strong>Hong Kong Advanced Level Examination (HKALE) Qualifications</strong>
						</h3>
						<p>Holders of Hong Kong Advanced Level Examination (HKALE) and Hong Kong Certificate of Education Examination (HKCEE)
							qualifications may apply for admission to Degree and Higher Diploma programmes. Please refer to the details and specific
							entrance requirements of individual programmes (Applicable to S7 students of old secondary academic structure)</p>
						<h3 style="color:#00f7c1;">
							<strong>Other Local, Mainland China and Non-local Qualifications</strong>
						</h3>
						<p>Applicants may apply with other local, Mainland China or non-local qualifications. Applications will be assessed individually
							by the departments concerned.</p>
					</div>
				</div>
			</div>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>