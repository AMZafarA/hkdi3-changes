<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "Industrial Collaborations";
$section = 'industrial-collaborations';
$subsection = 'industrial-collaborations-details';
$sub_nav = 'industrial-collaborations-details';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$collaboration_id = @$_REQUEST['collaboration_id'] ?? '';
$preview = @$_REQUEST["preview"] ?? false;
if($preview) {
	$collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_id = ?', '', array($collaboration_id));
}
else {
	$collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_status = ? AND collaboration_id = ?', '', array(STATUS_ENABLE, $collaboration_id));
}
$collaboration_images = DM::findAll($DB_STATUS.'collaboration_image', 'collaboration_image_status = ? AND collaboration_image_pid = ? AND collaboration_image_type = ?', 'collaboration_image_seq', array(STATUS_ENABLE, $collaboration_id, 'I'));
$membership_images = DM::findAll($DB_STATUS.'collaboration_image', 'collaboration_image_status = ? AND collaboration_image_pid = ? AND collaboration_image_type = ?', 'collaboration_image_seq', array(STATUS_ENABLE, $collaboration_id, 'M'));
$prev_collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_status = ? AND collaboration_id <> ? AND collaboration_seq <= ?', 'collaboration_seq DESC', array(STATUS_ENABLE, $collaboration_id, $collaboration['collaboration_seq']));
if (!$prev_collaboration)
{
	$prev_collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_status = ? AND collaboration_id <> ?', 'collaboration_seq DESC', array(STATUS_ENABLE, $collaboration_id));
}
$next_collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_status = ? AND collaboration_id <> ? AND collaboration_seq >= ?', 'collaboration_seq', array(STATUS_ENABLE, $collaboration_id, $collaboration['collaboration_seq']));
if (!$$next_collaboration)
{
	$next_collaboration = DM::findOne($DB_STATUS.'collaboration', 'collaboration_status = ? AND collaboration_id <> ?', 'collaboration_seq', array(STATUS_ENABLE, $collaboration_id));

}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/industrial-collaborations-details.css" type="text/css" />
        <script>
            $(document).ready(function () {
                var mySwiper = new Swiper ('.swiper-container', {
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                    },
                    paginationClickable: true,
                    autoHeight: true
                });
            });
        </script>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<!--
            	<div class="top-nav">
            		<div class="top-nav-left"><a class="back" href="index.php">Back</a></div>
                	<div class="top-nav-center"><h4>Industrial Collaboration</h4></div>
                	<div class="top-nav-right">&nbsp;</div>
            	</div>
				-->
                <div class="<?php echo $subsection ?>">
            	<section class="content-text">
			<?php if ($collaboration['collaboration_thumb'] != '' && file_exists($collaboration_path.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_thumb'])) { ?>
			<img src="<?php echo $collaboration_url.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_thumb']; ?>" />
			<?php } ?>
			<h2><?php echo $collaboration['collaboration_name_'.$lang]; ?></h2>
			<p><strong><?php echo $collaboration['collaboration_desc_'.$lang]; ?></strong></p>
                	<?php echo $collaboration['collaboration_detail_'.$lang]; ?>
                </section>
                <section class="visual-holder">
			<?php if ($collaboration['collaboration_image'] != '' && file_exists($collaboration_path.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_image'])) { ?>
			<img src="<?php echo $collaboration_url.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_image']; ?>" />
			<?php } ?>
			<?php if ($collaboration['collaboration_image1'] != '' && file_exists($collaboration_path.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_image1'])) { ?>
			<img src="<?php echo $collaboration_url.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_image1']; ?>" />
			<?php } ?>
		</section>
		
		<?php if (sizeof($collaboration_images) > 0) { ?>
                <section class="photos">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
			    	<?php $count = 1; ?>
			    	<?php $total_count = sizeof($collaboration_images); ?>
			    	<?php foreach ($collaboration_images AS $collaboration_image) { ?>
				<?php 	if ($count % 8 == 1) { ?>
				<div class="swiper-slide col">
				<?php 	} ?>
					<a href="<?php echo $collaboration_url.$collaboration_image['collaboration_image_pid'].'/'.$collaboration_image['collaboration_image_id'].'/'.$collaboration_image['collaboration_image_filename']; ?>"><img src="<?php echo $collaboration_url.$collaboration_image['collaboration_image_pid'].'/'.$collaboration_image['collaboration_image_id'].'/'.$collaboration_image['collaboration_image_filename']; ?>" /></a>
				<?php 	if ($count % 8 == 7 || $count == ($total_count - 1)) { ?>
				</div>
				<?php 	} ?>
				<?php 	$count++; ?>
				<?php } ?>
                            </div>                            
                            <div class="swiper-pagination"></div>
                        </div>
                </section>
		<?php } ?>
                
                <section class="membership">
                	<h2>Professional <span>Membership</span></h2>
                    <?php foreach ($membership_images AS $membership_image) { ?>
                    <div class="membership-block">
                        <img src="<?php echo $collaboration_url.$membership_image['collaboration_image_pid'].'/'.$membership_image['collaboration_image_id'].'/'.$membership_image['collaboration_image_filename']; ?>" />" />
                        <div class="txt">
			    <p>&nbsp;</p>
                            <p><strong><?php echo $membership_image['collaboration_image_desc_'.$lang]; ?></strong></p>
                        </div>
                    </div>
		    <?php } ?>
                </section>
            	<div class="bottom-nav">
			<?php if ($prev_collaboration) { ?>
                	<div class="bottom-nav-left"><a class="prev-page" href="industrial-collaborations-details.php?collaboration_id=<?php echo $prev_collaboration['collaboration_id']; ?>"><p>Previous Project</p><h4><?php echo $prev_collaboration['collaboration_name_'.$lang]; ?></h4></a></div>
			<?php } ?>
			<?php if ($next_collaboration) { ?>
                	<div class="bottom-nav-right"><a class="next-page" href="industrial-collaborations-details.php?collaboration_id=<?php echo $next_collaboration['collaboration_id']; ?>"><p>Next Project</p><h4><?php echo $next_collaboration['collaboration_name_'.$lang]; ?></h4></a></div>
			<?php } ?>
                </div>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
