<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = "Industrial Collaborations";
$section = 'industrial-collaborations';
$subsection = 'industrial-collaborations';
$sub_nav = 'industrial-collaborations';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$international_id = "2";

$collaboration_list = DM::findAll($DB_STATUS.'collaboration', 'collaboration_status = ?', 'collaboration_seq', array(STATUS_ENABLE));
$attachment_list = DM::findAll($DB_STATUS.'international_image', 'international_image_status = ? AND international_image_pid = ?', 'international_image_seq', array(STATUS_ENABLE, $international_id));

$breadcrumb_arr['Industrial Collaboration'] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">
    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $host_name?>css/industrial-collaborations.css" type="text/css" />
        <script type="text/javascript" src="<?php echo $host_name?>js/industrial_collaborations.js"></script>
    </head>
    <body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="top-divide-line"></div>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
                <div class="<?php echo $subsection ?>">
            	<section id="industrial-collaboration" class="content-text">
                    <h2>Industrial <span>Collaboration</span></h2>
                    <h4>HKDI collaborates with many industry partners to provide students with maximum exposure and opportunities to obtain hands-on experience by working on real projects.</h4>
                    <p>Our teaching staff work closely with students to help them develop experimental and innovative projects, empowering them to achieve their full potential. The result is work-ready graduates who can effectively integrate their creativity and professional knowledge.</p>
                    
                    <div class="article-detail__others">
						<div class="programme-news ani">
							<?php foreach ($collaboration_list AS $collaboration) { ?>
							<a class="block" href="industrial-collaborations-details.php?collaboration_id=<?php echo $collaboration['collaboration_id']; ?>">
								<?php if ($collaboration['collaboration_thumb'] != '' && file_exists($collaboration_path.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_thumb'])) { ?>
								<img src="<?php echo $collaboration_url.$collaboration['collaboration_id'].'/'.$collaboration['collaboration_thumb']; ?>" />
								<?php } ?>
								<div class="txt">
									<?php /*<p class="date">Latest News | 28 June 2017</p>*/ ?>
									<p><?php echo $collaboration['collaboration_name_'.$lang]; ?></p>
									<p class="btn-arrow"></p>
								</div>
							</a>
							<?php } ?>
						</div>
					</div>
                </section>
                
                <section id="industrial-attachment-programme" class="programme">
                	<h2>Industrial Attachment <span>Programme</span></h2>
                    <?php foreach ($attachment_list AS $attachment) { ?>
                    <div class="programme-block">
                        <img src="<?php echo $international_url.$international_id.'/'.$attachment['international_image_id'].'/'.$attachment['international_image_filename']; ?>" />
                        <div class="txt">
                            <p>&nbsp;</p>
                            <p><strong><?php echo $attachment['international_image_desc_'.$lang]; ?></strong></p>
                        </div>
                    </div>
		    <?php } ?>
                </section>
                <section id="recruit-graduate" class="recruit-form">
                	<h2>Recruit Graduate &amp; <span>Industrial Attachment</span></h2>
					<h4>If you are interested in offering an internship or recruiting one of our graduates, we would be delighted to help you.</h4>
                    <p>Just fill out the form below and we will be able to assist you in identifying the right candidate to meet your organisation’s needs.</p>
                    <form class="contact-form__form">
					<div class="field-row">
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
                    	<input class="form-check--empty" type="text" name="contact_name" placeholder="Name" />
							</div>
						</div>
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
						<input class="form-check--empty" type="text" name="contact_name" placeholder="Position" />
							</div>
						</div>
					</div>
					<div class="field-row">
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
                    			<input class="form-check--empty" type="text" name="contact_company" placeholder="Company" />
							</div>
							</div>
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
                    	<input class="form-check--empty form-check--tel" type="text" name="contact_tel" placeholder="Phone" />
							</div>
						</div>
					</div>
					<div class="field-row">
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
							                    	<input class="form-check--empty form-check--email" type="text" name="contact_email" placeholder="Email" />
							</div>
						</div>
						<div class="field field--1-2 field--mb-1-1">
							<div class="field__holder">
							                    	<input class="form-check--empty" type="text" name="contact_business" placeholder="Type of business" />
							</div>
						</div>
					</div>
					<div class="field-row">
						<div class="field field--1-1 field--mb-1-1">
							<div class="field__holder">
	                        <input type="text" name="contact_msg" placeholder="Message" />							
							</div>
						</div>
						</div>
			<div class="field-row">
				<div class="field field--1-1 field--mb-1-1">
					<div class="field__holder">
						<div class="captcha">
							<div class="captcha__img">
								<img id="booking_captcha" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
							</div>
							<div class="captcha__control">
								<div class="captcha__input">
									<input type="text" name="captcha" class="form-check--empty" placeholder="Captcha" />
								</div>
								<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include $inc_lang_path."inc_common/inc_form_err_msg.php"; ?>
                        <input class="submit-btn btn-send" type="submit" value="Submit" />
                    </form>
					
												<div class="contact-form__success-msg">
													<h3 class="desc-subtitle">Thank you!</h3>
													<p class="desc-l">You have successfully submitted your inquiry. Our staff will come to you soon.</p>
												</div>
                </section>
            </div>
            <div id="fp-nav" class="custom-sidenav right">
                <ul>
                    <li>
                        <a href="#industrial-collaboration">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Industrial Collaboration</div>
                    </li>
                    <li>
                        <a href="#industrial-attachment-programme">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Industrial Attachment Programme</div>
                    </li>
                    <li>
                        <a href="#recruit-graduate">
                            <span></span>
                        </a>
                        <div class="fp-tooltip right">Recruit Graduate & Industrial Attachment</div>
                    </li>
                </ul>
            </div>
        </main>
            <?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>
</html>
