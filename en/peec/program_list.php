<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Program';
$section = 'program';
$subsection = 'program_list';
$sub_nav = 'index';

$breadcrumb_arr['Peec'] = $host_name_with_lang.'peec/';;
$breadcrumb_arr['Program'] = '';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);

$conditions = "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, $today, $today);
$news_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, " news_date DESC, news_id DESC", $parameters);
$total_news_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => '最新動態', 'lang3' => '最新动态'),
//	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => '學生得獎作品', 'lang3' => '学生得奖作品'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => '活動', 'lang3' => '活动'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => '其他活動', 'lang3' => '其他活动')
);
for ($i = 0; $i < sizeof($news_types); $i++)
{
	$news_page_no = ($tab == $news_types[$i]['value'] ? $page : 1);
	$parameters2 = array_merge($parameters, array($news_types[$i]['value']));
	// combine hkdi_news and student_award
	// $news_types[0] = hkdi_news;
	if($i == 0){
	$conditions2 = $conditions." AND (news_type = ? OR news_type = ?)";
	array_push($parameters2, 'student_award');
	}else{
		$conditions2 = $conditions." AND news_type = ?";
	}
	$temp_news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions2, " news_date DESC, news_id DESC", $parameters2);
	$temp_news_total_count = DM::count($DB_STATUS.'news', $conditions2, $parameters2);
	$news_types[$i]['list'] = $temp_news_list;
	$news_types[$i]['total_count'] = $temp_news_total_count;
}


$latest_issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));
$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ? AND issue_id <> ?', 'issue_date DESC', array(STATUS_ENABLE, $latest_issue['issue_id']));
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<style>
			.block .txt .small {
				font-size: 13px !important;
				margin-left: 30px;
				display: block;
				margin-bottom: 5px;
			}
			.block .txt {
				height: 280px !important;
			}
		</style>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong>Interior Design</strong> and Architecture</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs">
				
				<div class="page-tabs__contents">
					<div class="page-tabs__wrapper">
						<div id="tabs-all" class="page-tabs__content <?php echo ($tab == 'all' ? 'is-active' : ''); ?>">
							
							<section class="programme-news ani">
								
								<a class="block" href="#">
									<img src="https://hkdi3.bcde.digital/uploaded_files/news/719/8199b222efce5c71277cf4b11a8c6e1742b982fc.jpg" />
									<div class="txt">
										<p class="date">Intake 33 | 21 January 2047</p>
										<p>Professional Certificate in Fashion Model Training Groaming</p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<a class="block" href="#">
									<img src="https://hkdi3.bcde.digital/uploaded_files/news/718/9f07a0872bb4685fcffe1d847a53ec61c7620efb.jpg" />
									<div class="txt">
										<p class="date">Intake 82 | 21 June 2047</p>
										<p style="margin-bottom: 10px;">Professional Diploma in Jewellery Design</p>
										<p class="small">Professional Certificate in Jewellery Design: Culture and Design Methods</p>
										<p class="small">Professional Certificate in Jewellery Design: Techniques, Trend and Branding</p>
										<p class="small">Professional Certificate in Jewellery Design: Profession and Business</p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<a class="block" href="#">
									<img src="https://hkdi3.bcde.digital/uploaded_files/news/719/8199b222efce5c71277cf4b11a8c6e1742b982fc.jpg" />
									<div class="txt">
										<p class="date">Intake 77 | 21 January 2047</p>
										<p>Certificate in Comic Art and Character Design</p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<a class="block" href="#">
									<img src="https://hkdi3.bcde.digital/uploaded_files/news/718/9f07a0872bb4685fcffe1d847a53ec61c7620efb.jpg" />
									<div class="txt">
										<p class="date">Latest News | 25 January 2020</p>
										<p>Online learing Series. SketchUp Essentials</p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								
								
							</section>

							

						</div>


						<!-- News Post -->
						<?php $is_curr_tab = ($tab == '' || $tab == 'all' ? TRUE : FALSE); ?>
						<div id="tabs-all" class="page-tabs__content <?php echo ($tab == 'all' ? 'is-active' : ''); ?>" style="display: none">
                        
							<section class="programme-news ani">
								<?php foreach ($news_list AS $news) { ?>
								<?php 	if ($news['news_thumb'] != '' && file_exists($news_path.$news['news_id']."/".$news['news_thumb'])) { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
									<div class="txt">
										<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
										<p><?php echo $news['news_name_'.$lang]; ?></p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} else { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
									<div class="table">
										<div class="cell">
											<p>
												<strong>Latest News</strong>
											</p>
											<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
										</div>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} ?>
								<?php } ?>
							</section>

							

						</div>

						<?php foreach ($news_types AS $news_type) { ?>
						<?php 	$is_curr_tab = ($tab == $news_type['value'] ? TRUE : FALSE); ?>
						<div id="tabs-<?php echo $news_type['value']; ?>" class="page-tabs__content <?php echo ($tab == $news_type['value'] ? 'is-active' : ''); ?>">
							<section class="programme-news ani">
								<?php foreach ($news_type['list'] AS $news) { ?>
								<?php 	if ($news['news_thumb'] != '' && file_exists($news_path.$news['news_id']."/".$news['news_thumb'])) { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&news_type=<?php echo $news['news_type']; ?>&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<img src="<?php echo $news_url.$news['news_id']."/".$news['news_thumb']; ?>" />
									<div class="txt">
										<p class="date"><?php echo $news_types_arr[$news['news_type']][$lang]?> | <?php echo date('j F Y', strtotime($news['news_date'])); ?></p>
										<p><?php echo $news['news_name_'.$lang]; ?></p>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} else { ?>
								<a class="block" href="news-detail.php?news_id=<?php echo $news['news_id']; ?>&news_type=<?php echo $news['news_type']; ?>&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<p class="date"><?php echo date('M j', strtotime($news['news_date'])); ?></p>
									<div class="table">
										<div class="cell">
											<p>
												<strong>Latest News</strong>
											</p>
											<p class="title"><?php echo $news['news_name_'.$lang]; ?></p>
										</div>
										<p class="btn-arrow"></p>
									</div>
								</a>
								<?php 	} ?>
								<?php } ?>
							</section>

							
                            
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="video-bg">
					<div class="video-bg__holder">
                    <?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
					</div>
				</div>
			</div>
			
		
			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>
