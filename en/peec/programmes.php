<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$programmes_category_slug = "";
if($programmes_category_id == "") {
	$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	$programmes_category_slug = array_pop($uriSegments);
}

$programmes_category_data = DM::findOne($DB_STATUS.'peec_programmes_categories', ' peec_programmes_categories_slug = ?', "", array($programmes_category_slug ));

if($programmes_category_data == "") {
	header("location: ../index.php");
}


$peec_programmes_categories_id = $programmes_category_data["peec_programmes_categories_id"];
$page_title = $programmes_category_data["peec_programmes_categories_name_".$lang];
$section = $programmes_category_data["peec_programmes_categories_name_".$lang];
$subsection = 'program_list';
$sub_nav = 'index';

$breadcrumb_arr['Peec'] = $host_name_with_lang.'peec/';;
$breadcrumb_arr[$page_title] = '';


$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);
$peec_programmes_page_size = 4;

$conditions = "peec_programmes_status = ? AND peec_programmes_from_date <= ? AND (peec_programmes_to_date >= ? OR peec_programmes_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, $today, $today);
$peec_programmes_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$peec_programmes_list = DM::findAllWithPaging($DB_STATUS.'peec_programmes', $peec_programmes_page_no, $peec_programmes_page_size, $conditions, " peec_programmes_date DESC, peec_programmes_id DESC", $parameters);
$peec_programmes_count = DM::count($DB_STATUS.'peec_programmes', $conditions, $parameters);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<style>
		.block .txt .small {
			font-size: 13px !important;
			margin-left: 30px;
			display: block;
			margin-bottom: 5px;
		}
		.block .txt {
			height: 280px !important;
		}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title">
						<h2><strong><?php echo $page_title; ?></strong></h2>
					</div>
				</div>
			</div>
		</div>
		<div class="page-tabs">

			<div class="page-tabs__contents">
				<div class="page-tabs__wrapper" style="padding: 80px 0 60px;">

					<section class="programme-news ani">
						<?php foreach ($peec_programmes_list AS $peec_programmes) { ?>
							<?php if ($peec_programmes['peec_programmes_thumb_banner'] != '' && ( file_exists($peec_programmes_path.$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner']) || file_exists(str_replace("uploaded_files", "CMS/uploaded_files", $peec_programmes_path).$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner']))) { ?>
								<?php $img = ''; ?>
								<?php if(file_exists($peec_programmes_path.$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner'])) { ?>
									<?php $img = $peec_programmes_url.$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner']; ?>
								<?php } elseif(file_exists(str_replace("uploaded_files", "CMS/uploaded_files", $peec_programmes_path).$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner'])) { ?>
									<?php $img = str_replace("uploaded_files", "CMS/uploaded_files", $peec_programmes_url).$peec_programmes['peec_programmes_id']."/".$peec_programmes['peec_programmes_thumb_banner']; ?>
								<?php } ?>
								<a class="block" href="../peec_programme.php?peec_programme=<?php echo $peec_programmes['peec_programmes_slug']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<img src="<?php echo $img; ?>" />
									<div class="txt">
										<p class="date"><?php echo date('j F Y', strtotime($peec_programmes['peec_programmes_created_date'])); ?></p>
										<p><?php echo $peec_programmes['peec_programmes_name_'.$lang]; ?></p>
										<p><?php echo $news['peec_programmes_detail_'.$lang]; ?></p>
										<p class="btn-arrow"></p>
									</div>
								</a>
							<?php } else { ?>
								<a class="block" href="../peec_programme.php?peec_programme=<?php echo $peec_programmes['peec_programmes_slug']; ?>&tab=all&page=<?php echo ($is_curr_tab ? $page : 1); ?>">
									<p class="date"><?php echo date('M j', strtotime($peec_programmes['peec_programmes_created_date'])); ?></p>
									<div class="table">
										<div class="cell">
											<p>
												<strong>Latest Programmes</strong>
											</p>
											<p class="title"><?php echo $peec_programmes['peec_programmes_name_'.$lang]; ?></p>
											<p><?php echo $news['peec_programmes_detail_'.$lang]; ?></p>
										</div>
										<p class="btn-arrow"></p>
									</div>
								</a>
							<?php 	} ?>
						<?php } ?>
					</section>

					<?php // if ($peec_programmes_count > $peec_programmes_page_size) { ?>
						<div class="pagination">
							<?php if ($is_curr_tab && $page > 1) { ?>
								<a href="index.php?tab=all&page=<?php echo ($page-1); ?>" class="pagination__btn-prev">Previous</a>
							<?php } ?>
							<div class="pagination__pages">
								<div class="pagination__current">
									<div class="pagination__current-pg"><?php echo ($tab == 'all' ? $page : 1); ?></div>
								</div>
								<span>of</span>
								<div class="pagination__total"><?php echo ceil($peec_programmes_count / $peec_programmes_page_size); ?></div>
							</div>
							<?php if ($peec_programmes_count > $peec_programmes_page_size * ($is_curr_tab ? $page : 1)) { ?>
								<a href="index.php?tab=all&page=<?php echo ($is_curr_tab ? $page+1 : 2); ?>" class="pagination__btn-next">Next</a>
							<?php } ?>
						</div>
					<?php // } ?>

				</div>
			</div>
			<div class="video-bg">
				<div class="video-bg__holder">
					<?php include $inc_lang_path . "inc_common/inc_video-source.php"; ?>
				</div>
			</div>
		</div>

		
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

</html>
