<?php
$submit = false;
if(isset($_GET['submit'])) {
	if($_GET['submit'] == "true") {
		$submit = "success";
	}
	else {
		$submit = "error";
	}
}

$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'PeecJOBS Advertise';
$section = 'PeecJOBS Advertise';
$subsection = 'peecjobs';
$sub_nav = 'index';

$breadcrumb_arr['Peec'] = $host_name_with_lang.'peec/';;
$breadcrumb_arr['PeecJOBS Advertise'] = '';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$page1 = DM::findAll("peecjob_form", "peecjob_form_page = ?", "peecjob_form_id ASC", array(1));
$page2 = DM::findAll("peecjob_form", "peecjob_form_page = ?", "peecjob_form_id ASC", array(2));

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<style>
		.peec-tab-container {
			margin-bottom: 80px;
		}
		.peec-tab-header {
			margin-bottom: 20px;
		}
		.peec-tab-header .tab-menus {
			display: flex;
			justify-content: flex-start;
			align-items: center;
			gap: 10px;
		}
		.peec-tab-header .tab-menus span {
			font-weight: 600;
			cursor: pointer;
			display: flex;
			justify-content: space-around;
			align-items: center;
			gap: 10px;
			padding: 7px 15px;
			border: 1px solid;
			border-radius: 8px;
			height: 45px;
		}
		.peec-tab-header .tab-menus span img {
			width: 30px;
		}
		.tab-content-container .tab-content-title {
			font-size: 30px;
			font-weight: 300;
		}
		.tab-content-inner-container {
			display: grid;
			grid-template-columns: repeat(3, 1fr);
			grid-gap: 20px;
			margin-bottom: 40px;
		}
		.tab-content-inner-container > div {
			display: grid;
			grid-gap: 5px;

		}
		.peec-toggle-section {
			/* margin-bottom: 80px; */
		}
		.tab-content-inner-container > div span:first-child {
			font-weight: 600;
		}
		.tab-content-2, .tab-content-3 {
			display: none;
		}
		.toggle-content {
			display: none;
			padding: 8px 0;
		}
		.show-toggle-content {
			display: block;
			border-bottom: 1px solid black;
		}
		.peec-toggle-container {
			/* max-width: 640px; */
		}
		.toggle-header {
			display: flex;
			justify-content: space-between;
			align-items: center;
			border-bottom: 1px solid black;
			cursor: pointer;
			padding: 3px 0 4px;
		}
		.toggle-item:nth-child(1) .toggle-header {
			border-top: 1px solid black;
		}
		.toggle-header .toggle-icon {
			width: 25px;
			height: 25px;
			border: 1px solid black;
			border-radius: 50%;
			display: flex;
			justify-content: center;
			align-items: center;
			font-size: 22px;
		}
		.page-tabs__contents {
			padding: 50px 0;
			background-color: #f5f5f5;
		}
		.job-desc {
			padding: 15px 0;
		}
		.job-desc p {
			margin-bottom: 12px;
		}
		.job-desc .desc-title {
			font-weight: bold;
			text-decoration: underline;
		}
		.job-desc .desc-line {
			background-color: #000;
			height: 1px;
			width: 100%;
			margin: 20px 0;
			display: block;
		}
		.job-desc .desc-link {
			text-decoration: none;
			color: blue;
		}
		.mail-to-link {
			color: blue;
		}
		.intro-desc {
			max-width: 1280px;
			margin: auto;
		}
		.intro-desc p {
			font-size: 18px;
			line-height: 1.5;
			margin-bottom: 30px;
		}
		.advertise-btn {
			display: inline-block;
			margin-bottom: 10px;
			padding: 10px 30px;
			font-size: 14px;
			border: 2px solid #000;
			box-sizing: border-box;
			transition: 0.4s;
			font-weight: bold;
		}
		.advertise-btn:hover {
			border-color: #51296f;
			background: #51296f;
			color: #FFF;
			opacity: 1;
		}
	</style>

	<style>

		#peecjob-form {
			background-color: #ffffff;
			margin: 0px auto;
			padding: 40px;
			/*width: 70%;*/
			min-width: 300px;
		}

		#peecjob-form .invalid {
			background-color: #ffdddd;
		}

		.tab {
			display: none;
		}

		#peecjob-form button {
			background-color: #51296f;
			color: #ffffff;
			border: none;
			padding: 10px 20px;
			font-size: 17px;
			font-family: Raleway;
			cursor: pointer;
		}

		#peecjob-form button:hover {
			opacity: 0.8;
		}

		#prevBtn {
			background-color: #bbbbbb;
		}

		.step {
			height: 15px;
			width: 15px;
			margin: 0 2px;
			background-color: #bbbbbb;
			border: none;  
			border-radius: 50%;
			display: inline-block;
			opacity: 0.5;
		}

		.step.active {
			opacity: 1;
		}

		.step.finish {
			background-color: #51296f;
		}
		form#peecjob-form input, form#peecjob-form select, form#peecjob-form textarea {
			border: 1px solid #ddd !important;
			width: 100%;
			color: #000 !important;
			padding: 10px;
			font-size: 17px;
		}
		form#peecjob-form label {
			font-size: 20px;
			display: block;
			margin-bottom: 10px;
		}
		form#peecjob-form p {
			margin-bottom: 40px;
		}
		form#peecjob-form small {
			font-size: 16px;
			margin-bottom: 10px;
			display: block;
			color: #666;
		}
		span.red {
			color: red;
			padding-left: 5px;
		}
		form#peecjob-form input[type="checkbox"]:nth-child(2), form#peecjob-form input[type="radio"]:nth-child(2) {
			margin-left: 0 !important;
		}

		form#peecjob-form input[type="checkbox"], form#peecjob-form input[type="radio"] {
			width: 15px;
			height: 15px;
			margin-left: 10px;
			margin-right: 5px;
		}
		div#msg-box p {
			font-size: 20px;
		}
		.msg-box {
			padding: 0px 100px;
			height: 150px;
			display: flex;
			justify-content: center;
			align-items: center;
			text-align: center;
			margin: 40px auto 0;
			max-width: 1280px;
			position: relative;
		}
		.success-msg {
			background: #00ff7e;
		}
		.error-msg {
			background: #ff4b4b;
		}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>


	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<?php
		if($submit == "success") {
			?>
			<div class="msg-box success-msg">
				<p>Successfully Submitted.</p>
			</div>
			<?php
		}
		elseif($submit == "error") {
			?>
			<div class="msg-box error-msg">
				<p>Error.</p>
			</div>
			<?php
		}
		?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title">
						<h2>
							<strong>PEECjobs: </strong>Advertise a Job
						</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="page-tabs__contents">
			<div class="page-tabs__wrapper">
				<div class="form-container">
					<p>PEECjobs is an online platform created by PEEC (Professional Education and Engagement Centre, HKDI) for HKDI's industry partners to advertise their job recruitments for FREE. This platform is convenient for VTC alumni to look for art and design related jobs. It is also open to public. We hope this platform can be a connection point of design related talents and the industry.</p>
					<p>This form is for the companies to fill in the information for publishing their post recruitment advertisements on PEECjobs webpage.</p>
					<p style="color:red;">*Required field</p>
					<form class="peecjob-form__form form-container" id="peecjob-form" method="post" action="./act.php">
						<div class="tab">
							<h1>Part A: Details of the Job Post</h1>
							<?php
							$i1 = 1;
							foreach ($page1 as $p1) {
								if($p1["peecjob_form_status"]) {
									generate_field($p1,$i1);
									$i1++;
								}
							}
							?>
						</div>

						<div class="tab">
							<h1>Part B: Company Contact for HKDI-PEEC</h1>
							<?php
							$i2 = 1;
							foreach ($page2 as $p2) {
								if($p2["peecjob_form_status"]) {
									generate_field($p2,$i2);
									$i2++;
								}
							}
							?>
						</div>

						<div style="text-align: right; margin-top: 20px;">
							<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
							<button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
						</div>

						<div style="text-align:center;margin-top:40px;">
							<span class="step"></span>
							<span class="step"></span>
						</div>

					</form>
				</div>
			</div>
		</div>

		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>

	<?php

	function generate_field($input,$index) {
		?>
		<p><label><?php echo "$index. " . $input["peecjob_form_label"] . ($input["peecjob_form_required"] ? '<span class="red">*</span>' : '')?></label>
			<?php
			if(!empty($input["peecjob_form_short_description"])) {
				?>
				<small><em><?php echo $input["peecjob_form_short_description"]; ?></em></small>
				<?php
			}
			?>
			<?php
			if( $input["peecjob_form_type"] == "text" || $input["peecjob_form_type"] == "number" || $input["peecjob_form_type"] == "email" || $input["peecjob_form_type"] == "date" ) {
				?>
				<input type="<?php echo $input["peecjob_form_type"]; ?>" name="<?php echo $input["peecjob_form_label"]; ?>" <?php echo $input["peecjob_form_required"] ? 'required' : ''; ?>>
				<?php
			}
			elseif( $input["peecjob_form_type"] == "textarea" ) {
				?>
				<textarea name="<?php echo $input["peecjob_form_label"]; ?>" <?php echo $input["peecjob_form_required"] ? 'required' : ''; ?>></textarea>
				<?php
			} 
			elseif( $input["peecjob_form_type"] == "checkbox" ) {
				$choices = json_decode($input["peecjob_form_choices"], true);
				if(!empty($choices)) {
					foreach ($choices as $choice) {
						?>
						<input type="checkbox" name="<?php echo $input["peecjob_form_label"]; ?>[]" value="<?php echo $choice["value"]; ?>" <?php echo $input["peecjob_form_required"] ? 'required' : ''; ?>><?php echo $choice["label"]; ?></option>
						<?php
					}
				}
			}
			elseif( $input["peecjob_form_type"] == "radio" ) {
				$choices = json_decode($input["peecjob_form_choices"], true);
				if(!empty($choices)) {
					foreach ($choices as $choice) {
						?>
						<input type="radio" name="<?php echo $input["peecjob_form_label"]; ?>" value="<?php echo $choice["value"]; ?>" <?php echo $input["peecjob_form_required"] ? 'required' : ''; ?>><?php echo $choice["label"]; ?></option>
						<?php
					}
				}
			}
			elseif( $input["peecjob_form_type"] == "dropdown" ) {
				?>
				<select name="<?php echo $input["peecjob_form_label"]; ?>" <?php echo $input["peecjob_form_required"] ? 'required' : ''; ?>>
					<?php 
					$choices = json_decode($input["peecjob_form_choices"], true);
					if(!empty($choices)) {
						foreach ($choices as $choice) {
							?>
							<option value="<?php echo $choice["value"]; ?>"><?php echo $choice["label"]; ?></option>
							<?php
						}
					}
					?>
				</select>
				<?php
			}
			?>
		</p>
		<?php
	}
	?>

	<script>
		var currentTab = 0;
		showTab(currentTab);

		function showTab(n) {
			var x = document.getElementsByClassName("tab");
			x[n].style.display = "block";
			if (n == 0) {
				document.getElementById("prevBtn").style.display = "none";
			} else {
				document.getElementById("prevBtn").style.display = "inline";
			}
			if (n == (x.length - 1)) {
				document.getElementById("nextBtn").innerHTML = "Submit";
			} else {
				document.getElementById("nextBtn").innerHTML = "Next";
			}
			fixStepIndicator(n)
		}

		function nextPrev(n) {
			var x = document.getElementsByClassName("tab");
			if (n == 1 && !validateForm()) return false;
			x[currentTab].style.display = "none";
			currentTab = currentTab + n;
			if (currentTab >= x.length) {
				document.getElementById("peecjob-form").submit();
				return false;
			}
			showTab(currentTab);
		}

		function validateForm() {
			var x, y, i, valid = true;
			x = document.getElementsByClassName("tab");
			y = x[currentTab].getElementsByTagName("input");
			for (i = 0; i < y.length; i++) {
				y[i].className = "";
				if (y[i].value == "" && y[i].hasAttribute("required")) {
					y[i].className = " invalid";
					valid = false;
				}
			}
			y = x[currentTab].getElementsByTagName("select");
			for (i = 0; i < y.length; i++) {
				y[i].className = "";
				if (y[i].value == "" && y[i].hasAttribute("required")) {
					y[i].className = " invalid";
					valid = false;
				}
			}
			y = x[currentTab].getElementsByTagName("textarea");
			for (i = 0; i < y.length; i++) {
				y[i].className = "";
				if (y[i].value == "" && y[i].hasAttribute("required")) {
					y[i].className = " invalid";
					valid = false;
				}
			}
			if (valid) {
				document.getElementsByClassName("step")[currentTab].className += " finish";
			}
			return valid;
		}

		function fixStepIndicator(n) {
			var i, x = document.getElementsByClassName("step");
			for (i = 0; i < x.length; i++) {
				x[i].className = x[i].className.replace(" active", "");
			}
			x[n].className += " active";
		}

		<?php 
		if($submit) { 
			if($submit == "success") { 
				?>
				setTimeout(function() {
					$(".success-msg").slideUp(1000, function(){$(".success-msg").detach();});
				}, 5000);
				<?php 
			} 
			elseif($submit == "error") { ?>
				setTimeout(function() {
					$(".error-msg").slideUp(1000, function(){$(".error-msg").detach();});
				}, 5000);
				<?php 
			} 
		} 
		?>
	</script>
</body>

</html>