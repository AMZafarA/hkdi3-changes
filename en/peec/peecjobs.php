<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'PeecJOBS';
$section = 'PeecJOBS';
$subsection = 'peecjobs';
$sub_nav = 'index';

$breadcrumb_arr['Peec'] = $host_name_with_lang.'peec/';;
$breadcrumb_arr['PeecJOBS'] = '';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);

$conditions = "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, $today, $today);
$news_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, " news_date DESC, news_id DESC", $parameters);
$total_news_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => '最新動態', 'lang3' => '最新动态'),
//	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => '學生得獎作品', 'lang3' => '学生得奖作品'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => '活動', 'lang3' => '活动'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => '其他活動', 'lang3' => '其他活动')
);
for ($i = 0; $i < sizeof($news_types); $i++)
{
	$news_page_no = ($tab == $news_types[$i]['value'] ? $page : 1);
	$parameters2 = array_merge($parameters, array($news_types[$i]['value']));
	// combine hkdi_news and student_award
	// $news_types[0] = hkdi_news;
	if($i == 0){
		$conditions2 = $conditions." AND (news_type = ? OR news_type = ?)";
		array_push($parameters2, 'student_award');
	}else{
		$conditions2 = $conditions." AND news_type = ?";
	}
	$temp_news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions2, " news_date DESC, news_id DESC", $parameters2);
	$temp_news_total_count = DM::count($DB_STATUS.'news', $conditions2, $parameters2);
	$news_types[$i]['list'] = $temp_news_list;
	$news_types[$i]['total_count'] = $temp_news_total_count;
}


$latest_issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));
$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ? AND issue_id <> ?', 'issue_date DESC', array(STATUS_ENABLE, $latest_issue['issue_id']));

$peec_data = DM::findOne($DB_STATUS.'peec_page', ' peec_page_id = ?', '', array(2));
$sections = DM::findAll('peec_section_section', 'peec_section_section_pid = ?', '', array(2));

function GenTitle($title){
	$title_arr = explode(" ", $title);
	$count_arr = count($title_arr);
	$h2_title = str_replace($title_arr[$count_arr-1],"",$title);

	if($count_arr ==1){
		return "<h2>".$title."</h2>";
	}else{
		return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
	}
}

function generateBlock($peec_section_sections, $loopI) {
	global $lang, $peec_section_file_url;
	$peec_section_file_url_cms = '';
	$peec_section_file_url_cms = str_replace("uploaded_files", "CMS/uploaded_files", $peec_section_file_url);
	for ($i = ($loopI + 1); $i < sizeof($peec_section_sections); $i++) {
		$peec_section_section = $peec_section_sections[$i];
		if ($peec_section_section['peec_section_section_type'] == 'Acknowledgement') {
			$peec_section_section_files = DM::findAll($DB_STATUS.'peec_section_section_file', 'peec_section_section_file_status = ? AND peec_section_section_file_psid = ?', 'peec_section_section_file_sequence', array(STATUS_ENABLE, $peec_section_section['peec_section_section_id']));
			?>
			<div class="block">
				<p><?php echo $peec_section_section['peec_section_section_title_'.$lang]; ?></p>
				<?php foreach ($peec_section_section_files AS $peec_section_section_file) { ?>
					<img src="<?php echo $peec_section_file_url_cms.$peec_section_section_file['peec_section_section_file_pid'].'/'.$peec_section_section_file['peec_section_section_file_id'].'/'.$peec_section_section_file['peec_section_section_file_filename']; ?>" />
				<?php } ?>
			</div>
		<?php } else if ($peec_section_section['peec_section_section_type'] == 'CT') { ?>
			<div class="hidden-block">
				<h3><?php echo $peec_section_section['peec_section_section_title_'.$lang]; ?></h3>
				<div class="hidden">
					<?php echo $peec_section_section['peec_section_section_content_'.$lang]; ?>           
				</div>
			</div>
		<?php } else { 
			break;
		}
	}
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<style>
		.peec-tab-container {
			margin-bottom: 80px;
		}
		.peec-tab-header {
			margin-bottom: 20px;
		}
		.peec-tab-header .tab-menus {
			display: flex;
			justify-content: flex-start;
			align-items: center;
			gap: 10px;
		}
		.peec-tab-header .tab-menus span {
			font-weight: 600;
			cursor: pointer;
			display: flex;
			justify-content: space-around;
			align-items: center;
			gap: 10px;
			padding: 7px 15px;
			border: 1px solid;
			border-radius: 8px;
			height: 45px;
		}
		.peec-tab-header .tab-menus span img {
			width: 30px;
		}
		.tab-content-container .tab-content-title {
			font-size: 30px;
			font-weight: 300;
		}
		.tab-content-inner-container {
			display: grid;
			grid-template-columns: repeat(3, 1fr);
			grid-gap: 20px;
			margin-bottom: 40px;
		}
		.tab-content-inner-container > div {
			display: grid;
			grid-gap: 5px;

		}
		.peec-toggle-section {
		}
		.tab-content-inner-container > div span:first-child {
			font-weight: 600;
		}
		.tab-content-2, .tab-content-3 {
			display: none;
		}
		.toggle-content {
			display: none;
			padding: 8px 0;
		}
		.show-toggle-content {
			display: block;
			border-bottom: 1px solid black;
		}
		.peec-toggle-container {
			/* max-width: 640px; */
		}
		.toggle-header {
			display: flex;
			justify-content: space-between;
			align-items: center;
			border-bottom: 1px solid black;
			cursor: pointer;
			padding: 3px 0 4px;
		}
		.toggle-item:nth-child(1) .toggle-header {
			border-top: 1px solid black;
		}
		.toggle-header .toggle-icon {
			width: 25px;
			height: 25px;
			border: 1px solid black;
			border-radius: 50%;
			display: flex;
			justify-content: center;
			align-items: center;
			font-size: 22px;
		}
		.page-tabs__contents {
			padding: 50px 0;
			background-color: #f5f5f5;
		}
		.job-desc {
			padding: 15px 0;
		}
		.job-desc p {
			margin-bottom: 12px;
		}
		.job-desc .desc-title {
			font-weight: bold;
			text-decoration: underline;
		}
		.job-desc .desc-line {
			background-color: #000;
			height: 1px;
			width: 100%;
			margin: 20px 0;
			display: block;
		}
		.job-desc .desc-link {
			text-decoration: none;
			color: blue;
		}
		.mail-to-link {
			color: blue;
		}
		.intro-desc {
			max-width: 1280px;
			margin: auto;
		}
		.intro-desc p {
			font-size: 18px;
			line-height: 1.5;
			margin-bottom: 30px;
		}
		.advertise-btn {
			display: inline-block;
			margin-bottom: 10px;
			padding: 10px 30px;
			font-size: 14px;
			border: 2px solid #000;
			box-sizing: border-box;
			transition: 0.4s;
			font-weight: bold;
		}
		.advertise-btn:hover {
			border-color: #51296f;
			background: #51296f;
			color: #FFF;
			opacity: 1;
		}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title">
						<?php echo GenTitle($peec_data["peec_name_".$lang]); ?>
					</div>
				</div>
			</div>
		</div>

		<section class="sec-content">
			<div class="content-wrapper desc-section">
				<div class="content-div">
					<?php echo $peec_data["peec_detail_".$lang]; ?>
				</div>
				<div class="image-div">
					<?php 
					$peec_image = '';
					if($peec_data["peec_image"] != "") {
						if( file_exists($peec_path . '/1/' . $peec_data["peec_image"]) ) {
							$peec_image = $peec_url . '/1/' . $peec_data["peec_image"];
						}
						elseif( file_exists(str_replace("uploaded_files", "CMS/uploaded_files", $peec_path) . '/1/' . $peec_data["peec_image"]) ) {
							$peec_image = str_replace("uploaded_files", "CMS/uploaded_files", $peec_url) . '/1/' . $peec_data["peec_image"];
						}
						?>
						<img src="<?php echo $peec_image; ?>">
						<?php
					}
					?>
				</div>
			</div>
		</section>

		<?php for ($i = 0; $i < sizeof($sections); $i++) { ?>
			<?php $section = $sections[$i]; ?>
			<?php if ($section['peec_section_section_type'] == 'T') { ?>
				<div style="padding:0.5px 0;background-color: <?php echo $section['peec_section_section_background_color']?>;">
					<section class="section-generated about section-<?php echo $section['peec_section_section_id']; ?>">
						<div class="content-wrapper">
							<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
							<?php echo $section['peec_section_section_content_'.$lang]; ?>
						</div>
					</section>
				</div>
				<?php generateBlock($sections, $i); ?>
			<?php } else if ($section['peec_section_section_type'] == 'Photo') { ?>
				<?php $photos = DM::findAll('peec_section_section_file', 'peec_section_section_file_status = ? AND peec_section_section_file_psid = ?', 'peec_section_section_file_sequence', array(STATUS_ENABLE, $section['peec_section_section_id'])); ?>
				<section class="section-generated photos section-<?php echo $section['peec_section_section_id']; ?>">
					<div class="content-wrapper">
						<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<?php $photo_count = 0; ?>
								<?php foreach ($photos AS $photo) { ?>
									<?php if ($photo_count % 8 == 0) { ?>
										<div class="swiper-slide col">
										<?php } ?>
										<?php 
										$peec_section_file_url_cms = '';
										$peec_section_file_url_cms = str_replace("uploaded_files", "CMS/uploaded_files", $peec_section_file_url); 
										?>
										<a href="<?php echo $peec_section_file_url_cms.$photo['peec_section_section_file_pid'].'/'.$photo['peec_section_section_file_id'].'/'.$photo['peec_section_section_file_filename']; ?>"><img src="<?php echo $peec_section_file_url_cms.$photo['peec_section_section_file_pid'].'/'.$photo['peec_section_section_file_id'].'/'.'crop_'.$photo['peec_section_section_file_filename']; ?>" /></a>

										<?php if ($photo_count % 8 == 7) { ?>
										</div>
									<?php } ?>
									<?php $photo_count++; ?>
								<?php } ?>
							</div>

							<div class="swiper-pagination"></div>
						</div>
					</div>
				</section>
				<!--<div class="acknowledgement"><?php generateBlock($sections, $i); ?></div>-->
			<?php } else if ($section['peec_section_section_type'] == 'Visit') { ?>
				<section class="section-generated access-map section-<?php echo $section['peec_section_section_id']; ?>">
					<div class="content-wrapper">
						<div class="access-info">
							<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
							<?php echo $section['peec_section_section_content_'.$lang]; ?>
						</div>
						<div id="map" class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.2681261428024!2d114.25116361467325!3d22.305697185320742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340403efc89ad0bd%3A0xd45da19c45e22088!2sHKDI%20Gallery!5e0!3m2!1sen!2s!4v1643109476328!5m2!1sen!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
					</div>
				</section>
				<!--<div class="acknowledgement"><?php generateBlock($sections, $i); ?></div>-->
			<?php } ?>
		<?php } ?>

		<div class="page-tabs">
			<div class="intro-desc">
				<p>
					PEECjobs is an online platform for HKDI's industry partners to advertise their job or internship recruitments for FREE. If you are interested in posting your ad through our platform, please contact us directly <a href="mailto:peechkdi@vtc.edu.hk" class="mail-to-link">mailto:peechkdi@vtc.edu.hk</a>
					<br>
					For those who are interested in the posts, please contact the respective company directly. No service charge is required.
				</p>
				<a href="https://forms.office.com/Pages/ResponsePage.aspx?id=qwXbfulCSEO4kyumJaNQxn1PbzXzjr1PpOMuag5sRVlUNUVQNkZGQ1MwMzUzQzNTV05VTFNSRjY4Sy4u&wdLOR=c47CFE584-940A-4A21-81E5-04D04DF5958A" class="advertise-btn" target="blank">Advertise a job</a>
			</div>
			<div class="page-tabs__btns">
				<div class="page-tabs__wrapper">
					<div class="peec-tab-header">
						<div class="tab-menus">
							<span class="tab-menu tab-menu-1" data-id="1">
								<img src="../../images/peec/job-icon.png">
								Jobs
							</span>
							<span class="tab-menu tab-menu-2" data-id="2">
								<img src="../../images/peec/internship-icon.png">
								Internship
							</span>
							<span class=" tab-menu-3" data-id="">
								<img src="../../images/peec/vtc-icon.jpg">
								<a href="https://www.vtc.edu.hk/html/en/career.html">Job Opportunity @VTC</a>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="page-tabs__contents">
				<div class="page-tabs__wrapper">
					<div class="peec-tab-contents">
						<div class="tab-content-container tab-content-1">
							<!-- Toggle section -->
							<div class="peec-toggle-section">
								<div class="peec-toggle-container">
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Vendor Coordinator and Digital Marketing Paid intern / Part-Time Hire</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<div class="job-desc">
												<p><strong>VTech Electronics Limited</strong></p>
												<p><strong>Vendor Coordinator and Digital Marketing Paid intern / Part-Time Hire</strong>  (Job Ref.: PJ2021075)</p>
												<p>> <a href="#" class="desc-link">Visit Contour Impex website</a></p>



												<p class="desc-title">Job Descriptions</p>
												<p>We’re looking for a vendor coordinator and digital marketing assistant to help generate and market content for our textiles trading company. In return you’ll learn business and industry fundamentals to bolster your career in procurement, specifically as it pertains to fashion, sports and conscious buying.</p>
												<ul>
													<li>On board and screen new factories to produce capability profiles across textiles categories;</li>
													<li>Dialogue with vendors weekly to generate ideas for content and sales runs;</li>
													<li>Produce both graphical and HTML content from images and samples in the most creative, interesting ways you can think of. You will be given access to the right tools including Photoshop, Illustrator and Active Campaign (training will be included);</li>
													<li>Responsible for dispatching, cutting, and indexing samples – you will have help!</li>
													<li>Administrative duties; ordering supplies and marketing collateral.</li>
												</ul>
												<p class="desc-title">Academic Qualifications</p>
												<p>Minimum of some university with a focus on fashion or other types of marketing, design.</p>
												<p class="desc-title">Language Requirement</p>
												<p>Ability to converse (including writing) in Putonghua strongly preferred but not required.</p>
												<p class="desc-title">Software Proficiency</p>
												<p>Knowledge of Adobe Illustrator or Photoshop, alternatively Canva is an option.</p>
												<p class="desc-title">Others</p>
												<p>Ability or willingness to learn new marketing tools.</p>

												<span class="desc-line"></span>
												<p class="desc-title">Work Hours</p>
												<p>P/T (3 days a week)</p>
												<p class="desc-title">Compensation</p>
												<p>HK$7,000 / month for P/T (3 days a week)</p>
												<p class="desc-title">Period of the Post</p>
												<p>Start Date: Immediate</p>
												<p>Closing Date: 6 months (3 months probationary) from start date with potential for extension into full-time / long-term employment</p>
												<p class="desc-title">Contact</p>
												<p>Email: exports@contourimpex.com</p>
												<p>Phone: 9284 1078</p>
											</div>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Project Assistant</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Product Developer</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Product Designer</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-content-container tab-content-2">
							<!-- Toggle section -->
							<div class="peec-toggle-section">
								<div class="peec-toggle-container">
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Product Designer</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<div class="job-desc">
												<p><strong>VTech Electronics Limited</strong></p>
												<p><strong>Product Designer (Full-time)</strong></p>
												<p>(Job Ref.: PJ2021072)</p>

												<span class="desc-line"></span>

												<p class="desc-title">About VTech Electronics Limited</p>
												<p>VTech (HKSE: 303) is the global leader in electronic learning products from infancy through toddler and preschool and the largest manufacturer of residential phones in the US. It also provides highly sought-after contract manufacturing services. With headquarters in Hong Kong and state-of-the-art manufacturing facilities in mainland China, Malaysia and Mexico, VTech has approximately 25,000 employees, including about 1,600 R&D professionals, in its operations across 15 countries and regions.</p>
												<p>> <a href="#" class="desc-link">visit VTech Electronics Limited website</a></p>

												<span class="desc-line"></span>

												<p class="desc-title">Job Descriptions</p>
												<ul>
													<li>Design Baby toys, especially educational toys and plush products;</li>
													<li>Perform product research, market analysis, generate new product ideas, rendering and presentation;</li>
													<li>Undertake detail project work and mock up sample presentation;</li>
													<li>Working experience & skills required.</li>
												</ul>

												<span class="desc-line"></span>

												<p class="desc-title">Academic Qualifications</p>
												<ul>
													<li>Higher Diploma or Degree in Design Studies</li>
												</ul>

												<p class="desc-title">Work Experience</p>
												<ul>
													<li>Min 1-2 years toys design experience, plush knowledge is preferable;</li>
													<li>Fresh graduate will also be considered..</li>
												</ul>

												<p class="desc-title">Language Requirement</p>
												<ul>
													<li>Good command of written and spoken English and Chinese</li>
												</ul>

												<p class="desc-title">Software Proficiency</p>
												<ul>
													<li>Able to use design software like Photoshop and Illustrator</li>
												</ul>

												<p class="desc-title">Others</p>
												<ul>
													<li>Good sketching skills;</li>
													<li>Good communication skills;</li>
													<li>Occasional Travel to China office and factories is required.</li>
												</ul>

												<span class="desc-line"></span>

												<p class="desc-title">Work Hours</p>
												<p>8 hours / day</p>

												<p class="desc-title">Compensation</p>
												<p>Double pay, Education allowance, Medical insurance, Performance bonus, Five-day work week, Flexible working hours</p>
												<p><strong>Closing Date</strong></p>
												<p>31 Jan 2022</p>

												<p class="desc-title">Contact</p>
												<p>Email: exports@contourimpex.com</p>
												<p>Phone: 9284 1078</p>
											</div>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Project Assistant</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Product Developer</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
									<div class="toggle-item">
										<div class="toggle-header">
											<span><strong>Product Designer</strong></span>
											<span class="toggle-icon">+</span>
										</div>
										<div class="toggle-content">
											<p><strong>Date : 2022 Jul 4</strong></p>
											<p>Time: Mon & Wed, 7-10pm</p>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="tab-content-container tab-content-3">
							<ul id="latest-news-lists">
								<li>
									<a href="#">Federation of Hong Kong Industries 香港工業總會</a>
								</li>
								<li>
									<a href="#">Hong Kong Watch Manufacturers Association香港表廠商會</a>
								</li>
								<li>
									<a href="#">Hong Kong Jade Association香港玉器商會</a>
								</li>
								<li>
									<a href="#">Hong Kong Jewellerys’ and Goldsmiths’ Association香港珠石玉器金銀首飾業商會</a>
								</li>
								<li>
									<a href="#">The Kowloon Pearls, Precious Stones, Jade, Gold and Silver Ornament Merchants Association九龍珠石玉器金銀首飾業商會</a>
								</li>
								<li>
									<a href="#">Hong Kong Opitcal Maunfacturers Association香港中華眼鏡製造廠商會</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>


			<section class="sec-content" style="margin-bottom: 4em;">
				<div class="content-wrapper desc-section">
					<div class="content-div">
						<div class="disclaimer-div">
							<h2>Disclaimer</h2>
							<?php echo $peec_data["peec_disclaimer_".$lang]; ?>
						</div>
					</div>
				</div>
			</section>

		</div>


		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

<script>
	const tabMenuBtns = document.querySelectorAll('.tab-menu');
	const tabContainers = document.querySelectorAll('.tab-content-container');

	tabMenuBtns.forEach((btn) => {
		btn.addEventListener('click', () => {
			const contentId = btn.dataset.id;

			tabContainers.forEach((container) => {
				container.style.display = 'none';
			});

			document.querySelector('.tab-content-' + contentId).style.display = 'block';
		})
	});

        // Toggle part
        const toggleMenus = document.querySelectorAll('.toggle-header');

        toggleMenus.forEach((menu) => {
        	menu.addEventListener('click', () => {
        		const x = menu.querySelector('.toggle-icon');
        		if(x.innerHTML == '+') {
        			x.innerHTML = '-';
        		} else {
        			x.innerHTML = '+';
        		}
        		menu.nextElementSibling.classList.toggle('show-toggle-content');
        	})

        })

        
    </script>
    </html>
