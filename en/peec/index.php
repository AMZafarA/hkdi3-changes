<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Peec';
$section = 'peec';
$subsection = 'index';
$sub_nav = 'index';

$breadcrumb_arr['Peec'] ='';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$tab = (@$_REQUEST['tab'] == '' ? 'all' : $_REQUEST['tab']);
$page = (@$_REQUEST['page'] == '' ? 1 : $_REQUEST['page']);

$conditions = "news_status = ? AND news_from_date <= ? AND (news_to_date >= ? OR news_to_date IS NULL)";
$parameters = array(STATUS_ENABLE, $today, $today);
$news_page_no = ($tab == 'all' || $tab == '' ? $page : 1);
$news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions, " news_date DESC, news_id DESC", $parameters);
$total_news_count = DM::count($DB_STATUS.'news', $conditions, $parameters);
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => '最新動態', 'lang3' => '最新动态'),
//	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => '學生得獎作品', 'lang3' => '学生得奖作品'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => '活動', 'lang3' => '活动'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => '其他活動', 'lang3' => '其他活动')
);
for ($i = 0; $i < sizeof($news_types); $i++) {
	$news_page_no = ($tab == $news_types[$i]['value'] ? $page : 1);
	$parameters2 = array_merge($parameters, array($news_types[$i]['value']));
	// combine hkdi_news and student_award
	// $news_types[0] = hkdi_news;
	if($i == 0){
		$conditions2 = $conditions." AND (news_type = ? OR news_type = ?)";
		array_push($parameters2, 'student_award');
	}else{
		$conditions2 = $conditions." AND news_type = ?";
	}
	$temp_news_list = DM::findAllWithPaging($DB_STATUS.'news', $news_page_no, $news_page_size, $conditions2, " news_date DESC, news_id DESC", $parameters2);
	$temp_news_total_count = DM::count($DB_STATUS.'news', $conditions2, $parameters2);
	$news_types[$i]['list'] = $temp_news_list;
	$news_types[$i]['total_count'] = $temp_news_total_count;
}


$latest_issue = DM::findOne($DB_STATUS.'issue', 'issue_status = ?', 'issue_date DESC', array(STATUS_ENABLE));
$issue_list = DM::findAll($DB_STATUS.'issue', 'issue_status = ? AND issue_id <> ?', 'issue_date DESC', array(STATUS_ENABLE, $latest_issue['issue_id']));

$peec_menu_lv1 = DM::findAll($DB_STATUS.'peec_menu', ' peec_menu_status = ? AND peec_menu_lv = ?', " peec_menu_sort_order ASC", array(STATUS_ENABLE,1));

$peec_data = DM::findOne($DB_STATUS.'peec_page', ' peec_page_id = ?', '', array(1));
$sections = DM::findAll('peec_section_section', 'peec_section_section_pid = ?', '', array(1));

function GenTitle($title){
	$title_arr = explode(" ", $title);
	$count_arr = count($title_arr);
	$h2_title = str_replace($title_arr[$count_arr-1],"",$title);

	if($count_arr ==1){
		return "<h2>".$title."</h2>";
	}else{
		return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
	}
}

function generateBlock($peec_section_sections, $loopI) {
	global $lang, $peec_section_file_url;
	$peec_section_file_url_cms = '';
	$peec_section_file_url_cms = str_replace("uploaded_files", "CMS/uploaded_files", $peec_section_file_url);
	for ($i = ($loopI + 1); $i < sizeof($peec_section_sections); $i++) {
		$peec_section_section = $peec_section_sections[$i];
		if ($peec_section_section['peec_section_section_type'] == 'Acknowledgement') {
			$peec_section_section_files = DM::findAll($DB_STATUS.'peec_section_section_file', 'peec_section_section_file_status = ? AND peec_section_section_file_psid = ?', 'peec_section_section_file_sequence', array(STATUS_ENABLE, $peec_section_section['peec_section_section_id']));
			?>
			<div class="block">
				<p><?php echo $peec_section_section['peec_section_section_title_'.$lang]; ?></p>
				<?php foreach ($peec_section_section_files AS $peec_section_section_file) { ?>
					<img src="<?php echo $peec_section_file_url_cms.$peec_section_section_file['peec_section_section_file_pid'].'/'.$peec_section_section_file['peec_section_section_file_id'].'/'.$peec_section_section_file['peec_section_section_file_filename']; ?>" />
				<?php } ?>
			</div>
		<?php } else if ($peec_section_section['peec_section_section_type'] == 'CT') { ?>
			<div class="hidden-block">
				<h3><?php echo $peec_section_section['peec_section_section_title_'.$lang]; ?></h3>
				<div class="hidden">
					<?php echo $peec_section_section['peec_section_section_content_'.$lang]; ?>           
				</div>
			</div>
		<?php } else { 
			break;
		}
	}
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<style>
		.sec-publication {
			overflow: unset !important;
		}
		.sec-publication__img img {
			width: 100%;
		}
		.peec-tab-container {
			margin-bottom: 80px;
		}
		.peec-tab-header {
			margin-bottom: 20px;
		}
		.peec-tab-header .tab-menus {
			display: flex;
			justify-content: flex-start;
			align-items: center;
			gap: 10px;
		}
		.peec-tab-header .tab-menus span {
			font-weight: 600;
			cursor: pointer;
		}
		.tab-content-container .tab-content-title {
			font-size: 30px;
			font-weight: 300;
		}
		.tab-content-inner-container {
			display: grid;
			grid-template-columns: repeat(3, 1fr);
			grid-gap: 20px;
			margin-bottom: 40px;
		}
		.tab-content-inner-container > div {
			display: grid;
			grid-gap: 5px;

		}
		.peec-toggle-section {
			margin-bottom: 80px;
		}
		.tab-content-inner-container > div span:first-child {
			font-weight: 600;
		}
		.tab-content-1, .tab-content-3 {
			display: none;
		}
		.toggle-content {
			display: none;
			padding: 8px 0;
		}
		.show-toggle-content {
			display: block;
			border-bottom: 1px solid black;
		}
		.peec-toggle-container {
			max-width: 640px;
		}
		.toggle-header {
			display: flex;
			justify-content: space-between;
			align-items: center;
			border-bottom: 1px solid black;
			cursor: pointer;
			padding: 3px 0 4px;
		}
		.toggle-item:nth-child(1) .toggle-header {
			border-top: 1px solid black;
		}
		.toggle-header .toggle-icon {
			width: 25px;
			height: 25px;
			border: 1px solid black;
			border-radius: 50%;
			display: flex;
			justify-content: center;
			align-items: center;
			font-size: 22px;
		}
		.fee-information-section {
			margin-bottom: 80px;
		}
		#latest-news-lists li {
			margin-bottom: 8px;
		}
		#latest-news-lists li a {
			font-size: 18px;
			color: blue;
		}
		#latest-news-lists li a:hover {
			text-decoration: underline;
		}
		#entry-requirement-section p {
			margin-bottom: 10px;
		}
		#peec-menu-container .dropdown {
			margin: 0;
			padding: 0;
			list-style: none;
			width: 150px;
			background-color: #0abf53;
		}

		#peec-menu-container .dropdown li {
			position: relative;
			z-index: 9 !important;
		}

		#peec-menu-container .dropdown li a {
			color: #000;
			background: #fafafa !important;
			text-align: center;
			text-decoration: none;
			display: block;
			padding: 10px;
		}

		#peec-menu-container .dropdown li ul {
			position: absolute;
			top: 100%;
			margin: 0;
			padding: 0;
			list-style: none;
			display: none;
			line-height: normal;
			background-color: #333;
			min-width: 150px;
		}

		#peec-menu-container .dropdown li ul li a {
			text-align: left;
			color: #1a1a1a;
			font-size: 14px;
			padding: 10px;
			display: block;
			white-space: nowrap;
		}

		#peec-menu-container .dropdown li ul li a:hover {
		}

		#peec-menu-container .dropdown li ul li ul {
			left: 100%;
			top: 0;
		}

		#peec-menu-container ul li:hover>a {
		}

		#peec-menu-container ul li:hover>ul {
			display: block;
		}
		.content-wrapper.desc-section {
			display: flex;
			flex-flow: wrap;
		}
		.content-wrapper.desc-section .content-div {
			width: 55%;
			margin-right: 5%;
		}
		.content-wrapper.desc-section .image-div {
			width: 40%;
		}
		@media only screen and (max-width: 480px) {
			.content-wrapper.desc-section > div {
				width: 100% !important;
			}
		}
		.content-wrapper.image-section {
			margin-bottom: 4em;
		}
	</style>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
</head>

<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="page-head">
			<div class="page-head__wrapper">
				<div class="page-head__title-holder">
					<div class="page-head__title">
						<?php echo GenTitle($peec_data["peec_name_".$lang]); ?>
					</div>
				</div>
			</div>
		</div>

		<section class="sec-content">
			<div class="content-wrapper content-section">
				<div class="content-div">
					<?php echo $peec_data["peec_detail_".$lang]; ?>
				</div>
			</div>
		</section>

		<section class="sec-publication">
			<div class="content-wrapper">
				<div id="peec-menu-container">
					<ul class="dropdown">
						<li>
							<a style="font-size: 20px;">PEEC Menu</a>
							<ul>
								<li>
									<a>
										Programmes
									</a>
									<ul>
										<?php
										$peec_programmes_categories = DM::findAll($DB_STATUS.'peec_programmes_categories', ' peec_programmes_categories_status = ?', " peec_programmes_categories_id ASC", array(STATUS_ENABLE));
										foreach ($peec_programmes_categories as $ppc) {
											?>
											<li>
												<a href="./peec_programmes/<?php echo $ppc["peec_programmes_categories_slug"]; ?>"><?php echo $ppc["peec_programmes_categories_name_".$lang]; ?></a>
											</li>
											<?php
										}
										?>
									</ul>
								</li>
								<li>
									<a href="./peecjobs.php">PEECjobs</a>
								</li>
								<?php
								// foreach ($peec_menu_lv1 as $lv1) {
								// 	$peec_menu_lv2 = DM::findAll($DB_STATUS.'peec_menu', ' peec_menu_status = ? AND peec_menu_parent = ? AND peec_menu_lv = ?', "peec_menu_sort_order ASC", array(STATUS_ENABLE,$lv1["peec_menu_id"],2));
								// 	$href1 = count($peec_menu_lv2) == 0 ? $lv1['peec_menu_href'] : '';
								// 	$l = '';
								// 	$l .= '<li><a' . ($href1 != '' ? ' href="' . $href1 . '">' : '>') . $lv1["peec_menu_name_".$lang] . '</a>';
								// 	if(count($peec_menu_lv2) > 0) {
								// 		$l .= '<ul>';
								// 		foreach ($peec_menu_lv2 as $lv2) {
								// 			$peec_menu_lv3 = DM::findAll($DB_STATUS.'peec_menu', ' peec_menu_status = ? AND peec_menu_parent = ? AND peec_menu_lv = ?', "peec_menu_sort_order ASC", array(STATUS_ENABLE,$lv2["peec_menu_id"],3));
								// 			$href2 = count($peec_menu_lv3) == 0 ? $lv2['peec_menu_href'] : '';
								// 			$l .= '<li><a' . ($href2 != '' ? ' href="' . $href2 . '">' : '>') . $lv2["peec_menu_name_".$lang] . '</a>';
								// 			if(count($peec_menu_lv3) > 0) {
								// 				$l .= '<ul>';
								// 				foreach ($peec_menu_lv3 as $lv3) {
								// 					$href3 = $lv3['peec_menu_href'];
								// 					$l .= '<li><a href="' . $href3 . '">' . $lv3["peec_menu_name_".$lang] . '</a></li>';
								// 				}
								// 				$l .= '</ul>';
								// 			}
								// 			$l .= '</li>';
								// 		}
								// 		$l .= '</ul>';
								// 	}
								// 	$l .= '</li>';

								// 	echo $l;
								// }
								?>
							</ul>
						</li>
					</ul>
				</div>

			</div>
		</section>

		<section class="sec-content">
			<div class="content-wrapper image-section">
				<div class="image-div">
					<?php 
					$peec_image = '';
					if($peec_data["peec_image"] != "") {
						if( file_exists($peec_path . '/1/' . $peec_data["peec_image"]) ) {
							$peec_image = $peec_url . '/1/' . $peec_data["peec_image"];
						}
						elseif( file_exists(str_replace("uploaded_files", "CMS/uploaded_files", $peec_path) . '/1/' . $peec_data["peec_image"]) ) {
							$peec_image = str_replace("uploaded_files", "CMS/uploaded_files", $peec_url) . '/1/' . $peec_data["peec_image"];
						}
						?>
						<img src="<?php echo $peec_image; ?>">
						<?php
					}
					?>
				</div>
			</div>
		</section>




		<?php for ($i = 0; $i < sizeof($sections); $i++) { ?>
			<?php $section = $sections[$i]; ?>
			<?php if ($section['peec_section_section_type'] == 'T') { ?>
				<!-- <div style="padding:0.5px 0;background-color: <?php echo $section['peec_section_section_background_color']?>;">
					<section class="section-generated about section-<?php echo $section['peec_section_section_id']; ?>">
						<div class="content-wrapper">
							<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
							<?php echo $section['peec_section_section_content_'.$lang]; ?>
						</div>
					</section>
				</div> -->
				<?php // generateBlock($sections, $i); ?>
			<?php } else if ($section['peec_section_section_type'] == 'Photo') { ?>
				<?php $photos = DM::findAll('peec_section_section_file', 'peec_section_section_file_status = ? AND peec_section_section_file_psid = ?', 'peec_section_section_file_sequence', array(STATUS_ENABLE, $section['peec_section_section_id'])); ?>
				<!-- <section class="section-generated photos section-<?php echo $section['peec_section_section_id']; ?>">
					<div class="content-wrapper">
						<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<?php $photo_count = 0; ?>
								<?php foreach ($photos AS $photo) { ?>
									<?php if ($photo_count % 8 == 0) { ?>
										<div class="swiper-slide col">
										<?php } ?>
										<?php 
										$peec_section_file_url_cms = '';
										$peec_section_file_url_cms = str_replace("uploaded_files", "CMS/uploaded_files", $peec_section_file_url); 
										?>
										<a href="<?php echo $peec_section_file_url_cms.$photo['peec_section_section_file_pid'].'/'.$photo['peec_section_section_file_id'].'/'.$photo['peec_section_section_file_filename']; ?>"><img src="<?php echo $peec_section_file_url_cms.$photo['peec_section_section_file_pid'].'/'.$photo['peec_section_section_file_id'].'/'.'crop_'.$photo['peec_section_section_file_filename']; ?>" /></a>

										<?php if ($photo_count % 8 == 7) { ?>
										</div>
									<?php } ?>
									<?php $photo_count++; ?>
								<?php } ?>
							</div>
							
							<div class="swiper-pagination"></div>
						</div>
					</div>
				</section> -->
				<!--<div class="acknowledgement"><?php generateBlock($sections, $i); ?></div>-->
			<?php } else if ($section['peec_section_section_type'] == 'Visit') { ?>
				<!-- <section class="section-generated access-map section-<?php echo $section['peec_section_section_id']; ?>">
					<div class="content-wrapper">
						<div class="access-info">
							<?php echo GenTitle($section['peec_section_section_title_'.$lang]);?>
							<?php echo $section['peec_section_section_content_'.$lang]; ?>
						</div>
						<div id="map" class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.2681261428024!2d114.25116361467325!3d22.305697185320742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340403efc89ad0bd%3A0xd45da19c45e22088!2sHKDI%20Gallery!5e0!3m2!1sen!2s!4v1643109476328!5m2!1sen!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
					</div>
				</section> -->
				<!--<div class="acknowledgement"><?php generateBlock($sections, $i); ?></div>-->
			<?php } ?>
		<?php } ?>

		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>

	<script>
		const tabMenuBtns = document.querySelectorAll('.tab-menu');
		const tabContainers = document.querySelectorAll('.tab-content-container');

		tabMenuBtns.forEach((btn) => {
			btn.addEventListener('click', () => {
				const contentId = btn.dataset.id;

				tabContainers.forEach((container) => {
					container.style.display = 'none';
				});

				document.querySelector('.tab-content-' + contentId).style.display = 'block';
			})
		});

		// Toggle part
		const toggleMenus = document.querySelectorAll('.toggle-header');

		toggleMenus.forEach((menu) => {
			menu.addEventListener('click', () => {
				const x = menu.querySelector('.toggle-icon');
				if(x.innerHTML == '+') {
					x.innerHTML = '-';
				} else {
					x.innerHTML = '+';
				}
				menu.nextElementSibling.classList.toggle('show-toggle-content');
			})

		})
	</script>
</body>

</html>
