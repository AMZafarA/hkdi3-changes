<?php

$inc_root_path = '';error_reporting(0);
require_once "../../include/config.php";
require_once "../../include/PHPMailer/PHPMailerAutoload.php";
include_once '../../include/securimage/securimage.php';
include_once "../../include/classes/DataMapper.php";

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$v["company_name"] = $_POST["Company_Name_(English_/_Chinese)"];
$v["entry_status"] = 0;
$peecjob_entry_id = DM::insert("peecjob_entry",$v);

if(isInteger($peecjob_entry_id)) {
	foreach ($_POST as $key => $value) {
		$arr = array(); 
		$arr["peecjob_entry_id"] = $peecjob_entry_id;
		$key = str_replace("_", " ", $key);
		$value = str_replace("_", " ", $value);
		if(is_array($value)) {
			$value = implode(", ", $value);
		}
		$arr["entry_key"] = $key;
		$arr["entry_value"] = $value;
		DM::insert("peecjob_entry_details",$arr);
	}

	// send email
	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');

	// $mail->IsSMTP();

	// $mail->Host = $SYSTEM_SMTP_SERVER;
	// $mail->SMTPAuth = $smtp_require_auth;
	// $mail->Username  = $smtp_username;
	// $mail->Password   = $smtp_password;
	// $mail->SMTPSecure = $smtp_security;
	// $mail->Port = $smtp_port;
	$mail->IsHTML(true);

	$mail->SetFrom("hkdi-booking@vtc.edu.hk", "HKDI");
	$subject = "PEEC - Job Posting";
	$mail->CharSet = "UTF-8";

	$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Dear PEEC Team,<br /><br />";
	$content .= "A Job posting form has been submitted and is awaiting your approval.<br /><br /><br />";
	$content .= "PEECjobs";
	$content .= "</div>";

	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);

	$mail->AddAddress("cellis.testing123@gmail.com",'');
	$mail->Send();
	$mail->ClearAddresses();
}

if(isInteger($peecjob_entry_id)) {
	header("location: peecjobs_form.php?submit=true");
}
else {
	header("location: peecjobs_form.php?submit=false");
}

?>