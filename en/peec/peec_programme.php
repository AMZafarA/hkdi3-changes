<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = '';
$section = '';
$subsection = '';
$sub_nav = '';


DM::setup($db_host, $db_name, $db_username, $db_pwd);
$peec_programmes_slug = @$_REQUEST['peec_programme'] ?? '';
$peec_programmes = DM::findOne($DB_STATUS.'peec_programmes', 'peec_programmes_status = ? AND peec_programmes_slug = ?', '', array(STATUS_ENABLE, $peec_programmes_slug));
if($peec_programmes == "") {
	header("location: ./index.php");
}
$peec_programmes_id = $peec_programmes["peec_programmes_id"];
$peec_programmes_sections = DM::findAll($DB_STATUS.'peec_programmes_section', 'peec_programmes_section_status = ? AND peec_programmes_section_pid = ?', 'peec_programmes_section_sequence', array(STATUS_ENABLE, $peec_programmes_id));

$news_page_no = (@$_REQUEST['news_page_no'] == '' ? 1 : $_REQUEST['news_page_no']);


$breadcrumb_arr['PEEC Programmes'] =$host_name_with_lang.'peec';
$breadcrumb_arr[$peec_programmes['peec_programmes_name_'.$lang]] ='';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
	<?php include $inc_root_path . "inc_meta.php"; ?>
	<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $host_name?>css/gallery.css" type="text/css" />
	<script type="text/javascript" src="<?php echo $host_name?>js/gallery.js"></script>
	<script type="text/javascript" src="<?php echo $host_name?>js/knowledge_centre.js"></script>
	<style>
		.contact-us{
			background: <?php echo $contactBg; ?> !important;
		}
		/*#section-11{
			background:#ebcc3b !important;
		}
		#section-37{
			background:#b77070 !important;
		}
		#section-58{
			background:#79698f !important;
		}
		#section-60{
			background:#3d9f9b !important;
		}*/
		.content {
			width: 100%;
			position: relative;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			-webkit-flex-direction: row;
			-ms-flex-direction: row;
			flex-direction: row;
			-webkit-flex-wrap: nowrap;
			-ms-flex-wrap: nowrap;
			flex-wrap: nowrap;
			-webkit-justify-content: space-between;
			-ms-flex-pack: justify;
			justify-content: space-between;
			-webkit-align-content: stretch;
			-ms-flex-line-pack: stretch;
			align-content: stretch;
			-webkit-align-items: flex-start;
			-ms-flex-align: start;
			align-items: flex-start;
		}
		.content > div {
			display: inline-block;
			width: calc(50% - 30px);
		}
	</style>
</head>

<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
	<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
	<h1 class="access"><?php echo $page_title?></h1>
	<main>
		<div class="top-divide-line"></div>
		<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
		<div class="top-nav">
			<div class="top-nav-left">
				<!--<a class="back" href="index.php">Back</a>-->
			</div>
			<!--<div class="top-nav-center">
				<div class="top-nav__title"><?php echo $peec_programmes['peec_programmes_name_'.$lang]; ?></div>
			</div>-->
		</div>
		<div class="content-wrapper">
			<div class="content">
				<div class="content-div">
					<?php for ($i = 0; $i < sizeof($peec_programmes_sections); $i++) { ?>
						<?php $peec_programmes_section = $peec_programmes_sections[$i]; ?>
						<?php if ($peec_programmes_section['peec_programmes_section_type'] == 'T') { ?>
							<section class="about section-<?php echo $peec_programmes_section['peec_programmes_section_id']; ?>" style="background-color: <?php echo $peec_programmes_section['peec_programmes_section_background_color']?>;">
								<?php echo GenTitle($peec_programmes_section['peec_programmes_section_title_'.$lang]);?>
								<?php echo $peec_programmes_section['peec_programmes_section_content_'.$lang]; ?>
							</section>
							<?php generateBlock($peec_programmes_sections, $i); ?>

						<?php } else if ($peec_programmes_section['peec_programmes_section_type'] == 'Photo') { ?>
							<?php $photos = DM::findAll($DB_STATUS.'peec_programmes_section_file', 'peec_programmes_section_file_status = ? AND peec_programmes_section_file_psid = ?', 'peec_programmes_section_file_sequence', array(STATUS_ENABLE, $peec_programmes_section['peec_programmes_section_id'])); ?>
							<section class="photos section-<?php echo $peec_programmes_section['peec_programmes_section_id']; ?>">
								<h2><?php echo $peec_programmes_section['peec_programmes_section_title_'.$lang]; ?></h2>
								<div class="swiper-container">
									<div class="swiper-wrapper">
										<?php $photo_count = 0; ?>
										<?php foreach ($photos AS $photo) { ?>
											<?php if ($photo_count % 8 == 0) { ?>
												<div class="swiper-slide col">
												<?php } ?>
												<a href="<?php echo $product_file_url.$photo['peec_programmes_section_file_pid'].'/'.$photo['peec_programmes_section_file_id'].'/'.$photo['peec_programmes_section_file_filename']?>"><img src="<?php echo $product_file_url.$photo['peec_programmes_section_file_pid'].'/'.$photo['peec_programmes_section_file_id'].'/crop_'.$photo['peec_programmes_section_file_filename']; ?>" /></a>
												<?php if ($photo_count % 8 == 7) { ?>
												</div>
											<?php } ?>
											<?php $photo_count++; ?>
										<?php } ?>
									</div>

								</div>
								<div class="swiper-pagination"></div>
							</section>
							<?php generateBlock($peec_programmes_sections, $i); ?>
						<?php } else if ($peec_programmes_section['peec_programmes_section_type'] == 'Visit') { ?>
							<section class="access-map section-<?php echo $peec_programmes_section['peec_programmes_section_id']; ?>">
								<div class="access-info">
									<h2><?php echo $peec_programmes_section['peec_programmes_section_title_'.$lang]; ?></h2>
									<?php echo $peec_programmes_section['peec_programmes_section_content_'.$lang]; ?>
								</div>
								<div id="map" class="map"></div>
							</section>
							<?php generateBlock($peec_programmes_sections, $i); ?>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="banner-div">
					<?php if ($peec_programmes['peec_programmes_banner'] != '' && (file_exists($peec_programmes_path.$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner']) || file_exists(str_replace("uploaded_files", "CMS/uploaded_files", $peec_programmes_path).$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner'])) ) { ?>
						<?php if (file_exists($peec_programmes_path.$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner']) ) { ?>
							<img src="<?php echo $peec_programmes_url.$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner']; ?>" />
						<?php } elseif(file_exists(str_replace('uploaded_files', "CMS/uploaded_files", $peec_programmes_path).$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner']) ) { ?>
							<img src="<?php echo str_replace('uploaded_files', "CMS/uploaded_files", $peec_programmes_url).$peec_programmes['peec_programmes_id'].'/'.$peec_programmes['peec_programmes_banner']; ?>" />
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</main>
	<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
	<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
</body>

</html>

<?php
function generateBlock($product_sections, $loopI)
{
	global $lang, $product_file_url;
	for ($i = ($loopI + 1); $i < sizeof($product_sections); $i++) {
		$product_section = $product_sections[$i];
		if ($product_section['product_section_type'] == 'Acknowledgement') {
			$product_section_files = DM::findAll($DB_STATUS.'product_section_file', 'product_section_file_status = ? AND product_section_file_psid = ?', 'product_section_file_sequence', array(STATUS_ENABLE, $product_section['product_section_id']));
			?>
			<div class="block">
				<p><?php echo $product_section['product_section_title_'.$lang]; ?></p>
				<?php foreach ($product_section_files AS $product_section_file) { ?>
					<img src="<?php echo $product_file_url.$product_section_file['product_section_file_pid'].'/'.$product_section_file['product_section_file_id'].'/'.$product_section_file['product_section_file_filename']; ?>" />
				<?php } ?>
			</div>
		<?php } else if ($product_section['product_section_type'] == 'CT') { ?>
			<div class="hidden-block">
				<h3><?php echo $product_section['product_section_title_'.$lang]; ?></h3>
				<div class="hidden">
					<?php echo $product_section['product_section_content_'.$lang]; ?>           
				</div>
			</div>
		<?php } else { 
			break;
		}
	}
}
function GenTitle($title){
	$title_arr = explode(" ", $title);
	$count_arr = count($title_arr);
	$h2_title = str_replace($title_arr[$count_arr-1],"",$title);

	if($count_arr ==1){
		return "<h2>".$title."</h2>";
	}else{
		return "<h2>".$h2_title."<span>".$title_arr[$count_arr-1]."</span></h2>";
	}


}
?>