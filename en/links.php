<?php
$inc_root_path = '../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Style Demo';
$section = 'style-demo';
$subsection = 'index';
$sub_nav = 'style-demo';
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

    <head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
        <?php include $inc_root_path . "inc_meta.php"; ?>
        <link rel="stylesheet" href="<?php echo $host_name?>css/style-demo.css" type="text/css" />
        <style>
            .site-links__grp {
                padding: 30px 0;
                border-bottom: 1px solid rgba(0,0,0,0.3);
            }
            
            .site-links__item {
                position: relative;
                margin-bottom: 20px;
            }
            .site-links__item h2 {
                margin-bottom: 30px;
            }
            
            .site-links__item h3 {
                font-size: 18px;
                margin-bottom: 8px;
            }
            .site-links__item a {
                text-decoration:underline;
            }
        </style>
    </head>

    <body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
        <?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
        <?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
        <h1 class="access"><?php echo $page_title?></h1>
        <main>
            <div class="site-links">
                <div class="content-wrapper">
                    <div class="site-links__grp">
                        <div class="site-links__item">
                            <h2>Home Page</h2>
                            <a href="<?php echo $host_name_with_lang?>">http://extranet.firmstudio.com/HKDI/wwwroot/en/</a>
                        </div>
                    </div>
                    <div class="site-links__grp">
                        <h2>Programme</h2>
                        <div class="site-links__item">
                            <h3>HKDI_01_Programmes_00.jpg:</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/index.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/index.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_01_Programmes_01.jpg:</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/programme.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/programme.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_01_Programmes_02.jpg:</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/award.php<">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/award.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_01_Programmes_04_01.jpg / HKDI_01_Programmes_04_02.jpg:</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/alumni.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/alumni.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_01_Programmes_05.jpg:</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/course.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/course.php</a>
                        </div>
                    </div>
                    <div class="site-links__grp">
                        <h2>Gallery</h2>
                        <div class="site-links__item">
                            <h3>HKDI_02_HKDIGallery_00_01.jpg / HKDI_02_HKDIGallery_00_02.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>gallery/index.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/gallery/index.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_02_HKDIGallery_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>gallery/gallery.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/gallery/gallery.php</a>
                        </div>
                    </div>




                    <div class="site-links__grp">
                        <h2>Bachelors Degree</h2>
                        <div class="site-links__item">
                            <h3>HKDI_03_BachelorsDegree_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/bachelor-degree.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_03_BachelorsDegree_02.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/BA-fine-art.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/BA-fine-art.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_03_BachelorsDegree_03.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/BA-visual-communication.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/BA-visual-communication.php</a>
                        </div>
                    </div>

                    <div class="site-links__grp">
                        <h2>EDT</h2>
                        <div class="site-links__item">
                            <h3>HKDI_04_EDT_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>edt/edt-2017.php<">http://extranet.firmstudio.com/HKDI/wwwroot/en/edt/edt-2017.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_04_EDT_02.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>edt/edt-info-day.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/edt/edt-info-day.php</a>
                        </div>
                    </div>

                    <div class="site-links__grp">
                        <h2>News</h2>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_00.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/news-detail.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/news-detail.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_02.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/publication.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/publication.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_03.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/publication-detail.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/publication-detail.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_04a.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/event.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/event.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_04b.jpg / HKDI_05_News_04c.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/event-list.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/event-list.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_05_News_04d.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>news/event-2.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/news/event-2.php</a>
                        </div>
                    </div>

                    <div class="site-links__grp">
                        <h2>Knowledge Centre</h2>
                        <div class="site-links__item">
                            <h3>HKDI_06_Knowledge_00.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>knowledge-centre/index.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/knowledge-centre/index.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_06_Knowledge_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>knowledge-centre/detail.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/knowledge-centre/detail.php</a>
                        </div>
                    </div>


                    <div class="site-links__grp">
                        <h2>Master</h2>
                        <div class="site-links__item">
                            <h3>HKDI_09_Master_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/master-degree.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/master-degree.php</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_09_Master_02.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>programmes/master-degree-details.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/programmes/master-degree-details.php</a>
                        </div>
                    </div>

                    <div class="site-links__grp">
                        <h2>International</h2>
                        <div class="site-links__item">
                            <h3>HKDI_10_International_00.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>global-learning/">http://extranet.firmstudio.com/HKDI/wwwroot/en/global-learning/</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_10_International_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>global-learning/international-details.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/global-learning/international-details.php</a>
                        </div>
                    </div>

                    <div class="site-links__grp">
                        <h2>IndustrialCollaboration</h2>
                        <div class="site-links__item">
                            <h3>HKDI_11_IndustrialCollaboration_00.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>en/industrial-collaborations/">http://extranet.firmstudio.com/HKDI/wwwroot/en/industrial-collaborations/</a>
                        </div>
                        <div class="site-links__item">
                            <h3>HKDI_11_IndustrialCollaboration_01.jpg</h3>
                            <a href="<?php echo $host_name_with_lang?>industrial-collaborations/industrial-collaborations-details.php">http://extranet.firmstudio.com/HKDI/wwwroot/en/industrial-collaborations/industrial-collaborations-details.php</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
    </body>

    </html>