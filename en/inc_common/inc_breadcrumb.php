<nav class="breadcrumb">
	<div class="content-wrapper">
		<div class="breadcrumb__item">
			<a href="<?php echo $host_name_with_lang?>" class="breadcrumb__home-link"></a>
		</div>
		<?php
			$last_key = end(array_keys($breadcrumb_arr));
			foreach ($breadcrumb_arr as $key => $value) {
			
		?>
		<div class="breadcrumb__item-arrow">&gt;</div>
		<div class="breadcrumb__item">
		<?php if($key == $last_key){ ?>
			<strong class="breadcrumb__link"><?php echo $key?></strong>
		<?php }else{ ?>
			<a href="<?php echo $value?>" class="breadcrumb__link"><?php echo $key?></a>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</nav>