<!-- Footer -->
<footer>
	<a href="#" class="footer__backtotop" title="Back to top"></a>
	<div class="content-wrapper">
		<div class="footer__top">
			<div class="footer__panel">
				<div class="footer__panel-item">
					<!--<a href="#" class="footer_panel__subscription"><h2 class="footer__panel-title">Subscription</h2><p class="footer__panel-tag">Subscribe our newsletter and <br>event information</p></a>-->
					<h2 class="footer__panel-title">Location</h2>
					<p>3 King Ling Road, Tseung Kwan O, Hong Kong</p>
				</div>
				<div class="footer__panel-item">
					<h2 class="footer__panel-title"></h2>
					<div class="btn-share__holder">
						<!--<a href="" class="btn-share btn-share--email"></a>-->
						<!--<a href="" class="btn-share btn-share--wa"></a>-->
						<a href="http://www.facebook.com/HKDI.HongKongDesignInstitute" class="btn-share btn-share--fb" target="_blank"></a>
						<a href="https://instagram.com/hkdi_hongkongdesigninstitute" class="btn-share btn-share--ig" target="_blank"></a>
						<a href="http://www.youtube.com/user/hkdichannel" class="btn-share btn-share--youtube" target="_blank"></a>
						<!--<a href="http://www.twitter.com/thehkdi" class="btn-share btn-share--tw" target="_blank"></a>-->
						<a href="https://www.linkedin.com/school/hong-kong-design-institute/?originalSubdomain=hk" class="btn-share btn-share--ln" target="_blank"></a>
						<!--<a href="" class="btn-share btn-share--ln"></a>-->
						<!--<a href="http://weibo.com/thehkdi" class="btn-share btn-share--wb" target="_blank"></a>-->
					</div>
				</div>
			</div>
			<div class="footer__sitemap">
				<div class="footer__sitemap-grp footer__sitemap-grp--primary">
					<a href="<?php echo $host_name_with_lang?>about" class="footer__sitemap-link">About Us</a>
					<a href="<?php echo $host_name_with_lang?>programmes" class="footer__sitemap-link">Design Programmes</a>
					<a href="<?php echo $host_name_with_lang?>knowledge-centre" class="footer__sitemap-link">Knowledge Centres</a>
					<!--<a href="http://www.hkdi.edu.hk/hkdi_gallery/index.php" class="footer__sitemap-link">HKDI Gallery</a>-->
					<a target="_blank" href="http://www.hkdi.edu.hk/hkdi_gallery/index.php" class="footer__sitemap-link">HKDI Gallery Presents</a>
					<!--<a href="<?php echo $host_name_with_lang?>edt" class="footer__sitemap-link">Emerging Design Talents</a>-->
					<a href="<?php echo $host_name_with_lang?>programmes/award.php?id=7" class="footer__sitemap-link">Student Awards</a>
					<a href="<?php echo $host_name_with_lang?>global-learning" class="footer__sitemap-link">Global Learning</a>
					<!-- <a href="<?php echo $host_name_with_lang?>industrial-collaborations" class="footer__sitemap-link">Industrial Collaborations</a> -->
					<a href="<?php echo $host_name_with_lang?>news" class="footer__sitemap-link">News</a>
				</div>
				<div class="footer__sitemap-grp">
					<a href="<?php echo $host_name_with_lang?>contact" class="footer__sitemap-link">Contact Us</a>
					<a href="https://www.facebook.com/VTCDAA/" class="footer__sitemap-link">VTCDAA</a>
					<a href="http://dilwl-radio708.vtc.edu.hk/" class="footer__sitemap-link">Channel 708</a>
					<a href="<?php echo $host_name_with_lang?>job-openings" class="footer__sitemap-link">Job Opening</a>
					<a href="<?php echo $host_name_with_lang?>friendly-links" class="footer__sitemap-link">Friendly links</a>
					<a href="http://www.vtc.edu.hk/admission/en/programme/s6/higher-diploma/#design" class="footer__sitemap-link">Admission</a>
					<a href="<?php echo $host_name_with_lang?>disclaimer" class="footer__sitemap-link">Disclaimer</a>
				</div>
				<div class="footer__sitemap-grp">
					<p><strong>Subscribe to our Newsletter</strong></p>
					<form id="subscribe-form" class="subscribe-form__form form-container">
						<div class="field-row">
							<div class="field field--1-1 field--mb-1-1">
								<div class="field__holder">
									<input type="text" name="subscription_name" placeholder="Name" class="form-check--empty">
								</div>
							</div>
						</div>
						<div class="field-row">
							<div class="field field--1-1 field--mb-1-1">
								<div class="field__holder">
									<input type="text" name="subscription_email" placeholder="Email" class="form-check--empty form-check--email">
								</div>
							</div>
						</div>
						<div class="field-row">
							<div class="field field--1-1 field--mb-1-1">
								<div class="field__holder">
									<div class="captcha">
										<div class="captcha__img">
											<img id="booking_captcha_s" src="<?php echo $host_name; ?>/include/securimage/securimage_show.php" alt="CAPTCHA Image" />
										</div>
										<div class="captcha__control">
											<div class="captcha__input">
												<input type="text" name="captcha" class="form-check--empty" />
											</div>
											<a href="#" class="captcha__btn" onClick="document.getElementById('booking_captcha_s').src = '<?php echo $host_name; ?>/include/securimage/securimage_show.php?' + Math.random(); return false">Reload Image</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="btn-row">
							<div class="err-msg-holder">
								<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
								<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
							</div>
							<div class="btn-row btn-row--al-hl">
								<a href="#" class="btn btn-send">
									<strong>Subscribe</strong>
								</a>
							</div>
						</div>
					</form>
					<div class="subscribe-form__success-msg" style="display: none;">
						<h3 class="desc-subtitle">Thank you!</h3>
						<p class="desc-l">You have successfully subscribed.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="footer__bottom-items">
				<div class="footer__bottom-item">&copy; <?php echo date("Y") ?> Hong Kong Design Institute. All rights reserved. </div>
				<!--span class="footer__bottom-sp">|</span-->
			</div>
		</div>
	</div>
</footer>
<!-- END Footer -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-116403822-1');
</script>
 -->