<div class="err-msg-holder">
	<p class="err-msg err-msg--empty">This field is required.</p>
	<p class="err-msg err-msg--tel">Please enter a valid mobile phone number </p>
	<p class="err-msg err-msg--email">E-mail is required field.</p>
	<p class="err-msg err-msg--email-format">Format of your email is invalid</p>
	<p class="err-msg err-msg--phone">Phone is required field.</p>
	<p class="err-msg err-msg--phone-format">Format of your phone number is invalid.</p>
	<p class="err-msg err-msg--tnc">You must accept the Terms and Conditions.</p>
	<p class="err-msg err-msg--captcha">Incorrect Captcha.</p>
	<p class="err-msg err-msg--general">There is an unknown error. Please try again later.</p>
</div>