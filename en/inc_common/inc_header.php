<!-- Code for utm -->
<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "inc_common/get_utm.php";
?>

<!-- Header -->
<header>
	<div class="header__inner">
		<a href="<?php echo $host_name_with_lang?>" class="header-logo">
			<img src="<?php echo $img_url ?>common/logo-main-purple.svg" alt="underground-sign" />
		</a>
		<div class="header__btns">
			<a href="#main-menu" class="header__btn btn-menu">
				<span class="btn-menu__lines">
					<span class="btn-menu__line"></span>
					<span class="btn-menu__line"></span>
					<span class="btn-menu__line"></span>
				</span>
				<span class="btn-menu__txt">
					<span class="btn-menu__txt--open">Menu</span>
					<span class="btn-menu__txt--close">Close</span>
				</span>
			</a>
			<a href="#" class="header__btn header__btn--search"></a>
			<a href="#" class="header__btn header__btn--lang tc" data-lang="tc">繁</a>
			<a href="#" class="header__btn header__btn--lang sc" data-lang="sc">簡</a>
		</div>

		<div href="#" class="header__deco-line"></div>
	</div>
</header>
<nav id="main-menu" class="mob-nav">
	<div class="mob-nav__wrapper">
		<div class="mob-nav__main-nav">
			<div class="mob-nav__row">
				<div class="menu-search">
					<p>Search</p>
					<form class="search-field" id="form-search" action="<?php echo $host_name_with_lang; ?>search/" method="POST">
						<input class="field" type="search" name="search_text" placeholder="Enter Keywords" /><input class="submit-btn btn-submit" type="button" />
					</form>
				</div>
				<div class="mob-nav__col">
					<a href="<?php echo $host_name_with_lang?>about" class="mob-nav__link">About Us</a>

					<a href="<?php echo $host_name_with_lang?>programmes" class="mob-nav__link">Design Programmes</a>

					<a href="<?php echo $host_name_with_lang?>knowledge-centre" class="mob-nav__link">Knowledge Centres</a>

					<a href="<?php echo $host_name_with_lang?>hkdi_gallery" class="mob-nav__link">HKDI Gallery</a>
					<a href="<?php echo $host_name_with_lang?>programmes/award.php?id=7" class="mob-nav__link">Student Awards</a>

					<a href="<?php echo $host_name_with_lang?>global-learning" class="mob-nav__link">Global Learning</a>
					<a href="<?php echo $host_name_with_lang?>news" class="mob-nav__link">News</a>
					<a href="<?php echo $host_name_with_lang?>peec" class="mob-nav__link">Continuing Education</a>
					<a href="<?php echo $host_name_with_lang?>industrial" class="mob-nav__link">Industry Collaboration</a>
				</div>
				<div class="mob-nav__col">
					<a href="#" class="mob-nav__link back-btn">&lt; Back</a>
					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>about" class="mob-nav__link">About HKDI</a>

						<a href="<?php echo $host_name_with_lang?>about/#tabs-about-campus" class="mob-nav__link">Our campus</a>

						<a href="<?php echo $host_name_with_lang?>about/#tabs-publication" class="mob-nav__link">Our publication</a>

						<a href="<?php echo $host_name_with_lang?>about/#tabs-experience" class="mob-nav__link">Experience</a>
					</div>
					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>programmes/#tabs-hd" class="mob-nav__link">Higher Diploma</a>

						<a href="<?php echo $host_name_with_lang?>programmes/#tabs-dg" class="mob-nav__link">Bachelor’s Degree</a>

						<a href="<?php echo $host_name_with_lang?>programmes/#tabs-md" class="mob-nav__link">Master Degree</a>

						<!--<a href="<?php echo $host_name_with_lang?>continuing-education/" class="mob-nav__link">Continuing Education</a>-->
						<a target="_blank" href="http://www.hkdi.edu.hk/peec/" class="mob-nav__link">Continuing Education</a>
					</div>
					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>knowledge-centre/ccd.php" class="mob-nav__link">Centre for Communication Design</a>
						<a href="<?php echo $host_name_with_lang?>knowledge-centre/cdss.php" class="mob-nav__link">Centre of Design Services and Solutions</a>
						<a href="<?php echo $host_name_with_lang?>knowledge-centre/cimt.php" class="mob-nav__link">Centre of Innovative Material and Technology</a>

						<a href="<?php echo $host_name_with_lang?>knowledge-centre/desis_lab.php" class="mob-nav__link">DESIS Lab</a>

						<a href="<?php echo $host_name_with_lang?>knowledge-centre/fashion_archive.php" class="mob-nav__link">Fashion Archive</a>

						<a href="<?php echo $host_name_with_lang?>knowledge-centre/media_lab.php" class="mob-nav__link">Media Lab</a>


					</div>
					<div class="sub-menu">
					</div>
					<div class="sub-menu">
					</div>

					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>global-learning#international-student-exchange-programme" class="mob-nav__link">Students Exchange Programme</a>
						<a href="<?php echo $host_name_with_lang?>global-learning#international-academic-partners" class="mob-nav__link">International Academic Partners</a>

						<a href="<?php echo $host_name_with_lang?>global-learning#scholarship" class="mob-nav__link">Scholarship and Donation</a>

						<a href="<?php echo $host_name_with_lang?>global-learning#master-lecture-series" class="mob-nav__link">Master Lecture Series</a>
					</div>
					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>news" class="mob-nav__link">Events, News and Awards</a>

						<a href="<?php echo $host_name_with_lang?>news/publication.php" class="mob-nav__link">SIGNED magazine</a>
					</div>
					<div class="sub-menu">
						<!-- <a href="<?php echo $host_name_with_lang?>peec/program_list.php" class="mob-nav__link">Program List</a> -->

						<!-- <a href="<?php echo $host_name_with_lang?>peec/peecjobs.php" class="mob-nav__link">PeecJOBS</a> -->
					</div>
					<div class="sub-menu">
						<a href="<?php echo $host_name_with_lang?>industrial#industrial-attachment" class="mob-nav__link">Industrial Attachment</a>
						<a href="<?php echo $host_name_with_lang?>industrial#collaborative-projects" class="mob-nav__link">Collaborative Projects</a>
						<a href="<?php echo $ge_link; ?>" class="mob-nav__link" target="_blank">Graduate Employment </a>
						<a href="<?php echo $host_name_with_lang?>industrial#hkdi-star-graduate-scheme" class="mob-nav__link">HKDI Star Graduate Scheme</a>
					</div>
				</div>
			</div>
		</div>
		<div class="mob-nav__bottom">
			<div class="mob-nav__subscription">
			</div>
			<div class="mob-nav__share">
				<div class="btn-share__holder">
					<a href="http://www.facebook.com/HKDI.HongKongDesignInstitute" class="btn-share btn-share--fb" target="_blank"></a>
					<a href="https://instagram.com/hkdi_hongkongdesigninstitute" class="btn-share btn-share--ig" target="_blank"></a>
					<a href="http://www.youtube.com/user/hkdichannel" class="btn-share btn-share--youtube" target="_blank"></a>
					<a href="https://www.linkedin.com/school/hong-kong-design-institute/?originalSubdomain=hk" class="btn-share btn-share--ln" target="_blank"></a>
				</div>
			</div>
			<div class="mob-nav__quick-links">
				<a href="<?php echo $host_name_with_lang?>contact" class="mob-nav__quick-link">Contact Us</a>
				<a href="https://www.facebook.com/VTCDAA/" class="mob-nav__quick-link">VTCDAA</a>
				<a href="http://dilwl-radio708.vtc.edu.hk/" class="mob-nav__quick-link">Channel 708</a>
				<a href="<?php echo $host_name_with_lang?>job-openings" class="mob-nav__quick-link">Job Opening</a>
				<a href="<?php echo $host_name_with_lang?>friendly-links" class="mob-nav__quick-link">Friendly links</a>
				<a href="http://www.vtc.edu.hk/admission/en/programme/s6/higher-diploma/#design" class="mob-nav__quick-link">Admission</a>
				<a href="<?php echo $host_name_with_lang?>disclaimer" class="mob-nav__quick-link">Disclaimer</a>
			</div>
		</div>
	</div>
</nav>
<!-- END Header -->
