<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'News';
$section = 'news';
$subsection = 'index';
$sub_nav = 'index';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$news_id = @$_REQUEST['news_id'] ?? '';
$news_type = @$_REQUEST['news_type'] ?? '';
$news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_lv1 = ? AND news_id = ? AND news_from_date <= ? AND news_to_date >= ?', " news_date DESC", array(STATUS_ENABLE,'4',$news_id,$today,$today));

$prev_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_lv1 = ? AND news_id <> ? AND news_from_date <= ? AND news_to_date >= ? AND news_date <= ?', " news_seq", array(STATUS_ENABLE,'4',$news_id,$today,$today,$news['news_date']));
$next_news = DM::findOne($DB_STATUS.'news', ' news_status = ? AND news_lv1 = ? AND news_id <> ? AND news_from_date <= ? AND news_to_date >= ? AND news_date >= ?', " news_seq DESC", array(STATUS_ENABLE,'4',$news_id,$today,$today,$news['news_date']));

if ($news_id == '' || $news === NULL)
{
	header("location: index.php");
	exit();
}
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<section class="article-detail">
				<div class="content-wrapper">
					<div class="article-detail__control">
						<a href="index.php" class="article-detail__btn-back">&lt;&nbsp;Back</a>
					</div>
					<div class="article-detail__detail-row">
						<div class="article-detail__detail">
							<div class="article-detail__head">
								<h2 class="article-detail__title">
									<strong><?php echo $news['news_name_'.$lang]; ?></strong>
								</h2>
								<div class="article-detail__info-items">
									<div class="article-detail__info-item"><?php echo date('d.m.Y', strtotime($news['news_date'])); ?></div>
									<div class="article-detail__info-item">Continuing Education</div>
									<div class="article-detail__info-item">LATEST NEWS</div>
								</div>
							</div>
							<div class="txt-editor">
								<!--
								<h4>
									<strong>
										<span class="txt-highlight">The Centre of Innovative Material and Technology (CIMT) established by Hong Kong Design Institute (HKDI) was officially
											opened on 7 April, with the participation of several hundreds of industry partners, design academics and professionals.</span>
									</strong>
								</h4>
								<p>
									On the same day, a Master Talk by renowned DJ, stand-up comedian and singer-songwriter Jan Lamb, and a Street Furniture workshop
									by street art organisation Start from Zero were organised and well received.
									<br> Housing the latest innovative materials and design technologies sourced globally, CIMT has been designed and established
									to provide an interactive platform for the materials and design industries by facilitating the exchange of latest
									materials knowledge and inspiring new applications of materials. In celebration of the Grand Opening of CIMT, and
									more importantly, the 35th Anniversary of the VTC, a series of programmes are being rolled out for students and
									the industry including thematic seminars and workshops, and a themed exhibition featuring the latest technology
									and materials applications.
								</p>
								-->
								<?php echo $news['news_detail_'.$lang]; ?>
							</div>
						</div>
						<div class="article-detail__gallery">
							<!--
							<div class="article-detail__gallery-item">
								<img src="<?php echo $img_url?>news/img-news-detail-1.jpg" alt="" />
							</div>
							<div class="article-detail__gallery-item">
								<img src="<?php echo $img_url?>news/img-news-detail-2.jpg" alt="" />
							</div>
							-->
							<?php $img_array = array('news_image1', 'news_image2', 'news_image3', 'news_image4', 'news_image5'); ?>
							<?php foreach ($img_array AS $key => $val) { ?>
							<?php 	if ($news[$val] == '' || !file_exists($news_path.$news['news_id']."/".$news[$val])) { continue; } ?>
							<div class="article-detail__gallery-item">
								<img src="<?php echo $news_url.$news['news_id']."/".$news[$val]; ?>" alt="" />
							</div>
							<?php } ?>
						</div>
					</div>
					<hr>
					<div class="article-detail__control">
						<?php if ($prev_news !== NULL) { ?>
						<a href="news-detail.php?news_id=<?php echo $prev_news['news_id']; ?>&news_type=<?php echo $news_type; ?>" class="article-detail__btn-prev">
							<div class="article-detail__btn-txt">
								<div class="article-detail__btn-label">Previous Article</div>
								<h4 class="article-detail__btn-title"><?php echo $prev_news['news_name_'.$lang]; ?></h4>
							</div>
						</a>
						<?php } ?>
						<?php if ($next_news !== NULL) { ?>
						<a href="news-detail.php?news_id=<?php echo $next_news['news_id']; ?>&news_type=<?php echo $news_type; ?>" class="article-detail__btn-next">
							<div class="article-detail__btn-txt">
								<div class="article-detail__btn-label">Next Article</div>
								<h4 class="article-detail__btn-title"><?php echo $next_news['news_name_'.$lang]; ?></h4>
							</div>
						</a>
						<?php } ?>
					</div>
				</div>
			</section>
			<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>