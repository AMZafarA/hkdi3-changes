<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
include $inc_root_path ."include/classes/DataMapper.php";
$page_title = 'Continuing Education';
$section = 'continuing-education';
$subsection = 'course';
$sub_nav = 'course';

DM::setup($db_host, $db_name, $db_username, $db_pwd);

$product_id = @$_REQUEST['product_id'] ?? '';
$preview = @$_REQUEST["preview"] ?? false;
if($preview) {
	$product = DM::findOne($DB_STATUS.'product', ' product_type = ? AND product_id = ? ', '', array('3', $product_id));
}
else {
	$product = DM::findOne($DB_STATUS.'product', 'product_status = ? AND product_type = ? AND product_id = ? ', '', array(STATUS_ENABLE, '3', $product_id));
}
$product_sections = DM::findAll($DB_STATUS.'product_section', 'product_section_status = ? AND product_section_pid = ?', 'product_section_sequence', array(STATUS_ENABLE, $product_id));

if ($product_id == '' || $product === NULL)
{
	header("location: index.php");
	exit();
}
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
	</head>

	<body class="page-<?php echo $section ?> inner-pageinner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title">
							<h2>
								<strong><?php echo $product['product_name_'.$lang]; ?></strong>
							</h2>
						</div>
					</div>
					<!--
					<div class="page-head__tag-holder">
						<div class="page-head__logo">
							<span class="page-head__logo-label">Co-organiser</span>
							<img class="page-head__logo-img" src="<?php echo $inc_root_path ?>images/programme/course/logo01.jpg" />
						</div>
						<div class="page-head__logo">
							<span class="page-head__logo-label">Sponsor</span>
							<img class="page-head__logo-img" src="<?php echo $inc_root_path ?>images/programme/course/logo02.jpg" />
						</div>
					</div>
					-->
				</div>
			</div>
			<div class="article-detail ani">
				<div class="content-wrapper">
					<div class="txt-editor ani">
						<p><?php echo $product['product_detail_'.$lang]; ?></p>
					</div>
				</div>
			</div>
			<?php if ($product['product_banner'] != '' && file_exists($product_path.$product['product_id'].'/'.$product['product_banner'])) { ?>
			<div class="float-gallery ani">
				<div class="float-gallery__wrapper">
					<div class="float-gallery__banner">
						<img src="<?php echo $product_url.$product['product_id'].'/'.$product['product_banner']; ?>" />
					</div>
				</div>
			</div>
			<?php } ?>
			
			<?php foreach ($product_sections AS $product_section) { ?>
			<div class="sec-basic" style="background-color: <?php echo $product_section['product_section_background_color']?>;">
				<div class="content-wrapper">
					<?php 	echo $product_section['product_section_content_'.$lang]; ?>
				</div>
			</div>
			<?php } ?>		
			
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>