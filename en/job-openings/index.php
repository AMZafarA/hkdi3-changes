<?php
$inc_root_path = '../../';
$inc_lang_path = substr($inc_root_path, 0, -3);
include $inc_lang_path . "lang.php";
include $inc_root_path . "include/config.php";
$page_title = 'Contact';
$section = 'contact';
$subsection = '';
$sub_nav = '';

$breadcrumb_arr['Job Openings'] ='';
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_name[$lang] ?>" lang="<?php echo $lang_name[$lang] ?>">

	<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116403822-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-116403822-1'); </script>
		<?php include $inc_root_path . "inc_meta.php"; ?>
		<link rel="stylesheet" href="<?php echo $host_name?>css/reuse.css" type="text/css" />
	</head>

	<body class="page-<?php echo $section ?> inner-page" data-section="<?php echo $section ?>" data-subsection="<?php echo $subsection ?>">
		<?php include $inc_lang_path . "inc_common/inc_common_top.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_header.php"; ?>
		<h1 class="access"><?php echo $page_title?></h1>
		<main>
			<?php include $inc_lang_path . "inc_common/inc_breadcrumb.php"; ?>
			<div class="page-head">
				<div class="page-head__wrapper">
					<div class="page-head__title-holder">
						<div class="page-head__title page-head__title--big">
							<h2>
								<strong>Job</strong> Openings
							</h2>
						</div>
					</div>
				</div>
			</div>
			<section class="general">
				<p>There are various teaching, administrative, technical and clerical grades of career opportunities at the HKDI and other VTC member institutions.</p>
				<p>Please <a href="http://www.vtc.edu.hk/html/en/career.html" target="_blank">click here</a> to view the details.</p>
				
			</section>
		</main>
		<?php include $inc_lang_path . "inc_common/inc_footer.php"; ?>
		<?php include $inc_lang_path . "inc_common/inc_common_bottom.php"; ?>
	</body>

	</html>