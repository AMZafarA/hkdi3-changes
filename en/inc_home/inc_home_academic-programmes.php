<section class="section academic-programmes" data-tooltip-name="Design Programmes">
	<div class="fullpage__content">
		<div class="fullpage__content-holder">
			<div class="academic-programmes__holder">
				<h2 class="academic-programmes__title title-lv1">Design Programmes</h2>
				<div class="tabs tabs--sync-height">
					<div class="tabs__btns">
						<a href="#tabs--higher-diploma" class="tabs__btn is-active">Higher Diploma</a>
						<a href="#tabs--degree" class="tabs__btn">Degree</a>
						<a href="#tabs--master-degree" class="tabs__btn">Master Degree</a>
						<a target="_blank" href="http://www.hkdi.edu.hk/peec/" class="tabs__btn is-link">Continuing Education</a>
					</div>
					<div class="tabs__contents">
						<div id="tabs--higher-diploma" class="tabs__content is-active">
							<div class="tetris-tabs">
								<div class="tetris-tabs__btns swiper-container">
									<div class="tetris-tabs__btns-wrapper swiper-wrapper">
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-aip" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=1" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">Architecture, <br>Interior & <br>Product Design</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div>
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-cdm" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=5" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">Communication <br>Design</span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
											</a>
										</div>
										<!--div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-dfs" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=2" class="tetris-tabs__btn">
												<span class="tetris-tabs__programmes-name">Design <br>Foundation<br>Studies</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div-->
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-ddm" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=28" class="tetris-tabs__btn swiper-slide">
												<span class="tetris-tabs__programmes-name">Digital<br> Media</span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
											</a>
										</div>
										<div class="tetris-tabs__btn-slide swiper-slide">
											<a id="btn-fid" href="<?php echo $inc_lang_path?>programmes/programme.php?programmes_id=18" class="tetris-tabs__btn swiper-slide">
												<span class="tetris-tabs__programmes-name">Fashion & <br>Image Design</span>
												<span class="tetris-tabs__block tetris-tabs__block-long"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
												<span class="tetris-tabs__block tetris-tabs__block-short"></span>
											</a>
										</div>
									</div>
								</div>
								<div class="tetris-tabs__contents">
									<div id="sec-aip" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">Architecture, Interior &amp; Product Design</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($AIP_list) / 2)); $i++) { ?>
												<?php 	$programme = $AIP_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($AIP_list)); $i++) { ?>
												<?php 	$programme = $AIP_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
											</ul>
											</div>
										</div>
									</div>
									<div id="sec-cdm" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">Communication Design &
													<span class="display--inline-block">Digital Media</span>
												</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($CDM_list) / 2)); $i++) { ?>
												<?php 	$programme = $CDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($CDM_list)); $i++) { ?>
												<?php 	$programme = $CDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
											</ul>
											</div>
										</div>
									</div>
									<?php /*
									<div id="sec-dfs" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">Design Foundation Studies</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($DFS_list) / 2)); $i++) { ?>
												<?php 	$programme = $DFS_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($DFS_list)); $i++) { ?>
												<?php 	$programme = $DFS_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>*/
									 ?>
									<div id="sec-ddm" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">Digital Media</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($DDM_list) / 2)); $i++) { ?>
												<?php 	$programme = $DDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($DDM_list)); $i++) { ?>
												<?php 	$programme = $DDM_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
									<div id="sec-fid" class="tetris-tabs__content">
										<div class="ap-content__detail">
											<!--div class="ap-content__title-holder">
												<h3 class="ap-content__title">Fashion & Image Design</h3>
											</div-->
											<div class="ap-content__list-holder">
												<ul class="ap-content__list-col">
												<?php for ($i = 0; $i < ceil((sizeof($FID_list) / 2)); $i++) { ?>
												<?php 	$programme = $FID_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>

												<ul class="ap-content__list-col">
												<?php for ($i; $i < (sizeof($FID_list)); $i++) { ?>
												<?php 	$programme = $FID_list[$i]; ?>
													<li>
														<a href="programmes/programme.php?programmes_id=<?php echo $programme['programmes_id']?>"><?php echo $programme['programmes_name_'.$lang]?></a>
													</li>
												<?php } ?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tabs--degree" class="tabs__content">
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">HKDSE</div>
									</div>
									<a href="programmes/#tabs-hd" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
											Year 1 & 2
											<br>Higher Diploma
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
											Year 3 Degree
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
											Year 4 Master
										</div>
									</a>
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">Bachelor's Degree</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI offers many degree programmes through partnerships with various renowned UK universities. By first completing a two-year Higher Diploma and then a final year degree programme, students can earn a Bachelor’s degree (Hons) in just three years.</p>
									</div>
								</a>
							</div>
						</div>
						<div id="tabs--master-degree" class="tabs__content">
							<div class="ap-content">
								<div class="ap-content__flow">
									<div class="ap-content__flow-head">
										<div class="ap-content__flow-item-txt">HKDSE</div>
									</div>
									<a href="programmes/#tabs-hd" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
											Year 1 & 2
											<br>Higher Diploma
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/bachelor-degree.php" class="ap-content__flow-item">
										<div class="ap-content__flow-item-txt">
											Year 3 Degree
										</div>
									</a>
									<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__flow-item is-active">
										<div class="ap-content__flow-item-txt">
											Year 4 Master
										</div>
									</a>
								</div>
								<a href="<?php echo $host_name_with_lang?>programmes/master-degree.php" class="ap-content__detail">
									<div class="ap-content__title-holder">
										<h3 class="ap-content__title">Master Degree</h3>
									</div>
									<div class="ap-content__desc-holder">
										<p class="ap-content__desc">HKDI offers a Master of Arts in Design Management in partnership with the UK’s renowned Birmingham City University. By first completing a two-year Higher Diploma and then a final year Bachelor’s degree (Hons) programme, students can earn a Master of Arts degree in just four (full-time) or five (part-time) years.</p>
									</div>
</a>
							</div>
						</div>
						<div id="tabs--continuing-education" class="tabs__content">
							<h3 class="ap-content__title">Continuing Education</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
