<section class="section knowledge-centres" data-tooltip-name="Knowledge Centres">
	<div class="fullpage__content">
		<div class="fullpage__content-holder">
			<div class="knowledge-centres__holder">
				<div class="knowledge-centres__info">
					<div class="knowledge-centres__info-holder">
						<div class="knowledge-centres__txt">
							<h2 class="knowledge-centres__title">Knowledge Centres</h2>
							<p class="knowledge-centres__desc">
								Through its Knowledge Centres, HKDI offers students access to interdisciplinary platforms and enables them to engage in projects with industry partners.
							</p>
						</div>
						<div class="knowledge-centres__links">
							<a data-kc-id="ccd" class="knowledge-centres__link" href="knowledge-centre/ccd.php">Centre for Communication Design</a>
							<a data-kc-id="cdss" class="knowledge-centres__link" href="knowledge-centre/cdss.php">Centre of Design Services and Solutions</a>
							<a data-kc-id="cimt" class="knowledge-centres__link" href="knowledge-centre/cimt.php">Centre of Innovative Material and Technology</a>
							<a data-kc-id="dl" class="knowledge-centres__link" href="knowledge-centre/desis_lab.php">DESIS Lab</a>
							<a data-kc-id="fa" class="knowledge-centres__link" href="knowledge-centre/fashion_archive.php">Fashion Archive</a>
							<a data-kc-id="ml" class="knowledge-centres__link" href="knowledge-centre/media_lab.php">Media Lab</a>
						</div>
					</div>
					<a class="knowledge-centres__btn" href="knowledge-centre/index.php"></a>
				</div>
				<div class="knowledge-centres__thumbs">
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="ccd" class="knowledge-centres__thumb" href="knowledge-centre/ccd.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-ccd.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">CCD</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="cdss" class="knowledge-centres__thumb" href="knowledge-centre/cdss.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-cdss.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">CDSS</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="cimt" class="knowledge-centres__thumb" href="knowledge-centre/cimt.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-cimt.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">CIMT</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="dl" class="knowledge-centres__thumb" href="knowledge-centre/desis_lab.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-desis-lab.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">DESIS Lab</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="fa" class="knowledge-centres__thumb" href="knowledge-centre/fashion_archive.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-fashion-archive.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">Fashion Archive</span>
						</a>
					</div>
					<div class="knowledge-centres__thumbs-item">
						<a data-kc-id="ml" class="knowledge-centres__thumb" href="knowledge-centre/media_lab.php">
							<img class="knowledge-centres__bg" src="<?php echo $img_url ?>home/img-knowledge-centres-media-lab.jpg" alt="" />
							<span  class="knowledge-centres__thumb-txt">Media Lab</span>
						</a>
					</div>

				</div>

			</div>
		</div>
	</div>
</section>
