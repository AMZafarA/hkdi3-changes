<section class="section hkdi-gallery" data-tooltip-name="HKDI Gallery">
    <div class="fullpage__content">
        <div class="fullpage__content-holder">
            <div class="hkdi-gallery__holder">
            <div class="hkdi-gallery__items">

		<?php foreach ($gallery_highlight_list AS $gallery) {

               /* switch ($gallery['product_id']) {
                    case 17:
                        $gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/head/';
                        break;
                    case 18:
                        $gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/cnsm/index.html';
                        break;
                    case 19:
                        $gallery_redirect_link = 'http://www.hkdi.edu.hk/hkdi_gallery/2017/reddot/';
                        break;
                }
                if($gallery){
                    http://www.hkdi.edu.hk/hkdi_gallery/2017/head/
                }*/

            ?>
                <div class="hkdi-gallery__item">
                        <!--<a href="gallery/gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="hkdi-gallery__thumb">-->
                            <a href="hkdi_gallery/gallery.php?product_id=<?php echo $gallery['product_id']; ?>" class="hkdi-gallery__thumb">
                            <div class="hkdi-gallery__thumb__inner">
                                <img src="<?php echo $product_url.$gallery['product_id'].'/'.$gallery['product_thumb_banner']; ?>" alt="" />
                                <div class="hkdi-gallery__thumb-info">
                                    <h3 class="hkdi-gallery__thumb-title"><?php echo $gallery['product_name_'.$lang]; ?></h3>
                                    <div class="hkdi-gallery__thumb-subinfo">
                                        <time class="hkdi-gallery__thumb-date">
                                            <?php echo date('d.m', strtotime($gallery['product_from_date'])); ?><strong><?php echo date('Y', strtotime($gallery['product_from_date'])); ?></strong>
                                        </time>
                                        <span class="hkdi-gallery__thumb-line"></span>
                                        <time class="hkdi-gallery__thumb-date">
                                            <?php echo date('d.m', strtotime($gallery['product_to_date'])); ?><strong><?php echo date('Y', strtotime($gallery['product_to_date'])); ?></strong>
                                        </time>
                                    </div>
                                </div>
                            </div>
                        </a>
                </div>

                <?php } ?>



                <div class="hkdi-gallery__item hkdi-gallery__item-info">
                    <div class="hkdi-gallery__info">
                        <h2 class="hkdi-gallery__title">HKDI Gallery</h2>
                        <p class="hkdi-gallery__desc">Affiliated to the Hong Kong Design Institute (HKDI), HKDI Gallery is a dynamic exhibition space with unique vision. We engaged with parties at home and abroad, including internationally acclaimed museums, designers and curators. With contemporary design as the cornerstone, we present a series of top-notch exhibitions every year ranging on graphic design, architecture, fashion, product design, multimedia and so on. </p>
                        <!--<a class="btn-arrow btn-arrow--animated" href="gallery/"></a>-->
                        <a class="btn-arrow btn-arrow--animated" href="http://www.hkdi.edu.hk/hkdi_gallery/index.php"></a>

                    </div>
                </div>
            </div>
                <!--
                    <div class="hkdi-gallery__thumbs">
                        <div class="hkdi-gallery__thumbs-row">
                        </div>
                        <div class="hkdi-gallery__thumbs-row">
                        </div>
                    </div>
                    <div class="hkdi-gallery__info">
                        <h2 class="hkdi-gallery__title">HKDI Gallery</h2>
                        <p class="hkdi-gallery__desc">Affiliated to the Hong Kong Design Institute (HKDI), HKDI Gallery is a dynamic exhibition space with unique vision. We engaged with parties at home and abroad, including internationally acclaimed museums, designers and curators. With contemporary design as the cornerstone, we present a series of top-notch exhibitions every year ranging on graphic design, architecture, fashion, product design, multimedia and so on.</p>
                        <a class="btn-arrow btn-arrow--animated" href="#"></a>
                    </div>
                -->
            </div>
        </div>
    </div>
</section>
