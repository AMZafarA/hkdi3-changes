<?php
$inc_root_path = '';
require_once "include/config.php";
require_once "include/PHPMailer/PHPMailerAutoload.php";
include_once 'include/securimage/securimage.php';
include_once "include/classes/DataMapper.php";

DM::setup($db_host, $db_name, $db_username, $db_pwd);

foreach ($_POST as $key => $value){
	$$key = $value;
}
$securimage = new Securimage();
if ($securimage->check($captcha) == false) {
	$return_json['status'] = 2;
	echo json_encode($return_json);
	exit();
}
	$waitlist = false;

	if($contact_rsvp_id){
		$rsvp_id = $contact_rsvp_id;
	}

	$rsvp = DM::load('rsvp',$rsvp_id);
	$rsvp_no_of_ticket = $rsvp['rsvp_no_of_ticket'];

	if(!$rsvp_id || $rsvp['rsvp_status'] !='1'){
		$return_json['status'] = 2;
		echo json_encode($return_json);
		exit();
	}
	if($rsvp_no_of_ticket){
		$rsvp_reg_count = DM::execute('SELECT SUM(rsvp_reg_no_ticket) as total_reg FROM rsvp_reg where rsvp_id='.$rsvp_id);

		if($rsvp_reg_count[0]['total_reg'] >= $rsvp_no_of_ticket){
			$waitlist = true;
		}

	}

	$timestamp = getCurrentTimestamp();
	$values['rsvp_id'] = $rsvp_id;
	$values['rsvp_reg_lang'] = $lang;
	$values['rsvp_reg_title'] = $contact_title;
	$values['rsvp_reg_first_name'] = $contact_first_name;
	$values['rsvp_reg_last_name'] = $contact_last_name;
	$values['rsvp_reg_company'] = $contact_company;
	$values['rsvp_reg_message'] = $contact_msg;
	$values['rsvp_reg_phone'] = $contact_tel;
	$values['rsvp_reg_email'] = $contact_email;
	$values['rsvp_reg_job_title'] = $contact_position;
	$values['rsvp_reg_address'] = $contact_address;
	$values['rsvp_reg_no_ticket'] = $contact_no_ticket;
	$values['rsvp_reg_waitlist'] = $waitlist;
	$values['rsvp_reg_date'] = $timestamp;
	
	$values['rsvp_reg_admission_type'] = $admission;
	$values['rsvp_reg_representative_title'] = $contact_rf_title;
	$values['rsvp_reg_representative_first_name'] = $contact_rf_first_name;
	$values['rsvp_reg_representative_last_name'] = $contact_rf_last_name;
	$values['rsvp_reg_representative_job_title'] = $contact_rf_position;
	$values['rsvp_reg_representative_email'] = $contact_rf_email;

	DM::insert('rsvp_reg', $values);

	$mail = new PHPMailer;
	mb_internal_encoding('UTF-8');
	
	$mail->IsSMTP();
	
	$mail->Host = $SYSTEM_SMTP_SERVER;
	$mail->SMTPAuth = $smtp_require_auth;
	$mail->Username  = $smtp_username;
	$mail->Password   = $smtp_password;
	$mail->SMTPSecure = $smtp_security;
	$mail->Port = $smtp_port;
	$mail->IsHTML(true);
	
	$mail->SetFrom("dilwl-emkt@vtc.edu.hk", "HKDI");
	$subject = "RSVP/ Enquiry";
	$mail->CharSet = "UTF-8";

	if($waitlist){
		$content = 'waitlist';
	}else{
		$content = 'ok';
	}
	
	/*$content = '<div style="font-size:17px;line-height:24px;">';
	$content .= "Title: ".$contact_title."<br />";
	$content .= "Name: ".$contact_name."<br />";
	$content .= "Company: ".$contact_company."<br />";
	$content .= "Phone: ".$contact_tel."<br />";
	$content .= "Email: ".$contact_email."<br />";
	$content .= "Message: ".$contact_msg."<br /><br />";
	$content .= "Additional Ticket Title: ".$contact_title2."<br />";
	$content .= "Additional Ticket Phone: ".$contact_tel2."<br />";
	$content .= "</div>";*/
	
	$mail->Subject = mb_encode_mimeheader($subject, "UTF-8");
	$mail->MsgHTML($content);
	
	$mail->AddAddress("vincent.chau@firmstudio.com",'');
	$mail->Send();	
	$mail->ClearAddresses();
	
	if($waitlist){
		$return_json['status'] = '3';
	}else{
		$return_json['status'] = '1';
	}
	echo json_encode($return_json);

?>