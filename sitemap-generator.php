<?php

$inc_root_path = '';error_reporting(E_ALL);
require_once "include/config.php";
include_once "include/classes/DataMapper.php";
$conn = new mysqli($db_host, $db_username, $db_pwd, $db_name);


$directory = './';
$exclude_dir = array( '~en' , 'CMS' , 'css' , 'images' , 'include' , 'js' , 'old' , 'pdf' , 'phpMyAdmin-5.1.1' , 'text_mask' , 'uploaded_files' , 'videos', 'inc_common' , 'inc_home' );
$exclude_files = array( 'sitemap-generator.php' , 'sitemap-tables.json' , 'sql' , 'bkp' , 'htaccess', 'bk2' , 'news-detail.php' , 'publication.php', 'publication-detail.php' , 'gallery.php' , 'exchange-programme-details.php' , 'outgoing-student-details.php' , 'award.php', 'programme.php' );
$xml = "";

$filter = function ( $file , $key , $iterator ) use ( $exclude_dir , $exclude_files ) {
	if ( $iterator->hasChildren() && !in_array($file->getFilename(), $exclude_dir)) {
		return true;
	}

	foreach( $exclude_files as $ef ) {
		if( str_contains( strtolower( $file->getFilename() ) , strtolower( $ef ) ) ) {
			return false;
		}
	}

	return $file->isFile();
};

$innerIterator = new RecursiveDirectoryIterator( $directory, RecursiveDirectoryIterator::SKIP_DOTS );
$iterator = new RecursiveIteratorIterator( new RecursiveCallbackFilterIterator( $innerIterator , $filter ) );

function url() {
	return ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' ) . '://' . $_SERVER['SERVER_NAME'];
}

function renameFile($ff) {
	$ff = str_replace( array( "./" , "index.php/" , "index.php" ) , "" , $ff );
	$ff = str_replace( "//" , "/" , $ff );
	$ff = htmlentities( $ff );
	return $ff;
}

$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';


foreach ($iterator as $pathname => $fileInfo) {
	$xml .= '<url>';
	$xml .= '<loc>' . url() . '/' . renameFile( $pathname ) . '</loc>';
	$xml .= '<lastmod>' . date( DATE_ATOM , time() ) . '</lastmod>';
	$xml .= '<changefreq>daily</changefreq>';
	$xml .= '<priority>1.0</priority>';
	$xml .= '</url>';
}

$file = "./sitemap-tables.json";
$json = str_replace( "\t", "", str_replace( "\n", " ", file_get_contents( $file ) ) );
$meta = json_decode( $json );
$langs = ["en","tc","sc"];

foreach ($meta as $j) {
	$s = "SELECT " . $j->col_label . "_id FROM " . $j->p_table . " WHERE " . $j->col_label . "_publish_status = 'AL'";
	if( $j->conditions ) {
		foreach ( $j->conditions as $ck => $cv ) {
			$s .= " AND " . $j->col_label . "_" . $ck . " = " . "'$cv'";
		}
	}
	$result = $conn->query($s);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			foreach ($langs as $lang) {
				$xml .= '<url>';
				$xml .= '<loc>' . url() . "/" . $lang . "/" . $j->frontend_url . "?" . $j->parameter_identifier . "=" . $row[$j->col_label . "_id"] . '</loc>';
				$xml .= '<lastmod>' . date( DATE_ATOM , time() ) . '</lastmod>';
				$xml .= '<changefreq>daily</changefreq>';
				$xml .= '<priority>1.0</priority>';
				$xml .= '</url>';
			}
		}
	} 
}

$xml .= '</urlset>';

$dom = new DOMDocument;
$dom->preserveWhiteSpace = FALSE;
$dom->loadXML($xml);
$f = time() .  '_sitemap.xml';
$dom->save('./' . $f);

echo "<a target=\"_blank\" href=\"$f\">$f</a>";

?>