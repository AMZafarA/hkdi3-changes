<?php

$directory = './';
$exclude_dir = array( '~en' , 'CMS' , 'css' , 'images' , 'include' , 'js' , 'old' , 'pdf' , 'phpMyAdmin-5.1.1' , 'text_mask' , 'uploaded_files' , 'videos', 'inc_common' , 'inc_home' );
$exclude_files = array( 'sitemap-generator.php' , 'sitemap-tables.json' , 'sql' , 'bkp' , 'htaccess', 'bk2' , 'news-detail.php' , 'publication.php', 'publication-detail.php' , 'gallery.php' , 'exchange-programme-details.php' , 'outgoing-student-details.php' , 'award.php', 'programme.php' );
$xml = "";

$filter = function ( $file , $key , $iterator ) use ( $exclude_dir , $exclude_files ) {
	if ( $iterator->hasChildren() && !in_array($file->getFilename(), $exclude_dir)) {
		return true;
	}

	foreach( $exclude_files as $ef ) {
		if( str_contains( strtolower( $file->getFilename() ) , strtolower( $ef ) ) ) {
			return false;
		}
	}

	return $file->isFile();
};

$innerIterator = new RecursiveDirectoryIterator( $directory, RecursiveDirectoryIterator::SKIP_DOTS );
$iterator = new RecursiveIteratorIterator( new RecursiveCallbackFilterIterator( $innerIterator , $filter ) );

function url() {
	return ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' ) . '://' . $_SERVER['SERVER_NAME'];
}

function renameFile($ff) {
	$ff = str_replace( array( "./" , "index.php/" , "index.php" ) , "" , $ff );
	$ff = str_replace( "//" , "/" , $ff );
	$ff = htmlentities( $ff );
	return $ff;
}

foreach ($iterator as $pathname => $fileInfo) {
	echo renameFile( $pathname ) . '<br><br>';
}

?>