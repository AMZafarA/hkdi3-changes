﻿<?php
ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);   
    error_reporting(0);
//Language handle
$site_title['lang1'] = 'Hong Kong Design Institute';
$site_title['lang2'] = '香港知專設計學院';
$site_title['lang3'] = '香港知专设计学院';

$lang_name['lang1'] = "en";
$lang_name['lang2'] = "zh-hk";
$lang_name['lang3'] = "zh-cn";

$lang_name_short['lang1'] = "en";
$lang_name_short['lang2'] = "tc";
$lang_name_short['lang3'] = "sc";

$lang_path['lang1'] = $lang_name_short['lang1'] . "/";
$lang_path['lang2'] = $lang_name_short['lang2'] . "/";
$lang_path['lang3'] = $lang_name_short['lang3'] . "/";

$lang_path_en = str_replace_limit($lang_path[$lang], $lang_path['lang1'], $_SERVER['REQUEST_URI'], 1);
$lang_path_tc = str_replace_limit($lang_path[$lang], $lang_path['lang2'], $_SERVER['REQUEST_URI'], 1);
$lang_path_sc = str_replace_limit($lang_path[$lang], $lang_path['lang3'], $_SERVER['REQUEST_URI'], 1);

$root_path = "/home/hkdi3/domains/hkdi3.bcde.digital/public_html/"; // Official path
$host_name = "https://hkdi3.bcde.digital/"; // Official Site
//$root_path = "/home/hkdi_staging/html/"; // Staging path
//$host_name = "http://172.30.24.85/"; // Staging Site
//$root_path = "/home/hkdi_staging/html/Beta_Site/"; // Staging - Beta Path
//$host_name = "http://172.30.24.85/Beta_Site/"; // Staging - Beta Site

$host_name_with_lang = $host_name . $lang_path[$lang];

//Other Variable and Paths 
$img_url = $host_name . 'images/';
$page_subtitle = '';      //default no subtitle
$file_name = '.php'; //for php to html conversion
//$file_name = '.html'; //for php to html conversion

$db_host ="localhost";
$db_username="hkdi3_db1";
$db_name = "hkdi3_db1";
$db_pwd="H!k@091204";

define("STATUS_ENABLE", "1");
define("STATUS_DISABLE", "0");

//$DB_STATUS="p_"; // For Live (Approved) site
$DB_STATUS="p_"; // For Staging site

define('IN_PAGE', true);

//Global Page Content
$global_page_desc['lang1'] = "HKDI - Hong Kong Design Institute, a leading design education institution in Hong Kong under the VTC group committed to nurturing design talents.";
$global_page_desc['lang2'] = "香港知專設計學院(HKDI)致力提供優質教育，建構知識和發展專業，為創意工業培育優秀的設計人才。HKDI採取「思考與實踐」的教育方法，開辦與時並進的課程，並與業界保持緊密聯繫及合作。前身為香港專業教育學院(IVE)，HKDI具有多年的設計教育經驗，融合多個設計學系的優勢，提供不同領域的設計課程，包括建築、室內及產品設計、基礎設計、傳意及數碼媒體設計、以及時裝及形象設計。";
$global_page_desc['lang3'] = "香港知专设计学院(HKDI)致力提供优质教育，建构知识和发展专业，为创意工业培育优秀的设计人才。 HKDI采取「思考与实践」的教育方法，开办与时并进的课程，并与业界保持紧密联系及合作。前身为香港专业教育学院(IVE)，HKDI具有多年的设计教育经验，融合多个设计学系的优势，提供不同领域的设计课程，包括建筑、室内和产品设计、基础设计、传意及数码媒体设计、以及时装及形象设计。";

$global_page_keywords['lang1'] = "design courses for professionals,interior design courses,design management courses,fashion design courses,fashion modeling courses,設計文憑/證書,設計短期進修,HKDI,香港知專設計學院,Continuing Education in Design,Typography,Culture & design,Exhibition & Event Design Courses,Online marketing Courses,Fashion Photography Courses,Product design & management courses,Art & Design Thinking courses,Retail shop interior design courses";
$global_page_keywords['lang2'] = "design courses for professionals,interior design courses,design management courses,fashion design courses,fashion modeling courses,設計文憑/證書,設計短期進修,HKDI,香港知專設計學院,Continuing Education in Design,Typography,Culture & design,Exhibition & Event Design Courses,Online marketing Courses,Fashion Photography Courses,Product design & management courses,Art & Design Thinking courses,Retail shop interior design courses";
$global_page_keywords['lang3'] = "design courses for professionals,interior design courses,design management courses,fashion design courses,fashion modeling courses,設計文憑/證書,設計短期進修,HKDI,香港知專設計學院,Continuing Education in Design,Typography,Culture & design,Exhibition & Event Design Courses,Online marketing Courses,Fashion Photography Courses,Product design & management courses,Art & Design Thinking courses,Retail shop interior design courses";

//store the info for nav links to avoid edit twice for desktop and mobile version
$nav_links = array(
    array(
        "section" => "emphasis-story",
        "href" => "emphasis-story/index" . $file_name,
        "txt" => "Emphasis Story",
        "txt2" => "Emphasis Story",
        "txt3" => "Emphasis Story"
    ),
    array(
        "section" => "collections",
        "href" => "collections/index" . $file_name,
        "txt" => "Collections",
        "txt2" => "Collections",
        "txt3" => "Collections",
        "subsections" => array(
            array(
                "subsection" => "daily-luxe",
                "href" => "collections/daily-luxe" . $file_name,
                "txt" => "Daily Luxe",
                "txt2" => "Daily Luxe",
                "txt3" => "Daily Luxe"
            ),
            array(
                "subsection" => "dazzling-flower",
                "href" => "collections/dazzling-flower" . $file_name,
                "txt" => "Dazzling Flower",
                "txt2" => "Dazzling Flower",
                "txt3" => "Dazzling Flower"
            ),
            array(
                "subsection" => "Earplay",
                "href" => "collections/earplay" . $file_name,
                "txt" => "Earplay",
                "txt2" => "Earplay",
                "txt3" => "Earplay"
            ),
            array(
                "subsection" => "ausoicious",
                "href" => "collections/ausoicious" . $file_name,
                "txt" => "Ausoicious",
                "txt2" => "Ausoicious",
                "txt3" => "Ausoicious"
            ),
            array(
                "subsection" => "marco-bicego",
                "href" => "collections/marco-bicego" . $file_name,
                "txt" => "Marco Bicego",
                "txt2" => "Marco Bicego",
                "txt3" => "Marco Bicego"
            ),
            array(
                "subsection" => "brown-diamond",
                "href" => "collections/brown-diamond" . $file_name,
                "txt" => "Brown Diamond",
                "txt2" => "Brown Diamond",
                "txt3" => "Brown Diamond"
            ),
            array(
                "subsection" => "crossover",
                "href" => "collections/crossover" . $file_name,
                "txt" => "Crossover",
                "txt2" => "Crossover",
                "txt3" => "Crossover"
            ),
        )
    ),
    array(
        "section" => "customer-services",
        "href" => "customer-services/index" . $file_name,
        "txt" => "Customer Services",
        "txt2" => "Customer Services",
        "txt3" => "Customer Services"
    ),
    array(
        "section" => "contact-us",
        "href" => "contact-us/index" . $file_name,
        "txt" => "Contact Us",
        "txt2" => "Contact Us",
        "txt3" => "Contact Us"
    )
);


    $programmes_path = $root_path."uploaded_files/programmes/";
    $programmes_url = $host_name."uploaded_files/programmes/";
    
    $news_path = $root_path."uploaded_files/news/";
    $news_url = $host_name."uploaded_files/news/";

    $rsvp_path = $root_path."uploaded_files/rsvp/";
    $rsvp_url = $host_name."uploaded_files/rsvp/";
    
    $project_path = $root_path."uploaded_files/project/";
    $project_url = $host_name."uploaded_files/project/";
    
    $issue_path = $root_path."uploaded_files/issue/";
    $issue_url = $host_name."uploaded_files/issue/";
    
    $signed_path = $root_path."uploaded_files/signed/";
    $signed_url = $host_name."uploaded_files/signed/";

    $universities_path = $root_path."uploaded_files/universities/";
    $universities_url = $host_name."uploaded_files/universities/";

    $top_up_degree_path = $root_path."uploaded_files/top_up_degree/";
    $top_up_degree_url = $host_name."uploaded_files/top_up_degree/";

    $product_path = $root_path."uploaded_files/product/";
    $product_url = $host_name."uploaded_files/product/";

    $student_award_path = $root_path."uploaded_files/student_award/";
    $student_award_url = $host_name."uploaded_files/student_award/";

    $alumni_path = $root_path."uploaded_files/alumni/";
    $alumni_url = $host_name."uploaded_files/alumni/";
    
    $product_file_path = $root_path."uploaded_files/product_file/";
    $product_file_url = $host_name."uploaded_files/product_file/";
    
    $new_collaboration_file_path = $root_path."uploaded_files/collaboration_file/";
    $new_collaboration_file_url = $host_name."uploaded_files/collaboration_file/";

    $continuing_edcation_path = $root_path."uploaded_files/continuing_edcation/";
    $continuing_edcation_url = $host_name."uploaded_files/continuing_edcation/";

    $international_path = $root_path."uploaded_files/international/";
    $international_url = $host_name."uploaded_files/international/";

    $collaboration_path = $root_path."uploaded_files/collaboration/";
    $collaboration_url = $host_name."uploaded_files/collaboration/";

    $new_collaboration_path = $root_path."uploaded_files/collaboration/";
    $new_collaboration_url = $host_name."uploaded_files/collaboration/";

    $master_lecture_path = $root_path."uploaded_files/master_lecture/";
    $master_lecture_url = $host_name."uploaded_files/master_lecture/";

    $dept_path = $root_path."uploaded_files/dept/";
    $dept_url = $host_name."uploaded_files/dept/";

    $talent_path = $root_path."uploaded_files/talent/";
    $talent_url = $host_name."uploaded_files/talent/";

    $peec_path = $root_path."uploaded_files/peec/";
    $peec_url = $host_name."uploaded_files/peec/";

    $peec_section_path = $root_path."uploaded_files/peec_section/";
    $peec_section_url = $host_name."uploaded_files/peec_section/";

    $peec_section_file_path = $root_path."uploaded_files/peec_section_file/";
    $peec_section_file_url = $host_name."uploaded_files/peec_section_file/";

    $publications_programmes_path = $root_path."uploaded_files/publications_programmes/";
    $publications_programmes_url = $host_name."uploaded_files/publications_programmes/";

    $peec_menu_path = $root_path."uploaded_files/peec_menu/";
    $peec_menu_url = $host_name."uploaded_files/peec_menu/";

    $peec_menu_section_path = $root_path."uploaded_files/peec_menu_section/";
    $peec_menu_section_url = $host_name."uploaded_files/peec_menu_section/";

    $peec_menu_section_file_path = $root_path."uploaded_files/peec_menu_section_file/";
    $peec_menu_section_file_url = $host_name."uploaded_files/peec_menu_section_file/";

    $peec_programmes_path = $root_path."uploaded_files/peec_programmes/";
    $peec_programmes_url = $host_name."uploaded_files/peec_programmes/";

    $peec_programmes_section_path = $root_path."uploaded_files/peec_programmes_section/";
    $peec_programmes_section_url = $host_name."uploaded_files/peec_programmes_section/";

    $peec_programmes_section_file_path = $root_path."uploaded_files/peec_programmes_section_file/";
    $peec_programmes_section_file_url = $host_name."uploaded_files/peec_programmes_section_file/";

    $peec_programmes_categories_path = $root_path."uploaded_files/peec_programmes_categories/";
    $peec_programmes_categories_url = $host_name."uploaded_files/peec_programmes_categories/";


$today = date('Y-m-d');
$news_page_size = 8;
$news_types = array(
	array('value' => 'hkdi_news', 'lang1' => 'HKDI News', 'lang2' => '最新動態', 'lang3' => '最新动态'),
	array('value' => 'student_award', 'lang1' => 'Student Award', 'lang2' => '學生得獎作品', 'lang3' => '学生得奖作品'),
	array('value' => 'events', 'lang1' => 'Events', 'lang2' => '活動', 'lang3' => '活动'),
	array('value' => 'public_events', 'lang1' => 'Public Events', 'lang2' => '其他活動', 'lang3' => '其他活动')
); // sync with wwwroot/CMS/include/config.php

$news_types_arr['hkdi_news']['lang1'] = 'HKDI News';
$news_types_arr['hkdi_news']['lang2'] = '最新動態';
$news_types_arr['hkdi_news']['lang3'] = '最新动态'; 

$news_types_arr['student_award']['lang1'] = 'Student Award';
$news_types_arr['student_award']['lang2'] = '學生得獎作品';
$news_types_arr['student_award']['lang3'] = '学生得奖作品';

$news_types_arr['events']['lang1'] = 'Events';
$news_types_arr['events']['lang2'] = '活動';
$news_types_arr['events']['lang3'] = '活动';

$news_types_arr['public_events']['lang1'] = 'Public Events';
$news_types_arr['public_events']['lang2'] = '其他活動';
$news_types_arr['public_events']['lang3'] = '其他活动';

$news_types_arr['collaborative_projects']['lang1'] = 'Collaborative Projects';
$news_types_arr['collaborative_projects']['lang2'] = 'Collaborative Projects';
$news_types_arr['collaborative_projects']['lang3'] = 'Collaborative Projects';


$exhibiton_types = array(
	array('value' => 0, 'key' => 'communication_design', 'lang1' => 'Communication Design', 'lang2' => '傳意設計', 'lang3' => '传意设计'),
	array('value' => 1, 'key' => 'fashion', 'lang1' => 'Fashion', 'lang2' => '時裝設計', 'lang3' => '时装设计'),
	array('value' => 2, 'key' => 'multi_disciplinary', 'lang1' => 'Multi-disciplinary', 'lang2' => '跨領域', 'lang3' => '跨领域'),
	array('value' => 3, 'key' => 'photography_film', 'lang1' => 'Photography/ Film', 'lang2' => '攝影/電影', 'lang3' => '摄影/电影'),
	array('value' => 4, 'key' => 'product', 'lang1' => 'Product', 'lang2' => '產品設計', 'lang3' => '产品设计'),
    array('value' => 5, 'key' => 'spatial', 'lang1' => 'Spatial', 'lang2' => '空間設計', 'lang3' => '空间设计')
); // sync with wwwroot/CMS/include/config.php

$SYSTEM_SMTP_SERVER = "smtp.vtc.edu.hk";
$smtp_port = "25";
$smtp_require_auth = true;
$smtp_require_ssl = false;
$smtp_username = "hkdi-ereply@vtc.edu.hk";
$smtp_password = "bm2019-TKL";
$smtp_security = "tls";


$loaded_config = true;
?>